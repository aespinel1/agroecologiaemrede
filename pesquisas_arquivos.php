<?php

include("conexao.inc.php");


?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function excluiRelacionamento(arquivo) {
	if(confirm('Você tem certeza que deseja remover o anexo ' + arquivo + '?')) {
		window.location.href='xt_pes_exclui_arquivo.php?idPes=<?php echo $pesquisa; ?>&arquivo='+arquivo;
	}
}

function criticaFormAltera(action) {
	form1.action = action;
	form1.submit();
}
//-->
</script>

<?php include("menu_geral.php"); ?>
<br>
<br>
<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="567" valign="top"> <table width="553" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="19" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="41" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="473" bgcolor="#80AAD2"> <p><strong>Pesquisa - Anexos </strong><b></b></p></td>
          <td width="20" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="17" height="32"></div></td>
        </tr>
      </table>

	  <form name="form1" method="post">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
          <td height="24" colspan="4">
            <p> <strong>&nbsp;</strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4">
			 Pesquisa:
               <select name="pesquisa">
<?php
                if ($tipo == "NE") {
				   $sql = "SELECT * FROM pesquisa where pe_id = $pesquisa and cod_usuario = '$cookieUsuario' and (pe_liberacao  = 'E' or pe_liberacao = 'N' or pe_liberacao = 'P')";
				}
				else {
				  $sql = "SELECT * FROM pesquisa where cod_usuario = '$cookieUsuario' and (pe_liberacao  = 'E' or pe_liberacao = 'N' or pe_liberacao = 'P') ORDER BY pe_descricao";
				}

				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->pe_descricao);
					$varSelected = "";
					if (isset($pesquisa) && $pesquisa == $row->pe_id) {
						$varSelected = "selected";
					}
					echo "<option value=\"$row->pe_id\" $varSelected>$desc</option>";
				}
?>
              </select> &nbsp;&nbsp;&nbsp;
			  <?php if ($tipo == "") { ?>
			   <input type="button" class="botaoLogin" onClick="criticaFormAltera('pesquisas_arquivos.php');" value="   Selecionar   ">
			  <?php } ?>
            </td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		</table>
	  </form>
<?php
	if (isset($pesquisa)) {

		$sql = "SELECT * FROM pesquisa_arquivo WHERE pa_id_pesquisa = $pesquisa";
		$query2 = $db->query($sql);

       if (DB::isError($query2)) {
          die ($query2->getMessage().$sql);
        }
	    $num = $query2->num_rows;

?>

    <form name="form2" enctype="multipart/form-data" method="post" action="xt_pes_grava_arquivo.php">
	<input type="hidden" name="id" value="<?php echo $pesquisa; ?>">
      <table width="567" border="0" align="center" cellpadding="5" cellspacing="1">
        <tr>
          <td height="1" bgcolor="#000000" colspan="2"> <div align="center"></div></td>
        </tr>
          <tr>
            <td colspan="2">Anexar novo arquivo :
              <input name="arquivo" type="file"   size="50">
              <input name="Submit"  type="submit" value="  Anexar  "> </td>
        </tr>

        <tr>
          <td height="1" bgcolor="#000000" colspan="2"><div align="center"></div></td>
        </tr>
        <tr>

          <td colspan="2"> <p>
		  <?php

           if ($num == 0) {
				echo "N�o h� nenhum anexo para esta pesquisa";
			}
		   else {
		        echo "Anexos j� cadastrados para esta pesquisa";
			}
	       ?>
	      </td>
        </tr>
       <tr>
          <td height="1" bgcolor="#000000" colspan="2"><div align="center"></div></td>
        </tr>

<?php
function my_filesize($file) {
	if(!is_file($file)) exit ("Anexo inexistente !");
	$kb = 1024;         // Kilobyte
	$mb = 1024 * $kb;   // Megabyte
	$gb = 1024 * $mb;   // Gigabyte
	$tb = 1024 * $gb;   // Terabyte
	$size = filesize($file);
	if($size < $kb) {
		return $size." B";
	}
	else if ($size < $mb) {
		return round($size/$kb,2)." KB";
	}
	else if($size < $gb) {
		return round($size/$mb,2)." MB";
	}
	else if($size < $tb) {
		return round($size/$gb,2)." GB";
	}
	else {
		return round($size/$tb,2)." TB";
	}
}

while ($row = $query2->fetch_object()) {

?>
        <tr>
          <td width="20" > <div align="center"><a href="#" onClick="excluiRelacionamento('<?php echo $row->pa_arquivo; ?>');"><img src="imagens/trash.gif" border="0"></a></div></td>
          <td width="547"> <a href="#" onclick="wk = window.open('/upload/arquivos/<?php echo  $row->pa_arquivo; ?>','wk'); wk.focus(); return false;"><?php echo  $row->pa_arquivo; ?> </a> <?php echo " - "; ?>  <?php echo my_filesize("upload/arquivos/".$row->pa_arquivo); ?>
          </td>
        </tr>
        <?php
}
?>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
      </table>
     </form>
<?php
	} // fim do if que verifica se uma experiência foi buscada

?>


	  </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
