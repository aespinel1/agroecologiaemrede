<?php
include("conexao.inc.php");
?>

<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<?php include("menu_admin.php"); ?>
<br>
<br>

<table width="580" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="570" valign="top"> <table width="558" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="477" bgcolor="#80AAD2"> <p><strong>Experi&ecirc;ncias x Data </strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
	  <br>
	  <br>
<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

if ($dti == '') {
   $dt_inicio = '19990101'; }
else {
   $dt_inicio = substr($dti,6,4) . substr($dti,3,2) . substr($dti,0,2);
}


if ($dtf == '') {
    $dt_fim    = '29990101'; }
else {
    $dt_fim    = substr($dtf,6,4) . substr($dtf,3,2) . substr($dtf,0,2) ;
}
if (($dti == '') or  ($dtf == '')) {  echo " ";}
   else {
         $resultado = (mktime(0,0,0,substr($dti,3,2),substr($dti,0,2),substr($dti,6,4)) - mktime(0,0,0,substr($dtf,3,2),substr($dtf,0,2),substr($dtf,6,4) ));
         if ($resultado  > 0)  { echo "Data inicio maior que data fim. Foi assumido data fim n�o informada. ";
                                 $dtf = "";
                               }
                          else { echo " "; }
        }
if  (isset($ind_dt_inclusao) && $ind_dt_inclusao == "S" &&  isset($ind_dt_alteracao) && $ind_dt_alteracao == "S") {
    $sql   = "select us_nome,dt_criacao, dt_atualizacao from usuario  where date_format(dt_criacao,'%Y%m%d') between $dt_inicio and $dt_fim";
    $sql  .= " union all ";
    $sql  .= "select us_nome,dt_criacao, dt_atualizacao from usuario  where date_format(dt_atualizacao,'%Y%m%d') between $dt_inicio and $dt_fim AND dt_atualizacao IS NOT NULL";
}


if  ((isset($ind_dt_inclusao) && $ind_dt_inclusao == "S") &&  (!isset($ind_dt_alteracao) || $ind_dt_alteracao == ""))   {
    $sql   = "select us_nome,dt_criacao, dt_atualizacao from usuario  where date_format(dt_criacao,'%Y%m%d') between $dt_inicio and $dt_fim";
}



if  ((!isset($ind_dt_inclusao) || $ind_dt_inclusao == "") and  (isset($ind_dt_alteracao) && $ind_dt_alteracao == "S"))  {
    $sql  = "select us_nome,dt_criacao, dt_atualizacao from usuario  where date_format(dt_atualizacao,'%Y%m%d') between $dt_inicio and $dt_fim AND dt_atualizacao IS NOT NULL";
}

if  ((!isset($ind_dt_inclusao) || $ind_dt_inclusao == "") and  (!isset($ind_dt_alteracao) || $ind_dt_alteracao == "")) {
    $sql   = "select us_nome,dt_criacao, dt_atualizacao from usuario ";
}

$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}


$arquivotemp = tempnam($dir['upload'], "lista_pessoas_por_data").'.csv';

$abrir=fopen($arquivotemp,"w");

$conteudo = "Agroecologia em Rede";
fwrite($abrir,$conteudo."\n");
$conteudo = " ";
fwrite($abrir,$conteudo."\n");
$conteudo = "Lista de pessoas por data";

if ($dti == '' & $dtf != '') {
   $conteudo .= " até $dtf.";
}

if ($dti != '' & $dtf == '') {
   $conteudo .= " desde $dtf.";
}

if ($dti != '' & $dtf != '') {
   $conteudo .= " de $dti até $dtf.";
}

if ($dti == '' & $dtf == '') {
   $conteudo .= " em qualquer período.";
   echo "depois conteudo = $conteudo";
}

fwrite($abrir,$conteudo."\n");
$conteudo = " ";
fwrite($abrir,$conteudo."\n");
fwrite($abrir,$conteudo."\n");
$conteudo = "'Pessoa';'Data Inclusão';'Data Alteração'";
fwrite($abrir,$conteudo."\n");
$conteudo = " ";
fwrite($abrir,$conteudo."\n");

while ($rowcriterio = $query->fetch_object()) {
                $conteudo =  "'$rowcriterio->us_nome';'$rowcriterio->dt_criacao';'$rowcriterio->dt_atualizacao'";
                fwrite($abrir,$conteudo."\n");
			}


//$conteudo = "";
//fwrite($abrir,$conteudo."\n");

fclose($abrir);
chmod($arquivotemp, 0777);

$db->close();
echo "<br><br><br>";
echo "<center><a href='".$dir['upload_URL'].basename($arquivotemp)."'>Clique com o botão direito do mouse nesse link, selecione 'salvar destino como', confirme o nome do arquivo, assim o arquivo será baixado em seu computador.</a></center>";
echo "<br><br><br><center><a href='main.php'>Retornar à página principal</a></center>";
?>
