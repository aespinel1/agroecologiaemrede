<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

$tipoTabela = isset($_REQUEST['tipoTabela'])
	? $_REQUEST['tipoTabela']
	: '';

include("conexao.inc.php");

switch ($tipoTabela = $_REQUEST['tipoTabela']) {
	case 'GU':
		$varNome = "Grupos de Experimentadores e Usuários";
		$varTabela = "grupo_usuarios";
		$varCampo = "gu_descricao";
		$varID = "gu_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010100') AND us_admin='1' ";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'ID':
		$varNome = "Idiomas";
		$varTabela = "idioma";
		$varCampo = "idm_descricao";
		$varID = "idm_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010800') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'TB':
		$varNome = "Tipos de Bibliografia";
		$varTabela = "tipo_bibliografias";
		$varCampo = "tb_descricao";
		$varID = "tb_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;

	case 'TI':
		$varNome = "Tipos de Instituição";
		$varTabela = "tipo_instituicoes";
		$varCampo = "ti_descricao";
		$varID = "ti_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010400') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;

	case 'TX':
		$varNome = "Tipos de Experiências";
		$varTabela = "tipo_experiencias";
		$varCampo = "tx_descricao";
		$varID = "tx_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010600') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'SX':
		$varNome = "Status de Experiências";
		$varTabela = "status_experiencia";
		$varCampo = "se_status";
		$varID = "se_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010700') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'LK':
		$varNome = "Tipos de Links";
		$varTabela = "tipo_link";
		$varCampo = "descricao";
		$varID = "id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='060000' OR fu_id='060000') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'lista_identidades':
		$varNome = "Identidades";
		$varTabela = $tipoTabela;
		$varCampo = "nome";
		$varID = "id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'lista_nivel_educacional':
		$varNome = "Níveis educacionais";
		$varTabela = $tipoTabela;
		$varCampo = "nome";
		$varID = "id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'lista_regime_ensino':
		$varNome = "Regimes de ensino";
		$varTabela = $tipoTabela;
		$varCampo = "nome";
		$varID = "id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
} // fim do switch
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaForm() {
	desc = form1.descricao.value;
	if (desc.length == 0) {
		alert('Digite um nome válido!');
		form1.descricao.focus();
		return;
	}
	form1.submit();
}

function excluiRegistro(id,desc) {
	if(confirm('Você tem certeza que deseja excluir o registro '+desc+'?')) {
		window.location.href='xt_deleta_tabela_simples.php?tipoTabela=<?php echo $tipoTabela; ?>&id='+id;
	}
}
</script>

<?php include("menu_admin.php"); ?>
<table width="700" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="700" valign="top" align="center">
      <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_simples.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2">
		  <p><b>
<?php
			echo $varNome;
?>
            </b></p>
		  </td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
?>
	  <form name="form1" method="post" action="xt_insere_tabela_simples.php">
	  <input type="hidden" name="tipoTabela" value="<?php echo $tipoTabela; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2"><p>Adicionar novo registro
                em "<strong><?php echo $varNome; ?></strong>"</p></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="81"><div align="right">Descri&ccedil;&atilde;o:</div></td>
            <td width="483"> <input name="descricao" type="text" size="40" maxlength="80">
              <input name="enviar" type="button" class="botaoLogin" value="Adicionar" onClick="criticaForm();">
            </td>
          </tr>
        </table>
      </form>
	    <br>
	  <form name="form2" method="post" action="xt_atualiza_tabela_simples.php">
	  <input type="hidden" name="tipoTabela" value="<?php echo $tipoTabela; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="3"><p>Registros em "<strong><?php echo $varNome; ?></strong>"</p></td>
          </tr>
          <tr>
            <td height="1" colspan="3" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="35"> <div align="center"><strong>ID</strong></div></td>
            <td width="460"><strong>Descri&ccedil;&atilde;o</strong></td>
            <td width="38"> <div align="center"><strong>Excluir</strong></div></td>
          </tr>
          <?php
	$sql = "SELECT * FROM $varTabela ORDER BY $varCampo";
	$query = $db->query($sql);
	if (!$query) {
    	die($db->error);
	}
	$i = 0;
	while ($row = $query->fetch_object()) {
?>
          <input type="hidden" name="id<?php echo $i; ?>" value="<?php echo $row->$varID; ?>">
          <tr>
            <td align="right"><?php echo $row->$varID; ?></td>
            <td><input name="desc<?php echo $i; ?>" type="text" value="<?php echo trim($row->$varCampo); ?>" size="60" maxlength="80"></td>
            <td><div align="center"><a href="#" onClick="excluiRegistro('<?php echo $row->$varID; ?>','<?php echo trim($row->$varCampo); ?>')"><img src="imagens/ico_exclui.gif" width="18" height="18" border="0"></a></div></td>
          </tr>
          <?php
		$i++;
	} // la�o do loop
?>
          <tr>
            <td colspan="3">
              <div align="center">
                <input name="Submit" type="submit" class="botaoLogin" value="   Atualizar   ">
              </div></td>
          </tr>
        </table>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
	  </form>
<?php
} // fim do if que verifica se o usuário tem acesso a estas informações
?>

      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
