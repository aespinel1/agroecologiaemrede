<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040200') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
		
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página principal.</p>";
	$db->disconnect;
	return;
}

switch ($tipo) {
	case 'AG':
		$varNome = "Áreas Geográficas";
		$varTabela = "area_geografica";
		$varCampo = "ag_descricao";
		$varID = "ag_id";
		$varPertence = "ag_pertence";
		$varTabelaRel = "rel_geo_experiencia";
		$varCampoRel = "rge_id_geo";
		$varIDRel = "rge_id_experiencia";
		break;
	case 'AT':
		$varNome = "Áreas Temáticas";
		$varTabela = "area_tematica";
		$varCampo = "at_descricao";
		$varID = "at_id";
		$varPertence = "at_pertence";
		$varTabelaRel = "rel_area_experiencia";
		$varCampoRel = "rae_id_area";
		$varIDRel = "rae_id_experiencia";
		break;
	case 'IN':
		$varNome = "Institui��es";
		$varTabela = "frm_instituicao";
		$varCampo = "in_nome";
		$varID = "in_id";
		$varTabelaRel = "rel_inst_experiencia";
		$varCampoRel = "rie_id_inst";
		$varIDRel = "rie_id_experiencia";
		break;
	case 'EX':
		$varNome = "Experimentadores";
		$varTabela = "usuario";
		$varCampo = "us_nome";
		$varID = "us_login";
		$varTabelaRel = "rel_usuario_experiencia";
		$varCampoRel = "rue_id_usuario";
		$varIDRel = "rue_id_experiencia";
		break;
	case 'RB':
		$varNome = "Refer�ncias Bibliogr�ficas";
		$varTabela = "bibliografia";
		$varCampo = "bb_titulo_todo";
		$varID = "bb_id";
		$varTabelaRel = "rel_bib_experiencia";
		$varCampoRel = "rbe_id_bibliografia";
		$varIDRel = "rbe_id_experiencia";
		break;
	case 'BM':
		$varNome = "Biomas";
		$varTabela = "habitat";
		$varCampo = "ha_descricao";
		$varID = "ha_id";
		$varTabelaRel = "rel_habitat_experiencia";
		$varCampoRel = "rhe_id_habitat";
		$varIDRel = "rhe_id_experiencia";
		break;
} // fim do switch

			function untree($parent,$level,$tabela,$pertence,$campo,$id) {
				$sql = "SELECT * FROM $tabela WHERE $pertence=$parent ORDER BY $campo";
				$parentsql = $db->query($sql);
			    if (!$parentsql->num_rows) {
				    return;
			    }
	    		else {
			        while($branch = $parentsql->fetch_object()) {
	    		        $tempID = $branch->$id;
						$echo_this = "<option value=\"$tempID\">";
		                for ($x=1; $x<=$level; $x++) {
	    		            $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;";
		                }
						$temp = $branch->$campo;
						$echo_this .= "$temp</option>\n";
			            echo $echo_this;
	    		        $rename_level = $level;
		    	        untree($tempID,++$rename_level,$tabela,$pertence,$campo,$id);
        		    }
		        }
		    }
?>
<!DOCTYPE html>
<html>
<head>
<title>Relacionamentos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function excluiRelacionamento(id,nome) {
	if(confirm('Você tem certeza que deseja excluir o relacionamento ' + nome + '?')) {
		window.location.href='xt_exp_exclui_relacionamento.php?nome=<?php echo $nome; ?>&idExp=<?php echo $id; ?>&tipo=<?php echo $tipo; ?>&idRel='+id;
	}
}
</script>
</head>

<body bgcolor="#EBEBEB">
<form name="form1" method="post" action="xt_exp_insere_relacionamento.php">
<input type="hidden" name="tipo" value="<?php echo $tipo; ?>">
<input type="hidden" name="idExp" value="<?php echo $id; ?>">
<input type="hidden" name="nome" value="<?php echo $nome; ?>">
<table width="650" border="0" align="center" cellpadding="5" cellspacing="1">
  <tr> 
    <td colspan="2"> <p>Relacionamento: <strong><?php echo $varNome; ?></strong></p></td>
  </tr>
  <tr> 
    <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
  </tr>
  <tr> 
    <td width="200" valign="top"> <div align="right"><strong><?php echo $varNome; ?></strong>:
<?php
			if ($tipo != "BM") {
?>
		<br>
	    <br>
        Selecione todos os que forem necess&aacute;rios, segurando a tecla CTRL 
        do seu teclado.
<?php
			}
?>
		</div></td>
    <td width="449" valign="top">
<?php
			if ($tipo == "BM") {
?>
			<select name="rel[]">
<?php
			}
			else {
?>
			<select name="rel[]" size="15" multiple>
<?php
			}
?>
<?php
			if ($tipo == "AG" || $tipo == "AT") {
				$sql = "SELECT * FROM $varTabela WHERE $varPertence=0 ORDER BY $varCampo";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$descRel = $row->$varCampo;
					$idRel = $row->$varID;
					echo "<option value=\"$idRel\">$descRel</option>";
					if ($tipo == "AG" || $tipo == "AT") {
						untree($idRel,1,$varTabela,$varPertence,$varCampo,$varID);
					}
				}
			}
			else {
				$sql = "SELECT * FROM $varTabela ORDER BY $varCampo";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$descRel = $row->$varCampo;
					$idRel = $row->$varID;
					echo "<option value=\"$idRel\">$descRel</option>";
				}
			}
?>
      </select> </td>
  </tr>
  <tr bgcolor="#000000"> 
    <td height="1" colspan="2"> <div align="center"></div></td>
  </tr>
  <tr> 
    <td colspan="2"><div align="center"> 
          <input name="Submit" type="submit" class="botaoLogin" value="   INSERIR   ">
      </div></td>
  </tr>
  <tr bgcolor="#000000"> 
    <td height="1" colspan="2"> <div align="center"></div></td>
  </tr>
</table>
</form>
  <br>
<?php
$sql = "SELECT * FROM $varTabela,$varTabelaRel,experiencia WHERE $varIDRel=ex_id AND $varID=$varCampoRel ".
	   "AND ex_id=$id ORDER BY $varCampo";
$query = $db->query($sql);
$numRel = $query->num_rows;
?>

  
<table width="650" border="0" align="center" cellpadding="5" cellspacing="1">
  <tr> 
    <td colspan="2"> <p><strong><?php echo $varNome; ?></strong> para experiência <strong><?php echo trim($nome); ?></strong></p></td>
  </tr>
  <tr> 
    <td height="1" bgcolor="#000000" colspan="2"> <div align="center"></div></td>
  </tr>
<?php
while ($row = $query->fetch_object()) {
?>
  <tr> 
    <td width="20"><div align="center"><a href="#" onClick="excluiRelacionamento('<?php echo $row->$varCampoRel; ?>','<?php echo $row->$varCampo; ?>');"><img src="imagens/trash.gif" border="0"></a></div></td>
    <td width="630">
	<?php echo $row->$varCampo; ?>
	</td>
  </tr>
<?php
}
?>
  <tr bgcolor="#000000"> 
    <td height="1" colspan="2"> <div align="center"></div></td>
  </tr>
</table>
</body>
</html>
<?php
$db->disconnect;
?>
