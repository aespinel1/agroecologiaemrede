<?php

if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");
$var = array();
for ($i=0;$i<=5;$i++) {
	$var[$i] = new stdClass();
}
$var[0] = new stdClass();
$var[0]->txt='ID';
$var[0]->rows=1;
$var[0]->cols=4;
$var[1] = new stdClass();
$var[1]->txt='Descrição';
$var[1]->rows=1;
$var[1]->cols=20;

switch ($tipo) {
	case 'AG':
		$varNome = "Áreas Geográficas";
		$varTabela = "area_geografica";
		$var[0]->campo = "ag_descricao";
		$var[1]->campo = "ag_pertence";
		$ord='ag_descricao';
		$varID = "ag_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020100') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'AT':
		$varNome = "Áreas Temáticas";
		$varTabela = "area_tematica";
		$var[0]->campo = "at_descricao";
		$var[1]->campo = "at_pertence";
		$ord='at_descricao';
		$varID = "at_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'EB':
		$varNome = "Entidades Biológicas";
		$varTabela = "entidade_biologica";
		$var[0]->campo = "eb_descricao";
		$var[1]->campo = "eb_id_externo";
		$var[2]->campo = "eb_url";
		$ord='eb_descricao';
		$varID = "eb_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'HA':
		$varNome = "Biomas";
		$varTabela = "habitat";
		$var[0]->campo = "ha_descricao";
		$ord='ha_descricao';
		$varID = "ha_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020400') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'TE':
		$varNome = "Tecnologias";
		$varTabela = "tecnologia";
		$var[0]->campo = "tc_descricao";
		$var[1]->campo = "tc_pertence";
		$ord='tc_descricao';
		$varID = "tc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020600') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'LC':
		$varNome = "Localidades";
		$varTabela = "localidade";
		$var[0]->campo = "lc_descricao";
		$ord='lc_descricao';
		$varID = "lc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020700') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'ZC':
		$varNome = "Zonas Climáticas";
		$varTabela = "zona_climatica";
		$var[0]->campo = "zc_descricao";
		$ord='zc_descricao';
		$varID = "zc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='02000') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'VG':
		$varNome = "Tipos de Vegetação";
		$varTabela = "vegetacao";
		$var[0]->campo = "vg_descricao";
		$ord='vg_descricao';
		$varID = "vg_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020900') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'mapeo_mapa_campos':
		$varNome = "Campos dos formulários - ".$_REQUEST['frm'];
		$varTabela = $tipo;
		$var[0]->campo = "titulo";
		$var[0]->txt = "titulo";
		$var[0]->rows=3;
		$var[0]->cols=25;

		$var[1]->campo = "ord";
		$var[1]->txt = "ord";
		$var[1]->rows=1;
		$var[1]->cols=1;

		$var[2]->campo = "bloco";
		$var[2]->txt = "bloco";
		$var[2]->rows=3;
		$var[2]->cols=18;

		$var[3]->campo = "sub_bloco";
		$var[3]->txt = "sub_bloco";
		$var[3]->rows=3;
		$var[3]->cols=15;

		$var[4]->campo = "ajuda";
		$var[4]->txt = "ajuda";
		$var[4]->rows=3;
		$var[4]->cols=35;

		$var[5]->campo = "repeticoes";
		$var[5]->txt = "repeticoes";
		$var[5]->rows=1;
		$var[5]->cols=1;

		$hidden[0] = "frm";
		$hidden[1] = "ano";

		$wherefrm = (substr($_REQUEST['frm'],4,3)=='exp')
			? "(frm='".$_REQUEST['frm']."' OR frm='frm_exp_base_comum')"
			: "frm='".$_REQUEST['frm']."'";
		$where=" AND tipo_form!='ignorar' AND tipo_form!='hidden' AND ".$wherefrm;

		$varID = "campo";
		$ord='ord';
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
} // fim do switch
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaForm() {
	desc = form1.descricao.value;
	if (desc.length == 0) {
		alert('Digite um nome válido!');
		form1.descricao.focus();
		return;
	}
	form1.submit();
}

function excluiRegistro(id,desc) {
	if(confirm('Você tem certeza que deseja excluir o registro '+desc+'?')) {
		window.location.href='xt_deleta_tabela_complexa.php?tipoTabela=<?php echo $tipo; ?>&id='+id;
	}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function muda_destino(tipo) {
 document.form2.action = "tabela_complexa2.php?tipo=" + tipo;
}
</script>
<style type="text/css">
<!--
.style1 {
	font-size: 9px;
	font-weight: bold;
}
.TAM8 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-style: normal;
	font-weight: bolder;
	text-transform: uppercase;
}
-->
</style>

<?php include("menu_admin.php"); ?>
<table width="80%" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td valign="top" align="center">
	<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_complexa.gif" width="34" height="32"></p></td>
          <td bgcolor="#80AAD2"> <p><b>
              <?php
			echo $varNome;
?>
              </b></p></td>
          <td bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <?php
if ($numPerm == 0)
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
else {
?>
      <form name="form2" method="post" action="xt_atualiza_tabela_complexa2.php">
        <p>
          <input type="hidden" name="tipoTabela" value="<?=$tipo?>">
          <input type="hidden" name="frm" value="<?=$_REQUEST['frm']?>">
</p>
        <div align="center">          </div>
        <table width="70%" border="0" cellpadding="1" cellspacing="0">
          <tr>
            <td colspan="3">
			<p>Registros em "<strong><?php echo $varNome; ?></strong>"</p>
			<?php /*
			<p align="center">
			  <span class="style1">
			  <?php
			  for ($i=ord('A');$i<=ord('Z');$i++) {
			  ?>
              	<input name="letra" type="submit" class="TAM8" value="<?=chr($i)?>" onclick="muda_destino('<?=$tipo?>');">&nbsp;&nbsp;
              <?php } ?>
			  </span></p>
			<p align="center"><input type="submit" value="Todos" name="letra" class="TAM8" onclick="muda_destino('<?=$tipo?>');"></p>			<p align="center">&nbsp;            </p>
			*/ ?>
			</td>
          </tr><tr><td width="100%" height="3"></td></tr>
	    </table>
		  <table width="100%" border="0" cellpadding="2" cellspacing="1">
		  <?php $cabecalho = '
          <tr>
            <td height="1" colspan="'.(count($var)+1).'" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td> <div align="center"><strong>ID</strong></div></td>';
			foreach ($var as $v)
				$cabecalho.='<td> <div align="center"><strong>'.$v->txt.'</strong></div></td>'.chr(10);
			?> <!--<td> <div align="center"><strong>Excluir</strong></div></td>-->
			<?php

			$cabecalho .= '</tr>';

		if (!isset($letra) || !$letra)
			$letra = ".A";
		elseif ($letra == "Todos")
			$letra = "";

		//$sql = "SELECT * FROM $varTabela where ".$var[0]->campo." like '$letra%' ORDER BY ".$ord;
		$sql = "SELECT * FROM $varTabela where 1".$where." ORDER BY ".$ord;

		$query = $db->query($sql);

		if (!$query) {
    		die($db->error);
		}
		$i = 0;
		while ($row = $query->fetch_object()) {
			if ($i/10==floor($i/10))
				echo $cabecalho;
		?>
          <input type="hidden" name="r[<?=$i?>][<?=$varID?>]" value="<?php echo $row->$varID; ?>">
          <?php foreach ($hidden as $hid) { ?>
	          <input type="hidden" name="r[<?=$i?>][<?=$hid?>]" value="<?=$row->$hid?>">
	      <?php } ?>
          <tr>
            <td style="width:0;whitespace:nowrap;text-align:right"><?=$row->$varID?></td>
            <?php
            foreach ($var as $v) {
            	$campo=$v->campo;
            	if ($v->rows>1) {
	            ?>
	            <td><textarea name="r[<?=$i?>][<?=$campo?>]" ROWS="<?=$v->rows?>" COLS="<?=$v->cols?>"><?=trim($row->$campo)?></textarea></td>
				<?php } else {
				?>
				<td><input type="text" name="r[<?=$i?>][<?=$campo?>]" SIZE="<?=$v->cols?>" VALUE="<?=trim($row->$campo)?>" /></td>
				<?php
				}
			}
			?>
			<!--
            <td><div align="center"><a href="#" onClick="excluiRegistro('<?=$row->$varID?>','<?=trim($row->$varID)?>')"><img src="imagens/ico_exclui.gif" width="18" height="18" border="0"></a></div></td>
            -->
          </tr>
          <?php
		$i++;
	} // la�o do loop
?>
          <tr>
            <td colspan="10"> <div align="center">
                <input name="Submit" type="submit" class="botaoLogin" value="   Atualizar   ">
              </div></td>
          </tr>
        </table>
        <input type="hidden" name="qtItens" value="<?=$i?>">
      </form>
      <?php
} // fim do if que verifica se o usuário tem acesso a estas informações
?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
