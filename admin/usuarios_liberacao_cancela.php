<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$varDesc = "Cancela Libera��o";
$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='030000' OR fu_id='030500') AND us_admin='1' ";
$query = $db->query($sql);
$numPerm = $query->num_rows;



?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

function criticaFormFuncao() {
	cod = form1.cod.value;
	des = form1.desc.value;

	if (cod.length == 0) {
		alert('Digite um c�digo v�lido!');
		form1.cod.focus();
		return;
	}

	if (des.length == 0) {
		alert('Digite uma descrição v�lida!');
		form1.desc.focus();
		return;
	}

	form1.submit();
}

var ordem = new Array();
var i=1;
function inserir(func,descricao){
	var temp1 = func.substring(0,2);
	var temp2 = func.substring(2,6);
	if(temp2 == '0000'){
		for(h=1 ; h<=i-1 ; h++){
			if(ordem[h] != 0){
				var a = ordem[h].substring(0,2);
				var b = ordem[h].substring(2,6);
				if((a == temp1) && (b != '0000')){
					ordem[h] = 0;
				}
			}
		}
		desabilitar(temp1,descricao);
	}
	var p=1;
	var flag = true;
	while(ordem[p] != null){
		if(func != ordem[p]){
			p++;
		}
		else{
			ordem[p]=0;
			flag = false;
			break;
		}
	}
	if(flag){
		ordem[i] = func;
		i++;
	}
}
function mostrar(){
	for(j=1 ; j<=i-1 ; j++){
		if(ordem[j] != 0){
		    //alert('seq.valor= '+ordem[j]);
   			document.form2.sequencia.value = document.form2.sequencia.value + ordem[j] + " ";
		}
	}
	if(document.form2.sequencia.value == ''){
		if(!confirm('Nenhuma permiss�o foi selecionada! Deseja dar permiss�es a este Experimentador?')){
			window.location.href = 'usuarios.php?tipo=DP';
		}
	}
	else{
		var seq = document.form2.sequencia.value;
		if(confirm('Deseja salvar as novas permiss�es deste Experimentador?')){
			window.location.href = 'xt_usuario_atualiza_permissao.php?sequencia=' + seq + '&usuario=<?php echo $usuario; ?>';
		}
		else{
			document.form2.sequencia.value = '';
			//alert('seq= '+document.form1.sequencia.value);
		}
	}
}
function desabilitar(funcao,descricao){
 	var marcado = true;
	var str = 'window.document.form2.'+descricao;
	var tempobj = eval(str);
	if(tempobj.checked == 1) {
		marcado = false;
	}
	var cont = document.form2.elements.length;
	for(k=0; k<cont; k++){
		var cod1 = document.form2.elements[k].value;
		var cod2 = cod1.substring(0,2);
		var cod3 = cod1.substring(2,6);
		if(!marcado){
			if(cod2 == funcao){
				document.form2.elements[k].checked = 1;
				if(cod3 != '0000'){
					document.form2.elements[k].disabled = 1;
				}
			}
		}
		else{
			if(cod2 == funcao){
				document.form2.elements[k].checked = 0;
				if(cod3 != '0000'){
					document.form2.elements[k].disabled = 0;
				}
			}
		}
   	}
}

function criticaFormUsuario() {

	nome = formNE.nome.value;
	login = formNE.login.value;
	email = formNE.email.value;
	senha = formNE.senha.value;
	senhaConf = formNE.senhaConf.value;
	grupo = formNE.grupo.value;
	ag = formNE.agID.value;
	at = formNE.atID.value;

	if (nome.length == 0) {
		alert('Preencha um nome v�lido para o Experimentador/Usu�rio!');
		formNE.nome.focus();
		return;
	}

	if (login.length == 0 && grupo != "4") {
		alert('Preencha um login v�lido para o Usu�rio!');
		formNE.login.focus();
		return;
	}

	if (senha.length == 0 && grupo != "4") {
		alert('Preencha uma senha v�lida para o Usu�rio!');
		formNE.senha.focus();
		return;
	}

	if (senha != senhaConf) {
		alert('A senha n�o confere! Por favor, digite e confirme a senha do Experimentador!');
		formNE.senha.value = '';
		formNE.senhaConf.value = '';
		formNE.senha.focus();
		return;
	}

	if (email.length == 0 && grupo != "4") {
		alert('Preencha um e-mail v�lido para o Usu�rio!');
		formNE.email.focus();
		return;
	}

	if (ag.length == 0) {
		alert('Selecione pelo menos uma �rea Geogr�fica do seu interesse!');
		formNE.areasGeograficas.focus();
		return;
	}

	if (at.length == 0) {
		alert('Selecione pelo menos uma �rea Tem�tica do seu interesse!');
		formNE.areasTematicas.focus();
		return;
	}

	formNE.submit();
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function marca_desmarca_tabela () {
if (document.formNE.admin.checked == true) {
   var el = document.all['dados_especiais']
   el.style.display = "";
   }
else
   {
    var el = document.all['dados_especiais'];
    el.style.display = "none";
   }
}
</script>
<style type="text/css">
<!--
.style3 {color: #000000}
.style5 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style9 {color: #FFFFFF}
-->
</style>

<?php include("menu_admin.php"); ?>

<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>

    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_usuarios.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><b>Pessoas - <?php echo $varDesc ?></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
<?php
		if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {

				$sql = "SELECT * FROM usuario WHERE us_liberacao='S' order by us_nome";
				$query = $db->query($sql);
?>
	  <form name="formLULibera" method="post" action="xt_usu_cancela_libera_usuario.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
          <td height="24" colspan="4">
            <p> <strong>Cancelar libera��o </strong>
                <?php if (isset($usuLibera)) { ?>
                - pessoa(s) : realizado com sucesso!
                <?php } ?>
			</p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000" colspan="4"> <div align="center"></div></td>
          </tr>
<?php
					$i = 0;
					while ($row = $query->fetch_object()) {
?>
          <tr>
            <td width="23" ><div align="center"><input type="checkbox" name="usu<?php echo $i; ?>" value="<?php echo $row->us_login; ?>"></div></td>
			<td width="237" ><b><?php echo $row->us_nome; ?></b></td>
			<td width="126" ><b><?php echo $row->us_email; ?></b></td>
			<td width="126" ><b><?php echo $row->us_login; ?></b></td>
          </tr>
<?php
						$i++;
					}
?>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input Type="submit" class="botaoLogin" value="   Cancelar Libera��o ">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		</table>
	  </form>
<?php
			} // fim do if que verifica se o Experimentador tem acesso a estas informações

?>
      <p>&nbsp;</p>
      <p>&nbsp;</p></td>
  </tr>
</table>
 <p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
