<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='030000' OR fu_id='030101') AND us_admin='1' ";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function excluiUsuario(usr) {
	if(confirm('Você tem certeza que deseja excluir o usuário ' + usr + '?')) {
		window.location.href='xt_usuario_exclui_usuario.php?usuario='+usr;
	}
}
</script>

<?php include("menu_admin.php"); ?>

<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>

    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_usuarios.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Pessoa - Exclus�o</strong><b> </b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {

if (isset($usuario) == false) {
?>
		<form name="form1" method="post" action="usuarios_exclusao.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2"> <p> <strong>Selecione uma pessoa para exclus�o </strong>
                &nbsp;</p></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="60"> <div align="center">Pessoa : </div></td>
            <td width="474"> <select name="usuario">
<?php
		$sql = "SELECT * FROM usuario ORDER BY us_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
			$varSelected = "";
			if ($usuario == trim($row->us_nome)) {
				$varSelected = "selected";
			}
?>
                <option <?php echo $varSelected; ?> value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select>
              <input name="Submit" type="submit" class="botaoLogin" value="  Selecionar   ">
            </td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="2"> <div align="center"></div></td>
          </tr>
        </table>
		</form>

<?php
}

		if (isset($usuario)) {
			$sql = "SELECT * FROM usuario WHERE us_login='$usuario'";
			$query = $db->query($sql);
			$rowUSR = $query->fetch_object();
?>
      <form name="formCU" method="post" action="xt_usuario_exclui_usuario.php">
	  <input type="hidden" name="login" value="<?php echo trim($rowUSR->us_login); ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="4"> <p> <strong>Dados da Pessoa </strong>
              </p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="80"> <div align="right">Nome :</div></td>
            <td colspan="3"><input name="nome" type="text" id="nome" size="89" maxlength="255" value="<?php echo $rowUSR->us_nome; ?>"></td>
          </tr>
          <tr>
            <td width="80" > <div align="right">E-mail :</div></td>
            <td width="227"><input name="email" type="text" id="email" size="40" maxlength="255" value="<?php echo $rowUSR->us_email; ?>"></td>
          </tr>
          <tr>
            <td width="80" > <div align="right">Website :</div></td>
            <td colspan="3"><input name="website" type="text" id="website" value="<?php echo $rowUSR->us_website; ?>" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td colspan="3"><div align="center">                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input name="button" type="button" class="botaoLogin" onClick="excluiUsuario('<?php echo $usuario; ?>');" value="   Excluir  ">&nbsp;&nbsp;
		        <input name="button" type="submit" class="botaoLogin" value="   Selecionar Outra  ">

              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
      </form>
	    <br>	<form name="formTema" method="post">
	  <?php
		} // fim do if que verifica se o usuário j� foi selecionado

} // fim do if que verifica se o usuário tem acesso a estas informações
?>
    </form>
	</td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
