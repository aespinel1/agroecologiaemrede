<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");
$sql = "SELECT * FROM usuario WHERE us_login='$cookieUsuario' AND us_session='$cookieSession' AND us_admin=1";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}
$row = $query->fetch_object();
$usr = $row->us_login;
$psd = $row->us_senha;
$nom = $row->us_nome;

$sql = "SELECT count(log_usuario) as qtAcesso FROM log_acesso WHERE log_usuario='$cookieUsuario'";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}
$row = $query->fetch_object();
$cnt = $row->qtAcesso;

$sql = "SELECT count(ex_id) AS count FROM experiencia";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}
$row = $query->fetch_object();
$qtExperiencias = $row->count;

$sql = "SELECT count(us_login) AS count FROM usuario";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}
$row = $query->fetch_object();
$qtUsuarios = $row->count;

$sql = "SELECT count(in_id) AS count FROM frm_instituicao";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}
$row = $query->fetch_object();
$qtInstituicoes = $row->count;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {
	color: #333399;
	font-weight: bold;
}
.style2 {color: #FFCC00}
-->
</style>
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57"> 
      <div align="center"><a class="Branco" href="xt_logout.php"><img src="imagens/ico_logoff.gif" border="0"><br><br>
        <strong>Sair</strong></a></div>
	</td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> 
      <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="256" bgcolor="#80AAD2"><p >Bem-vindo, <strong><?php echo $nom; ?></strong></p></td>
          <td width="257" bgcolor="#80AAD2"><p align="right">Este &eacute; 
              o seu acesso de n&uacute;mero <strong><?php echo $cnt; ?></strong></p></td>
          <td width="18" bgcolor="#80AAD2"> 
            <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr> 
          <td colspan="3"><p><strong>SisGEA / CNiP</strong></p></td>
        </tr>
        <tr> 
          <td height="1" colspan="3" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr> 
          <td colspan="2"><img src="imagens/seta02.gif" width="12" height="13"> 
            <strong>Estat&iacute;sticas</strong><br>
            <br> &nbsp;&nbsp;&nbsp;&nbsp;<?php echo "<strong>$qtExperiencias</strong> Experiências"; ?><br> 
            &nbsp;&nbsp;&nbsp;&nbsp;<?php echo "<strong>$qtUsuarios</strong> Experimentadores"; ?><br> 
            &nbsp;&nbsp;&nbsp;&nbsp;<?php echo "<strong>$qtInstituicoes</strong> Institui��es"; ?><br> 
          </td>
          <td width="174"><p><img src="imagens/seta02.gif" width="12" height="13"> 
              <strong>Telefones &Uacute;teis<br>
              </strong><br>
              &nbsp;&nbsp;&nbsp;&nbsp;Tel: 81539207 Kleber <br>
              &nbsp;&nbsp;&nbsp;&nbsp;Tel: 81539207 Claudia</p></td>
        </tr>
        <tr> 
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="3"><p><strong>&Uacute;ltimas experi&ecirc;ncias 
              cadastradas</strong></p></td>
        </tr>
        <tr> 
          <td height="1" colspan="3" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr> 
          <td colspan="3"> 
            <?php
	$sql = "SELECT * FROM experiencia,usuario WHERE us_login=ex_usuario ORDER BY ex_id DESC LIMIT 0,5";
	$query = $db->query($sql);
	while ($row = $query->fetch_object()) {
		$data = explode("-",$row->dt_criacao);
		echo "<img src=\"imagens/seta02.gif\"> <b>$row->ex_descricao</b>, cadastrada por <b>$row->us_nome</b> em <b>$data[2]/$data[1]/$data[0]</b><br>";
	}
?>
          </td>
        </tr>
        <tr> 
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="3"><p><strong>Administradores do 
              sistema </strong></p></td>
        </tr>
        <tr> 
          <td height="1" colspan="3" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr> 
          <td colspan="3"> 
<?php
	$sql = "SELECT * FROM usuario WHERE us_admin='1'";
	$query = $db->query($sql);
	while ($row = $query->fetch_object()) {
		echo "<img src=\"imagens/seta02.gif\"> <b>$row->us_nome</b>, e-mail: <a href=\"mailto:$row->us_email\">$row->us_email</a><br>";
	}
?>
          </td>
        </tr>
        <tr> 
          <td colspan="3">&nbsp;</td>
        </tr>
        <tr> 
          <td colspan="3">&nbsp; </td>
        </tr>
        <tr> 
          <td width="170"> <div align="center"><a href="docs/manual.pdf" class="Preto"><img src="imagens/ico_pdf.gif" width="32" height="32" border="0"><br>
              Manual SisGEA</a></div></td>
          <td width="179"><div align="center"><a href="http://www.utd.com.br" target="_blank"><span class="textoPreto"><span class="style1">U <span class="style2">T</span> D</span></span></a><span class="textoPreto"><br>
              <span class="style1">Desenvolvimento</span></span></div></td>
          <td><div align="center"><a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank"><img src="imagens/getacro.gif" width="88" height="31" border="0"></a></div></td>
        </tr>
      </table>
		
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
