<?php
require("conexao.inc.php");

$login = isset($_REQUEST['login'])
  ? str_replace(' ', '', trim(tira_acentos(strtolower($_REQUEST['login']))))
  : null;
$senha = isset($_REQUEST['senha'])
  ? $_REQUEST['senha']
  : null;

$sql = "SELECT * FROM usuario WHERE us_login LIKE '$login' AND us_liberacao='S' AND us_admin='1'";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}
$row = $query->fetch_object();
$usr = $row->us_login;
$psd = $row->us_senha;

if (  ($psd == "" && $senha != "") or ($psd != "" && $senha == "") ) {
?>
	<script language="JavaScript">
		window.location.href='index.php?authFailed=1';
	</script>
<?php
    return;
}

if (trim($login) != trim($usr)) {
?>
	<script language="JavaScript">
		window.location.href='index.php?authFailed=2';
	</script>
<?php

	return;
}

$psd = $row->us_senha;

if ($senha != "" or $psd != "") {
  $psd = $row->us_senha;

  if ( md5(trim($senha)) != $row->us_senha ) {
     $psd = $row->us_senha;
     ?>
     <script language="JavaScript">
	     window.location.href='index.php?authFailed=3'
     </script>
     <?php
     return;
  }
}


session_start();
$id = session_id();
setcookie("cookieUsuario",$login, 0,'/');
setcookie("cookieSession",$id, 0,'/');
setcookie("cookieAdmin",1, 0,'/');

$data = date("Y-m-d");
$hora = date("H:i:s");

$sql = "INSERT INTO log_acesso (log_usuario) VALUES ('$usr')";
$query = $db->query($sql);
if (!$query) {
  die($db->error);
}

$sql = "UPDATE usuario SET us_session='$id' WHERE us_login LIKE '$login'";
$query = $db->query($sql);
if (!$query) {
  die($db->error);
}

$db->close();

?>
<script language="JavaScript">
	window.location.href='main.php';
</script>
