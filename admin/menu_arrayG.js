// Milonic DHTML Menu version 3.3

// Please note that major changes to this file have been made and is not compatible with earlier versions..

//

// You no longer need to number your menus as in previous versions. 

// The new menu structure allows you to name the menu instead. This means that you to remove menus and not break the system.

// The structure should also be much easier to modify, add & remove menus and menu items.

// 

// If you are having difficulty with the menu please read the FAQ at http://www.milonic.co.uk/menu/faq.php before contacting us.

//

// Please note that the above text CAN be erased if you wish.





// Milonic DHTML Menu version 3.2.0

// The following line is critical for menu operation, and must appear only once.

menunum=0;menus=new Array();_d=document;function addmenu(){menunum++;menus[menunum]=menu;}function dumpmenus(){mt="<script language=javascript>";for(a=1;a<menus.length;a++){mt+=" menu"+a+"=menus["+a+"];"}mt+="<\/script>";_d.write(mt)}

//Please leave the above line intact. The above also needs to be enabled.







////////////////////////////////////

// Editable properties START here777777 //

////////////////////////////////////



timegap=500					// The time delay for menus to remain visible

followspeed=5				// Follow Scrolling speed

followrate=20				// Follow Scrolling Rate

suboffset_top=9;			// Sub menu offset Top position 

suboffset_left=20;			// Sub menu offset Left position



effect = "fade(duration=0.2);Shadow(color='#ffffcc', Direction=135, Strength=10)" // Special effect string for IE5.5 or above please visit http://www.milonic.co.uk/menu/filters_sample.php for more filters



style1=[					// style1 is an array of properties. You can have as many property arrays as you need. This means that menus can have their own style.
"000000",					// Mouse Off Font Color
"F1B616",					// Mouse Off Background Color
"006633",					// Mouse On Font Color
"ffffcc",					// Mouse On Background Color
"000000",					// Menu Border Color
9,						    // Font Size
"normal",					// Font Style 
"bold",					    // Font Weight
"Verdana,Tahoma,Helvetica",	// Font Name
4,						    // Menu Item Padding
"comments.gif",							// Sub Menu Image (Leave this blank if not needed)
2,						    // 3D Border & Separator bar
"000000",					// 3D High Color "3666ff"
"000000",					// 3D Low Color
,							// Referer Item Font Color (leave this blank to disable)
,							// Referer Item Background Color (leave this blank to disable)
,							// Top Bar image (Leave this blank to disable)
,							// Menu Header Font Color (Leave blank if headers are not needed)
,							// Menu Header Background Color (Leave blank if headers are not needed)

]



style2=[					// style1 is an array of properties. You can have as many property arrays as you need. This means that menus can have their own style.
"000000",					// Mouse Off Font Color
"F1B616",					// Mouse Off Background Color
"006633",					// Mouse On Font Color
"ffffcc",					// Mouse On Background Color
"000000",					// Menu Border Color
9,						    // Font Size
"normal",					// Font Style 
"bold",					    // Font Weight
"Verdana,Tahoma,Helvetica",	// Font Name
4,						    // Menu Item Padding
"clientsc.gif",							// Sub Menu Image (Leave this blank if not needed)
2,						    // 3D Border & Separator bar
"000000",					// 3D High Color "3666ff"
"000000",					// 3D Low Color
,							// Referer Item Font Color (leave this blank to disable)
,							// Referer Item Background Color (leave this blank to disable)
,							// Top Bar image (Leave this blank to disable)
,							// Menu Header Font Color (Leave blank if headers are not needed)
,							// Menu Header Background Color (Leave blank if headers are not needed)

]

addmenu(menu=[				// This is the array that contains your menu properties and details
"menu_principal",       		// Menu Name - This is needed in order for the menu to be called
52,					// Menu Top - The Top position of the menu in pixels
-1,					// Menu Left - The Left position of the menu in pixels
96,			    // Menu Width - Menus width in pixels
0,					// Menu Border Width 
"left",		        	    // Screen Position - here you can use "center;left;right;middle;top;bottom" or a combination of "center:middle"
style1,						// Properties Array - this is set higher up, as above
1,							// Always Visible - allows the menu item to be visible at all time
"center",					// Alignment - sets the menu elements text alignment, values valid here are: left, right or center
,							// Filter - Text variable for setting transitional effects on menu activation - see above for more info
,							// Follow Scrolling - Tells the menu item to follow the user down the screen (visible at all times)
1, 							// Horizontal Menu - Tells the menu to become horizontal instead of top to bottom style
,							// Keep Alive - Keeps the menu visible until the user moves over another menu or clicks elsewhere on the page
"right",					// Position of TOP sub image left:center:right
"form",						// Type of menu use "form" or blank
,							// Right To Left - Used in Hebrew for example.
,							// Open the Menus OnClick - leave blank for OnMouseover
,							// ID of the div you want to hide on MouseOver (useful for hiding form elements)
,							// Reserved for future use
,							// Reserved for future use
,							// Reserved for future use

,"Experi�ncias","show-menu=menu_experiencias",,,1		        // "Description Text", "URL", "Alternate URL", "Status", "Separator Bar"
,"Pesquisas","show-menu=menu_pesquisas",,"",1
,"Pessoas","show-menu=menu_pessoa",,"",1
,"Institui��es","show-menu=menu_instituicao",,"",1
,"Links","show-menu=menu_links",,"",1
,"Tabelas","show-menu=menu_tabelas_auxiliares",,"",1
,"Consultas","show-menu=menu_relatorios",,"",1
,"Principal","main.php target=_self",,"",1

])



addmenu(menu=["menu_experiencias",
,,170,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Cadastrar", "experiencias_cadastro.php target=_self",,,0
," Alterar", "experiencias_altera.php target=_self",,,0
," An�lise", "experiencias_libera.php target=_self",,,0
," Cancelar Libera��o/Rejei��o", "experiencias_cancela_liberacao.php target=_self",,,0
," Excluir", "experiencias_excluir.php target=_self",,,0
," Anexos", "experiencias_arquivos.php target=_self",,,0
," Coment�rios", "show-menu=menu_experiencias_comentario",,,0

])

addmenu(menu=["menu_experiencias_comentario",
,,170,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," An�lise", "experiencias_libera_comentario.php target=_self",,,0
," Cancelar Libera��o/Rejei��o", "experiencias_cancela_liberacao_comentario.php target=_self",,,0
])

addmenu(menu=["menu_pesquisas",

,,170,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Cadastrar", "pesquisas_cadastro.php target=_self",,,0
," Alterar", "pesquisas_altera.php target=_self",,,0
," An�lise", "pesquisas_libera.php target=_self",,,0
," Cancelar Libera��o/Rejei��o", "pesquisas_cancela_liberacao.php target=_self",,,0
," Excluir", "pesquisas_excluir.php target=_self",,,0
," Anexos", "pesquisas_arquivos.php target=_self",,,0
," Coment�rios", "show-menu=menu_pesquisas_comentario",,,0
])


addmenu(menu=["menu_pesquisas_comentario",

,,170,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," An�lise", "pesquisa_libera_comentario.php target=_self",,,0
," Cancelar Libera��o/Rejei��o", "pesquisa_cancela_liberacao_comentario.php target=_self",,,0
])

addmenu(menu=["menu_cd",

,,150,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Cadastrar", "bibliografia.php?tipo=NR target=_self",,,0
," Alterar", "bibliografia.php?tipo=AR  target=_self",,,0
," Liberar", "bibliografia.php?tipo=LR target=_self",,,0
," Cancelar Libera��o", "bibliografia.php?tipo=CL target=_self",,,0
," Excluir", "bibliografia.php?tipo=ER target=_self",,,0

])



addmenu(menu=["menu_pessoa",

,,200,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Cadastrar", "usuarios.php target=_self",,,0
," Alterar", "usuarios_cadastro.php target=_self",,,0
," Liberar", "usuarios_liberacao.php target=_self",,,0
," Cancelar Libera��o", "usuarios_liberacao_cancela.php target=_self",,,0
," Excluir", "usuarios_exclusao.php?tipo=EX target=_self",,,0

])



addmenu(menu=["menu_instituicao",
,,150,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Cadastrar", "instituicoes_cadastro.php target=_self",,,0
," Alterar", "instituicoes_altera.php  target=_self",,,0
," Liberar", "instituicoes_liberacao.php target=_self",,,0
," Cancela Libera��o", "instituicoes_liberacao_cancela.php target=_self",,,0
," Excluir", "instituicoes_exclusao.php target=_self",,,0

])





addmenu(menu=["menu_tabelas_auxiliares",
,,200,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," �reas geogr�ficas", "tabela_complexa.php?tipo=AG target=_self",,,0
," Áreas Temáticas", "tabela_complexa.php?tipo=AT  target=_self",,,0
," Biomas", "tabela_complexa.php?tipo=HA target=_self",,,0
," Idiomas", "tabela_simples.php?tipoTabela=ID target=_self",,,0
," Tipos de bibliografias", "tabela_simples.php?tipoTabela=TB target=_self",,,0
," Tipos de institui��o", "tabela_simples.php?tipoTabela=TI target=_self",,,0
," Textos/Par�metros do sistema", "textos.php target=_self",,,0

])



addmenu(menu=["menu_relatorios",
,,230,1,"",style2,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Buscas mais Realizadas", "rel_mais_procurados.php target=_self",,,0
," Lista de Endere�os", "show-menu=menu_lista_enderecos",,,0
," Lista de Cadastros", "show-menu=menu_lista_cadastros",,,0
," Evolu��o", "rel_evolucao.php target=_self",,,0
,"Estat�sticas de Acesso (Rits)","http://www.agroecologiaemrede.org.br/awstats target=_self",,,0

])

addmenu(menu=["menu_lista_enderecos",
,,150,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Institui��es", "rel_enderecos_instituicoes.php target=_self",,,0
," Pessoas", "rel_enderecos_pessoas.php target=_self",,,0
])

addmenu(menu=["menu_lista_cadastros",
,,150,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Pesquisas x Data", "rel_pesquisa_data.php target=_self",,,0
," Experi�ncias x Data", "rel_experiencia_data.php target=_self",,,0
," Pessoas x Data", "rel_pessoa_data.php target=_self",,,0
])

addmenu(menu=["menu_links",
,,150,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Gerenciamento", "links.php target=_self",,,0
," Categorias", "tabela_simples.php?tipoTabela=LK  target=_self",,,0

])







//////////////////////////////////

// Editable properties END here  //

//////////////////////////////////

dumpmenus() // This must be the last line in this file

