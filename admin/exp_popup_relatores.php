<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040200') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
		
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página principal.</p>";
	$db->disconnect;
	return;
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Relacionamentos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function excluiRelacionamento(id,nome) {
	if(confirm('Você tem certeza que deseja excluir o relator ' + nome + '?')) {
		window.location.href='xt_exp_exclui_relator.php?nome=<?php echo $nome; ?>&idExp=<?php echo $id; ?>&idRelator='+id;
	}
}
function excluiRelacionamentoInst(id,nome) {
	if(confirm('Você tem certeza que deseja excluir a instituição ' + nome + '?')) {
		window.location.href='xt_exp_exclui_relator.php?nome=<?php echo $nome; ?>&idExp=<?php echo $id; ?>&idInst='+id;
	}
}
</script>
</head>

<body bgcolor="#EBEBEB">
<form name="form1" method="post" action="xt_exp_insere_relator.php">
<input type="hidden" name="idExp" value="<?php echo $id; ?>">
<input type="hidden" name="nome" value="<?php echo $nome; ?>">
<table width="650" border="0" align="center" cellpadding="5" cellspacing="1">
  <tr> 
    <td colspan="2"> <p><strong>Relatores</strong></p></td>
  </tr>
  <tr> 
    <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
  </tr>
  <tr> 
    <td width="200" valign="top"> <div align="right"><strong>Relatores</strong>:<br>
        <br>
          Selecione todos os que forem necess&aacute;rios, segurando a tecla CTRL 
          do seu teclado.</div></td>
      <td width="449" valign="top"> <select name="relator[]" size="15" multiple id="relator[]">
          <?php
		$sql = "SELECT * FROM usuario WHERE us_relator='1' AND us_liberacao='S' ORDER BY us_login";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
			$varSelected = "";
			if ($usuario == trim($row->us_login)) {
				$varSelected = "selected";
			}
?>
          <option value="<?php echo trim($row->us_login); ?>" <?php echo $varSelected; ?>><?php echo trim($row->us_nome); ?></option>
          <?php
		} // la�o do loop
?>
        </select>
        <select name="instRelator[]" size="15" multiple id="instRelator[]">
          <?php
		$sql = "SELECT in_id,in_nome FROM frm_instituicao WHERE in_liberacao='S' ORDER BY in_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
?>
          <option value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
          <?php
		} // la�o do loop
?>
        </select> </td>
  </tr>
  <tr bgcolor="#000000"> 
    <td height="1" colspan="2"> <div align="center"></div></td>
  </tr>
  <tr> 
    <td colspan="2"><div align="center"> 
          <input name="Submit" type="submit" class="botaoLogin" value="   INSERIR   ">
      </div></td>
  </tr>
  <tr bgcolor="#000000"> 
    <td height="1" colspan="2"> <div align="center"></div></td>
  </tr>
</table>
</form>
  <br>
<?php
	$sql = "SELECT * FROM experiencia_relator,usuario WHERE exr_id_usuario=us_login AND exr_id_experiencia=$id";
	$query = $db->query($sql);
?>

  
<table width="650" border="0" align="center" cellpadding="5" cellspacing="1">
  <tr> 
    <td colspan="2"> <p><strong>Relatores</strong> para experiência <strong><?php echo trim($nome); ?></strong></p></td>
  </tr>
  <tr> 
    <td height="1" bgcolor="#000000" colspan="2"> <div align="center"></div></td>
  </tr>
<?php
while ($row = $query->fetch_object()) {
?>
  <tr> 
    <td width="20"><div align="center"><a href="#" onClick="excluiRelacionamento('<?php echo $row->us_login; ?>','<?php echo $row->us_nome; ?>');"><img src="imagens/trash.gif" border="0"></a></div></td>
    <td width="630">
	<?php echo $row->us_nome; ?>
	</td>
  </tr>
<?php
}
?>
  <tr bgcolor="#000000"> 
    <td height="1" colspan="2"> <div align="center"></div></td>
  </tr>
</table>
<?php
	$sql = "SELECT * FROM experiencia_relator,frm_instituicao WHERE exr_id_inst=in_id AND exr_id_experiencia=$id";
	$query = $db->query($sql);
?>

  
<table width="650" border="0" align="center" cellpadding="5" cellspacing="1">
  <tr> 
    <td colspan="2"> <p><strong>Autores (INSTITUI��ES)</strong> para experiência <strong><?php echo trim($nome); ?></strong></p></td>
  </tr>
  <tr> 
    <td height="1" bgcolor="#000000" colspan="2"> <div align="center"></div></td>
  </tr>
<?php
while ($row = $query->fetch_object()) {
?>
  <tr> 
    <td width="20"><div align="center"><a href="#" onClick="excluiRelacionamentoInst('<?php echo $row->in_id; ?>','<?php echo $row->in_nome; ?>');"><img src="imagens/trash.gif" border="0"></a></div></td>
    <td width="630">
	<?php echo $row->in_nome; ?>
	</td>
  </tr>
<?php
}
?>
  <tr bgcolor="#000000"> 
    <td height="1" colspan="2"> <div align="center"></div></td>
  </tr>
</table>
</body>
</html>
<?php
$db->disconnect;
?>
