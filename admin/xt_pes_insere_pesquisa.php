<?php

$atID = $_POST["atID"];
$agID = $_POST["agID"];
$biomas = $_POST["biomas"];

if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040100') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
		
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você n�o tem acesso a inserir estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página principal.</p>";
	$db->disconnect;
	return;
}

function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) { 
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}


$sql = "SELECT pe_id FROM pesquisa WHERE pe_descricao='$nome'";
$query = $db->query($sql);
$row = $query->fetch_object();
$pesquisaID = $row->pe_id;

if (isset($pesquisaID)) {
?>

		<script language="JavaScript">

			alert('Já existe uma pesquisa com este t�tulo ! ');

			window.location.href='pesquisas_cadastro.php';

		</script>

<?php
}


$data = date("Y-m-d");
$hora = date("H:i:s");
$sql = "INSERT INTO pesquisa (pe_descricao,pe_resumo,pe_liberacao,pe_usuario,pe_data_proc,pe_hora_proc,pe_chamada,dt_criacao,txt_comentario,ano_publicacao) ".
	   "VALUES ('$nome','$resumo','N','$cookieUsuario','$data','$hora','$nome','$current_date','$comentario','$ano_publicacao')";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "SELECT pe_id FROM pesquisa WHERE pe_data_proc='$data' AND pe_hora_proc='$hora' AND pe_descricao='$nome'";
$query = $db->query($sql);
$row = $query->fetch_object();
$pesquisaID = $row->pe_id;


if (isset($autor)) { 
	$i=0; 
	while($i<count($autor)){ 
		$autorID = $autor[$i];
		$sql = "INSERT INTO pesquisa_autor VALUES ($pesquisaID,'$autorID',0)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

if (isset($instAutor)) { 
	$i=0; 
	while($i<count($instAutor)){ 
		$instID = $instAutor[$i];
		$sql = "INSERT INTO pesquisa_autor VALUES ($pesquisaID,'NULL',$instID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}




if (strlen(trim($agID)) > 0) {
	$arrayAG = explode(";",$agID);
	$arrayAG = my_array_unique($arrayAG);
	for ($x = 0; $x < count($arrayAG); $x++) {
		$temp = $arrayAG[$x];
		if (strlen(trim($temp)) > 0) {
			$sql = "INSERT INTO rel_geo_pesquisa VALUES ($temp,$pesquisaID)";	
			$query = $db->query($sql);
		}
	}
}


if (strlen(trim($atID)) > 0) {
	$arrayAT = explode(";",$atID);
	$arrayAT = my_array_unique($arrayAT);
	for ($x = 0; $x < count($arrayAT); $x++) {
		$temp = $arrayAT[$x];
		if (strlen(trim($temp)) > 0) {
			$sql = "INSERT INTO rel_area_pesquisa VALUES ($temp,$pesquisaID)";	
			$query = $db->query($sql);
		}
	}
}


if (strlen(trim($biomas)) > 0) {
	$sql = "INSERT INTO rel_habitat_pesquisa VALUES ($biomas,$pesquisaID)";	
	$query = $db->query($sql);
}



$db->close();
?>
<script language="JavaScript">
	window.location.href='pesquisas_arquivos.php?tipo=NE&pesquisa=<?php echo $pesquisaID; ?>&pesDescricao=<?php echo trim($nome); ?>';
</script>