<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='060000') AND us_admin=1";
$numPerm = faz_query($sql,$db,'num_rows');
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaForm() {
	desc = form1.nome.value;
	url = form1.url.value;
	if (desc.length == 0) {
		alert('Digite um nome válido!');
		form1.nome.focus();
		return;
	}
	if (url.length == 0) {
		alert('Digite uma url válida!');
		form1.url.focus();
		return;
	}
	form1.submit();
}

function excluiRegistro(id,desc) {
	if(confirm('Você tem certeza que deseja excluir o registro '+desc+'?')) {
		window.location.href='xt_deleta_link.php?id='+id;
	}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
</script>

<?php include("menu_admin.php"); ?>
<table width="700" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="700" valign="top" align="center">
	<table width="653" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="22" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="45" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_complexa.gif" width="34" height="32"></p></td>
          <td width="536" bgcolor="#80AAD2"> <p><b>Links</b></p></td>
          <td width="50" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
?>
      <form name="form1" method="post" action="xt_insere_link.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2"><p>Adicionar novo Link</p></td>
          </tr>
          <tr>
            <td height="1" colspan="10" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="81"><div align="right">Descri&ccedil;&atilde;o :</div></td>
            <td width="483"> <input name="nome" type="text" size="40" maxlength="80"></td>
          </tr>
          <tr>
            <td width="81"><div align="right">URL :</div></td>
            <td width="483"> <input name="url" type="text" size="40" maxlength="80"></td>
          </tr>
<?php
		$sql = "SELECT * FROM tipo_link ORDER BY descricao";
		$query = $db->query($sql);
?>
          <tr>
            <td width="81"><div align="right">Categoria :</div></td>
            <td>
			  <select name="tipo">
<?php
		while ($row = $query->fetch_object()) {
?>
			    <option value="<?php echo $row->id; ?>"><?php echo trim($row->descricao); ?></option>
<?php
		}
?>
              </select> </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input name="enviar" type="button" class="botaoLogin" value="Adicionar" onClick="criticaForm();"></td>
          </tr>
        </table>
      </form>
      <br>
	  <form name="form2" method="post" action="xt_atualiza_links.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="3">
			<p>&nbsp;</p>
			</td>
          </tr>
          <tr>
            <td height="1" colspan="10" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td><strong>Descri&ccedil;&atilde;o</strong></td>
            <td><strong>URL</strong></td>
            <td><strong>Tipo</strong></td>
            <td width="38"> <div align="center"><strong>Excluir</strong></div></td>
          </tr>
<?php
	$sql = "SELECT l.id as lid,tipo,nome,url,t.id as tid,descricao FROM link l,tipo_link t WHERE tipo=t.id ORDER BY descricao,nome";
	$query = $db->query($sql);
	if (!$query) {
   		die($db->error);
	}
	$i = 0;
	while ($row = $query->fetch_object()) {
?>
          <input type="hidden" name="id<?php echo $i; ?>" value="<?php echo $row->lid; ?>">
          <tr>
            <td>
			<input name="desc<?php echo $i; ?>" type="text" value="<?php echo trim($row->nome); ?>" size="40" maxlength="80">
			</td>
            <td>
			<input name="url<?php echo $i; ?>" type="text" value="<?php echo trim($row->url); ?>" size="40" maxlength="80">
			</td>
            <td>
<?php
		$sql = "SELECT * FROM tipo_link ORDER BY descricao";
		$queryInt = $db->query($sql);
		echo "<select name=\"tipo$i\" class=\"inputFlat\">\n";
		while ($rowInt = $queryInt->fetch_object()) {
			$varSelected = "";
			if ($row->tipo == $rowInt->id) {
				$varSelected = "selected";
			}
			echo "<option $varSelected value=\"$rowInt->id\">$rowInt->descricao</option>\n";
		}
?>
			</td>
            <td><div align="center"><a href="#" onClick="excluiRegistro('<?php echo $row->lid; ?>','<?php echo trim($row->nome); ?>')"><img src="imagens/ico_exclui.gif" width="18" height="18" border="0"></a></div></td>
          </tr>
<?php
		$i++;
	} // la�o do loop
?>
          <tr>
            <td colspan="10"> <div align="center">
                <input name="Submit" type="submit" class="botaoLogin" value="   Atualizar   ">
              </div></td>
          </tr>
        </table>
        <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
      </form>
<?php
} // fim do if que verifica se o usuário tem acesso a estas informações
?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
