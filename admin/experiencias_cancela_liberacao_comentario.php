<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
$numPerm  = 1;

include("conexao.inc.php");

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

function submitFormLE(acao) {

	formLE.acao.value = 'cancela';
	formLE.submit();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

</script>
<style type="text/css">
<!--
.style1 {
	color: #006666;
	font-weight: bold;
}
-->
</style>
<?php include("menu_admin.php"); ?> 

<table width="729" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="719" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20"  bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42"  bgcolor="#80AAD2"> <p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Coment&aacute;rio  - Cancela Libera&ccedil;&atilde;o/Rejei&ccedil;&atilde;o</strong><b></b></p></td>
          <td width="19"  bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
  </tr>
</table>

<?php
			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
				$sql = "select a.*, date_format(dt_comentario,'%d/%m/%Y') as dt_comentario from comentarios a where ind_situacao <> 'N' and ind_tipo='E' ";
				$query = $db->query($sql);
?>
	  <form name="formLE" method="post" action="xt_experiencia_cancela_liberacao_comentario.php">
	  <input type="hidden" name="acao" value="">
        <table width="711" border="0" cellpadding="5" cellspacing="1" align="center">
           <tr>
            <td height="1" bgcolor="#000000" colspan="9"> <div align="center"></div></td>
          </tr>
		   <tr>
            <td width="62">&nbsp;</td>
            <td width="162">Nome</td>
            <td width="99">E-mail</td>
            <td width="246">Coment&aacute;rio</td>
            <td width="84">Data Inclusão</td>
            <td width="20">Situa��o</td>
          </tr>
		  <tr>
            <td height="1" colspan="9" bgcolor="#000000"> <div align="center"></div></td>
          </tr>

          <?php
		  $i = 0;
		  while ($row = $query->fetch_object()) {
?>
          <tr>
            <td> <div align="center">
                <input type="checkbox" name="cod_comentario<?php echo $i; ?>" value="<?php echo $row->cod_comentario; ?>">
              </div></td>
           <?php   $cor = "#99CC99"; ?>


			<td><b><?php echo $row->txt_nome; ?></b></td>
			<td><b><?php echo $row->e_mail; ?></b></td>
			<td><b><?php echo	$row->txt_comentario; ?></b></td>
			<td><b><?php	echo $row->dt_comentario;  ?></b></td>
            <?php if ($row->ind_situacao == "R") { ?>
			   <td><b>Rejeitado</b></td>
            <?php } else { ?>
               <td><b>Liberado</b></td>
            <?php } ?>
          </tr>
          <?php
						$i++;
					}
?>
          <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td height="1" colspan="9" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="9"> <div align="center">                <input name="Button2" type="button" class="botaoLogin"   value="   Cancelar   " onClick="submitFormLE('cancela');">
            </div></td>
          </tr>
          <tr>
            <td height="1" colspan="9" bgcolor="#000000"><div align="center"></div></td>
          </tr>
        </table>
	  </form>
<?php
  } // fim do if que verifica se o usuário tem acesso a estas informações

?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
