<?php
include("conexao.inc.php");
if (!isset($initFiltro)) {
	setcookie("cookieBuscaAT");
	setcookie("cookieBuscaAG");
	setcookie("cookieBuscaBM");
}
function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) {
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}

?>
<!DOCTYPE html>
<html>
<link rel="alternate stylesheet" type="text/css" media="all" href="calendar-blue.css" title="winter" />
<link rel="alternate stylesheet" type="text/css" media="all" href="calendar-blue2.css" title="blue" />
<link rel="alternate stylesheet" type="text/css" media="all" href="calendar-brown.css" title="summer" />
<link rel="alternate stylesheet" type="text/css" media="all" href="calendar-green.css" title="green" />
<link rel="stylesheet" type="text/css" media="all" href="calendar-win2k-1.css" title="win2k-1" />
<link rel="alternate stylesheet" type="text/css" media="all" href="calendar-win2k-2.css" title="win2k-2" />
<link rel="alternate stylesheet" type="text/css" media="all" href="calendar-win2k-cold-1.css" title="win2k-cold-1" />
<link rel="alternate stylesheet" type="text/css" media="all" href="calendar-win2k-cold-2.css" title="win2k-cold-2" />
<link rel="alternate stylesheet" type="text/css" media="all" href="calendar-system.css" title="system" />
<!-- import the calendar script -->
<script type="text/javascript" src="calendar.js"></script>
<!-- import the language module -->
<script type="text/javascript" src="lang/calendar-br.js"></script>
<!-- other languages might be available in the lang directory; please check
your distribution archive. -->
<!-- helper script that uses the calendar -->
<script type="text/javascript">
var oldLink = null;
// code to change the active stylesheet
function setActiveStyleSheet(link, title) {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
      if(a.getAttribute("title") == title) a.disabled = false;
    }
  }
  if (oldLink) oldLink.style.fontWeight = 'normal';
  oldLink = link;
  link.style.fontWeight = 'bold';
  return false;
}
// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked && (cal.sel.id == "sel1" || cal.sel.id == "sel3"))
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}
// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
//  cal.destroy();
  calendar = null;
}
// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {
  var el = document.getElementById(id);
  if (calendar != null) {
    // we already have some calendar created
    calendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(true, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    calendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  calendar.setDateFormat(format);    // set the specified date format
  calendar.parseDate(el.value);      // try to parse the text in field
  calendar.sel = el;                 // inform it what input field we use
  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  calendar.showAtElement(el.nextSibling, "Br");        // show the calendar
  return false;
}
var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;
// If this handler returns true then the "date" given as
// parameter will be disabled.  In this example we enable
// only days within a range of 10 days from the current
// date.
// You can use the functions date.getFullYear() -- returns the year
// as 4 digit number, date.getMonth() -- returns the month as 0..11,
// and date.getDate() -- returns the date of the month as 1..31, to
// make heavy calculations here.  However, beware that this function
// should be very fast, as it is called for each day in a month when
// the calendar is (re)constructed.
function isDisabled(date) {
  var today = new Date();
  return (Math.abs(date.getTime() - today.getTime()) / DAY) > 10;
}
function flatSelected(cal, date) {
  var el = document.getElementById("preview");
  el.innerHTML = date;
}
function showFlatCalendar() {
  var parent = document.getElementById("display");
  // construct a calendar giving only the "selected" handler.
  var cal = new Calendar(true, null, flatSelected);
  // hide week numbers
  cal.weekNumbers = false;
  // We want some dates to be disabled; see function isDisabled above
  cal.setDisabledHandler(isDisabled);
  cal.setDateFormat("%A, %B %e");
  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  cal.create(parent);
  // ... we can show it here.
  cal.show();
}
</script>
<style type="text/css">
.ex { font-weight: bold; background: #fed; color: #080 }
.help { color: #080; font-style: italic; }
table { font: 13px verdana,tahoma,sans-serif; }
a { color: #00f; }
a:visited { color: #00f; }
a:hover { color: #f00; background: #fefaf0; }
a:active { color: #08f; }
.key { border: 1px solid #000; background: #fff; color: #008;
padding: 0px 5px; cursor: default; font-size: 80%; }
</style>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL + '&dti=' + this.dti.value + '&dtf=' + this.dtf.value,winName,features);
}

function mOvr(src,clrOver) {
  if (!src.contains(event.fromElement)) {
      src.style.cursor = 'hand';
      src.bgColor = clrOver;
  }
}
function mOut(src,clrIn) {
  if (!src.contains(event.toElement)) {
     src.style.cursor = 'default';
     src.bgColor = clrIn;
  }
}
function gotourl(url) {
  window.location.href=url;
}


</script>
<?php include("menu_admin.php"); ?>
<br>
<br>
<table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
   <tr>
     <td width="20"  bgcolor="#80AAD2"> <div align="left"><img src="../imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
     <td width="42"  bgcolor="#80AAD2"> <p><img src="../imagens/ico_experiencias.gif" width="34" height="32"></p></td>
     <td width="474" bgcolor="#80AAD2"> <p><strong>Buscas mais realizadas </strong></p></td>
     <td width="19"  bgcolor="#80AAD2"> <div align="right"><img src="../imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
  </tr>
</table>
<br>
<form name="formCU" method="post" action="rel_mais_procurados_gera.php">
<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="567" valign="top">
        <table width="546" height="107" border="0" align="center" cellpadding="5" cellspacing="1">
          <tr>
            <td width="159"> <div align="right">Per&iacute;odo para Pesquisa de</div></td>
            <td width="148"><input type="text" name="dti" value="<?= isset($dti) ? $dti : ""; ?>" id="dt_inicio" size="15" readonly="yes"><input type="reset" value=" ... " onclick="return showCalendar('dt_inicio', '%d/%m/%Y');">&nbsp;</td>
            <td width="30"> <div align="center">at&eacute;</div></td>
            <td width="164"><input type="text" name="dtf" value="<?= isset($dtf) ? $dtf : ""; ?>" id="dt_fim" size="15" readonly="yes"><input type="reset" value=" ... " onclick="return showCalendar('dt_fim', '%d/%m/%Y');">&nbsp;</td>
          </tr>
        </table>
<br>
    <center><input name="Submit" type="submit" class="botaoLogin" value="   Gerar Arquivo para Download   " >
    </center>
    </form>
</body>
</html>
<?php
$db->close();
?>
