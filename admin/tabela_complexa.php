<?php

if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

switch ($tipo) {
	case 'AG':
		$varNome = "Áreas Geográficas";
		$varTabela = "area_geografica";
		$varCampo1 = "ag_descricao";
		$varCampo2 = "ag_pertence";
		$varID = "ag_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020100') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'AT':
		$varNome = "Áreas Temáticas";
		$varTabela = "area_tematica";
		$varCampo1 = "at_descricao";
		$varCampo2 = "at_pertence";
		$varID = "at_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'EB':
		$varNome = "Entidades Biológicas";
		$varTabela = "entidade_biologica";
		$varCampo1 = "eb_descricao";
		$varCampo2 = "eb_id_externo";
		$varCampo3 = "eb_url";
		$varID = "eb_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'HA':
		$varNome = "Biomas";
		$varTabela = "habitat";
		$varCampo1 = "ha_descricao";
		$varID = "ha_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020400') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'TE':
		$varNome = "Tecnologias";
		$varTabela = "tecnologia";
		$varCampo1 = "tc_descricao";
		$varCampo2 = "tc_pertence";
		$varID = "tc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020600') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'LC':
		$varNome = "Localidades";
		$varTabela = "localidade";
		$varCampo1 = "lc_descricao";
		$varID = "lc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020700') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'ZC':
		$varNome = "Zonas Climáticas";
		$varTabela = "zona_climatica";
		$varCampo1 = "zc_descricao";
		$varID = "zc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='02000') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'VG':
		$varNome = "Tipos de Vegetação";
		$varTabela = "vegetacao";
		$varCampo1 = "vg_descricao";
		$varID = "vg_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020900') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'mapeo_mapa_campos':
		$varNome = "Campos dos formulários";
		$varTabela = $tipo;
		$varCampo1 = "frm";
		$varCampo2 = "ord";
		$varCampo3 = "titulo";
		$varCampo4 = "bloco";
		$varCampo5 = "sub-bloco";
		$varCampo6 = "ajuda";
		$varCampo7 = "repeticoes";
		$varCampo8 = "padrao";
		$varID = "campo";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
} // fim do switch
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaForm() {
	desc = form1.descricao.value;
	if (desc.length == 0) {
		alert('Digite um nome válido!');
		form1.descricao.focus();
		return;
	}
	form1.submit();
}

function excluiRegistro(id,desc) {
	if(confirm('Você tem certeza que deseja excluir o registro '+desc+'?')) {
		window.location.href='xt_deleta_tabela_complexa.php?tipoTabela=<?php echo $tipo; ?>&id='+id;
	}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function muda_destino(tipo) {
 document.form2.action = "tabela_complexa.php?tipo=" + tipo;
}
</script>
<style type="text/css">
<!--
.style1 {
	font-size: 9px;
	font-weight: bold;
}
.TAM8 {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 9px;
	font-style: normal;
	font-weight: bolder;
	text-transform: uppercase;
}
-->
</style>

<?php include("menu_admin.php"); ?>
<table width="652" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="642" valign="top" align="center">
	<table width="571" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="23" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="48" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_complexa.gif" width="34" height="32"></p></td>
          <td width="452" bgcolor="#80AAD2"> <p><b>
              <?php
			echo $varNome;
?>
              </b></p></td>
          <td width="48" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
?>
      <form name="form1" method="post" action="xt_insere_tabela_complexa.php">
        <input type="hidden" name="tipoTabela" value="<?php echo $tipo; ?>">
		<?php
		if (isset($erro) && $erro) {
	       echo "<p align='center' class='ver' >Atenção: $erro</p>";
        }
?>
        <table width="528" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2"><p>Adicionar novo registro
                em "<strong><?php echo $varNome; ?></strong>"</p></td>
          </tr>
          <tr>
            <td height="1" colspan="10" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="79"><div align="right">Descri&ccedil;&atilde;o:</div></td>
            <td width="426"> <input name="descricao" type="text" size="40" maxlength="80"></td>
          </tr>
<?php
	if ($tipo == 'AG' || $tipo == 'AT' || $tipo == 'TE') {

		$sql = "SELECT * FROM $varTabela ORDER BY $varCampo1";
		$query = $db->query($sql);
?>
          <tr>
            <td><div align="right">Pertence:</div></td>
            <td>
			  <select name="pertence<?php echo $i; ?>">
			  	<option value="">-- Selecione --</option>
<?php
		while ($row = $query->fetch_object()) {
?>
			    <option value="<?php echo $row->$varID; ?>"><?php echo trim($row->$varCampo1); ?></option>
<?php
		}
?>
              </select> </td>
          </tr>

<?php
	}
?>
          <tr>
            <td>&nbsp;</td>
            <td><input name="enviar" type="button" class="botaoLogin" value="Adicionar" onClick="criticaForm();"></td>
          </tr>
        </table>
      </form>
      <form name="form2" method="post" action="xt_atualiza_tabela_complexa.php">
        <p>
          <input type="hidden" name="tipoTabela" value="<?php echo $tipo; ?>">
</p>
        <div align="center">          </div>
        <table width="547" border="0" cellpadding="1" cellspacing="0">
          <tr>
            <td colspan="3">
			<p>Registros em "<strong><?php echo $varNome; ?></strong>"
</p>
			<p align="center">
			  <span class="style1">
              <input name="letra" type="submit" class="TAM8" value="A" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="B" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="C" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="D" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="E" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="F" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="G" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="H" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="I" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="J" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="K" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="L" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="M" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
			  <input type="submit" value="N" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="O" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="P" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="Q" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="R" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="S" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="T" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="U" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="V" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="W" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="X" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="Y" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
              <input type="submit" value="Z" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');">
			  </span></p>
			<p align="center"><input type="submit" value="Todos" name="letra" class="TAM8" onclick="muda_destino('<?php echo $tipo; ?>');"></p>			<p align="center">&nbsp;            </p></td>
          </tr><tr><td width="555" height="3"></td></tr>
	    </table>
		  <table width="570" border="0" cellpadding="2" cellspacing="1">
          <tr>
            <td height="1" colspan="10" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="29"> <div align="center"><strong>ID</strong></div></td>
            <td width="276"><strong>Descri&ccedil;&atilde;o</strong></td>
<?php
		if ($tipo == 'AG' || $tipo == 'AT' || $tipo == 'TE') {
?>
            <td width="199"><strong>Pertence</strong></td>
<?php
		}
?>
            <td width="41"> <div align="center"><strong>Excluir</strong></div></td>
          </tr>
<?php
       if (!isset($letra) || !$letra)
		{
		$letra = ".A";
	} else if ($letra == "Todos")
		{
		$letra = "";
		}



		$sql = "SELECT * FROM $varTabela where $varCampo1 like '$letra%' ORDER BY $varCampo1";

		$query = $db->query($sql);

		if (!$query) {
    		die($db->error);
		}
		$i = 0;
		while ($row = $query->fetch_object()) {
?>
          <input type="hidden" name="id<?php echo $i; ?>" value="<?php echo $row->$varID; ?>">
          <tr>
            <td align="right"><?php echo $row->$varID; ?></td>
            <td>
<input name="desc<?php echo $i; ?>" type="text" value="<?php echo trim($row->$varCampo1); ?>" size="40" maxlength="80">
<?php if ($tipo == 'AT') { ?>
			<!--
			<br>
			[ <a href="rel_tecnologia_tema.php?area=<?php echo $row->$varID; ?>" class="Preto">AT/TC</a> ]
			[ <a href="rel_area_inst.php?area=<?php echo $row->$varID; ?>" class="Preto">AT/IN</a> ]
			-->
			<?php } ?>
			</td>
<?php
		if ($tipo == 'AG' || $tipo == 'AT' || $tipo == 'TE') {
			$numPertence = 0;
			if (strlen(trim($row->$varCampo2)) != 0) {
				$tempPertence = $row->$varCampo2;
				$sql = "SELECT $varID,$varCampo1 FROM $varTabela WHERE $varID=$tempPertence";
				$queryPertence = $db->query($sql);
				$numPertence = $queryPertence->num_rows;
				$rowPertence = $queryPertence->fetch_object();
			}
?>
            <td>
<?php
		if (isset($numPertence) && $numPertence > 0) {
			$tempID = $row->$varID;
			$tempIDPertence = $row->$varCampo2;
			$tempDesc = $row->$varCampo1;
			$tempOrigem = $rowPertence->$varCampo1;
			echo "<a href=\"#\" class=\"Preto\" onClick=\"MM_openBrWindow('popup_pertence.php?tipo=$tipo&id=$tempID&idPertence=$tempIDPertence&desc=$tempDesc','pertence','scrollbars=no,width=398,height=200')\">$tempOrigem</a>";
		}
		else {
			$tempID = $row->$varID;
			$tempDesc = $row->$varCampo1;
			echo "<a href=\"#\" class=\"Preto\" onClick=\"MM_openBrWindow('popup_pertence.php?tipo=$tipo&id=$tempID&idPertence=&desc=$tempDesc','pertence','scrollbars=no,width=398,height=200')\">---------</a>";
		}
 ?>
			</a></td>
			<input type="hidden" name="pertence<?php echo $i; ?>" value="<?php echo isset($rowPertence->$varCampo2) ? $rowPertence->$varCampo2 : ''; ?>">
<?php
		}
?>
            <td><div align="center"><a href="#" onClick="excluiRegistro('<?php echo $row->$varID; ?>','<?php echo trim($row->$varCampo1); ?>')"><img src="imagens/ico_exclui.gif" width="18" height="18" border="0"></a></div></td>
          </tr>
          <?php
		$i++;
	} // la�o do loop
?>
          <tr>
            <td colspan="10"> <div align="center">
                <input name="Submit" type="submit" class="botaoLogin" value="   Atualizar   ">
              </div></td>
          </tr>
        </table>
        <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
      </form>
      <?php
} // fim do if que verifica se o usuário tem acesso a estas informações
?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
