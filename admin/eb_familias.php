<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaForm() {
	desc = form1.descricao.value;
	if (desc.length == 0) {
		alert('Digite um nome válido!');
		form1.descricao.focus();
		return;
	}
	form1.submit();
}

function excluiRegistro(id,desc) {
	if(confirm('Você tem certeza que deseja excluir o registro '+desc+'?')) {
		window.location.href='xt_deleta_familia.php?&id='+id;
	}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  descricao = form1.descricao.value;
  idExterno = form1.idExterno.value;
  infra = form1.infra.value;
  nomes = form1.nomes.value;
  destino = selObj.options[selObj.selectedIndex].value + '&descricao=' + descricao + '&idExterno=' + idExterno + '&infra=' + infra + '&nomes=' + nomes;
  eval(targ+".location='"+destino+"'");
  if (restore) selObj.selectedIndex=0;
}
</script>
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57"> 
      <div align="center"><a class="Branco" href="xt_logout.php"><img src="imagens/ico_logoff.gif" border="0"><br><br>
        <strong>LOGOUT</strong></a></div>
	</td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_complexa.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><b>Animais e Plantas - Fam&iacute;lias</b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br> 
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
?>
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td colspan="2"><p><strong>Op��es</strong></p></td>
          </tr>
          <tr> 
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            
          <td width="283"><img src="imagens/seta02.gif" width="12" height="13"> 
            <a href="eb_familias.php" class="Preto">Cadastro de Fam&iacute;lias</a><br> 
              <br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="eb_generos.php" class="Preto">Cadastro 
            de G&ecirc;neros</a><br>
          </td>
          <td width="283" valign="top"><img src="imagens/seta02.gif" width="12" height="13"> 
            <a href="eb_especies.php" class="Preto">Cadastro de Esp&eacute;cies</a><br>
            <br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="eb_cadastro.php" class="Preto">Cadastro 
            de Animais e Plantas</a> </td>
          </tr>
        </table>
      <form name="form1" method="post" action="xt_insere_familia.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td colspan="2"><p>Adicionar novo registro 
                em "<strong>Fam�lias</strong>"</p></td>
          </tr>
          <tr> 
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td width="81"><div align="right">Descri&ccedil;&atilde;o:</div></td>
            <td width="483"> <input name="descricao" type="text" size="40" maxlength="80"> 
              <input name="enviar" type="button" class="botaoLogin" value="Gravar" onClick="criticaForm();">	
            </td>
          </tr>
        </table>
      </form>
      <br>
	  <?php if (isset($novaFamilia)) { ?>
	  <p align="center">Fam�lia <b><?php echo $novaFamilia; ?></b> cadastrada com sucesso!</p>
	  <?php } ?>
	  
	   <form name="form2" method="post" action="xt_atualiza_familia.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td colspan="3"><p>Registros em "<strong>Fam�lias</strong>"</p></td>
          </tr>
          <tr> 
            <td height="1" colspan="3" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td width="35"> <div align="center"><strong>ID</strong></div></td>
            <td width="460"><strong>Descri&ccedil;&atilde;o</strong></td>
            <td width="38"> <div align="center"><strong>Excluir</strong></div></td>
          </tr>
          <?php
	$sql = "SELECT * FROM eb_familia ORDER BY ebf_familia";
	$query = $db->query($sql);
	if (!$query) {
    	die($db->error);
	}
	$i = 0;
	while ($row = $query->fetch_object()) {
?>
          <input type="hidden" name="id<?php echo $i; ?>" value="<?php echo $row->ebf_id; ?>">
          <tr> 
            <td align="right"><?php echo $row->ebf_id; ?></td>
            <td><input name="desc<?php echo $i; ?>" type="text" value="<?php echo trim($row->ebf_familia); ?>" size="60" maxlength="80"></td>
            <td><div align="center"><a href="#" onClick="excluiRegistro('<?php echo $row->ebf_id; ?>','<?php echo trim($row->ebf_familia); ?>')"><img src="imagens/ico_exclui.gif" width="18" height="18" border="0"></a></div></td>
          </tr>
          <?php
		$i++;
	} // la�o do loop
?>
          <tr> 
            <td colspan="3"> <div align="center"> 
                <input name="Submit" type="submit" class="botaoLogin" value="   ATUALIZAR   ">
              </div></td>
          </tr>
        </table>
        <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
      </form>
      <?php
} // fim do if que verifica se o usuário tem acesso a estas informações
?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>