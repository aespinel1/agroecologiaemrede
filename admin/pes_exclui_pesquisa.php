<?php
include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040600') AND us_admin='1' ";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style2 {color: #FFFFFF}
-->
</style>
<script language="JavaScript">
function excluiPesquisa(pesquisa) {
	if(confirm('Você tem certeza que deseja excluir a pesquisa ?')) {
		window.location.href='xt_pes_exclui_pesquisa.php?pesquisa='+pesquisa;
	}
}
function seleciona_outra() {
  	window.location.href='pesquisas_excluir.php';
}
</script>
<?php include("menu_admin.php"); ?>
<br>
<br>
<table width="616" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="606" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Detalhes
              da Pesquisa a ser exclu&iacute;da </strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
	  <br>
	  <br>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
	$sql = "SELECT * FROM pesquisa WHERE pe_id=$pesquisa";
	$query = $db->query($sql);
	$row = $query->fetch_object();

	$sql = "SELECT * FROM pesquisa_autor,usuario WHERE pea_id_usuario=us_login AND pea_id_pesquisa=$pesquisa";
	$queryAutor = $db->query($sql);
	$numAutor = $queryAutor->num_rows;


	$sql = "SELECT * FROM pesquisa_relator,usuario WHERE per_id_usuario=us_login AND per_id_pesquisa=$pesquisa";
	$queryRelator = $db->query($sql);
	$numRelator = $queryRelator->num_rows;

	$sql = "SELECT * FROM pesquisa_autor,frm_instituicao WHERE pea_id_inst=in_id AND pea_id_pesquisa=$pesquisa";
	$queryInstAutor = $db->query($sql);
	$numInstAutor = $queryInstAutor->num_rows;


	$sql = "SELECT * FROM pesquisa_relator,frm_instituicao WHERE per_id_inst=in_id AND per_id_pesquisa=$pesquisa";
	$queryInstRelator = $db->query($sql);
	$numInstRelator = $queryInstRelator->num_rows;
?>
      <table width="610" border="0">

        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td colspan="2">
<?php

		echo "Pesquisa : <strong>$row->pe_descricao</strong><br>";
		echo "Chamada : <strong>$row->pe_chamada</strong><br>";
?>
		  </td>
        </tr>

		<tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
		<tr>
          <td height="4" colspan="2"> <div align="center">&nbsp;</div></td>
        </tr>
        <tr>
          <td width="342" valign="top"> <?php echo nl2br($row->pe_resumo); ?> <span class="style2"></span>          </td>
          <td width="244" valign="top">
<?php
   if ($numAutor > 0 or $numAutor > 0) {
?>
		  <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="21"><img src="../imagens/ico_usuarios_transp_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Autor(es):</strong></td>
              </tr>
              <tr>
                <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr bgcolor="#B3CBCC">
              <td colspan="2">
<?php
		if ($numAutor > 0) {
			echo "<br>";
			while ($rowAutor = $queryAutor->fetch_object()) {
				echo "<img src=\"imagens/seta02.gif\">$rowAutor->us_nome<br>";
			}
		}

		if ($numInstAutor > 0) {
			echo "<br>";
			while ($rowInstAutor = $queryInstAutor->fetch_object()) {
				echo "<img src=\"../imagens/seta02.gif\">$rowInstAutor->in_nome<br>";
			}
		}
?>
                </td>
              </tr>
            </table>

 <?php
}

		if ($numRelator > 0 or $numInstRelator > 0) {
?>
            <br> <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="21"><img src="../imagens/ico_bibliografia_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Relator(es)</strong></td>
              </tr>
              <tr>
                <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr bgcolor="#B3CBCC">
                <td colspan="2">
                  <?php

		if ($numRelator > 0) {
			echo "<br>";
			while ($rowRelator = $queryRelator->fetch_object()) {
				echo "<img src=\"../imagens/seta02.gif\">$rowRelator->us_nome<br>";
			}
		}


		if ($numInstRelator > 0) {
			echo "<br>";
			while ($rowInstRelator = $queryInstRelator->fetch_object()) {
				echo "<img src=\"../imagens/seta02.gif\">$rowInstRelator->in_nome<br>";
			}
		}

?>
                </td>
              </tr>
            </table>

<?php
}

$sql   = "SELECT * FROM pesquisa_arquivo WHERE pa_id_pesquisa=$pesquisa";
$query = $db->query($sql);
$num   = $query->num_rows;

if ($num > 0) {
?>
            <br> <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="17"><img src="../imagens/ico_ficha.gif" width="14" height="13"></td>
                <td width="176"><strong>Anexos</strong></td>
              </tr>
              <tr>
                <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td colspan="2" bgcolor="#DADADA">
                  <?php

}

	while ($arrayArquivos = $query->fetch_object()) {
		$nomeDoc = substr(trim($arrayArquivos->pa_arquivo),0,-20);
		$fileSizeDoc = my_filesize("../upload/arquivos/".$arrayArquivos->pa_arquivo);
		echo "&nbsp;<img src=\"../imagens/seta02.gif\"> <a href=\"../upload/arquivos/$arrayArquivos->pa_arquivo\" class=\"preto\">$nomeDoc ($fileSizeDoc)</a><br>";
	}


?>
                </td>
              </tr>
            </table>
<?php } ?>
        </tr>
      </table>
	  </td>
  </tr>
   <tr>
            <td colspan="3"><div align="center">                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input name="button" type="button" class="botaoLogin" onClick="excluiPesquisa('<?php echo $pesquisa; ?>');" value="       Excluir     ">&nbsp;&nbsp;
		        <input name="button" type="button" class="botaoLogin" onClick="seleciona_outra();" value="   Selecionar Outra">

              </div></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
}
$db->close();
?>
