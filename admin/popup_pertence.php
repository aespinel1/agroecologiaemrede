<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

switch ($tipo) {
	case 'AG':
		$varNome = "Áreas Geográficas";
		$varTabela = "area_geografica";
		$varCampo1 = "ag_descricao";
		$varCampo2 = "ag_pertence";
		$varID = "ag_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020100') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'AT':
		$varNome = "Áreas Temáticas";
		$varTabela = "area_tematica";
		$varCampo1 = "at_descricao";
		$varCampo2 = "at_pertence";
		$varID = "at_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'EB':
		$varNome = "Entidades Biológicas";
		$varTabela = "entidade_biologica";
		$varCampo1 = "eb_descricao";
		$varCampo2 = "eb_id_externo";
		$varCampo3 = "eb_url";
		$varID = "eb_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'HA':
		$varNome = "Habitats";
		$varTabela = "habitat";
		$varCampo1 = "ha_descricao";
		$varID = "ha_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020400') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'TE':
		$varNome = "Tecnologias";
		$varTabela = "tecnologia";
		$varCampo1 = "tc_descricao";
		$varCampo2 = "tc_pertence";
		$varID = "tc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020600') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
} // fim do switch

if (strlen($idPertence) > 0) {
	$tempPertence = $idPertence;
	$sql = "SELECT $varID,$varCampo1 FROM $varTabela WHERE $varID=$tempPertence";
	$queryPertence = $db->query($sql);
	$numPertence = $queryPertence->num_rows;
	$rowPertence = $queryPertence->fetch_object();
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Pertence</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="400" border="0" cellpadding="5">
  <tr> 
    <td bgcolor="#666666" class="textoBranco"><?php echo $varNome; ?></td>
  </tr>
  <tr> 
    <td>
	
	
	
	
	
      <form name="form1" method="post" action="xt_atualiza_tabela_complexa_pertence.php">
        <input type="hidden" name="tipoTabela" value="<?php echo $tipo; ?>">
		<input type="hidden" name="id" value="<?php echo $id; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td colspan="2"><p>Adicionar novo registro 
                em "<strong><?php echo $varNome; ?></strong>"</p></td>
          </tr>
          <tr> 
            <td height="1" colspan="10" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td width="81"><div align="right">Descri&ccedil;&atilde;o:</div></td>
            <td width="483"><?php echo $desc; ?></td>
          </tr>
<?php 
	if ($tipo == 'AG' || $tipo == 'AT' || $tipo == 'TE') { 
	
		$sql = "SELECT * FROM $varTabela ORDER BY $varCampo1";
		$query = $db->query($sql);		  
?>
          <tr> 
            <td><div align="right">Pertence:</div></td>
            <td>
			  <select name="pertence">
			  	<option value="">-- Selecione --</option>
<?php
		while ($row = $query->fetch_object()) {
			$varSelected = "";
			if ($row->$varID == $idPertence) {
				$varSelected = "selected";
			}
?>
			    <option <?php echo $varSelected; ?> value="<?php echo $row->$varID; ?>"><?php echo trim($row->$varCampo1); ?></option>
<?php
		}
?>
              </select> </td>
          </tr>

<?php
	}
?>
          <tr> 
            <td>&nbsp;</td>
            <td><input name="enviar" type="submit" class="botaoLogin" value="Gravar" onClick="criticaForm();"></td>
          </tr>
        </table>
      </form>
	
	
	
	
	
	</td>
  </tr>
</table>
</body>
</html>
