<?php
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">

<?php
include("menu_admin.php");
?>
<br>
<br>

<table width="580" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="570" valign="top"> <table width="558" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="477" bgcolor="#80AAD2"> <p><strong>Lista de Endere&ccedil;os: Pessoas </strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
	  <br>
	  <br>
<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

$sql = "select us_nome,us_email,txt_endereco,txt_cidade,txt_estado,txt_pais,txt_cep,txt_telefone from usuario  order by us_nome";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}

$arquivo = "rel_enderecos_pessoas";

$arquivotemp = tempnam($dir['upload'], "relacao_pessoas").'.csv';

$abrir=fopen($arquivotemp,"w");

$conteudo = "'Nome';'E-mail';'Endereço';'Cidade';'Estado';'Pais';'CEP';'Telefone'";
fwrite($abrir,$conteudo."\n");
while ($rowcriterio = $query->fetch_object()) {
                $conteudo =  "'$rowcriterio->us_nome';'$rowcriterio->us_email';'$rowcriterio->txt_endereco';'$rowcriterio->txt_cidade';'$rowcriterio->txt_estado';'$rowcriterio->txt_pais';'$rowcriterio->txt_cep';'$rowcriterio->txt_telefone'";
                fwrite($abrir,$conteudo."\n");
			}

fclose($abrir);
chmod($arquivotemp, 0777);

$db->close();
echo "<br><br><br>";
echo "<center><a href='".$dir['upload_URL'].basename($arquivotemp)."'>Clique com o botão direito do mouse nesse link, selecione 'salvar destino como', confirme o nome do arquivo, assim o arquivo será baixado em seu computador.</a></center>";
echo "<br><br><br><center><a href='main.php'>Retornar à página principal</a></center>";
?>
