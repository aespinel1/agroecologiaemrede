<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

if (isset($tipo)) {
	switch ($tipo) {
		case "NU":
			$varDesc = "Cadastro";
			$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='030000' OR fu_id='030100') AND us_admin='1' ";
			$query = $db->query($sql);
			$numPerm = $query->num_rows;
			break;
		case "CF":
			$varDesc = "Permiss�es Acesso";
			$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='030000' OR fu_id='030200') AND us_admin='1' ";
			$query = $db->query($sql);
			$numPerm = $query->num_rows;
			break;
		case "LU":
			$varDesc = "Libera��o";
			$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='030000' OR fu_id='030500') AND us_admin='1' ";
			$query = $db->query($sql);
			$numPerm = $query->num_rows;
			break;
	}
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

function criticaFormFuncao() {
	cod = form1.cod.value;
	des = form1.desc.value;

	if (cod.length == 0) {
		alert('Digite um c�digo v�lido!');
		form1.cod.focus();
		return;
	}

	if (des.length == 0) {
		alert('Digite uma descrição v�lida!');
		form1.desc.focus();
		return;
	}

	form1.submit();
}

var ordem = new Array();
var i=1;
function inserir(func,descricao){
	var temp1 = func.substring(0,2);
	var temp2 = func.substring(2,6);
	if(temp2 == '0000'){
		for(h=1 ; h<=i-1 ; h++){
			if(ordem[h] != 0){
				var a = ordem[h].substring(0,2);
				var b = ordem[h].substring(2,6);
				if((a == temp1) && (b != '0000')){
					ordem[h] = 0;
				}
			}
		}
		desabilitar(temp1,descricao);
	}
	var p=1;
	var flag = true;
	while(ordem[p] != null){
		if(func != ordem[p]){
			p++;
		}
		else{
			ordem[p]=0;
			flag = false;
			break;
		}
	}
	if(flag){
		ordem[i] = func;
		i++;
	}
}
function mostrar(){
	for(j=1 ; j<=i-1 ; j++){
		if(ordem[j] != 0){
		    //alert('seq.valor= '+ordem[j]);
   			document.form2.sequencia.value = document.form2.sequencia.value + ordem[j] + " ";
		}
	}
	if(document.form2.sequencia.value == ''){
		if(!confirm('Nenhuma permiss�o foi selecionada! Deseja dar permiss�es a este Experimentador?')){
			window.location.href = 'usuarios.php?tipo=DP';
		}
	}
	else{
		var seq = document.form2.sequencia.value;
		if(confirm('Deseja salvar as novas permiss�es deste Experimentador?')){
			window.location.href = 'xt_usuario_atualiza_permissao.php?sequencia=' + seq + '&usuario=<?php echo $usuario; ?>';
		}
		else{
			document.form2.sequencia.value = '';
			//alert('seq= '+document.form1.sequencia.value);
		}
	}
}
function desabilitar(funcao,descricao){
 	var marcado = true;
	var str = 'window.document.form2.'+descricao;
	var tempobj = eval(str);
	if(tempobj.checked == 1) {
		marcado = false;
	}
	var cont = document.form2.elements.length;
	for(k=0; k<cont; k++){
		var cod1 = document.form2.elements[k].value;
		var cod2 = cod1.substring(0,2);
		var cod3 = cod1.substring(2,6);
		if(!marcado){
			if(cod2 == funcao){
				document.form2.elements[k].checked = 1;
				if(cod3 != '0000'){
					document.form2.elements[k].disabled = 1;
				}
			}
		}
		else{
			if(cod2 == funcao){
				document.form2.elements[k].checked = 0;
				if(cod3 != '0000'){
					document.form2.elements[k].disabled = 0;
				}
			}
		}
   	}
}

function criticaFormUsuario() {

	nome = formNE.nome.value;
	login = formNE.login.value;
	email = formNE.email.value;
	senha = formNE.senha.value;
	senhaConf = formNE.senhaConf.value;
	grupo = formNE.grupo.value;
	ag = formNE.agID.value;
	at = formNE.atID.value;

	if (nome.length == 0) {
		alert('Preencha um nome v�lido para o Experimentador/Usu�rio!');
		formNE.nome.focus();
		return;
	}

	if (login.length == 0 && grupo != "4") {
		alert('Preencha um login v�lido para o Usu�rio!');
		formNE.login.focus();
		return;
	}

	if (senha.length == 0 && grupo != "4") {
		alert('Preencha uma senha v�lida para o Usu�rio!');
		formNE.senha.focus();
		return;
	}

	if (senha != senhaConf) {
		alert('A senha n�o confere! Por favor, digite e confirme a senha do Experimentador!');
		formNE.senha.value = '';
		formNE.senhaConf.value = '';
		formNE.senha.focus();
		return;
	}

	if (email.length == 0 && grupo != "4") {
		alert('Preencha um e-mail v�lido para o Usu�rio!');
		formNE.email.focus();
		return;
	}

	if (ag.length == 0) {
		alert('Selecione pelo menos uma �rea Geogr�fica do seu interesse!');
		formNE.areasGeograficas.focus();
		return;
	}

	if (at.length == 0) {
		alert('Selecione pelo menos uma �rea Tem�tica do seu interesse!');
		formNE.areasTematicas.focus();
		return;
	}

	formNE.submit();
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function marca_desmarca_tabela () {
if (document.formNE.admin.checked == true) {
   var el = document.all['dados_especiais']
   el.style.display = "";
   }
else
   {
    var el = document.all['dados_especiais'];
    el.style.display = "none";
   }
}
</script>
<style type="text/css">
<!--
.style3 {color: #000000}
.style5 {font-family: Verdana, Arial, Helvetica, sans-serif}
.style9 {color: #FFFFFF}
-->
</style>

<?php include("menu_admin.php"); ?>

<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>

    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_usuarios.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><b>Usuários / Contatos / Experimentadores - <?php echo $varDesc ?></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
<?php
	if (isset($tipo)) {

		if ($tipo == "NU") {

			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
?>
			<form name="formNE" method="post" action="xt_usuario_insere_usuario.php">

        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="5"> <p> <strong><?php echo $varDesc; ?></strong>
                <br>
                </td>
          </tr>
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td><div align="right">Nome:</div></td>
            <td colspan="4"><input name="nome" type="text" id="nome" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td ><div align="right">E-mail:</div></td>
            <td colspan="4"><input name="email" type="text" id="email3" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">Website:</div></td>
            <td colspan="4"><input name="website" type="text" id="website" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">Telefones:</div></td>
            <td colspan="4">              <span>
              <input name="fone" type="text" id="fone3" size="89" maxlength="255">
            </span></td>
          </tr>
          <tr>
            <td> <div align="right">Endere&ccedil;o:</div></td>
            <td colspan="4"><input name="endereco" type="text" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">Cidade:</div></td>
            <td width="180"> <input name="cidade" type="text" maxlength="255">
            </td>
            <td > <div align="right">Estado:</div></td>
            <td width="209" colspan="2"><input name="estado" type="text" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">CEP/Zip:</div></td>
            <td><input name="cep" type="text" maxlength="10"></td>
            <td> <div align="right">Pa&iacute;s:</div></td>
            <td colspan="2"><input name="pais" type="text" maxlength="80"></td>
          </tr>
		  </table>

		 <table width="567" border="0" cellpadding="5" cellspacing="1"  >


            <td width="155" bgcolor="#CCCCCC"><div align="center">
                <input name="admin" type="checkbox" id="admin" onClick="marca_desmarca_tabela();" value="1">
            Administrador</div></td>
          </tr>
		 </table>
          <table width="567" border="0" cellpadding="5" cellspacing="1"  id="dados_especiais" style="display:none" >
          <tr bgcolor="#CC3333">
            <td><div align="right" class="style9">Login:</div></td>
            <td><input name="login" type="text" class="style5" id="login2" maxlength="80"></td>
            <td bgcolor="#CC3330" class="textoPreto10px style3">&nbsp;</td>
            <td colspan="2">&nbsp;</td>
          </tr>
          <tr bgcolor="#CC3333">
            <td> <div align="right" class="style9">Senha:</div></td>
            <td><input name="senha" type="password" class="style5" id="login3" maxlength="80"></td>
            <td bgcolor="#CC3332"> <div align="right" class="style9">Confirmar senha:</div></td>
            <td colspan="2"><input name="senhaConf" type="password" class="style5" id="login4" maxlength="80">
            </td>
          </tr>
          	 </table>
          <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr bgcolor="#000000">
            <td height="1" colspan="5" valign="top"><div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5" valign="top"><strong>Institu&ccedil;&otilde;es</strong>:
              Se o usuário � membro, colaborador de uma ou mais institui��es, indique-as abaixo, segurando a tecla CTRL do seu teclado.</td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="5" valign="top"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5" valign="top"> <div align="right">
              </div>
              <select name="inst[]" size="6" multiple>
                <?php
				$sql = "SELECT * FROM frm_instituicao WHERE in_liberacao='S' ORDER BY in_nome";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->in_nome);
					echo "<option value=\"$row->in_id\">$desc</option>";
				}
?>
              </select> </td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="5" valign="top"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5" valign="top"><strong>&Aacute;reas
              Geogr&aacute;ficas</strong></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="5" valign="top"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5" valign="top"> <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_ag_nav.php?url=experiencias.php','at','scrollbars=yes,width=517,height=300,status=yes')">Clique
              aqui para adicionar<strong> &Aacute;reas Geogr&aacute;ficas</strong></a><br>
              <textarea name="areasGeograficas" cols="50" rows="5" wrap="VIRTUAL" readonly></textarea>
              <input type="hidden" name="agID"> </td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="5" valign="top"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5" valign="top"><strong>&Aacute;reas
              Tem&aacute;ticas</strong></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="5" valign="top"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5" valign="top"> <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_at_nav.php?url=experiencias.php','at','scrollbars=yes,width=517,height=300,status=yes')">Clique
              aqui para adicionar<strong> &Aacute;reas Tem&aacute;ticas</strong></a><br>
              <textarea name="areasTematicas" cols="50" rows="5" wrap="VIRTUAL" readonly></textarea>
              <input type="hidden" name="atID"> </td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="5" valign="top"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5"><div align="center">
                <input type="button" class="botaoLogin" value="   Incluir   " onClick="criticaFormUsuario();">
              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="5"> <div align="center"></div></td>
          </tr>
        </table>

	  </form>
	  <?php if (isset($loginNovo)) { ?>
	  <p align="center">Experimentador <b><?php echo $loginNovo; ?></b> cadastrado com sucesso!</p>
	  <?php } ?>
<?php
			} // fim do if que verifica se o Experimentador tem acesso a esta fun��o
		}
		else if ($tipo == "CF") {

			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
?>
	  <form name="form1" method="post" action="xt_usuario_insere_funcao.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="4">
			<p>
			<strong><?php echo $varDesc; ?></strong>&nbsp;&nbsp;
			[ <a href="usuarios_cadastro_funcoes.php" class="Preto">Administrar fun��es</a> ]
			</p>
			</td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="72"> <div align="right">C&oacute;digo:</div></td>
            <td width="120"><input name="cod" type="text" id="cod" maxlength="80"></td>
            <td width="103"> <div align="right">Descri&ccedil;&atilde;o:</div></td>
            <td width="227"><input name="desc" type="text" id="desc" size="40" maxlength="255"></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input type="button" class="botaoLogin" value="   Incluir   " onClick="criticaFormFuncao();">
              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
	  </form>
	  <?php if (isset($desc)) { ?>
		  <p align="center">Fun��o <b><?php echo $desc; ?></b> de c�digo <b><?php echo $cod; ?></b> cadastrada com sucesso!</p>
	  <?php } ?>
<?php
			} // fim do if que verifica se o Experimentador tem acesso a estas informações

		}
		else if ($tipo == "DP") {

			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
?>
		<form name="form1" method="post" action="usuarios.php?tipo=DP">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2"> <p> <strong><?php echo $varDesc; ?></strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="60"> <div align="center">Experimentador:</div></td>
            <td width="484"> <select name="usuario">
<?php
				$sql = "SELECT * FROM usuario ORDER BY us_login";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$varSelected = "";
					if ($usuario == trim($row->us_login)) {
						$varSelected = "selected";
					}
?>
                <option <?php echo $varSelected; ?> value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_login); ?></option>
                <?php
				} // la�o do loop
?>
              </select>
              <input name="Submit" type="submit" class="botaoLogin" value="   Selecionar   ">
            </td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="2"> <div align="center"></div></td>
          </tr>
        </table>
		</form>
<?php
				if (isset($usuario)) {
?>
		<form name="form2" method="post" action="xt_usuario_atualiza_permissao.php">
		<input type="hidden" name="usuario" value="<?php echo $usuario; ?>">
 	    <input type="hidden" name="sequencia">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td height="1" colspan="3" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="20">
              <div align="center"><strong>-</strong></div></td>
            <td width="74">
              <div align="center"><strong>C&oacute;digo</strong></div></td>
            <td width="439"><strong>Descri&ccedil;&atilde;o</strong></td>
          </tr>
<?php
					$sql = "SELECT* FROM funcao ORDER BY fc_id";
					$query = $db->query($sql);
					$i = 0;
					$funcoesPai = '';
					$funcoesFilha = '';
					$funcPai = 0;
					while ($row = $query->fetch_object()) {

						if (substr($row->fc_id,-4) == '0000') {
							$varColor = "#B2B2B2";
							$funcPai++;
						}
						else {
							$varColor = "#D8D8D8";
						}

						$d = str_replace(" ","",$row->fc_descricao);
?>
          <tr bgcolor="<?php echo $varColor; ?>">
		  	<td align="center"><input name="<?php echo trim($d); ?>" type="checkbox" value="<?php echo $row->fc_id; ?>" onClick="inserir('<?php echo trim($row->fc_id); ?>','<?php echo trim($d); ?>');"></td>
            <td><div align="center">
				<?php echo $row->fc_id; ?>
              </div></td>
            <td>
              <?php echo trim($row->fc_descricao); ?>
            </td>
          </tr>
<?php
						$i++;
					} // la�o do loop
?>
			<input type="hidden" name="funcoes" value="">
			<input type="hidden" name="funcpai" value="<?php echo $funcPai; ?>">
			<input type="hidden" name="funcoespai" value="<?php echo trim($funcoesPai); ?>">
          <tr>
            <td colspan="3"> <div align="center">
                <input name="Button" onclick="mostrar();" type="button" class="botaoLogin" value="   Atualizar   ">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="3" bgcolor="#000000">
              <div align="center"></div></td>
          </tr>
        </table>
		</form>
<?php
				} // fim do if que verifica se o form de busca por Experimentador permiss�es foi postado

			} // fim do if que verifica se o Experimentador tem acesso a estas informações

		}
		else if ($tipo == "LU") {

			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {

				$sql = "SELECT * FROM usuario WHERE us_liberacao='N'";
				$query = $db->query($sql);
?>
	  <form name="formLULibera" method="post" action="xt_usu_libera_usuario.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
          <td height="24" colspan="4">
            <p> <strong>Liberar Experimentadores</strong>
                <?php if (isset($usuLibera)) { ?>
                - experimentadore(s) liberados com sucesso!
                <?php } ?>
			</p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000" colspan="4"> <div align="center"></div></td>
          </tr>
<?php
					$i = 0;
					while ($row = $query->fetch_object()) {
?>
          <tr>
            <td><div align="center"><input type="checkbox" name="usu<?php echo $i; ?>" value="<?php echo $row->us_login; ?>"></div></td>
			<td><b><?php echo $row->us_login; ?></b></td>
			<td><b><?php echo $row->us_nome; ?></b></td>
			<td><b><?php echo $row->us_email; ?></b></td>
          </tr>
<?php
						$i++;
					}
?>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input Type="submit" class="botaoLogin" value="   Atualizar   ">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		</table>
	  </form>
<?php
			} // fim do if que verifica se o Experimentador tem acesso a estas informações

		} // fim do if do tipo

	} // fim do if que checa se alguma op��o foi selecionada
?>
      <p>&nbsp;</p>
      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
