<?php

$atID = $_POST["atID"];
$agID = $_POST["agID"];
$biomas = $_POST["biomas"];

if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040100') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
		
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você n�o tem acesso a inserir estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página principal.</p>";
	$db->disconnect;
	return;
}

function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) { 
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}



$sql = "SELECT ex_id FROM experiencia WHERE ex_descricao='$nome'";
$query = $db->query($sql);
$row = $query->fetch_object();
$expID = $row->ex_id;

if (isset($expID)) {
?>

		<script language="JavaScript">

			alert('Já existe uma experiência com este t�tulo ! ');

			window.location.href='experiencias_cadastro.php';

		</script>

<?php
}

$data = date("Y-m-d");
$hora = date("H:i:s");
$sql = "INSERT INTO experiencia (ex_descricao, ex_resumo, ex_liberacao, ex_usuario, ex_chamada, dt_criacao, txt_comentario, ano_publicacao, lat, lng) ".
	   "VALUES ('$nome', '$resumo', 'N', '$cookieUsuario', '$chamada', '$current_date', '$comentario', '$ano_publicacao', str_replace(',','.',$latitude), str_replace(',','.',$longitude))";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "SELECT ex_id FROM experiencia WHERE dt_criacao='$data' AND ex_descricao='$nome'";
$query = $db->query($sql);
$row = $query->fetch_object();
$expID = $row->ex_id;

if (strlen($chaves) > 0) {
	$array = explode(";",$chaves);
	for ($i = 0; $i < count($array); $i++) {
		$palavraChave = trim($array[$i]);
		$tam = strlen($palavraChave);
		if ($tam > 0) {
			$sql = "SELECT pc_id FROM palavra_chave WHERE pc_palavra='$palavraChave'";
			$query = $db->query($sql);
			$row = $query->fetch_object();
			$num = $query->num_rows;
			if ($num > 0) {
				$palavraChaveID = $row->pc_id;
			}
			else {
				$sql = "INSERT INTO palavra_chave (pc_palavra) VALUES ('$palavraChave')";
				$query = $db->query($sql);
			
				$sql = "SELECT pc_id FROM palavra_chave WHERE pc_palavra='$palavraChave'";
				$query = $db->query($sql);
				$row = $query->fetch_object();
				$palavraChaveID = $row->pc_id;
			}
		
			$sql = "INSERT INTO experiencia_chave VALUES ($expID,$palavraChaveID)";
			$query = $db->query($sql);
			if (!$query) {
    			die($db->error);
			}
		}
	}
}

if (isset($autor)) { 
	$i=0; 
	while($i<count($autor)){ 
		$autorID = $autor[$i];
		$sql = "INSERT INTO experiencia_autor VALUES ($expID,'$autorID',0)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

if (isset($instAutor)) { 
	$i=0; 
	while($i<count($instAutor)){ 
		$instID = $instAutor[$i];
		$sql = "INSERT INTO experiencia_autor VALUES ($expID,'NULL',$instID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

if (isset($relator)) { 
	$i=0; 
	while($i<count($relator)){ 
		$relatorID = $relator[$i];
		$sql = "INSERT INTO experiencia_relator VALUES ($expID,'$relatorID',0)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

if (isset($instRelator)) { 
	$i=0; 
	while($i<count($instRelator)){ 
		$instID = $instRelator[$i];
		$sql = "INSERT INTO experiencia_relator VALUES ($expID,'NULL',$instID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

/*if (isset($geo)) { 
	$i=0; 
	while($i<count($geo)){ 
		$geoID = $geo[$i];
		$sql = "INSERT INTO rel_geo_experiencia VALUES ($geoID,$expID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}*/

if (strlen(trim($agID)) > 0) {
	$arrayAG = explode(";",$agID);
	$arrayAG = my_array_unique($arrayAG);
	for ($x = 0; $x < count($arrayAG); $x++) {
		$temp = $arrayAG[$x];
		if (strlen(trim($temp)) > 0) {
			$sql = "INSERT INTO rel_geo_experiencia VALUES ($temp,$expID)";	
			$query = $db->query($sql);
		}
	}
}

/*if (isset($tema)) { 
	$i=0; 
	while($i<count($tema)){ 
		$temaID = $tema[$i];
		$sql = "INSERT INTO rel_area_experiencia VALUES ($temaID,$expID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}*/

if (strlen(trim($atID)) > 0) {
	$arrayAT = explode(";",$atID);
	$arrayAT = my_array_unique($arrayAT);
	for ($x = 0; $x < count($arrayAT); $x++) {
		$temp = $arrayAT[$x];
		if (strlen(trim($temp)) > 0) {
			$sql = "INSERT INTO rel_area_experiencia VALUES ($temp,$expID)";	
			$query = $db->query($sql);
		}
	}
}



if (strlen(trim($biomas)) > 0) {
	$sql = "INSERT INTO rel_habitat_experiencia VALUES ($biomas,$expID)";	
	$query = $db->query($sql);
}

/*if (isset($hab)) { 
	$i=0; 
	while($i<count($hab)){ 
		$habID = $hab[$i];
		$sql = "INSERT INTO rel_habitat_experiencia VALUES ($habID,$expID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}*/

if (isset($bib)) { 
	$i=0; 
	while($i<count($bib)){ 
		$bibID = $bib[$i];
		$sql = "INSERT INTO rel_bib_experiencia VALUES ($bibID,$expID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

$db->close();
?>
<script language="JavaScript">
	window.location.href='experiencias_arquivos.php?tipo=NE&experiencia=<?php echo $expID; ?>&expDescricao=<?php echo trim($nome); ?>';
</script>
