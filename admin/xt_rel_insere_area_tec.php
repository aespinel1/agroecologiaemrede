<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020200') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;

if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página principal.</p>";
	$db->disconnect;
	return;
}

$sql = "SELECT * FROM rel_area_tec WHERE rat_id_tc=$tec AND rat_id_at=$area";
$query = $db->query($sql);
$num = $query->num_rows;

if ($num == 0) {
	$sql = "INSERT INTO rel_area_tec VALUES ($area,$tec)";
	$query = $db->query($sql);
	if (!$query) {
 	   die($db->error);
	}
?>
	<script language="JavaScript">
		window.location.href='rel_tecnologia_tema.php?area=<?php echo $area; ?>';
	</script>
<?php
}
else {
?>
	<script language="JavaScript">
		alert('Já existe um registro desta tecnologia para esta �rea!');
		window.location.href='rel_tecnologia_tema.php?area=<?php echo $area; ?>&novaTecnologia=1';
	</script>
<?php
}
$db->close();
?>
