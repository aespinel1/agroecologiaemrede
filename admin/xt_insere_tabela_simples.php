<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");


switch ($tipoTabela = $_REQUEST['tipoTabela']) {
	case 'GU':
		$varTabela = "grupo_usuarios";
		$varCampo = "gu_descricao";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010100') AND us_admin='1' ";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'ID':
		$varTabela = "idioma";
		$varCampo = "idm_descricao";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010800') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'TB':
		$varTabela = "tipo_bibliografias";
		$varCampo = "tb_descricao";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'TE':
		$varTabela = "tipo_enderecos";
		$varCampo = "te_descricao";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010300') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'TI':
		$varTabela = "tipo_instituicoes";
		$varCampo = "ti_descricao";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010400') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'TT':
		$varTabela = "tipo_telefones";
		$varCampo = "tt_descricao";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010500') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'TX':
		$varTabela = "tipo_experiencias";
		$varCampo = "tx_descricao";
		$varID = "tx_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010600') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'SX':
		$varTabela = "status_experiencia";
		$varCampo = "se_status";
		$varID = "se_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010700') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'LK':
		$varTabela = "tipo_link";
		$varCampo = "descricao";
		$varID = "id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='060000' OR fu_id='060000') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'lista_identidades':
		$varTabela = $tipoTabela;
		$varCampo = "nome";
		$varID = "id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010600') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
} // fim do switch

if ($numPerm == 0) {
	echo "<p align=\"center\">Você não tem acesso a estas informações</p>";
	return;
}

$sql = "SELECT * FROM $varTabela WHERE $varCampo LIKE '$descricao'";
$query = $db->query($sql);
$num = $query->num_rows;

if ($num == 0) {
	$sql = "INSERT INTO $varTabela ($varCampo) VALUES ('$descricao')";
	$query = $db->query($sql);
	if (!$query) {
 	   die($db->error);
	}
	$db->close();
?>
	<script language="JavaScript">
		window.location.href='tabela_simples.php?tipoTabela=<?php echo $tipoTabela; ?>';
	</script>
<?php
}
else {
?>
	<script language="JavaScript">
		alert('Já existe um registro com esta descrição!');
		window.location.href='tabela_simples.php?tipoTabela=<?php echo $tipoTabela; ?>';
	</script>
<?php
}
?>
