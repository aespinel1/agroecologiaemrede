<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300')";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaForm() {
	desc = form2.descricao.value;
	familia = form2.idFamilia.value;
	genero = form2.idGenero.value;
	especie = form2.idEspecie.value;
	nomes = form2.nomes.value;
	if (desc.length == 0) {
		alert('Digite um nome válido!');
		form2.descricao.focus();
		return;
	}
	if (familia.length == 0 && genero.length == 0 && especie.length == 0 && nomes.length == 0) {
		alert('Você deve escolher uma cadeia de fam�lia/g�nero/esp�cie ou digitar pelo\nmenos um nome vulgar para cadastrar uma Animal ou Planta!');
		return;
	}
	form2.submit();
}

function excluiRegistro(id,desc) {
	if(confirm('Você tem certeza que deseja excluir o registro '+desc+'?')) {
		window.location.href='xt_eb_deleta_url.php?id='+id+'&url='+desc;
	}
}

function excluiEB(id) {
	if(confirm('Você tem certeza que deseja excluir esta Animal ou Planta do SisGEA?')) {
		window.location.href='xt_eb_deleta_eb.php?id='+id;
	}
}

function criticaFormURL() {
	url = formURL.url.value;
	if (url.length == 0) {
		alert('Digite uma url válida!');
		formURL.url.focus();
		return;
	}
	formURL.submit();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  destino = selObj.options[selObj.selectedIndex].value;
  eval(targ+".location='"+destino+"'");
  if (restore) selObj.selectedIndex=0;
}
</script>
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr>
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57">
      <div align="center"><a class="Branco" href="xt_logout.php"><img src="imagens/ico_logoff.gif" border="0"><br><br>
        <strong>LOGOUT</strong></a></div>
	</td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td width="173" valign="top">
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_complexa.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><b>Animais e Plantas</b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
?>
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2"><p><strong>Op��es</strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>

          <td width="283"><img src="imagens/seta02.gif" width="12" height="13">
            <a href="eb_familias.php" class="Preto">Cadastro de Fam&iacute;lias</a><br>
              <br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="eb_generos.php" class="Preto">Cadastro
            de G&ecirc;neros</a><br>
          </td>
          <td width="283" valign="top"><img src="imagens/seta02.gif" width="12" height="13">
            <a href="eb_especies.php" class="Preto">Cadastro de Esp&eacute;cies</a><br>
            <br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="eb_cadastro.php" class="Preto">Cadastro
            de Animais e Plantas</a> </td>
          </tr>
        </table>

      <br>
	  <form name="form1" method="post" action="eb_cadastro.php">
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr>
          <td><p><strong>Selecione uma Entidade Biol&oacute;gica
              </strong></p></td>
        </tr>
        <tr>
          <td height="1" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td>
<?php
	$sql = "SELECT eb_id,eb_descricao FROM entidade_biologica ORDER BY eb_descricao";
	$query = $db->query($sql);
	echo "<select name=\"entidade\" class=\"inputFlat\">";
	while ($row = $query->fetch_object()) {
       	$varSelected = "";
		if (isset($entidade) && $entidade == $row->eb_id) {
			$varSelected = "selected";
		}
		echo "<option $varSelected value=\"$row->eb_id\">$row->eb_descricao</option>";
	} // la�o do loop
	echo "</select>";
?>
		<input type="submit" class="botaoLogin" value="Buscar">

		  </td>
        </tr>
      </table>
	  </form>
      <p><br>
      </p>
      <?php if (isset($dadosAtualizados)) { ?>
		<p align="center">
		<b>Dados Atualizados com sucesso!</b>
		</p>
		<?php } ?>
<?php
	if (isset($entidade)) {
		$sql = "SELECT * FROM entidade_biologica WHERE eb_id=$entidade";
		$query = $db->query($sql);
		$rowEB = $query->fetch_object();
?>
      <form name="form2" method="post" action="xt_atualiza_entidade_biologica.php">
	  <input type="hidden" name="id" value="<?php echo $entidade; ?>">
<?php
		if ($rowEB->eb_familia != 0) {
?>
	  <input type="hidden" name="idFamilia" value="<?php echo $rowEB->eb_familia; ?>">
	  <input type="hidden" name="idGenero" value="<?php echo $rowEB->eb_genero; ?>">
	  <input type="hidden" name="idEspecie" value="<?php echo $rowEB->eb_especie; ?>">
<?php
		}
		else {
?>
	  <input type="hidden" name="idFamilia" value="<?php echo $familiaID; ?>">
	  <input type="hidden" name="idGenero" value="<?php echo $generoID; ?>">
	  <input type="hidden" name="idEspecie" value="<?php echo $especieID; ?>">
<?php
		}
?>
        <table width="567" border="0" cellpadding="5" cellspacing="1">

          <tr>
            <td colspan="2"><p>Atualizar dados</p></td>
          </tr>
          <tr>
            <td height="1" colspan="10" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="185"><div align="right"><strong>Descri&ccedil;&atilde;o</strong>:</div></td>
            <td width="359"> <input value="<?php echo $rowEB->eb_descricao; ?>" name="descricao" type="text" size="40" maxlength="255"></td>
          </tr>
          <tr>
            <td><div align="right"><strong>ID Externo</strong>:</div></td>
            <td> <input value="<?php echo $rowEB->eb_id_externo; ?>" name="idExterno" type="text" class="inputFlatRight" id="idExterno">
            </td>
          </tr>
          <tr>
            <td><div align="right"><strong>Fam&iacute;lia</strong>:</div></td>
            <td>
<?php
		$sql = "SELECT * FROM eb_familia ORDER BY ebf_familia";
		$query = $db->query($sql);
		echo "<select name=\"familia\" class=\"inputFlat\" onChange=\"MM_jumpMenu('parent',this,0)\">";
		echo "<option value=\"eb_cadastro.php?entidade=$entidade\">-- Selecione uma familia --</option>";
		while ($row = $query->fetch_object()) {
       		$varSelected = "";
			if ($rowEB->eb_familia == $row->ebf_id || $familiaID == $row->ebf_id) {
				$varSelected = "selected";
			}
			echo "<option $varSelected value=\"eb_cadastro.php?entidade=$entidade&familiaID=$row->ebf_id\">$row->ebf_familia</option>";
		} // la�o do loop
		echo "</select>";
?>
            </td>
          </tr>
          <tr>
            <td><div align="right"><strong>G&ecirc;nero</strong>:</div></td>
            <td>
<?php
		if (isset($familiaID) || $rowEB->eb_genero != 0) {
			if ($rowEB->eb_genero != 0 && !isset($familiaID)) {
				$tempIDFamilia = $rowEB->eb_familia;
				$sql = "SELECT * FROM eb_genero WHERE ebg_id_familia=$tempIDFamilia ORDER BY ebg_genero";
			}
			else {
				$sql = "SELECT * FROM eb_genero WHERE ebg_id_familia=$familiaID ORDER BY ebg_genero";
			}
			$query = $db->query($sql);
			echo "<select name=\"genero\" class=\"inputFlat\" onChange=\"MM_jumpMenu('parent',this,0)\">";
			echo "<option value=\"eb_cadastro.php?entidade=$entidade&familiaID=$familiaID\">-- Selecione um g�nero --</option>";
			while ($row = $query->fetch_object()) {
   			   	$varSelected = "";
				if ($rowEB->eb_genero == $row->ebg_id || $generoID == $row->ebg_id) {
					$varSelected = "selected";
				}
	   		   	echo "<option $varSelected value=\"eb_cadastro.php?entidade=$entidade&familiaID=$familiaID&generoID=$row->ebg_id\">$row->ebg_genero</option>";
			} // la�o do loop
			echo "</select>";
		}
?>
            </td>
          </tr>
          <tr>
            <td><div align="right"><strong>Esp&eacute;cie</strong>:</div></td>
            <td>
<?php
		if (isset($generoID) || $rowEB->eb_especie != 0) {
			if ($rowEB->eb_especie != 0 && !isset($generoID)) {
				$tempIDGenero = $rowEB->eb_genero;
				$sql = "SELECT * FROM eb_especie WHERE ebe_id_genero=$tempIDGenero ORDER BY ebe_especie";
			}
			else {
				$sql = "SELECT * FROM eb_especie WHERE ebe_id_genero=$generoID ORDER BY ebe_especie";
			}
			$query = $db->query($sql);
			echo "<select name=\"especie\" class=\"inputFlat\" onChange=\"MM_jumpMenu('parent',this,0)\">";
			echo "<option value=\"eb_cadastro.php?entidade=$entidade&familiaID=$familiaID&generoID=$generoID\">-- Selecione uma esp�cie --</option>";
			while ($row = $query->fetch_object()) {
   		   		$varSelected = "";
				if ($rowEB->eb_especie == $row->ebe_id || $especieID == $row->ebe_id) {
					$varSelected = "selected";
				}
		   	   	echo "<option $varSelected value=\"eb_cadastro.php?entidade=$entidade&familiaID=$familiaID&generoID=$generoID&especieID=$row->ebe_id\">$row->ebe_especie</option>";
			} // la�o do loop
			echo "</select>";
		}
?>
            </td>
          </tr>
          <tr>
            <td> <div align="right"><strong>Infra-esp&eacute;cie</strong>:</div></td>
            <td><input value="<?php echo $rowEB->eb_infra_especie; ?>" name="infra" type="text" id="infra" size="40" maxlength="255"></td>
          </tr>
          <tr>
            <td valign="top"> <div align="right"><strong>Nomes
                vulgares</strong>:<br>
                <br>
                Digite os nomes vulgares, separando-os por espa&ccedil;os.</div></td>
            <?php
		$sql = "SELECT * FROM eb_nome_vulgar WHERE ebn_id_entidade=$entidade";
		$query = $db->query($sql);
?>
            <td><textarea name="nomes" cols="67" rows="10" id="nomes">
<?php
		while ($row = $query->fetch_object()) {
	   	   	echo "$row->ebn_nome ";
		} // la�o do loop
?>
			</textarea></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input name="enviar" type="button" class="botaoLogin" value="ATUALIZAR" onClick="criticaForm();">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input name="Button" type="button" class="botaoLogin" value="Excluir Animal ou Planta" onClick="excluiEB('<?php echo $entidade; ?>');"></td>
          </tr>
        </table>
		</form>
        <br>
		<form  name="formURL" method="post" action="xt_eb_insere_url.php">
		<input type="hidden" name="id" value="<?php echo $entidade; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2"><p><strong>Websites</strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="81"><div align="right">URL:</div></td>
            <td width="483"> <input name="url" type="text" size="60" maxlength="255">
            </td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input type="button" class="botaoLogin" value="Gravar" onClick="criticaFormURL();"></td>
          </tr>
        </table>
		</form>
        <br>
		<form method="post" name="formURL2" action="xt_eb_atualiza_url.php">
        <input type="hidden" name="id" value="<?php echo $entidade; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2"><p>Registros em "<strong>URL</strong>"</p></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="527"><strong>URL</strong></td>
            <td width="40"> <div align="center"><strong>Excluir</strong></div></td>
          </tr>
          <?php
	$sql = "SELECT * FROM eb_url WHERE ebu_id_entidade=$entidade ORDER BY ebu_url";
	$query = $db->query($sql);
	if (!$query) {
    	die($db->error);
	}
	$i = 0;
	while ($row = $query->fetch_object()) {
?>
          <tr>
			<input name="urlAntiga<?php echo $i; ?>" type="hidden" value="<?php echo trim($row->ebu_url); ?>">
            <td><input name="url<?php echo $i; ?>" type="text" value="<?php echo trim($row->ebu_url); ?>" size="60" maxlength="80"></td>
            <td><div align="center"><a href="#" onClick="excluiRegistro('<?php echo $row->ebu_id_entidade; ?>','<?php echo trim($row->ebu_url); ?>')"><img src="imagens/ico_exclui.gif" width="18" height="18" border="0"></a></div></td>
          </tr>
          <?php
		$i++;
	} // la�o do loop
?>
          <tr>
            <td colspan="2"> <div align="center">
                <input name="enviar" type="submit" class="botaoLogin" value="   ATUALIZAR   ">
              </div></td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
      </form>
<?php
	} // fim do if que verifica se a busca foi feita

} // fim do if que verifica se o usuário tem acesso � informa��o
?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
