<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040500') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
		
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você n�o tem acesso a inserir estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página principal.</p>";
	$db->disconnect;
	return;
}

if ($acao == 'libera') {

    $sql = "SELECT * FROM texto WHERE tex_id='ASS_CONFIRMACAO'";
    $query1 = $db->query($sql);
    $rowMsg3 = $query1->fetch_object();

    $sql = "SELECT * FROM texto WHERE tex_id='SUBJECT_CONFIRMACAO'";
    $query1 = $db->query($sql);
    $rowMsg2 = $query1->fetch_object();

    $sql = "SELECT * FROM texto WHERE tex_id='TEXTO_CONFIRMACAO'";
    $query1 = $db->query($sql);
    $rowMsg1 = $query1->fetch_object();

	for ($i = 0; $i < $qtItens; $i++) {
	
		$id = '';
		eval("\$id = \"\$exp$i\";");
		
		if (strlen($id) > 0) {
			$sql = "SELECT us_email,us_nome,ex_descricao FROM usuario,experiencia WHERE ex_usuario=us_login AND ex_id=$id";
			$query = $db->query($sql);
			$row = $query->fetch_object();
			$email = $row->us_email;
			$nome = $row->us_nome;
			$experienciaDescricao = $row->ex_descricao;
		
			$sql = "UPDATE experiencia SET ex_liberacao='S' WHERE ex_id='$id'";
			$query = $db->query($sql);
			if (!$query) {
    			die($db->error);
			}
	
			$corpo = "Caro(a) $nome,\n\n";
			$corpo .= $rowMsg1->tex_texto;
			$corpo .= $experienciaDescricao."\n\n";
			$corpo .= $rowMsg3->tex_texto;
			$mail = mail($email,"$rowMsg2->tex_texto \"$experienciaDescricao\" ",$corpo);
		}
	}
}
else if ($acao == 'unico') {
	
	 $sql = "SELECT us_email,us_nome,ex_descricao FROM usuario,experiencia WHERE ex_usuario=us_login AND ex_id=$experiencia";
	 $query = $db->query($sql);
	 $row = $query->fetch_object();
	 $email = $row->us_email;
	 $nome = $row->us_nome;
	 $experienciaDescricao = $row->ex_descricao;
		
	 $sql = "UPDATE experiencia SET ex_liberacao='S' WHERE ex_id='$experiencia'";
	 $query = $db->query($sql);
	 if (!$query) {
    	die($db->error);
	 }
	 
	 $sql = "SELECT * FROM texto WHERE tex_id='ASS_CONFIRMACAO'";
     $query1 = $db->query($sql);
     $rowMsg3 = $query1->fetch_object();

     $sql = "SELECT * FROM texto WHERE tex_id='SUBJECT_CONFIRMACAO'";
     $query1 = $db->query($sql);
     $rowMsg2 = $query1->fetch_object();

     $sql = "SELECT * FROM texto WHERE tex_id='TEXTO_CONFIRMACAO'";
     $query1 = $db->query($sql);
     $rowMsg1 = $query1->fetch_object();
	
	 $corpo = "Caro(a) $nome,\n\n";
	 $corpo .= $rowMsg1->tex_texto."\n\n";
	 $corpo .= $experienciaDescricao."\n\n";
	 $corpo .= $rowMsg3->tex_texto;
	 $mail = mail($email,"$rowMsg2->tex_texto \"$experienciaDescricao\" ",$corpo);
		
}
else if ($acao == 'pendente') {

	 $sql = "SELECT * FROM texto WHERE tex_id='ASS_BLOQUEIO'";
     $query1 = $db->query($sql);
     $rowMsg3 = $query1->fetch_object();

     $sql = "SELECT * FROM texto WHERE tex_id='SUBJECT_BLOQUEIO'";
     $query1 = $db->query($sql);
     $rowMsg2 = $query1->fetch_object();

     $sql = "SELECT * FROM texto WHERE tex_id='TEXTO_BLOQUEIO'";
     $query1 = $db->query($sql);
     $rowMsg1 = $query1->fetch_object();
	 
	 for ($i = 0; $i < $qtItens; $i++) {
	
		$id = '';
		eval("\$id = \"\$exp$i\";");
	
		if (strlen($id) > 0) {
		
			$sql = "SELECT us_email,us_nome,ex_descricao FROM usuario,experiencia WHERE ex_usuario=us_login AND ex_id=$id";
			$query = $db->query($sql);
			$row = $query->fetch_object();
			$email = $row->us_email;
			$nome = $row->us_nome;
			$experienciaDescricao = $row->ex_descricao;
			
			$sql = "UPDATE experiencia SET ex_liberacao='P',txt_pendente='$justificativa', dt_pendente='$current_date' WHERE ex_id=$id ";
			$query = $db->query($sql);
			if (!$query) {
    			die($db->error);
			}
	
	        $corpo = "Caro(a) $nome,\n\n";
	        $corpo .= $rowMsg1->tex_texto."\n\n";
	        $corpo .= $experienciaDescricao."\n\n";
			$corpo .= "Motivo: $justificativa\n\n";
	        $corpo .= $rowMsg3->tex_texto;
	        $mail = mail($email,"$rowMsg2->tex_texto \"$experienciaDescricao\" ",$corpo);
	 			
		}
	}
}
else {

	$sql = "SELECT * FROM texto WHERE tex_id='ASS_REJEICAO'";
    $query1 = $db->query($sql);
    $rowMsg3 = $query1->fetch_object();

    $sql = "SELECT * FROM texto WHERE tex_id='SUBJECT_REJEICAO'";
    $query1 = $db->query($sql);
    $rowMsg2 = $query1->fetch_object();

    $sql = "SELECT * FROM texto WHERE tex_id='TEXTO_REJEICAO'";
    $query1 = $db->query($sql);
    $rowMsg1 = $query1->fetch_object();
	 
	for ($i = 0; $i < $qtItens; $i++) {
	
		$id = '';
		eval("\$id = \"\$exp$i\";");

		if (strlen($id) > 0) {
			
			$sql = "SELECT us_email,us_nome,ex_descricao FROM usuario,experiencia WHERE ex_usuario=us_login AND ex_id=$id";
			$query = $db->query($sql);
			$row = $query->fetch_object();
			$email = $row->us_email;
			$nome = $row->us_nome;
			$experienciaDescricao = $row->ex_descricao; 
			
			$sql = "UPDATE experiencia SET ex_liberacao='R' WHERE ex_id=$id";
			$query = $db->query($sql);
			if (!$query) {
    			die($db->error);
			}
		
	        $corpo = "Caro(a) $nome,\n\n";
	        $corpo .= $rowMsg1->tex_texto."\n\n";
	        $corpo .= $experienciaDescricao."\n\n";
			$corpo .= "Motivo: $justificativa\n\n";
	        $corpo .= $rowMsg3->tex_texto;
	        $mail = mail($email,"$rowMsg2->tex_texto \"$experienciaDescricao\" ",$corpo);			
		}
	}

}

$db->close();
?>
<script language="JavaScript">
	window.location.href='experiencias_libera.php?expLibera=1';
</script>
