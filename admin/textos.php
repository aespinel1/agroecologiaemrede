<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND fu_id='070000' AND us_admin=1 AND us_admin='1'";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script>
	function marca_tabela () {

	   var el = document.all['selecao_criterio']
	   el.style.display = "";
	   var el = document.all['atualiza_bloco']
	   el.style.display = "none";

	}

	function desmarca_tabela () {
	    var el = document.all['selecao_criterio'];
	    el.style.display = "none";

	    var el = document.all['atualiza_bloco']
	    el.style.display = "";

	}
</script>

<?php include("menu_admin.php"); ?>
<table width="650" border="0" cellpadding="5" cellspacing="0" align="center" >
  <tr>
    <td width="640" valign="top" align="center">
	<table width="580" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="23" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="48" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="490" bgcolor="#80AAD2"> <p><strong>Textos / Parâmetros
              do Sistema </strong><b></b></p></td>
          <td width="79" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
      <?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {

?>
      <form name="form1" method="post"  >

        <table width="567" border="0" cellpadding="5" cellspacing="1" id="selecao_criterio" <?php if (isset($texto) && $texto <> "") { echo "style='display:none'"; }  ?> >
          <tr>
          <td height="24" colspan="4">
            <p><strong>Selecione o Texto / Par&acirc;metro </strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"> <select name="texto">
<?php
				$sql = "SELECT * FROM texto ORDER BY tex_titulo";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$titulo = trim($row->tex_titulo);
					$varSelected = "";
					if (isset($texto) && $texto == $row->tex_id) {
						$varSelected = "selected";
					}
					echo "<option value=\"$row->tex_id\" $varSelected>$titulo</option>";
				}
?>
              </select> &nbsp;&nbsp;&nbsp; <input type="submit" class="botaoLogin" value="   Selecionar   " >            </td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		</table>
	  </form>
<?php


	if (isset($texto)) {
		if (isset($atualiza) && $atualiza == "1") {

			$sql = "UPDATE texto SET tex_titulo='".$db->real_escape_string($tit)."',  tex_texto='".$db->real_escape_string($text)."'
					WHERE tex_id='$texto'";

			$db->query($sql);

		}

		$sql = "SELECT * FROM texto WHERE tex_id='$texto'";
		$query = $db->query($sql);
		$row = $query->fetch_object();

?>
    <form name="form2" method="post" action="textos.php">
	<input type="hidden" name="texto" value="<?php echo $texto; ?>" >
	<input type="hidden" name="atualiza" value="1">
        <table width="567" border="0" align="center" cellpadding="5" cellspacing="1" id="atualiza_bloco" >
          <tr>
            <td colspan="2"> <p>Informa&ccedil;&otilde;es</td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td ><div align="right">T&iacute;tulo:</div></td>
            <td width="449"><input name="tit" type="text" size="80" maxlength="255" value="<?php echo $row->tex_titulo; ?>" readonly></td>
          </tr>
          <tr>
            <td><div align="right">Texto / Valor :</div></td>
            <td><textarea name="text" cols="80" rows="10" wrap="VIRTUAL" id="text"><?php echo $row->tex_texto; ?></textarea></td>
          </tr>

          <tr>
            <td height="1" colspan="2" bgcolor="#000000"><div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="2">
              <div align="center">
                <input name="Submit" type="submit" class="botaoLogin" value="Atualizar">
				&nbsp;&nbsp;<input name="Submit" type="button" class="botaoLogin" value="Selecionar outro texto" onclick="marca_tabela();">

              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"><div align="center"></div></td>
          </tr>
        </table>
     </form>
<?php
}
  else
  {
      //marca_tabela();
	} // fim do if que verifica se uma experiência foi buscada

} // fim do if que verifica se o usuário tem acesso a estas informações
?>


	  </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
