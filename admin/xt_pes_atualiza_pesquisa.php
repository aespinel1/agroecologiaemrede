<?php

$atID = $_POST["atID"];
$agID = $_POST["agID"];
$biomas = $_POST["biomas"];

if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040200') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
		
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você n�o tem acesso a inserir estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página principal.</p>";
	$db->disconnect;
	return;
}

function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) { 
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}

$sql = "UPDATE pesquisa SET pe_descricao='$nome',pe_resumo='$resumo',pe_chamada='$nome',dt_atualizacao='$current_date',txt_comentario='$comentario',ano_publicacao='$ano_publicacao' WHERE pe_id=$pesquisa";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "DELETE FROM pesquisa_autor WHERE pea_id_pesquisa=$pesquisa";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}


if (isset($autor)) { 
	$i=0; 
	while($i<count($autor)){ 
		$autorID = $autor[$i];
		$sql = "INSERT INTO pesquisa_autor VALUES ($pesquisa,'$autorID',0)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

if (isset($instAutor)) { 
	$i=0; 
	while($i<count($instAutor)){ 
		$instID = $instAutor[$i];
		$sql = "INSERT INTO pesquisa_autor VALUES ($pesquisa,'NULL',$instID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}


$sql = "DELETE FROM pesquisa_relator WHERE per_id_pesquisa=$pesquisa";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "DELETE FROM rel_geo_pesquisa WHERE rgp_id_pesquisa=$pesquisa";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}

$agID = $_POST["agID"];
$atID = $_POST["atID"];


if (strlen(trim($agID)) > 0) {
	$arrayAG = explode(";",$agID);
	$arrayAG = my_array_unique($arrayAG);
	for ($x = 0; $x < count($arrayAG); $x++) {
		$temp = $arrayAG[$x];
		if (strlen(trim($temp)) > 0) {
			$sql = "INSERT INTO rel_geo_pesquisa VALUES ($temp,$pesquisa)";	
			$query = $db->query($sql);
		}
	}
}


$sql = "DELETE FROM rel_area_pesquisa WHERE rap_id_pesquisa=$pesquisa";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}


if (strlen(trim($atID)) > 0) {
	$arrayAT = explode(";",$atID);
	$arrayAT = my_array_unique($arrayAT);
	for ($x = 0; $x < count($arrayAT); $x++) {
		$temp = $arrayAT[$x];
		if (strlen(trim($temp)) > 0) {
			$sql = "INSERT INTO rel_area_pesquisa VALUES ($temp,$pesquisa)";	
			$query = $db->query($sql);
		}
	}
}

$sql = "DELETE FROM rel_habitat_pesquisa WHERE rhp_id_pesquisa=$pesquisa";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

if (strlen(trim($biomas)) > 0) {
	$sql = "INSERT INTO rel_habitat_pesquisa VALUES ($biomas,$pesquisa)";	
	$query = $db->query($sql);
}


$db->close();
?>
<script language="JavaScript">
	window.location.href='pesquisas_altera.php?pesAtualizada=1&pesquisa=<?php echo $pesquisa; ?>';
</script>