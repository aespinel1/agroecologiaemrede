<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");
$sql = "SELECT * FROM usuario WHERE us_login='$cookieUsuario' AND us_session='$cookieSession' AND us_admin=1";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

if (isset($tipo)) {
	switch ($tipo) {
		case "NE":
			$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040100') AND us_admin=1";
			$query = $db->query($sql);
			$numPerm = $query->num_rows;
			break;
		case "AD":
			$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040200') AND us_admin=1";
			$query = $db->query($sql);
			$numPerm = $query->num_rows;
			break;
		case "LE":
			$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040500') AND us_admin=1";
			$query = $db->query($sql);
			$numPerm = $query->num_rows;
			break;
		case "EE":
			$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040600') AND us_admin='1' ";
			$query = $db->query($sql);
			$numPerm = $query->num_rows;
			break;
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function criticaFormExp() {
	desc = formNE.nome.value;
	cham = formNE.chamada.value;
	if (desc.length == 0) {
		alert('Digite um t�tulo v�lido para a experiência!');
		formNE.nome.focus();
		return;
	}
	if (cham.length == 0) {
		alert('Digite uma chamada v�lida para a experiência!');
		formNE.chamada.focus();
		return;
	}
	formNE.submit();
}

function criticaFormAltera(action) {
	formAD.action = action;
	formAD.submit();
}

function criticaFormAtualiza() {
	nome = formAtualiza.nome.value;
	cham = formAtualiza.chamada.value;
	if (nome.length == 0) {
		alert('Digite um nome v�lido para a experiência!');
		formAtuliza.nome.focus();
		return;
	}
	if (cham.length == 0) {
		alert('Digite uma chamada v�lida para a experiência!');
		formAtuliza.chamada.focus();
		return;
	}

	formAtualiza.submit();
}

function submitFormLE(acao) {
	if (acao == 'libera') {
		formLE.acao.value = 'libera';
	}
	else if (acao == 'pendente') {
		formLE.acao.value = 'pendente';
	}
	else {
		formLE.acao.value = 'cancela';
	}

	formLE.submit();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_setTextOfLayer(objName,x,newText) { //v4.01
  if ((obj=MM_findObj(objName))!=null) with (obj)
    if (document.layers) {document.write(unescape(newText)); document.close();}
    else innerHTML = unescape(newText);
}
//-->
</script>
<style type="text/css">
<!--
.style1 {
	color: #006666;
	font-weight: bold;
}
-->
</style>
<?php include("menu_admin.php"); ?> 

<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>

    <td width="567" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20"  bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42"  bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Experi&ecirc;ncias - Cancela Libera��o</strong><b></b></p></td>
          <td width="19"  bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>

	  <?php if (isset($novaExperiencia)) { ?>
      <p align="center" class="textoPreto10px style1">Experiência <?php echo $expDescricao; ?> cadastrada com sucesso !</p>
	  <?php } ?>


<?php
	if (isset($tipo)) {

		if ($tipo == "NE") {

			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
?>

      <form name="formNE" method="post" action="xt_exp_insere_experiencia.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td colspan="4"> <p> <strong>Cadastrar nova
                experiência</strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td > <div align="right"><strong>T&iacute;tulo</strong>:</div></td>
            <td width="363" colspan="3"><input name="nome" type="text" id="nome" size="69" maxlength="255"></td>
          </tr>
          <tr>
            <td><div align="right"><strong>Chamada</strong>:</div></td>
            <td colspan="3"><input name="chamada" type="text" id="chamada" size="69" maxlength="70"></td>
          </tr>
          <tr>
            <td valign="top"> <div align="right"><strong>Autor(es)</strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla
                CTRL do seu teclado. </div></td>
            <td colspan="3"><select name="autor[]" size="6" multiple id="autor[]">
                <?php
		$sql = "SELECT us_login,us_nome FROM usuario WHERE us_autor='1' AND us_liberacao='S' ORDER BY us_login";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select> <select name="instAutor[]" size="6" multiple id="instAutor[]">
                <?php
		$sql = "SELECT in_id,in_nome FROM frm_instituicao WHERE in_liberacao='S' ORDER BY in_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select></td>
          </tr>
          <tr>
            <td valign="top"> <div align="right"><strong>Relator(es)</strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla
                CTRL do seu teclado. </div></td>
            <td colspan="3"><select name="relator[]" size="6" multiple id="relator[]">
                <?php
		$sql = "SELECT * FROM usuario WHERE us_relator='1' AND us_liberacao='S' ORDER BY us_login";
		$query = $db->query($sql);

		while ($row = $query->fetch_object()) {
			$varSelected = "";
			if ($usuario == trim($row->us_login)) {
				$varSelected = "selected";
			}
?>
                <option value="<?php echo trim($row->us_login); ?>" <?php echo $varSelected; ?>><?php echo trim($row->us_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select> <select name="instRelator[]" size="6" multiple id="instRelator[]">
                <?php
		$sql = "SELECT in_id,in_nome FROM frm_instituicao WHERE in_liberacao='S' ORDER BY in_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select></td>
          </tr>
          <tr>
            <td valign="top"> <div align="right"><strong>Resumo</strong>:<br>
                <br>
                Tags HTML s&atilde;o aceitos. &lt;i&gt;&lt;/i&gt; para it&aacute;lico,
                &lt;b&gt;&lt;/b&gt; para negrito. As quebras de linha s&atilde;o
                automaticamente convertidas em quebras na impress&atilde;o do
                relat&oacute;rio. </div></td>
            <td colspan="3"><textarea name="resumo" cols="67" rows="10" id="resumo"></textarea></td>
          </tr>
          <tr>
            <td valign="top"><div align="right"><strong>&Aacute;reas
                Geogr&aacute;ficas</strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla
                CTRL do seu teclado.</div></td>
            <td colspan="3" valign="top"> <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_ag_nav.php?url=experiencias.php','at','scrollbars=yes,width=517,height=300,status=yes')">Clique
              aqui para adicionar<strong> &Aacute;reas Geogr&aacute;ficas</strong></a><br>
              <textarea name="areasGeograficas" cols="50" rows="5" wrap="VIRTUAL" readonly></textarea>
              <input type="hidden" name="agID"> </td>
          </tr>
          <tr>
            <td valign="top"><div align="right"><strong>&Aacute;reas
                Tem&aacute;ticas</strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla
                CTRL do seu teclado.</div></td>
            <td colspan="3" valign="top"> <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_at_nav.php?url=experiencias.php','at','scrollbars=yes,width=517,height=300,status=yes')">Clique
              aqui para adicionar<strong> &Aacute;reas Tem&aacute;ticas</strong></a><br>
              <textarea name="areasTematicas" cols="50" rows="5" wrap="VIRTUAL" readonly></textarea>
              <input type="hidden" name="atID"> </td>
          </tr>
          <tr>
            <td valign="top"><div align="right"><strong>Biomas</strong>:</div></td>
            <!--
			<td colspan="3" valign="top">
			<a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_biomas.php?url=experiencias.php','bi','scrollbars=yes,width=517,height=300,status=yes')">Clique
              aqui para adicionar<strong> Biomas</strong></a><br>
              <textarea name="biomas" cols="50" rows="5" wrap="VIRTUAL" readonly></textarea>
              <input type="hidden" name="biomaID"> </td>-->
			 <td colspan="3" valign="top">
<?php
				$sql = "SELECT * FROM habitat ORDER BY ha_descricao";
				$query = $db->query($sql);
				echo "<select name=\"biomas\" class=\"inputFlat\">\n";
				while ($row = $query->fetch_object()) {
					echo "<option value=\"$row->ha_id\">$row->ha_descricao</option>\n";
				}
?>

			 </td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input type="button" class="botaoLogin" value="   Inserir   " onClick="criticaFormExp();">
              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
	  </form>
<?php
			} // fim do if que verifica se o usuário tem acesso a esta fun��o
		}
		else if ($tipo == "AD") {

			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
?>
	  <form name="formAD" method="post">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
          <td height="24" colspan="4">
            <p> <strong>Alterar Dados - Experi&ecirc;ncia</strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4">
			 Experi&ecirc;ncia:
              <select name="experiencia">
<?php
				$sql = "SELECT * FROM experiencia ORDER BY ex_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->ex_chamada);
					$varSelected = "";
					if (isset($experiencia) && $experiencia == $row->ex_id) {
						$varSelected = "selected";
					}
					echo "<option value=\"$row->ex_id\" $varSelected>$desc</option>";
				}
?>
              </select> &nbsp;&nbsp;&nbsp; <input type="button" class="botaoLogin" onClick="criticaFormAltera('experiencias.php?tipo=AD');" value="   Selecionar   ">
            </td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		</table>
	  </form>
<?php
				if (isset($experiencia)) {

					$sql = "SELECT * FROM experiencia WHERE ex_id=$experiencia";
					$query = $db->query($sql);
					$rowExp = $query->fetch_object();
?>
	<?php if (isset($libera) && $libera == "1") { ?>
	<p><b>[ <a href="experiencias.php?tipo=LE&experiencia=<?php echo $experiencia; ?>" class="Preto">Voltar para a tela de libera��o</a> ]</b></p>
	<?php } ?>
	<form name="formAtualiza" method="post" action="xt_exp_atualiza_experiencia.php">
	<input type="hidden" name="id" value="<?php echo $rowExp->ex_id; ?>">
	<input type="hidden" name="libera" value="<?php echo $libera; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="196"> <div align="right"><strong>T&iacute;tulo</strong>:</div></td>
            <td width="326" colspan="3"> <input name="nome" type="text" size="69" maxlength="255" value="<?php echo trim($rowExp->ex_descricao); ?>"></td>
          </tr>
          <tr>
            <td valign="top"><div align="right"><strong>Chamada</strong>:</div></td>
            <td colspan="3"><input name="chamada" type="text" id="chamada" value="<?php echo trim($rowExp->ex_chamada); ?>" size="69" maxlength="70"></td>
          </tr>
          <tr>
            <td valign="top"> <div align="right"><strong>Resumo</strong>:<br>
                <br>
                Tags HTML s&atilde;o aceitos. &lt;i&gt;&lt;/i&gt; para it&aacute;lico,
                &lt;b&gt;&lt;/b&gt; para negrito. As quebras de linha s&atilde;o
                automaticamente convertidas em quebras na impress&atilde;o do
                relat&oacute;rio. </div></td>
            <td colspan="3"> <textarea name="resumo" cols="67" rows="10" id="textarea"><?php echo trim($rowExp->ex_resumo); ?></textarea></td>
          </tr>
          <tr>
            <td colspan="4" valign="top">
              <div align="center">[ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=AG&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">&Aacute;reas
                Geogr&aacute;ficas</a> ] &nbsp;&nbsp;[ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=AT&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">&Aacute;reas
                Tem&aacute;ticas</a> ]&nbsp;&nbsp; [ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_autores.php?id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Autores</a>
                ]&nbsp;&nbsp;&nbsp;[ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relatores.php?id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Relatores</a>
                ]<br>
                <br>
                [ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=RB&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Refer&ecirc;ncias
                Bibliogr&aacute;ficas</a> ] [ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=BM&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Biomas</a>
                ]</div></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000">
              <div align="center"></div></td>

          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input Type="button" class="botaoLogin" onClick="criticaFormAtualiza();" value="   Atualizar   ">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000">
              <div align="center"></div></td>
          </tr>
          <?php
				} // fim do if que verifica se foi feita a busca
?>
        </table>
	  </form>
<?php
			} // fim do if que verifica se o usuário tem acesso a estas informações

		}
		else if ($tipo == "LE") {

			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
?>
<?php
				$sql = "SELECT * FROM experiencia WHERE ex_liberacao='N'";
				$query = $db->query($sql);
?>
	  <form name="formLE" method="post" action="xt_exp_libera_experiencia.php">
	  <input type="hidden" name="acao" value="">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td height="24" colspan="5"> <p> <strong>Liberar
                Experiências</strong>
                <?php if (isset($expLibera)) { ?>
                <span class="style1">- experiência(s) liberadas com sucesso !</span>
                <?php } ?>
              </p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000" colspan="5"> <div align="center"></div></td>
          </tr>
          <?php
					$i = 0;
					while ($row = $query->fetch_object()) {
						$data = explode("-",$row->dt_criacao);
?>
          <tr>
            <td> <div align="center">
                <input type="checkbox" name="exp<?php echo $i; ?>" value="<?php echo $row->ex_id; ?>">
              </div></td>
            <td><b><a class="Preto" href="experiencias.php?tipo=AD&libera=1&experiencia=<?php echo $row->ex_id; ?>"><?php echo $row->ex_descricao; ?></a></b></td>
            <?php
				$sql = "SELECT * FROM exp_log_cadastro WHERE experiencia=$row->ex_id";
				$queryInt = $db->query($sql);
				$rowInt = $queryInt->fetch_object();
			?>
			<td><b><?php echo $rowInt->nome; ?></b></td>
			<td><b><?php echo $rowInt->email; ?></b></td>
            <td align="center"><b><?php echo "$data[2]/$data[1]/$data[0]"; ?></b></td>
          </tr>
          <?php
						$i++;
					}
?>
          <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5"> <div align="center">
                <input name="Button" Type="button" class="botaoLogin" value="   Liberar   " onClick="submitFormLE('libera');">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5"> <div align="left"> <span>Utilize
                o campo abaixo para justificar o motivo do cancelamento ou mudan&ccedil;a
                de status para &quot;Pendente&quot; para o usu&aacute;rio que
                efetuou este cadastro. Este ir&aacute; receber um e-mail com os
                dados digitados abaixo.</span><br>
                <br>
                <textarea name="justificativa" cols="105" rows="5" wrap="VIRTUAL" id="justificativa"></textarea>
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5"> <div align="center">
                <input name="Button" Type="button" class="botaoLogin" value="   Deixar Pendente   " onClick="submitFormLE('pendente');">
                &nbsp;&nbsp;
                <input name="Button2" Type="button" class="botaoLogin" value="   Cancelar   " onClick="submitFormLE('cancela');">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"><div align="center"></div></td>
          </tr>
        </table>
	  </form>
<?php
			} // fim do if que verifica se o usuário tem acesso a estas informações

		} // fim do if que verifica qual a fun��o
		else if ($tipo == "EE") {

			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {

				$sql = "SELECT * FROM experiencia,usuario WHERE ex_usuario=us_login";
				$query = $db->query($sql);

				if (isset($expExcluida)) {
					echo "<p class=\"textoPreto10px\" align=\"center\"><b>Experiência exclu�da com sucesso!</b></p>";
				}
?>
	  <form name="formExclui" method="post" action="exp_exclui_experiencia.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td height="24" colspan="4"> <p> <strong>Excluir
                Experiência </strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"> Experi&ecirc;ncia:
              <select name="experiencia" id="experiencia">
                <?php
				$sql = "SELECT * FROM experiencia ORDER BY ex_chamada";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->ex_chamada);
					$varSelected = "";
					if (isset($experiencia) && $experiencia == $row->ex_id) {
						$varSelected = "selected";
					}
					echo "<option value=\"$row->ex_id\" $varSelected>$desc</option>";
				}
?>
              </select> &nbsp;&nbsp;&nbsp; <input name="Submit" type="submit" class="botaoLogin" value="   Visualizar   ">
            </td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
        </table>
      </form>
<?php
			} // fim do if que verifica se o usuário tem acesso a estas informações

		} // fim do if que verifica qual a fun��o

	} // fim do if que verifica se o tipo de a��o foi selecionado
?>


    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
