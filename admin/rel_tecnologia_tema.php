<?php
include("conexao.inc.php");
?>
<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020200' OR fu_id='020200') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaFormUsuario() {

	nome = formCU.nome.value;
	email = formCU.email.value;
	senha = formCU.senha.value;
	senhaConf = formCU.senhaConf.value;
	
	if (nome.length == 0) {
		alert('Preencha um nome v�lido para o usuário!');
		formCU.nome.focus();
		return;
	}
	
	if (senha.length > 0) {
		if (senha != senhaConf) {
			alert('A senha n�o confere! Por favor, digite e confirme a senha do usuário!');
			formCU.senha.value = '';
			formCU.senhaConf.value = '';
			formCU.senha.focus();
			return;
		}
	}
	
	if (email.length == 0) {
		alert('Preencha um e-mail v�lido para o usuário!');
		formCU.email.focus();
		return;
	}
	
	formCU.submit();
	
}

function insereTecnologia() {
	formTec.action = 'xt_rel_insere_area_tec.php';
	formTec.submit();
}

function excluiTecnologia(area,desc,id) {
	if(confirm('Você tem certeza que deseja excluir a tecnologia ' + desc + ' para esta �rea tem�tica?')) {
		window.location.href='xt_rel_exclui_area_tec.php?area='+area+'&tec='+id;
	}
}

</script>
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57"> 
      <div align="center"><a class="Branco" href="xt_logout.php"><img src="imagens/ico_logoff.gif" border="0"><br><br>
        <strong>LOGOUT</strong></a></div>
	</td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_complexa.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Relacionamento 
              de Tecnologias e &Aacute;reas Tem&aacute;ticas</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {

	$sql = "SELECT * FROM area_tematica WHERE at_id=$area ORDER BY at_descricao";
	$query = $db->query($sql);
	if (!$query) {
   		die($db->error);
	}
	$i = 0;
	$row = $query->fetch_object();
	$nome = $row->at_descricao;
?>
	<form name="formTec" method="post">
	<input type="hidden" name="area" value="<?php echo $area; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td colspan="4"> <p> Tecnologias para a �rea tem�tica <strong><?php echo $nome; ?></strong>&nbsp;&nbsp; 
                [ <a href="rel_tecnologia_tema.php?area=<?php echo $area; ?>&novaTecnologia=1" class="Preto">Nova 
                Tecnologia</a> ]</p></td>
          </tr>
          <tr> 
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
<?php
		if (isset($novaTecnologia)) {
?>
          <tr> 
            <td width="86"> <div align="right">Tecnologia:</div></td>
            <td width="458" colspan="3"> <select name="tec" id="select3">
                <?php
			$sql = "SELECT * FROM tecnologia ORDER BY tc_descricao";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->tc_id; ?>"><?php echo trim($row->tc_descricao); ?></option>
                <?php
			} // la�o do loop
?>
              </select> </td>
          </tr>
          <tr> 
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td colspan="4"><div align="center"> 
                <input name="button" type="button" class="botaoLogin" onClick="insereTecnologia();" value="   INSERIR TECNOLOGIA   ">
              </div></td>
          </tr>
          <tr> 
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <?php
		} // fim do que verifica se foi clicado para uma nova tecnologia
		
		$sql = "SELECT * FROM rel_area_tec,tecnologia,area_tematica WHERE rat_id_tc=tc_id AND ".
			   "rat_id_at=at_id AND rat_id_at=$area ORDER BY at_descricao";
		$query = $db->query($sql);
		$i = 0;
		while ($row = $query->fetch_object()) {
?>
          <input type="hidden" name="instID<?php echo $i; ?>2" value="<?php echo trim($row->at_id); ?>">
          <tr> 
            <td  width="86"> <div align="right">Tecnologia:</div></td>
            <td width="458" colspan="3"><strong><?php echo trim($row->tc_descricao); ?></strong> 
              [ <a href="#" onClick="excluiTecnologia('<?php echo $area; ?>','<?php echo trim($row->tc_descricao); ?>','<?php echo trim($row->tc_id); ?>');" class="Preto">Excluir</a> 
              ] </td>
          </tr>
          <?php
			$i++;
		} // la�o do loop
?>
          <tr bgcolor="#000000"> 
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
        <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
		</form>
		<br>
<?php
} // fim do if que verifica se o usuário tem acesso a estas informações
?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
