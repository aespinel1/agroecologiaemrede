<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;

if ($numPerm == 0) {
	echo "<p align=\"center\">Você não tem acesso a estas informações</p>";
	return;
}

$sql = "SELECT * FROM entidade_biologica WHERE eb_familia=$id";
$query = $db->query($sql);
$numRel = $query->num_rows;

$sql = "SELECT * FROM eb_genero WHERE ebg_id_familia=$id";
$query = $db->query($sql);
$numRel += $query->num_rows;


if ($numRel != 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você n�o pode excluir estas informações pois existem dados associados! Clique <a href=\"eb_familias.php\">aqui</a> para voltar para a página anterior.</p>";
	$db->disconnect;
	return;
}

$sql = "DELETE FROM eb_familia WHERE ebf_id=$id";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$db->close();
?>
<script language="JavaScript">
	window.location.href='eb_familias.php';
</script>