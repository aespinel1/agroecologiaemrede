<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
$numPerm  = 1;

include("conexao.inc.php");

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

function submitFormLE(acao) {

	formLE.acao.value = 'cancela';
	formLE.submit();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

</script>
<?php include("menu_admin.php"); ?>

<table>
  <tr>
    <td width="719" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20"  bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42"  bgcolor="#80AAD2"> <p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Coment&aacute;rio  - Cancela Libera&ccedil;&atilde;o/Rejei&ccedil;&atilde;o</strong><b></b></p></td>
          <td width="19"  bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
				</tr>
			</table>
  </tr>
</table>

<?php
			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
				$sql = "select a.*, date_format(dt_comentario,'%d/%m/%Y') as dt_comentario from comentarios a where ind_situacao <> 'N' and ind_tipo='P' ";
				$query = $db->query($sql);
?>
	  <form name="formLE" method="post" action="xt_pesquisa_cancela_liberacao_comentario.php">
	  <input type="hidden" name="acao" value="">
			<div class="table-responsive-md">
        <table id="tabela" class="table table-stripped table-light">
					<thead>
		   <tr>
            <th width="62">&nbsp;</th>
            <th width="162">Nome</th>
            <th width="99">E-mail</th>
            <th width="246">Coment&aacute;rio</th>
            <th width="84">Data Inclusão</th>
            <th width="20">Situação</th>
          </tr>
				</thead>
				<tbody>
          <?php
		  $i = 0;
		  while ($row = $query->fetch_object()) {
?>
          <tr>
            <td> <div align="center">
                <input type="checkbox" name="cod_comentario<?php echo $i; ?>" value="<?php echo $row->cod_comentario; ?>">
              </div></td>
           <?php   $cor = "#99CC99"; ?>


			<td><?php echo $row->txt_nome; ?></td>
			<td><?php echo $row->e_mail; ?></td>
			<td><?php echo	$row->txt_comentario; ?></td>
			<td><?php	echo $row->dt_comentario;  ?></td>
            <?php if ($row->ind_situacao == "R") { ?>
			   <td>Rejeitado</td>
            <?php } else { ?>
               <td>Liberado</td>
            <?php } ?>
          </tr>
          <?php
						$i++;
					}
?>
          <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td colspan="9"> <div align="center">                <input name="Button2" type="button" class="btn btn-primary btn-lg"   value="   Cancelar   " onClick="submitFormLE('cancela');">
            </div></td>
          </tr>
				</tbody>
        </table>
			</div>
	  </form>
<?php
  } // fim do if que verifica se o usuário tem acesso a estas informações

?>
<script>
$(document).ready(function() {
  //$('#tabela').DataTable();
} );
</script>
</body>
</html>
<?php
$db->close();
?>
