<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	die("Você não tem permissão para acessar esta página");
}
include("conexao.inc.php");
$sql = "SELECT * FROM usuario WHERE us_login='".$_COOKIE['cookieUsuario']."' AND us_session='".$_COOKIE['cookieSession']."' AND us_admin='1'";
$row = faz_query($sql, $db, 'object')[0];
$usr = $row->us_login;
$psd = $row->us_senha;
$nom = $row->us_nome;
$adm = $row->us_admin;

$sql = "SELECT count(log_usuario) as qtAcesso FROM log_acesso WHERE log_usuario='".$_COOKIE['cookieUsuario']."'";
$row = faz_query($sql, $db, 'object')[0];
$cnt = $row->qtAcesso;

$sql = "SELECT ex_id, ex_cidade_ref, ex_descricao, ex_anexos FROM frm_exp_base_comum WHERE ex_anexos IS NOT NULL AND (LOCATE('.jpg',ex_anexos)>0 OR LOCATE('.gif',ex_anexos)>0 OR LOCATE('.png',ex_anexos)>0)";
$query = $db->query($sql);
while ($row = $query->fetch_object())
	$universo[]=$row;
$arqs = array();
while(!isset($arqs[0]) || !$arqs[0]) {
	$escolhido = $universo[array_rand($universo)];
	$todos_arqs = explode('|',$escolhido->ex_anexos);
	$arqs=array();
	foreach ($todos_arqs as $arq) {
		if (file_exists($dir['upload'].$arq) && is_array(getimagesize($dir['upload'].$arq))) {
			$arqs[]=$arq;
		}
	}
}

$arq = $arqs[array_rand($arqs)];

$txt_imagem_com_legenda = '<a href="'.$dir['base_URL'].'experiencias.php?experiencia='.$escolhido->ex_id.'">'
	.'<img class="w-100" src="'.$dir['upload_URL'].$arq.'" title="'.$escolhido->ex_descricao.'"><i>'
	.$escolhido->ex_descricao.'</i></a>';


	$sql = "SELECT * FROM mapeo_formularios";
	 $rows = faz_query($sql, $db, 'object');

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<?php include("menu_admin.php"); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-success">
				<h1><i class="oi oi-cog"></i> Área de administração</h2>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-info">
				Bem-vinda/o, <strong><?=$nom?></strong>! Este é o seu acesso de número <strong><?php echo $cnt; ?></strong>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-6 col-lg-4">
			<?=$txt_imagem_com_legenda?>
		</div>
		<div class="col-md-6 col-lg-8">
			<div class="card border-primary">
				<div class="card-header">
					<h3 class="card-title">Tarefas pendentes</h3>
				</div>
				<div class="card-body">
					<h3 class="card-title"><?=$txt['menu_consultar_experiencia']?></h3>
					<?php foreach ($rows as $q) { ?>
				 		<?php if (in_array($usr,explode('|',strtolower($q->usuarias_administradoras))) && substr($q->nomefrm,4,3)=='exp') { ?>
				 			<?php $sql = "SELECT a.ex_liberacao as cod, c.nome, count(*) as num FROM frm_exp_base_comum AS a, ".$q->nomefrm." AS b, lista_cod_liberacao AS c WHERE a.ex_liberacao=c.id AND a.".$campo['frm_exp_base_comum']->id."=b.".$campo[$q->nomefrm]->id." GROUP BY a.ex_liberacao"; ?>
				 			<?php $res = faz_query($sql, $db, 'object'); ?>
				 			<?php if (count($res)) { ?>
				 				<b><?=traduz($q->titulo,$lingua)?></b><br>
				 				<ul class="list-group mb-4">
				 					<?php foreach ($res as $r) { ?>
				 						<?php if (!in_array($r->cod,array('R','S','I'))) { ?>
				 							<a class="list-group-item list-group-item-active" href="../mapeo/?f=lista_formularios&t1_s6=<?=$r->cod?>&nomefrm=<?=$q->nomefrm?>&ord=t1_c5&t1_ordem=DESC">
				 								<?=$r->num.' '.$txt['menu_consultar_experiencia'].' '.strtolower(traduz($r->nome,$lingua))?><?=($r->num>1)?'s':''?>
				 							</a>
			 						 	<?php } ?>
			 					 	<?php } ?>
			 					</ul>
		 			 		 <?php } ?>
			 	   <?php } ?>
			 	 <?php } ?>

				 <h3 class="card-title">Pesquisas</h3>
				 <ul class="list-group mb-4">
					 <?php $found = false; ?>
					 <?php $sql = "SELECT * FROM pesquisa WHERE pe_liberacao='N'"; ?>
					 <?php $num = faz_query($sql, $db, 'num_rows'); ?>
					 <?php if ($num > 0) { ?>
						 <?php $found=true; ?>
						 <a class="list-group-item list-group-active" href="<?=$dir['base_URL']?>pesquisas_libera.php">
							 <b><?=$num?></b> pesquisas a serem liberadas
						 </a>
					 <?php } ?>

					 <?php $sql = "SELECT * FROM pesquisa WHERE pe_liberacao='P'"; ?>
					 <?php $num = faz_query($sql, $db, 'num_rows'); ?>
					 <?php if ($num > 0) { ?>
						 <?php $found=true; ?>
						 <a class="list-group-item list-group-active" href="<?=$dir['base_URL']?>pesquisas_libera.php">
							 <b><?=$num?></b> pesquisas pendentes com as pessoas
						 </a>
					 <?php } ?>

					 <?php $sql = "SELECT * FROM pesquisa WHERE pe_liberacao='E'"; ?>
					 <?php $num = faz_query($sql, $db, 'num_rows'); ?>
					 <?php if ($num > 0) { ?>
						 <?php $found=true; ?>
						 <a class="list-group-item list-group-active" href="<?=$dir['base_URL']?>pesquisas_libera.php">
							 <b><?=$num?></b> pesquisas pendentes aguardando análise
						 </a>
					 <?php } ?>
					 <?php if (!$found) { ?>
						 <a href="#" class="list-group-item disabled">Não há pendências</a>
					 <?php } ?>
				 </ul>

				 <?php $sql = "SELECT * FROM frm_instituicao WHERE in_liberacao='N'"; ?>
				 <?php $num = faz_query($sql, $db, 'num_rows'); ?>
				 <?php if ($num > 0) { ?>
				 	 <h3 class="card-title">Instituições</h3>
					 <ul class="list-group mb-4">
				 	 	 <a class="list-group-item list-group-item-active" href="instituicoes_liberacao.php">
							 <b><?=$num?></b> instituições a serem liberadas
						 </a>
					 </ul>
				 <?php } ?>

				 <?php $sql = "SELECT * FROM usuario WHERE us_liberacao='N'"; ?>
				 <?php $num = faz_query($sql, $db, 'num_rows'); ?>
				 <?php if ($num > 0) { ?>
					 <h3 class="card-title">Usuárias/os</h3>
					 <ul class="list-group mb-4">
						 <a class="list-group-item list-group-item-active" href="usuarios_liberacao.php">
							 <b><?=$num?></b> usuárias/os a serem liberadas/os
						 </a>
					 </ul>
				 <?php } ?>

				 <h3 class="card-title">Comentários</h3>
				 <?php $found = false; ?>
				 <ul class="list-group mb-4">
					 <?php $sql = "SELECT * FROM comentarios WHERE  ind_situacao='N' and ind_tipo = 'E'"; ?>
					 <?php $num = faz_query($sql, $db, 'num_rows'); ?>
					 <?php if ($num > 0) { ?>
						 <?php $found = true; ?>
						 <a class="list-group-item list-group-item-active" href="experiencias_libera_comentario.php">
							 <b><?=$num?></b> comentários de experiências a serem analisados
						 </a>
					 <?php } ?>

					 <?php $sql = "SELECT * FROM comentarios WHERE  ind_situacao='N' and ind_tipo = 'P'"; ?>
					 <?php $num = faz_query($sql, $db, 'num_rows'); ?>
					 <?php if ($num > 0) { ?>
						 <?php $found = true; ?>
						 <a class="list-group-item list-group-item-active" href="pesquisa_libera_comentario.php">
							 <b><?=$num?></b> comentários de pesquisas a serem analisados
					 	 </a>
					 <?php } ?>
					 <?php if (!$found) { ?>
						 <a href="#" class="list-group-item disabled">Não há pendências</a>
					 <?php } ?>
				 </ul>


				</div>
				<div class="card-footer text-center">
					<a href="docs/manual.pdf" class="btn btn-secondary btn-lg"><img src="imagens/ico_pdf.gif" width="32" height="32" border="0">
						Baixar o manual
					</a>
				</div>
			</div>
	</div>

</div>

</body>
</html>
<?php
$db->close();
?>
