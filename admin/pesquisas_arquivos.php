<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040300') AND us_admin=1";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function excluiRelacionamento(arquivo) {
	if(confirm('Você tem certeza que deseja remover o anexo ' + arquivo + '?')) {
		window.location.href='xt_pes_exclui_arquivo.php?idPes=<?php echo $pesquisa; ?>&arquivo='+arquivo;
	}
}

function criticaFormAltera(action) {
	form1.action = action;
	form1.submit();
}
//-->
</script>

<?php include("menu_admin.php"); ?>
<br>
<br>
<div class="container">
	<div class="col-md-12">
<table class="table table-primary">
  <tr>
    <td width="567" valign="top"> <table width="553" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="19" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="41" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="473" bgcolor="#80AAD2"> <p><strong>Pesquisa - Anexos </strong><b></b></p></td>
          <td width="20" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="17" height="32"></div></td>
        </tr>
      </table>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
?>
	  <form name="form1" method="post">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
          <td height="24" colspan="4">
            <p> <strong>&nbsp;</strong></p></td>
          </tr>
          <tr>
            <td colspan="4">
			 Pesquisa:
               <select name="pesquisa">
<?php
                if ($tipo == "NE") {
				   $sql = "SELECT * FROM pesquisa where pe_id = $pesquisa ";
				}
				else {
				  $sql = "SELECT * FROM pesquisa ORDER BY pe_descricao";
				}

				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->pe_descricao);
                    if (strlen(trim($row->pe_descricao)) > 100) {
					$desc = substr(trim($row->pe_descricao),0,100) . " ...";
                    }
					$varSelected = "";
					if (isset($pesquisa) && $pesquisa == $row->pe_id) {
						$varSelected = "selected";
					}
					echo "<option value=\"$row->pe_id\" $varSelected>$desc</option>";
				}
?>
              </select> &nbsp;&nbsp;&nbsp;
			  <?php if ($tipo == "") { ?>
			   <input type="button" class="botaoLogin" onClick="criticaFormAltera('pesquisas_arquivos.php');" value="   Selecionar   ">
			  <?php } ?>
            </td>
          </tr>
		</table>
	  </form>
<?php
	if (isset($pesquisa)) {

		$sql = "SELECT * FROM pesquisa_arquivo WHERE pa_id_pesquisa = $pesquisa";
		$query2 = $db->query($sql);

    $num = $query2->num_rows;

?>

    <form name="form2" enctype="multipart/form-data" method="post" action="xt_pes_grava_arquivo.php">
	<input type="hidden" name="id" value="<?php echo $pesquisa; ?>">
      <table width="567" border="0" align="center" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2">Anexar novo arquivo :
              <input name="arquivo" type="file"   size="50">
              <input name="Submit"  type="submit" value="  Anexar  "> </td>
        </tr>

        <tr>

          <td colspan="2"> <p>
		  <?php

           if ($num == 0) {
				echo "Não há nenhum anexo para esta pesquisa";
			}
		   else {
		        echo "Anexos já cadastrados para esta pesquisa";
			}
	       ?>
	      </td>
        </tr>

<?php

while ($row = $query2->fetch_object()) {

?>
        <tr>
          <td width="20" > <div align="center"><a href="#" onClick="excluiRelacionamento('<?php echo $row->pa_arquivo; ?>');"><img src="imagens/trash.gif" border="0"></a></div></td>
          <td width="547"> <a href="#" onclick="wk = window.open('../upload/arquivos/<?php echo  $row->pa_arquivo; ?>','wk'); wk.focus(); return false;"><?php echo  $row->pa_arquivo; ?> </a> <?php echo " - "; ?>  <?php echo my_filesize("../upload/arquivos/".$row->pa_arquivo); ?>
          </td>
        </tr>
        <?php
}
?>
        </tr>
      </table>
     </form>
<?php
	} // fim do if que verifica se uma experiência foi buscada

} // fim do if que verifica se o usuário tem acesso a estas informações
?>


	  </td>
  </tr>
</table>
</div></div>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
