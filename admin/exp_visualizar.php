<?php
include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040500') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<?php include("menu_admin.php"); ?> 
<br>
<br>

<table width="580" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="570" valign="top"> <table width="558" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="477" bgcolor="#80AAD2"> <p><strong>Detalhes
              da Experi&ecirc;ncia</strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
	  <br>
	  <br>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
	$sql = "SELECT * FROM experiencia WHERE ex_id=$experiencia";
	$query = $db->query($sql);
	$row = $query->fetch_object();

	$sql = "SELECT * FROM experiencia_autor,usuario WHERE exa_id_usuario=us_login AND exa_id_experiencia=$experiencia";
	$queryAutor = $db->query($sql);
	$numAutor = $queryAutor->num_rows;


	$sql = "SELECT * FROM experiencia_relator,usuario WHERE exr_id_usuario=us_login AND exr_id_experiencia=$experiencia";
	$queryRelator = $db->query($sql);
	$numRelator = $queryRelator->num_rows;

	$sql = "SELECT * FROM experiencia_autor,frm_instituicao WHERE exa_id_inst=in_id AND exa_id_experiencia=$experiencia";
	$queryInstAutor = $db->query($sql);
	$numInstAutor = $queryInstAutor->num_rows;


	$sql = "SELECT * FROM experiencia_relator,frm_instituicao WHERE exr_id_inst=in_id AND exr_id_experiencia=$experiencia";
	$queryInstRelator = $db->query($sql);
	$numInstRelator = $queryInstRelator->num_rows;
?>
      <table width="567" border="0">
        <tr>
          <td colspan="2">
		  <table width="561" border="0">
              <tr>
                <td width="280">
                  <?php
		echo "Experiência: <strong>$row->ex_descricao</strong><br>";
		if ($numAutor > 0) {
			echo "<br>Autor(es):<br>";
			while ($rowAutor = $queryAutor->fetch_object()) {
				echo "&nbsp;&nbsp;<strong>$rowAutor->us_nome</strong><br>";
			}
		}
		else {
			echo "<br>Autor(es): <strong>Desconhecido</strong><br>";
		}
		if ($numRelator > 0) {
			echo "<br>Relator(es):<br>";
			while ($rowRelator = $queryRelator->fetch_object()) {
				echo "&nbsp;&nbsp;<strong>$rowRelator->us_nome</strong><br>";
			}
		}
		else {
			echo "<br>Relator(es): <strong>Desconhecido</strong><br><br>";
		}
?>
                  <br>
            [ <a href="javascript:history.back(1);" class="Preto">Voltar</a> ]
			  </td>
	            <td width="280">
                  <?php
		if ($numInstAutor > 0) {
			echo "<br>Instituição(ões) Autor(as):<br>";
			while ($rowInstAutor = $queryInstAutor->fetch_object()) {
				echo "&nbsp;&nbsp;<strong>$rowInstAutor->in_nome</strong><br>";
			}
		}
		else {
			echo "<br>Instituição(ões) Autor(as): <strong>Desconhecido</strong><br>";
		}
		if ($numInstRelator > 0) {
			echo "<br>Instituição(ões) Relator(as):<br>";
			while ($rowInstRelator = $queryInstRelator->fetch_object()) {
				echo "&nbsp;&nbsp;<strong>$rowInstRelator->in_nome</strong><br>";
			}
		}
		else {
			echo "<br>Instituição(ões) Relator(as): <strong>Desconhecido</strong><br><br>";
		}
?>
                </td>
              </tr>
            </table>
			</td>
        </tr>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td width="363">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="363" valign="top"> <?php echo nl2br($row->ex_resumo); ?>
          </td>
          <td width="205" valign="top">
		  <table width="204" border="0" cellspacing="0">
              <tr>
                <td height="1" colspan="2" bgcolor="#006633"> <div align="center"></div></td>
              </tr>
              <tr>
                <td width="10" bgcolor="#006633">&nbsp;</td>
                <td width="186"><strong>&nbsp;Anexos</strong></td>
              </tr>
              <tr>
                <td bgcolor="#006633">&nbsp;</td>
                <td>
                  <?php
	$sql = "SELECT * FROM experiencia_arquivo WHERE ea_id_experiencia=$experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {
		while ($arrayArquivos = $query->fetch_object()) {
			$nomeDoc = trim($arrayArquivos->ea_arquivo);
			echo "&nbsp;<img src=\"imagens/seta02.gif\"> <a href=\"../upload/arquivos/$arrayArquivos->ea_arquivo\" class=\"preto\">$nomeDoc</a><br>";
		}
	}
	else {
		echo "<p class=\"textoPreto10px\">&nbsp;N�o h� registros!</p>";
	} // fim do if que verifica se a busca retornou os resultados
?>
                </td>
              </tr>
            </table>
            <br><table width="204" border="0" cellspacing="0">
              <tr bgcolor="#333333">
                <td height="1" colspan="2"> <div align="center"></div></td>
              </tr>
              <tr>
                <td width="10" bgcolor="#333333">&nbsp;</td>
                <td width="186"><strong>&nbsp;&Aacute;reas
                  Tem&aacute;ticas </strong></td>
              </tr>
              <tr>
                <td bgcolor="#333333">&nbsp;</td>
                <td>
                  <?php
	$sql = "SELECT * FROM rel_area_experiencia,area_tematica WHERE rae_id_area=at_id AND rae_id_experiencia=$experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {
		while ($arrayAreas = $query->fetch_object()) {
			echo "&nbsp;$arrayAreas->at_descricao";
		}
	}
	else {
		echo "<p class=\"textoPreto10px\">&nbsp;N�o h� registros!</p>";
	} // fim do if que verifica se a busca retornou os resultados
?>
                </td>
              </tr>
            </table>
            <br> <table width="204" border="0" cellspacing="0">
              <tr bgcolor="#999999">
                <td height="1" colspan="2"> <div align="center"></div></td>
              </tr>
              <tr>
                <td width="10" bgcolor="#999999">&nbsp;</td>
                <td width="186"><strong>&nbsp;&Aacute;reas
                  Geogr&aacute;ficas </strong></td>
              </tr>
              <tr>
                <td bgcolor="#999999">&nbsp;</td>
                <td>
                  <?php
	$sql = "SELECT * FROM rel_geo_experiencia,area_geografica WHERE rge_id_geo=ag_id AND rge_id_experiencia=$experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {
		while ($arrayGeo = $query->fetch_object()) {
			echo "&nbsp;$arrayGeo->ag_descricao";
		}
	}
	else {
		echo "<p class=\"textoPreto10px\">&nbsp;N�o h� registros!</p>";
	} // fim do if que verifica se a busca retornou os resultados
?>
                </td>
              </tr>
            </table>
            <br> <table width="204" border="0" cellspacing="0">
              <tr bgcolor="#B4B4B4">
                <td height="1" colspan="2"> <div align="center"></div></td>
              </tr>
              <tr>
                <td width="10" bgcolor="#B4B4B4">&nbsp;</td>
                <td width="186"><strong>&nbsp;Habitats</strong></td>
              </tr>
              <tr>
                <td bgcolor="#B4B4B4">&nbsp;</td>
                <td>
                  <?php
	$sql = "SELECT * FROM rel_habitat_experiencia,habitat WHERE rhe_id_habitat=ha_id AND rhe_id_experiencia=$experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {
		while ($arrayHab = $query->fetch_object()) {
			echo "&nbsp;$arrayHab->ha_descricao";
		}
	}
	else {
		echo "<p class=\"textoPreto10px\">&nbsp;N�o h� registros!</p>";
	} // fim do if que verifica se a busca retornou os resultados

} // fim do if que verifica as credenciais do usuário
?>
                </td>
              </tr>
            </table></td>
        </tr>
      </table> </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
