<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");


switch ($tipoTabela = $_REQUEST['tipoTabela']) {
	case 'AG':
		$varTabela = "area_geografica";
		$varCampo1 = "ag_descricao";
		$varCampo2 = "ag_pertence";
		$varID = "ag_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020100') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		
		$sql = "SELECT * FROM rel_geo_experiencia WHERE rge_id_geo=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;
		
		break;
	case 'AT':
		$varTabela = "area_tematica";
		$varCampo1 = "at_descricao";
		$varCampo2 = "at_pertence";
		$varID = "at_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		
		$sql = "SELECT * FROM rel_area_experiencia WHERE rae_id_area=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;
		
		break;
	case 'EB':
		$varTabela = "entidade_biologica";
		$varCampo1 = "eb_descricao";
		$varCampo2 = "eb_id_externo";
		$varCampo3 = "eb_url";
		$varID = "eb_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		
		$sql = "SELECT * FROM rel_entidade_experiencia WHERE ree_id_entidade=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;
		
		break;
	case 'HA':
		$varTabela = "habitat";
		$varCampo1 = "ha_descricao";
		$varID = "ha_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020400') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		
		$sql = "SELECT * FROM rel_habitat_experiencia WHERE rhe_id_habitat=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;
		
		break;
	case 'TE':
		$varTabela = "tecnologia";
		$varCampo1 = "tc_descricao";
		$varCampo2 = "tc_pertence";
		$varID = "tc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020600') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		
		$sql = "SELECT * FROM rel_area_tec WHERE rat_id_tc=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;
		
		break;
	case 'LC':
		$varTabela = "localidade";
		$varCampo1 = "lc_descricao";
		$varID = "lc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020700') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		
		$sql = "SELECT * FROM rel_local_experiencia WHERE rle_id_local=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;
		
		break;
	case 'ZC':
		$varTabela = "zona_climatica";
		$varCampo1 = "zc_descricao";
		$varID = "zc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020800') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		
		$sql = "SELECT * FROM rel_zona_experiencia WHERE rze_id_zona=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;
		
		break;
	case 'VG':
		$varTabela = "vegetacao";
		$varCampo1 = "vg_descricao";
		$varID = "vg_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020900') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		
		$sql = "SELECT * FROM rel_veg_experiencia WHERE rve_id_veg=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;
		
		break;
} // fim do switch

if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você n�o tem acesso a excluir estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página principal.</p>";
	$db->disconnect;
	return;
}

if ($numRel != 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você n�o pode excluir estas informações pois existem dados associados! Clique <a href=\"tabela_complexa.php?tipo=$tipoTabela\">aqui</a> para voltar para a página anterior.</p>";
	$db->disconnect;
	return;
}

$sql = "SELECT $varCampo1 FROM $varTabela WHERE $varID=$id";
$query = $db->query($sql);
$row = $query->fetch_object();
$tempDesc = $row->$varCampo1;

$sql = "DELETE FROM $varTabela WHERE $varID=$id";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$db->close();
?>
<script language="JavaScript">
	window.location.href='tabela_complexa.php?tipo=<?php echo $tipoTabela; ?>&letra=<?php echo substr($tempDesc,0,1); ?>';
</script>