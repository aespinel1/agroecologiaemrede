<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Pertence</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="400" border="0" cellpadding="5">
  <tr> 
    <td bgcolor="#666666" class="textoBranco"><?php echo $genDesc; ?></td>
  </tr>
  <tr> 
    <td>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
?>
      <form name="form1" method="post" action="xt_atualiza_eb_genero.php">
		<input type="hidden" name="id" value="<?php echo $genero; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td colspan="2"><p>Atualizar G�nero</strong></p></td>
          </tr>
          <tr> 
            <td height="1" colspan="10" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td width="81"><div align="right">Descri&ccedil;&atilde;o:</div></td>
            <td width="483"><?php echo $genDesc; ?></td>
          </tr>
<?php 
		$sql = "SELECT * FROM eb_familia ORDER BY ebf_familia";
		$query = $db->query($sql);		  
?>
          <tr> 
            <td><div align="right">Fam�lia:</div></td>
            <td>
			  <select name="familia">
			  	<option value="">-- Selecione --</option>
<?php
		while ($row = $query->fetch_object()) {
			$varSelected = "";
			if ($row->ebf_id == $familia) {
				$varSelected = "selected";
			}
?>
			    <option <?php echo $varSelected; ?> value="<?php echo $row->ebf_id; ?>"><?php echo trim($row->ebf_familia); ?></option>
<?php
		}
?>
              </select> </td>
          </tr>
          <tr> 
            <td>&nbsp;</td>
            <td><input name="enviar" type="submit" class="botaoLogin" value="Gravar"></td>
          </tr>
        </table>
      </form>
<?php
} // fim do if que verifica as credenciais
?>
	</td>
  </tr>
</table>
</body>
</html>
