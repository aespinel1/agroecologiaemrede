<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$sql = "SELECT * FROM usuario WHERE us_login='$cookieUsuario' AND us_session='$cookieSession' AND us_admin=1";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040100') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;

$sql = "SELECT * FROM texto WHERE tex_id='EXPLICACAO_CTRL'";
$query = $db->query($sql);
$rowMsg = $query->fetch_object();


$sql = "SELECT * FROM texto WHERE tex_id='EXPLICACAO_RESUMO'";
$query = $db->query($sql);
$rowRes = $query->fetch_object();

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function criticaFormExp() {

	formNE.submit();
}

function SelecionaOutra() {
    window.location.href="experiencia_altera.php"
	formNE.submit();
}
function Tecla(e)
{
if(document.all) // Internet Explorer
var tecla = event.keyCode;

else if(document.layers) // Nestcape
var tecla = e.which;


if(tecla > 47 && tecla < 58) // numeros de 0 a 9
return true;
else
{
if (tecla != 8) // backspace
return false;
else
return true;
}
}

</script>

<?php include("menu_admin.php"); ?>

<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>

    <td width="567" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Experi&ecirc;ncia - Alteração</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>

	  <?php if (isset($experiencia)) {
	        $sql = "select a.*, date_format(dt_criacao,'%d/%m/%Y') as data_inclusao , date_format(dt_atualizacao,'%d/%m/%Y') as data_alteracao   from experiencia a where ex_id=$experiencia";
			$query = $db->query($sql);
			$rowExp = $query->fetch_object();
         }

		 if ($numPerm == 0) {
			echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
		 }
		else {
?>
<form name="formNE" method="post" action="xt_exp_atualiza_experiencia.php">
      <table width="567" border="0" cellpadding="5" cellspacing="1" align="center">
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr bgcolor="#FFFFCC">
          <td colspan="4" align="center"><span class="style5"></span>Data inclusão: <?php echo $rowExp->data_inclusao; ?> &nbsp;&nbsp; Data �ltima alteração : <?php echo $rowExp->data_alteracao; ?></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr>
          <td >
            <div align="right"><strong>Identifica��o</strong>:</div></td>
          <td width="363" colspan="3"><input name="experiencia" type="text" id="experiencia" size="20" maxlength="20" value="<?php echo $experiencia ?>" readonly></td>
        </tr>
        <tr>
          <td >
            <div align="right"><strong>T&iacute;tulo</strong>:</div></td>
          <td width="363" colspan="3"><input name="nome" type="text" id="experiencia" size="69" maxlength="255"  value="<?php echo trim($rowExp->ex_descricao); ?>"></td>
        </tr>
        <tr>
          <td><div align="right"><strong>Chamada</strong>:</div></td>
          <td colspan="3"><input name="chamada" type="text" id="chamada2" size="69" maxlength="70" value="<?php echo trim($rowExp->ex_chamada); ?>"></td>
        </tr>
         <tr>
            <td><div align="right"><strong>Ano Publicação</strong>:</div></td>
            <td colspan="3"><input name="ano_publicacao" type="text" id="ano_publicacao" size="6" maxlength="4" value="<?php echo trim($rowExp->ano_publicacao); ?>" onKeyPress="return Tecla(event);"></td>
          </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr bgcolor="#FFFFCC">
          <td colspan="4" align="center"><strong>Autor(es)</strong><br>
              <br><?php echo $rowMsg->tex_texto; ?></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr>
          <td valign="top">
            <div align="right"><strong>Pessoa(s)</strong>:<br>
                <br>
          </div></td>
          <td colspan="3"><select name="autor[]" size="6" multiple id="select2">
              <?php
		          $sql = "SELECT us_login,us_nome FROM usuario  ORDER BY us_nome";
		          $query = $db->query($sql);
		          while ($row = $query->fetch_object()) {

				  $sql = "select * from experiencia_autor where exa_id_usuario = '$row->us_login' and  exa_id_experiencia =$experiencia";
				  $queryA = $db->query($sql);
				  $qtd = $queryA->num_rows;

				  if ($qtd == 0) {
                ?>
              <option value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
              <?php
				  }
				  else {
                ?>
              <option selected value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
              <?php
				  }

		          } // la�o do loop
               ?>
            </select>
          </td>
        </tr>
        <tr>
          <td valign="top"><div align="right"><strong>Institui&ccedil;&atilde;o(&otilde;es)</strong>:</div></td>
          <td colspan="3"><select name="instAutor[]" size="6" multiple id="select3">
              <?php
		$sql = "SELECT in_id,in_nome FROM frm_instituicao  ORDER BY in_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {


		 $sql = "select * from experiencia_autor where exa_id_inst = '$row->in_id' and  exa_id_experiencia =$experiencia";
		 $queryA = $db->query($sql);
		 $qtd = $queryA->num_rows;

		 if ($qtd == 0) { ?>
              <option value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
              <?php }
		 else { ?>
              <option selected value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
              <?php
	     }
		} // la�o do loop
?>
          </select></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr bgcolor="#FFFFCC">
          <td colspan="4" align="center"><strong>Relator(es)</strong><br>
              <br><?php echo $rowMsg->tex_texto; ?></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr>
          <td valign="top">
            <div align="right"><strong>Pessoa(s)</strong>:<br>
                <br>
          </div></td>
          <td colspan="3"><select name="relator[]" size="6" multiple id="select4">
              <?php
		$sql = "SELECT * FROM usuario  ORDER BY us_nome";
		$query = $db->query($sql);

		while ($row = $query->fetch_object()) {

		   $sql = "select * from experiencia_relator where exr_id_usuario = '$row->us_login' and  exr_id_experiencia =$experiencia";
		   $queryA = $db->query($sql);
		   $qtd = $queryA->num_rows;

		   if ($qtd == 0) { ?>
              <option value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
              <?php }
		 else { ?>
              <option selected value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
              <?php
	     }
		} // la�o do loop
?>
            </select>
          </td>
        </tr>
        <tr>
          <td valign="top"><div align="right"><strong>Institui&ccedil;&atilde;o(&otilde;es)</strong>:</div></td>
          <td colspan="3"><select name="instRelator[]" size="6" multiple id="select5">
              <?php
		$sql = "SELECT in_id,in_nome FROM frm_instituicao ORDER BY in_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {

		   $sql = "select * from experiencia_relator where exr_id_inst = '$row->in_id' and  exr_id_experiencia =$experiencia";
		   $queryA = $db->query($sql);
		   $qtd = $queryA->num_rows;

		   if ($qtd == 0) { ?>
              <option value="<?php echo trim($row->in_id); ?>"><?php echo trim($row->in_nome); ?></option>
              <?php }
		 else { ?>
              <option selected value="<?php echo trim($row->in_id); ?>"><?php echo trim($row->in_nome); ?></option>
              <?php
	     }

		} // la�o do loop
?>
          </select></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr bgcolor="#FFFFCC">
          <td colspan="4" align="center"><strong>Resumo</strong><br>
              <br><?php echo $rowRes->tex_texto; ?></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr>
          <td valign="top">
            <div align="right"></div></td>
          <td colspan="3"><textarea name="resumo" cols="87" rows="20" id="textarea"><?php echo trim($rowExp->ex_resumo); ?></textarea></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr bgcolor="#FFFFCC">
          <td colspan="4" align="center"><strong>Crit&eacute;rios para busca </strong></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr>
          <td valign="top"><div align="right"><strong>&Aacute;reas Geogr&aacute;ficas</strong>:<br>
                        <br>
          </div></td>
          <td colspan="3" valign="top" ><a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_ag_nav.php?url=experiencias_cadastro.php','at','scrollbars=yes,width=565,height=450,status=yes')"> Clique aqui para adicionar</a>&nbsp;&nbsp; <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_exclui_ag.php?url=experiencias_cadastro.php','at','scrollbars=yes,width=300,height=250,status=yes')">ou aqui para retirar da rela��o</a>
              <textarea name="areasGeograficas" cols="65" rows="5" wrap="VIRTUAL" readonly></textarea> <br>
              <?php
			     $sql = "select b.ag_id, b.ag_descricao from rel_geo_experiencia a, area_geografica b where a.rge_id_geo =  b.ag_id and a.rge_id_experiencia = $experiencia order by b.ag_descricao";
				 $query = $db->query($sql);
				 $CagID = "";
		         while ($row = $query->fetch_object()) {
				       $CagID = $CagID . $row->ag_id . ";";
?>
              <script language="JavaScript">
		     formNE.areasGeograficas.value = formNE.areasGeograficas.value + '<?php echo $row->ag_descricao; ?>\n';
	      </script>
              <?php
		         }
			  ?>

              <span style="font-weight:bold">Latitude: <input type="text" name="latitude" value="<?php echo $rowExp->lat; ?>"></span><br>
              <span style="font-weight:bold">Longitude: <input type="text" name="longitude" value="<?php echo $rowExp->lng; ?>"></span>
              <input   type="hidden" name="agID" value="<?php echo $CagID; ?>">
          </td>
        </tr>
        <tr>
          <td valign="top"><div align="right"><strong>&Aacute;reas Tem&aacute;ticas</strong>:<br>
                        <br>
          </div></td>
          <td colspan="3" valign="top"> <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_at_nav.php?url=experiencias_cadastro.php','at','scrollbars=yes,width=565,height=450,status=yes')">Clique aqui para relacionar</a>&nbsp;&nbsp; <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_exclui_at.php?url=experiencias_cadastro.php','at','scrollbars=yes,width=300,height=250,status=yes')">ou aqui para retirar da rela��o</a>
                    <textarea name="areasTematicas" cols="65" rows="5" wrap="VIRTUAL" readonly></textarea>
                    <?php
			     $sql = "select b.at_id, b.at_descricao  from rel_area_experiencia a, area_tematica b where a.rae_id_area =  b.at_id and a.rae_id_experiencia = $experiencia order by b.at_descricao";
				 $query = $db->query($sql);
				 $CatID = "";
		         while ($row = $query->fetch_object()) {
				       $CatID = $CatID . $row->at_id . ";";
?>
                    <script language="JavaScript">
		     formNE.areasTematicas.value = formNE.areasTematicas.value + '<?php echo $row->at_descricao; ?>\n';
	          </script>
                    <?php
		         }
			  ?>
                    <input type="hidden" name="atID" value="<?php echo $CatID; ?>">
                    <br>
          </td>
        </tr>
        <tr>
          <td valign="top"><div align="right"><strong>Biomas</strong>:</div></td>
          <!--
			<td colspan="3" valign="top">
			<a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_biomas.php?url=experiencias.php','bi','scrollbars=yes,width=517,height=300,status=yes')">Clique
              aqui para adicionar<strong> Biomas</strong></a><br>
              <textarea name="biomas" cols="50" rows="5" wrap="VIRTUAL" readonly></textarea>
              <input type="hidden" name="biomaID"> </td>-->
          <td colspan="3" valign="top">
            <?php
				$sql = "SELECT * FROM habitat ORDER BY ha_descricao";
				$query = $db->query($sql);
				echo "<select name=\"biomas\" class=\"inputFlat\">\n";
                echo "<option value=''>--- selecione ---</option>\n";
				while ($row = $query->fetch_object()) {

					$sql = "select * from rel_habitat_experiencia where rhe_id_habitat = '$row->ha_id' and  rhe_id_experiencia =$experiencia";
		            $queryA = $db->query($sql);
		            $qtd = $queryA->num_rows;

		            if ($qtd == 0) {
					   echo "<option value=\"$row->ha_id\">$row->ha_descricao</option>\n";
				    }
					else {
					   echo "<option selected value=\"$row->ha_id\">$row->ha_descricao</option>\n";
					}
				}
?>
          </td>
        </tr>
 	      <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		    <tr bgcolor="#FFFFCC">
            <td colspan="4" align="center"><strong>Coment&aacute;rios e Sugest&otilde;es</strong></td>
          </tr>
		     <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td valign="top"> <div align="right"><br>
                <br>
                </div></td>

            <td colspan="3"><textarea name="comentario" cols="85" rows="20" id="resumo"><?php echo trim($rowExp->txt_comentario); ?></textarea></td>
          </tr>
		  <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>

        <tr>
          <td colspan="4"><div align="center">
              <input name="button" type="button" class="botaoLogin" onClick="criticaFormExp();" value="   Alterar   ">
			  &nbsp;&nbsp;<input name="button" type="button" class="botaoLogin" onClick="SelecionaOutra();" value="   Seleciona outra   ">
          </div></td>
        </tr>
        <tr bgcolor="#000000">
          <td height="1" colspan="4">
            <div align="center"></div></td>
        </tr>
      </table>
</form>
<?php
		}
?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
