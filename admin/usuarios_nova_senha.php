<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='030000' OR fu_id='030102') AND us_admin='1' ";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaFormUsuario() {

	nome = formCU.nome.value;
	email = formCU.email.value;
	senha = formCU.senha.value;
	senhaConf = formCU.senhaConf.value;

	if (nome.length == 0) {
		alert('Preencha um nome v�lido para o usuário!');
		formCU.nome.focus();
		return;
	}

	if (senha.length > 0) {
		if (senha != senhaConf) {
			alert('A senha n�o confere! Por favor, digite e confirme a senha do usuário!');
			formCU.senha.value = '';
			formCU.senhaConf.value = '';
			formCU.senha.focus();
			return;
		}
	}

	if (email.length == 0) {
		alert('Preencha um e-mail v�lido para o usuário!');
		formCU.email.focus();
		return;
	}

	formCU.submit();

}
</script>
</head>
<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr>
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57">
      <div align="center"><a class="Branco" href="xt_logout.php"><img src="imagens/ico_logoff.gif" border="0"><br><br>
        <strong>LOGOUT</strong></a></div>
	</td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td width="173" valign="top">
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_usuarios.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Gerenciamento
              de Usu&aacute;rios</strong><b>
              </b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr>
          <td colspan="2"><p><strong>Op��es</strong></p></td>
        </tr>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td width="283"><img src="imagens/seta02.gif" width="12" height="13">
            <a href="usuarios.php?tipo=NU" class="Preto">Cadastro de Experimentadores
            e Usu&aacute;rios</a><br>
            <!--
			<br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="usuarios.php?tipo=CF" class="Preto">Cadastrar
            fun&ccedil;&atilde;o</a><br>-->
            <br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="usuarios.php?tipo=LU" class="Preto">Liberar
            Experimentadores e Usu&aacute;rios </a> </td>
          <td width="283" valign="top"><img src="imagens/seta02.gif" width="12" height="13">
            <a href="usuarios.php?tipo=DP" class="Preto">Definir permiss&otilde;es</a><br>
            <!--
			<br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="tabela_simples?tipoTabela=GU" class="Preto">Grupos
            de usu&aacute;rios</a>--></td>
        </tr>
      </table>
      <br>
      <br>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
?>
		<form name="form1" method="post" action="usuarios_nova_senha.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2"> <p> <strong>Selecione o usuário</strong>
                &nbsp;&nbsp;[ <a href="usuarios_cadastro.php" class="Preto">Alterar
                dados cadastrais</a> ]</p></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="57"> <div align="center">Usu&aacute;rio:</div></td>
            <td width="487"> <select name="usuario">
<?php
		$sql = "SELECT * FROM usuario ORDER BY us_login";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
			$varSelected = "";
			if ($usuario == trim($row->us_login)) {
				$varSelected = "selected";
			}
?>
                <option <?php echo $varSelected; ?> value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_login); ?></option>
                <?php
		} // la�o do loop
?>
              </select>
              <input name="Submit" type="submit" class="botaoLogin" value="   Buscar   ">
            </td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="2"> <div align="center"></div></td>
          </tr>
        </table>
		</form>

<?php
		if (isset($usuario)) {
			$sql = "SELECT * FROM usuario WHERE us_login='$usuario'";
			$query = $db->query($sql);
			$rowUSR = $query->fetch_object();
?>
      <form name="formCU" method="post" action="xt_usuario_envia_senha.php">
	  <input type="hidden" name="login" value="<?php echo trim($rowUSR->us_login); ?>">
	  <input type="hidden" name="email" value="<?php echo trim($rowUSR->us_email); ?>">
	  <input type="hidden" name="nome" value="<?php echo trim($rowUSR->us_nome); ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="4"> <p> <strong>Dados do usu&aacute;rio</strong>
                <?php if (isset($atualiza)) { ?>
                - um e-mail com uma nova senha foi enviada para o usuário <strong><?php echo $usuario; ?></strong>!
                <?php } ?>
              </p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="72"> <div align="right">Nome:</div></td>
            <td width="120" colspan="3"><strong><?php echo $rowUSR->us_nome; ?></strong></td>
          </tr>
          <tr>
            <td> <div align="right">Login:</div></td>
            <td width="120"><strong><?php echo $rowUSR->us_login; ?></strong></td>
            <td width="103"> <div align="right">E-mail:</div></td>
            <td width="120"><strong><?php echo $rowUSR->us_email; ?></strong></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input type="submit" class="botaoLogin" value="   ENVIAR NOVA SENHA   ">
              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
	  </form>
<?php
		} // fim do if que verifica se o usuário j� foi selecionado

} // fim do if que verifica se o usuário tem acesso a estas informações
?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
