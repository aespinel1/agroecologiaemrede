<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");


switch ($tipoTabela = $_REQUEST['tipoTabela']) {
	case 'AG':
		$varTabela = "area_geografica";
		$varCampo1 = "ag_descricao";
		$varCampo2 = "ag_pertence";
		$varID = "ag_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020100') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'AT':
		$varTabela = "area_tematica";
		$varCampo1 = "at_descricao";
		$varCampo2 = "at_pertence";
		$varID = "at_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'EB':
		$varTabela = "entidade_biologica";
		$varCampo1 = "eb_descricao";
		$varCampo2 = "eb_id_externo";
		$varCampo3 = "eb_url";
		$varID = "eb_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'HA':
		$varTabela = "habitat";
		$varCampo1 = "ha_descricao";
		$varID = "ha_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020400') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'TE':
		$varTabela = "tecnologia";
		$varCampo1 = "tc_descricao";
		$varCampo2 = "tc_pertence";
		$varID = "tc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020600') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
	case 'LC':
		$varTabela = "localidade";
		$varCampo1 = "lc_descricao";
		$varID = "lc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020700') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'VG':
		$varTabela = "vegetacao";
		$varCampo1 = "vg_descricao";
		$varID = "vg_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020900') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
	case 'ZC':
		$varTabela = "zona_climatica";
		$varCampo1 = "zc_descricao";
		$varID = "zc_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020800') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		break;
} // fim do switch
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você n�o tem acesso a inserir estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página princiapal.</p>";
	$db->disconnect;
	return;
}

if ($tipoTabela == 'AG' || $tipoTabela == 'AT' || $tipoTabela == 'TE') { 
	if (strlen($pertence) == 0) {
		$sql = "INSERT INTO $varTabela ($varCampo1,$varCampo2) VALUES ('$descricao',NULL)";
		?>
		<script language="JavaScript">
	      window.location.href='tabela_complexa.php?tipo=<?php echo $tipoTabela; ?>&erro=Falta%20informar%20onde%20pertence.';
       </script>
		<?php
	}
	else {
		$sql = "INSERT INTO $varTabela ($varCampo1,$varCampo2) VALUES ('$descricao',$pertence)";
	}
	$query = $db->query($sql);
	if (!$query) {
	   die($db->error);
	}
	$db->close();
}
else if ($tipoTabela == 'EB') {
	if (strlen($idExterno) == 0) {
		$sql = "INSERT INTO $varTabela ($varCampo1,$varCampo2,$varCampo3) VALUES ('$descricao',NULL,'$url')";
	}
	else {
		$sql = "INSERT INTO $varTabela ($varCampo1,$varCampo2,$varCampo3) VALUES ('$descricao',$idExterno,'$url')";
	}
	$query = $db->query($sql);
	if (!$query) {
	   die($db->error);
	}
	$db->close();
}
else if ($tipoTabela == 'HA') {
	$sql = "INSERT INTO $varTabela ($varCampo1) VALUES ('$descricao')";
	$query = $db->query($sql);
	if (!$query) {
	   die($db->error);
	}
	$db->close();
}
else {
	$sql = "INSERT INTO $varTabela ($varCampo1) VALUES ('$descricao')";
	$query = $db->query($sql);
	if (!$query) {
	   die($db->error);
	}
	$db->close();
}
?>
<script language="JavaScript">
	window.location.href='tabela_complexa.php?tipo=<?php echo $tipoTabela; ?>&letra=<?php echo substr($descricao,0,1); ?>';
</script>