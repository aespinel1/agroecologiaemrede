<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040200')";
$query = $db->query($sql);
$numPerm = $query->num_rows;
		
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página principal.</p>";
	$db->disconnect;
	return;
}

switch ($tipo) {
	case 'AG':
		$varTabelaRel = "rel_geo_experiencia";
		$varCampoRel = "rge_id_geo";
		$varIDRel = "rge_id_experiencia";
		break;
	case 'AT':
		$varTabelaRel = "rel_area_experiencia";
		$varCampoRel = "rae_id_area";
		$varIDRel = "rae_id_experiencia";
		break;
	case 'IN':
		$varTabelaRel = "rel_inst_experiencia";
		$varCampoRel = "rie_id_inst";
		$varIDRel = "rie_id_experiencia";
		break;
	case 'HA':
		$varTabelaRel = "rel_habitat_experiencia";
		$varCampoRel = "rhe_id_habitat";
		$varIDRel = "rhe_id_experiencia";
		break;
	case 'EX':
		$varTabelaRel = "rel_usuario_experiencia";
		$varCampoRel = "rue_id_usuario";
		$varIDRel = "rue_id_experiencia";
		break;
	case 'BM':
		$varTabelaRel = "rel_habitat_experiencia";
		$varCampoRel = "rhe_id_habitat";
		$varIDRel = "rhe_id_experiencia";
		break;
} // fim do switch

if ($tipo == "EX") {
	$sql = "DELETE FROM $varTabelaRel WHERE $varCampoRel='$idRel' AND $varIDRel=$idExp";
}
else {
	$sql = "DELETE FROM $varTabelaRel WHERE $varCampoRel=$idRel AND $varIDRel=$idExp";
}
$query = $db->query($sql);
if (!$query) {
	die($db->error);
}

$db->close();
?>
<script language="JavaScript">
	window.location.href='exp_popup_relacionamentos.php?tipo=<?php echo $tipo; ?>&id=<?php echo $idExp; ?>&nome=<?php echo trim($nome); ?>&relacionamentoExcluido=1';
</script>