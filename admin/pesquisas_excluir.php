<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM usuario WHERE us_login='$cookieUsuario' AND us_session='$cookieSession' AND us_admin=1";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040600') AND us_admin='1' ";
$query = $db->query($sql);
$numPerm = $query->num_rows;

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

function criticaFormExp() {
	desc = formNE.nome.value;
	cham = formNE.chamada.value;
	if (desc.length == 0) {
		alert('Digite um t�tulo v�lido para a pesquisa !');
		formNE.nome.focus();
		return;
	}
	if (cham.length == 0) {
		alert('Digite uma chamada v�lida para a pesquisa !');
		formNE.chamada.focus();
		return;
	}
	formNE.submit();
}


function criticaFormAtualiza() {
	nome = formAtualiza.nome.value;
	cham = formAtualiza.chamada.value;
	if (nome.length == 0) {
		alert('Digite um nome v�lido para a experiência!');
		formAtuliza.nome.focus();
		return;
	}
	if (cham.length == 0) {
		alert('Digite uma chamada v�lida para a experiência!');
		formAtuliza.chamada.focus();
		return;
	}

	formAtualiza.submit();
}


function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


</script>
<style type="text/css">
<!--
.style1 {
	color: #006666;
	font-weight: bold;
}
-->
</style>
<?php include("menu_admin.php"); ?>
<br>
<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>

    <td width="567" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Pesquisas</strong><b> - Exclus&atilde;o </b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
<br>
<?php
			if ($numPerm == 0 and $numPerm = 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações !</p>";
			}
			else {

				$sql = "SELECT * FROM pesquisa,usuario WHERE pe_usuario=us_login";
				$query = $db->query($sql);

				if (isset($pesExcluida)) {
					echo "<p class=\"textoPreto10px\" align=\"center\"><b>Pesquisa exclu�da com sucesso !</b></p>";
				}
?>
	  <form name="formExclui" method="post" action="pes_exclui_pesquisa.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td height="24" colspan="4"> <p> <strong>Excluir Pesquisa </strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"> Pesquisa :
              <select name="pesquisa" id="experiencia">
                <?php
				$sql = "SELECT * FROM pesquisa ORDER BY pe_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->pe_descricao);
                    if (strlen(trim($row->pe_descricao)) > 100) {
					$desc = substr(trim($row->pe_descricao),0,100) . " ...";
                    }
					$varSelected = "";
					if (isset($pesquisa) && $pesquisa == $row->pe_id) {
						$varSelected = "selected";
					}
					echo "<option value=\"$row->pe_id\" $varSelected>$desc</option>";
				}
?>
              </select> &nbsp;&nbsp;&nbsp; <input name="Submit" type="submit" class="botaoLogin" value="   Excluir   ">
            </td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
        </table>
      </form>
<?php
	} // fim do if que verifica se o usuário tem acesso a estas informações

?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
