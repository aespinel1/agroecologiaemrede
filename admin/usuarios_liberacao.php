<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$varDesc = "Liberação";
$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='030000' OR fu_id='030500') AND us_admin='1' ";
$query = $db->query($sql);
$numPerm = $query->num_rows;


?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

function criticaFormFuncao() {
	cod = form1.cod.value;
	des = form1.desc.value;

	if (cod.length == 0) {
		alert('Digite um c�digo v�lido!');
		form1.cod.focus();
		return;
	}

	if (des.length == 0) {
		alert('Digite uma descrição v�lida!');
		form1.desc.focus();
		return;
	}

	form1.submit();
}

var ordem = new Array();
var i=1;
function inserir(func,descricao){
	var temp1 = func.substring(0,2);
	var temp2 = func.substring(2,6);
	if(temp2 == '0000'){
		for(h=1 ; h<=i-1 ; h++){
			if(ordem[h] != 0){
				var a = ordem[h].substring(0,2);
				var b = ordem[h].substring(2,6);
				if((a == temp1) && (b != '0000')){
					ordem[h] = 0;
				}
			}
		}
		desabilitar(temp1,descricao);
	}
	var p=1;
	var flag = true;
	while(ordem[p] != null){
		if(func != ordem[p]){
			p++;
		}
		else{
			ordem[p]=0;
			flag = false;
			break;
		}
	}
	if(flag){
		ordem[i] = func;
		i++;
	}
}
function mostrar(){
	for(j=1 ; j<=i-1 ; j++){
		if(ordem[j] != 0){
		    //alert('seq.valor= '+ordem[j]);
   			document.form2.sequencia.value = document.form2.sequencia.value + ordem[j] + " ";
		}
	}
	if(document.form2.sequencia.value == ''){
		if(!confirm('Nenhuma permiss�o foi selecionada! Deseja dar permiss�es a este Experimentador?')){
			window.location.href = 'usuarios.php?tipo=DP';
		}
	}
	else{
		var seq = document.form2.sequencia.value;
		if(confirm('Deseja salvar as novas permiss�es deste Experimentador?')){
			window.location.href = 'xt_usuario_atualiza_permissao.php?sequencia=' + seq + '&usuario=<?php echo $usuario; ?>';
		}
		else{
			document.form2.sequencia.value = '';
			//alert('seq= '+document.form1.sequencia.value);
		}
	}
}
function desabilitar(funcao,descricao){
 	var marcado = true;
	var str = 'window.document.form2.'+descricao;
	var tempobj = eval(str);
	if(tempobj.checked == 1) {
		marcado = false;
	}
	var cont = document.form2.elements.length;
	for(k=0; k<cont; k++){
		var cod1 = document.form2.elements[k].value;
		var cod2 = cod1.substring(0,2);
		var cod3 = cod1.substring(2,6);
		if(!marcado){
			if(cod2 == funcao){
				document.form2.elements[k].checked = 1;
				if(cod3 != '0000'){
					document.form2.elements[k].disabled = 1;
				}
			}
		}
		else{
			if(cod2 == funcao){
				document.form2.elements[k].checked = 0;
				if(cod3 != '0000'){
					document.form2.elements[k].disabled = 0;
				}
			}
		}
   	}
}

function criticaFormUsuario() {

	nome = formNE.nome.value;
	login = formNE.login.value;
	email = formNE.email.value;
	senha = formNE.senha.value;
	senhaConf = formNE.senhaConf.value;
	grupo = formNE.grupo.value;
	ag = formNE.agID.value;
	at = formNE.atID.value;

	if (nome.length == 0) {
		alert('Preencha um nome v�lido para a pessoa !');
		formNE.nome.focus();
		return;
	}



	if (senha != senhaConf) {
		alert('A senha n�o confere! Por favor, digite e confirme a senha do Experimentador!');
		formNE.senha.value = '';
		formNE.senhaConf.value = '';
		formNE.senha.focus();
		return;
	}


	if (ag.length == 0) {
		alert('Selecione pelo menos uma �rea Geogr�fica do seu interesse !');
		formNE.areasGeograficas.focus();
		return;
	}

	if (at.length == 0) {
		alert('Selecione pelo menos uma �rea Tem�tica do seu interesse !');
		formNE.areasTematicas.focus();
		return;
	}

	formNE.submit();
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function marca_desmarca_tabela () {
if (document.formNE.admin.checked == true) {
   var el = document.all['dados_especiais']
   el.style.display = "";
   }
else
   {
    var el = document.all['dados_especiais'];
    el.style.display = "none";
   }
}
</script>

<?php include("menu_admin.php"); ?>

<div class="container">
	<div class="row">
		<div class="col-md-12">
    <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_usuarios.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><b>Pessoas</b><b> - <?php echo $varDesc ?></b></p>          </td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
    </table>
<?php
		if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
				$orderby = ($_REQUEST['orderby'])
					? $_REQUEST['orderby']
					: 'us_nome';
				if ($_REQUEST['desc'])
					$orderby.=" DESC";
				$sql = "SELECT *, UNIX_TIMESTAMP(dt_criacao) as data FROM usuario WHERE us_liberacao='N' order by ".$orderby;
				$query = $db->query($sql);
?>
	  <form name="formLULibera" method="post" action="xt_usu_libera_usuario.php">
				<div class="table-responsive-md">
				<table class="table table-striped table-light">
          <tr>
          <td height="24" colspan="5" style="border-top:none">
				<div style="float:right">
				ordenar por: <a href="usuarios_liberacao.php?orderby=us_nome">nome</a> | <a href="usuarios_liberacao.php?orderby=dt_criacao&desc=1">data (decrescente)</a> | <a href="usuarios_liberacao.php?orderby=us_login">login</a>
				</div>
			<strong>Liberar </strong>
                <?php if (isset($usuLibera)) { ?>
                - pessoa(s): realizado com sucesso !
                <?php } ?>
			</td>
          </tr>
<?php
					$i = 0;
					while ($row = $query->fetch_object()) {
?>
          <tr>
            <td class="text-truncate" style="max-width:200px"><div align="center"><input type="checkbox" name="usu<?php echo $i; ?>" value="<?php echo $row->us_login; ?>"></div></td>
			<td class="text-truncate" style="max-width:200px"><a href="usuarios_cadastro.php?usuario=<?=$row->us_login?>"><?=$row->us_nome?></a></td>
			<td class="text-truncate" style="max-width:200px"><?=$row->us_email?></td>
			<td class="text-truncate" style="max-width:200px"><?=$row->us_login?></td>
			<td class="text-truncate" style="max-width:200px"<?=date('d/m/Y',$row->data)?></td>
          </tr>
<?php
						$i++;
					}
?>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td colspan="5"><div align="center">
                <input class="btn btn-primary btm-lg" type="submit" class="botaoLogin" value="Liberar">
              </div></td>
          </tr>
		</table></div>
	  </form>
<?php
			} // fim do if que verifica se o Experimentador tem acesso a estas informações

?>
</div></div></div>
</body>
</html>
<?php
$db->close();
?>
