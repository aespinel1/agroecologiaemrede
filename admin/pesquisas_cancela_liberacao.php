<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM usuario WHERE us_login='$cookieUsuario' AND us_session='$cookieSession' AND us_admin=1";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040500') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">


function submitFormLE(acao) {
	if (acao == 'libera') {
		formLE.acao.value = 'libera';
	}
	else if (acao == 'pendente') {
		formLE.acao.value = 'pendente';
	}
	else {
		formLE.acao.value = 'cancela';
	}

	formLE.submit();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


</script>
<style type="text/css">
<!--
.style1 {
	color: #006666;
	font-weight: bold;
}
.style2 {color: #FF0000}
-->
</style>
<?php include("menu_admin.php"); ?> 
<br>
<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="567" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Pesquisas - Cancelamento Libera��o / Rejei��o</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
  </tr>
</table>
 <br>
<?php
			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
				$sql = "select * from pesquisa where pe_liberacao  in ('S','R')  order by pe_data_proc desc limit 0 ,100 ";
				$query = $db->query($sql);
?>
	  <form name="formLE" method="post" action="xt_pes_cancela_libera_pesquisa.php">
	  <input type="hidden" name="acao" value="">
        <table width="567" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td height="24" colspan="5"> <p> <span class="style1">Cancelamento da Libera��o/Rejei��o das
                Pesquisas</span>
                <?php if (isset($expLibera)) { ?>
                <span class="style1">- realizado com sucesso !</span>
                <?php } ?>
            </p>
              <p class="textoPreto10px style2">Aten&ccedil;&atilde;o: Ser&atilde;o exibidas as &uacute;ltimas 100 liberadas/rejeitadas em ordem decrescente de data </p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000" colspan="5"> <div align="center"></div></td>
          </tr>
          <?php
					$i = 0;
					while ($row = $query->fetch_object()) {
						$data = explode("-",$row->pe_data_proc);
?>
          <tr>
            <td width="20"> <div align="center">
                <input type="checkbox" name="pes<?php echo $i; ?>" value="<?php echo $row->pe_id; ?>">
              </div></td>
            <td width="332"><b><a class="Preto" href="pes_visualizar.php?pesquisa=<?php echo $row->pe_id; ?>"><?php echo $row->pe_descricao; ?></a></b></td>
            <td width="171" align="center"><b><?php echo "$data[2]/$data[1]/$data[0]"; ?></b></td>
          </tr>
          <?php
						$i++;
					}
?>
          <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5"> <div align="center">
                <input name="Button" Type="button" class="botaoLogin" value="   Cancela Libera��o/Rejei��o   " onClick="submitFormLE('libera');">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
        </table>
	  </form>
<?php
  } // fim do if que verifica se o usuário tem acesso a estas informações

?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
