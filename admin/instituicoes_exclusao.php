<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='030000' OR fu_id='030101') AND us_admin='1' ";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function excluiInstituicao(inst,nome) {
	if(confirm('Você tem certeza que deseja excluir a instituicao ' + nome + '?')) {
		window.location.href='xt_inst_exclusao.php?inst='+inst;
	}
}
</script>
<style type="text/css">
<!--
.style2 {color: #FFFFFF}
-->
</style>

<?php include("menu_admin.php"); ?>
<br>
<table width="580" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="570" valign="top">
	<br>
	  <table width="567" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_instituicoes.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Instituição Exclus�o</strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
	  <br>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {

if (isset($inst) == false) {
?>
		<form name="form1" method="post" action="instituicoes_exclusao.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td colspan="2"> <p> <strong>Selecione uma instituição para exclus�o </strong>
                &nbsp;</p></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="80"> <div align="center">Instituição :</div></td>
            <td width="464"> <select name="inst" size="10">
<?php
		$sql = "SELECT * FROM frm_instituicao ORDER BY in_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
			$varSelected = "";
			if ($inst == trim($row->in_id)) {
				$varSelected = "selected";
			}
?>
                <option <?php echo $varSelected; ?> value="<?php echo trim($row->in_id); ?>"><?php echo trim($row->in_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select>
              <input name="Submit" type="submit" class="botaoLogin" value="  Selecionar   ">
            </td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="2"> <div align="center"></div></td>
          </tr>
        </table>
		</form>

<?php
}
		if (isset($inst)) {
			$sql = "select * from frm_instituicao where in_id = '$inst' ";
			$query = $db->query($sql);
			$rowUSR = $query->fetch_object();
?>
      <form name="formCU" method="post" action="xt_inst_exclusao.php">
	  <input type="hidden" name="inst" value="<?php echo trim($rowUSR->in_id); ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td colspan="4"> <p> <strong>Dados da Instituição</strong>
              </p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		  <tr>
            <td width="105"> <div align="right">Identifica��o :</div></td>
            <td width="432" ><?php echo $rowUSR->in_id; ?></td>
          </tr>
          <tr>
            <td  width="105"> <div align="right">Nome :</div></td>
            <td ><?php echo $rowUSR->in_nome; ?></td>
          </tr>
          <tr>
            <td width="105"> <div align="right">E-mail :</div></td>
            <td><?php echo $rowUSR->in_email; ?></td>
          </tr>
          <tr>
            <td width="105"> <div align="right">Website :</div></td>
            <td><?php echo $rowUSR->in_url; ?></td>
          </tr>
		  <tr bgcolor="#FFFF99">
		    <td   colspan=2>
			<?php

           $sql = "select * FROM rel_inst_usuario a, usuario b  WHERE a.riu_id_inst=$inst  and a.riu_id_usuario = b.us_login ";
           $query = $db->query($sql);
		   $num = $query->num_rows;
           if ($num == 0) {
				echo "> Nenhum usuário faz refer�ncia a esta instituição.<br>";
			}
		   else {	echo "> Pessoas que referenciam esta instituição :<br><br>";
		            while ($row = $query->fetch_object()) {
                          echo "&nbsp;&nbsp;&nbsp;-&nbsp;$row->us_nome<br>";
					}
		   }
 		   echo "<br>";
		   $sql = "select * FROM rel_inst_experiencia a, experiencia b  WHERE a.rie_id_inst=$inst  and a.rie_id_experiencia = b.ex_id ";
           $query = $db->query($sql);
		   $num = $query->num_rows;
           if ($num == 0) {
				echo "> Nenhuma experiência faz refer�ncia a esta instituição.<br><br>";
			}
		   else {	echo "> Experiências que referenciam esta instituição : <br><br>";
		            while ($row = $query->fetch_object()) {
                          echo "&nbsp;&nbsp;&nbsp;-&nbsp;$row->ex_descricao<br>";
					}
		   }
?>
            <span class="style2"></span> </td>
  		 </tr>
          <tr>
            <td colspan="3"><div align="center">                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input name="button" type="button" class="botaoLogin" onClick="excluiInstituicao('<?php echo $rowUSR->in_id; ?>','<?php echo $rowUSR->in_nome;?>');" value="   Excluir  ">&nbsp;&nbsp;
		        <input name="button" type="submit" class="botaoLogin" value="   Selecionar Outra   ">

              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
      </form>
	    <br>	<form name="formTema" method="post">
	  <?php
		} // fim do if que verifica se o usuário j� foi selecionado

} // fim do if que verifica se o usuário tem acesso a estas informações
?>
    </form>
	</td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
