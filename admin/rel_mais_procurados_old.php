<?php
include("../conexao.inc.php");
if (!isset($initFiltro)) {
	setcookie("cookieBuscaAT");
	setcookie("cookieBuscaAG");
	setcookie("cookieBuscaBM");
}
function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) {
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="../estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function mOvr(src,clrOver) {
  if (!src.contains(event.fromElement)) {
      src.style.cursor = 'hand';
      src.bgColor = clrOver;
  }
}
function mOut(src,clrIn) {
  if (!src.contains(event.toElement)) {
     src.style.cursor = 'default';
     src.bgColor = clrIn;
  }
}
function gotourl(url) {
  window.location.href=url;
}


</script>

<?php include("menu_admin.php"); ?>
<br>
<br>
<table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
   <tr>
     <td width="20"  bgcolor="#80AAD2"> <div align="left"><img src="../imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
     <td width="42"  bgcolor="#80AAD2"> <p><img src="../imagens/ico_experiencias.gif" width="34" height="32"></p></td>
     <td width="474" bgcolor="#80AAD2"> <p><strong>Banco de Experiências</strong><b></b></p></td>
     <td width="19"  bgcolor="#80AAD2"> <div align="right"><img src="../imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
  </tr>
</table>
<br>
<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="567" valign="top">
      <table width="567" border="0" cellpadding="5" cellspacing="1" align="center">
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td width="307"> <form name="form1" method="post" action="../banco_experiencias.php">
              <?php if (isset($initFiltro)) { ?>
              <input type="hidden" name="initFiltro" value="">
              <?php } ?>
              <input type="hidden" name="pagina" value="1">
              <input type="hidden" name="init" value="0">
              <table width="290" border="0" cellspacing="0" cellpadding="3">
                <tr>
                  <td width="217"> <input name="busca" type="text" id="busca" size="40" value="<?= isset($busca) ? $busca : ""; ?>"></td>
                  <td width="61"> <?php if (!isset($initFiltro)) { ?>
                    <input name="Submit" type="submit" class="botaoLogin" value="Buscar" class="btn btn-success">
                    <?php } ?> </td>
                </tr>
              </table>
            </form></td>
          <td width="237" valign="top"><img src="../imagens/seta02.gif" width="12" height="13">
            <a href="../textos.php?id=AGROECOLOGIA_SOBRE" class="Preto">O que &eacute;
            Experi&ecirc;ncia em Agroecologia?</a><br> <img src="../imagens/seta02.gif" width="12" height="13">
            <a href="../textos.php?id=ENCONTRA_AQUI" class="Preto">Como utilizar
            a busca</a><br><br>
			</td>



        </tr>


        <?php

if (isset($initFiltro)) {
?>
        <tr>
          <td colspan="2">
		  <table width="555" border="0" cellspacing="1" cellpadding="3">
		  	  <tr>
			  	<td colspan="3">
					<p><b>Formulação de Busca</b></p>
				</td>
			  </tr>
			  <tr>
			  	<td colspan="3" height="1" bgcolor="#000000"><div></div></td>
			  </tr>
              <tr>
                <td width="33%" class="textoPreto" valign="top" bgcolor="#ACCBD0">
                  <?php
	echo "<b>Áreas Temáticas</b><br><br>";
	if (isset($_COOKIE['cookieBuscaAT']) && strlen(trim($_COOKIE['cookieBuscaAT'])) > 0) {
		$arrayAT = explode(";",$_COOKIE["cookieBuscaAT"]);
		$arrayAT = my_array_unique($arrayAT);
		for ($x = 0; $x < count($arrayAT); $x++) {
			$temp = $arrayAT[$x];
			$sql = "SELECT * FROM area_tematica WHERE at_id=$temp";
			$query = $db->query($sql);
			$row = $query->fetch_object();
		echo "&nbsp;&nbsp; - $row->at_descricao<br>";
		}
	}
	else {
		echo "&nbsp;&nbsp; - Todos<br>";
	}
?>
                </td>
                <td width="33%" class="textoPreto" valign="top" bgcolor="#B9D69E">
                  <?php
	echo "<b>Áreas Geográficas</b><br><br>";
	if (isset($_COOKIE['cookieBuscaAG']) && strlen(trim($_COOKIE['cookieBuscaAG'])) > 0) {
		$arrayAG = explode(";",$_COOKIE['cookieBuscaAG']);
		$arrayAG = my_array_unique($arrayAG);
		for ($x = 0; $x < count($arrayAG); $x++) {
			$temp = $arrayAG[$x];
			$sql = "SELECT * FROM area_geografica WHERE ag_id='$temp'";
			$query = $db->query($sql);
			$row = $query->fetch_object();
		echo "&nbsp;&nbsp; - $row->ag_descricao<br>";
		}
	}
	else {
		echo "&nbsp;&nbsp; - Todos<br>";
	}
?>
                </td>
                <td width="43%" class="textoPreto" valign="top" bgcolor="#D6C6A0">
                  <?php
	echo "<b>Biomas</b><br><br>";
	if (isset($_COOKIE['cookieBuscaBM']) && strlen(trim($_COOKIE['cookieBuscaBM'])) > 0) {
		$arrayBM = explode(";",$_COOKIE['cookieBuscaBM']);
		$arrayBM = my_array_unique($arrayBM);
		for ($x = 0; $x < count($arrayBM); $x++) {
			$temp = $arrayBM[$x];
			$sql = "SELECT * FROM habitat WHERE ha_id=$temp";
			$query = $db->query($sql);
			$row = $query->fetch_object();
		echo "&nbsp;&nbsp; - $row->ha_descricao<br>";
		}
	}
	else {
		echo "&nbsp;&nbsp; - Todos<br>";
	}
?>
                </td>
              </tr>
            </table>
            <br> <center>
              <input name="Submit" type="button" class="botaoLogin" value="     Buscar     " onClick="javascript:form1.submit();">
            </center></td>
        </tr>
        <?php
} // fim do if que verifica os filtros

if (isset($busca)) {

	if (isset($_COOKIE['cookieBuscaAT']) && strlen(trim($_COOKIE['cookieBuscaAT'])) > 0) {

		function untree($parent, $level, &$filhos) {
			$parentsql = $db->query("SELECT * FROM area_tematica WHERE at_pertence=$parent ORDER BY at_descricao");
		    if (!$parentsql->num_rows) {
			    return;
		    }
    		else {
		        while($branch = $parentsql->fetch_object()) {
					$filhos .= "$branch->at_id;";
    		        $rename_level = $level;
	    	        untree($branch->at_id, ++$rename_level, $filhos);
       		    }
	        }

			return $filhos;
	    }

		$j = 0;
		$z = 0;

		$arrayTema = array();
		$arrayAT = explode(";",$_COOKIE["cookieBuscaAT"]);
		$arrayAT = my_array_unique($arrayAT);
		$filhosAT = "";
		for ($i = 0; $i < count($arrayAT); $i++){
			$temaID = $arrayAT[$i];

			$sql = "INSERT INTO log_consulta (at_id,dt_consulta, ind_tipo) VALUES ($temaID,'$current_date','E')";

	        $query = $db->query($sql);
         	if (!$query) {
        	   die($db->error);
        	}


			$sql = "SELECT * FROM rel_area_experiencia WHERE rae_id_area=$temaID";
			$query = $db->query($sql);
			$num = $query->num_rows;

			$son = "";
			$filhosAT = untree($temaID,1,$son);

			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayTema[$z][$j] = $row->rae_id_experiencia;
					$j++;
				}
				if (strlen(trim($filhosAT)) > 0) {
					$arrayATF = explode(";",$filhosAT);
					$arrayATF = my_array_unique($arrayATF);
					for ($c = 0; $c < count($arrayATF) - 1; $c++){
						$temaID = $arrayATF[$c];
						$sql = "SELECT * FROM rel_area_experiencia WHERE rae_id_area=$temaID";
						$query = $db->query($sql);
						$num = $query->num_rows;
						if ($num > 0) {
							while($row = $query->fetch_object()) {
								$arrayTema[$z][$j] = $row->rae_id_experiencia;
								$j++;
							}
						}
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else if ($num == 0 && strlen(trim($filhosAT)) > 0) {
				$arrayATF = explode(";",$filhosAT);
				$arrayATF = my_array_unique($arrayATF);
				for ($c = 0; $c < count($arrayATF) - 1; $c++){
					$temaID = $arrayATF[$c];
					$sql = "SELECT * FROM rel_area_experiencia WHERE rae_id_area=$temaID";
					$query = $db->query($sql);
					$num = $query->num_rows;
					if ($num > 0) {
						while($row = $query->fetch_object()) {
							$arrayTema[$z][$j] = $row->rae_id_experiencia;
							$j++;
						}
					}
				}
				$z++;
			}
			else {
				$arrayTema[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

		} // la�o do for


		if ($z > 0) { // os selects montaram pelo menos 1 array?
			if ($z == 1) {
				$intersecaoArraysAT = $arrayTema[0];
			}
			if ($z > 1) {
				$intersecaoArraysAT = array_intersect($arrayTema[0],$arrayTema[1]);
				if ($z > 2) {
					for ($x = 2; $x < $z; $x++) {
						$intersecaoArraysAT = array_intersect($intersecaoArraysAT,$arrayTema[$x]);
					} // la�o do for
				}
			}
			$intersecaoArraysAT = array_unique($intersecaoArraysAT);
			sort($intersecaoArraysAT,SORT_NUMERIC);
		} // fim do if que verifica se a sele��o retornou mais de 0 arrays

		/*
		$filhosAT = substr($filhosAT,0,-1); // PEGA OS FILHOS DAS �REAS TEM�TICAS

		if (strlen(trim($filhosAT)) > 0) { // Quando tem filhos, d� um MERGE, SOMA-SE!!

			$arrayAT = explode(";",$filhosAT);
			$arrayAT = my_array_unique($arrayAT);
			for ($i = 0; $i < count($arrayAT); $i++){
				$temaID = $arrayAT[$i];
				$sql = "SELECT * FROM rel_area_experiencia WHERE rae_id_area=$temaID";
				$query = $db->query($sql);
				$num = $query->num_rows;

				if ($num > 0) {
					while($row = $query->fetch_object()) {
						$arrayTema[$z][$j] = $row->rae_id_experiencia; // $z e $j continuam contando de onde pararam
						$j++;
					}
					$z++;

				} // fim do if que verifica se h� registros
				else {
					$arrayTema[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
					$j++;
					$z++;
				}

			} // la�o do for

			if ($z > 0) { // os selects montaram pelo menos 1 array?
				if ($z == 1) {
					$intersecaoArraysAT = $arrayTema[0];
				}
				if ($z > 1) {
					$intersecaoArraysAT = array_intersect($arrayTema[0],$arrayTema[1]);
					if ($z > 2) {
						for ($x = 2; $x < $z; $x++) {
							$intersecaoArraysAT = array_intersect($intersecaoArraysAT,$arrayTema[$x]);
						} // la�o do for
					}
				}
				$intersecaoArraysAT = array_unique($intersecaoArraysAT);
				sort($intersecaoArraysAT,SORT_NUMERIC);
			}

		} // fim do if que verifica se tem filhos
		*/

	} // fim do if que pergunta se tem filtro de AT



	/******************************** AREA GEOGRFICA ****************************************/



	if (isset($_COOKIE['cookieBuscaAG']) && strlen(trim($_COOKIE['cookieBuscaAG'])) > 0) {

/*
		function untree2($parent, $level, &$filhos) {
			$filhos = "";
			$parentsql = $db->query("SELECT * FROM area_geografica WHERE ag_pertence=$parent ORDER BY ag_descricao");
		    if (!$parentsql->num_rows) {
			    return;
		    }
    		else {
		        while($branch = $parentsql->fetch_object()) {
					$filhos .= "$branch->ag_id;";
    		        $rename_level = $level;
	    	        untree2($branch->ag_id, ++$rename_level, $filhos);
       		    }
	        }

			return $filhos;
	    }

*/
         function untree2($parent, &$filhos) {
			$parentsql = $db->query("SELECT ag_id FROM area_geografica WHERE ag_pertence=$parent ORDER BY ag_descricao");
		    if (!$parentsql->num_rows) {
			    return;
		    }
    		else {
		        while($branch = $parentsql->fetch_object()) {
					$filhos .= "$branch->ag_id;";
	    	        untree2($branch->ag_id, $filhos);
       		    }
	        }

			return $filhos;
	    }
		$j = 0;
		$z = 0;
		$arrayGeo = array();
		$arrayAG = explode(";",$_COOKIE['cookieBuscaAG']);
		$arrayAG = my_array_unique($arrayAG);
		$filhosAG = "";

		$nada = "";
		for ($i = 0; $i < count($arrayAG); $i++){
			$geoID = $arrayAG[$i];


			$sql = "INSERT INTO log_consulta (ag_id,dt_consulta, ind_tipo) VALUES ($geoID,'$current_date','E')";
	        $query = $db->query($sql);
         	if (!$query) {
        	   die($db->error);
        	}


			$sql = "SELECT * FROM rel_geo_experiencia WHERE rge_id_geo=$geoID";
			$query = $db->query($sql);
			$num = $query->num_rows;

			$son = "";
			//$filhosAG = untree2($geoID,1,$son);
			$nada = untree2($geoID,$filhosAG);

			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayGeo[$z][$j] = $row->rge_id_experiencia;
					$j++;
				}
				if (strlen(trim($filhosAG)) > 0) {
					$arrayAGF = explode(";",$filhosAG);
					$arrayAGF = my_array_unique($arrayAGF);

					for ($c = 0; $c < count($arrayAGF) - 1; $c++){
						$geoID = $arrayAGF[$c];
						if ($geoID != "" ) {
						   $sql = "SELECT * FROM rel_geo_experiencia WHERE rge_id_geo=$geoID";
						   $query = $db->query($sql);
						   $num = $query->num_rows;
						   if ($num > 0) {
							  while($row = $query->fetch_object()) {
								  $arrayGeo[$z][$j] = $row->rge_id_experiencia;
								  $j++;
							  }
						  }
					   }
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else if ($num == 0 && strlen(trim($filhosAG)) > 0) {
				$arrayAGF = explode(";",$filhosAG);
				$arrayAGF = my_array_unique($arrayAGF);
				for ($c = 0; $c < count($arrayAGF) - 1; $c++){
					$geoID = $arrayAGF[$c];
					$sql = "SELECT * FROM rel_geo_experiencia WHERE rge_id_geo=$geoID";
					$query = $db->query($sql);
					$num = $query->num_rows;
					if ($num > 0) {
						while($row = $query->fetch_object()) {
							$arrayGeo[$z][$j] = $row->rge_id_experiencia;
							$j++;
						}
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else {
				$arrayGeo[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

		} // la�o do for

		if ($z > 0) { // os selects montaram pelo menos 1 array?
			if ($z == 1) {
				$intersecaoArraysAG = $arrayGeo[0];
			}
			if ($z > 1) {
				$intersecaoArraysAG = array_merge($arrayGeo[0],$arrayGeo[1]);
				if ($z > 2) {
					for ($x = 2; $x < $z; $x++) {
						$intersecaoArraysAG = array_merge($intersecaoArraysAG,$arrayGeo[$x]);
					} // la�o do for
				}
			}
			$intersecaoArraysAG = array_unique($intersecaoArraysAG);
			sort($intersecaoArraysAG,SORT_NUMERIC);
		} // fim do if que verifica se a sele��o retornou mais de 0 arrays


		/*
		$filhosAG = substr($filhosAG,0,-1); // PEGA OS FILHOS DAS �REAS GEOGR�FICAS

		if (strlen(trim($filhosAG)) > 0) { // Quando tem filhos, d� um MERGE, SOMA-SE!!

			$arrayAG = explode(";",$filhosAG);
			$arrayAG = my_array_unique($arrayAG);
			for ($i = 0; $i < count($arrayAG); $i++){
				$geoID = $arrayAG[$i];
				$sql = "SELECT * FROM rel_geo_experiencia WHERE rge_id_geo=$geoID";
				$query = $db->query($sql);
				$num = $query->num_rows;

				if ($num > 0) {
					while($row = $query->fetch_object()) {
						$arrayGeo[$z][$j] = $row->rge_id_experiencia; // $z e $j continuam contando de onde pararam
						$j++;
					}
					$z++;

				} // fim do if que verifica se h� registros
				else {
					$arrayGeo[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
					$j++;
					$z++;
				}

			} // la�o do for

			if ($z > 0) { // os selects montaram pelo menos 1 array?
				if ($z == 1) {
					$intersecaoArraysAG = $arrayGeo[0];
				}
				if ($z > 1) {
					$intersecaoArraysAG = array_merge($arrayGeo[0],$arrayGeo[1]);
					if ($z > 2) {
						for ($x = 2; $x < $z; $x++) {
							$intersecaoArraysAG = array_merge($intersecaoArraysAG,$arrayGeo[$x]);
						} // la�o do for
					}
				}
				$intersecaoArraysAG = array_unique($intersecaoArraysAG);
				sort($intersecaoArraysAG,SORT_NUMERIC);
			}

		} // fim do if que verifica se tem filhos
		*/

	} // fim do if que pergunta se tem filtro de AG

	/******************************** BIOMAS ****************************************/


	if (isset($_COOKIE['cookieBuscaBM']) && strlen(trim($_COOKIE['cookieBuscaBM'])) > 0) {

		$j = 0;
		$z = 0;
		$arrayBio = array();
		$arrayBM = explode(";",$_COOKIE['cookieBuscaBM']);
		$arrayBM = my_array_unique($arrayBM);
		for ($i = 0; $i < count($arrayBM); $i++){
			$bioID = $arrayBM[$i];

			$sql = "INSERT INTO log_consulta (ha_id,dt_consulta, ind_tipo) VALUES ($bioID,'$current_date','E')";
	        $query = $db->query($sql);
         	if (!$query) {
        	   die($db->error);
        	}


			$sql = "SELECT * FROM rel_habitat_experiencia WHERE rhe_id_habitat=$bioID";
			$query = $db->query($sql);
			$num = $query->num_rows;

			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayBio[$z][$j] = $row->rhe_id_experiencia;
					$j++;
				}
				$z++;

			} // fim do if que verifica se h� registros
			else {
				$arrayBio[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

		} // la�o do for

		if ($z > 0) { // os selects montaram pelo menos 1 array?
			if ($z == 1) {
				$intersecaoArraysBM = $arrayBio[0];
			}
			if ($z > 1) {
				$intersecaoArraysBM = array_intersect($arrayBio[0],$arrayBio[1]);
				if ($z > 2) {
					for ($x = 2; $x < $z; $x++) {
						$intersecaoArraysBM = array_intersect($intersecaoArraysBM,$arrayBio[$x]);
					} // la�o do for
				}
			}
			$intersecaoArraysBM = array_unique($intersecaoArraysBM);
			sort($intersecaoArraysBM,SORT_NUMERIC);

		} // fim do if que verifica se a sele��o retornou mais de 0 arrays

	} // fim do if que pergunta se tem filtro de BM

	$arrays = 0;
	if (isset($intersecaoArraysAT) && count($intersecaoArraysAT) >= 0) {
		$arraysCompletos[$arrays] = $intersecaoArraysAT;
		$arrays++;
		$arrayUnico = $intersecaoArraysAT;
	}
	if (isset($intersecaoArraysAG) && count($intersecaoArraysAG) >= 0) {
		$arraysCompletos[$arrays] = $intersecaoArraysAG;
		$arrays++;
		$arrayUnico = $intersecaoArraysAG;
	}
	if (isset($intersecaoArraysBM) && count($intersecaoArraysBM) >= 0) {
		$arraysCompletos[$arrays] = $intersecaoArraysBM;
		$arrays++;
		$arrayUnico = $intersecaoArraysBM;
	}

	$sqlMaster = "SELECT * FROM experiencia WHERE ex_liberacao='S' AND ";

	if (isset($arraysCompletos) && count($arraysCompletos) == 1) {
		$arrayMontado = "";
		$arrayFinal = $arraysCompletos[0];
		while (list ($key, $val) = each ($arraysCompletos[0])) {
    		$arrayMontado .= "$val,";
		}
		$arrayMontado = substr($arrayMontado,0,-1);
	}
	else if (count($arraysCompletos) > 1) {
		$arrayFinal = array_intersect($arraysCompletos[0],$arraysCompletos[1]);
		if (count($arraysCompletos) > 2) {
			for ($x = 2; $x < count($arraysCompletos); $x++) {
				$arrayFinal = array_intersect($arrayFinal,$arraysCompletos[$x]);
			}
		}
		$arrayMontado = "";
		while (list ($key, $val) = each ($arrayFinal)) {
    		$arrayMontado .= "$val,";
		}
		$arrayMontado = substr($arrayMontado,0,-1);
	}

	$buscaParam = $busca;

	$busca = explode(" ",$busca);
	$arrayChaves = array();
	$i = 0;
	for ($x = 0; $x < count($busca); $x++) {
		if (strlen(trim($busca[$x])) > 0) {
			$arrayChaves[$i] = $busca[$x];
			$i++;
		}
	}

	for ($x = 0; $x < count($arrayChaves); $x++) {
		$temp = $arrayChaves[$x];
		$sqlMaster .= "(UCASE(ex_descricao) LIKE UCASE('%$temp%') OR UCASE(ex_resumo) LIKE UCASE('%$temp%') OR UCASE(ex_chamada) LIKE UCASE('%$temp%')) AND ";
	}

	$temp = substr(trim($sqlMaster),-3);
	if ($temp == "AND") {
		$sqlMaster = substr(trim($sqlMaster),0,-3);
	}
	if (isset($arrayMontado) && strlen(trim($arrayMontado)) > 0) {
		$sqlMaster .= " AND ex_id in ($arrayMontado)";
	}
	else if (isset($arrayMontado) && strlen(trim($arrayMontado)) == 0) {
		$sqlMaster .= " AND ex_id in (0)";
	}

	$query = $db->query($sqlMaster);
	$num = $query->num_rows;

	$numRegistros = $num;

	if ($numRegistros > 15) {
		$inicial = 15;
	}
	else {
		$inicial = $numRegistros;
	}
	if ($init == 0) { $initp = 0;}   else { $initp = $init - 1;}

	$sqlMaster .= " ORDER BY ex_descricao LIMIT $initp,15";
	$query = $db->query($sqlMaster);
	if ($init == 0 && $numRegistros > 1 ) { $init = 1;}
?>
        <tr>
          <td><p><strong>Resultados da busca</strong> [
              <a href="../banco_experiencias.php" class="Preto"><strong>Nova Busca</strong></a>
              ]<br>
              <br>
              <img src="../imagens/ico_ficha.gif" width="14" height="13"> Passe o
              mouse no &iacute;cone para visualizar o resumo</p></td>
          <td align="right"><p>Exibindo <strong><?php echo $init; ?></strong>
              a <strong><?php
			  if ($inicial * $pagina >= $numRegistros) { echo $numRegistros; }
			  else {
			  echo $inicial * $pagina; }?></strong> de <strong><?php echo $numRegistros; ?></strong>
              registros encontrados</p></td>
        </tr>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"><div align="center"></div></td>
        </tr>
        <tr>
          <td colspan="2"><p>
              <?php
	if ($num > 0) {
		while ($row = $query->fetch_object()) {
			if (strlen(trim($row->ex_chamada)) > 0) {
				echo "<a href=\"experiencias.php?experiencia=$row->ex_id\"><span class=\"oi oi-document lead text-secondary\"></span><img src=\"imagens/ico_ficha.gif\" style=\"display:none\"  alt=\"$row->ex_resumo\" border=\"0\"></a> <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"Preto\"><strong>".substr($row->ex_chamada,0,70)."</strong></a><br>";
			}
			else {
				echo "<a href=\"experiencias.php?experiencia=$row->ex_id\"><span class=\"oi oi-document lead text-secondary\"></span><img src=\"imagens/ico_ficha.gif\" style=\"display:none\"  alt=\"$row->ex_resumo\" border=\"0\"></a> <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"Preto\"><strong>Sem Informa��o</strong></a><br>";
			}
		}
	}
	else {
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
?>
            </p></td>
        </tr>
        <tr>
          <td colspan="2"> <p class="textoPreto">Páginas:
              <?php
	$numPaginas = (int)($numRegistros/15);
	if ($numRegistros % 15 > 0) {
		$numPaginas++;
	}
	$init = 1;
	for ($x = 1; $x <= $numPaginas; $x++) {
		if ($pagina == $x) {
			echo "[ $x ] ";
		}
		else {
			echo "[ <a href=\"banco_experiencias.php?busca=$buscaParam&pagina=$x&init=$init&initFiltro=S\" class=\"Preto\">$x</a> ] ";
		}
		$init = $init + 15;
	}
?>
            </p></td>
        </tr>
        <?php
} // fim do if que verifica se a busca foi feita

if ($numRegistros > 0) {
?>
 <tr>
          <td height="1" colspan="2" bgcolor="#000000"><div align="center"></div></td>
        </tr>
<?php
}
else { ?>
        <tr>
          <td><p><strong>Áreas Temáticas</strong></p></td>
          <td><p align="right" class="textoPreto">[ <a href="../banco_experiencias.php" class="Preto"><strong>Nova
              Busca</strong></a> ]<strong></strong></p></td>
        </tr>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td colspan="2"> <table width="555" border="0" cellpadding="10" cellspacing="2">
              <?php
$sql = "SELECT * FROM area_tematica WHERE at_pertence=0 ORDER BY at_descricao";
$query = $db->query($sql);
$i = 0;
while ($row = $query->fetch_object()) {
	if ($i == 0) {
    	echo "<tr bgcolor=\"#ACCBD0\" class=\"textoPreto10px\">";
	}
    echo "<td onmouseover=\"mOvr(this,'#86AEB0');\" onmouseout=\"mOut(this,'#ACCBD0');\" onClick=\"abrePopup('exp_popup_busca_temas_pesq.php?id=$row->at_id&url=banco_experiencias.php','tema','scrollbars=yes,width=517,height=300')\">
          <div align=\"center\"><strong>$row->at_descricao</strong></div>
		  </td>";
	if ($i == 3) {
		echo "</tr>";
	}

	$i++;

	if ($i == 3) {
		$i = 0;
	}

}
} ?>
            </table></td>
        </tr>
<?php
if ($numRegistros > 0) {
?>
  <tr>
          <td colspan=2><p align="center" class="textoPreto">[ <a href="../banco_experiencias.php" class="Preto"><strong>Nova
              Busca</strong></a> ]<strong></strong></p></td>
        </tr>
 <tr>
          <td height="1" colspan="2" bgcolor="#000000"><div align="center"></div></td>
        </tr>
<?php
}
else { ?>
        <tr>
          <td colspan="2"> <table width="555" border="0" cellpadding="10" cellspacing="2">
              <tr bgcolor="#B9D69E">
                <td width="254" bgcolor="#B9D69E" onClick="abrePopup('exp_popup_busca_ag_pesq.php?url=banco_experiencias.php','ag','scrollbars=yes,width=517,height=300')" onmouseover="mOvr(this,'#89BA5C');" onmouseout="mOut(this,'#B9D69E');">
                  <div align="center"><strong>&Aacute;reas Geogr&aacute;ficas</strong></div></td>
                <td width="255" bgcolor="#D6C6A0" onClick="abrePopup('exp_popup_busca_biomas_pesq.php?url=banco_experiencias.php','biomas','scrollbars=yes,width=517,height=300')" onmouseover="mOvr(this,'#C4AC77');" onmouseout="mOut(this,'#D6C6A0');">
                  <div align="center"><strong>Biomas</strong></div></td>
              </tr>
<?php } ?>
            </table></td>
        </tr>
      </table>
      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
