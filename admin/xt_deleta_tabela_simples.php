<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");
$numPerm=0;
$numRel=0;
$varTabela = $_REQUEST['tipoTabela'];
$varID = 'id';

if (!isset($loggedIn)) {
	if (isset($_COOKIE['cookieUsuario']) && $_COOKIE['cookieUsuario']) {
		if (!isset($s) || !$s) {
			$s = new stdClass();
			$s->u = resgata_dados_usuario();
		}
		if ((!isset($_COOKIE['cookieAdmin']) || !$_COOKIE['cookieAdmin']) && (isset($s) && isset($s->u) && isset($s->u->us_admin))) {
			unset($s->u->us_admin);
		}
	}

	$loggedIn = isset($s) && isset($s->u) && $s->u;
	$isAdmin = $loggedIn && isset($s->u->us_admin) && $s->u->us_admin;
}

switch ($tipoTabela = $_REQUEST['tipoTabela']) {
	case 'GU':
		$varTabela = "grupo_usuarios";
		$varCampo = "gu_descricao";
		$varID = "gu_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010100') AND us_admin='1' ";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;

		$sql = "SELECT * FROM usuario WHERE us_grupo=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;

		break;
	case 'ID':
		$varTabela = "idioma";
		$varCampo = "idm_descricao";
		$varID = "idm_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010800') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;

		$sql = "SELECT * FROM bibliografia WHERE bb_tipo=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;

		break;
	case 'TB':
		$varTabela = "tipo_bibliografias";
		$varCampo = "tb_descricao";
		$varID = "tb_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010200') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;

		$sql = "SELECT * FROM bibliografia WHERE bb_tipo=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;

		break;
	case 'TE':
		$varTabela = "tipo_enderecos";
		$varCampo = "te_descricao";
		$varID = "te_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010300') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;

		$sql = "SELECT * FROM endereco_instituicao WHERE ei_id_endereco=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;

		$sql = "SELECT * FROM endereco_usuario WHERE eu_id_endereco=$id";
		$query = $db->query($sql);
		$numRel += $query->num_rows;

		break;
	case 'TI':
		$varTabela = "tipo_instituicoes";
		$varCampo = "ti_descricao";
		$varID = "ti_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010400') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;

		$sql = "SELECT * FROM frm_instituicao WHERE in_tipo=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;

		break;
	case 'TT':
		$varTabela = "tipo_telefones";
		$varCampo = "tt_descricao";
		$varID = "tt_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010500') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;

		$sql = "SELECT * FROM telefone_instituicao WHERE ti_id_telefone=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;

		$sql = "SELECT * FROM telefone_usuario WHERE tu_id_telefone=$id";
		$query = $db->query($sql);
		$numRel += $query->num_rows;

		break;
	case 'TX':
		$varTabela = "tipo_experiencias";
		$varCampo = "tx_descricao";
		$varID = "tx_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010600') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;

		$sql = "SELECT ex_tipo FROM experiencia WHERE ex_tipo=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;

		break;
	case 'SX':
		$varTabela = "status_experiencia";
		$varCampo = "se_status";
		$varID = "se_id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='010000' OR fu_id='010700') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;

		$sql = "SELECT ex_status FROM experiencia WHERE ex_status=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;

		break;

	case 'LK':
		$varTabela = "tipo_link";
		$varCampo = "descricao";
		$varID = "id";
		$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='060000' OR fu_id='060000') AND us_admin=1";
		$query = $db->query($sql);
		$numPerm = $query->num_rows;
		$sql = "SELECT * FROM link WHERE tipo=$id";
		$query = $db->query($sql);
		$numRel = $query->num_rows;
		break;
} // fim do switch

if ($numPerm==0 && !$isAdmin) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a excluir estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página principal.</p>";
	$db->disconnect;
	return;
}

if ($numRel != 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não pode excluir estas informações pois existem dados associados! Clique <a href=\"tabela_simples.php?tipoTabela=$tipoTabela\">aqui</a> para voltar para a página anterior.</p>";
	$db->disconnect;
	return;
}

$sql = "DELETE FROM $varTabela WHERE $varID=$id";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$db->close();

?>
<script language="JavaScript">
	window.location.href='tabela_simples.php?tipoTabela=<?php echo $tipoTabela; ?>';
</script>
