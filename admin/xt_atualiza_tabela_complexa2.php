<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");
$hid[0] = "frm";
$hid[1] = "ano";
$hid[2] = "campo";
foreach ($_POST['r'] as $rs) {
	$sql= 'UPDATE '.$_POST['tipoTabela'].' SET ';
	foreach ($rs as $campo=>$valor)
		if (!in_array($campo,$hid))
			$sql.="`".$campo.'`="'.$db->real_escape_string($valor).'", ';
	$sql = substr($sql,0,-2);
	$sql.= ' WHERE ';
	foreach ($hid as $hi)
		$sql.='`'.$hi.'`="'.$db->real_escape_string($rs[$hi]).'" AND ';
	$sql = substr($sql,0,-5).';'.chr(10);
	//echo $sql;exit;
	$res = $db->query($sql);
	if (!$res)
		echo $db->error();
}
header('location:tabela_complexa2.php?tipo='.$_POST['tipoTabela'].'&frm='.$_POST['frm']);
?>
