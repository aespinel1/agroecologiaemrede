<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Fam&iacute;lias / G&ecirc;neros / Esp&eacute;cies</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  destino = selObj.options[selObj.selectedIndex].value;
  eval(targ+".location='"+destino+"'");
  if (restore) selObj.selectedIndex=0;
}

function criticaForm() {
	familia = form1.idFamilia.value;
	genero = form1.idGenero.value;
	especie = form1.idEspecie.value;
	if (familia.length == 0 || genero.length == 0 || especie.length == 0) {
		alert('Você deve escolher uma cadeia de fam�lia/g�nero/esp�cie ou digitar pelo\nmenos um nome vulgar para cadastrar uma Animal ou Planta!');
		return;
	}
	form1.submit();
}
</script>
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="400" border="0" cellpadding="5">
  <tr> 
    <td bgcolor="#666666" class="textoBranco">Atualizar dados</td>
  </tr>
  <tr> 
    <td>
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {

	$sql = "SELECT * FROM entidade_biologica WHERE eb_id=$entidade";
	$query = $db->query($sql);
	$rowEB = $query->fetch_object();
?>
      <form name="form1" method="post" action="xt_atualiza_eb_familia_etc.php">
		<input type="hidden" name="id" value="<?php echo $entidade; ?>">
		  <input type="hidden" name="idFamilia" value="<?php echo $familiaID; ?>">
		  <input type="hidden" name="idGenero" value="<?php echo $generoID; ?>">
		  <input type="hidden" name="idEspecie" value="<?php echo $especieID; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td colspan="2"><p><strong>Atualizar Fam&iacute;lia 
                / G&ecirc;nero / Esp&eacute;cie</strong></p></td>
          </tr>
          <tr> 
            <td height="1" colspan="10" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td width="81"><div align="right"><strong>Descri&ccedil;&atilde;o:</strong></div></td>
            <td width="483"><strong><?php echo $rowEB->eb_descricao; ?></strong></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
<?php
		echo "<p class=\"textoPreto10px\">";
		if ($rowEB->eb_familia != 0) {
			$temp = $rowEB->eb_familia;
			$sql = "SELECT * FROM eb_familia WHERE ebf_id=$temp";
			$query = $db->query($sql);
			$row = $query->fetch_object();
			echo "$row->ebf_familia / ";
		}
		if ($rowEB->eb_genero != 0) {
			$temp = $rowEB->eb_genero;
			$sql = "SELECT * FROM eb_genero WHERE ebg_id=$temp";
			$query = $db->query($sql);
			$row = $query->fetch_object();
			echo "$row->ebg_genero / ";
		}
		if ($rowEB->eb_especie != 0) {
			$temp = $rowEB->eb_especie;
			$sql = "SELECT * FROM eb_especie WHERE ebe_id=$temp";
			$query = $db->query($sql);
			$row = $query->fetch_object();
			echo "$row->ebe_especie";
		}
		echo "</p>";
?>
			</td>
          </tr>
          <tr> 
            <td><div align="right"><strong>Fam�lia:</strong></div></td>
            <td>
              <select name="familia" onChange="MM_jumpMenu('parent',this,0)">
                <option value="popup_familias.php?entidade=<?php echo $entidade; ?>">-- Selecione --</option>
<?php 
		$sql = "SELECT * FROM eb_familia ORDER BY ebf_familia";
		$query = $db->query($sql);		  
		while ($row = $query->fetch_object()) {
			$varSelected = "";
			if ($row->ebf_id == $familiaID) {
				$varSelected = "selected";
			}
?>
                <option <?php echo $varSelected; ?> value="popup_familias.php?entidade=<?php echo $entidade; ?>&familiaID=<?php echo $row->ebf_id; ?>"><?php echo trim($row->ebf_familia); ?></option>
<?php
		}
?>
              </select>
              </td>
          </tr>
          <tr> 
            <td><div align="right"><strong>G&ecirc;nero:</strong></div></td>
            <td>
<?php
		if (isset($familiaID)) {
?>
              <select name="genero" onChange="MM_jumpMenu('parent',this,0)">
                <option value="popup_familias.php?entidade=<?php echo $entidade; ?>&familia=<?php echo $familiaID; ?>">-- Selecione --</option>
<?php
			$sql = "SELECT * FROM eb_genero WHERE ebg_id_familia=$familiaID ORDER BY ebg_genero";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
				$varSelected = "";
				if ($row->ebg_id == $generoID) {
					$varSelected = "selected";
				}
?>
					<option <?php echo $varSelected; ?> value="popup_familias.php?entidade=<?php echo $entidade; ?>&familiaID=<?php echo $familiaID; ?>&generoID=<?php echo $row->ebg_id; ?>"><?php echo trim($row->ebg_genero); ?></option>
<?php
			}
?>
              </select>
<?php
		} // fim do if
?>
              </td>
          </tr>
          <tr> 
            <td><div align="right"><strong>Esp&eacute;cie:</strong></div></td>
            <td> <strong> 
<?php
		if (isset($generoID)) {
?>
              <select name="especie" onChange="MM_jumpMenu('parent',this,0)">
                <option value="popup_familias.php?entidade=<?php echo $entidade; ?>&familiaID=<?php echo $familiaID; ?>&generoID=<?php echo $generoID;?>">-- Selecione --</option>
                <?php
			$sql = "SELECT * FROM eb_especie WHERE ebe_id_genero=$generoID ORDER BY ebe_especie";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
				$varSelected = "";
				if ($row->ebe_id == $especieID) {
					$varSelected = "selected";
				}
?>
                <option <?php echo $varSelected; ?> value="popup_familias.php?entidade=<?php echo $entidade; ?>&familiaID=<?php echo $familiaID; ?>&generoID=<?php echo $generoID;?>&especieID=<?php echo $row->ebe_id; ?>"><?php echo trim($row->ebe_especie); ?></option>
                <?php
			}
?>
              </select>
<?php
		} // fim do if
?>
              </strong></td>
          </tr>
          <tr> 
            <td>&nbsp;</td>
            <td><strong> 
              <input name="enviar" type="button" class="botaoLogin" value="Gravar" onClick="criticaForm();">
              </strong></td>
          </tr>
        </table>
      </form>
<?php
} // fim do if que verifica as credenciais
?>
	</td>
  </tr>
</table>
</body>
</html>