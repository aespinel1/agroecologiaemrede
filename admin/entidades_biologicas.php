<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaForm() {
	familia = form1.idFamilia.value;
	//genero = form1.idGenero.value;
	//especie = form1.idEspecie.value;
	nomes = form1.nomes.value;
	if (familia.length == 0 && nomes.length == 0) {
		alert('Você deve escolher pelo menos uma fam�lia ou digitar pelo\nmenos um nome vulgar para cadastrar uma Animal ou Planta!');
		return;
	}
	form1.submit();
}

function excluiRegistro(id,desc) {
	if(confirm('Você tem certeza que deseja excluir o registro '+desc+'?')) {
		window.location.href='xt_deleta_tabela_complexa.php?tipoTabela=<?php echo $tipo; ?>&id='+id;
	}
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_jumpMenu(targ,selObj,restore){ //v3.0
  idExterno = form1.idExterno.value;
  infra = form1.infra.value;
  nomes = form1.nomes.value;
  destino = selObj.options[selObj.selectedIndex].value + '&idExterno=' + idExterno + '&infra=' + infra + '&nomes=' + nomes;
  eval(targ+".location='"+destino+"'");
  if (restore) selObj.selectedIndex=0;
}
</script>
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57"> 
      <div align="center"><a class="Branco" href="xt_logout.php"><img src="imagens/ico_logoff.gif" border="0"><br><br>
        <strong>LOGOUT</strong></a></div>
	</td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_complexa.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><b>Animais 
              e Plantas</b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br> 
<?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
?>
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td colspan="2"><p><strong>Op��es</strong></p></td>
          </tr>
          <tr> 
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            
          <td width="283"><img src="imagens/seta02.gif" width="12" height="13"> 
            <a href="eb_familias.php" class="Preto">Cadastro de Fam&iacute;lias</a><br> 
              <br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="eb_generos.php" class="Preto">Cadastro 
            de G&ecirc;neros</a><br>
          </td>
          <td width="283" valign="top"><img src="imagens/seta02.gif" width="12" height="13"> 
            <a href="eb_especies.php" class="Preto">Cadastro de Esp&eacute;cies</a><br>
            <br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="eb_cadastro.php" class="Preto">Manuten&ccedil;&atilde;o 
            de Animais e Plantas</a></td>
          </tr>
        </table>
        <br>
		<?php if (isset($novaEntidade)) { ?>
		<p align="center">
		<b>Animal ou Planta cadastrada com sucesso!</b>
		</p>
		<?php } ?>
      <form name="form1" method="post" action="xt_insere_entidade_biologica.php">
	  <input type="hidden" name="idFamilia" value="<?php echo $familiaID; ?>">
	  <input type="hidden" name="idGenero" value="<?php echo $generoID; ?>">
	  <input type="hidden" name="idEspecie" value="<?php echo $especieID; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td colspan="2"><p>Cadastrar nova <strong>Animal ou Planta</strong></p></td>
          </tr>
          <tr> 
            <td height="1" colspan="10" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td width="141" valign="top"> <div align="right"><strong>Nomes 
                vulgares</strong>:<br>
                <br>
                Digite os nomes vulgares, separando-os por v&iacute;rgulas.</div></td>
            <td width="403"><textarea name="nomes" cols="67" rows="10" id="nomes"><?php echo $nomes; ?></textarea></td>
          </tr>
          <tr> 
            <td><div align="right"><strong>Fam&iacute;lia</strong>:</div></td>
            <td> 
<?php
	$sql = "SELECT * FROM eb_familia ORDER BY ebf_familia";
	$query = $db->query($sql);
	echo "<select name=\"familia\" class=\"inputFlat\" onChange=\"MM_jumpMenu('parent',this,0)\">";
	echo "<option value=\"entidades_biologicas.php\">-- Selecione uma familia --</option>";
	while ($row = $query->fetch_object()) {
       	$varSelected = "";
		if (isset($familiaID) && $familiaID == $row->ebf_id) {
			$varSelected = "selected";
		}
		echo "<option $varSelected value=\"entidades_biologicas.php?familiaID=$row->ebf_id\">$row->ebf_familia</option>";
	} // la�o do loop
	echo "</select>";
?>
            </td>
          </tr>
          <tr> 
            <td><div align="right"><strong>G&ecirc;nero</strong>:</div></td>
            <td> 
              <?php
	if (isset($familiaID)) {
		$sql = "SELECT * FROM eb_genero WHERE ebg_id_familia=$familiaID ORDER BY ebg_genero";
		$query = $db->query($sql);
		echo "<select name=\"genero\" class=\"inputFlat\" onChange=\"MM_jumpMenu('parent',this,0)\">";
		echo "<option value=\"entidades_biologicas.php?familiaID=$familiaID\">-- Selecione um g�nero --</option>";
		while ($row = $query->fetch_object()) {
    	   	$varSelected = "";
			if (isset($generoID) && $generoID == $row->ebg_id) {
				$varSelected = "selected";
			}
    	   	echo "<option $varSelected value=\"entidades_biologicas.php?familiaID=$familiaID&generoID=$row->ebg_id\">$row->ebg_genero</option>";
		} // la�o do loop
		echo "</select>";
	}
?>
            </td>
          </tr>
          <tr> 
            <td><div align="right"><strong>Esp&eacute;cie</strong>:</div></td>
            <td> 
              <?php
	if (isset($generoID)) {
		$sql = "SELECT * FROM eb_especie WHERE ebe_id_genero=$generoID ORDER BY ebe_especie";
		$query = $db->query($sql);
		echo "<select name=\"especie\" class=\"inputFlat\" onChange=\"MM_jumpMenu('parent',this,0)\">";
		echo "<option value=\"entidades_biologicas.php?familiaID=$familiaID&generoID=$generoID\">-- Selecione uma esp�cie --</option>";
		while ($row = $query->fetch_object()) {
    	   	$varSelected = "";
			if (isset($especieID) && $especieID == $row->ebe_id) {
				$varSelected = "selected";
			}
    	   	echo "<option $varSelected value=\"entidades_biologicas.php?familiaID=$familiaID&generoID=$generoID&especieID=$row->ebe_id\">$row->ebe_especie</option>";
		} // la�o do loop
		echo "</select>";
	}
?>
            </td>
          </tr>
          <tr> 
            <td> <div align="right"><strong>Infra-esp&eacute;cie</strong>:</div></td>
            <td><input value="<?php echo $infra; ?>" name="infra" type="text" id="infra" size="40" maxlength="255"></td>
          </tr>
          <tr>
            <td><div align="right"><strong>ID Externo</strong>:</div></td>
            <td> <input value="<?php echo $idExterno; ?>" name="idExterno" type="text" class="inputFlatRight" id="idExterno"> 
            </td>
          </tr>
          <tr> 
            <td>&nbsp;</td>
            <td><input name="enviar" type="button" class="botaoLogin" value="Gravar" onClick="criticaForm();"></td>
          </tr>
        </table>
      </form>
<?php
} // fim do if que verifica se o usuário tem acesso � informa��o
?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>