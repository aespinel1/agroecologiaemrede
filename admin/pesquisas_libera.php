<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM usuario WHERE us_login='$cookieUsuario' AND us_session='$cookieSession' AND us_admin=1";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040500') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

function submitFormLE(acao) {
	if (acao == 'libera') {
		formLE.acao.value = 'libera';
	}
	else if (acao == 'pendente') {
		formLE.acao.value = 'pendente';
	}
	else {
		formLE.acao.value = 'cancela';
	}

	formLE.submit();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}



</script>
<style type="text/css">
<!--
.style1 {
	color: #006666;
	font-weight: bold;
}
-->
</style>
<?php include("menu_admin.php"); ?> 

<table width="729" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="719" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20"  bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42"  bgcolor="#80AAD2"> <p><img src="imagens/ico_bibliografia.gif" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Pesquisas - An�lise</strong><b></b></p></td>
          <td width="19"  bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
  </tr>
</table>

<?php
			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
				$sql = "select a.*, date_format(dt_pendente,'%d/%m/%Y') as data_pend , date_format(dt_resposta,'%d/%m/%Y') as data_resp , date_format(dt_criacao,'%d/%m/%Y') as data_inclusao , date_format(dt_atualizacao,'%d/%m/%Y') as data_alteracao  from pesquisa a where pe_liberacao='N' or pe_liberacao='P' or pe_liberacao='E'  ";
				$query = $db->query($sql);
?>
	  <form name="formLE" method="post" action="xt_pes_libera_pesquisa.php">
	  <input type="hidden" name="acao" value="">
        <table width="711" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td height="24" colspan="9"> <p> <span class="style1"><strong>An�lise
                Pesquisas</strong>
                <?php if (isset($pesLibera)) { ?>
                - pesquisa(s) liberadas com sucesso !
                <?php } ?>
              </span></p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000" colspan="9"> <div align="center"></div></td>
          </tr>
		   <tr>
            <td width="23">&nbsp;</td>
            <td width="36">T�tulo</td>
            <td width="74">Situa��o</td>
            <td width="70">Pend�ncia</td>
            <td width="74">Registro<br>Pend�ncia</td>
            <td width="102">Resposta</td>
            <td width="61">Registro<br>Resposta</td>
            <td width="73">Data Inclusão</td>
            <td width="96">Data �ltima<br>Alteração</td>
          </tr>
		  <tr>
            <td height="1" colspan="9" bgcolor="#000000"> <div align="center"></div></td>
          </tr>

          <?php
		  $i = 0;
		  while ($row = $query->fetch_object()) {
?>
          <tr>
            <td> <div align="center">
                <input type="checkbox" name="pes<?php echo $i; ?>" value="<?php echo $row->pe_id; ?>">
              </div></td>
            <td><b><a class="Preto" href="pes_visualizar.php?pesquisa=<?php echo $row->pe_id; ?>"><?php echo $row->pe_descricao; ?></a></b></td>
<?php
			 if ($row->pe_liberacao == "P") {
			    $cor = "#FF3366";
		     }

			 if ($row->pe_liberacao == "E") {
			    $cor = "#FFFFCC";
		     }

			 if ($row->pe_liberacao == "N") {
			    $cor = "#99CC99";
		     }
            ?>

            <td bgcolor="<?php echo $cor; ?>"><?php
			 if ($row->pe_liberacao == "P") {
			    echo "Pendente, Aguardando solu��o pessoa cadastro.";
		     }

			 if ($row->pe_liberacao == "E") {
			    echo "Pend�ncia Respondida, aguardando an�lise.";
		     }

			 if ($row->pe_liberacao == "N") {
			    echo "Aguardando An�lise, inclusão.";
		     }
            ?>
            </td>

			<td><b><?php echo $row->txt_pendente; ?></b></td>
			<td><b><?php if ($row->dt_pendente == "") { echo "&nbsp;&nbsp;-"; } else { echo $row->data_pend; }?></b></td>

			<td><b><?php echo $row->txt_resposta; ?></b></td>
			<td><b><?php	if ($row->dt_resposta == "") { echo "&nbsp;&nbsp;-"; } else { echo $row->data_altera; } ?></b></td>

			<td><b><?php	if ($row->dt_criacao == "") { echo "&nbsp;&nbsp;-"; } else { echo $row->data_inclusao; } ?></b></td>
			<td><b><?php	if ($row->dt_atualizacao == "") { echo "&nbsp;&nbsp;-"; } else { echo $row->data_alteracao; } ?></b></td>

          </tr>
          <?php
						$i++;
					}
?>
          <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td height="1" colspan="9" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="9"> <div align="center">
                <input name="Button" Type="button" class="botaoLogin" value="   Liberar   " onClick="submitFormLE('libera');">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="9" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr bgcolor="#FFFF99">
            <td colspan="9"> <div align="left"> <span>Utilize
                o campo abaixo para justificar o motivo da rejei&ccedil;&atilde;o ou da pend&ecirc;ncia para a pessoa que
                efetuou o cadastro. Esta ir&aacute; receber um e-mail com os
                dados digitados abaixo.</span><br>
                <br>
                <textarea name="justificativa" cols="135" rows="3" wrap="VIRTUAL" id="justificativa"></textarea>
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="9" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="9"> <div align="center">                &nbsp;&nbsp;
                <input name="Button"  Type="button" class="botaoLogin"  value="   Pendente   " onClick="submitFormLE('pendente');">
                &nbsp;&nbsp;
                <input name="Button2" Type="button" class="botaoLogin"  value="   Rejeitar   " onClick="submitFormLE('cancela');">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="9" bgcolor="#000000"><div align="center"></div></td>
          </tr>
        </table>
	  </form>
<?php
  } // fim do if que verifica se o usuário tem acesso a estas informações

?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
