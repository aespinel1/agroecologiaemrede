<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM usuario WHERE us_login='$cookieUsuario' AND us_session='$cookieSession' AND us_admin=1";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='040000' OR fu_id='040500') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function criticaFormExp() {
	desc = formNE.nome.value;
	cham = formNE.chamada.value;
	if (desc.length == 0) {
		alert('Digite um t�tulo v�lido para a experiência!');
		formNE.nome.focus();
		return;
	}
	if (cham.length == 0) {
		alert('Digite uma chamada v�lida para a experiência!');
		formNE.chamada.focus();
		return;
	}
	formNE.submit();
}

function criticaFormAltera(action) {
	formAD.action = action;
	formAD.submit();
}

function criticaFormAtualiza() {
	nome = formAtualiza.nome.value;
	cham = formAtualiza.chamada.value;
	if (nome.length == 0) {
		alert('Digite um nome v�lido para a experiência!');
		formAtuliza.nome.focus();
		return;
	}
	if (cham.length == 0) {
		alert('Digite uma chamada v�lida para a experiência!');
		formAtuliza.chamada.focus();
		return;
	}

	formAtualiza.submit();
}

function submitFormLE(acao) {
	if (acao == 'libera') {
		formLE.acao.value = 'libera';
	}
	else if (acao == 'pendente') {
		formLE.acao.value = 'pendente';
	}
	else {
		formLE.acao.value = 'cancela';
	}

	formLE.submit();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_setTextOfLayer(objName,x,newText) { //v4.01
  if ((obj=MM_findObj(objName))!=null) with (obj)
    if (document.layers) {document.write(unescape(newText)); document.close();}
    else innerHTML = unescape(newText);
}
//-->
</script>
<style type="text/css">
<!--
.style1 {
	color: #006666;
	font-weight: bold;
}
-->
</style>
<?php include("menu_admin.php"); ?> 

<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="567" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Experi&ecirc;ncias - Libera��o</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
  </tr>
</table>

<?php
			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
				$sql = "select * from experiencia where ex_liberacao='N'";
				$query = $db->query($sql);
?>
	  <form name="formLE" method="post" action="xt_exp_libera_experiencia.php">
	  <input type="hidden" name="acao" value="">
        <table width="567" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td height="24" colspan="5"> <p> <strong>Liberar
                Experiências</strong>
                <?php if (isset($expLibera)) { ?>
                <span class="style1">- experiência(s) liberadas com sucesso !</span>
                <?php } ?>
              </p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000" colspan="5"> <div align="center"></div></td>
          </tr>
          <?php
					$i = 0;
					while ($row = $query->fetch_object()) {
						$data = explode("-",$row->dt_criacao);
?>
          <tr>
            <td> <div align="center">
                <input type="checkbox" name="exp<?php echo $i; ?>" value="<?php echo $row->ex_id; ?>">
              </div></td>
            <td><b><a class="Preto" href="experiencias_libera.php?libera=1&experiencia=<?php echo $row->ex_id; ?>"><?php echo $row->ex_descricao; ?></a></b></td>
            <?php
				$sql = "SELECT * FROM exp_log_cadastro WHERE experiencia=$row->ex_id";
				$queryInt = $db->query($sql);
				$rowInt = $queryInt->fetch_object();
			?>
			<td><b><?php echo $rowInt->nome; ?></b></td>
			<td><b><?php echo $rowInt->email; ?></b></td>
            <td align="center"><b><?php echo "$data[2]/$data[1]/$data[0]"; ?></b></td>
          </tr>
          <?php
						$i++;
					}
?>
          <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5"> <div align="center">
                <input name="Button" Type="button" class="botaoLogin" value="   Liberar   " onClick="submitFormLE('libera');">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5"> <div align="left"> <span>Utilize
                o campo abaixo para justificar o motivo do cancelamento ou mudan&ccedil;a
                de status para &quot;Pendente&quot; para o usu&aacute;rio que
                efetuou este cadastro. Este ir&aacute; receber um e-mail com os
                dados digitados abaixo.</span><br>
                <br>
                <textarea name="justificativa" cols="105" rows="5" wrap="VIRTUAL" id="justificativa"></textarea>
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5"> <div align="center">
                <input name="Button" Type="button" class="botaoLogin" value="   Deixar Pendente   " onClick="submitFormLE('pendente');">
                &nbsp;&nbsp;
                <input name="Button2" Type="button" class="botaoLogin" value="   Cancelar   " onClick="submitFormLE('cancela');">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="5" bgcolor="#000000"><div align="center"></div></td>
          </tr>
        </table>
	  </form>
<?php
  } // fim do if que verifica se o usuário tem acesso a estas informações

?>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
