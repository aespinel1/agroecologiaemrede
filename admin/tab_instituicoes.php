<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

if (isset($tipo)) {
	switch ($tipo) {
		case "NI":
			$varDesc = "Cadastrar INSTITUI��O";
			break;
		case "AD":
			$varDesc = "Alterar DADOS";
			break;
		case "LI":
			$varDesc = "Liberar Institui��es";
			break;
	}
}

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020500') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaFormInst() {
	desc = formNE.nome.value;
	mail = formNE.email.value;
	ag = formNE.agID.value;
	at = formNE.atID.value;

	if (desc.length == 0) {
		alert('Digite um nome válido!');
		formNE.nome.focus();
		return;
	}

	if (ag.length == 0) {
		alert('Selecione pelo menos uma �rea Geogr�fica do seu interesse!');
		formNE.areasGeograficas.focus();
		return;
	}

	if (at.length == 0) {
		alert('Selecione pelo menos uma �rea Tem�tica do seu interesse!');
		formNE.areasTematicas.focus();
		return;
	}

	formNE.submit();
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function insereEndereco() {
	formEnd.action = 'xt_inst_insere_endereco.php';
	formEnd.submit();
}

function insereTelefone() {
	formTel.action = 'xt_inst_insere_telefone.php';
	formTel.submit();
}

function insereArea() {
	formTema.action = 'xt_inst_insere_area.php';
	formTema.submit();
}

function insereGeo() {
	formGeo.action = 'xt_inst_insere_geo.php';
	formGeo.submit();
}

function excluiEndereco(desc,id) {
	inst = formEnd.inst.value;
	if(confirm('Você tem certeza que deseja excluir o endereço de ' + desc + ' para esta instituição?')) {
		window.location.href='xt_instituicao_exclui_endereco.php?inst='+inst+'&id='+id;
	}
}

function excluiTelefone(desc,id) {
	inst = formEnd.inst.value;
	if(confirm('Você tem certeza que deseja excluir o telefone de ' + desc + ' para esta instituição?')) {
		window.location.href='xt_instituicao_exclui_telefone.php?inst='+inst+'&id='+id;
	}
}

function excluiArea(desc,id) {
	inst = formTema.inst.value;
	if(confirm('Você tem certeza que deseja excluir a �rea tem�tica ' + desc + ' para esta instituição?')) {
		window.location.href='xt_inst_exclui_area.php?inst='+inst+'&tema='+id;
	}
}

function excluiGeo(desc,id) {
	inst = formGeo.inst.value;
	if(confirm('Você tem certeza que deseja excluir a �rea geogr�fica ' + desc + ' para esta instituição?')) {
		window.location.href='xt_inst_exclui_geo.php?inst='+inst+'&geo='+id;
	}
}

function submitFormLI(acao) {
	if (acao == 'libera') {
		formLI.acao.value = 'libera';
	}
	formLI.submit();
}
</script>

<?php include("menu_admin.php"); ?>
<table width="589" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="579" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_instituicoes.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Institui&ccedil;&otilde;es</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr>
          <td colspan="2"><p><strong>Op��es</strong></p></td>
        </tr>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td width="283"><img src="imagens/seta02.gif" width="12" height="13">
            <a href="tab_instituicoes.php?tipo=NI" class="Preto">Cadastro de nova
            institui&ccedil;&atilde;o</a><br>
            <br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="tab_instituicoes.php?tipo=AD" class="Preto">Alterar
            dados de uma institui&ccedil;&atilde;o</a></td>
          <td width="283" valign="top"><img src="imagens/seta02.gif" width="12" height="13">
            <a href="tab_instituicoes.php?tipo=LI" class="Preto">Liberar Institui&ccedil;&otilde;es</a></td>
        </tr>
      </table>
      <br>
      <br>
<?php
	if (isset($tipo)) {

		if ($tipo == "LI") {

				$sql = "SELECT * FROM frm_instituicao WHERE in_liberacao='N'";
				$query = $db->query($sql);
?>
	  <form name="formLI" method="post" action="xt_inst_libera_instituicao.php">
	  <input type="hidden" name="acao" value="">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td height="24" colspan="5"> <p> <strong>Liberar
                Institui��es</strong>
                <?php if (isset($instLibera)) { ?>
                - instituição(ões) liberadas com sucesso!
                <?php } ?>
              </p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000" colspan="5"> <div align="center"></div></td>
          </tr>
          <tr bgcolor="#CCCCCC">
            <td><div align="center"></div></td>
            <td><b>Nome</b></td>
            <td><b>E-mail</b></td>
            <td><b>Website</b></td>
            <td><b>Data de Entrada</b></td>
          </tr>
          <?php
					$i = 0;
					while ($row = $query->fetch_object()) {
?>
          <tr>
            <td> <div align="center">
                <input type="checkbox" name="inst<?php echo $i; ?>" value="<?php echo $row->in_id; ?>">
              </div></td>
            <td><b><?php echo $row->in_nome; ?></b></td>
            <td><b><?php echo $row->in_email; ?></b></td>
            <td><b><?php echo $row->in_url; ?></b></td>
            <td><b><?php echo $row->in_data_proc; ?></b></td>
          </tr>
          <?php
						$i++;
					}
?>
          <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5"> <div align="center">
                <input name="Button" Type="button" class="botaoLogin" value="   Liberar   " onClick="submitFormLI('libera');">
              </div></td>
          </tr>
        </table>
	  </form>
<?php
		}
		else if ($tipo == "NI") {

			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
?>
			<form name="formNE" method="post" action="xt_inst_insere_instituicao.php">

        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="4"> <p> <strong><?php echo $varDesc; ?></strong>
              </p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td > <div align="right">Nome:</div></td>
            <td colspan="3"><input name="nome" type="text" id="nome" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">E-mail:</div></td>
            <td colspan="3"><input name="email" type="text" id="login" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">Website:</div></td>
            <td colspan="3"><input name="url" type="text" id="login" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">Tipo de Inst.:</div></td>
            <td colspan="3"><select name="tipo" id="tipo">
                <?php
			$sql = "SELECT * FROM tipo_instituicoes ORDER BY ti_descricao";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->ti_id; ?>"><?php echo trim(traduz($row->ti_descricao)); ?></option>
                <?php
			} // la�o do loop
?>
              </select></td>
          </tr>
          <tr>
            <td> <div align="right">Endere&ccedil;o:</div></td>
            <td colspan="3"><input name="endereco" type="text" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">Telefone:</div></td>
            <td   colspan="3" width="214"><input name="fone" type="text" id="fone" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">Cidade:</div></td>
            <td> <input name="cidade" type="text" maxlength="255">
            </td>
            <td> <div align="right">Estado:</div></td>
            <td><input name="estado" type="text" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">CEP/Zip:</div></td>
            <td><input name="cep" type="text" maxlength="80"></td>
            <td> <div align="right">Pa&iacute;s:</div></td>
            <td><input name="pais" type="text" maxlength="80"></td>
          </tr>
          <tr>
            <td colspan="4"><table width="567" border="0" cellspacing="1" cellpadding="3">
                <tr>
                  <td width="248" valign="top"><div align="right"><strong>&Aacute;reas
                      Geogr&aacute;ficas</strong>:</div></td>
                  <td width="304"><a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_ag_nav.php?url=cadastro_instituicoes.php','ag','scrollbars=yes,width=517,height=300,status=yes')">Clique
                    aqui para adicionar<strong> &Aacute;reas Geogr&aacute;ficas</strong></a><br>
                    <textarea name="areasGeograficas" cols="50" rows="5" wrap="VIRTUAL" readonly></textarea>
                    <input type="hidden" name="agID"> </td>
                </tr>
                <tr>
                  <td valign="top"><div align="right"><strong>&Aacute;reas
                      Tem&aacute;ticas</strong>:</div></td>
                  <td><a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_at_nav.php?url=cadastro_instituicoes.php','at','scrollbars=yes,width=517,height=300,status=yes')">Clique
                    aqui para adicionar<strong> &Aacute;reas Tem&aacute;ticas</strong></a><br>
                    <textarea name="areasTematicas" cols="50" rows="5" wrap="VIRTUAL" readonly></textarea>
                    <input type="hidden" name="atID"> </td>
                </tr>
              </table></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input type="button" class="botaoLogin" value="   Incluir   " onClick="criticaFormInst();">
              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
	  </form>
	  <?php if (isset($instNova)) { ?>
	  <p align="center">Instituição <b><?php echo $instNova; ?></b> cadastrada com sucesso!</p>
	  <?php } ?>
<?php
			} // fim do if que verifica se o usuário tem acesso a esta fun��o
		}
		else if ($tipo == "AD") {

			if ($numPerm == 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {
?>
	  <form name="formAD" method="post" action="tab_instituicoes.php?tipo=AD">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="4">
			<p>
			<strong>Selecione</strong>
			</p>
			</td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="67"> <div align="center">Instituição:</div></td>
            <td width="477">
			<select name="inst">
<?php
			$sql = "SELECT * FROM frm_instituicao ORDER BY in_nome";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
				$varSelected = "";
				if (isset($inst)) {
					if ($inst == $row->in_id) {
						$varNome = $row->in_nome;
						$varEmail = $row->in_email;
						$varURL = $row->in_url;
						$varTipo = $row->in_tipo;
						$varSelected = "selected";
					}
				}
?>
                <option <?php echo $varSelected; ?> value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
                <?php
			} // la�o do loop
?>
              </select>
              <input name="Submit" type="submit" class="botaoLogin" value=" Selecionar ">
            </td>
          </tr>
          <tr bgcolor="#000000">
            <td colspan="4"><div align="center"> </div></td>
          </tr>
        </table>
	  </form>
<?php
			if (isset($inst)) {
?>
		<form name="formNI" method="post" action="xt_inst_atualiza_instituicao.php">
		<input type="hidden" name="inst" value="<?php echo $inst; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="4"> <p> <strong>Dados da instituição</strong>
              </p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="72"> <div align="right">Nome:</div></td>
            <td colspan="3"><input name="nome" type="text" id="nome" size="89" maxlength="255" value="<?php echo $varNome; ?>"></td>
          </tr>
          <tr>
            <td> <div align="right">E-mail:</div></td>
            <td colspan="3"><input name="email" type="text" id="login" size="89" maxlength="255" value="<?php echo $varEmail; ?>"></td>
          </tr>
          <tr>
            <td> <div align="right">Website:</div></td>
            <td colspan="3"><input name="url" type="text" id="login" size="89" maxlength="255" value="<?php echo $varURL; ?>"></td>
          </tr>
          <tr>
            <td> <div align="right">Tipo:</div></td>
            <td colspan="3"><select name="tipo" id="tipo">
                <?php
			$sql = "SELECT * FROM tipo_instituicoes ORDER BY ti_descricao";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
				$varSelected = "";
				if ($varTipo == $row->ti_id) {
					$varSelected = "selected";
				}
?>
                <option <?php echo $varSelected; ?> value="<?php echo $row->ti_id; ?>"><?php echo trim(traduz($row->ti_descricao)); ?></option>
                <?php
			} // la�o do loop
?>
              </select></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input type="submit" class="botaoLogin" value="   Atualizar   ">
              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
	  </form>
	  <br>
	<form name="formTema" method="post">
		<input type="hidden" name="inst" value="<?php echo $inst; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="4"> <p> <strong>Áreas Temáticas</strong>&nbsp;&nbsp;
                [ <a href="tab_instituicoes.php?inst=<?php echo $inst; ?>&novaArea=1&tipo=AD" class="Preto">Nova �rea Tem�tica</a>
                ]</p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
<?php
		if (isset($novaArea)) {
?>
          <tr>
            <td width="86"> <div align="right">�rea:</div></td>
            <td width="458" colspan="3">
		 <select name="tema" id="tema">
<?php
			function untree($parent, $level) {
				$parentsql = $db->query("SELECT * FROM area_tematica WHERE at_pertence=$parent ORDER BY at_descricao");
			    if (!$parentsql->num_rows) {
				    return;
			    }
	    		else {
			        while($branch = $parentsql->fetch_object()) {
	    		        $echo_this = "<option value=\"$branch->at_id\">";
		                for ($x=1; $x<=$level; $x++) {
	    		            $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;";
		                }
						$echo_this .= "- $branch->at_descricao</option>";
			            echo $echo_this;
	    		        $rename_level = $level;
		    	        untree($branch->at_id, ++$rename_level);
        		    }
		        }
		    }

			$sql = "SELECT * FROM area_tematica WHERE at_pertence = 0 ORDER BY at_descricao";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->at_id; ?>"><?php echo trim($row->at_descricao); ?></option>
<?php
			    untree($row->at_id, 1);
			} // la�o do loop
?>
              </select>
			</td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input type="button" class="botaoLogin" onClick="insereArea();" value="   Inserir �rea Tem�tica   ">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>

<?php
		} // fim do que verifica se foi clicado para um novo endereço

		$sql = "SELECT * FROM rel_area_inst,frm_instituicao,area_tematica WHERE rai_id_inst=in_id AND ".
			   "rai_id_area=at_id AND rai_id_inst=$inst ORDER BY at_descricao";
		$query = $db->query($sql);
		$i = 0;
		while ($row = $query->fetch_object()) {
?>
		<input type="hidden" name="areaID<?php echo $i; ?>" value="<?php echo trim($row->at_id); ?>">
          <tr>
            <td  width="86"> <div align="right">�rea:</div></td>
            <td width="458" colspan="3"><strong><?php echo trim($row->at_descricao); ?></strong> [ <a href="#" onClick="excluiArea('<?php echo trim($row->at_descricao); ?>','<?php echo trim($row->at_id); ?>');" class="Preto">Excluir</a> ] </td>
          </tr>
<?php
			$i++;
		} // la�o do loop
?>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
		</form>
		<br>
	<form name="formGeo" method="post">
		<input type="hidden" name="inst" value="<?php echo $inst; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="4"> <p> <strong>Áreas Geográficas</strong>&nbsp;&nbsp;
                [ <a href="tab_instituicoes.php?inst=<?php echo $inst; ?>&novaGeo=1&tipo=AD" class="Preto">Nova �rea Geogr�fica</a>
                ]</p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
<?php
		if (isset($novaGeo)) {
?>
          <tr>
            <td width="86"> <div align="right">�rea:</div></td>
            <td width="458" colspan="3">
		 <select name="geo" id="tema">
<?php
			function untree($parent, $level) {
				$parentsql = $db->query("SELECT * FROM area_geografica WHERE ag_pertence=$parent ORDER BY ag_descricao");
			    if (!$parentsql->num_rows) {
				    return;
			    }
	    		else {
			        while($branch = $parentsql->fetch_object()) {
	    		        $echo_this = "<option value=\"$branch->ag_id\">";
		                for ($x=1; $x<=$level; $x++) {
	    		            $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;";
		                }
						$echo_this .= "- $branch->ag_descricao</option>";
			            echo $echo_this;
	    		        $rename_level = $level;
		    	        untree($branch->ag_id, ++$rename_level);
        		    }
		        }
		    }

			$sql = "SELECT * FROM area_geografica WHERE ag_pertence = 0 ORDER BY ag_descricao";
			echo $sql;
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->ag_id; ?>"><?php echo trim($row->ag_descricao); ?></option>
<?php
			    untree($row->ag_id, 1);
			} // la�o do loop
?>
              </select>
			</td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input type="button" class="botaoLogin" onClick="insereGeo();" value="   Inserir �rea Geogr�fica   ">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>

<?php
		} // fim do que verifica se foi clicado para um novo endereço

		$sql = "SELECT * FROM rel_geo_inst,frm_instituicao,area_geografica WHERE rgi_id_inst=in_id AND ".
			   "rgi_id_geo=ag_id AND rgi_id_inst=$inst ORDER BY ag_descricao";
		$query = $db->query($sql);
		$i = 0;
		while ($row = $query->fetch_object()) {
?>
		<input type="hidden" name="geoID<?php echo $i; ?>" value="<?php echo trim($row->ag_id); ?>">
          <tr>
            <td  width="86"> <div align="right">�rea:</div></td>
            <td width="458" colspan="3"><strong><?php echo trim($row->ag_descricao); ?></strong> [ <a href="#" onClick="excluiGeo('<?php echo trim($row->ag_descricao); ?>','<?php echo trim($row->ag_id); ?>');" class="Preto">Excluir</a> ] </td>
          </tr>
<?php
			$i++;
		} // la�o do loop
?>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
		</form>
		<br>
		<form name="formEnd" method="post" action="xt_inst_atualiza_endereco.php">
		<input type="hidden" name="inst" value="<?php echo $inst; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="4"> <p> <strong>Endereços</strong>&nbsp;&nbsp;
                [ <a href="tab_instituicoes.php?tipo=AD&inst=<?php echo $inst; ?>&novoEndereco=1" class="Preto">Novo endereço</a>
                ]</p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
<?php
		if (isset($novoEndereco)) {
?>
          <tr>
            <td> <div align="right">Tipo:</div></td>
            <td colspan="3">
		 <select name="tipo">
<?php
			$sql = "SELECT * FROM tipo_enderecos ORDER BY te_descricao";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->te_id; ?>"><?php echo trim($row->te_descricao); ?></option>
                <?php
			} // la�o do loop
?>
              </select>
			</td>
          </tr>
          <tr>
            <td width="72"> <div align="right">Endere&ccedil;o:</div></td>
            <td colspan="3"><input name="endereco" type="text" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">Cidade:</div></td>
            <td width="120">
              <input name="cidade" type="text" maxlength="255">
              </td>
            <td width="103"> <div align="right">Estado:</div></td>
            <td width="227"><input name="estado" type="text" maxlength="255"></td>
          </tr>
          <tr>
            <td> <div align="right">CEP/Zip:</div></td>
            <td><input name="cep" type="text" maxlength="80"></td>
            <td> <div align="right">Pa&iacute;s:</div></td>
            <td><input name="pais" type="text" maxlength="80"></td>
          </tr>
		   <tr>
              <td> <div align="right">Telefone:</div></td>
              <td   colspan="3" width="214"><input name="fone" type="text" id="fone" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input type="button" class="botaoLogin" onClick="insereEndereco();" value="   Inserir Endereços Adicionais   ">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>

<?php
		} // fim do que verifica se foi clicado para um novo endereço

		$sql = "SELECT a.*, b.*, c.nome as cidade, c.UF FROM frm_instituicao as a,tipo_enderecos as b, _BR as c WHERE ei_id_endereco=te_id AND ei_cidade=c.id AND ".
			   "ei_id_instituicao=$inst ORDER BY te_descricao";
		$query = $db->query($sql);
		$i = 0;
		while ($row = $query->fetch_object()) {
?>
		<input type="hidden" name="tipoID<?php echo $i; ?>" value="<?php echo trim($row->te_id); ?>">
		<input type="hidden" name="id<?php echo $i; ?>" value="<?php echo trim($row->ei_id); ?>">
          <tr>
            <td> <div align="right">Tipo:</div></td>
            <td colspan="3"><strong><?php echo trim($row->te_descricao); ?></strong> [ <a href="#" onClick="excluiEndereco('<?php echo trim($row->te_descricao); ?>','<?php echo trim($row->ei_id); ?>');" class="Preto">Excluir</a> ] </td>
          </tr>
          <tr>
            <td width="72"> <div align="right">Endere&ccedil;o:</div></td>
            <td colspan="3"><input name="endereco<?php echo $i; ?>" type="text" size="89" maxlength="255" value="<?php echo trim($row->ei_endereco); ?>"></td>
          </tr>
          <tr>
            <td> <div align="right">Cidade:</div></td>
            <td width="120">
              <input name="cidade<?php echo $i; ?>" type="text" maxlength="255" value="<?php echo trim($row->cidade); ?>">
              </td>
            <td width="103"> <div align="right">Estado:</div></td>
            <td width="227"><input name="estado<?php echo $i; ?>" type="text" maxlength="255" value="<?php echo trim($row->UF); ?>"></td>
          </tr>
          <tr>
            <td> <div align="right">CEP/Zip:</div></td>
            <td><input name="cep<?php echo $i; ?>" type="text" maxlength="80" value="<?php echo trim($row->ei_cep); ?>"></td>
            <td> <div align="right">Pa&iacute;s:</div></td>
            <td><input name="pais<?php echo $i; ?>" type="text" maxlength="80" value="<?php echo trim($row->ei_pais); ?>"></td>
          </tr>
		  <tr>
            <td> <div align="right">Telefone:</div></td>
            <td   colspan="3" width="214"><input name="fone" type="text" id="fone" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
<?php
			$i++;
		} // la�o do loop
?>
          <tr>
            <td colspan="4"><div align="center">
                <input name="button2" type="submit" class="botaoLogin" value="   Atualizar   ">
              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
		</form>
		<br>
		<form name="formTel" method="post" action="xt_inst_atualiza_telefone.php">
		<input type="hidden" name="inst" value="<?php echo $inst; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="4"> <p> <strong>Telefones</strong>&nbsp;&nbsp;
                [ <a href="tab_instituicoes.php?tipo=AD&inst=<?php echo $inst; ?>&novoTelefone=1" class="Preto">Novo telefone</a>
                ]</p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
<?php
		if (isset($novoTelefone)) {
?>
          <tr>
            <td> <div align="right">Tipo:</div></td>
            <td colspan="3">
		 <select name="tipo">
<?php
			$sql = "SELECT * FROM tipo_telefones ORDER BY tt_descricao";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->tt_id; ?>"><?php echo trim($row->tt_descricao); ?></option>
                <?php
			} // la�o do loop
?>
              </select>
			</td>
          </tr>
          <tr>
            <td width="72"> <div align="right">Telefone:</div></td>
            <td colspan="3"><input name="fone" type="text" size="89" maxlength="255"></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input type="button" class="botaoLogin" onClick="insereTelefone();" value="   INSERIR TELEFONE   ">
              </div></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>

<?php
		} // fim do que verifica se foi clicado para um novo telefone

		$sql = "SELECT * FROM telefone_instituicao,tipo_telefones WHERE ti_id_telefone=tt_id AND ".
			   "ti_id_instituicao=$inst ORDER BY tt_descricao";
		$query = $db->query($sql);
		$i = 0;
		while ($row = $query->fetch_object()) {
?>
		<input type="hidden" name="tipoID<?php echo $i; ?>" value="<?php echo $row->tt_id; ?>">
		<input type="hidden" name="id<?php echo $i; ?>" value="<?php echo trim($row->ti_id); ?>">
          <tr>
            <td> <div align="right">Tipo:</div></td>
            <td colspan="3"><strong><?php echo trim($row->tt_descricao); ?></strong> [ <a href="#" onClick="excluiTelefone('<?php echo trim($row->tt_descricao); ?>','<?php echo trim($row->ti_id); ?>');" class="Preto">Excluir</a> ] </td>
          </tr>
          <tr>
            <td width="72"> <div align="right">N�mero</div></td>
            <td colspan="3"><input name="telefone<?php echo $i; ?>" type="text" size="40" maxlength="255" value="<?php echo trim($row->ti_telefone); ?>"></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
<?php
			$i++;
		} // la�o do loop
?>
          <tr>
            <td colspan="4"><div align="center">
                <input type="submit" class="botaoLogin" value="   ATUALIZAR DADOS   ">
              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
		</form>


<?php
			} // fim do if que verifica se uma instituição foi selecionada

			} // fim do if que verifica se o usuário tem acesso a estas informações

		} // fim do if do tipo

	} // fim do if que checa se alguma op��o foi selecionada
?>
      <p>&nbsp;</p>
      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
