<?php
include("conexao.inc.php");
?>

<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaForm() {
	login = formLogin.login.value;
	if (login.length == 0) {
		alert('Digite sua indentificação  !');
		formLogin.login.focus();
		return;
	}
	formLogin.submit();
}
</script>
<?php include("menu_admin.php"); ?>

<div class="container">
	<div class="row mb-4">
    <div class="col-md-12">
      <div class="alert alert-success">
        <h1><i class="oi oi-cog"></i> Entrar como administrador/a</h1>
      </div>
    </div>
  </div>
	<?php if (isset($authFailed))	 { ?>
		<div class="row">
			<div class="col-sm-12 col-md-8 col-lg-6 mx-auto">
				<div class="alert alert-danger">
					Identificação e/ou senha inválidos!
				</div>
			</div>
		</div>
	<?php } ?>
	<div class="row">
		<div class="col-sm-12 col-md-8 col-lg-6 mx-auto">
			<form id="formLogin" method="post" action="xt_loga.php" class="needs-validation">
				<div class="form-row mb-2">
				 	<div class="col-md-3">
						<label for="frm_login">Identificação</label>
					</div>
					<div class="col-md-9">
						<input if="frm_login" type="text" class="form-control" name="login" required>
					</div>
				</div>
				<div class="form-row mb-2">
				 	<div class="col-md-3">
						<label for="frm_senha">Senha</label>
					</div>
					<div class="col-md-9">
						<input id="frm_senha" type="password" class="form-control" name="senha" required>
					</div>
				</div>
				<div class="form-row">
        	<input name="botao" type="submit" class="btn btn-primary" value="<?=$txt['login_titulo_conectar']?>">&nbsp;
				</div>
      </form>
		</div>
	</div>
</div>
</body>
</html>
