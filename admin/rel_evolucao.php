<?php
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">

<?php
include("menu_admin.php");
?>
<br>
<br>

<table width="580" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="570" valign="top"> <table width="558" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="477" bgcolor="#80AAD2"> <p><strong>Evolu&ccedil;&atilde;o do Cadastro </strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
	  <br>
	  <br>
<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

$sql   = "SELECT 'Usuários       ' AS 'Tipo_Cadastro', '2004 (até)' as 'Ano' , ' - ' as 'Mês'  , count( * )  as 'Total' ";
$sql .= " FROM usuario ";
$sql .= " UNION  ";
$sql .= " SELECT 'Pesquisas ', '2004 (até)' , ' - '  , count( * )  ";
$sql .= " FROM pesquisa ";
$sql .= " WHERE year( dt_criacao ) < 2005 ";
$sql .= " UNION  ";
$sql .= " SELECT 'Experiências ', '2004 (até)' , ' - '  , count( * )  ";
$sql .= " FROM experiencia ";
$sql .= " WHERE year( dt_criacao ) < 2005 ";
$sql .= " UNION ";
$sql .= " SELECT 'Instituição ', '2004 (até)' , ' - ' , count( * ) ";
$sql .= " FROM frm_instituicao ";
$sql .= " WHERE year( dt_criacao ) < 2005 ";
$sql .= " union ";
$sql .= " SELECT 'Usuários       ' AS 'Tipo Cadastro', year( dt_criacao ) , month( dt_criacao ) , count( * )  as 'Total' ";
$sql .= " FROM usuario ";
$sql .= " WHERE year( dt_criacao ) >=2005 ";
$sql .= " GROUP BY year( dt_criacao ) , month( dt_criacao ) ";
$sql .= " UNION ";
$sql .= " SELECT 'Pesquisas ', year( dt_criacao ) , month( dt_criacao ) , count( * ) ";
$sql .= " FROM pesquisa ";
$sql .= " WHERE year( dt_criacao ) >=2005 ";
$sql .= " GROUP BY year( dt_criacao ) , month( dt_criacao ) ";
$sql .= " UNION  ";
$sql .= " SELECT 'Experiências ', year( dt_criacao ) , month( dt_criacao ) , count( * ) ";
$sql .= " FROM experiencia ";
$sql .= " WHERE year( dt_criacao ) >=2005 ";
$sql .= " GROUP BY year( dt_criacao ) , month( dt_criacao ) ";
$sql .= " UNION ";
$sql .= " SELECT 'Instituição ', year( dt_criacao ) , month( dt_criacao ) , count( * )  ";
$sql .= " FROM frm_instituicao ";
$sql .= " WHERE year( dt_criacao ) >=2005 ";
$sql .= " GROUP BY year( dt_criacao ) , month( dt_criacao ) ";
$sql .= " ORDER BY 2 desc, 3 desc, 1 ";

$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}

$arquivo = "rel_evolucao";

$arquivotemp = tempnam($dir['upload'], "relacao_evolucao").'.csv';

$abrir=fopen($arquivotemp,"w");

$conteudo = "Agroecologia em Rede";
fwrite($abrir,$conteudo."\n");

$conteudo = "Posição Cadastral - Movimento de Inclusão";
fwrite($abrir,$conteudo."\n");

$conteudo = " ";
fwrite($abrir,$conteudo."\n");
fwrite($abrir,$conteudo."\n");
$conteudo = "'Tipo Cadastro';'Ano';'Mês';'Total'";
fwrite($abrir,$conteudo."\n");
$conteudo = " ";
fwrite($abrir,$conteudo."\n");

while ($rowcriterio = $query->fetch_object()) {
                $conteudo =  "'$rowcriterio->Tipo_Cadastro';'$rowcriterio->Ano';'$rowcriterio->Mês';'$rowcriterio->Total';";
                fwrite($abrir,$conteudo."\n");
			}


$conteudo = " ";
fwrite($abrir,$conteudo."\n");
fwrite($abrir,$conteudo."\n");



$sql   = "SELECT 'Usuários       ' AS 'Tipo_Cadastro', '2004 (até)' as 'Ano' , ' - ' as 'Mês'  , count( * )  as 'Total' ";
$sql .= " FROM usuario ";
$sql .= " UNION  ";
$sql .= " SELECT 'Pesquisas ', '2004 (até)' , ' - '  , count( * )  ";
$sql .= " FROM pesquisa ";
$sql .= " WHERE year( dt_atualizacao ) < 2005 ";
$sql .= " UNION  ";
$sql .= " SELECT 'Experiências ', '2004 (até)' , ' - '  , count( * )  ";
$sql .= " FROM experiencia ";
$sql .= " WHERE year( dt_atualizacao ) < 2005 ";
$sql .= " UNION ";
$sql .= " SELECT 'Instituição ', '2004 (até)' , ' - ' , count( * ) ";
$sql .= " FROM frm_instituicao ";
$sql .= " WHERE year( dt_atualizacao ) < 2005 ";
$sql .= " union ";
$sql .= " SELECT 'Usuários       ' AS 'Tipo Cadastro', year( dt_atualizacao ) , month( dt_atualizacao) , count( * )  as 'Total' ";
$sql .= " FROM usuario ";
$sql .= " WHERE year( dt_atualizacao ) >=2005 ";
$sql .= " GROUP BY year( dt_atualizacao ) , month( dt_atualizacao ) ";
$sql .= " UNION ";
$sql .= " SELECT 'Pesquisas ', year( dt_atualizacao ) , month( dt_atualizacao ) , count( * ) ";
$sql .= " FROM pesquisa ";
$sql .= " WHERE year( dt_atualizacao ) >=2005 ";
$sql .= " GROUP BY year( dt_atualizacao ) , month( dt_atualizacao ) ";
$sql .= " UNION  ";
$sql .= " SELECT 'Experiências ', year( dt_atualizacao ) , month( dt_atualizacao ) , count( * ) ";
$sql .= " FROM experiencia ";
$sql .= " WHERE year( dt_atualizacao ) >=2005 ";
$sql .= " GROUP BY year( dt_atualizacao ) , month( dt_atualizacao ) ";
$sql .= " UNION ";
$sql .= " SELECT 'Instituição ', year( dt_atualizacao ) , month( dt_atualizacao ) , count( * )  ";
$sql .= " FROM frm_instituicao ";
$sql .= " WHERE year( dt_atualizacao ) >=2005 ";
$sql .= " GROUP BY year( dt_atualizacao ) , month( dt_atualizacao ) ";
$sql .= " ORDER BY 2 desc, 3 desc, 1 ";

$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}

$conteudo = "Posição Cadastral - Movimento de Alteração ";
fwrite($abrir,$conteudo."\n");

$conteudo = " ";
fwrite($abrir,$conteudo."\n");
fwrite($abrir,$conteudo."\n");
$conteudo = "'Tipo Cadastro';'Ano';'Mês';'Total'";
fwrite($abrir,$conteudo."\n");
$conteudo = " ";
fwrite($abrir,$conteudo."\n");

while ($rowcriterio = $query->fetch_object()) {
                $conteudo =  "'$rowcriterio->Tipo_Cadastro';'$rowcriterio->Ano';'$rowcriterio->Mês';'$rowcriterio->Total';";
                fwrite($abrir,$conteudo."\n");
			}

//$conteudo = "";
//fwrite($abrir,$conteudo."\n");

fclose($abrir);
chmod($arquivotemp, 0777);

$db->close();
echo "<br><br><br>";
echo "<center><a href='".$dir['upload_URL'].basename($arquivotemp)."'>Clique com o botão direito do mouse nesse link, selecione 'salvar destino como', confirme o nome do arquivo, assim o arquivo será baixado em seu computador.</a></center>";
echo "<br><br><br><center><a href='main.php'>Retornar à página principal</a></center>";
?>
