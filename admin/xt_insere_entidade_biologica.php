<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020300') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;

if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você n�o tem acesso a inserir estas informações! Clique <a href=\"main.php\">aqui</a> para voltar para a página princiapal.</p>";
	$db->disconnect;
	return;
}

if (strlen(trim($idExterno)) == 0) {
	$idExterno = 0;
}
if (strlen(trim($idFamilia)) == 0) {
	$idFamilia = 0;
}
if (strlen(trim($idGenero)) == 0) {
	$idGenero = 0;
}
if (strlen(trim($idEspecie)) == 0) {
	$idEspecie = 0;
}
	
$sql = "INSERT INTO entidade_biologica (eb_id_externo,eb_familia,eb_genero,eb_especie,eb_infra_especie) ".
	   "VALUES ($idExterno,$idFamilia,$idGenero,$idEspecie,'$infra')";
$query = $db->query($sql);
if (!$query) {
   die($db->error);
}

$sql = "SELECT max(eb_id) AS id FROM entidade_biologica";
$query = $db->query($sql);
$row = $query->fetch_object();
$ebID = $row->id;
if (strlen($nomes) > 0) {
	$array = explode(",",$nomes);
	for ($i = 0; $i < count($array); $i++) {
		$nomeVulgar = trim($array[$i]);
		$tam = strlen($nomeVulgar);
		if ($tam > 0) {
			$sql = "INSERT INTO eb_nome_vulgar VALUES ($ebID,'$nomeVulgar')";
			$query = $db->query($sql);
			if (!$query) {
   				die($db->error);
			}
		}
	}
}

$db->close();
		
?>
<script language="JavaScript">
	window.location.href='entidades_biologicas.php?novaEntidade=<?php echo $ebID; ?>';
</script>
