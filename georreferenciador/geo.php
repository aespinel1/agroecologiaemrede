<?php
//coding:utf-8 
//por daniel tygel (dtygel em fbes pt org pt br) em 2008
//Aqui envio as informações de cabeçalho:
header('Content-type: text/html; charset=utf-8');

require_once "../../public_html/farejador/config.php";
//Aqui eu recupero arquivos de configuração, funções comuns do programa, funções de acentuação, e funções de Formulário (se estou na área restrita):
require_once "../../public_html/farejador/funcoes_comuns.php";
require_once "../../public_html/farejador/conversor_acentos_funcoes.php";
//Faço a conexão ao banco de dados:
$con=conecta($bd);
mysql_select_db('agrorede');
mysql_set_charset('LATIN1');
if (mysql_error()) { echo mysql_error(); exit; }

$sql="SELECT a.ex_id, a.ex_descricao, a.ex_resumo, a.ex_chamada, a.dt_atualizacao, a.txt_comentario, b.ea_arquivo, GROUP_CONCAT(DISTINCT at_descricao SEPARATOR '|') as tema, GROUP_CONCAT(DISTINCT ag_descricao SEPARATOR '|') as local FROM `experiencia` as a LEFT JOIN experiencia_arquivo as b ON a.ex_id=b.ea_id_experiencia LEFT JOIN experiencia_autor as c ON a.ex_id=c.exa_id_experiencia LEFT JOIN usuario as d ON c.exa_id_usuario=d.us_login LEFT JOIN rel_area_experiencia as e ON a.ex_id=e.rae_id_experiencia LEFT JOIN area_tematica as f ON e.rae_id_area=f.at_id LEFT JOIN rel_geo_experiencia as g ON a.ex_id=g.rge_id_experiencia LEFT JOIN area_geografica as h ON h.ag_id=g.rge_id_geo WHERE a.lat=0 GROUP BY a.ex_id ORDER BY a.ex_id ASC";

	$dados=faz_query($sql,$con,'array');
	$nao_pode=array('Brasil', 'Nordeste', 'Sudeste', 'Sul', 'Centro-Oeste', 'Norte');
   	foreach ($dados['ex_id'] as $k=>$id) {   	
		unset($long,$lat,$cep,$precisao,$xmltmp,$sq,$arvore,$cidade,$d);
		$d=array('latitude_ibge'=>0,'longitude_ibge'=>0);
		$local=explode('|',$dados['local'][$k]);
		$n=count($local)-1;
		for ($i=$n;$i>-1;$i--)
			if (!in_array($local[$i],$nao_pode))
				$d['end'].=$local[$i].', ';
		$d['end']=substr($d['end'],0,strlen($d['end'])-2);
		echo '<b>ID = '.$id.'</b><br>';
		$status=georreferenciadoragrorede(true);
		print_r($d);
		echo '<br><hr><br>';
		//Agora que foi georreferenciado, insiro na tabela cons_dadosbasicos:
		if ($d['lat'] && $d['long']) {
			$sql="UPDATE experiencia SET `lat`=".$d['lat'].", `lng`=".$d['long'];
			$sql.=" WHERE ex_id=".$id;
			$res=mysql_query($sql,$con);
			if (mysql_error()) {echo mysql_error(); exit; }
		}
		
		usleep(1800000);
	} // fim do loop nos dados
	
	
function georreferenciadoragrorede($mostra=false) {
	global $erros,$key,$d;

	unset($long,$lat,$cep,$precisao,$xmltmp,$sq,$arvore,$cidade);
	$endtmp=str_replace(array(chr(10),chr(13)),'',strip_tags($d['end']));
	$ruins=array(',','.','-',':','/','º','ª','  ',' NR ',' Nr ',' nr ',' n ',' N ', ' No ',' NO ');
	//$endtmp=str_replace($ruins,' ',$endtmp);
	$endtmp.=', BRA';
	$url="http://maps.google.com.br/maps/geo";
	$variaveis="q=".$endtmp;
	$variaveis.="&output=xml&key=".$key;
	$endereco=$url.'?'.$variaveis;
	// Aqui pego os dados do google:
	//$xmltmp = simplexml_load_file(str_replace(' ','+',utf8_encode($endereco))) or die("não consegui carregar!");
	//echo $endereco;exit;
	
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, str_replace(' ','+',$endereco));
	curl_setopt($ch, CURLOPT_HEADER,0); //Change this to a 1 to return headers
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER[‘HTTP_USER_AGENT’]);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$data = curl_exec($ch);
	curl_close($ch);
	$xmltmp=simplexml_load_string(utf8_encode($data));

	// Parse the returned XML file		
	$status=$xmltmp->Response->Status->code;
	// Aqui é pro caso de erros mais graves, tais como o 620! O código parará:		
	if (in_array($status,$erros)) {
		echo "Deu pau no processo de georreferenciamento. O código do erro é ".$status.'.<br>';
		echo "Eu tinha feito o seguinte pedido: <br>";
		echo str_replace(' ','+',$endereco).'<br><br><br>';
		echo '<h2>Tchau tchau :-(</h2>';
		exit;
	}
	$cep=$xmltmp->Response->Placemark->AddressDetails->Country->AdministrativeArea->Locality->PostalCode->PostalCodeNumber;
	$cidade=utf8_encode($xmltmp->Response->Placemark->AddressDetails->Country->AdministrativeArea->Locality->LocalityName);
	$uf=$xmltmp->Response->Placemark->AddressDetails->Country->AdministrativeArea->AdministrativeAreaName;
	$bairro = utf8_encode($xmltmp->Response->Placemark->AddressDetails->Country->AdministrativeArea->Locality->DependentLocality->DependentLocalityName);
	$precisao=$xmltmp->Response->Placemark->AddressDetails['Accuracy'];
	$coord = $xmltmp->Response->Placemark->Point->coordinates;
	//$coincide_cidade = (strtoupper($cidade)==strtoupper(utf8_encode($d['cidade'])) || strtoupper($bairro)==strtoupper(utf8_encode($d['cidade'])));

	if ( $status=='200')
			{					
			// Parse the coordinate stringSELECT * FROM `cons_dadosbasicos` WHERE 1
			list($long, $lat, $alt) = explode(",",$coord);			
			if ($lat && $long) {
					$d['long']=$long;
					$d['lat']=$lat;
					$d['cep']=$cep;
					$d['geomodificou']=$precisao;
			} else {
				echo 'fodeu!';
				/*	$d['long']=$d['longitude_ibge'];
					$d['lat']=$d['latitude_ibge'];
					$d['cep']=$d['cep'];*/
					$d['geomodificou']=-1;
			}
		} else {
			$d['geomodificou']=-1;
	}

	if ($status=='200') {
			echo 'id='.$id_sies.' / status='.$status.' / precisao='.$precisao.' / cidade_fonte='.$d['cidade'].' / cidade google='.$cidade.'<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$endtmp.'<br>';
			echo '<br>';
	}
	
	return $status;
}
?>
