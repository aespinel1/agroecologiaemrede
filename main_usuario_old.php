<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");
$sql = "SELECT * FROM usuario WHERE us_login='$cookieUsuario' AND us_session='$cookieSession'";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}
$row = $query->fetch_object();
$usr = $row->us_login;
$psd = $row->us_senha;
$nom = $row->us_nome;
$adm = $row->us_admin;

$sql = "SELECT count(log_usuario) as qtAcesso FROM log_acesso WHERE log_usuario='$cookieUsuario'";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}
$row = $query->fetch_object();
$cnt = $row->qtAcesso;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<?php include("menu_geral.php"); ?>
<table width="760" border="0" cellpadding="10" cellspacing="0">
  <tr>

    <td width="700" valign="top">
      <table width="565" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="262" bgcolor="#80AAD2"><p >Bem-vindo, <strong><?php echo $nom; ?></strong></p></td>
          <td width="263" bgcolor="#80AAD2"><p align="right">Este &eacute;
              o seu acesso de n&uacute;mero <strong><?php echo $cnt; ?></strong></p></td>
          <td width="19" bgcolor="#80AAD2">
            <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
      <table width="655" border="0" cellpadding="5" cellspacing="1" align="center">
        <tr>
          <td colspan="3"><p><strong>Agroecologia em Rede</strong></p></td>
        </tr>
        <tr>
          <td height="1" colspan="3" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td width="170" height="285" rowspan="2" valign="top"><img src="admin/imagens/abertura10.jpg" width="177" height="259"></td>
          <td height="154" colspan="2" valign="top">
	    </tr>
        <tr>
          <td height="81" valign="center" > <div align="center"><a href="docs/manual.pdf" class="Preto"><img src="imagens/ico_pdf.gif" width="32" height="32" border="0"><br>
          Manual</a></div></td>
          <td valign="center" ><div align="center"><a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank"><img src="imagens/getacro.gif" width="88" height="31" border="0"></a></div></td>
        </tr>
      </table>
      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
