<?php
  include('conexao.inc.php');
  if (!isset($maxLevel)) {
    $maxLevel = 0;
  }
  if (!isset($tipo)) {
    $tipo = 'AT';
  }
  $prefix = strtolower($tipo);
  function untree($parent, $level, $maxLevel) {
    global $db, $lingua, $prefix;
    if ($maxLevel>0 && $level>$maxLevel) {
      return;
    }
    $sql = ($prefix=='at')
      ? "SELECT * FROM area_tematica WHERE at_pertence='".$parent."' ORDER BY at_descricao"
      : "SELECT * FROM area_geografica WHERE ag_pertence='".$parent."' ORDER BY ag_descricao";
    $parentsql = $db->query($sql);
    if (!$parentsql->num_rows) {
      return;
    }
    $mostraBotaoEnviar = false;
    $botaoEnviarTexto = '<input class="btn btn-sm btn-primary" value="Selecionar" type="submit">';
    while($branch = $parentsql->fetch_object()) {
      $echo_this = "<li>";
      if ($level==1) {
          //echo "<div class='float-right'>$botaoEnviarTexto</div>";
      }
      for ($x=1; $x<=$level; $x++) {
        $echo_this .=  "<span class='mr-4 border-left border-primary'></span>";
      }
      $echo_this .= '<input id="'.$branch->{$prefix.'_id'}.'" type="checkbox" name="'.$prefix.'[]" value="'.$branch->{$prefix.'_id'}.'"> <label for="'.$branch->{$prefix.'_id'}.'">'.traduz($branch->{$prefix.'_descricao'}, $lingua).'</label></li>';
      echo $echo_this;
      $rename_level = $level;
      untree($branch->{$prefix.'_id'}, ++$rename_level, $maxLevel);
    }
  }

  if ($prefix=='at') {
    $sql = "SELECT * FROM area_tematica WHERE at_pertence=0 AND at_id=$id ORDER BY at_descricao";
  } else {
    if (!isset($id) || !$id) {
      $sql = "SELECT * FROM area_geografica WHERE ag_pertence='0' ORDER BY ag_descricao";
    } else {
      $sql = "SELECT * FROM area_geografica WHERE ag_id='".$id ."' ORDER BY ag_descricao";
    }
  }
  $query = $db->query($sql);
?>
<input type="hidden" name="url" value="<?php echo $url; ?>">
<ul class="list-unstyled">
  <?php while ($row = $query->fetch_object()) { ?>
    <li>
      <input id="<?=$row->{$prefix.'_id'}?>" type="checkbox" name="<?=$prefix?>[]" value="<?=$row->{$prefix.'_id'}?>"> <label for="<?=$row->{$prefix.'_id'}?>"><strong><?=traduz(trim($row->{$prefix.'_descricao'}), $lingua)?></strong></label>
    </li>
    <?php untree($row->{$prefix.'_id'}, 1, $maxLevel); ?>
  <?php } ?>
</ul>
<?php $db->close(); ?>
