<?php
//coding:utf-8
//por daniel tygel (dtygel em fbes pt org pt br) em 2008
//Defino a codificação do PHP:
//mb_internal_encoding("utf-8");
//Faço a conexão ao banco de dados:
include("conexao.inc.php");
$verphp=phpversion();
$mapa=($_REQUEST['mapa']);
$conta=($_REQUEST['conta']);
$busca=$_REQUEST['busca'];
$q3=$_REQUEST['Q3'];
if (strlen($q3)>2) {
	$distmax=$_REQUEST['distmax'];
	$sql='select nome as cidade, id_ibge as , latitude, `longitude`
		FROM _BR
		WHERE id='.$q3.' LIMIT 1';
	$res=$db->query($sql);
	$dados1 = $res->fetch_object();
	$id_uf=substr($q3,0,2);
	$tmplat="(".$dados1->latitude.")";
	$tmplong="(".$dados1->longitude.")";
	$cidade_ref=$dados1->cidade;
	$id_cidade_ref=$dados1->id;
} else {
	$id_uf=$q3;
}
// Aqui eu começo a preparar o XML (a raiz é o FAREJA):
if ($verphp<'5') {
	$docxml = domxml_new_doc("1.0");
	$docxml->format_output=true;
	$node = $docxml->create_element("fareja");
	$raiz = $docxml->append_child($node);
} else {
	$docxml = new DOMDocument("1.0");
	$docxml->formatOutput=true;
	$node = $docxml->CreateElement("fareja");
	$raiz = $docxml->AppendChild($node);
}

// Dados iniciais de configuração
if ($verphp<'5') {
	$node = $docxml->create_element("config");
	$nivel1 = $raiz->append_child($node);
} else {
	$node = $docxml->CreateElement("config");
	$nivel1 = $raiz->AppendChild($node);
}
if ($mapa) {
	$campos=array(
		'exp'=>array('_titulo',''),
		'chamada'=>array('','{dado}'),
//		'resumo'=>array('','<b>Descrição:</b> '),
//		'comentario'=>array('','<b>Obs:</b> '),
		'anexo'=>array('','<a href="http://www.agroecologiaemrede.org.br/upload/arquivos/{dado}">Clique aqui</a> para acessar o documento anexo!'),
		'temas'=>array('_ativ','<b>Área(s) temática(s):</b><br /> '),
		'local'=>array('','<b>Localização: </b>'),
		'link'=>array('_aviso',"Veja ficha completa <a href='http://www.agroecologiaemrede.org.br/experiencias.php?experiencia={dado}' target='_BLANK'>no agroecologia em rede (clique aqui)</a>")
	);
	foreach ($campos as $valor) {
		$estilotmp.=$valor[0].'|';
		$cpotmp.=$valor[1].'|';
	}
	if ($verphp<'5') {
		$nivel1->set_attribute("campos", substr($cpotmp,0,strlen($cpotmp)-1));
		$nivel1->set_attribute("estilos", substr($estilotmp,0,strlen($estilotmp)-1));
		$nivel1->set_attribute("cat", 'agrorede');
		$nivel1->set_attribute("tipo", 'Peq');
		$nivel1->set_attribute("cor", 'Verde');
	} else {
		$nivel1->SetAttribute("campos", substr($cpotmp,0,strlen($cpotmp)-1));
		$nivel1->SetAttribute("estilos", substr($estilotmp,0,strlen($estilotmp)-1));
		$nivel1->SetAttribute("cat", 'agrorede');
		$nivel1->SetAttribute("tipo", 'Peq');
		$nivel1->SetAttribute("cor", 'Verde');
	}
} else {
	$campos=array(
		'Experiência'=>array('', '', '<a href="http://www.agroecologiaemrede.org.br/upload/arquivos/{dado}">Anexo (clique para acessar)</a>'),
		'Área(s) Temática(s)'=>array(''),
		'Localização'=>array(''),
		'Link'=>array('<a href="http://www.agroecologiaemrede.org.br/experiencias.php?experiencia={dado}" target="_BLANK">Acessar ficha completa da experiência</a>')
	);
	unset($txtmp);
	foreach ($campos as $col=>$campo) {
		$txtmp.=$col.'==';
		foreach ($campo as $cpo)
			$txtmp.=$cpo.'|';
		$txtmp=substr($txtmp,0,strlen($txtmp)-1).'||';
	}
	if ($verphp<'5') {
		$nivel1->set_attribute("colunas", substr($txtmp,0,strlen($txtmp)-2));
	} else {
		$nivel1->SetAttribute("colunas", substr($txtmp,0,strlen($txtmp)-2));
	}
}

if ($verphp<'5') {
	$node = $docxml->create_element("markers");
	$nivel1 = $raiz->append_child($node);
} else {
	$node = $docxml->CreateElement("markers");
	$nivel1 = $raiz->AppendChild($node);
}

	$sql="SELECT
		a.ex_id,
		a.ex_descricao,
		a.lat,
		a.lng, ";
if ($distmax)
	$sql.= "((acos(sin(".$tmplat."*pi()/180) * sin(a.lat*pi()/180) + cos(".$tmplat."*pi()/180) * cos(a.lat*pi()/180) * cos((a.`lng` - ".$tmplong.")*pi()/180)))*6378.7) as distancia, ";
	$sql.="
		a.ex_resumo,
		a.ex_chamada,
		a.dt_atualizacao,
		a.txt_comentario,
		b.ea_arquivo,
		GROUP_CONCAT(DISTINCT at_descricao SEPARATOR '|') as tema,
		GROUP_CONCAT(DISTINCT ag_descricao SEPARATOR '|') as local
	FROM `experiencia` as a
		LEFT JOIN experiencia_arquivo as b ON a.ex_id=b.ea_id_experiencia
		LEFT JOIN experiencia_autor as c ON a.ex_id=c.exa_id_experiencia
		LEFT JOIN usuario as d ON c.exa_id_usuario=d.us_login
		LEFT JOIN rel_area_experiencia as e ON a.ex_id=e.rae_id_experiencia
		LEFT JOIN area_tematica as f ON e.rae_id_area=f.at_id
		LEFT JOIN rel_geo_experiencia as g ON a.ex_id=g.rge_id_experiencia
		LEFT JOIN area_geografica as h ON h.ag_id=g.rge_id_geo
	WHERE a.lat<>0 ";
if ($distmax) {
	$sql.=" AND ((acos(sin(".$tmplat."*pi()/180) * sin(a.lat*pi()/180) + cos(".$tmplat."*pi()/180) * cos(a.lat*pi()/180) * cos((a.`lng` - ".$tmplong.")*pi()/180)))*6378.7)<".$distmax;
	$tit=" até ".$distmax." Km de ".$cidade_ref;
} else {
	//$sql.="WHERE SUBSTRING(a.id_cidade,1,2)=".$id_uf; RESOLVER!!!
	$tit=": ".$lista_UFs[$id_uf];
}
if ($busca)
	$sql.=" AND (a.ex_descricao LIKE '%".$busca."%' OR a.ex_chamada LIKE '%".$busca."%' OR a.ex_resumo LIKE '%".$busca."' OR at_descricao LIKE '%".$busca."')";
$sql.="
	GROUP BY a.ex_id
	ORDER BY ";
if ($distmax)
	$sql.="distancia";
	else $sql.="a.cidade";
$sql.=", a.ex_descricao";

$res=$db->query($sql);
$numpts=end($db->num_rows);
if ($conta)
	$dados=$numpts;
$lat_repetido=array();
$long_repetido=array();
/*
if (!$conta) {
   	while ($dados = $res->fetch_object()) {
		if (in_array($dados->lng,$long_repetido) || in_array($dados->latitude,$lat_repetido)) {
			$dados->lng=$dados->lng+(rand(-500,500)*(0.05/500));
			$dados->lat=$dados->lat+(rand(-500,500)*(0.05/500));
		} else {
			$long_repetido[]=$dados->lng;
			$lat_repetido[]=$dados->lat;
		}
		if ($verphp<'5') {
			$node = $docxml->create_element("pt");
			$newnode = $nivel1->append_child($node);
			if (!$conta) {
				$newnode->set_attribute("lat", $dados->lat);
				$newnode->set_attribute("lng", $dados->lng);
				$newnode->set_attribute("msg", mb_convert_encoding(mensagem_html($dados),'utf-8'));
			}
			$newnode->set_attribute("id", $dados->ex_id);
		} else {
			$node = $docxml->createElement("pt");
			$newnode = $nivel1->appendChild($node);
			if (!$conta) {
				$newnode->setAttribute("lat", $dados->lat);
				$newnode->setAttribute("lng", $dados->lng);
				$newnode->setAttribute("msg", utf8_encode(mensagem_html($dados)));
			}
			$newnode->setAttribute("id", $dados->ex_id);
		}
	} // fim do loop nos dados
} // fim do if tem dados.
else {
	if ($verphp<'5') {
		$node = $docxml->create_element("pt");
		$newnode = $nivel1->append_child($node);
		$newnode->set_attribute("num", $dados);
	} else {
		$node = $docxml->createElement("pt");
		$newnode = $nivel1->appendChild($node);
		$newnode->setAttribute("num", $dados);
	}
}
*/
if ($numpts) {
	//Aqui envio as informações de cabeçalho:
	header('Content-Type: text/xml; charset=utf-8');
	if ($verphp<'5')
		echo $docxml->dump_mem();
		else echo $docxml->saveXML();
}

function mensagem_html($dados) {
global $cidade_ref, $id_cidade_ref, $busca;
	$dados->ex_descricao=wordwrap($dados->ex_descricao,30,"<br />");
	$dados->ex_chamada=wordwrap($dados->ex_chamada,30,"<br />");
	$dados->ex_resumo=wordwrap($dados->ex_resumo,30,"<br />");
	$dados->txt_comentario=wordwrap($dados->txt_comentario,30,"<br />");
	$dados->tema=wordwrap(str_replace('|',', ',$dados->tema),30,"<br />");
	$dados->local=wordwrap(str_replace('|',', ',$dados->local),30,"<br />");
	if ($busca) {
		$dados->ex_descricao=str_ireplace($busca,'<span class="busca">'.$busca.'</span>',$dados->ex_descricao);
		$dados->ex_chamada=str_ireplace($busca,'<span class="busca">'.$busca.'</span>',$dados->ex_chamada);
		$dados->ex_resumo=str_ireplace($busca,'<span class="busca">'.$busca.'</span>',$dados->ex_resumo);
		$dados->ex_tema=str_ireplace($busca,'<span class="busca">'.$busca.'</span>',$dados->ex_tema);
	}
	$texto.=$dados->ex_descricao.'|'
		. $dados->ex_chamada.'|'
//		. $dados->ex_resumo.'|'
//		. $dados->txt_comentario.'|'
		. urlencode($dados->ea_arquivo).'|'
		. $dados->tema.'|'
		. $dados->local.'|'
		. $dados->ex_id;
	return $texto;
}
?>
