<?php
if (isset($_GET['f']) && $_GET['f']) {
	header('Location:mapeo/index.php');
}
include("conexao.inc.php");
setcookie('agrorede_lingua',$_SESSION['agrorede_lingua'],time()+60*60*24*365,'/');

$itensConheca = array(
	'AeR_o_que_e',
	'AeR_historico',
	'AeR_gestao'
);

// aqui é o texto de abertura:
$sql = "SELECT * FROM texto WHERE tex_id='AGROREDE_SOBRE'";
$query = $db->query($sql);
$rowTexto = $query->fetch_object();

if (isset($cookieUsuarioPub) && strlen($cookieUsuarioPub) != 0) {
	$sql = "SELECT us_nome FROM usuario WHERE us_login='$cookieUsuarioPub'";
	$query = $db->query($sql);
	if (!$query) {
    	die($db->error);
	}
	$row = $query->fetch_object();
	$nome = trim($row->us_nome);
}

$sql = "SELECT count(ex_id) AS count FROM experiencia";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}
$row = $query->fetch_object();
$qtExperiencias = $row->count;

$sql = "SELECT count(us_login) AS count FROM usuario";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}
$row = $query->fetch_object();
$qtUsuarios = $row->count;

$sql = "SELECT count(in_id) AS count FROM frm_instituicao";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}
$row = $query->fetch_object();
$qtInstituicoes = $row->count;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php include("menu_geral.php");

// Aqui eu escolho uma imagem randômica do banco de imagens do sistema:
$sql = "SELECT ex_id, ex_cidade_ref, ex_descricao, ex_anexos FROM frm_exp_base_comum WHERE ex_anexos IS NOT NULL AND ex_anexos!='' AND ex_liberacao!='R' ORDER BY dt_criacao DESC";
$universo = faz_query($sql,$db,'object');
shuffle($universo);
$imgs = array();
$i=0;
foreach($universo as $ex) {
	$exArqs = explode('|',$ex->ex_anexos);
	foreach ($exArqs as $arq) {
		if (file_exists($dir['upload'].$arq) && is_array(getimagesize($dir['upload'].$arq))) {
			$imgs[]=array(
				'ex_id'=>$ex->ex_id,
				'ex_descricao'=>$ex->ex_descricao,
				'arquivo'=>$arq,
				'localizacao'=>organiza_geos($ex->ex_cidade_ref)->label
			);
			break;
		}
	}
	if (count($imgs)>20) {
		break;
	}
}

?>
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-4 mb-4">
			<a href="<?=$dir['base_URL']?>mapeo/index.php?f=consultar&tipofrm=experiencia" class="btn btn-primary w-100">
				<div class="col-md-12">
					<i class="aericon-experiencias text-white" style="font-size:5rem"></i>
				</div>
				<div class="col-md-12">
					<h5 class="text-white"><strong>Experiências</strong></h5>
				</div>
			</a>
		</div>
		<div class="col-sm-12 col-md-4 mb-4">
			<a href="<?=$dir['base_URL']?>usuarios.php" class="btn btn-primary w-100">
				<div class="col-md-12">
					<i class="aericon-pessoas text-white" style="font-size:5rem"></i>
				</div>
				<div class="col-md-12">
					<h5 class="text-white"><strong>pessoas</strong></h5>
				</div>
			</a>
		</div>
		<div class="col-sm-12 col-md-4 mb-4">
			<a href="<?=$dir['base_URL']?>mapeo/index.php?f=consultar&tipofrm=instituicao" class="btn btn-primary w-100">
				<div class="col-md-12">
					<i class="aericon-instituicoes text-white" style="font-size:5rem"></i>
				</div>
				<div class="col-md-12">
					<h5 class="text-white"><strong>Instituições</strong></h5>
				</div>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-4 mb-4">
			<a href="<?=$dir['base_URL']?>login.php" class="btn btn-secondary w-100 text-dark py-4">
				<strong>Cadastrar</strong>
			</a>
		</div>
		<div class="col-sm-12 col-md-4 mb-4">
			<a href="<?=$dir['base_URL']?>documentacao/manual_agroecologia_em_rede_set2009.pdf" class="btn btn-light border-primary w-100 text-dark py-4">
				<strong>Manual de uso</strong>
			</a>
		</div>
		<div class="col-sm-12 col-md-4 mb-4">
			<a href="<?=$dir['base_URL']?>documentacao/ficha_identificacao_agroecologiaemrede_para_impressao.doc" class="btn btn-light border-primary w-100 text-dark py-4">
				<strong>Imprimir ficha</strong>
			</a>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-6 mb-4">
			<div id="carouselHome" class="carousel slide col-md-12" data-ride="false">
				<div class="carousel-inner">
					<?php foreach ($imgs as $i=>$img) { ?>
						<div class="carousel-item<?=($i==0) ? ' active' : ''?>">
							<a id="image<?=$i?>" href="<?=$dir['base_URL']?>experiencias.php?experiencia=<?=$img['ex_id']?>">
								<img class="d-block w-100" src="<?=getResizedImgURL($img['arquivo'],1024,800)?>" alt="<?=$img['ex_descricao']?>">
							</a>
							<div class="aer-carousel-caption d-none d-md-block p-2 bg-light">
								<div class="aer-carousel-caption-title text-truncate"><small><?=$img['ex_descricao']?></small></div>
								<div class="aer-carousel-caption-subtitle text-truncate"><small><?=$img['localizacao']?></small></div>
							</div>
						</div>
					<?php } ?>
				</div>
				<?php if (count($imgs)>1) { ?>
					<a class="carousel-control-prev" href="#carouselHome" role="button" data-slide="prev">
						<span class="carousel-control-prev-icon" aria-hidden="true"></span>
						<span class="sr-only">Anterior</span>
					</a>
					<a class="carousel-control-next" href="#carouselHome" role="button" data-slide="next">
						<span class="carousel-control-next-icon" aria-hidden="true"></span>
						<span class="sr-only">Próxima</span>
					</a>
				<?php } ?>
			</div>
		</div>
		<div class="col-sm-12 col-md-6">
			<h5>Conheça</h5>
			<dl id="conheca" class="mt-2">
				<?php foreach ($itensConheca as $item) { ?>
					<dt data-toggle="modal" data-target="#modalInfos" data-content="<?=$item?>" class="mt-4"><i class="oi oi-chevron-right"></i> <?=$txt['mysql'][$item]->tex_titulo?></dt>
					<dd data-toggle="modal" data-target="#modalInfos" data-content="<?=$item?>"><?=truncate(strip_tags($txt['mysql'][$item]->tex_texto),200)?></dd>
				<?php } ?>
			</dl>
		</div>
	</div>

	<div class="modal" id="modalInfos" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title"></h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
		      </div>
	    </div>
	  </div>
	</div>
</div>

<div class="container-fluid mt-4">
	<div class="row bg-light pt-4">
		<div class="col-12 col-md-6 col-xl-4 offset-xl-2 mb-2">
			<div class="row">
				<div class="col-12">
					<small><strong>Realização e gestão:</strong></small>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-sm-6 text-center">
					<a class="mr-4 my-4" href="http://www.agroecologia.org.br"><img class="my-2" style="height:70px" src="<?=$dir['base_URL']?>imagens/logos/logo_ANA_transp.png"></a>
				</div>
				<div class="col-12 col-sm-6 text-center">
					<a class="mr-4 my-4" href="http://www.aba-agroecologia.org.br/"><img class="my-2" style="height:70px" src="<?=$dir['base_URL']?>imagens/logos/logo_ABA_transp.png"></a>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-3 col-xl-2 mb-2">
			<div class="row">
				<div class="col-12">
					<small><strong>Cooperação:</strong></small>
				</div>
			</div>
			<div class="row">
				<div class="col-12 text-center">
					<a class="mr-4 my-4" href="https://portal.fiocruz.br/"><img class="my-2" style="height:70px" src="<?=$dir['base_URL']?>imagens/logos/logo_Fiocruz.png"></a>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-3 col-xl-2 mb-2">
			<div class="row">
				<div class="col-12">
					<small><strong>Desenvolvido em código livre:</strong></small>
				</div>
			</div>
			<div class="row">
				<div class="col-12 text-center">
					<a class="mr-4 my-4" href="http://eita.org.br/"><img class="my-2" style="height:70px" src="<?=$dir['base_URL']?>imagens/logos/Eita_roda_400x400.png"></a>
				</div>
			</div>
	</div>
</div>
</body>
</html>

<?php
$db->close();
?>
