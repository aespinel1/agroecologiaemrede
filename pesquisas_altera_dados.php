<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$sql = "SELECT * FROM texto WHERE tex_id='EXPLICACAO_CTRL'";
$query = $db->query($sql);
$rowMsg = $query->fetch_object();

$sql = "SELECT * FROM texto WHERE tex_id='EXPLICACAO_RESUMO'";
$query = $db->query($sql);
$rowRes = $query->fetch_object();
$numPerm = 1;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function criticaFormPES() {

	formNE.submit();
}

function SelecionaOutra() {
    window.location.href="pesquisas_altera.php"
	formNE.submit();
}

function Tecla(e)
{
if(document.all) // Internet Explorer
var tecla = event.keyCode;

else if(document.layers) // Nestcape
var tecla = e.which;


if(tecla > 47 && tecla < 58) // numeros de 0 a 9
return true;
else
{
if (tecla != 8) // backspace
return false;
else
return true;
}
}
</script>


<?php include("menu_geral.php"); ?>

<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>

    <td width="567" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Pesquisa - Alteração</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>

	  <?php if (isset($pesquisa)) {
	        $sql = "select a.*, date_format(dt_criacao,'%d/%m/%Y') as data_inclusao , date_format(dt_atualizacao,'%d/%m/%Y') as data_alteracao from pesquisa a where pe_id=$pesquisa";
			$query = $db->query($sql);
			$rowPES = $query->fetch_object();
         }

		 if ($numPerm == 0) {
			echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
		 }
		else {
?>
<form name="formNE" method="post" action="xt_pes_atualiza_pesquisa.php">
      <table width="567" border="0" cellpadding="5" cellspacing="1" align="center">
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr bgcolor="#FFFFCC">
          <td colspan="4" align="center"><span class="style5"></span>Data inclusão: <?php echo $rowPES->data_inclusao; ?> &nbsp;&nbsp; Data �ltima alteração : <?php echo $rowPES->data_alteracao; ?></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr>
          <td >
            <div align="right"><strong>Identifica��o</strong>:</div></td>
          <td width="363" colspan="3"><input name="pesquisa" type="text" id="pesquisa" size="20" maxlength="20" value="<?php echo $pesquisa ?>" readonly></td>
        </tr>
        <tr>
          <td >
            <div align="right"><strong>T&iacute;tulo</strong>:</div></td>
          <td width="363" colspan="3"><input name="nome" type="text" id="pesquisa" size="69" maxlength="255"  value="<?php echo trim($rowPES->pe_descricao); ?>"></td>
        </tr>
        <tr>
            <td><div align="right"><strong>Ano Publicação</strong>:</div></td>
            <td colspan="3"><input name="ano_publicacao" type="text" id="ano_publicacao" size="6" maxlength="4" value="<?php echo trim($rowPES->ano_publicacao); ?>" onKeyPress="return Tecla(event);"></td>
          </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr bgcolor="#FFFFCC">
          <td colspan="4" align="center"><strong>Autor(es)</strong><br>
              <br><?php echo $rowMsg->tex_texto; ?></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr>
          <td valign="top">
            <div align="right"><strong>Pessoa(s)</strong>:<br>
                <br>
          </div></td>
          <td colspan="3"><select name="autor[]" size="6" multiple id="select2">
              <?php
		          $sql = "SELECT us_login,us_nome FROM usuario ORDER BY us_nome";
		          $query = $db->query($sql);
		          while ($row = $query->fetch_object()) {

				  $sql = "select * from pesquisa_autor where pea_id_usuario = '$row->us_login' and  pea_id_pesquisa =$pesquisa";
				  $queryA = $db->query($sql);
				  $qtd = $queryA->num_rows;

				  if ($qtd == 0) {
                ?>
              <option value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
              <?php
				  }
				  else {
                ?>
              <option selected value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
              <?php
				  }

		          } // la�o do loop
               ?>
            </select>
          </td>
        </tr>
        <tr>
          <td valign="top"><div align="right"><strong>Institui&ccedil;&atilde;o(&otilde;es)</strong>:</div></td>
          <td colspan="3"><select name="instAutor[]" size="6" multiple id="select3">
              <?php
		$sql = "SELECT in_id,in_nome FROM frm_instituicao ORDER BY in_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {


		 $sql = "select * from pesquisa_autor where pea_id_inst = '$row->in_id' and  pea_id_pesquisa =$pesquisa";
		 $queryA = $db->query($sql);
		 $qtd = $queryA->num_rows;

		 if ($qtd == 0) { ?>
              <option value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
              <?php }
		 else { ?>
              <option selected value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
              <?php
	     }
		} // la�o do loop
?>
          </select></td>
        </tr>
        <tr bgcolor="#FFFFCC">
          <td colspan="4" align="center"><strong>Resumo</strong><br>
              <br><?php echo $rowRes->tex_texto; ?></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr>
          <td valign="top">
            <div align="right"></div></td>
          <td colspan="3"><textarea name="resumo" cols="87" rows="20" id="textarea"><?php echo trim($rowPES->pe_resumo); ?></textarea></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr bgcolor="#FFFFCC">
          <td colspan="4" align="center"><strong>Crit&eacute;rios para busca </strong></td>
        </tr>
        <tr>
          <td height="1" colspan="4" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
        <tr>
          <td valign="top"><div align="right"><strong>&Aacute;reas Geogr&aacute;ficas</strong>:<br>
                        <br>
          </div></td>
          <td colspan="3" valign="top" ><a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_ag_nav.php?url=pesquisas_altera_dados.php','at','scrollbars=yes,width=565,height=450,status=yes')"> Clique aqui para adicionar</a>&nbsp;&nbsp; <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_exclui_ag.php?url=experiencias_cadastro.php','at','scrollbars=yes,width=300,height=250,status=yes')">ou aqui para retirar da rela��o</a>
              <textarea name="areasGeograficas" cols="65" rows="5" wrap="VIRTUAL" readonly></textarea>
              <?php
			     $sql = "select b.ag_id, b.ag_descricao from rel_geo_pesquisa a, area_geografica b where a.rgp_id_geo =  b.ag_id and a.rgp_id_pesquisa = $pesquisa order by b.ag_descricao";
				 $query = $db->query($sql);
				 $CagID = "";
		         while ($row = $query->fetch_object()) {
				       $CagID = $CagID . $row->ag_id . ";";
?>
              <script language="JavaScript">
		     formNE.areasGeograficas.value = formNE.areasGeograficas.value + '<?php echo $row->ag_descricao; ?>\n';
	      </script>
              <?php
		         }
			  ?>
              <input   type="hidden" name="agID" value="<?php echo $CagID; ?>">
          </td>
        </tr>
        <tr>
          <td valign="top"><div align="right"><strong>&Aacute;reas Tem&aacute;ticas</strong>:<br>
                        <br>
          </div></td>
          <td colspan="3" valign="top"> <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_at_nav.php?url=pesquisas_altera_dados.php','at','scrollbars=yes,width=565,height=450,status=yes')">Clique aqui para relacionar</a>&nbsp;&nbsp; <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_exclui_at.php?url=experiencias_cadastro.php','at','scrollbars=yes,width=300,height=250,status=yes')">ou aqui para retirar da rela��o</a>
                    <textarea name="areasTematicas" cols="65" rows="5" wrap="VIRTUAL" readonly></textarea>
                    <?php
			     $sql = "select b.at_id, b.at_descricao  from rel_area_pesquisa a, area_tematica b where a.rap_id_area =  b.at_id and a.rap_id_pesquisa = $pesquisa order by b.at_descricao";
				 $query = $db->query($sql);
				 $CatID = "";
		         while ($row = $query->fetch_object()) {
				       $CatID = $CatID . $row->at_id . ";";
?>
                    <script language="JavaScript">
		     formNE.areasTematicas.value = formNE.areasTematicas.value + '<?php echo $row->at_descricao; ?>\n';
	          </script>
                    <?php
		         }
			  ?>
                    <input type="hidden" name="atID" value="<?php echo $CatID; ?>">
                    <br>
          </td>
        </tr>
        <tr>
          <td valign="top"><div align="right"><strong>Biomas</strong>:</div></td>
          <td colspan="3" valign="top">
            <?php
				$sql = "SELECT * FROM habitat ORDER BY ha_descricao";
				$query = $db->query($sql);
				echo "<select name=\"biomas\" class=\"inputFlat\">\n";
				echo "<option value=''>--- selecione ---</option>\n";
				while ($row = $query->fetch_object()) {

					$sql = "select * from rel_habitat_pesquisa where rhp_id_habitat = '$row->ha_id' and  rhp_id_pesquisa =$pesquisa";
		            $queryA = $db->query($sql);
		            $qtd = $queryA->num_rows;

		            if ($qtd == 0) {
					   echo "<option value=\"$row->ha_id\">$row->ha_descricao</option>\n";
				    }
					else {
					   echo "<option selected value=\"$row->ha_id\">$row->ha_descricao</option>\n";
					}
				}
?>
          </td>
        </tr>
        <tr bgcolor="#000000">
 	      <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		    <tr bgcolor="#FFFFCC">
            <td colspan="4" align="center"><strong>Coment&aacute;rios e Sugest&otilde;es</strong></td>
          </tr>
		     <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td valign="top"> <div align="right"><br>
                <br>
                </div></td>

            <td colspan="3"><textarea name="comentario" cols="85" rows="20" id="resumo"><?php echo trim($rowPES->txt_comentario); ?></textarea></td>
          </tr>
		  <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		        <tr>
          <td colspan="4"><div align="center">
              <input name="button" type="button" class="botaoLogin" onClick="criticaFormPES();" value="   Alterar   ">
			  &nbsp;&nbsp;<input name="button" type="button" class="botaoLogin" onClick="SelecionaOutra();" value="   Seleciona outra   ">
          </div></td>
        </tr>
      </table>
</form>
<?php
		}
?>
    </td>
  </tr>

</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
