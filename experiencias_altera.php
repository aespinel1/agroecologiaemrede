<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$sql = "SELECT * FROM texto WHERE tex_id='EXPLICACAO_ALTERACAO'";
$query = $db->query($sql);
$rowRes = $query->fetch_object();

$sql = "SELECT * FROM texto WHERE tex_id='MSG_ALTERACAO_EXPERIENCIA'";
$query = $db->query($sql);
$rowMsg = $query->fetch_object();


$numPerm = 1;

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.style2 {color: #0000FF}
.style4 {color: #FF0000}
-->
</style>
<?php include("menu_geral.php"); ?>
<br>
<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>

    <td width="567" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Experi&ecirc;ncias</strong><b> - Alteração </b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
<br>
<?php
			if ($numPerm == 0 and $numPerm = 0) {
				echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
			}
			else {

				/*$sql = "SELECT * FROM experiencia,usuario WHERE ex_usuario=us_login";
				$query = $db->query($sql);  */

				$sql = "SELECT * FROM experiencia where ex_usuario = '$cookieUsuario' and (ex_liberacao  = 'P' or ex_liberacao = 'N' or ex_liberacao  = 'E') ORDER BY ex_descricao";
				$query = $db->query($sql);
				$QtdRec = $query->num_rows;


?>
 </p>
<table width="550" align="center">
<tr><td><div align="justify"><span class="style17 style2"><?php echo $rowRes->tex_texto; ?></span></div></td>
</tr>
</table>
<br>
<?php if ($QtdRec > 0) {  ?>
	  <form name="formAltera" method="post" action="experiencias_altera_dados.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td height="24" colspan="4"> <p> <strong>Alterar
                Experiência </strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"> Experi&ecirc;ncia:
              <select name="experiencia" id="experiencia">
                <?php
				while ($row = $query->fetch_object()) {
					$desc = trim($row->ex_descricao);
					$varSelected = "";
					if (isset($experiencia) && $experiencia == $row->ex_id) {
						$varSelected = "selected";
					}
					echo "<option value=\"$row->ex_id\" $varSelected>$desc</option>";
				}
?>
              </select> &nbsp;&nbsp;&nbsp; <input name="Submit" type="submit" class="botaoLogin" value="   Selecionar   ">
            </td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
        </table>
      </form>

    </td>
  </tr>
</table>
<?php } else { ?>
<p align="center" class="style4" ><?php echo $rowMsg->tex_texto; ?></p>
<?php }

}  ?>
</body>
</html>
<?php
$db->close();
?>
