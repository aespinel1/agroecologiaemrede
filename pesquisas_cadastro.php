<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$sql = "SELECT * FROM texto WHERE tex_id='EXPLICACAO_CTRL'";
$query = $db->query($sql);
$rowMsg = $query->fetch_object();

$sql = "SELECT * FROM texto WHERE tex_id='EXPLICACAO_RESUMO'";
$query = $db->query($sql);
$rowRes = $query->fetch_object();

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
function criticaFormExp() {
	desc = formNE.nome.value;
	if (desc.length == 0) {
		alert('Digite um t�tulo v�lido para a pesquisa !');
		formNE.nome.focus();
		return;
	}
	formNE.submit();
}

function Tecla(e)
{
if(document.all) // Internet Explorer
var tecla = event.keyCode;

else if(document.layers) // Nestcape
var tecla = e.which;


if(tecla > 47 && tecla < 58) // numeros de 0 a 9
return true;
else
{
if (tecla != 8) // backspace
return false;
else
return true;
}
}
//-->
</script>
<!--#include file="sis_criticas.js"-->
<style type="text/css">
<!--
.style1 {
	color: #006666;
	font-weight: bold;
}
-->
</style>
<?php include("menu_geral.php"); ?>

<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>

    <td width="567" height="1619" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Pesquisas - Cadastramento</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
 	        <p><br>
        <span>Caso estejam faltando pessoas ou institui&ccedil;&otilde;es
        nos campos de sele&ccedil;&atilde;o, entre em <a href="contato.php" class="Preto">contato</a>
        conosco ou as cadastre antes de iniciar o cadastramento da pesquisa. N&atilde;o esque&ccedil;a que <a href="usuarios_inclusao.php" class="Preto">voc&ecirc;
        deve se cadastrar previamente</a> no Agroecologia em Rede, al&eacute;m de poder tamb&eacute;m
        <a href="instituicoes_cadastro.php" class="Preto">cadastrar sua institui&ccedil;&atilde;o</a>.</span></p>


	        <?php if (isset($novaPesquisa)) { ?>
      <p align="center" class="textoPreto10px style1">Pesquisa <?php echo $pesDescricao; ?> cadastrada com sucesso !</p>
	  <?php } ?>
<form name="formNE" method="post" action="xt_pes_insere_pesquisa.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td colspan="4"> <p> <strong>Cadastramento de nova pesquisa </strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td > <div align="right"><strong>T&iacute;tulo</strong>:</div></td>
            <td width="363" colspan="3"><input name="nome" type="text" id="nome" size="69" maxlength="255"></td>
          </tr>
         <tr>
            <td><div align="right"><strong>Ano Publicação</strong>:</div></td>
            <td colspan="3"><input name="ano_publicacao" type="text" id="ano_publicacao" size="6" maxlength="4" onKeyPress="return Tecla(event);"></td>
          </tr>
		   <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		   <tr bgcolor="#FFFFCC">
            <td colspan="4" align="center"><strong>Autor(es)</strong><br></strong><a href="textos.php?id=AUTOR_SOBRE" class="Preto">D&uacute;vidas</a>?<br>
              <br><?php echo $rowMsg->tex_texto; ?></td>
          </tr>

		   <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td valign="top"> <div align="right"><strong>Pessoa(s)</strong>:<br>
                <br>
                </div></td>
            <td colspan="3"><select name="autor[]" size="6" multiple id="autor[]">
                <?php
		$sql = "SELECT us_login,us_nome FROM usuario ORDER BY us_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select> </td>
          </tr>
          <tr>
            <td valign="top"><strong>Institui&ccedil;&atilde;o(&otilde;es)</strong>:</td>
            <td colspan="3"><select name="instAutor[]" size="6" multiple id="instAutor[]">
              <?php
		$sql = "SELECT in_id,in_nome FROM frm_instituicao ORDER BY in_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
?>
              <option value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
              <?php
		} // la�o do loop
?>
            </select></td>

	      <tr bgcolor="#FFFFCC">
            <td colspan="4" align="center"><strong>Resumo</strong><br>            <a href="textos.php?id=RESUMO_SOBRE" class="Preto">D&uacute;vidas</a>?<br>
              <br><?php echo $rowRes->tex_texto; ?></td>
          </tr>
		     <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td valign="top"> <div align="right"><br>
                <br>
                </div></td>

            <td colspan="3"><textarea name="resumo" cols="85" rows="20" id="resumo"></textarea></td>
          </tr>
		  <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		  <tr bgcolor="#FFFFCC">
            <td colspan="4" align="center"><strong>Crit&eacute;rios para busca </strong></td>
          </tr>
		  <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>

          <tr>
            <td valign="top"><div align="right"><strong>&Aacute;reas
                Geogr&aacute;ficas</strong>:<br>
                <br>
                </div></td>
            <td colspan="3" valign="top" ><a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_ag_nav.php?url=pesquisas_cadastro.php','at','scrollbars=yes,width=517,height=300,status=yes')">
              Clique aqui para adicionar</a>&nbsp;&nbsp;
			  <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_exclui_ag.php?url=pesquisas_cadastro.php','at','scrollbars=yes,width=300,height=250,status=yes')">ou aqui para retirar da rela��o</a>
              <textarea name="areasGeograficas" cols="65" rows="5" wrap="VIRTUAL" readonly></textarea>
              <input  type="hidden" name="agID">
		    </td>
          </tr>
          <tr>
            <td valign="top"><div align="right"><strong>&Aacute;reas
                Tem&aacute;ticas</strong>:<br>
                <br>
                </div></td>
            <td colspan="3" valign="top"> <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_at_nav.php?url=pesquisas_cadastro.php','at','scrollbars=yes,width=517,height=300,status=yes')">Clique
              aqui para relacionar</a>&nbsp;&nbsp; <a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_exclui_at.php?url=pesquisas_cadastro.php','at','scrollbars=yes,width=300,height=250,status=yes')">ou aqui para retirar da rela��o</a>
              <textarea name="areasTematicas" cols="65" rows="5" wrap="VIRTUAL" readonly></textarea>
              <input type="hidden" name="atID"> <br>
		    </td>
          </tr>
          <tr>
            <td valign="top"><div align="right"><strong>Biomas</strong>:</div></td>
            <!--
			<td colspan="3" valign="top">
			<a href="javascript:;" class="Preto" onClick="abrePopup('exp_popup_busca_biomas.php?url=experiencias.php','bi','scrollbars=yes,width=517,height=300,status=yes')">Clique
              aqui para adicionar<strong> Biomas</strong></a><br>
              <textarea name="biomas" cols="50" rows="5" wrap="VIRTUAL" readonly></textarea>
              <input type="hidden" name="biomaID"> </td>-->
			 <td colspan="3" valign="top">
<?php
				$sql = "SELECT * FROM habitat ORDER BY ha_descricao";
				$query = $db->query($sql);
				echo "<select name=\"biomas\" class=\"inputFlat\">\n";
				echo "<option value=''>--- selecione ---</option>\n";
				while ($row = $query->fetch_object()) {
					echo "<option value=\"$row->ha_id\">$row->ha_descricao</option>\n";
				}
?>

			 </td>
          </tr>

 	      <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		    <tr bgcolor="#FFFFCC">
            <td colspan="4" align="center"><strong>Coment&aacute;rios e Sugest&otilde;es</strong></td>
          </tr>
		     <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td valign="top"> <div align="right"><br>
                <br>
                </div></td>

            <td colspan="3"><textarea name="comentario" cols="85" rows="20" id="resumo"></textarea></td>
          </tr>
		  <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>

          <tr>
            <td colspan="4"><div align="center">
                <input type="button" class="botaoLogin" value="   Inserir   " onClick="criticaFormExp();">
              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
	  </form>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
