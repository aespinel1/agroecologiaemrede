<?php
  include('conexao.inc.php');

  $id = isset($_REQUEST['id'])
    ? $_REQUEST['id']
    : '';

  header('Content-Type: text/json; charset=utf-8');

  if (!$id) {
    $t = new StdClass();
    $t->error = 'no id provided!';
    echo json_encode($t);
    exit;
  }

  if (isset($txt[$id])) {
    $ret = $txt[$id];
  } else if (isset($txt['mysql'][$id])) {
    $ret = $txt['mysql'][$id];
  } else {
    $t = new StdClass();
    $t->error = 'couldn\'t find no string with id "'.$id.'"!';
    echo json_encode($t);
    exit;
  }

  echo json_encode($ret);
