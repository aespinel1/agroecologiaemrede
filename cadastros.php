<?php
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57">&nbsp; </td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top">
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr> 
          <td width="566" colspan="2"><p><strong>Cadastros</strong></p></td>
        </tr>
        <tr> 
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr> 
          <td width="566"><div align="center"><a href="cadastro_contatos.php" class="Preto"><img src="imagens/ico_usuarios_transp.gif" width="34" height="32" border="0"><br>
              T&eacute;cnicos e Experimentadores</a></div></td>
          <td width="50%"><div align="center"><a href="cadastro_instituicoes.php" class="Preto"><img src="imagens/ico_tabela_simples_transp.gif" width="34" height="32" border="0"><br>
              Institui&ccedil;&otilde;es</a></div></td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr> 
          <td><div align="center"><a href="cadastro_experiencias.php" class="Preto"><img src="imagens/ico_experiencias_transp.gif" width="34" height="32" border="0"><br>
              Experi&ecirc;ncia</a></div></td>
          <td><div align="center"><a href="cadastro_referencias.php" class="Preto"><img src="imagens/ico_bibliografia_transp.gif" width="34" height="32" border="0"><br>
              Documento</a></div></td>
        </tr>
        <tr> 
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td><div align="center"><a href="cadastro_pesquisa.php" class="Preto"><img src="imagens/ico_pesquisas_transp.gif" width="34" height="32" border="0"><br>
              Pesquisa</a></div></td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>