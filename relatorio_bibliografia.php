<?php include("conexao.inc.php"); ?>
<!DOCTYPE html>
<html>
<head>
<title>Relat&oacute;rio</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="760" border="0" cellpadding="3">
  <tr> 
    <td width="380" height="83"><img src="imagens/logo_sisgea_branco.gif" width="350" height="80"></td>
    <td width="362"><div align="right"><img src="imagens/logo_cnip_branco.gif" width="182" height="80"></div></td>
  </tr>
  <tr> 
    <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
  </tr>
  <tr> 
    <td colspan="2">
<?php
$RELATORIO = "<!DOCTYPE html>
<html>
				<body leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">
				<table width=\"760\" border=\"0\" cellpadding=\"3\">
				  <tr> 
				    <td width=\"380\" height=\"83\"><img src=\"http://www.cnip.org.br/sisgea/imagens/logo_sisgea_branco.gif\" width=\"350\" height=\"80\"></td>
				    <td width=\"362\"><div align=\"right\"><img src=\"http://www.cnip.org.br/sisgea/imagens/logo_cnip_branco.gif\" width=\"182\" height=\"80\"></div></td>
				  </tr>
				  <tr> 
				    <td height=\"1\" colspan=\"2\" bgcolor=\"#000000\"> <div align=\"center\"></div></td>
				  </tr>
				  <tr> 
			    <td colspan=\"2\">";


/******************** REFER�NCIAS BIBLIOGR�FICAS ********************/
if ($tipo == "BB1") {
	$busca = strtoupper($busca);
	$titulo = strtoupper($titulo);
	$autor = strtoupper($autor);
	$tituloTodo = strtoupper($tituloTodo);
	$autorTodo = strtoupper($autorTodo);
	$imprenta = strtoupper($imprenta);
	$serie = strtoupper($serie);
	$localizacao = strtoupper($localizacao);
	$idioma = strtoupper($idioma);
	$sql = "SELECT * FROM bibliografia WHERE UCASE(bb_titulo) LIKE '$busca%' AND UCASE(bb_autor) LIKE '$autor%' ".
		   "AND UCASE(bb_titulo_todo) LIKE '$tituloTodo%' AND UCASE(bb_imprenta) LIKE '$imprenta%' ".
		   "AND UCASE(bb_serie) LIKE '$serie%' AND UCASE(bb_localizacao) LIKE '$localizacao%' ".
		   "AND UCASE(bb_idioma) LIKE '$idioma%' ORDER BY bb_titulo";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Animais e Plantas</strong> do <strong>SisGEA</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\"><strong>Refer�ncias Bibliogr�ficas</strong> do <strong>SisGEA</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"entidades_biologicas.php?bioDesc=$row->eb_descricao&bio=$row->eb_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->eb_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"bib_pesquisa.php?bibDesc=$row->bb_titulo&bib=$row->bb_id\" class=\"preto\">$row->bb_titulo</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Sua busca n�o retornou resultados!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
else if ($tipo == "BB2") {
	$sql = "SELECT * FROM bibliografia,tipo_bibliografias,rel_bib_experiencia,experiencia WHERE bb_id=$bib AND ".
		   "bb_tipo=tb_id AND bb_id=rbe_id_bibliografia AND ex_id=rbe_id_experiencia AND ex_liberacao='S'";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>SisGEA</strong> para a <strong>Animal ou Planta</strong> <b>\"$bioDesc\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>SisGEA</strong> para a <strong>Refer�ncia Bibliogr�fica</strong> <b>\"$bibDesc\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			if ($i == 1) {
				$RELATORIO .= "Refer�ncia Bibliogr�fica: <strong>$bibDesc</strong><br>";				
				$RELATORIO .= "Tipo: <strong>$row->tb_descricao</strong><br>";
				$RELATORIO .= "Autor: <strong>$row->bb_autor</strong><br>";
				$RELATORIO .= "T�tulo do todo: <strong>$row->bb_titulo_todo</strong><br>";
				$RELATORIO .= "Autor do todo: <strong>$row->bb_autor_todo</strong><br>";
				$RELATORIO .= "Data: <strong>$row->bb_data</strong><br>";
				$RELATORIO .= "Imprenta: <strong>$row->bb_imprenta</strong><br>";
				$RELATORIO .= "S�rie: <strong>$row->bb_serie</strong><br>";
				$RELATORIO .= "Descritores: <strong>$row->bb_descritores</strong><br>";
				$RELATORIO .= "Identificadores: <strong>$row->bb_identificadores</strong><br>";
				$RELATORIO .= "Nota��o principal: <strong>$row->bb_not_principal</strong><br>";
				$RELATORIO .= "Localiza��o: <strong>$row->bb_localizacao</strong><br>";
				$RELATORIO .= "Idioma: <strong>$row->bb_idioma</strong><br>";
				$RELATORIO .= "Detalhes: <strong>$row->bb_detalhes</strong><br>";
				$RELATORIO .= "Resumo: <strong>$row->bb_resumo</strong><br>";
				$RELATORIO .= "<br><br><br>";
				$RELATORIO .= "Foram encontrados as seguintes experiências para a Refer�ncia Bibliogr�fica <b>\"$bibDesc\"</b>:";

				echo "Refer�ncia Bibliogr�fica: <strong>$bibDesc</strong><br>";				
				echo "Tipo: <strong>$row->tb_descricao</strong><br>";
				echo "Autor: <strong>$row->bb_autor</strong><br>";
				echo "T�tulo do todo: <strong>$row->bb_titulo_todo</strong><br>";
				echo "Autor do todo: <strong>$row->bb_autor_todo</strong><br>";
				echo "Data: <strong>$row->bb_data</strong><br>";
				echo "Imprenta: <strong>$row->bb_imprenta</strong><br>";
				echo "S�rie: <strong>$row->bb_serie</strong><br>";
				echo "Descritores: <strong>$row->bb_descritores</strong><br>";
				echo "Identificadores: <strong>$row->bb_identificadores</strong><br>";
				echo "Nota��o principal: <strong>$row->bb_not_principal</strong><br>";
				echo "Localiza��o: <strong>$row->bb_localizacao</strong><br>";
				echo "Idioma: <strong>$row->bb_idioma</strong><br>";
				echo "Detalhes: <strong>$row->bb_detalhes</strong><br>";
				echo "Resumo: <strong>$row->bb_resumo</strong><br>";
				echo "<br><br><br>";
				echo "Foram encontrados as seguintes experiências para a Refer�ncia Bibliogr�fica <b>\"$bibDesc\"</b>:";
			}
			$RELATORIO .= "<br><br>";
			echo "<br><br>";
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->ex_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>N�o h� experiências para estas Animais e Plantas!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências para estas refer�ncias bibliogr�ficas!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
/******************** FIM REF BIB ********************/

$RELATORIO .= "	</td>
			  </tr>
			</table>
			</body>
			</html>";
			
if ($acao == "IM") {
?>
<script language="JavaScript">
window.print();
alert('O relatório está sendo impresso. Você será redirecionado dentro de 10 segundos.');
var delay = 10; // Delay in seconds
var targetURL = "index.php"; // URl to load
setTimeout('self.location.replace(targetURL)', (delay * 1000));
</script>
<?php
	return;
}
else {
	$headers = "From: CNiP/SisGEA <sisgea@cnip.org.br>\n";
	$headers .= "X-Sender: <sisgea@cnip.org.br>\n";
	$headers .= "X-Mailer: PHP\n";
	$headers .= "X-Priority: 0\n";
	$headers .= "Return-Path: <sisgea@cnip.org.br>\n";
	$headers .= "Content-Transfer-Encoding: 8bit\n";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-Type: text/html; charset=iso-8859-1" ;

	$mail = mail($email, "RELAT�RIO CNiP/SisGEA",$RELATORIO,$headers);
	
	if ($mail == FALSE) {
?>
		<script language="JavaScript">
			alert('Houve um problema no envio do e-mail para o cliente!');
		</script>
<?php

	} // fim do if que verifica se o e-mail foi enviado
	
} // Fim do if que verifica se o relatório deve ser impresso ou enviado por e-mail
?>
	</td>
  </tr>
</table>
</body>
</html>
