<?php

include("conexao.inc.php");

if ( isset($nome) && $nome ) {
	$sql = "SELECT * FROM texto WHERE tex_id='E_MAIL_FALECONOSCO'";
	$query = $db->query($sql);
	$row = $query->fetch_object();

	$to = trim(traduz($row->tex_texto),$lingua);

	$assunto = "[AeR] $nome enviou uma mensagem!";

	$msg =  "Nome       : $nome\r\n";
	$msg .= "E-mail     : $email\r\n";
	$msg .= "Endereço   : $endereco\r\n";
	$msg .= "Instituição: $instituicao\r\n";
	$msg .= "Comentários: $comentarios\r\n";

	$headers = 'From: contato@eita.org.br' . "\r\n" .
    'Reply-To: contato@eita.org.br' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();

	$to = 'dtygel@eita.org.br';

	//pR($to.' - '.$assunto.' - '.$msg.' - '.$headers);
	mail($to, $assunto, $msg, $headers);
	// TODO: verificar se o email é realmente enviado

	$msgEnviada = true;
} else {
	$msgEnviada = false;
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<?php include("menu_geral.php"); ?>

<?php
	$dados = new StdClass();
	if ($loggedIn) {
		$dados->nome = $s->u->us_nome;
		$dados->email = (isset($s->u->us_email) && $s->u->us_email)
			? $s->u->us_email
			: "";
			$dados->endereco = (isset($s->u->txt_endereco) && $s->u->txt_endereco)
				? $s->u->txt_endereco
				: "";
	} else {
		$dados->nome = $dados->email = $dados->endereco = "";
	}
?>
<div class="container">
  <div class="row">
    <div class="col-md-12">
			<div class="alert alert-success">
      	<h1><i class="oi oi-chat"></i> Fale conosco</h1>
			</div>
    </div>
  </div>
	<?php if (isset($msgEnviada) && $msgEnviada) { ?>
		<?php $msgEnviada=""; ?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<h5>Sua mensagem foi enviada com sucesso. Agradecemos muito sua contribuição! Ela é essencial para que o Agroecologia em Rede melhore cada dia mais!</h5>
				</div>
			</div>
		</div>
	<?php } else { ?>
		<div class="row mb-4">
			<div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
				<div class="card bg-light border-primary">
					<div class="card-body">
						<form name="form1" method="post" action="contato.php" class="needs-validation">
							<input type="hidden" name="check" value="<?php echo isset($msgEnviada) ? $msgEnviada : ''; ?>">
							<div class="form-group row border-bottom pb-2">
								<label for="nome" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
									<?=$txt['frm_nome']?>
								</label>
								<div class="col-sm-12 col-md-8 col-lg-9">
									<input type="text" class="form-control" name="nome" id="nome" value="<?=$dados->nome?>" required>
								</div>
							</div>

							<div class="form-group row border-bottom pb-2">
								<label for="email" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
									<?=$txt['frm_email']?>
								</label>
								<div class="col-sm-12 col-md-8 col-lg-9">
									<input class="form-control" name="email" type="email" id="email" value="<?=$dados->email?>" required>
								</div>
							</div>

							<div class="form-group row border-bottom pb-2">
								<label for="instituicao" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
									Instituição
								</label>
								<div class="col-sm-12 col-md-8 col-lg-9">
									<input class="form-control" name="instituicao" type="text" id="instituicao" value="<?=$instituicao?>">
								</div>
							</div>

							<div class="form-group row border-bottom pb-2">
								<label for="endereco" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
									Endereço
								</label>
								<div class="col-sm-12 col-md-8 col-lg-9">
									<textarea class="form-control" name="endereco" type="text" id="endereco"><?=$dados->endereco?></textarea>
								</div>
							</div>

							<div class="form-group row border-bottom pb-2">
								<label for="comentarios" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
									Sua mensagem
								</label>
								<div class="col-sm-12 col-md-8 col-lg-9">
									<textarea class="form-control" name="comentarios" type="text" id="comentarios" required></textarea>
								</div>
							</div>

							<div class="form-group row border-bottom pb-2">
								<div class="col-md-12 text-center">
									<input type="submit" class="btn btn-success btn-lg" value="Enviar">
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>

</body>
</html>
<?php
$db->close();
?>
