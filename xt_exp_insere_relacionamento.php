<?php
if (strlen($cookieUsuarioPub) == 0) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");



switch ($tipo) {
	case 'AG':
		$varNome = "Áreas Geográficas";
		$varTabela = "area_geografica";
		$varCampo = "ag_descricao";
		$varID = "ag_id";
		$varTabelaRel = "rel_geo_experiencia";
		$varCampoRel = "rge_id_geo";
		$varIDRel = "rge_id_experiencia";
		break;
	case 'AT':
		$varNome = "Áreas Temáticas";
		$varTabela = "area_tematica";
		$varCampo = "at_descricao";
		$varID = "at_id";
		$varTabelaRel = "rel_area_experiencia";
		$varCampoRel = "rae_id_area";
		$varIDRel = "rae_id_experiencia";
		break;
	case 'IN':
		$varNome = "Institui��es";
		$varTabela = "frm_instituicao";
		$varCampo = "in_nome";
		$varID = "in_id";
		$varTabelaRel = "rel_inst_experiencia";
		$varCampoRel = "rie_id_inst";
		$varIDRel = "rie_id_experiencia";
		break;
	case 'HA':
		$varNome = "Habitats";
		$varTabela = "habitat";
		$varCampo = "ha_descricao";
		$varID = "ha_id";
		$varTabelaRel = "rel_habitat_experiencia";
		$varCampoRel = "rhe_id_habitat";
		$varIDRel = "rhe_id_experiencia";
		break;
	case 'EX':
		$varNome = "Experimentadores";
		$varTabela = "usuario";
		$varCampo = "us_nome";
		$varID = "us_login";
		$varTabelaRel = "rel_usuario_experiencia";
		$varCampoRel = "rue_id_usuario";
		$varIDRel = "rue_id_experiencia";
		break;
	case 'RB':
		$varNome = "Refer�ncias Bibliogr�ficas";
		$varTabela = "bibliografia";
		$varCampo = "bb_titulo";
		$varID = "bb_id";
		$varTabelaRel = "rel_bib_experiencia";
		$varCampoRel = "rbe_id_bibliografia";
		$varIDRel = "rbe_id_experiencia";
		break;
} // fim do switch

$i = 0;
while($i<count($rel)){ 
	$relID = $rel[$i];
	if ($tipo == "EX") {
		$sql = "SELECT * FROM $varTabelaRel WHERE $varCampoRel='$relID' AND $varIDRel=$idExp";
	}
	else {
		$sql = "SELECT * FROM $varTabelaRel WHERE $varCampoRel=$relID AND $varIDRel=$idExp";
	}
	$query = $db->query($sql);
	$numRels = $query->num_rows;
	if ($numRels == 0) {
		if ($tipo == "EX") {
			$sql = "INSERT INTO $varTabelaRel VALUES ('$relID',$idExp)";
		}
		else {
			$sql = "INSERT INTO $varTabelaRel VALUES ($relID,$idExp)";
		}
		$query = $db->query($sql);
		if (!$query) {
    		die($db->error);
		}
   	}
	$i++; 
} 

$db->close();
?>
<script language="JavaScript">
	window.location.href='exp_popup_relacionamentos.php?tipo=<?php echo $tipo; ?>&id=<?php echo $idExp; ?>&nome=<?php echo trim($nome); ?>&relacionamentoInserido=1';
</script>
