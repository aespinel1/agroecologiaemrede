<?php
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57">&nbsp; </td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Pesquisa 
              de experi&ecirc;ncia</strong>&nbsp;&nbsp; 
            </p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
      <form action="exp_pesquisa.php" method="post" name="formChave">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td><p><strong>Busca por palavra chave</strong> 
                - &agrave;s experi&ecirc;ncias s&atilde;o associadas palavras 
                chave, tornando a pesquisa mais r&aacute;pida e eficiente.</p></td>
          </tr>
          <tr> 
            <td height="1" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td> <input name="buscaChave" type="text" id="buscaChave" size="80" maxlength="255" value="<?php echo $buscaChave; ?>"> 
              <input type="submit" class="botaoLogin" value="Buscar"> 
            </td>
          </tr>
        </table>
		</form>
        <br>
      <form action="exp_pesquisa.php" method="post" name="formDesc">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td><p><strong>Busca por descri&ccedil;&atilde;o</strong> 
                - esta busca ir&aacute; vasculhar as descri&ccedil;&otilde;es 
                e resumos das experi&ecirc;ncias cadastradas no SisGEA. Utilize 
                esta apenas nos casos em que a busca por palavras chave n&atilde;o 
                retornar o desejado. Esta busca &eacute; um pouco mais lenta que 
                as demais.</p></td>
          </tr>
          <tr> 
            <td height="1" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td> <input name="buscaDesc" type="text" id="buscaDesc" size="80" maxlength="255" value="<?php echo $buscaDesc; ?>"> 
              <input type="submit" class="botaoLogin" value="Buscar"> 
            </td>
          </tr>
        </table>
        <p>&nbsp;</p>
      </form>
	  <br>
<?php
if (isset($buscaChave) || isset($buscaDesc)) {

	if (isset($buscaChave)) {
	
		$busca = $buscaChave;
		$tipo = "Palavra Chave";
		$sql = "SELECT distinct(ex_descricao),ex_id FROM experiencia_chave,experiencia,palavra_chave ".
			   "WHERE ec_id_experiencia=ex_id AND ec_id_palavra=pc_id AND ".
			   "pc_palavra LIKE '$buscaChave%' AND ex_liberacao='S' ORDER BY ex_descricao";
	}
	else if (isset($buscaDesc)) {
		
		$busca = $buscaDesc;
		$tipo = "Descrição";
		$sql = "SELECT * FROM experiencia WHERE ex_liberacao='S' AND (ex_resumo LIKE '%$buscaDesc%' OR ex_descricao LIKE '%$buscaDesc%') ORDER BY ex_descricao";
	
	}
	
	$query = $db->query($sql);
	$num = $query->num_rows;
	
	if ($num > 0) {
	
		echo "<p class=\"textoPreto10px\">Tipo de busca: <strong>$tipo</strong> <br>
		Foram encontrados as seguintes experiências com o argumento <b>\"$busca\"</b>:
		<br><br>";

		$i = 1;
		while ($row = $query->fetch_object()) {
			
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
	
			$i++;

		}
		
		echo "</p>";			

	} 
	else {
		
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";

	} // fim do if que verifica se a busca retornou os resultados
	
} // fim do if que verifica se uma busca foi feita
?>
	  
    </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>