<?php

$charset='utf-8';
$foradoar=0;
$maquinalocal=0;

if ($foradoar && $_GET['int']=='AGROREDEsisgea')
	setcookie('cookieInterno',true,time()+60*60*24*365,'/');

//Esta é a chave para travar edição (exceto dos admins, que sempre têm acesso a tudo). A configuração é por ano que pode ser editado separados por '|'. Por exemplo, se 07 e 06 estão abertos a edição, a configuração será '06|07'. Atenção: somente usuários do ano ativado poderão editá-lo. No exemplo acima, somente usuários do ano 06 mexem no ano 2006, e os de 07 mexem em 2007. Outro exemplo: se eu coloco $edicao='07', usuários de 2006 não editam nem os de 2006, nem os de 2007, e usuários 07 mexem apenas nos de 2007.
$edicao='09';

//Esta é a chave que indica se estou na internet
$narede=true;


// Número de horas de defasagem com o horário do servidor!
$defasagem=0;
$chave='{YOUR_KEY}';
// Numero padrão máximo de opções para ainda ser rádio ou checkbox. Acima disso se tornará um select...
$padrao_nmax_opcoes=14;
// Numero padrão de colunas nos checklists e radios:
$padrao_numcols=3;
// Tamanho padrão das caixas de textos:
$padrao_outro_size=20;
// Número de linhas padrão das caixas de select múltiplas:
$padrao_size_select_multiplo=10;
// Nome padrão do campo nas tabelas auxiliares em que se indica os divs relacionados quando div_rel=1:
$padrao_campo_id_div_rel="id_div_rel";
// Nomes padrão para os campos 'id' e 'nome' de tabelas auxiliares:
$padrao_campo_nome='nome';
$padrao_campo_id='id';
// Formulário e UF padrões se nenhum tiver sido especificado pelo usuário:
$nomefrm_padrao='frm_exp_base_geral';
$ano_padrao='09';
$data_padrao='2009-04-01';
$padrao_data_inicio='1998-01-01';
$padrao_data_fim='2015-01-01';
$padrao_lingua='pt-br';
$padrao_itens_por_pagina=30;
// Funções que podem ser acessadas pelo usuário sem estar logado:
$funcoes_offline=array('estatisticas','consultar','sobre');
// Deve mostrar o hachurado?
$mostra_hachurado=false;

//Dados do servidor mysql e diretórios
if ($maquinalocal==1) {
	$bd['servidor']="";
	$bd['usuaria'] = "";
	$bd['senha'] = "";
	$bd['agrorede']= "";
	$bd['refs']= "agrorede";
	$dir['base_URL']="http://localhost/agrorede/";
	$dir['base']="/var/www/agrorede/";
} else {
	$bd['usuaria'] = '';
	$bd['senha'] = '';
	$bd['servidor'] = '';
	$bd['agrorede'] = '';
	$bd['refs']= "agroecologiaem";
	$dir['base_URL']="http://www.agroecologiaemrede.org.br/";
	$dir['base']="/home/agroecologiaemrede/public_html/agroecologiaemrede/";
}

// Tipos de campos no mysql que exigem aspas:
$tipos_txt_mysql = array('char', 'blob', 'text', 'binary', 'enum', 'set', 'date');

//Diretórios
$dir['tema']='/temas/';
$dir['imgs']="imgs/";
$dir['icones']="imgs/icones/";
$dir['cont']=$dir['base']."mapeo/conteudo/";
$dir['cont_URL']=$dir['base_URL']."mapeo/conteudo/";
$dir['apoio']=$dir['base']."mapeo/apoio/";
$dir['apoio_URL']=$dir['base_URL']."mapeo/apoio/";
$dir['upload']=$dir['base']."upload/arquivos/";
$dir['upload_URL']=$dir['base_URL']."upload/arquivos/";
$dir['mapas']=$dir['base']."mapas/";
$dir['mapas_URL']=$dir['base_URL']."mapas/";
$dir['mostra_experiencia_URL']=$dir['base_URL']."experiencias.php?experiencia={id}";
$dir['mostra_instituicao_URL']=$dir['base_URL']."instituicoes.php?inst={id}";

$char_sistema='UTF-8';

//Dados de camadas:
//$geoXml['biomas']=$dir['base_url'].'/mapas/areas/biomas.kmz';
//$geoXml['biomas']='http://www.plataformabndes.org.br/mapas/mapas/areas/biomas.kmz';
$geoXml['biomas']='http://www.agroecologiaemrede.org.br/mapas/biomas.kmz';
$geoXml['UC']='http://www.agroecologiaemrede.org.br/mapas/gera_UC.kmz.php';
$geoXml['BrasilIndigena']='http://www.agroecologiaemrede.org.br/mapas/gera_Brasil_Indigena.kmz.php';
$geoXml['BrasilIndigenaLegenda']='http://www.agroecologiaemrede.org.br/mapas/gera_Brasil_Indigena_legenda.kmz.php';
$geoXml['territorios_encontro_dialogos']='http://www.agroecologiaemrede.org.br/mapas/gera_territorios_encontro_dialogos.kmz.php';

$tab_meses=array('janeiro','fevereiro','março','abril','maio','junho', 'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro');

$tab_linguas=array('pt-br','es');


$ano_lim['min']='1996';
$ano_lim['max']='2015';

// Macrorregiões (código do IBGE)
$lista_MR=array(5=>'Centro-oeste',2=>'Nordeste',1=>'Norte',3=>'Sudeste',4=>'Sul',0=>'Nacional', 7=>'Internacional');

// Campos a serem mostrados para cada formulário na tabela inicial:
$campo['frm_exp_base_comum']->mostrar=array(
	array('campo'=>'ex_id', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'cod_usuario', 'admin'=>1, 'ord_padrao'=>false, 'combo'=>true, 'href'=>"../usuarios.php?usu={s}"),
	array('campo'=>'ex_descricao', 'admin'=>0, 'ord_padrao'=>true, 'combo'=>false, 'href'=>"index.php?f=formulario_geral&nomefrm={nomefrm}&id={id}"),
	array('campo'=>'dt_criacao', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'ex_liberacao', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>true, 'href'=>'')
);
$campo['frm_exp_geral']->mostrar=array(
	array('campo'=>'ex_id', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'cod_usuario', 'admin'=>1, 'ord_padrao'=>false, 'combo'=>true, 'href'=>"../usuarios.php?usu={s}"),
	array('campo'=>'ex_descricao', 'admin'=>0, 'ord_padrao'=>true, 'combo'=>false, 'href'=>"index.php?f=formulario_geral&nomefrm={nomefrm}&id={id}"),
	array('campo'=>'ex_cidade_ref', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'dt_criacao', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'ex_liberacao', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>true, 'href'=>'')
);
$campo['frm_exp_cca']->mostrar=array(
	array('campo'=>'id', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'cod_usuario', 'admin'=>1, 'ord_padrao'=>false, 'combo'=>true, 'href'=>"../usuarios.php?usu={s}"),
	array('campo'=>'ex_descricao', 'admin'=>0, 'ord_padrao'=>true, 'combo'=>false, 'href'=>"index.php?f=formulario_geral&nomefrm={nomefrm}&id={id}"),
	array('campo'=>'ex_cidade_ref', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'dt_criacao', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'ex_liberacao', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>true, 'href'=>'')
);
$campo['frm_instituicao']->mostrar=array(
	array('campo'=>'in_id', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'cod_usuario', 'admin'=>1, 'ord_padrao'=>false, 'combo'=>true, 'href'=>"../usuarios.php?usu={s}"),
	array('campo'=>'in_nome', 'admin'=>0, 'ord_padrao'=>true, 'combo'=>false, 'href'=>"index.php?f=formulario_geral&nomefrm={nomefrm}&id={id}"),
	array('campo'=>'in_tipo', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>true, 'href'=>''),
	array('campo'=>'ei_cidade', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'dt_criacao', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>false, 'href'=>''),
	array('campo'=>'in_liberacao', 'admin'=>0, 'ord_padrao'=>false, 'combo'=>true, 'href'=>'')
);

// Campos que devem ser alvo de buscas em cada tipo de formulário:
$campo['frm_exp_base_comum']->textoBusca=array('ex_descricao', 'ex_resumo');
$campo['frm_exp_geral']->textoBusca=array('ex_descricao', 'ex_resumo', 'ex_chamada');
$campo['frm_exp_cca']->textoBusca=array('ex_descricao', 'ex_resumo');
$campo['frm_instituicao']->textoBusca=array('in_nome');

// Campos padrão de cada formulário (de localização geográfica, de nome e de id):
$campo['frm_exp_base_comum']->id='ex_id';
$campo['frm_exp_base_comum']->nome='ex_descricao';
$campo['frm_exp_base_comum']->localizacao='ex_cidade_ref';
$campo['frm_exp_base_comum']->lat='ex_lat';
$campo['frm_exp_base_comum']->lng='ex_lng';
$campo['frm_exp_base_comum']->localizacao_lat='ref_lat';
$campo['frm_exp_base_comum']->localizacao_lng='ref_lng';
$campo['frm_exp_base_comum']->abrangencia='ex_abrangencia';
$campo['frm_exp_base_comum']->abrangencia_latlng='ex_abrangencia_latlng';
$campo['frm_exp_base_comum']->digitadora='cod_usuario';
$campo['frm_exp_base_comum']->status='ex_liberacao';
$campo['frm_exp_base_comum']->descricao='ex_resumo';
$campo['frm_exp_base_comum']->criacao='dt_criacao';
$campo['frm_exp_geral']->id='ex_id';
$campo['frm_exp_geral']->nome='ex_descricao';
$campo['frm_exp_geral']->localizacao='ex_cidade_ref';
$campo['frm_exp_geral']->localizacao_lat='ref_lat';
$campo['frm_exp_geral']->localizacao_lng='ref_lng';
$campo['frm_exp_geral']->lat='ex_lat';
$campo['frm_exp_geral']->lng='ex_lng';
$campo['frm_exp_geral']->abrangencia='ex_abrangencia';
$campo['frm_exp_geral']->abrangencia_latlng='ex_abrangencia_latlng';
$campo['frm_exp_geral']->digitadora='cod_usuario';
$campo['frm_exp_geral']->status='ex_liberacao';
$campo['frm_exp_geral']->descricao='ex_resumo';
$campo['frm_exp_geral']->criacao='dt_criacao';
$campo['frm_exp_cca']->id='id';
$campo['frm_exp_cca']->nome='ex_descricao';
$campo['frm_exp_cca']->localizacao='ex_cidade_ref';
$campo['frm_exp_cca']->localizacao_lat='ref_lat';
$campo['frm_exp_cca']->localizacao_lng='ref_lng';
$campo['frm_exp_cca']->lat='ex_lat';
$campo['frm_exp_cca']->lng='ex_lng';
$campo['frm_exp_cca']->abrangencia='ex_abrangencia';
$campo['frm_exp_cca']->abrangencia_latlng='ex_abrangencia_latlng';
$campo['frm_exp_cca']->digitadora='cod_usuario';
$campo['frm_exp_cca']->status='ex_liberacao';
$campo['frm_exp_cca']->descricao='ex_resumo';
$campo['frm_exp_cca']->criacao='dt_criacao';
$campo['frm_instituicao']->id='in_id';
$campo['frm_instituicao']->nome='in_nome';
$campo['frm_instituicao']->localizacao='ei_cidade';
$campo['frm_instituicao']->digitadora='cod_usuario';
$campo['frm_instituicao']->status='in_liberacao';


//*
//*
// Configurações dos gráficos:
//*
//*
$grafx=720;
$grafy=480;
/* Fontes do Jpgraph!
DEFINE("FF_FONT0",1);
DEFINE("FF_FONT1",2);
DEFINE("FF_FONT2",4);
DEFINE("FF_COURIER",10);
DEFINE("FF_VERDANA",11);
DEFINE("FF_TIMES",12);
DEFINE("FF_COMIC",14);
DEFINE("FF_ARIAL",15);
DEFINE("FF_GEORGIA",16);
DEFINE("FF_TREBUCHE",17);
DEFINE("FF_VERA",19);
DEFINE("FF_VERAMONO",20);
DEFINE("FF_VERASERIF",21);*/
$tmp=gd_info();
//putenv('GDFONTPATH=/home/fbesorg/public_html/fontes');
//echo $_GLOBALS['GDFONTPATH'];
if ($tmp['FreeType Support']) {
	DEFINE("FONTE",14);
	$tam_legenda=12;
	$tam_eixos=10;
	$tam_titulo=18;
	$tam_dados=12;
} else {
	DEFINE("FONTE",2);
	$tam_legenda=12;
	$tam_eixos=10;
	$tam_titulo=22;
	$tam_dados=12;
}
/*$extensao='jpg';
$tipografico='jpeg';*/
$extensao='png';
$tipografico='png';
$corfundo1='#6666ff';
$corfundo2='#ffffff';
$corfonte='black';
$corgrade='black@0.5';
$coreixo='black';

// Configurações do mapa:
$padrao_mapa->autozoom = true;
// Chave do google maps para agroecologiaemrede.com.br
$padrao_mapa->apiKey="";
$padrao_mapa->controlType ='large';
$padrao_mapa->showControl=true;
$padrao_mapa->addMapType[]='G_PHYSICAL_MAP';
$padrao_mapa->showType=true;
$padrao_mapa->tipo_mapa="G_PHYSICAL_MAP";
$padrao_mapa->mapWidth=800;
$padrao_mapa->mapHeight=430;
$padrao_mapa->posicaoControle="G_ANCHOR_BOTTOM_RIGHT";
?>
