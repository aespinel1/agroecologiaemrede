DROP TABLE IF EXISTS `lista_area_atuacao`;
CREATE TABLE `lista_area_atuacao` (
  `id` int(11) NOT NULL auto_increment,
  `mae` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_area_atuacao SET id=1, mae=0, nome='Rural';
INSERT INTO lista_area_atuacao SET id=2, mae=0, nome='Urbana';
INSERT INTO lista_area_atuacao SET id=3, mae=1, nome='Quilombolas';
INSERT INTO lista_area_atuacao SET id=4, mae=1, nome='Ind�genas';
INSERT INTO lista_area_atuacao SET id=5, mae=1, nome='Agricultura Familiar';



DROP TABLE IF EXISTS `lista_forma_org`;
CREATE TABLE `lista_forma_org` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_forma_org SET id=1, nome='Grupo informal';
INSERT INTO lista_forma_org SET id=2, nome='Associa��o';
INSERT INTO lista_forma_org SET id=3, nome='Cooperativa';
INSERT INTO lista_forma_org SET id=4, nome='Micro ou pequena empresa';






-- Preciso colocar aqui depois o id do CNAE! A maneira de fazer isso � fazendo um pequeno script para facilitar a identifica��o do ramo de atividade...
DROP TABLE IF EXISTS `lista_ativ_economica`;
CREATE TABLE `lista_ativ_economica` (
  `id` int(11) NOT NULL auto_increment,
  `id_CNAE` varchar(9) NOT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_ativ_economica SET id=1, nome='Limpeza e higiene';
INSERT INTO lista_ativ_economica SET id=2, nome='Decora��o';
INSERT INTO lista_ativ_economica SET id=3, nome='Bijuteria';
INSERT INTO lista_ativ_economica SET id=4, nome='Confec��o';
INSERT INTO lista_ativ_economica SET id=5, nome='Reciclagem';
INSERT INTO lista_ativ_economica SET id=6, nome='Alimenta��o';
INSERT INTO lista_ativ_economica SET id=7, nome='Cultura e lazer';
INSERT INTO lista_ativ_economica SET id=8, nome='Cr�dito / Fundo rotativo';
INSERT INTO lista_ativ_economica SET id=9, nome='Turismo';
INSERT INTO lista_ativ_economica SET id=10, nome='Movelaria';
INSERT INTO lista_ativ_economica SET id=11, nome='Sa�de';
INSERT INTO lista_ativ_economica SET id=12, nome='Agricultura familiar - in natura';
INSERT INTO lista_ativ_economica SET id=13, nome='Agricultura familiar - processado';
INSERT INTO lista_ativ_economica SET id=14, nome='Agricultura urbana';
INSERT INTO lista_ativ_economica SET id=15, nome='Fitoter�picos';
INSERT INTO lista_ativ_economica SET id=16, nome='Presta��o de servi�os';
INSERT INTO lista_ativ_economica SET id=17, nome='Artesanato';












DROP TABLE IF EXISTS `lista_forma_trab`;
CREATE TABLE `lista_forma_trab` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_forma_trab SET id=1, nome='coletivamente';
INSERT INTO lista_forma_trab SET id=2, nome='individualmente';
INSERT INTO lista_forma_trab SET id=3, nome='misto';














DROP TABLE IF EXISTS `lista_forma_comerc`;
CREATE TABLE `lista_forma_comerc` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_forma_comerc SET id=1, nome='coletivamente';
INSERT INTO lista_forma_comerc SET id=2, nome='individualmente';
INSERT INTO lista_forma_comerc SET id=3, nome='misto';












-- Consultar a lista de categorias por forma de comercializa��o que eu criei no site www.agroecosol.da.ru
DROP TABLE IF EXISTS `lista_metodo_comerc`;
CREATE TABLE `lista_metodo_comerc` (
  `id` int(11) NOT NULL auto_increment,
  `mae` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_metodo_comerc SET id=1, nome='Direta', mae=0;
INSERT INTO lista_metodo_comerc SET id=2, nome='Indireta', mae=0;
INSERT INTO lista_metodo_comerc SET id=3, nome='Compromisso expl�cito entre consumidores finais e produtores', mae=1;
INSERT INTO lista_metodo_comerc SET id=4, nome='Sem compromisso expl�cito dos consumidores', mae=1;
INSERT INTO lista_metodo_comerc SET id=5, nome='V�nculo da moeda social', mae=1;
INSERT INTO lista_metodo_comerc SET id=6, nome='Por cadeia �tica', mae=2;
INSERT INTO lista_metodo_comerc SET id=7, nome='Por cadeia convencional n�o local', mae=2;
INSERT INTO lista_metodo_comerc SET id=8, nome='Por cadeia convencional local', mae=2;
INSERT INTO lista_metodo_comerc SET id=9, nome='Institucional', mae=2;
INSERT INTO lista_metodo_comerc SET id=10, nome='Feiras', mae=4;
INSERT INTO lista_metodo_comerc SET id=11, nome='Fornecimento para intermedi�rios', mae=7;
INSERT INTO lista_metodo_comerc SET id=12, nome='Fornecimaneo para lojas de varejo em geral', mae=7;
INSERT INTO lista_metodo_comerc SET id=13, nome='Em estabelecimento comercial (loja) mantido pelo grupo', mae=4;
INSERT INTO lista_metodo_comerc SET id=14, nome='Atrav�s de visitas domiciliares', mae=4;
INSERT INTO lista_metodo_comerc SET id=15, nome='Fornecimento para �rg�os p�blicos', mae=2;
INSERT INTO lista_metodo_comerc SET id=16, nome='Grupos de consumidores solid�rios', mae=3;
INSERT INTO lista_metodo_comerc SET id=17, nome='Fornecimento para setor industrial', mae=3;
INSERT INTO lista_metodo_comerc SET id=18, nome='Em casa', mae=3;
















DROP TABLE IF EXISTS `lista_conceitos`;
CREATE TABLE `lista_conceitos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_conceitos SET id=1, nome='Ruim';
INSERT INTO lista_conceitos SET id=2, nome='Razo�vel';
INSERT INTO lista_conceitos SET id=3, nome='Bom';
INSERT INTO lista_conceitos SET id=4, nome='�timo';
INSERT INTO lista_conceitos SET id=5, nome='N�o sei avaliar';

















DROP TABLE IF EXISTS `lista_faturamento`;
CREATE TABLE `lista_faturamento` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_faturamento SET id=1, nome='Menos de R$100,00';
INSERT INTO lista_faturamento SET id=2, nome='de R$101,00 a R$300,00';
INSERT INTO lista_faturamento SET id=3, nome='de R$301,00 a R$500,00';
INSERT INTO lista_faturamento SET id=4, nome='de R$501,00 a R$1.000,00';
INSERT INTO lista_faturamento SET id=5, nome='de R$1.001,00 a R$2.000,00';
INSERT INTO lista_faturamento SET id=6, nome='de R$2.001,00 a R$5.000,00';
INSERT INTO lista_faturamento SET id=7, nome='de R$5.001,00 a R$10.000,00';
INSERT INTO lista_faturamento SET id=8, nome='de R$10.001,00 a R$15.000,00';









-- Se � classe, n�o coloco o item. Os valores ser�o classe-item, sendo classe de 2 d�gitos e item de 2 d�gitos tb.
DROP TABLE IF EXISTS `lista_CNPS`;
DROP TABLE IF EXISTS `lista_CNPS_classes`;
CREATE TABLE `lista_CNPS` (
  `id` varchar(5) NOT NULL,  
  `nome` varchar(255) NOT NULL,
  `descricao` text not null,
PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO lista_CNPS SET id='01', nome='Produtos e subst�ncias qu�micas e minerais e aqueles de origem animal ou vegetal, predominantemente destinados ao uso industrial', descricao='Contrap�e-se, portanto, aos produtos destinados ao uso final, com exce��o daqueles inclu�dos nos itens 01.40 e 01.45. Em geral, os produtos e subst�ncias deste item dependem ainda de algum tipo de tratamento para seu consumo final, podendo ent�o estar inclu�dos em outros itens previstos nas demais classes.';
INSERT INTO lista_CNPS SET id='01-10', nome='Resinas em geral', descricao='';





DROP TABLE IF EXISTS `lista_produtos_servicos`;
CREATE TABLE `lista_produtos_servicos` (
  `id` int(11) NOT NULL auto_increment,
  `id_CNPS` varchar(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



















DROP TABLE IF EXISTS `lista_fonte_info`;
CREATE TABLE `lista_fonte_info` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_fonte_info SET id=1, nome='R�dio';
INSERT INTO lista_fonte_info SET id=4, nome='TV';
INSERT INTO lista_fonte_info SET id=6, nome='Internet';
INSERT INTO lista_fonte_info SET id=7, nome='Amigos';
INSERT INTO lista_fonte_info SET id=2, nome='Entidade de apoio';
INSERT INTO lista_fonte_info SET id=5, nome='F�rum de Economia Solid�ria';
INSERT INTO lista_fonte_info SET id=3, nome='Folder, faixa, cartaz, banner, outdoor';

















DROP TABLE IF EXISTS `lista_feiras_ES`;
CREATE TABLE `lista_feiras_ES` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


















DROP TABLE IF EXISTS `lista_abrangencia`;
CREATE TABLE `lista_abrangencia` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_abrangencia SET id=1, nome='Local';
INSERT INTO lista_abrangencia SET id=2, nome='Regional';
INSERT INTO lista_abrangencia SET id=3, nome='Estadual';
INSERT INTO lista_abrangencia SET id=4, nome='Nacional';
INSERT INTO lista_abrangencia SET id=5, nome='Internacional';


















DROP TABLE IF EXISTS `lista_periodicidade`;
CREATE TABLE `lista_periodicidade` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_periodicidade SET id=1, nome='Semanal';
INSERT INTO lista_periodicidade SET id=2, nome='Quinzenal';
INSERT INTO lista_periodicidade SET id=3, nome='Mensal';
INSERT INTO lista_periodicidade SET id=4, nome='Semestral';
INSERT INTO lista_periodicidade SET id=5, nome='Anual';





















DROP TABLE IF EXISTS `lista_participacao_construcao`;
CREATE TABLE `lista_participacao_construcao` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;




















DROP TABLE IF EXISTS `lista_diferencas`;
CREATE TABLE `lista_diferencas` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;




















DROP TABLE IF EXISTS `lista_aspectos_positivos`;
CREATE TABLE `lista_aspectos_positivos` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;




DROP TABLE IF EXISTS `lista_aspectos_negativos`;
CREATE TABLE `lista_aspectos_negativos` (
  `id` int(11) NOT NULL auto_increment,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

