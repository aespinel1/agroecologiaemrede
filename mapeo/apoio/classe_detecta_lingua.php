<?php
/**
* Implements language negotiation
*
* This class can be used to: detect all languages acepted by user browser,
* choose the most qualified (primary language) or to return the list of
* languages.
*
* This class was inspired in "lang_detect" class by Carlos Falo HervÃ¡s,
* that can be found at http://www.phpclasses.org/browse/package/543.html
*
* @author Daniel Costa <http://danielcosta.info>
* @link http://danielcosta.info
*/

/**
* Implements language negotiation
*/
class LanguageDetect {
  
    /**
     * Counter for how many languages are supported by user browser
     */
    //private $_languages;
    var $_languages='';
    
    /**
     * Language list pairs [language,qualifier]
     */
    //private $_languagesList;
    var $_languagesList=array();
    
    /**
     * Original HTTP_ACCEPT_LANGUAGES
     */
    //private $_httpAcceptLanguages;
    var $_httpAcceptLanguages='';
    
    
    /**
     * Constructor: loads http_accept_language
     */
    function languageDetect()
    {
        $this->_httpAcceptLanguages = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        $this->languageInit();
    }
    
    /**
     * Returns true if given locale prefix matches primary lang prefix
     */
    function IsPrimaryLanguage($languagePrefix)
    {
        $this->ResetList();
        $subList1 = explode("-",$languagePrefix);
        $prefix1 = $subList1[0] ;
        $subList2 = explode("-",key($this->_languagesList)) ;
        $prefix2 = $subList2[0] ;
        if ($prefix1==$prefix2)
            return true;
        else
            return false;
    }
    
    /**
     * Return full locale string for primary language
     */
    function getPrimaryLanguage()
    {
        $this->resetList();
        return key($this->_languagesList);
    }
    
    /**
     * Return prefix string for primary language
     */
    function getPrimaryPrefix()
    {
        $this->resetList();
        $subList = explode("-",key($this->_languagesList));
    }
    
    /**
     * Return qualifier value for the prefix in given locale
     */
    function findLanguage($locale)
    {
        $found = 0;
        $this->resetList();
        $subList = explode("-",$locale);
        $prefix1 = $subList[0];
        while ($current = each($this->_languagesList)) {
            $subList = explode("-",$current[0]);
            $prefix2 = $subList[0];
            if ($prefix2 == $prefix1) {
                return $current[1];
            }
        }
        return false;
    }
    
    /**
     * Fills sublist with an ordered list of accepted languages from given
     * array based on each locale prefix. Ordering is on qualifier so first
     * element in the array is best match from the given list.
     */
    function getLanguagesList($arr)
    {
        $lista = Array();
        $this->resetList();
        while ($current = each($this->_languagesList)) {
            $subList2 = explode("-",$current[0]);
            $locale = $subList2[0];
            if (in_array($locale,$arr))
                $lista += Array($locale => $current[1]) ;
            if (in_array($current[0],$arr))
                $lista += Array($current[0] => $current[1]) ;
        }
        // This will order based on qualifier descending
        if (count($lista)) {
            arsort($lista);
            return $lista;
        } else {
            return false;
        }
    }
    
    /**
     * Prints the languages list
     */
    function printList()
    {
        $this->resetList();
        while($current = each($this->_languagesList)) {
            echo $current[0] . ": " . $current[1] ."<br />";
        }
    }
    
    /**
     * Initialize language list
     */
    //private function languageInit()
    function languageInit()
    {
        $this->_languagesList = Array();
        $list = explode(",",$this->_httpAcceptLanguages);
        for ($i = 0; $i < count($list); $i++) {
            $counter = strchr($list[$i],";");
            if ($counter === false) {
                // No qualifier, it is only a locale
                $this->_languagesList += Array($list[$i] => 100);
                $this->_languages++;
            } else {
                // Has a qualifier rating
                $q = explode(";",$list[$i]);
                $locale = $q[0];
                $q = explode("=",$q[1]);
                $this->_languagesList += Array($locale => ($q[1]*100));
                $this->_languages++;
            }
        }
        return ($this->_languages);
    }
    
    /**
     * Resets languages list
     */
    //private function resetList(){
    function resetList(){
        reset($this->_languagesList);
    }
    
}
?>
