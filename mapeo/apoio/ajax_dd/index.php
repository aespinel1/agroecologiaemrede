<!DOCTYPE html>
<html>
<head>
	<script language="JavaScript" src="ajax.js"></script>
	<link href="css.css" rel="stylesheet" type="text/css"/>	
</head>
<body>
<div id="loading"></div>

	<table border=0 cellpadding=0 cellspacing="0" align="center" width="40%">
	<form>		
		<tr><td colspan="2" height="25"></td></tr>		
		<tr><td colspan="2" class="textheading">AJAX Dropdown to Dropdown</td></tr>
		<tr><td colspan="2" height="25"></td></tr>		
		<tr><td class="text" width="50%">Select category : </td><td>
				<?php
					require_once("ajax_select.php");
					$obj = new AjaxDropdown('lista_estados');
					$uf_padrao=11;
					$strRet=$obj->fazOptions('--Select--', 'id_UF', 'Estado', $uf_padrao);
				?>
				<select name="selCat" class="text" onChange="javascript:Ajax.Request('method=getXML&primeiro=--Select--&tabela=lista_cidades_ibge&campo_id=id&campo_nome=cidade&padrao=&pai_nome=&pai_valor='+this.value, Ajax.Response);">
					<?php echo $strRet; ?>
				</select>
				</td></tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr><td class="text">Sub categories :</td><td>
				<select name="selSubCat" id="selSubCat" class="text">
					<option value="0">--Select--</option>
					<option value="12">test</option>
				</select>
				</td></tr>
	</form>
	</table>
</body>
</html>