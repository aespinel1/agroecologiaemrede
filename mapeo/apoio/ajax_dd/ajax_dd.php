<?php

require_once "../../config.php";
//Aqui eu recupero arquivos de configuração, funções comuns do programa, funções de acentuação, e funções de Formulário (se estou na área restrita):
require_once '../../'.$dir['apoio']."funcoes_comuns.php";
require_once '../../../../includes/conversor_acentos_funcoes.php';
require_once '../../'.$dir['apoio']."ajax_select.php";
//Faço a conexão ao banco de dados:
$db = conecta($bd);
$db->select_db($bd['ref']);

//Aqui envio as informações de cabeçalho:
header('Content-type: text/html; charset='.$charset);
?>
<!DOCTYPE html>
<html>
<head>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=<?php echo $charset; ?>">
	<script language="JavaScript" src="ajax.js"></script>
	<link href="css.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div id="carregando"></div>

	<table border=0 cellpadding=0 cellspacing="0" align="center" width="40%">
	<form>
		<tr><td colspan="2" height="25"></td></tr>
		<tr><td colspan="2" class="textheading">AJAX Dropdown to Dropdown</td></tr>
		<tr><td colspan="2" height="25"></td></tr>
		<tr><td class="text" width="50%">Select category1 : </td><td>
				<?php

					$obj = new AjaxDropdown('CNAE2');

					$strRet=$obj->fazOptions('--Selecione--', 'id', 'nome', '', "LENGTH(id)=1");


				$d = criptografa("LENGTH(id)=3 AND SUBSTRING(id,1,1)='{valor}'", $chave);
				?>
				<select name="cat1" class="text" onChange="javascript:Ajax.Request('method=getXML&onde=<?php echo urlencode($d); ?>&valor='+this.value+'&primeiro=--Selecione--&tabela=CNAE2&campo_id=id&campo_nome=nome&padrao=', Ajax.Response, 'selcubcat');">
					<?php echo $strRet; ?>
				</select>
				</td></tr>
				<tr><td colspan="2" height="5"></td></tr>
				<tr><td class="text">Sub categories :</td><td>
				<select name="selcubcat" id="selcubcat" class="text">
					<option value="0">--Select--</option>
					<option value="12">test</option>
				</select>
				</td></tr>
	</form>
	</table>
</body>
</html>
<?php
?>
