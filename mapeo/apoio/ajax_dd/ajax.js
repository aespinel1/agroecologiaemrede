var Ajax = new Object();

Ajax.Request = function(params, callbackMethod, alvo, limpa, idCarregando)
{
	if ( params == 0 )
		return;
	Page.getPageCenterX();
	if (typeof idCarregando!='undefined')
		Ajax.idCarregando = document.getElementById(idCarregando);
	Ajax.alvo=alvo;
	Ajax.limpa = (limpa) ? limpa : [];
	Ajax.request = Ajax.createRequestObject();
	Ajax.request.onreadystatechange = callbackMethod;
	Ajax.request.open("POST", 'apoio/ajax_select.php?'+params, true);
	Ajax.request.send('apoio/ajax_select.php?'+params);
}
Ajax.Response = function ()
{
	if(Ajax.CheckReadyState(Ajax.request))
	{
		document.getElementById(Ajax.alvo).length = 0;
		for (var i=0; i<Ajax.limpa.length;i++)
			document.getElementById(Ajax.limpa[i]).length = 0;
		//document.getElementById(Ajax.alvo).options[0] = new Option("--Select--","0");
		var	response = Ajax.request.responseXML.documentElement;
		var _data = response.getElementsByTagName('categoria');
		if(_data.length == 0)
		{
			document.getElementById(Ajax.alvo).options[0] = new Option("Não encontrei dados...",'0');
		}
		for (var i = 0 ; i < _data.length ; i ++ )
		{
			var res = response.getElementsByTagName('id')[i].firstChild.data;
			if (res=='-9999')
				res='';
			document.getElementById(Ajax.alvo).options[i] = new Option(response.getElementsByTagName('fname')[i].firstChild.data,res);
			if (response.getElementsByTagName('padrao')[i].firstChild.data==1) document.getElementById(Ajax.alvo).options[i].selected=true;
		}
		$('.selectpicker').selectpicker('refresh');
	}
}

Ajax.Request2 = function(url, callbackMethod, el)
{
	if ( !url )
		return;
	Ajax.alvo2=el;
	Ajax.request2 = Ajax.createRequestObject();
	Ajax.request2.onreadystatechange = callbackMethod;
	Ajax.request2.open("GET", url, true);
	Ajax.request2.send(url);
}
Ajax.contaDadosMapa = function ()
{
	if(Ajax.CheckReadyState(Ajax.request2))
	{
		var	response = Ajax.request2.responseXML.documentElement;
		var markers = response.getElementsByTagName('pt');
		numpts = markers[0].getAttribute("num");
		$('.'+Ajax.alvo2).text(numpts);
	}
}


Ajax.createRequestObject = function()
{
	var obj;
	if(window.XMLHttpRequest)
		obj = new XMLHttpRequest();
	else if(window.ActiveXObject)
		obj = new ActiveXObject("MSXML2.XMLHTTP");
	return obj;
}

Ajax.CheckReadyState = function(obj)
{
		if(obj.readyState < 4 && Ajax.idCarregando)
			Ajax.idCarregando.style.visibility='visible';
		if(obj.readyState == 4)
			if(obj.status == 200) {
				if (Ajax.idCarregando)
					Ajax.idCarregando.style.visibility='hidden';
				return true;
			} else
				if (Ajax.idCarregando)
					Ajax.idCarregando.innerHTML = "HTTP " + obj.status;
}

var Page = new Object();
Page.width;
Page.height;
Page.top;

Page.loadOut = function ()
{
	document.getElementById('carregando').style.visibility='hidden';
}
Page.getPageCenterX = function ()
{
		var fWidth;
		var fHeight;
		//For old IE browsers
		if(document.all)
		{
		fWidth = document.body.clientWidth;
		fHeight = document.body.clientHeight;
		}
		//For DOM1 browsers
		else if(document.getElementById &&!document.all)
		{
		fWidth = innerWidth;
		fHeight = innerHeight;
		}
		else if(document.getElementById)
		{
		fWidth = innerWidth;
		fHeight = innerHeight;
		}
		//For Opera
		else if (is.op)
		{
		fWidth = innerWidth;
		fHeight = innerHeight;
		}
		//For old Netscape
		else if (document.layers)
		{
		fWidth = window.innerWidth;
		fHeight = window.innerHeight;
		}
	Page.width = fWidth;
	Page.height = fHeight;
	Page.top = window.document.body.scrollTop;
}
