<?php
//coding:utf-8
/*Pelo Javascript, foram testadas várias coisas, mas faltaram duas importantes:
	1. Checar se o número do formulário inserido já não existe! Isso é
	pra não haver cruzamentos no caso de formulários em que o usuário coloca a ID
	manualmente (coisa rara: somente em casos de alimentação de CAMPO, em que
	os formulários reais têm um id definido);
	2. checar a quantidade máxima
	de checkboxes para os campos com repetição. É isso que vou checar agora.
	Caso o teste dê errado, terei que recarregar o formulário como estava, sem
	armazenar no mysql, e dar o aviso de erro e o campo em que ele ocorreu!
*/
if (!$id && $_POST['id']) {
	$tmp=faz_query("SELECT ".$campo[$FrmAno]->nome." FROM ".$tab." AS a WHERE a.".$campo[$FrmAno]->id."='".$_POST['id']."'", '', 'row');
	if (count($tmp)) $erro['id_repetido']=$tmp[0][0];
}
foreach ($mapa as $m)
	if (!isset($_POST[$m->campo]) && !trim($_POST[$m->campo]) && $cmd[$m->campo]->regras=='NotEmpty') {
		$erro['erro_NotEmpty'] = $m->titulo.': '.$txt['erro_NotEmpty'];
	} elseif ($m->tipo_form=='file' && !$cmd[$m->campo]->link) {
		// Caso seja tipo arquivo, tenho que verificar se deu certo o upload do arquivo! Se deu certo, já crio o $_POST (como array) com o(s) nome(s) do(s) arquivo(s) que vai/vão ser colocado(s) no banco de dados:
		for ($i=0;$i<($m->repeticoes+1);$i++) {
			if ($_POST['hidden_'.$m->campo][$i]) {
				if ($_POST[$m->campo][$i]) {
					unlink($dir['upload'].$_POST['hidden_'.$m->campo][$i]);
					unset($_POST[$m->campo][$i]);
					} else {
						$_POST[$m->campo][$i]=$_POST['hidden_'.$m->campo][$i];
					}
			} elseif ($_FILES[$m->campo]['error'][$i]==0) {
				$nomearq = $nomefrm.'_'.$m->campo.'_'.$i.'_{id}_'.str_replace(' ','_',tira_acentos(trim($_FILES[$m->campo]['name'][$i])));
				if (move_uploaded_file($_FILES[$m->campo]['tmp_name'][$i], $dir['upload'].$nomearq)) {
					$_POST[$m->campo][$i]=$nomearq;
				} else {
						// Não deu certo o upload!
						$erro['falha_upload']=$m->titulo.': '.$txt['erro_falha_upload'];
					}
			}
		}
	} elseif (in_array($m->tipo_form, array('checkbox','file')) || $m->repeticoes) {
		if (is_array($_POST[$m->campo]) && $m->tipo_form=='file')
			for ($i=0;$i<($m->repeticoes+1);$i++)
				if ($_POST['hidden_'.$m->campo][$i])
					if ($_POST[$m->campo][$i])
						unset($_POST[$m->campo][$i]);
					else $_POST[$m->campo][$i]=$_POST['hidden_'.$m->campo][$i];
				elseif (!$_POST[$m->campo][$i]) unset($_POST[$m->campo][$i]);
		$n=0;
		for ($i=0;$i<$m->repeticoes;$i++)
			if (trim($_POST[$m->campo.'_outro'.$i]))
				$n++;
		if (is_array($_POST[$m->campo])) {
			foreach ($_POST[$m->campo] as $tmp)
				if ($tmp && strval($tmp)<>'-9999')
					$n++;
		} elseif ($_POST[$m->campo] && strval($_POST[$m->campo])<>'-9999') $n++;
		if ($n > $m->repeticoes)
			$erro['excesso_opcoes'].=$m->titulo.', ';
	} elseif ($m->tipo_form=='radio' && strpos($m->regras,'Empty') && $_POST[$m->campo]=='-9999' && !trim($_POST[$m->campo.'_outro'])) {
		$erro['erro_NotEmpty']=$m->titulo.': '.$txt['erro_NotEmpty'];
	} elseif (in_array($m->campo, array($campo[$FrmAno]->abrangencia_latlng, $campo[$tab_base_comum]->abrangencia_latlng)) && $_POST[$m->campo] && $_POST[$m->campo!="0,00/0,00;0,00/0,00"]) {
		//Tenho que verificar se a pessoa colocou corretamente os dados de latitude e longitude em série, usando ";" para dividir entre os pares, e o "/" para dividir entre a latitude  e longitude de cada par:
		$tmp1 = explode(';',str_replace(' ','',$_POST[$m->campo]));
		foreach ($tmp1 as $t) {
			$tmp2 = explode('/',str_replace(',','.',$t));
			if ((floatval($tmp2[0]) && !floatval($tmp2[1]))
				|| (!floatval($tmp2[0]) && floatval($tmp2[1]))) {
				$erro['erro_formato']=$m->titulo.': '.$txt['erro_formato_numerico'];
				break;
			}
		}
	}

if (isset($erro) && $erro) {
	$aviso='ERRO!<br />';
	if ($erro['id_repetido']) $aviso.=str_replace('{campos}', $erro['id_repetido'], $txt['erro_id_repetido']).'<br />';
	if ($erro['excesso_opcoes']) $aviso.=str_replace('{campos}', '<br />"'.substr($erro['excesso_opcoes'],0,-2).'"', $txt['erro_excesso_opcoes']);
	if ($erro['erro_NotEmpty']) $aviso.=$erro['erro_NotEmpty'];
	if ($erro['falha_upload']) $aviso.=$erro['falha_upload'];
	if ($erro['erro_formato']) $aviso.=$erro['erro_formato'];
} else {
	// Se não houve nenhum destes erros... alimentar!!
	//Aqui eu tenho que alimentar o banco de dados MySQL com os dados do formulário, e eventualmente mudar tabela, se é o caso de uma tabela de múltiplas páginas.

	// Tenho que definir se vou atualizar ou inserir novo formulário. Posso simplesmente
	//   testar se o $id está definido e se é $novofrm. Mas pode ser que uma pessoa tenha inserido apenas a
	//   primeira tabela, portanto é bom ainda assim verificar se o id existe naquele caso...
	unset ($tmp);
	if ($id && !$novofrm)
		$tmp=faz_query('SELECT * FROM '.$tab.' WHERE '.$campo[$FrmAno]->id.'="'.$id.'"', '', 'num_rows');
	if (!$tmp) {
		// Descubro o último id, para definir o próximo (que será inserido):
		$tmp=faz_query('SELECT '.$campo[$FrmAno]->id.' FROM '.$tab.' ORDER BY '.$campo[$FrmAno]->id.' DESC LIMIT 1', '', 'rows');
		if ($tab_base_comum)
			$tmp2=faz_query('SELECT '.$campo[$tab_base_comum]->id.' FROM '.$tab_base_comum.' ORDER BY '.$campo[$tab_base_comum]->id.' DESC LIMIT 1', '', 'rows');
		if (!$id)
			$id = ($tmp[0][0] > $tmp2[0][0])
				? $tmp[0][0]+1
				: $tmp2[0][0]+1;
		$res=faz_query('INSERT INTO '.$tab.' SET '.$campo[$FrmAno]->id.'='.$id);
		if ($tab_base_comum)
			$res=faz_query('INSERT INTO '.$tab_base_comum.' SET '.$campo[$tab_base_comum]->id.'='.$id);
	} elseif (substr($tab,4,3)=='exp') {
		//Se existe o id, ou seja, a experiência já foi inserida uma vez, tenho que apagar os dados das tabelas experiencia e seus anexos:
		faz_query("DELETE FROM experiencia WHERE ex_id=".$id);
		faz_query("DELETE FROM experiencia_autor WHERE exa_id_experiencia=".$id);
		faz_query("DELETE FROM experiencia_relator WHERE exr_id_experiencia=".$id);
		faz_query("DELETE FROM experiencia_arquivo WHERE ea_id_experiencia=".$id);
		faz_query("DELETE FROM rel_area_experiencia WHERE rae_id_experiencia=".$id);
		faz_query("DELETE FROM rel_geo_experiencia WHERE rge_id_experiencia=".$id);
	}

	$sql='UPDATE '.$tab.' AS a';
	$where=' WHERE a.'.$campo[$FrmAno]->id.'="'.$id.'"';
	if ($tab_base_comum) {
		$sql.=', '.$tab_base_comum.' AS b';
		$where.=' AND a.'.$campo[$FrmAno]->id.'=b.'.$campo[$tab_base_comum]->id;
	}
	$sql.=' SET ';
	foreach ($mapa as $m) {
		// Este aqui é um artifício para colocar a data de criação do registro quando é um novo formulário. Lá no mapa_campos eu coloquei o valor padrão {agora} e o tipo do formulário como hidden:
		if ($_POST[$m->campo]=='{agora}') {
			$_POST[$m->campo]='CURDATE()';
			$m->tipo_mysql='int';
		}

		// Aqui é a modificação dos POSTs para contemplar dia, mês e ano...
		if ($m->tipo_form=='data')
			$_POST[$m->campo]=$_POST['p_'.$m->campo.'_year'].'-'
				.$_POST['p_'.$m->campo.'_month'].'-'
				.$_POST['p_'.$m->campo.'_day'];

		// Aqui é para o caso do tipo do campo ser "float", para evitarmos o problema do divisor de casas decimais:
		if ($m->tipo_mysql=='float')
			$_POST[$m->campo]=str_replace(",",".",$_POST[$m->campo]);

		// E aqui é para modificar o POST inserindo o ID da ficha para o caso de inserção do arquivo, se o tipo de campo é de upload de arquivo, e foi realmente feito um upload:
		if ($m->tipo_form=='file' && !$cmd[$m->campo]->link)
			for ($i=0;$i<($m->repeticoes)+1;$i++)
				if ($_POST[$m->campo][$i] && ($id || $_POST['id'])) {
					$nomearq_id = ($id) ? $id : $_POST['id'];
					$nomearq = str_replace('{id}',$nomearq_id,$_POST[$m->campo][$i]);
					if (file_exists($dir['upload'].$_POST[$m->campo][$i])) {
						rename($dir['upload'].$_POST[$m->campo][$i], $dir['upload'].$nomearq );
						$_POST[$m->campo][$i]=$nomearq;
					} else {
						// TODO: Eliminar a referência ao arquivo da base de dados!
					}
				} elseif ($_POST[$m->campo][$i])
					$arqs_corrigir_nome[$m->campo][$i]=$_POST[$m->campo][$i];

		$aspas = (in_array($m->tipo_mysql, $tipos_txt_mysql)) ? "'" : "";
		if ($m->tipo_form=='checkbox' || $m->repeticoes) {
			$n=1;
			$i=0;
			unset($sels);
			while (isset($_POST[$m->campo.'_outro'.$i])) {
				if (trim($_POST[$m->campo.'_outro'.$i])) {
					$otmp=trim($_POST[$m->campo.'_outro'.$i]);
					$campo_nome = ($cmd[$m->campo]->campo_nome) ? $cmd[$m->campo]->campo_nome : 'nome';
					$campo_id = ($cmd[$m->campo]->campo_id) ? $cmd[$m->campo]->campo_id : 'id';
					// Antes de adicionar um novo "outro", verifico se esta categoria já existe:
					$sqltmp="SELECT ".$campo_id.', '.$campo_nome." FROM ".$cmd[$m->campo]->apoio." WHERE ".$campo_nome." LIKE '".$otmp."' LIMIT 1;";
					$tmp=faz_query($sqltmp, '', 'array');
					if ($tmp[$campo_id][0]) {
						// Se o outro já existe, preciso antes verificar se a pessoa não colocou em mais de um "outro". Se não, eu crio mais um item no array sels[]
						if (!in_array($tmp[$campo_id][0],$sels))
							$sels[] = $tmp[$campo_id][0];
					} else {
						// E se não existe o item adicionado na tabela de apoio, adiciono-a na tabela de apoio
						// e no array $sels
						$sqltmp='INSERT INTO '.$cmd[$m->campo]->apoio." SET ".$campo_nome."='".$db->real_escape_string($otmp)."';";
						$sels[]=faz_query($sqltmp,'','id');
					}
				}
				$i++;
			} // fim do loop nos "outro_$i"

			/* Bem, estes foram os "outros" inseridos. Agora tenho que colocar os inseridos simplesmente
				ticados. A classe 'forms' me dá tudo num Array, uma beleza!! */
			if (is_array($_POST[$m->campo])) {
				foreach ($_POST[$m->campo] as $q)
					if (strval($q)<>'-9999')
						$sels[] = $q;
				} elseif ($_POST[$m->campo] && $_POST[$m->campo]<>'-9999')
					$sels[] = $_POST[$m->campo];

			// E aqui finalmente é a conjunção dos dados para colocar na tabela: é o conjunto de dados num só
			// campo, dividido por '|':
			$sql .= (is_array($sels))
				? $m->campo.'="'.$db->real_escape_string(implode('|',$sels)).'", '
				: $m->campo.'=null, ';
		} elseif ($m->tipo_form!='ignorar') {
			// Se não tem repetições:
			if (trim($_POST[$m->campo.'_outro'])) {
				$campo_nome = ($cmd[$m->campo]->campo_nome) ? $cmd[$m->campo]->campo_nome : 'nome';
				$campo_id = ($cmd[$m->campo]->campo_id) ? $cmd[$m->campo]->campo_id : 'id';

				//Aqui eu checo se já existe um igual:
				$otmp=trim($_POST[$m->campo.'_outro']);
				$sqltmp="SELECT ".$campo_id.', '.$campo_nome." FROM ".$cmd[$m->campo]->apoio." WHERE ".$campo_nome." LIKE '".$otmp."' LIMIT 1;";
				$tmp=faz_query($sqltmp, '', 'array');
				if ($tmp[$campo_id][0]) $idtmp=$tmp[$campo_id][0];
					else {
						$sqltmp='INSERT INTO '.$cmd[$m->campo]->apoio." SET ".$campo_nome."='".$db->real_escape_string($otmp)."';";
						$idtmp=faz_query($sqltmp, '', 'id');
					}
				$sql.=$m->campo.'='.$idtmp.', ';
			} elseif ( !($m->campo==$campo[$FrmAno]->id || ($tab_base_comum && $m->campo==$campo[$tab_base_comum]->id)) )
				$sql.= ($_POST[$m->campo] && $_POST[$m->campo]<>'-9999')
					? $m->campo.'='.$aspas.$db->real_escape_string($_POST[$m->campo]).$aspas.', '
					: $m->campo.'=null, ';
		}
	} // fim do loop nos campos

	//
	//
	// INSERÇÃO / ATUALIZAÇÃO DOS DADOS!
	//
	//
	$sql=substr($sql, 0, strlen($sql)-2).$where;
	//echo $sql;exit;
	$res=faz_query($sql);

	// Aqui é um malabarismo para eu alimentar as colunas ref_lat e ref_lng a partir da cidade OU da latitude/longitude diretamente colocada pelo usuário ao alimentar a ficha:
	if ($campo[$tab]->lat && $_POST[$campo[$tab]->localizacao]) {
		$sql='UPDATE '.$tab.' AS a, _'.substr($_POST[$campo[$tab]->localizacao],0,2).' as cidades';
		$where=' WHERE a.'.$campo[$tab]->id.'="'.$id.'" AND '.$campo[$tab]->localizacao.'=cidades.id';
		if ($tab_base_comum) {
			$sql.=', '.$tab_base_comum.' AS b';
			$where.=' AND a.'.$campo[$tab]->id.'=b.'.$campo[$tab_base_comum]->id;
		}
		$txtlat = ($_POST[$campo[$tab]->lat])
			? "'".$_POST[$campo[$tab]->lat]."'"
			: "cidades.lat";
		$txtlng = ($_POST[$campo[$tab]->lng])
			? "'".$_POST[$campo[$tab]->lng]."'"
			: "cidades.lng";
		$sql .= " SET ref_lat=".$txtlat.", ref_lng=".$txtlng;
		$sql.=$where;
		//echo $sql;exit;
		faz_query($sql);
	}
	// Um dia teremos uma total integração entre o agroecologiaemrede e o mapeo. Por enquanto, tenho que atualizar a tabela experiencia (e seus adendos) na marra, após ter feito a inserção do dado nas tabelas frm_exp... Além disso, aqui faço a parte de gestão das mensagens para o usuário ou para o administrador:
	if (substr($tab,4,3) == 'exp') {
		if ($tab=='frm_exp_geral') {
			$txt1='b.ex_chamada';
			$txt2='b.ano_publicacao';
		} else {
			$txt1="'' as ex_chamada";
			$txt2="'' as ano_publicacao";
		}
		// Dependendo se foi o admin ou o usuário, é colocada a data na "pendência" ou na "resposta", respectivamente
		$header = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/plain; charset=UTF-8' . "\r\n";
		if ($s->u->us_admin) {
			if (trim($_POST['txt_pendente']) || $_POST['ex_liberacao']!='N') {
				$dt_pendente = (trim($_POST['txt_pendente']))
					? "'$current_date' as dt_pendente"
					: "dt_pendente";
				$dt_resposta="dt_resposta";
				// Colocar aqui a construção do e-mail para o usuário:
				$sql = 'SELECT us_email, us_nome FROM usuario WHERE us_login LIKE "'.$_POST[$campo[$tab]->digitadora].'"';
				$dados_recipiente = faz_query($sql,'','object');
				switch ($_POST['ex_liberacao']) {
					case 'S':
						$stmp=array(
							$txt['mysql']['EXP_CONFIRMACAO_S_TEXTO']->tex_texto,
							$txt['mysql']['EXP_ASSINATURA']->tex_texto,
							$txt['mysql']['EXP_CONFIRMACAO_S_SUBJECT']->tex_texto
						);
					break;
					case 'I':
						$stmp=array(
							$txt['mysql']['EXP_CONFIRMACAO_I_TEXTO']->tex_texto,
							$txt['mysql']['EXP_ASSINATURA']->tex_texto,
							$txt['mysql']['EXP_CONFIRMACAO_I_SUBJECT']->tex_texto
						);
					break;
					case 'P':
						$stmp=array(
							$txt['mysql']['EXP_PENDENTE_TEXTO']->tex_texto,
							$txt['mysql']['EXP_ASSINATURA']->tex_texto,
							$txt['mysql']['EXP_PENDENTE_SUBJECT']->tex_texto
						);
					break;
					case 'N':
						$stmp=array(
							$txt['mysql']['EXP_PENDENTE_TEXTO']->tex_texto,
							$txt['mysql']['EXP_ASSINATURA']->tex_texto,
							$txt['mysql']['EXP_PENDENTE_SUBJECT']->tex_texto
						);
						$sql = 'UPDATE frm_exp_base_comum SET ex_liberacao="P" WHERE '.$campo['frm_exp_base_comum']->id.'="'.$id.'"';
						faz_query($sql);
					break;
					case 'R':
						$stmp=array(
							$txt['mysql']['EXP_REJEICAO_TEXTO']->tex_texto,
							$txt['mysql']['EXP_ASSINATURA']->tex_texto,
							$txt['mysql']['EXP_REJEICAO_SUBJECT']->tex_texto
						);
					break;
				}
				$corpo = sprintf($txt['ola']." ".$dados_recipiente[0]->us_nome.",\n\n%s\n\n==============================\n".$txt['nome_experiencia'].": %s\n\n".$txt['mensagem'].": %s\n==============================\n\n%s",
					$stmp[0],
					$_POST[$campo[$tab]->nome],
					$_POST['txt_pendente'],
					$stmp[1]
					);
				$assunto = $stmp[2].': "'.$_POST[$campo[$tab]->nome].'"';
				$email=$dados_recipiente[0]->us_email;
				$mail = mail($email,'=?UTF-8?B?'.base64_encode($assunto).'?=',$corpo,$header);
				if (!$mail)
					echo 'Erro enviando e-mail. O e-mail era: '.chr(10).$email.chr(10).$assunto.chr(10).$corpo;
			} else {
				$dt_pendente="dt_pendente";
				$dt_resposta="dt_resposta";
			}
		} else {
			if (trim($_POST['txt_resposta']) && $_POST['ex_liberacao']!='N') {
				$dt_pendente="dt_pendente";
				$dt_resposta="'$current_date' as dt_resposta";
				// Atualizo o campo de liberação para 'E', ou seja, espera de resposta do admin:
				/*$sql = 'UPDATE frm_exp_base_comum SET ex_liberacao="E" WHERE '.$campo['frm_exp_base_comum']->id.'="'.$id.'"';
				faz_query($sql);*/
				// Colocar aqui a construção do e-mail para os admins (quais??)
				$stmp=array(
					$txt['mysql']['EXP_RESPOSTA_USUARIO_TEXTO']->tex_texto,
					$txt['mysql']['EXP_ASSINATURA']->tex_texto,
					$txt['mysql']['EXP_RESPOSTA_USUARIO_SUBJECT']->tex_texto
				);
				$corpo = sprintf($txt['ola']." Administrador/a,\n\n%s\n\n\n==============================\n".$txt['nome_experiencia'].": %s\n\n".$txt['mensagem'].": %s\n==============================\n\n%s",
					$stmp[0],
					$_POST[$campo[$tab]->nome],
					$_POST['txt_resposta'],
					$stmp[1]
				);
				$assunto = $stmp[2].': "'.$_POST[$campo[$tab]->nome].'"';
				foreach ($admins as $a)
					$mail = mail($a->us_email,'=?UTF-8?B?'.base64_encode($assunto).'?=',$corpo,$header);
				if (!$mail)
					echo 'Erro enviando e-mail. O e-mail era: '.chr(10).$a->us_email.chr(10).$assunto.chr(10).$corpo;
			} else {
				$sql = 'UPDATE frm_exp_base_comum SET txt_resposta="" WHERE '.$campo['frm_exp_base_comum']->id.'="'.$id.'"';
				faz_query($sql);
				$dt_pendente="dt_pendente";
				$dt_resposta="dt_resposta";
			}
		}
		$sql = "INSERT INTO experiencia SELECT a.ex_id, a.ex_descricao, a.ex_resumo, a.ex_liberacao, a.cod_usuario, ".$txt1.", a.dt_criacao, a.dt_atualizacao, txt_pendente, ".$dt_pendente.", txt_resposta, ".$dt_resposta.", '' as txt_comentario, a.cod_usuario, ".$txt2.", a.ex_lat, a.ex_lng, '".$tab."' as tipo
		FROM frm_exp_base_comum as a, ".$tab." as b
		WHERE a.ex_id=b.".$campo[$tab]->id." AND a.ex_id=".$id;
		faz_query($sql);
		// As instituições:
		/*$res = faz_query("SELECT ex_id, ex_instituicao_referencia FROM frm_exp_base_comum WHERE CONCAT(ex_id,'-',ex_instituicao_referencia) NOT IN (SELECT CONCAT(exa_id_experiencia,'-',exa_id_inst) FROM experiencia_autor)",'','object');
		foreach ($res as $r) {
			faz_query('INSERT INTO experiencia_autor VALUES ("'.$r->ex_id.'", null,"'.$r->ex_instituicao_referencia.'")');
			echo $r->ex_id.'<br>';
		}*/
		faz_query('INSERT INTO experiencia_autor VALUES ("'.$id.'", null,"'.$_POST['ex_instituicao_referencia'].'")');
		$sql = "SELECT ex_instituicoes FROM frm_exp_base_comum WHERE ex_id=".$id;
		$res=faz_query($sql,'','row');
		if (trim($res[0][0])) {
			$tmp = explode('|',trim($res[0][0]));
			foreach ($tmp as $t)
				faz_query('INSERT INTO experiencia_autor VALUES ('.$id.', null, '.$t.')');
		}

		// O relator:
		$sql = "SELECT cod_usuario FROM frm_exp_base_comum WHERE ex_id=".$id;
		$res=faz_query($sql,'','row');
		if ($t=trim($res[0][0]))
			faz_query('INSERT INTO experiencia_relator VALUES ('.$id.', "'.$t.'", null)');

		// Os arquivos:
		$sql = "SELECT ex_anexos FROM frm_exp_base_comum WHERE ex_id=".$id;
		$res=faz_query($sql,'','row');
		if (trim($res[0][0])) {
			$tmp = explode('|',trim($res[0][0]));
			foreach ($tmp as $t)
				faz_query('INSERT INTO experiencia_arquivo VALUES ('.$id.', "'.$t.'")');
		}

		// As áreas temáticas:
		$sql = "SELECT ex_areas_tematicas FROM frm_exp_base_comum WHERE ex_id=".$id;
		$res=faz_query($sql,'','row');
		if (trim($res[0][0])) {
			$ats = explode('|',trim($res[0][0]));
			foreach ($ats as $t)
				faz_query('INSERT INTO rel_area_experiencia VALUES ('.$t.', '.$id.')');
		} else $ats=array();

		// Atualiza-se as áreas temáticas lá nas instituições que foram citadas:
		$sql = "SELECT CONCAT(ex_instituicao_referencia,IF(ex_instituicoes IS NOT NULL,CONCAT('|',ex_instituicoes),'')) as insts FROM frm_exp_base_comum WHERE ex_id=".$id;
		$res=faz_query($sql,'','row');
		$insts = explode('|',trim($res[0][0]));
		$sql = "SELECT in_id, in_areas_tematicas FROM frm_instituicao WHERE in_id IN ( ".implode(', ',$insts)." )";
		$insts=faz_query($sql,'','object');
		foreach ($insts as $inst) {
			$ats_inst = ($inst->in_areas_tematicas)
				? array_merge($ats,explode('|',$inst->in_areas_tematicas))
				: $ats;
			$ats_inst = array_unique($ats_inst);
			$sql = "UPDATE frm_instituicao SET in_areas_tematicas='".implode('|',$ats_inst)."' WHERE in_id='".$inst->in_id."'";
			$res = faz_query($sql);
		}

		// Os municípios (de referência e os de abrangência):
		$sql = "SELECT ex_cidade_ref, ex_abrangencia FROM frm_exp_base_comum WHERE ex_id=".$id;
		$res=faz_query($sql,'','row');
		if (trim($res[0][0]))
			faz_query('INSERT INTO rel_geo_experiencia VALUES ("'.trim($res[0][0]).'", '.$id.')');
		if (trim($res[0][1])) {
			$tmp = explode('|',trim($res[0][0]));
			foreach ($tmp as $t)
				faz_query('INSERT INTO rel_geo_experiencia VALUES ("'.$t.'", '.$id.')');
		}
	}



	/* Aqui eu adiciono uma nova página se ainda não estou na página final, ou vou para
		a primeira página se já acabei o formulário completo */
	$pgatual=$pg;
	if ($pg==$nmaxpgs) {
		// Se a pessoa quer ir pra frente, vai cair num novo formulário em branco.
		//   Senão, voltará à página anterior, mesmo tendo encerrado a alimentação do formulário:
		if ($_POST['enviar'] || $_POST['enviar_x']) {
			$aviso=($novofrm)
				? str_replace('{id}', $id, $txt['sucesso_adicao'])
				: str_replace('{id}', $id, $txt['sucesso_atualizacao']);
			$novofrm=true;
			$pg=1;
			unset($id);
		} else
			$pg--; //volto à página anterior e não mostro aviso algum
	} else {
		//Se não estou na última página, posso seguir para a próxima e, se
		//  possível (pg>1), voltar para a anterior:
		if ($_POST['enviar'] || $_POST['enviar_x']) {
			$pg++;
			if ($novofrm) $id=$_POST['id'];
		} else {
			$pg--;
			unset($novofrm);
		}
	}

	unset($_POST);
	$_POST['processe']=1;
	// Se mudei de página, mudo de tabela:
	/*if ($pg<>$pgatual) {
		$tab=$FrmAno.$pg;
		$mapa=gera_mapa_campos($ano,$tab,$tab_base_comum,$cmd);
	}*/
} // fim do else do if tem erros.
?>
