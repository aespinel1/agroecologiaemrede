<?php
class form_dtygel_class extends form_class {

	function rodaMapaCampos($mapa, $cmd, $dados=array(), $s=false) {
		global $paises, $dir, $campo, $bd, $padrao_campo_nome, $padrao_campo_id, $padrao_data_inicio, $padrao_data_fim, $padrao_nmax_opcoes, $padrao_size_select_multiplo, $padrao_outro_size, $chave, $ano_lim;

		$db = conecta($bd);

		/* Aqui vou entrando os campos, 1 a 1....
		*/
		$isAdmin = isset($s) && isset($s->u) && isset($s->u->us_admin) && $s->u->us_admin;
		foreach ($mapa as $m) {
			$cmdo=isset($cmd[$m->campo])
				? $cmd[$m->campo]
				: new stdClass();
		// Este if inicial é o que define a exibição ou não de campos que estão definidos como exclusivamente editáveis pelo admin ou pelo usuário (comandos admin:1|mostrar:1 ou 0; e comando: naoadmin:1|mostrar:0 ou 1). Note que ao fim de todo o enorme loop do $m, há um else para este if, que mostra o campo como hidden!
		if 	(
			((!isset($cmdo->admin) || !$cmdo->admin) && (!isset($cmdo->naoadmin) || !$cmdo->naoadmin)) ||
			(isset($cmdo->mostra) && $cmdo->mostra) ||
			((isset($cmdo->admin) && $cmdo->admin) && $isAdmin) ||
			((isset($cmdo->naoadmin) && $cmdo->naoadmin) && !$isAdmin)
		) {
			unset($opsform, $opcoes, $regras);
			// Como sempre uso o cmd[$m->campo] vamos economizar um pouco:
			$cmdo->bloqueado = (
					(isset($cmdo->bloqueado) && $cmdo->bloqueado) ||
					((isset($cmdo->admin) && $cmdo->admin) && !$isAdmin) ||
					((isset($cmdo->naoadmin) && $cmdo->naoadmin) && $isAdmin)
				);
			if (!isset($cmdo->campo_nome) || !$cmdo->campo_nome) $cmdo->campo_nome=$padrao_campo_nome;
			if (!isset($cmdo->campo_id) || !$cmdo->campo_id) $cmdo->campo_id=$padrao_campo_id;
			if (!isset($cmdo->campo_id_div_rel) || !$cmdo->campo_id_div_rel) $cmdo->campo_id_div_rel=isset($padrao_campo_id_div_rel) ? $padrao_campo_id_div_rel : '';
			if (isset($cmdo->bd) && $cmdo->bd && isset($cmdo->apoio) && $cmdo->apoio) $cmdo->apoio=$bd[$cmdo->bd].'.'.$cmdo->apoio;
			// Aqui resgato as regras para cada campo:
			if (isset($m->regras) && $m->regras) $regras=explode('|',$m->regras);
				else $regras=Array(-1);

			//Aqui eu recupero condições de valores-padrão definidos na tabela mapa-campos (campo $cmdo->padrao).
			$tmp = (isset($cmdo->padrao)) ? substr($cmdo->padrao,8) : '';
			if (isset($cmdo->padrao) && $cmdo->padrao && !(isset($dados[$m->campo]) && $dados[$m->campo]))
				$dados[$m->campo] = (substr($cmdo->padrao,0,7)!='var_get')
					? ($cmdo->padrao=='{ano}')
						? date('Y')
						: $cmdo->padrao
					: $s->u->$tmp;

			// O tipo de input depende do tipo de campo (tipo_form)
			if ($m->tipo_form!='ignorar' && !(isset($cmdo->ignorar) && $cmdo->ignorar))
			switch ($m->tipo_form) {
				case 'data':
					if (!class_exists('form_date_class')) require($dir['apoio']."forms/form_date.php");
					$opsform[0]["TYPE"]="custom";
					$opsform[0]['CLASS']='form-control';
					$opsform[0]["ID"]=$m->campo;
					$opsform[0]["LABEL"]=$m->titulo;
					if ($m->ajuda)
						$opsform[0]['LabelAJUDA']=$m->ajuda;
					$opsform[0]["CustomClass"]="form_date_class";
					$valor = ($cmdo->padrao) ? $cmdo->padrao : $data_padrao;
					if ($dados[$m->campo]<>'0000-00-00') {
						$opsform[0]["VALUE"]=($dados[$m->campo]) ? $dados[$m->campo] : $valor;
					}
					$opsform[0]["Format"]="{day}/{month}/{year}";
					$opsform[0]["Months"]=array(
						"01"=>"janeiro",
						"02"=>"fevereiro",
						"03"=>"março",
						"04"=>"abril",
						"05"=>"maio",
						"06"=>"junho",
						"07"=>"julho",
						"08"=>"agosto",
						"09"=>"setembro",
						"10"=>"outubro",
						"11"=>"novembro",
						"12"=>"dezembro"
					);
					$opsform[0]["Optional"]=1;
					$opsform[0]["ValidationStartDate"] = ($cmdo->data_inicio)
						? $cmdo->data_inicio
						: $padrao_data_inicio;
					$opsform[0]["ValidationStartDateErrorMessage"]="Você especificou uma data anterior à data mínima.";
					$opsform[0]["ValidationEndDate"] = ($cmdo->data_fim)
						? $cmdo->data_fim
						: $padrao_data_fim;
					$opsform[0]["ValidationEndDateErrorMessage"]="Você especificou uma data posterior à data limite.";
					if (isset($cmdo->bloqueado) && $cmdo->bloqueado)
						$opsform[0]['Accessible']=0;
				break;
				case 'checkbox':
					if ($cmdo->nmax_opcoes) $max=$cmdo->nmax_opcoes;
						else $max=$padrao_nmax_opcoes;
					if (!(isset($dados[$m->campo]) && $dados[$m->campo]))
						$dados[$m->campo]=array();
					//Se há mais opções que o $max, então teremos forçosamente um elemento SELECT, e não CHECKBOX:
					$num_opcoes=faz_query('select '.$cmdo->campo_id.' from '.$cmdo->apoio, $db, 'num_rows');
					$select=($num_opcoes>$max || !$num_opcoes);

					// $opcoes é o array que tem as opções possíveis do checkbox ou select. É do tipo valor=>texto, a partir da tabela auxiliar.
					$wtmp = isset($cmdo->where) ? $cmdo->where : '';
					$otmp = isset($cmdo->order) ? $cmdo->order : '';
					$opcoes=faz_select($cmdo->apoio,$cmdo->campo_id,'',$cmdo->campo_nome, $otmp, $wtmp, false, false);
					if (!$opcoes) $opcoes=array();

					// Aqui eu confiro e elimino os itens dos $dados que não estão nas opções:
					$ttt=array();
					if (is_array($dados[$m->campo]))
						foreach ($dados[$m->campo] as $dado)
							if (in_array($dado,array_keys($opcoes)))
								$ttt[]=$dado;
					$dados[$m->campo]=$ttt;
					// Se é select, é apenas um elemento do tipo "múltiplo", com as várias opções:
					if ($select) {
						$opsform[0]['NAME']=$m->campo;
						$opsform[0]['ID']=$m->campo;
						if (isset($cmdo->size) && $cmdo->size) $opsform[0]['SIZE']=intval($cmdo->size);
							else $opsform[0]['SIZE']=$padrao_size_select_multiplo;
						$opsform[0]['LABEL']=$m->titulo;
						if ($m->ajuda)
							$opsform[0]['LabelAJUDA']=$m->ajuda;
						$opsform[0]['TYPE']='select';
						$opsform[0]['MULTIPLE']=1;
						$opsform[0]['CLASS']='form-control selectpicker';
						$opsform[0]['DATALIVESEARCH']='true';
						$opsform[0]['NMAX']=$m->repeticoes;
						$opsform[0]['SELECTED'] = (is_array($dados[$m->campo]))
							? $dados[$m->campo]
							: array();
						$opsform[0]['OPTIONS']=$opcoes;
						// Se é um campo obrigatório, preciso avisar:
						if (in_array('NotEmpty', $regras) && $cmdo->outro) {
							$opsform[0]['ValidateAsSet']=1;
							$opsform[0]['ValidationErrorMessage']=$m->titulo.': '.$this->txt_lingua['erro_NotEmpty'];
						}
						if (isset($cmdo->bloqueado) && $cmdo->bloqueado)
							$opsform[0]['Accessible']=0;
					// fim do if $select
					// Se é checkbox, temos que fazer um AddInput para cada opção
					} else {
						$id_valores=array_keys($opcoes);
						// Aqui eu resgato os ids dos divs relacionados a estes checkboxes, caso a opção div_rel esteja ativa:
						if (isset($cmdo->div_rel) && $cmdo->div_rel && !(isset($cmdo->outro) && $cmdo->outro))
							$id_div_rel = resgata_div_rel($cmdo->apoio,$cmdo->campo_id,$cmdo->campo_id_div_rel);
						//echo '<br>'.$m->campo.': ';print_r($dados[$m->campo]);echo '<br>';print_r($id_valores);
						for ($i=0;$i<$num_opcoes;$i++) {
							$idValoresTmp = (isset($id_valores[$i]))
								? $id_valores[$i]
								: '';
							$opsform[$i]['NAME']=$m->campo;
							$opsform[$i]['ID']=$m->campo.'_'.$idValoresTmp;
							$opsform[$i]['VALUE']=$idValoresTmp;
							$opsform[$i]['LABEL']=isset($opcoes[$idValoresTmp]) ? $opcoes[$idValoresTmp] : '';
							$opsform[$i]['TYPE']='checkbox';
							$opsform[$i]['MULTIPLE']=1;
							if (in_array($idValoresTmp, $dados[$m->campo]))
								$opsform[$i]['CHECKED']=1;

							// Aqui eu insiro o javascript se a opção div_rel estiver ativa, que é a que faz com que a escolha da pessoa abra/feche um div especificado pela tabela auxiliar, no campo cujo nome é definido por campo_div_rel:
							if ((isset($cmdo->div_rel) && $cmdo->div_rel) && !(isset($cmdo->outro) && $cmdo->outro)) {
								$opsform[$i]['ONCLICK']="alterna_visao('".$m->campo."_".$id_valores[$i]."','sub_bloco_".$id_div_rel[$$idValoresTmp]."')";
								$tmpdivrel = new StdClass();
								$tmpdivrel->origem=$opsform[$i]['ID'];
								$tmpdivrel->alvo='sub_bloco_'.$id_div_rel[$idValoresTmp];
								$this->div_rel[]=$tmpdivrel;
							}

							// Se é um campo obrigatório, preciso avisar, mas só na primeira opção:
							if (in_array('NotEmpty', $regras) && $i==0) {
								$opsform[$i]['ValidateAsSet']=1;
								$opsform[$i]['ValidationErrorMessage']=$m->titulo.': '.$this->txt_lingua['erro_NotEmpty'];
							}
							if (isset($cmdo->bloqueado) && $cmdo->bloqueado)
								$opsform[$i]['Accessible']=0;

						}
					} // fim do else (ou seja, fim da produção do checkbox)
					//print_r($opsform);echo '<br>'.$m->campo.': '; print_r($dados[$m->campo]); echo '<br><br>';
				break;
				case 'radio':
					$max = ($cmdo->nmax_opcoes) ? $cmdo->nmax_opcoes : $padrao_nmax_opcoes;

					// $opcoes é o array que tem as opções possíveis do radio ou select. É do tipo valor=>texto, a partir da tabela auxiliar.
					$opcoes=faz_select($cmdo->apoio,$cmdo->campo_id,'',$cmdo->campo_nome, $cmdo->order, $cmdo->where, false, false);
					if (!$opcoes) $opcoes=array();
					// Aqui eu confiro e elimino os itens dos $dados que não estão nas opções:
					$dados[$m->campo] = (in_array($dados[$m->campo],array_keys($opcoes)))
						? $dados[$m->campo]
						: '';
					//Se há mais opções que o $max, então teremos forçosamente um elemento SELECT, e não RADIO:
					$num_opcoes=count($opcoes);
					$select=($num_opcoes>$max || !$num_opcoes);
					// Se é select, é apenas um elemento, com as várias opções:
					if ($select) {
						$opsform[0]['NAME']=$m->campo;
						$opsform[0]['ID']=$m->campo;
						$opsform[0]['LABEL']=$m->titulo;
						if ($m->ajuda)
							$opsform[0]['LabelAJUDA']=$m->ajuda;
						$opsform[0]['TYPE']='select';
						$opsform[0]['CLASS']='form-control selectpicker';
						$opsform[0]['DATALIVESEARCH']='true';
						if (!in_array('NotEmpty', $regras) || $cmdo->outro)
							$opcoes=array('-9999'=>'(em branco ou outro...)') + $opcoes;
							else $opcoes=array(''=>'') + $opcoes;
						$valornulo = ($opcoes['-9999']) ? '-9999' : '';
						$opsform[0]['VALUE'] = ($dados[$m->campo]) ? $dados[$m->campo] : $valornulo;
						$opsform[0]['OPTIONS']=$opcoes;
						// Tenho que ver as regras:
						if ($regras[0]<>(-1))
							foreach ($regras as $rs) {
								$opsform[0]['ValidateAs'.$rs]=1;
								$opsform[0]['ValidateAs'.$rs.'ErrorMessage']=$m->titulo.': '.$this->txt_lingua['erro_'.$rs];
							}
						if (isset($cmdo->bloqueado) && $cmdo->bloqueado)
							$opsform[0]['Accessible']=0;

					// Se é radio, temos que fazer um AddInput para cada opção
					} else {
						if (isset($cmdo->outro) && $cmdo->outro) {
							$opcoes=$opcoes+array(-9999=>'Outro...');
							$num_opcoes++;
						}
						$id_valores=array_keys($opcoes);
						// Aqui eu resgato os ids dos divs relacionados a estes checkboxes, caso a opção div_rel esteja ativa:
						if (isset($cmdo->div_rel) && $cmdo->div_rel && !(isset($cmdo->outro) && $cmdo->outro))
							$id_div_rel = resgata_div_rel($cmdo->apoio,$cmdo->campo_id,$cmdo->campo_id_div_rel);
						for ($i=0;$i<$num_opcoes;$i++) {
							$opsform[$i]['NAME']=$m->campo;
							$opsform[$i]['ID']=$m->campo.'_'.$id_valores[$i];
							$opsform[$i]['VALUE']=$id_valores[$i];
							$opsform[$i]['LABEL']=$opcoes[$id_valores[$i]];
							$opsform[$i]['TYPE']='radio';
							if ($dados[$m->campo]==$id_valores[$i]) $opsform[$i]['CHECKED']=1;
							// Aqui eu insiro o javascript se a opção div_rel estiver ativa, que é a que faz com que a escolha da pessoa abra/feche um div especificado pela tabela auxiliar, no campo cujo nome é definido por campo_div_rel:
							if (isset($cmdo->div_rel) && $cmdo->div_rel && !(isset($cmdo->outro) && $cmdo->outro)) {
								//$opsform[$i]['ONCLICK']="alterna_visao('".$m->campo."_".$id_valores[$i]."','".$id_div_rel[$id_valores[$i]]."')";
								$opsform[$i]['ONCLICK']="atualiza_visao()";
								$tmpdivrel = new StdClass();
								$tmpdivrel->origem=$opsform[$i]['ID'];
								$tmpdivrel->alvo='sub_bloco_'.$id_div_rel[$id_valores[$i]];
								$this->div_rel[]=$tmpdivrel;
							}

							// Se é um campo obrigatório, preciso avisar, mas só no primeiro:
							if (in_array('NotEmpty', $regras) && $i==0) {
								$opsform[$i]['ValidateAsSet']=1;
								$opsform[$i]['ValidationErrorMessage']=$m->titulo.': '.$this->txt_lingua['erro_NotEmpty'];
							}
							if (isset($cmdo->bloqueado) && $cmdo->bloqueado)
								$opsform[$i]['Accessible']=0;

						}
					} // fim do else (ou seja, fim da produção do radio)
					//print_r($opsform);echo '<br>'.$m->campo.': '.$dados[$m->campo].'<br><br>';exit;
				break;
				case 'select_hierarquia':
					$nmaxsels=$cmdo->niveis-1;
					unset($padrao, $cmdsel);
					if ($cmdo->bd) {
						$db->select_db($bd[$cmdo->bd]);
					}

					//Primeiro tenho que extrair os comandos "tab", "where", "campo_id", "campo_txt", "titulo":
					for ($i=0;$i<=$nmaxsels;$i++) {
						$tmp='tab'.$i;			$cmdsel[$i]->tab=$cmdo->$tmp;
						$tmp='where'.$i;		$cmdsel[$i]->where=$cmdo->$tmp;
						$tmp='campo_id'.$i;		$cmdsel[$i]->campo_id = ($cmdo->$tmp) ? $cmdo->$tmp : $padrao_campo_id;
						$tmp='campo_txt'.$i; 	$cmdsel[$i]->campo_txt = ($cmdo->$tmp) ? $cmdo->$tmp : $padrao_campo_nome;
						$tmp='titulo'.$i; 		$cmdsel[$i]->titulo = ($cmdo->$tmp) ? $cmdo->$tmp : $m->titulo;
						$tmp='padrao'.$i; 		$cmdsel[$i]->padrao=$cmdo->$tmp;
					}

					//Tenho que obter os valores padrão de cada select em cascata, a partir dos "where" fornecidos pelo comando cmds_sel. Se o $id não existe, eu coloco como padrão a capital (se o $uf estiver definido!):
					// o padrão do fim da cascata é o próprio valor do campo, ou a regra definida no cmds_sel:
					if ($dados[$m->campo]) $padrao[$nmaxsels]=$dados[$m->campo];
						elseif ($cmdsel[0]->tab=='estados' && $uf && $uf<>99) $padrao[0]=$uf;
					if ($padrao[$nmaxsels]) {
					// os restantes são resgatados a partir dele:
						for ($i=$nmaxsels-1;$i>-1;$i--) {
							$a=chr(97+$i);
							$b=chr(97+$i+1);
							$campo_a=$a.'.'.$cmdsel[$i]->campo_id;
							$campo_b=$b.'.'.$cmdsel[$i+1]->campo_id;
							// Este comando sql é complexo. Aviso apenas que estou usando a seguinte regra de nomenclatura: o primeiro select é "AS a" (chr(97)), o segundo é "AS b", e assim por diante, pra evitar confusão caso os campos nas duas tabelas sejam os mesmos! Basicamente, com este comando estou resgatando a relação entre o select $i e o select $i+1, para ir resgatando os padrões a partir do $i=$cmdo->niveis-1], que é a ponta final da cascata:
							$sql='select '.$campo_a.' from '.$cmdsel[$i]->tab.' AS '.$a.', '.$cmdsel[$i+1]->tab.' AS '.$b.' where '.$campo_b.'='.$padrao[($i+1)].' AND ';

							//Aqui a relação entre as tabelas é feita diretamente por default, mas pode ser alterada caso tenha sido definido um WHERE no $cmdsel[$i+1]->where:
							$sql .= ($cmdsel[$i+1]->where)
								? $cmdsel[$i+1]->where
								: $campo_b.'='.$campo_a;

							$tmp=faz_query($sql, $db, 'row');
							$padrao[$i]=$tmp[0][0];
						} // Fim do loop para resgate dos padrões
					} // Fim do if (existe $padrao)

					//Agra é a hora de produzir os selects em cascata
					//Começamos pelos primeiros níveis até o penúltimo. Tratam-se de selects normais com a chamada javascript:
					for ($i=0;$i<$nmaxsels;$i++) {
						//Aqui eu produzo o SQL esdrúxulo para ser usado no Query. Atenção que o "{valor}" será substituído pelo valor selecionado no select!
						$onde = '';
						$a=chr(97+$i);
						$b=chr(97+$i+1);
						$campo_a=$a.'.'.$cmdsel[$i]->campo_id;
						$campo_b=$b.'.'.$cmdsel[$i+1]->campo_id;
						$txt_a=$a.'.'.$cmdsel[$i]->campo_txt;
						$txt_b=$b.'.'.$cmdsel[$i+1]->campo_txt;
						$sql = ($m->repeticoes)
							? 'SELECT '.$campo_b.', CONCAT('.$txt_a.', "/", '.$txt_b.') as "'.$txt_b.'" FROM '.$cmdsel[$i]->tab.' AS '.$a.', '.$cmdsel[$i+1]->tab.' AS '.$b.' WHERE '
							: 'SELECT '.$campo_b.', '.$txt_b.' FROM '.$cmdsel[$i]->tab.' AS '.$a.', '.$cmdsel[$i+1]->tab.' AS '.$b.' WHERE ';
						//A relação entre as tabelas é feita diretamente por default, mas pode ser alterada caso tenha sido definido um WHERE no $cmdo->where'.($i+1)]:
						$onde = ($cmdsel[$i+1]->where)
							? $cmdsel[$i+1]->where
							: $campo_a.'='.$campo_b;
						$onde.=' AND '.$campo_a;
						$sql.=$onde.'={valor}';
						// Ufa! Espero nunca mais ter que mexer nessa query acima!! Acabei que ter que mexer: ||||||||

						$opsform[$i]['TYPE']='select';
						$opsform[$i]['CLASS']='form-control selectpicker';
						$opsform[$i]['NAME']=$m->campo.'_'.$i;
						$opsform[$i]['ID']=$m->campo.'_'.$i;
						$opsform[$i]['LABEL']=$cmdsel[$i]->titulo;
						if ($padrao[$i] || $i==0) {
							$opsform[$i]['VALUE']= ($padrao[$i]) ? $padrao[$i] : '-9999';
							$opsform[$i]['OPTIONS']=array('-9999'=>'---') + faz_select($cmdsel[$i]->tab, $cmdsel[$i]->campo_id, '', $cmdsel[$i]->campo_txt, $cmdo->order, $cmdsel[$i]->where, false, false);
						} else {
							$opsform[$i]['OPTIONS']=array('0'=>'---');
							$opsform[$i]['VALUE']='0';
						}
						$id_prox_select = ($i==$nmaxsels-1) ? $m->campo : $m->campo.'_'.($i+1);
						$primeiro = ($m->repeticoes) ? '' : '&primeiro=--'.$this->txt_lingua['frm_selecione'].'--';
						$agregacampos = ($m->repeticoes) ? '&agregacampos=1' : '';
						$opsform[$i]['ONCHANGE']="javascript:Ajax.Request('method=getXML&onde=".urlencode(criptografa($sql, $chave))."&valor='+this.value+'".$primeiro.$agregacampos."&tabela=".$cmdsel[$i+1]->tab."&campo_id=".$cmdsel[$i+1]->campo_id."&campo_nome=".$cmdsel[$i+1]->campo_txt."&padrao=".urlencode(criptografa($cmdsel[$i+1]->padrao,$chave))."', Ajax.Response, '".$id_prox_select."',false,'carregando_".$m->campo."');";

					} // fim do loop para os primeiros selects antes do final

					// E agora é o select final, que é afetado pelos anteriores mas não afeta mais ninguém, então não tem o javascript...
					$opsform[$nmaxsels]['TYPE']='select';
					$opsform[$nmaxsels]['CLASS']='form-control selectpicker';
					$opsform[$nmaxsels]['NAME']=$m->campo;
					$opsform[$nmaxsels]['ID']=$m->campo;
					if ($m->repeticoes) {
						$opsform[$nmaxsels]['MULTIPLE']=1;
						$opsform[$nmaxsels]['NMAX']=$m->repeticoes;
						$opsform[$nmaxsels]['SELECTED'] = ($dados[$m->campo])
							? $dados[$m->campo]
							: array();
						if ($dados[$m->campo][0]) {
							// Aqui eu monto os textos selected pra aparecer no select múltiplo da direita:
							$onde = ($cmdsel[$nmaxsels]->where)
								? $cmdsel[$nmaxsels]->where
								: $campo_a.'='.$campo_b;
							$sql = 'SELECT '.$campo_b.' as "'.$campo_b.'", CONCAT('.$txt_a.', "/", '.$txt_b.') as "'.$txt_b.'" FROM '.$cmdsel[$nmaxsels-1]->tab.' AS '.$a.', '.$cmdsel[$nmaxsels]->tab.' AS '.$b.' WHERE '.$onde.' AND '.$campo_b.' IN (';
							foreach ($dados[$m->campo] as $p)
								$sql.=$p.', ';
							$sql=substr($sql,0,-2).')';
							$tmp=faz_query($sql,$db,'object');
							$tmpid=$cmdsel[$nmaxsels]->campo_id;
							$tmptxt=$cmdsel[$nmaxsels]->campo_txt;

							foreach($tmp as $p)
								$opsform[$nmaxsels]['OPTIONS'][$p->$campo_b]=$p->$txt_b;
						} else $opsform[$nmaxsels]['OPTIONS']=array();
						$opsform[$nmaxsels]['SIZE']=15;
					}
					$opsform[$nmaxsels]['LABEL'] = (!$cmdsel[$nmaxsels]->titulo) ? $m->titulo : $cmdsel[$nmaxsels]->titulo;
					if ($padrao[$nmaxsels] && !$m->repeticoes) {
						$opsform[$nmaxsels]['VALUE']=$padrao[$nmaxsels];
						$opsform[$nmaxsels]['OPTIONS']=faz_select($cmdsel[$nmaxsels]->tab, $cmdsel[$nmaxsels]->campo_id, '', $cmdsel[$nmaxsels]->campo_txt, $cmdo->order, "SUBSTRING(id,1,2)=".substr($padrao[$nmaxsels],0,2), false, false);
					} elseif ($cmdsel[0]->tab=='estados' && $uf && $uf<>99 && !$m->repeticoes) {
					// Pequeno hack para o caso das cidades. Realmente, esse lance de selects relacionados é muito difícil! Estou cansado e me vi obrigado a fazer este hack... uma pena!
						$sqltmp="select id, cidade from cidades_ibge, estados where substring(id,1,2)=id_UF AND id_UF=".$uf;
						$tmp=faz_query($sqltmp, $db, 'array');

						$opsform[$nmaxsels]['OPTIONS']=faz_select($tmp,'id', '', 'cidade', '', '', false, false);

						$sqltmp.=" AND capital=1";
						$tmp=faz_query($sqltmp, $db, 'row');
						$opsform[$nmaxsels]['VALUE']=$tmp[0][0];
					} else {
						if (!$opsform[$nmaxsels]['OPTIONS'])
							$opsform[$nmaxsels]['OPTIONS']=array(''=>'---');
						$opsform[$nmaxsels]['VALUE']='';
					}
					if (in_array('NotEmpty', $regras)) {
						$opsform[$nmaxsels]['ValidateAsNotEmpty']=1;
						$opsform[$nmaxsels]['ValidationErrorMessage']=$m->titulo.': '.$this->txt_lingua['erro_NotEmpty'];
					} // fim do if in_array
					if ($cmdo->bd)
					//print_r($opsform[$nmaxsels]);exit;
				break;
				case 'georref':
					unset($padrao, $cmdsel);
					$padrao = array();
					if (!(isset($cmdo->pais) && $cmdo->pais)) {
						$where_select_paises = 'id IN (';
						foreach ($paises as $pa)
							$where_select_paises.='"'.$pa.'", ';
						$where_select_paises = substr($where_select_paises,0,-2).')';
					} else {
						$dados[$m->campo][0]=$padrao[0]=$cmdo->pais;
					}
					if (!(isset($m->repeticoes) && $m->repeticoes)) {
						// Este primeiro if verifica se foi passado o terceiro nível (cidade). Se foi, então o $dados não mais deve ser um array, e sim simplesmente o valor $dados[$m->campo][2], que dá certinho
						if ((isset($dados[$m->campo]) && is_array($dados[$m->campo])) && (isset($dados[$m->campo][2]) && $dados[$m->campo][2])) {
							$dados[$m->campo]=$dados[$m->campo][2];
						}
						if ((isset($dados[$m->campo]) && $dados[$m->campo]) && !is_array($dados[$m->campo])) {
							$padrao[2]=$dados[$m->campo];
						} elseif (isset($dados[$m->campo]) && is_array($dados[$m->campo])) {
						// Este else é para o caso de dados passados até país ou estado. Neste caso, não temos o valor final da cidade, mas temos pelo menos o select de estados do país, e mesmo o select de municípios de estado, se o estado também foi escolhido (nível 2):
							$padrao = $dados[$m->campo];
						} else {
							// hack para definir o nível 0 como BR
							// TODO: voltar a permitir países que não o Brasil
							$padrao[0] = 'BR';
						}
					}
					$paises_selecionados=array();
					// Seqüência importante de IFs:
					if ($m->repeticoes && is_array($dados[$m->campo])) {
						// Se o $dados[$m->campo] é um array e este campo é múltiplo, significa que tenho que deixar no box múltiplo da direita as cidades escolhidas, mas montadas com estado e país:
						$where_select_cidades = 'a.id IN (';
						foreach ($dados[$m->campo] as $p) {
							if (isset($p) && $p && !in_array(substr($p,0,2), $paises_selecionados))
								$paises_selecionados[]=substr($p,0,2);
							$where_select_cidades.='"'.$p.'", ';
						}
						$where_select_cidades = substr($where_select_cidades,0,-2).')';
						foreach ($paises_selecionados as $paistmp) {
							$sql = "SELECT a.id, CONCAT('".$paistmp."', '/', b.nome, '/', a.nome) AS nome FROM ".$bd['refs']."._".$paistmp." as a, ".$bd['refs']."._".$paistmp."_maes as b WHERE a.id_mae=b.id AND ".$where_select_cidades;
							$tmp = faz_query($sql,$db,'object');
							if ($tmp)
								foreach ($tmp as $t)
									$opsform[2]['OPTIONS'][$t->id]=$t->nome;
						}
						if (!isset($opsform[2]['OPTIONS']))
							$opsform[2]['OPTIONS']=array(''=>'---');
						$padroes_protegido=array();
						foreach ($dados[$m->campo] as $k=>$t)
							if (in_array($t,array_keys($opsform[2]['OPTIONS'])))
								$padroes_protegido[]=$t;
						$dados[$m->campo]=$padroes_protegido;
					} elseif (isset($padrao[2])) {
						// Por outro lado, se o $padrao[2] existe (e portanto também não é um $m->repeticoes pela sua definição), então tenho um select único em cascata, que deve estar nas posições padrão de país, estado e cidade!
						$padrao[0]=substr($padrao[2],0,2);
						$sql='select id_mae from '.$bd['refs'].'._'.$padrao[0].' WHERE id="'.$padrao[2].'"';
						$tmp=faz_query($sql, $db, 'row');
						$padrao[1]=$tmp[0][0];
						if ($padrao[1]) {
							$opsform[2]['VALUE']=$padrao[2];
							$opsform[2]['OPTIONS']=array(''=>'---')+faz_select($bd['refs'].'._'.$padrao[0], 'id', '', 'nome', 'nome', 'id_mae="'.$padrao[1].'"', false, false);
						} else {
							$opsform[2]['VALUE']='';
							$opsform[2]['OPTIONS']=array(''=>'---');
						}
						// Aqui eu confiro e elimino os itens dos $dados que não estão nas opções:
						$ttt=array();
						if (is_array($dados[$m->campo])) {
							foreach ($dados[$m->campo] as $dado)
								if (in_array($dado,array_keys($opsform[2]['OPTIONS'])))
									$ttt[]=$dado;
						} else {
							$ttt = (in_array($dados[$m->campo],array_keys($opsform[2]['OPTIONS'])))
								? $dados[$m->campo]
								: '';
						}
						$dados[$m->campo]=$ttt;
						if (!$ttt[0] || !$ttt)
							unset($padrao);

					} elseif (isset($padrao) && is_array($padrao)) {
						$opsform[2]['VALUE']='';
						// Este é o caso em que foi indicado apenas o país ou o estado, mas não a cidade (padrao[2]). Neste caso, temos que montar os selects até o nível que for:
						if ($padrao[1]) {
							$opsform[2]['OPTIONS']=array(''=>'---')+faz_select($bd['refs'].'._'.$padrao[0], 'id', '', 'nome', 'nome', 'id_mae="'.$padrao[1].'"', false, false);
						} else {
							$opsform[2]['OPTIONS']=array(''=>'---');
						}
					} else {
						// Por fim, este é o caso em que não nada marcado: assim, o value e options da cidade são nulos!
						$opsform[2]['VALUE']='';
						$opsform[2]['OPTIONS']=array(''=>'---');
					}

					$primeiro_nivel1 = ($m->repeticoes) ? '' : '&primeiro=--'.$this->txt_lingua['frm_selecione'].'--';
					$agregacampos = ($m->repeticoes) ? '&agregacampos=1' : '';
					$id_prox_select = array($m->campo.'_1', $m->campo);
					// País:
					if (!(isset($cmdo->pais) && $cmdo->pais)) {
						$opsform[0]['TYPE']='select';
						$opsform[0]['CLASS']='form-control selectpicker';
						$opsform[0]['NAME']=$m->campo.'_0';
						$opsform[0]['ID']=$m->campo.'_0';
						$opsform[0]['LABEL']=$this->txt_lingua['pais'];
						$opsform[0]['VALUE']= (isset($padrao[0])) ? $padrao[0] : '-9999';
						$opsform[0]['OPTIONS']=array('-9999'=>'---') + faz_select($bd['refs'].'.__paises', 'id', '', 'nome', 'nome', $where_select_paises, false, false);
						$opsform[0]['ONCHANGE']="javascript:Ajax.Request('method=getGeorref&nivel=0&primeiro=--".$this->txt_lingua['frm_selecione']."--&valor='+this.value+'".(isset($primeiro) ? $primeiro : '')."', Ajax.Response, '".$id_prox_select[0]."', ['".$id_prox_select[1]."'],'carregando_".$m->campo."');";
					}
					// Estado:
					$opsform[1]['TYPE']='select';
					$opsform[1]['CLASS']='form-control selectpicker';
					$opsform[1]['NAME']=$m->campo.'_1';
					$opsform[1]['ID']=$m->campo.'_1';
					$opsform[1]['LABEL']=$this->txt_lingua['estado'];
					$opsform[1]['VALUE'] = (isset($padrao[1])) ? $padrao[1] : '';
					$opsform[1]['OPTIONS'] = (isset($padrao[0]))
						? array(''=>'---')+faz_select($bd['refs'].'._'.$padrao[0].'_maes', 'id', '', 'nome', 'nome', '', false, false)
						: array(''=>'---');

					$opsform[1]['ONCHANGE']="javascript:Ajax.Request('method=getGeorref&nivel=1&valor='+this.value+'".$agregacampos.$primeiro_nivel1."', Ajax.Response, '".$id_prox_select[1]."',false,'carregando_".$m->campo."');";

					// Municípios (este select é afetado pelos anteriores mas não afeta mais ninguém, então não tem o javascript...)
					$opsform[2]['TYPE']='select';
					$opsform[2]['CLASS']='form-control selectpicker';
					$opsform[2]['NAME']=$m->campo;
					$opsform[2]['ID']=$m->campo;
					if ($m->repeticoes) {
						$opsform[2]['MULTIPLE']=1;
						$opsform[2]['DATALIVESEARCH']=true;
						$opsform[2]['NMAX']=$m->repeticoes;
						$opsform[2]['SELECTED'] = ($dados[$m->campo])
							? $dados[$m->campo]
							: array();
						$opsform[2]['SIZE']= ($cmdo->size) ? $cmdo->size : 15;
					}
					$opsform[2]['LABEL'] = $this->txt_lingua['municipio'];
					if (in_array('NotEmpty', $regras)) {
						$opsform[2]['ValidateAsNotEmpty']=1;
						$opsform[2]['ValidationErrorMessage']=$m->titulo.': '.$this->txt_lingua['erro_NotEmpty'];
					} // fim do if in_array
					//print_r($opsform);exit;
				break;
				case 'text':
				case 'textarea':
					$opsform[0]['NAME']=$m->campo;
					$opsform[0]['ID']=$m->campo;
					if ($dados[$m->campo])
						$opsform[0]['VALUE'] = ($m->tipo_mysql=='float')
							? number_format($dados[$m->campo],4,',','')
							: $dados[$m->campo];
					$opsform[0]['LABEL']=$m->titulo;
					if ($m->ajuda)
						$opsform[0]['LabelAJUDA']=$m->ajuda;
					$opsform[0]['TYPE']=$m->tipo_form;
					$opsform[0]['CLASS']='form-control';
					// Se for o campo 'id' do formulário, e já estiver definido, eu não deixo o usuário mudar!
					// Senão, ferraria tudo! Por isso o "accessible"=0 aqui:
					if (isset($FrmAno) && ($m->campo==$campo[$FrmAno]->id && (isset($dados[$m->campo]) && $dados[$m->campo])))
						$opsform[0]['Accessible']=0;

					if (isset($cmdo->size) && $cmdo->size) $opsform[0]['SIZE']=intval($cmdo->size);
						elseif (isset($cmdo->cols) && $cmdo->cols) {
							$opsform[0]['COLS']=intval($cmdo->cols);
							$opsform[0]['ROWS']=intval($cmdo->rows);
						}
					// Tenho que ver as regras:
					if ($regras[0]<>(-1))
						foreach ($regras as $rs) {
							$opsform[0]['ValidateAs'.$rs]=1;
							$opsform[0]['ValidateAs'.$rs.'ErrorMessage']=$m->titulo.': '.$this->txt_lingua['erro_'.$rs];

							// Se tem regra de Integer ou Float, tenho que verificar se tem limite máximo ou mínimo:
							if (in_array(substr($rs,0,10),array('LowerLimit', 'UpperLimit'))) {
								$tmp=explode(':',$rs);
								if (intval($tmp[1]))
									$opsform[0]['Validation'.$tmp[0]]=intval($tmp[1]);
							}
						} // fim do foreach $regras
					if (isset($cmdo->bloqueado) && $cmdo->bloqueado)
						$opsform[0]['Accessible']=0;
				break;
				case 'hidden':
					if ($dados[$m->campo]) {
						$opsform[0]['NAME']=$m->campo;
						$opsform[0]['VALUE']=$dados[$m->campo];
						$opsform[0]['TYPE']=$m->tipo_form;
					}
				break;
				case 'file':
					$j=$m->repeticoes+1;
					for ($i=0;$i<($m->repeticoes+1);$i++) {
						if (!isset($dados[$m->campo][$i]) || !$dados[$m->campo][$i]) {
							// O input para fazer upload de novo arquivo:
							$opsform[$i]['NAME']=$m->campo.'['.$i.']';
							$opsform[$i]['ID']=$m->campo.'_'.$i;
							if (isset($dados[$m->campo][$i])) $opsform[$i]['VALUE']=$dados[$m->campo][$i];
							$opsform[$i]['LABEL']=$m->titulo;
							$opsform[$i]['TYPE'] = (isset($cmdo->link))
								? 'text'
								: $m->tipo_form;
							$opsform[$i]['CLASS']='form-control';
							if (isset($cmdo->size) && $cmdo->size) $opsform[$i]['SIZE']=intval($cmdo->size);
							if (isset($cmdo->bloqueado) && $cmdo->bloqueado)
								$opsform[$i]['Accessible']=0;
						} else {
							// Se já existe arquivo uploaded ou link inserido, o input é um checkbox com a opção de apagar o arquivo ou link:
							$opsform[$i]['NAME']=$m->campo.'['.$i.']';
							$opsform[$i]['ID']=$m->campo.'_'.$i;
							$opsform[$i]['LABEL']=$this->txt_lingua['apagar_arquivo'];
							$opsform[$i]['TYPE']='checkbox';
							$opsform[$i]['CLASS']='form-control';
							$opsform[$i]['ONCLICK']="alterna_risca_texto(this.checked,'a_".$m->campo."_".$i."')";
							$opsform[$i]['VALUE']=$dados[$m->campo][$i];

							// O input hidden que guarda o nome do arquivo que atualmente existe
							$opsform[$j]['NAME']='hidden_'.$m->campo.'['.$i.']';
							$opsform[$j]['ID']='hidden_'.$m->campo.'_'.$i;
							$opsform[$j]['VALUE']=$dados[$m->campo][$i];
							$opsform[$j]['TYPE']='hidden';
						}
						$j++;
					}
				break;
				case 'arvore':
					$campo_id=$cmdo->campo_id;
					$campo_nome=$cmdo->campo_nome;
					$campo_mae=$cmdo->campo_mae;
					$arvore_bruta = resgata_arvore($cmdo->apoio,$campo_id,$campo_nome,$campo_mae);
					$arvore = new stdClass();
					$arvore->campo = $m->campo;
					foreach ($arvore_bruta as $i=>$no) {
						$arvore->dados[$i] = new stdClass();
						$arvore->dados[$i]->id=$m->campo.'_'.$no->$campo_id;
						$arvore->dados[$i]->parentes=explode('|',$no->parentes);

						$opsform[$i]['NAME']=$m->campo;
						$opsform[$i]['ID']=$m->campo.'_'.$no->$campo_id;
						$opsform[$i]['VALUE']=$no->$campo_id;
						$opsform[$i]['LABEL']=$no->$campo_nome;
						$opsform[$i]['TYPE']='checkbox';
						$opsform[$i]['MULTIPLE']=1;
						$opsform[$i]['ARVORE']=true;
						if ((isset($dados[$m->campo]) && is_array($dados[$m->campo])) && in_array($no->$campo_id, $dados[$m->campo]))
							$opsform[$i]['CHECKED']=1;
						// Se é um campo obrigatório, preciso avisar, mas só na primeira opção:
						if (in_array('NotEmpty', $regras) && $i==0) {
							$opsform[$i]['ValidateAsSet']=1;
							$opsform[$i]['ValidationErrorMessage']=$m->titulo.': '.$this->txt_lingua['erro_NotEmpty'];
						}
						if (isset($cmdo->bloqueado) && $cmdo->bloqueado)
							$opsform[$i]['Accessible']=0;
					}
					$opsform[0]['ARVORE']=$arvore;
				break;
			} // FIM DO SWITCH
			// Finalmente, entra o elemento AddInput após termos definido suas opções. Mas temos que tomar cuidado se não é do tipo de várias opções (por isso o loop):
			//print_r($opsform);
			if ($opsform)
				foreach ($opsform as $o)
					$this->AddInput($o);

			//Se é um campo que tem tabela auxiliar e pode ter a adição de novo valor ("outro"), então temos que adicionar um campo de texto para o "outro":
			if (isset($cmdo->outro) && $cmdo->outro) {
				if (isset($cmdo->outro_size)) $tmp=intval($cmdo->outro_size);
						else $tmp=intval($padrao_outro_size);
				if ($m->repeticoes) for ($i=0;$i<$m->repeticoes;$i++)
					$this->AddInput(array("TYPE"=>"text", "CLASS"=>"form-control", "NAME"=>$m->campo.'_outro'.$i, "ID"=>$m->campo.'_outro'.$i,
		"LABEL"=>"Outro ".$i.":", "SIZE"=>$tmp));

					else $this->AddInput(array("TYPE"=>"text", "CLASS"=>"form-control", "NAME"=>$m->campo.'_outro', "ID"=>$m->campo.'_outro',
		"LABEL"=>"Outro:", "SIZE"=>$tmp));
			}
		} // Fim do if que trava os campos 'admin' para os não admin!
			else {
				// Caso seja admin para não admin, o valor é passado como hidden:
				if ($dados[$m->campo] || $cmdo->padrao) {
					$opsform = array(0=>array());
					$opsform[0]['NAME']=$m->campo;
					$opsform[0]['VALUE'] = ($dados[$m->campo])
						? $dados[$m->campo]
						: $cmdo->padrao;
					$opsform[0]['TYPE']='hidden';
					$this->AddInput($opsform[0]);
				}
			}
		} // Fim do loop foreach pelos campos $mapa todos da tabela $tab

	} // fim da função rodaMapaCampos
}
?>
