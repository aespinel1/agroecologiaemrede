<?php

//
// +------------------------------------------------------------------------+
// | PHP version 5.0 					                                  	|
// +------------------------------------------------------------------------+
// | Description:													      	|
// | Class to populate drop down using AJAX + PHP 	  						|
// | 																		|
// +------------------------------------------------------------------------+
// | Author				: Neeraj Thakur <neeraj_th@yahoo.com>   			|
// | Created Date     	: 18-12-2006                  						|
// | Last Modified    	: 08-04-2009                  						|
// | Last Modified By 	: daniel tygel                  					|
// +------------------------------------------------------------------------+

if (!isset($bd)) {
	include "../../conexao.inc.php";
}
$debug=0;
DEFINE ('DB_USER', $bd['usuaria']);
DEFINE ('DB_PASSWORD', $bd['senha']);
DEFINE ('DB_HOST', $bd['servidor']);
DEFINE ('CHARSET', $charset);

if (headers_sent($filename, $linenum)) echo $filename . ' - '.$linenum;

if ($debug)
	header("Content-Type: text/html; charset=UTF-8");
	else header("Content-Type: application/xml; charset=UTF-8");

switch ($_GET['method']) {
	case 'getXML':
		DEFINE ('DB_NAME', $bd['agrorede']);
		$tabela = $_GET['tabela'];
	break;
	case 'getGeorref':
		DEFINE ('DB_NAME', $bd['refs']);
	break;
}
$obj = new AjaxDropdown($db);
$obj->table = $tabela;
echo $obj->{$_GET['method']}();
$db->close();



// Aqui começa a definição da classe:

class AjaxDropdown
{
	var $table;
	var $db;

	function AjaxDropdown($db)
	{
		$this->db = $db;
		// Make the connnection and then select the database.
		/*$dbc = @mysql_connect (DB_HOST, DB_USER, DB_PASSWORD) OR die ('Could not connect to MySQL: ' . mysql_error() );
		mysql_select_db (DB_NAME) OR die ('Could not select the database: ' . mysql_error() );
		//mysql_set_charset('UTF8');
		mysql_query("SET CHARACTER SET utf8");*/
	}

	function dbConnect()
	{
		DEFINE ('LINK', mysql_connect (DB_HOST, DB_USER, DB_PASSWORD));
	}

	function getXML()
	{
		global $chave;
		//$this->dbConnect();

		$primeiro	=	$_GET['primeiro'];
		$campo_id	=	$_GET['campo_id'];
		$campo_nome	=	$_GET['campo_nome'];
		$onde		=	$_GET['onde'];
		$valor		=	$_GET['valor'];
		$padrao		=	($_GET['padrao']) ? $_GET['padrao'] : 0;


		$sql = str_replace('{valor}',$valor,decifra(stripslashes($onde),$chave));
		if ($padrao) {
			$padrao=decifra(stripslashes($padrao),$chave);
			if (strpos($padrao,'=')) {
				if ($this->table=='cidades_ibge') $tmp='SUBSTRING(id,1,2)';
					else $tmp=$campo_id;
				$sqltmp="SELECT `".$campo_id."` FROM $this->table WHERE ".$padrao.' AND '.$tmp.'='.$valor.' LIMIT 1';
				$res=$this->db->query($sqltmp);
				$tmp=$res->fetch_row();
				$padrao=$tmp[0];
			}
		}
		$dados = $this->db->query($sql);
		return $this->geraXML($dados,$primeiro,$padrao);
	}

	function getGeorref()
	{
		global $chave;
		//$this->dbConnect();

		$nivel			= $_GET['nivel'];
		$valor			= $_GET['valor'];
		$primeiro		= $_GET['primeiro'];
		$agregacampos	= $_GET['agregacampos'];

		if ($valor!=-9999) {
		switch ($nivel) {
			case '0':
				$pais=$valor;
				$sql = 'SELECT id, nome FROM _'.$pais.'_maes ORDER BY nome';
			break;
			case '1':
				$pais=substr($valor,0,2);
				$sql = ($agregacampos)
					? 'SELECT a.id, CONCAT("'.$pais.'/",b.nome,"/",a.nome) as nome FROM _'.$pais.' as a, _'.$pais.'_maes as b WHERE a.id_mae=b.id AND a.id_mae="'.$valor.'" ORDER BY a.nome'
					: 'SELECT id, nome FROM _'.$pais.' WHERE id_mae="'.$valor.'" ORDER BY nome';
			break;
		}
		$dados = $this->db->query($sql);
		} else {
			$dados = null;
		}
		/*if (mysql_error())
			echo $sql.'<br>'.mysql_error();*/
		return $this->geraXML($dados,$primeiro);
	}

	function geraXML($dados,$primeiro='',$padrao='') {
		$xml = '<?phpxml version="1.0" encoding="'.CHARSET.'" ?>';
		$xml .= '<categorias>';

		// Aqui entra o valor zero:
		if ($primeiro) {
			$xml .= '<categoria>';
			$xml .= '<id>-9999</id>';
			$xml .= '<fname>'.$primeiro.'</fname>';
			$sel = (!isset($padrao) && !isset($row['capital'])) ? '1' : '0';
			$xml.='<padrao>'.$sel.'</padrao>';
			$xml .= '</categoria>';
		}

		if ($dados) {
			while($row = $dados->fetch_array())
			{
				$xml .= '<categoria>';
				$xml .= '<id>'. $row[0] .'</id>';
				$xml .= '<fname>'. $row[1] .'</fname>';
				$sel = ($row[0]==$padrao || (!$padrao && $row['capital'])) ? '1' : '0';
				$xml.='<padrao>'.$sel.'</padrao>';
				$xml .= '</categoria>';
			}
		}
		$xml .= '</categorias>';
		return $xml;
	}
}
?>
