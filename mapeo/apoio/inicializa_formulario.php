<?php

/*Tenho que ver se a pessoa não teclou no "selecao_rapida", pois
em caso positivo, deve ir direto para a edição deste formulário escolhido!!	*/
if ($_POST['selecao_rapida'] && $_POST['selecao_rapida']<>-9999) {
	$id=($_POST['selecao_rapida']<>-9999) ? $_POST['selecao_rapida'] : null;
	unset($_POST);
}
// Se a pessoa escolheu algum id no "selecao_vinculo", o atual formulário terá um $id definido e alguns valores padrão
if ($_POST['selecao_vinculo'] && $_POST['selecao_vinculo']<>-9999) {
	$sql="SELECT ";
	foreach($vinculorel as $vfonte=>$v) $sql.=$vfonte.', ';
	$sql=substr($sql,0,strlen($sql)-2);
	$sql.=" FROM ".$vinculotab." WHERE ".$campo[$vinculoFrmAno]->id."=".$_POST['selecao_vinculo'];
	$d=faz_query($sql,'', 'array');
	if ($d) foreach ($d as $dd1=>$dd2) {
		if ($par) $dados[$vinculorel[$dd1]]=$dd2[0];
		$par=!$par;
	}
	$id=$_POST['selecao_vinculo'];
	unset($_POST);
}

$novofrm = ($_POST['novofrm'] || !$id);

// Aqui eu inicializo a variável $frm!
//$frm=new form_class;
$frm=new form_dtygel_class;

// Faço as definições iniciais do formulário:
$frm->NAME="formulario_geral";
$frm->METHOD="POST";
$frm->ACTION="index.php?f=formulario_geral";
$frm->debug="trigger_error"; //Esse é pra eu debugar, enquanto programo
$frm->ResubmitConfirmMessage=$msg['resubmissao_de_mesmos_dados'];
$frm->OutputPasswordValues=0; //ver manual sobre isso...
$frm->OptionsSeparator="<br />\n";
$frm->ShowAllErrors=0;
$frm->InvalidCLASS='invalido';
$frm->encoding='utf-8';
if (!$edicao) $frm->ReadOnly=1;
/*
 * Style to apply to all invalid inputs when you just want to override a
 * few style attributes, instead of replacing the CSS class
 * Set to a non-empty string to specify the invalid input style attributes
 *
 * $frm->InvalidSTYLE='background-color: #ffcccc; border-color: #000080';
 *
 */
$frm->ErrorMessagePrefix="-> ";
$frm->ErrorMessageSuffix="";
unset($dados);
if ($_POST['processe']) {
	// Aqui eu recupero os dados passados pela pessoa que não foram armazenados por algum erro. Neste caso, deve-se recuperar o conjunto de informações que a pessoa passou!!
	foreach ($mapa as $m)
		$dados[$m->campo]=$_POST[$m->campo];
} elseif (!$novoform) {
	// Se o formulário não é novo, pego os dados que servirão de base para serem os padrões dos campos.
	// aqui eu recupero os DADOS básicos do formulário 'id', que se tornarão valores padrão
	// para formulários não novos. Estes valores estão no array $dados, uma importante variável!!
	unset($tmp,$where);
	foreach ($mapa as $m)
		if (
			$m->tipo_form!='ignorar' &&
			!($m->campo==$campo[$tab]->id || ($tab_base_comum && $m->campo==$campo[$tab_base_comum]->id) )
		)
			$tmp[]=$m->campo;
	$sql="SELECT {campos} FROM ".$tab." AS a";
	if ($tab_base_comum) {
		$sql.=", ".$tab_base_comum." AS b";
		$where = " AND a.".$campo[$FrmAno]->id."=b.".$campo[$tab_base_comum]->id;
	}
	$sql.=" WHERE a.".$campo[$FrmAno]->id."='".$id."'".$where;
	$sql=str_replace('{campos}',implode(', ',$tmp),$sql);
	$d=faz_query($sql,'', 'array');
	if ($d)
		foreach ($d as $dd1=>$dd2) {
			$dados[$dd1] = (in_array($mapa[$dd1]->tipo_form,array('checkbox','file','arvore')) || $mapa[$dd1]->repeticoes)
				? explode('|',$dd2[0])
				: $dd2[0];
		}
}

// Se estou na primeira parte de um novo formulário, sem $id definido, posso editar dinamicamente
//   um formulário com a "caixa rápida de seleção"!
if (!$id) {
	unset($opcoes);
	$onde=' WHERE publicada=1';
	// Se o usuário não é admin, aparecem apenas os questionários que ele preencheu:
	if (!$s->u->us_admin)
		$onde .= ' AND '.$campo[$FrmAno]->digitadora.' = "'.$s->u->us_login.'"';
	//aqui eu recupero TODOS os formulários já inseridos no banco de dados da tabela $tab, respeitado o $onde:
	$sql="SELECT a.".$campo[$FrmAno]->id.", ".$campo[$FrmAno]->nome." FROM ".$tab." AS a";
	if ($tab_base_comum) {
		$sql.=", ".$tab_base_comum;
		$onde.=" AND a.".$campo[$FrmAno]->id."=".$tab_base_comum.'.'.$campo[$tab_base_comum]->id;
	}
	$sql .=$onde."  ORDER BY ".$campo[$FrmAno]->nome;
	$ds=faz_query($sql, '', 'row');
	$opcoes['-9999']="selecionar...";
	if (isset($dd) && $dd) {
		foreach ($dd as $d) {
			if ($campo[$FrmAno]->id<>$campo[$FrmAno]->nome)
				$opcoes[$d[0]]=substr($d['1'],0,115);
			$opcoes[$d[0]].=' ('.$d[0].')';
		}
	}
	// Se há algum questionário preenchido pelo usuário, aparece o campo "edição rápida":
	if (count($opcoes)>1)
		$frm->AddInput(array(
			"TYPE"=>"select",
			"NAME"=>"selecao_rapida",
			"ID"=>"selecao_rapida",
			"VALUE"=>'-9999',
			"OPTIONS"=>$opcoes,
			//"ONCHANGE"=>"javascript:this.form.submit();",
			"ONCHANGE"=>"javascript:muda_menu('".$dir['base_URL']."mapeo/?f=formulario_geral&nomefrm=".$tab."&id='+this.value);",
			"LABEL"=>$txt['titulo_selecao_rapida'],
			"ACCESSKEY"=>"r"
		));

	// Aqui eu dou a opção da pessoa utilizar a ficha de inscrição para ter automaticamente os dados iniciais do empreendimento. Isto só vale para os casos em que haja uma ficha de inscrição e um sujeito que seja o mesmo que se inscreveu. Ou seja, se o formulário atual tem um vínculo com outro, ali no campo "vinculo" da tabela "formularios", que ja foi resgatado lá no index.php na variável array $vinculofrm:
	if ($vinculofrm) {
		unset($opcoes);
		$onde=' WHERE publicada=1';
		$sql="SELECT ".$campo[$vinculoFrmAno]->id.", ".$campo[$vinculoFrmAno]->nome." FROM ".substr($vinculotab,0,strlen($vinculotab)-1)."1".$onde."  ORDER BY ".$campo[$vinculoFrmAno]->nome;
		$res=faz_query($sql);
		$opcoes['-9999']="selecionar...";
		while ($d=mysql_fetch_row($res)) {
			if ($campo[$vinculoFrmAno]->id<>$campo[$vinculoFrmAno]->nome)
				$opcoes[$d[0]]=substr($d['1'],0,115);
			$opcoes[$d[0]].=' ('.$d[0].')';
		}
		if (count($opcoes)>1)
			$frm->AddInput(array(
				"TYPE"=>"select",
				"CLASS"=>"form-control",
				"NAME"=>"selecao_vinculo",
				"ID"=>"selecao_vinculo",
				"VALUE"=>'-9999',
				"OPTIONS"=>$opcoes,
				"ONCHANGE"=>"javascript:this.form.submit();",
				"LABEL"=>$vinculotitulo
			));
	} // fim da seleção rápida do formulário vinculado


} //fim do if (!$id)

// Aqui é a função gigante que gera os addInputs pra cada tipo de campo, rodando no $mapa.
$frm->rodaMapaCampos($mapa,$cmd,$dados,$s);

//Aqui vem os botões. Depende se estamos na última página, na primeira, ou no meio:
// Por enquanto desisti de ter uma imagem especial para a última página... algo a se pensar, não?
if ($pg==$nmaxpgs)
	/*$frm->AddInput(array(
		"TYPE"=>"image",
		"ID"=>"enviar",
		"NAME"=>"enviar",
		"SRC"=>$dir['imgs'].'stenviar.gif',
		"ONMOUSEOVER"=>"this.src='".$dir['imgs']."stenviar_a.gif'",
		"ONMOUSEOUT"=>"this.src='".$dir['imgs']."stenviar.gif'",
		"ALT"=>'Enviar',
		"VALUE"=>"Enviar",
		"ACCESSKEY"=>"v"
	));*/
	$frm->AddInput(array(
		"TYPE"=>"submit",
		"ID"=>"enviar",
		"NAME"=>"enviar",
		"CLASS"=>"btn btn-success btn-lg mb-4",
		"VALUE"=>$txt['enviar_dados']
	));
	else $frm->AddInput(array(
		"TYPE"=>"image",
		"ID"=>"enviar",
		"NAME"=>"enviar",
		"SRC"=>$dir['imgs'].'stproximo.gif',
		"ONMOUSEOVER"=>"this.src='".$dir['imgs']."stproximo_a.gif'",
		"ONMOUSEOUT"=>"this.src='".$dir['imgs']."stproximo.gif'",
		"ALT"=>'Enviar',
		"VALUE"=>"Enviar",
		"ACCESSKEY"=>"v"
	));

if ($pg>1) $frm->AddInput(array(
		"TYPE"=>"image",
		"ID"=>"voltar",
		"NAME"=>"voltar",
		"SRC"=>$dir['imgs'].'stanterior.gif',
		"ONMOUSEOVER"=>"this.src='".$dir['imgs']."stanterior_a.gif'",
		"ONMOUSEOUT"=>"this.src='".$dir['imgs']."stanterior.gif'",
		"ALT"=>'Voltar',
		"VALUE"=>"Voltar",
		"ACCESSKEY"=>"a"
	));


/*
 * 'processe' é o campo oculto que vai indicar se este formulário está sendo mostrado
 * pela primeira vez ou foi submetido. Também serve pra indicar a ação a tomar
 * (ou seja, verificar se foi validado ou não)
 */
	$frm->AddInput(array(
		"TYPE"=>"hidden",
		"NAME"=>"processe",
		"VALUE"=>1
	));
// Há outros campos ocultos importantes: nomefrm, pg, ano e ID, se estiver definido:
	$frm->AddInput(array(
		"TYPE"=>"hidden",
		"NAME"=>"nomefrm",
		"VALUE"=>$nomefrm
	));
	$frm->AddInput(array(
		"TYPE"=>"hidden",
		"NAME"=>"pg",
		"VALUE"=>$pg
	));
	if ($id) $frm->AddInput(array(
		"TYPE"=>"hidden",
		"NAME"=>"id_hidden",
		"VALUE"=>$id
	));
	$frm->AddInput(array(
		"TYPE"=>"hidden",
		"NAME"=>"ano",
		"VALUE"=>$ano
	));
	if ($novofrm || !$id) $frm->AddInput(array(
		"TYPE"=>"hidden",
		"NAME"=>"novofrm",
		"VALUE"=>1
	));

/*
 * Empty the array that will list the values with invalid field after validation.
 */
	$verify=array();
?>
