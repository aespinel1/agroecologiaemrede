$(document).ready(function () {
  $('#modalMenu').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var modal = $(this);
    modal.find('.modal-title').text(button.text());
    modal.find('.modal-body').html("<i>Carregando...</i>");
    $.get("partials/"+button.data('content')+".html", function(html) {
      modal.find('.modal-body').html(html);
    },'html');
  });
  $('#modalPainel').on('show.bs.modal', function (event) {
    var modalBody = $(this).find('.modal-body div');
    var painel = $('#painel');
    modalBody.html(painel.html());
    painel.html("");
  });
  $('#modalPainel').on('hide.bs.modal', function (event) {
    var modalBody = $(this).find('.modal-body div');
    var painel = $('#painel');
    painel.html(modalBody.html());
    modalBody.html("");
  });
  $('#modalInfos').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var modal = $(this);
    modal.find('.modal-body').html("<i>Carregando...</i>");
    $.get("ajax_text.php?id="+button.data('content'), function(data) {
      modal.find('.modal-title').text(data.tex_titulo);
      modal.find('.modal-body').html(data.tex_texto);
    },'json');
  });
});


/*if (screen.width <= 800)
			document.write("<link rel='stylesheet' href='<?=$dir['apoio_URL']?>estilo800.css' type='text/css'>");*/
function MM_jumpMenu(targ,selObj,restore){
	eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
	if (restore) selObj.selectedIndex=0;
}
function muda_menu(url){ window.location=url; }
function trim(texto) { return texto.replace(/^\s+|\s+$/g,""); }
function alterna_visao(origem,alvo) {
	var elm = document.getElementById(alvo);
	//elm.style.display = origem.checked ? "inline" : "none";
	var elm_origem = document.getElementById(origem);
	elm.style.display = elm_origem.checked ? "inline" : "none";
}
function alterna_visao_arvore(alvo) {
	var elm = document.getElementById('div_'+alvo);
	var s = document.getElementById('s'+alvo);
	var sub = $('#s'+alvo+' i');
	if (elm.style.display == "none") {
		elm.style.display="inline";
		sub[0].className='oi oi-minus';
	} else {
		elm.style.display="none";
		sub[0].className='oi oi-plus';
	}
}
function alterna_risca_texto(valor, alvo) {
	var elm = document.getElementById(alvo);
	elm.style.textDecoration = (valor) ? "line-through" : "none";
}
window.size = function() {
	var w = 0;
	var h = 0;
	//IE
	if(!window.innerWidth) {
		//strict mode
		if(!(document.documentElement.clientWidth == 0)) {
			w = document.documentElement.clientWidth;
			h = document.documentElement.clientHeight;
		}
		//quirks mode
		else {
			w = document.body.clientWidth;
			h = document.body.clientHeight;
		}
	}
	//w3c
	else {
		w = window.innerWidth;
		h = window.innerHeight;
	}
	return {width:w,height:h};
}
function entrouMousePainel (idCampo) {
	var idCampoAtual = $('#modalFiltros').attr('data-idCampo');
	if (idCampoAtual!=idCampo && idCampoAtual!="") {
		// Original contents must be moved to modal to avoid 'id' duplicates:
		$('#'+idCampoAtual+'_opcoes').html($('#modalFiltros .modal-body').html());
		$('#'+idCampoAtual+'_opcoesTitulo').text($('#modalFiltros .modal-title').text());
	}
	if (idCampoAtual!=idCampo) {
		// only if modal content and filter content are different we should change modal content:
		$('#modalFiltros').attr('data-idCampo', idCampo);
		$('#modalFiltros .modal-body').html($('#'+idCampo+'_opcoes').html());
		$('#modalFiltros .modal-title').text($('#'+idCampo+'_opcoesTitulo').text());
		$('#'+idCampo+'_opcoes').html("");
		$('#'+idCampo+'_opcoesTitulo').text("");
	}
	$('#modalPainel').modal('hide');
	$('#modalFiltros').modal('show');
}
function inicializaFiltros (inicializacao = 1) {
	for (var i=0;i<filtros.length;i++) {
		if (filtros[i].sels) {
			for (var j=0;j<filtros[i].sels.length;j++) {
				if (!filtros[i].sels[j].select && !filtros[i].sels[j].texto)
					document.getElementById(filtros[i].sels[j].id).checked=true;
				if (!filtros[i].sels[j].texto) {
					atualizaSelecao(filtros[i].idCampo, JSON.stringify(filtros[i].sels[j]),inicializacao);
				}
			}
		}
	}
}
function inicializaLegenda(idLegenda) {
	var l='';
	var ls=[];
	var n=0;
	var ltmp='';
	for (var i=0;i<filtros.length;i++) {
		if (filtros[i].sels) {
			ls.length = 0;
			n = 0;
			ltmp = '<p><b>'+filtros[i].nome+':</b> ';
			for (var j=0;j<filtros[i].sels.length;j++) {
				if (filtros[i].sels[j].nome) {
					ls[n] = filtros[i].sels[j].nome;
					if (filtros[i].sels[j].texto)
						ls[n]='"'+ls[n]+'"';
					n++;
				}
			}
			if (ls.length>0)
				l += ltmp + ls.join(', ')+'</p>';
		}
	}
	document.getElementById(idLegenda).innerHTML=l;
}
function alteraVariavelFiltrosSels (idCampo,sel,insere,inicializacao) {
	if (!inicializacao) {
		for (var i=0;i<filtros.length;i++) {
			if (filtros[i].idCampo == idCampo) {
				if (filtros[i].sels) {
					if (insere) {
						filtros[i].sels.push(sel);
					} else {
						for (var j=0;j<filtros[i].sels.length;j++) {
							if (filtros[i].sels[j].id == sel.id) {
								if (filtros[i].sels.length>1)
									if (j == (filtros[i].sels.length-1))
										filtros[i].sels.pop()
										else filtros[i].sels.splice(j,1,filtros[i].sels.pop());
									else filtros[i].sels = '';
								break;
							}
						}
					}
				} else if (insere) {
					filtros[i].sels = [];
					filtros[i].sels[0] = sel;
				}
				break;
			}
		}
	}
}
function atualizaSelecao(idCampo,sel_json,inicializacao) {
	var sel = eval("("+sel_json+")");
	var mae = document.getElementById(idCampo+'_opcoesSels');
	/* Trabalho para o caso de selects (como a localização geográfica) */
	if (sel.select) {
		function funcaotmp(id) {this.id = id;}
		//var el = document.getElementById(sel.id);
		var el = $('#'+sel.id);
		if (document.getElementById('span_'+idCampo+'_'+idCampo)) {
			var t = new funcaotmp(idCampo);
			alteraVariavelFiltrosSels(idCampo,t,0,inicializacao);
			mae.removeChild(document.getElementById('span_'+idCampo+'_'+t.id));
		}
		if (sel.href==1) {
			document.getElementById(idCampo).selectedIndex=0;
			if (sel.id!=idCampo) {
				var tmp = document.getElementById(idCampo);
				var o = tmp.options[0];
				tmp.options.length=0;
				tmp.options[0]=o;
			}
		}
		if (sel.id!=idCampo) {
			for (var i=0;i<10;i++) {
				var t = new funcaotmp(idCampo+'_'+i);
				if (document.getElementById(t.id)) {
					if (t.id >= sel.id) {
						alteraVariavelFiltrosSels(idCampo,t,0,inicializacao);
						if (document.getElementById('span_'+idCampo+'_'+t.id)) {
							mae.removeChild(document.getElementById('span_'+idCampo+'_'+t.id));
						}
						if (sel.href==1) {
							//$("#"+t.id).val("");
							$("#"+t.id).val($("#"+t.id+" option:first").val());
							if (t.id > sel.id) {
								var tmp = document.getElementById(t.id);
								var o = tmp.options[0];
								tmp.options.length=0;
								tmp.options[0]=o;
							}
						}
					}
				} else {
					break;
				}
			}
		}
		if (sel.href!=1 && typeof el.val() != 'undefined') {
			sel.nome = $('#'+sel.id+' option:selected').text();
			sel.valor = el.val();
			var span = document.getElementById('span_'+idCampo+'_'+sel.id);
			var barra = '';
			/*barra = (mae.innerHTML)
				? ' / '
				: '';*/

			if (!span) {
				var span = document.createElement('span');
				span.setAttribute('id','span_'+idCampo+'_'+sel.id);
				span.className = 'span';
				mae.appendChild(span);
			}
			sel.href=1;
			alteraVariavelFiltrosSels(idCampo,sel,1,inicializacao);
			span.innerHTML = barra + '<a class="col-sm-12 col-md-6 text-truncate badge badge-light badge-pill badge-sm" href="javascript:atualizaSelecao(\''+idCampo+'\', \''+JSON.stringify(sel).replace(/\"/gi,"\\'")+'\')" title="'+sel.nome+'"><i class="oi oi-circle-x text-danger"></i> ' + sel.nome + '</a>';
		}
	} /* fim do if select... */
	else if (sel.texto) {
		/* Trabalho para o caso de texto simples */
		alteraVariavelFiltrosSels(idCampo,sel,0,inicializacao);
		sel.valor = document.getElementById(sel.id).value;
		sel.nome = sel.valor;
		alteraVariavelFiltrosSels(idCampo,sel,1,inicializacao);
	} else {
		/* Trabalho para o caso de checkboxes */
		if (document.getElementById(sel.id).checked!=true || sel.href==1) {
			alteraVariavelFiltrosSels(idCampo,sel,0,inicializacao);
			if (document.getElementById('span_'+idCampo+'_'+sel.id))
				mae.removeChild(document.getElementById('span_'+idCampo+'_'+sel.id));
			if (sel.href==1)
				document.getElementById(sel.id).checked=false;
		} else {
			alteraVariavelFiltrosSels(idCampo,sel,1,inicializacao);
			var novo = document.createElement('span');
			novo.setAttribute('id','span_'+idCampo+'_'+sel.id);
			novo.className = 'span';
			/*if (mae.innerHTML) {
				novo.innerHTML = ' | ';
			}*/
			sel.href=1;
			novo.innerHTML += '<a class="col-sm-12 col-md-6 text-truncate badge badge-light badge-pill badge-sm" href="javascript:atualizaSelecao(\''+idCampo+'\', \''+JSON.stringify(sel).replace(/\"/gi,"\\'")+'\')" title="'+sel.nome+'"><i class="oi oi-circle-x text-danger"></i> ' + sel.nome + '</a>';
			mae.appendChild(novo);
		}
	}
	// e aqui eu resgato os resultados pra por no painel
	if (!inicializacao) {
		// pego a qtde de campos com filtros:
		var n=0;
		for (var i=0;i<filtros.length;i++) {
			var sels = filtros[i].sels;
			if (typeof sels == 'string') {
				if (sels != '') {
					n++;
				}
			} else if (typeof sels == 'object') {
				if (sels[0].valor != '') {
					n++;
				}
			}
		}
		$('#badgeFiltros')
			.text(n)
			.toggleClass('d-none', n<=1);


		/* atualizo o painel de resultados
		if (!isMap) {
			$('#resultados').addClass('d-none');
			$('.'+el).removeClass('d-none');
		}*/
		resgataPontos('painelPreResultados');
		$('#modalFiltros').modal('hide');
		$('#modalPainel').modal('hide');
	}
}
function resgataPontos(el,recarrega,idLegenda) {
	if (recarrega==true) {
		$('.'+el).addClass('d-none');
	} else {
		$('.span_'+el).html("<img src='"+dirBase+"mapeo/imgs/loader16.gif'>");
	}
	numpts = 0;
	var tipofrm = document.getElementById('tipofrm');
	var dadosGet = atualiza_dados_get();
	if (typeof map !== 'undefined' && isMap) {
			if (recarrega==true) {
				//inicializaLegenda(idLegenda);
			}
			//pega_dados(Array(dadosGet), 0, el, conta, idLegenda);
			loadData('span_'+el);
	} else {
    $('#carregando_mapa').removeClass('d-none');
    $('#mostraTabela').addClass('d-none');
    $.get(url.url+dadosGet+'&outputFormat=html', function (html) {
      $('#mostraTabela')
        .html(html)
        .removeClass('d-none');
      var indiceHTML = ($('#indiceRodape').html())
        ? $('#indiceRodape').html()
        : '';
      $('#indiceCabecalho').html(indiceHTML);
      $('#carregando_mapa').addClass('d-none');
      var numpts = $('#mostraTabela .card-deck').attr('data-numpts')
        ? $('#mostraTabela .card-deck').attr('data-numpts')
        : 0;
      $('.span_'+el).text(numpts);
    });
    /*
		if (recarrega==true) {
			document.painel_mapas.submit();
		}	else {
			Ajax.Request2(urls[0]+dadosGet, Ajax.contaDadosMapa, 'span_'+el);
		}
    */
	}
}
function atualiza_dados_get() {
	url.dados_get = '?c=1&tipofrm='+tipofrm.value+'&visao=';
  url.dados_get += (isMap)
    ? 'mapa'
    : 'tabela';
	for (i=0;i<filtros.length;i++) {
		if (filtros[i].sels && filtros[i].sels!="") {
			url.dados_get += '&'+filtros[i].idCampo+'=';
			var tmp='';
			for (j=0;j<filtros[i].sels.length;j++) {
				if (filtros[i].sels[j].valor!==null)
					tmp += filtros[i].sels[j].valor+'|';
			}
			if (tmp.length>0)
				url.dados_get += tmp.substr(0,tmp.length-1);
		}
	}
	var newUrl = dirBase+'mapeo/index.php'+url.dados_get+'&f=consultar';
  var toggleDisplayUrl = (isMap)
    ? newUrl+'&visao=tabela'
    : newUrl+'&visao=mapa';
	$('.toggleDisplay').attr('href',toggleDisplayUrl);
	window.history.pushState("", "Agroecologia em Rede", newUrl);
	return url.dados_get;
}

function findPos(obj) {
    var curleft = curtop = 0;
    if (obj.offsetParent) {
    do {
    		curleft += obj.offsetLeft;
    		curtop += obj.offsetTop;
    } while (obj = obj.offsetParent);
    return [curleft,curtop];
    }
}



function findPosX(obj) {
    var curleft = 0;
    if(obj.offsetParent)
        while(1)
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
}

function findPosY(obj)  {
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
}

function abreDialog(url, targetElementId, title, action) {
	$('#'+targetElementId+' form').attr('action', action);
	$('#'+targetElementId+' .modal-title').text(title);
	$.get(url, function(data) {
		$('#'+targetElementId+' .modal-body').html(data);
		$('#'+targetElementId).modal('show');
	});
}

$(function () {
	$('[data-toggle="tooltip"]').tooltip();
	$(".popImage").on("click", function(e) {
	   $('#imagepreview').attr('src', $(e)[0].target.src);
	   $('#imagemodal').modal('show');
	});
	var animationDuration = 300;
	/*$(window).scroll(function() {
		var offset = 250;
		if ($(this).scrollTop() > offset) {
			$('#top-link-block').fadeIn(animationDuration);
		} else {
			$('#top-link-block').fadeOut(animationDuration);
		}
	});*/
	if ( ($(window).height() + 100) < $(document).height() ) {
    $('#top-link-block').removeClass('d-none');
	}
	$('#top-link-block').click(function(event) {
 		event.preventDefault();
		$('html, body').animate({scrollTop: 0}, animationDuration);
		return false;
	});

	/*$('.tree').each(function(i,e) {
		//var nmaxsels = $(e).attr("nmaxsels");
		var nmaxsels = 5;
		$(e).treeview({
			data: getTreeData($(e).attr("tree-data-source-id")),
			checkedIcon: "oi oi-check",
			uncheckedIcon: "oi oi-media-stop",
			showCheckbox: false,
			collapseIcon: "oi oi-minus mr-4",
			emptyIcon: "oi",
			expandIcon: "oi oi-plus mr-4",
			nodeIcon: "",
			showIcon: false,
			multiSelect: true,
			levels:1,
			onNodeUnselected: function(event, data) {
				console.log(data);
				console.log($(e).treeview('getSelected'));
			},
			onNodeSelected: function(event, data) {
				var selected = $(e).treeview('getSelected');
				//console.log(nmaxsels+'-'+selected.length);
				if(nmaxsels && selected.length>=nmaxsels) {
					alert('Seu limite máximo é de '+nmaxsels+' opções!');
					$(e).treeview('unselectNode', [data, {silent:false}]);
				}
				console.log($(e).treeview('getSelected'));
			}
		});
	});
	*/
	$('#previa_frm_instituicao_search').keyup(function(){
		var current_query = $('#previa_frm_instituicao_search').val().toLowerCase();
		if (current_query !== "") {
			$(".list-group a").hide();
			$(".list-group a").each(function(){
				var current_keyword = $(this).text().toLowerCase();
				if (current_keyword.indexOf(current_query) >=0) {
					$(this).show();
				};
			});
		} else {
			$(".list-group a").show();
		};
	});

});
/*
function getTreeData(id) {
	var flatTree = [];
	var tree=[];
	$('#'+id).find('.tree-item').each(function(i,e) {
		var item = $(e);
		var treeItemId = item.attr('data-item-id');
		var treeParentId = item.attr('data-parent');
		var item = {
			id: treeItemId,
			treeParentId: treeParentId,
			text: $("[for='"+treeItemId+"']").text(),
			selectable: true,
			state: {
				expanded: false
			},
			nodes: []
		}
		flatTree.push(item);
		if (!treeParentId) {
			tree.push(item);
			tree[tree.length-1].nodesRef = flatTree.length-1;
		}
	});
	for (var i=0;i<flatTree.length;i++) {
		if (!flatTree[i].treeParentId) {
			flatTree[i].nodes = findChildren(flatTree, flatTree[i]);
		}
	}
	for (var i=0;i<tree.length;i++) {
		tree[i].nodes = flatTree[tree[i].nodesRef].nodes;
	}
	return tree;
}

function findChildren(flatTree, node) {
	var children = [];
	for (var i=0;i<flatTree.length;i++) {
		if (flatTree[i].treeParentId==node.id) {
			children.push(flatTree[i]);
		}
	}
	for (var i=0;i<children.length;i++) {
		children[i].nodes = findChildren(flatTree, children[i]);
	}
	return children;
}
*/
