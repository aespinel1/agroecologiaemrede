<?php
require_once "../config.php";
function return_empty_image() {
	global $dir;
	header('Content-Type: image/png');
	$img = imagecreatefromfile($dir['base'].'/imagens/imagem_nao_encontrada.png');
	imagepng($img['img']);
	imagedestroy($img['img']);
}
if (!isset($_REQUEST['img']) || !isset($_REQUEST['s'])) {
	return_empty_image();
	exit;
}
$img = $_REQUEST['img'];
if (!file_exists($dir['upload'].$img)) {
	return_empty_image();
	exit;
}
$imgParts = pathinfo($img);
list($width,$height) = explode('x',$_REQUEST['s']);
$useImageResizeClass = true;
require_once $dir['apoio']."funcoes_comuns.php";
use \Gumlet\ImageResize;

$newImageFilename = $imgParts['filename'].'-'.$width.'x'.$height.'.'.strtolower($imgParts['extension']);
$newImage = imagecreatefromfile($dir['upload'].$newImageFilename);

if (!$newImage) {
	try {
		$image = new ImageResize($dir['upload'].$img);
		$image->resizeToBestFit($width, $height);
		$image->save($dir['upload'].$newImageFilename);
	} catch(Exception $e) {
		$newImageFilename = $img;
	}

	$newImage = imagecreatefromfile($dir['upload'].$newImageFilename);
}

//$newImage = imagecreatefromfile($dir['base'].'/imagens/imagem_nao_encontrada.png');
header('Content-Type: image/'.$newImage['type']);
switch ($newImage['type']) {
	case 'jpg':
		imagejpeg($newImage['img']);
		break;
	case 'png':
		imagepng($newImage['img']);
		break;
	case 'gif':
		imagegif($newImage['img']);
		break;
	case 'bmp':
		imagebmp($newImage['img']);
		break;
}
imagedestroy($newImage['img']);
