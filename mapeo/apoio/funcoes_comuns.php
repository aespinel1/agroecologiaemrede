<?php

if (isset($useImageResizeClass) && $useImageResizeClass) {
	include $dir['apoio']."php-image-resize/lib/ImageResize.php";
}

function criptografa($texto, $chave)
{
	$result = '';
	for($i=1; $i<=strlen($texto); $i++)
	{
		if (ord($texto[$i-1])>126) return 0;
		$char = $texto[$i-1];
		$chavechar = $chave[($i % strlen($chave))-1];
		$tmp=ord($char)+ord($chavechar)-126;
		if ($tmp<1) $tmp=$tmp+126;
		//echo ord($char).': '.$char.' -- '.ord($chavechar).': '.$chavechar.' -- fim='.$tmp.' -- '.chr(32).'<br>';
		$char = chr($tmp);
		$result.=$char;
	}
	return $result;
}

function decifra($texto, $chave)
{
	$result = '';
	for($i=1; $i<=strlen($texto); $i++)
	{
		$char = $texto[$i-1];
		$chavechar = $chave[($i % strlen($chave))-1];
		$tmp=ord($char)-ord($chavechar)+126;
		if ($tmp>126) $tmp=$tmp-126;
		//echo ord($char).': '.$char.' -- '.ord($chavechar).': '.$chavechar.' -- fim='.$tmp.' -- '.chr(32).'<br>';
		$char = chr($tmp);
		$result.=$char;
	}
	return $result;
}



//*
//*
//* Insere ou atualiza no mysql a ficha editada...
//*
//*
function adiciona() {
	global $bd;
	//TODO
}


//*
//*
//*
//* Formata números com o separador de milhar (".") e com o separador de decimais (",").
//*
//*
//*
function yLabelFormat($aLabel) {
    return number_format($aLabel, 0, ',', '.');
}




//*
//*
//*
//* Tira os slashes de matrizes
//*
//*
//*
function stripslashes_array($resultado)
{
	if (is_array($resultado)) $resultado=array_map('stripslashes_array', $resultado);
		else $resultado=stripslashes($resultado);
	return $resultado;
}

//*
//*
//*
//* Faz o query e escreve o erro, caso não tenha dado certo!
//*
//*
//*
function faz_query($sql,$db='',$tipo='',$assoc='', $encode=false) {
	global $bd;
	if (!$db) {
		$db = conecta($bd);
	}
	$res=$db->query($sql);
	if (!$res) {
		echo '<p>Deu pau no mysql! O comando sql que foi tentado foi o seguinte:</p>'.chr(10);
		echo '<p>'.$sql.'</p>'.chr(10);
		echo '<p>E o erro anunciado foi o seguinte: "'.$db->error.'"</p>'.chr(10);
	}
	$retorno = null;
	switch ($tipo) {
		default:
			$retorno=$res;
		break;
		case 'array':
			$i=0;
			while ($q=$res->fetch_array()) {
				if (!isset($keys) || !$keys) $keys=array_keys($q);
				foreach ($keys as $k)
					$retorno[$k][$i]=$q[$k];
				$i++;
			}
			if (isset($enconde) && $encode)
				$retorno = converte_charset2($retorno);
		break;
		case 'array_assoc':
			$i=0;
			while ($q=$res->fetch_array(MYSQLI_ASSOC)) {
				if (!isset($keys)) $keys=array_keys($q);
				foreach ($keys as $k)
					if ($k<>$assoc) $retorno[$q[$assoc]][$k]=$q[$k];
				$i++;
			}
			if (isset($enconde) && $encode)
				$retorno = converte_charset2($retorno);
		break;
		case 'row':
		case 'rows':
			$i=0;
			while ($q=$res->fetch_row()) {
				if (!isset($keys)) $keys=array_keys($q);
				foreach ($keys as $k)
					$retorno[$k][$i]=$q[$k];
				$i++;
			}
			if (isset($enconde) && $encode)
				$retorno = converte_charset2($retorno);
		break;
		case 'object':
			$i=0;
			$retorno = array();
			while ($q=$res->fetch_object()) {
				$arr=get_object_vars($q);
				if (!isset($keys) || !$keys) {
					$keys=array_keys($arr);
				}
				$retorno[$i] = new stdClass();
				foreach ($keys as $k) {
					$retorno[$i]->$k=$q->$k;
				}
				$i++;
			}
			if (isset($enconde) && $encode)
				$retorno = converte_charset2($retorno);
		break;
		case 'object_assoc':
			$i=0;
			$retorno = array();
			while ($q=$res->fetch_object()) {
				$arr=get_object_vars($q);
				if (!isset($keys)){
					$keys=array_keys($arr);
				}
				if (!isset($retorno[$q->$assoc])) {
					$retorno[$q->$assoc] = new stdClass();
				}
				foreach ($keys as $k)
					$retorno[$q->$assoc]->$k=$q->$k;
				$i++;
			}
			if (isset($enconde) && $encode)
				$retorno = converte_charset2($retorno);
		break;
		case 'num':
		case 'numrows':
		case 'num_rows':
			$retorno=$res->num_rows;
		break;
		case 'id':
			$retorno=$db->insert_id;
		break;
	}
	return $retorno;
}



//*
//*
//* Gera o mapa de campos a partir da tabela mapa_campos
//*
//*
function gera_mapa_campos($ano,$nomefrm,$tab_base_comum='',&$cmd,&$cmd_mapa='') {
	global $lingua;
	/*Obtenho os campos e características da tabela $tab armazenados no mapa_campos, e
		crio a variável objeto $mapa. Ela pode dar valores de duas maneiras:
		pelo índice na tabela mapa_campos ($mapa[1]->titulo),
		ou pelo campo específico ($mapa['Q1']->titulo) */
	$sql="select * from mapeo_mapa_campos where ano='".$ano."' AND (frm='".$nomefrm."'";
	if ($tab_base_comum)
		$sql .= " OR frm='".$tab_base_comum."'";
	$sql .= ") ORDER BY ord";
	$mapa=faz_query($sql,'','object_assoc', 'campo');
	/* A partir da $mapa, obtenho o conjunto completo de comandos para todos os
		campos da tabela. Trata-se de uma variável objeto $cmd, de tipo array ainda por
		cima: $cmd[CAMPO]->FUNCAO=VALOR */
	$campos_lingua = array("titulo", "titulocurto", "bloco", "sub_bloco", "ajuda");
	foreach ($mapa as $i=>$m) {
		if ($m->cmd) {
			$tmps=explode('|',$m->cmd);
			foreach ($tmps as $tmp) {
				$tmp2=explode(':',$tmp);
				if(!isset($cmd[$m->campo])) {
					$cmd[$m->campo] = new stdClass();
				}
				$cmd[$m->campo]->{$tmp2[0]}=$tmp2[1];
			}
			if (isset($cmd[$m->campo]->apoio) && $cmd[$m->campo]->apoio) {
				if (!isset($cmd[$m->campo]->campo_nome))
					$cmd[$m->campo]->campo_nome='nome';
				if (!isset($cmd[$m->campo]->campo_id))
					$cmd[$m->campo]->campo_id='id';
			}
		}
		if ($m->cmd_mapa) {
			$tmps=explode('|',$m->cmd_mapa);
			foreach ($tmps as $tmp) {
				$tmp2=explode(':',$tmp);
				if(!isset($cmd_mapa[$m->campo])) {
					if (!$cmd_mapa) {
						$cmd_mapa = array();
					}
					$cmd_mapa[$m->campo] = new stdClass();
				}
				$cmd_mapa[$m->campo]->{$tmp2[0]}=$tmp2[1];
			}
		}
		if ($lingua)
			foreach ($campos_lingua as $cmpo_lng)
				$mapa[$i]->$cmpo_lng = traduz($mapa[$i]->$cmpo_lng, $lingua);
	}
	return $mapa;
}


// Pega um texto e dá a sua resposta na língua $lingua.
// O texto tem que estar no seguinte formato: "lng1:txtlng1|lng2:txtlng2|lng3:txtlng3" etc.
// Se não estiver neste formato ou não houver a língua $lingua, devolve simplesmente o primeiro txt
function traduz($txt,$lingua='') {
	$tmps = explode('|',$txt);
	unset($tit);
	foreach ($tmps as $i=>$tmp) {
		$tmp2 = explode(':',$tmp);
		if (strlen($tmp2[0])>5) // pra evitar considerar dois pontos de texto normal
			$tit[$i]=$tmp;
			elseif (isset($tmp2[1]))
				$tit[$tmp2[0]]=str_replace($tmp2[0].':','',$tmp);
			elseif (!in_array($tmp2[0],array($lingua,substr($lingua,0,2))))
				$tit[$i]=$tmp2[0];
				else $tit[$i]='';
	}
	$keys = array_keys($tit);
	if ($lingua) {
		if (!isset($tit[$lingua])) {
			$txt = (isset($tit[substr($lingua,0,2)]))
				? $tit[substr($lingua,0,2)]
				: $tit[$keys[0]];
		} else {
			$txt = $tit[$lingua];
		}
	} else {
		$txt=$tit[$keys[0]];
	}
	return $txt;
}


//*
//*
//* Faz options para determinada variável ou tabela "$fonte"
//*
//*
function faz_select($fonte, $campo_id, $padrao='', $campo_nome='', $order='', $where='', $html=true, $limite=70) {
global $bd, $lingua, $padrao_lingua;
	if (!isset($lingua) || !$lingua)
		$lingua = $padrao_lingua;
	if (!$order)
		$order=$campo_nome;
	if (!$campo_nome)
		$campo_nome=$campo_id;
	if ($where) {
		$where = str_replace('{campo_id}',$campo_id,$where);
		$where = str_replace('{campo_nome}',$campo_nome,$where);
	}
	// Aqui eu faço a varredura numa tabela mysql se a variável $fonte for uma string de texto, pois significa que ela é um nome de uma tabela.
	if (!is_array($fonte)) {
		$sql="SELECT ".$campo_id.", ".$campo_nome.", ".$order." as ord FROM ".$fonte;
		$sql.=" WHERE $campo_nome IS NOT NULL";
		if ($where)
			$sql .=" AND ($where)";
		$sql.=" ORDER BY ".$order.";";
		$fontetmp=faz_query($sql,'','array');
		// Aqui faço o ordenamento mais complexo, pois pode ser que a tabela tenha traduções, o que faria falhar o "order by" no query do mysql:
		$fonte=array();
		foreach ($fontetmp['ord'] as $i=>$txt)
			$fontetmp['ord'][$i]=traduz($txt,$lingua);
		asort($fontetmp['ord']);
		$j=0;
		foreach ($fontetmp['ord'] as $i=>$txt) {
			$fonte[$campo_id][$j]=traduz($fontetmp[$campo_id][$i],$lingua);
			if ($campo_nome!=$campo_id)
				$fonte[$campo_nome][$j]=traduz($fontetmp[$campo_nome][$i],$lingua);
			$j++;
		}
	}
	if (!$fonte) return '';
	for ($i=0;$i<count($fonte[$campo_id]);$i++) {
		if ($limite && strlen(traduz($fonte[$campo_nome][$i],$lingua))>$limite)
			$fonte[$campo_nome][$i] = substr($fonte[$campo_nome][$i],0,$limite).'...';
		if ($fonte[$campo_id][$i]===0) $fonte[$campo_id][$i]='0';
		if ($html) {
			$select .= '<option value="'.$fonte[$campo_id][$i].'"';
			if ($fonte[$campo_id][$i]==$padrao)
				$select .= ' selected="selected"';
			$select .= '>'.traduz($fonte[$campo_nome][$i],$lingua).'</option>'.chr(10);
		} else
			$select[$fonte[$campo_id][$i]]=traduz($fonte[$campo_nome][$i],$lingua);
	}
	//print_r($select);
	return $select;
}



//*
//*
//* é quase como o faz_select, mas apenas resgata os valores dos divs relacionados indicados em determinada tabela de apoio, para uso com o comando div_rel:
//*
//*
function resgata_div_rel($tab_aux, $campo_id, $campo_id_div_rel="") {
	global $bd, $padrao_campo_id_div_rel;
	if (!$campo_id_div_rel)
		$campo_id_div_rel = $padrao_campo_id_div_rel;
	$db = conecta($bd);
	$db->select_db($bd['universo']);
	$sql="SELECT ".$campo_id.", ".$campo_id_div_rel." FROM ".$tab_aux;
	$dados=faz_query($sql,$db,'array');
	if (!$dados) return '';
	for ($i=0;$i<count($dados[$campo_id]);$i++)
		$ret[$dados[$campo_id][$i]]=$dados[$campo_id_div_rel][$i];
	$db->close();
	return $ret;
}


//*
//*
//* Faz a conexão no banco de dados
//*
//*
function conecta($bd) {
	$db = new mysqli($bd['servidor'],$bd['usuaria'],$bd['senha'],$bd['agrorede']);
	if ($db->connect_errno) {
	    printf("Connect failed: %s\n", $db->connect_error);
	    exit();
	}
	$db->set_charset('utf8');
	return $db;
}

// Bobagens, para eu poder fingir que coloco dados, mas sem colocar de verdade...
function faz_query2($texto, $db='') {
	echo $texto.'<br>';
	return true;
}
function mysql_insere_id() {
	return 456;
}


//*
//*
//* Elimina acentos de um texto.
//*
//*
function tira_acentos($texto) {
		$tab['sem_acentos']=array(192=>"A", 193=>"A", 194=>"A", 195=>"A", 196=>"A", 197=>"A", 198=>"A", 199=>"C", 200=>"E", 201=>"E", 202=>"E", 203=>"E", 204=>"I", 205=>"I", 206=>"I", 207=>"I", 208=>"O", 209=>"N", 210=>"O", 211=>"O", 212=>"O", 213=>"O", 214=>"O", 216=>"O", 217=>"U", 218=>"U", 219=>"U", 220=>"U", 221=>"Y", 224=>"a", 225=>"a", 226=>"a", 227=>"a", 228=>"a", 229=>"a", 230=>"a", 231=>"c", 232=>"e", 233=>"e", 234=>"e", 235=>"e", 236=>"i", 237=>"i", 238=>"i", 239=>"i", 241=>"n", 242=>"o", 243=>"o", 244=>"o", 245=>"o", 246=>"o", 248=>"o", 249=>"u", 250=>"u", 251=>"u", 252=>"u", 253=>"y", 255=>"y");
		$tab['com_acentos']=array(chr(192), chr(193), chr(194), chr(195), chr(196), chr(197), chr(198), chr(199), chr(200), chr(201), chr(202), chr(203), chr(204), chr(205), chr(206), chr(207), chr(208), chr(209), chr(210), chr(211), chr(212), chr(213), chr(214), chr(216), chr(217), chr(218), chr(219), chr(220), chr(221), chr(224), chr(225), chr(226), chr(227), chr(228), chr(229), chr(230), chr(231), chr(232), chr(233), chr(234), chr(235), chr(236), chr(237), chr(238), chr(239), chr(241), chr(242), chr(243), chr(244), chr(245), chr(246), chr(248), chr(249), chr(250), chr(251), chr(252), chr(253), chr(255));
		$texto=str_replace($tab['com_acentos'],$tab['sem_acentos'],$texto);
		return($texto);
}

function resgata_dados_usuario() {
	$sql = "SELECT * FROM usuario WHERE us_login='".$_COOKIE['cookieUsuario']."'";
	$dados = faz_query($sql,'','object');
	$ret = $dados[0];
	$sql = "SELECT count(log_usuario) as qtAcesso FROM log_acesso WHERE log_usuario='".$_COOKIE['cookieUsuario']."'";
	$dados = faz_query($sql,'','object');
	$ret->cnt = $dados[0]->qtAcesso;
	/*
		Os dados são os seguintes: $ret->us_login, $ret->us_senha, $ret->us_nome, $ret->us_admin, $ret->cnt
	*/
	return $ret;
}

function resgata_arvore($tab,$campo_id,$campo_nome,$campo_mae,$id=0,$nivel=0,$parentes='0') {
	global $lingua;
	$tab_parentes = explode('|',$parentes);
	if (!in_array($id,$tab_parentes))
		$parentes .= '|'.$id;
	$sql = 'SELECT '.$campo_id.', '.$campo_nome.', '.intval($nivel).' as nivel FROM '.$tab.' WHERE '.$campo_mae.'="'.$id.'" ORDER BY '.$campo_nome;
	$dados=faz_query($sql,'','object');
	if ($dados)
		foreach ($dados as $dado) {
			$dado->parentes = $parentes;
			$dado->$campo_nome = traduz($dado->$campo_nome, $lingua);
			$rets[]=$dado;
			$tmps = resgata_arvore($tab,$campo_id,$campo_nome,$campo_mae,$dado->$campo_id,$nivel+1,$parentes);
			if ($tmps)
				foreach ($tmps as $i=>$tmp) {
					$rets[]=$tmp;
				}
		}
	else return false;
	return $rets;
}

function pega_xml_arquivo($url) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER,0); //Change this to a 1 to return headers
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER[‘HTTP_USER_AGENT’]);
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$data = curl_exec($ch);
	curl_close($ch);
	if ($data)
		$data=simplexml_load_string(utf8_encode($data));
	return $data;
}

function resgata_filtros($tipofrm) {
	global $campo, $txt;
	$sql = "SELECT * FROM mapeo_formularios WHERE tipo='".$tipofrm."'";
	$frms = faz_query($sql,'','object');

	$frm = $frms[0];
	// A variável $filtro->tab contem a tabela mais básica de um determinado tipo de formulário. Cada tipo de formulário tem uma tabela de base que é comum a todos os formulários, o que permite um mapa comum de ser gerado. Isso é para os filtros, mas não necessariamente para os dados do formulário que aparecerão no balão, mas isso é outra história.
	$filtro = new stdClass();
	$filtro->tab = (isset($frm->tab_base_comum) && $frm->tab_base_comum)
		? $frm->tab_base_comum
		: $frm->nomefrm;

	$mapa = gera_mapa_campos($frm->ano,$frm->nomefrm,$frm->tab_base_comum,$cmd,$cmd_mapa);

	// este gera o item para selecionar num determinado tipofrm que tenha mais de uma tabela
	/*if (count($frms)>1) {
		$tmp['nomefrm']->tipo_form = 'checkbox';
		$tmp['nomefrm']->campo = 'nomefrm';
		$tmp['nomefrm']->titulo = $txt['titulo_tipo_'.$tipofrm];
		$tmpc['nomefrm']->apoio = 'mapeo_formularios';
		$tmpc['nomefrm']->campo_id = 'nomefrm';
		$tmpc['nomefrm']->campo_nome = 'titulo';
		$tmpc['nomefrm']->where = 'tipo="'.$tipofrm.'"';
		$tmpcm['nomefrm']->filtro = 1;
		$mapa = $tmp + $mapa;
		$cmd = $tmpc + $cmd;
		$cmd_mapa = $tmpcm + $cmd_mapa;
	}*/

	foreach ($mapa as $m) {
		$cmdo_mapa = isset($cmd_mapa[$m->campo])
			? $cmd_mapa[$m->campo]
			: new stdClass();
		$cmdo = isset($cmd[$m->campo])
			? $cmd[$m->campo]
			: new stdClass();
		$cmdo->nmax_opcoes = 2000;
		// Um eventual where do $cmdo_mapa tem primazia sobre o where do $cmdo:
		if (isset($cmdo_mapa->where) && $cmdo_mapa->where)
			$cmdo->where = $cmdo_mapa->where;
		// Aqui eu transformo "radios" em "checkboxes" para o filtro permitir interseções:
		if ($mapa[$m->campo]->tipo_form=='radio')
			$mapa[$m->campo]->tipo_form='checkbox';
		unset ($cmdo->admin, $cmdo->ignorar, $mapa[$m->campo]->regras);
		if (isset($cmdo_mapa->geraXML) && $cmdo_mapa->geraXML) {
			foreach ($campo[$filtro->tab] as $obj_nome=>$obj_campo)
				if ($obj_campo == $m->campo) {
					$filtro->geraXML[$m->campo] = $obj_nome;
						break;
				}
			if (!isset($filtro->geraXML[$m->campo]))
				$filtro->geraXML[$m->campo] = '';
		}
		if (isset($cmdo_mapa->info) && $cmdo_mapa->info)
			$filtro->info[$m->campo] = ($m->titulocurto) ? $m->titulocurto : $m->titulo;
		if (isset($cmdo_mapa->filtro) && $cmdo_mapa->filtro) {
			$filtro->filtro[] = $m->campo;
			if (isset($cmdo->apoio) && $cmdo->apoio) {
				$otmp = isset($cmdo->order) ? $cmdo->order : '';
				$wtmp = isset($cmdo->where) ? $cmdo->where : '';
				$filtro->opcoes[$m->campo] = faz_select($cmdo->apoio, $cmdo->campo_id, '', $cmdo->campo_nome, $otmp, $wtmp, false);
			}

			// dtygel: hack para forçar o nível zero a ser sempre Brasil, por enquanto...
			// TODO: voltar a mostrar os países.
			if ($mapa[$m->campo]->tipo_form=='georref' && !(isset($_REQUEST[$m->campo.'_0']) && $_REQUEST[$m->campo.'_0']) && !(isset($_REQUEST[$m->campo]) && $_REQUEST[$m->campo])) {
				$_REQUEST[$m->campo.'_0'] = 'BR';
			}

			// resgate do que pode ter vindo por REQUEST:
			if (in_array($mapa[$m->campo]->tipo_form,array('georref','select_hierarquia')) && isset($_REQUEST[$m->campo.'_0']) && $_REQUEST[$m->campo.'_0']) {
				for ($i=0;$i<10;$i++) {
					if (isset($_REQUEST[$m->campo.'_'.$i]) && $_REQUEST[$m->campo.'_'.$i]) {
						$filtro->sels[$m->campo][$i] = $_REQUEST[$m->campo.'_'.$i];
						} else {
							if (isset($_REQUEST[$m->campo]) && $_REQUEST[$m->campo]) {
								$filtro->sels[$m->campo][$i] = $_REQUEST[$m->campo];
							}
							break;
						}
				}
			} elseif (isset($_REQUEST[$m->campo]) && $_REQUEST[$m->campo]) {
				$filtro->sels[$m->campo] = (is_array($_REQUEST[$m->campo]))
					? $_REQUEST[$m->campo]
					: explode('|',$_REQUEST[$m->campo]);
			}
		} elseif (!isset($cmdo_mapa->info) || !$cmdo_mapa->info) {
			unset($mapa[$m->campo],$cmd[$m->campo]);
		}
	}
	$filtro->textoBusca = (isset($_REQUEST['textoBusca']))
		? $_REQUEST['textoBusca']
		: '';
	$filtro->mapa=$mapa;
	$filtro->cmd=$cmd;
	$filtro->cmd_mapa=$cmd_mapa;
	return $filtro;
}

function organiza_geos($id=false,$distmax=false,$lingua='pt-br') {
	if ($id) {
		$geo = new StdClass();
		$geo->pais = new StdClass();
		$geo->pais->id = substr($id,0,2);
		$sql = "SELECT nome FROM __paises WHERE id='".$geo->pais->id."'";
		$res = faz_query($sql,'','rows');
		$geo->pais->nome = traduz($res[0][0],$lingua);
		if (strlen($id)>=5) {
			$geo->estado = new StdClass();
			$geo->estado->id=substr($id,0,5);
			$sql = "SELECT id, nome FROM _".$geo->pais->id."_maes WHERE id='".$geo->estado->id."'";
			if ($geo->pais->id=='BR') {
				$sql .= " OR id2='".$geo->estado->id."'";
			}
			$res = faz_query($sql,'','object');
			$geo->estado->nome = traduz($res[0]->nome,$lingua);
			$geo->estado->id = $res[0]->id;
			//echo $res[0][0];exit;
		}
		if (strlen($id)==9) {
			$geo->microrregiao = new StdClass();
			$geo->microrregiao->id = $id;
			$sql = "SELECT nome FROM _".$geo->pais->id."_microrregioes WHERE idmicro='".$geo->microrregiao->id."'";
			$res = faz_query($sql,'','rows');
			$geo->microrregiao->nome = traduz($res[0][0],$lingua);
			$geo->label = $geo->microrregiao->nome.", ".$geo->estado->nome;
		} elseif (strlen($id)==11) {
			$sql = "SELECT id, nome, lat, lng FROM _".$geo->pais->id." WHERE id='".$id."'";
			if ($geo->pais->id=='BR') {
				$sql .= " OR id2='$id'";
			}
			$res = faz_query($sql,'','object');
			$geo->cidade = new StdClass();
			$geo->cidade->id = $res[0]->id;
			$geo->cidade->nome = traduz($res[0]->nome,$lingua);
			$geo->cidade->lat = $res[0]->lat;
			$geo->cidade->lng = $res[0]->lng;
			$geo->label = $geo->cidade->nome.", ".$geo->estado->nome;
		}
		$geo->id = $id;
		$geo->nome = (isset($geo->cidade) && isset($geo->cidade->id))
			? ($geo->cidade->nome)
			: ($geo->microrregiao->id
				? ($geo->microrregiao->nome)
				: ($geo->estado->id
					? ($geo->estado->nome)
					: $geo->pais->nome
				)
			);
		$geo->precisao = $geo->cidade->id
			? 'cidade'
			: ($geo->microrregiao->id
				? 'microrregiao'
				: ($geo->estado->id
					? 'estado'
					: 'pais'
				)
			);
		if ($geo->cidade->id)
			$geo->distmax = $distmax;
	}
	return $geo;
}

function truncate($text, $chars = 25, $ellipses="(...)") {
  if (strlen($text) <= $chars) {
    return $text;
  }
  $text = $text." ";
  $text = substr($text,0,$chars);
  $text = substr($text,0,strrpos($text,' '));
  $text = $text.$ellipses;
  return $text;
}

function my_filesize($file) {
	if(is_file($file)) {
		$kb = 1024;         // Kilobyte
		$mb = 1024 * $kb;   // Megabyte
		$gb = 1024 * $mb;   // Gigabyte
		$tb = 1024 * $gb;   // Terabyte
		$size = filesize($file);
		if($size < $kb) {
			return number_format($size, 2, ",", ".")." B";
		}
		if ($size < $mb) {
			return number_format(round($size/$kb,2), 2, ",", ".")." KB";
		}
		if($size < $gb) {
			return number_format(round($size/$mb,2), 2, ",", ".")." MB";
		}
		if($size < $tb) {
			return number_format(round($size/$gb,2), 2, ",", ".")." GB";
		}

		return number_format(round($size/$tb,2), 2, ",", ".")." TB";

	}
}

function imagecreatefromfile( $filename ) {
  if (!file_exists($filename)) {
    return false;
  }
  switch ( strtolower( pathinfo( $filename, PATHINFO_EXTENSION ))) {
    case 'jpeg':
    case 'jpg':
        return array(
					'img' => imagecreatefromjpeg($filename),
					'type' => 'jpg'
				);
    	break;
    case 'png':
			return array(
				'img' => imagecreatefrompng($filename),
				'type' => 'png'
			);
    	break;
    case 'gif':
			return array(
				'img' => imagecreatefromgif($filename),
				'type' => 'gif'
			);
    	break;
		case 'bmp':
			return array(
				'img' => imagecreatefrombmp($filename),
				'type' => 'bmp'
			);
    	break;
    default:
      return false;
    	break;
  }
}

function getResizedImgURL($arq, $width=800, $height=800) {
	global $dir;
	$img = pathinfo($arq);
	$img['extension'] = strtolower($img['extension']);
	if (!in_array($img['extension'], array('gif','jpg','jpeg','png','webp'))) {
		return $dir['upload'].$arq;
	}
	$s = $width."x".$height;
	$img['path']=$img['filename'].'-'.$s.'.'.strtolower($img['extension']);
	if (!file_exists($dir['upload'].$img['path'])) {
		$img['url'] = $dir['apoio_URL'].'get_image.php?s='.$s.'&img='.$arq;
	} else {
		$img['url'] = $dir['upload_URL'].$img['path'];
	}
	return $img['url'];
}

function sql_distancia_geo($pos,$lat='localizacao_lat',$lng='localizacao_lng') {
	return "((acos(sin(".$pos->lat."*pi()/180) * sin(".$lat."*pi()/180) + cos(".$pos->lat."*pi()/180) * cos(".$lat."*pi()/180) * cos((".$lng." - ".$pos->lng.")*pi()/180)))*6378.7)";
}

function pR($t) {
	echo "<hr><pre>";
	print_r($t);
	echo "</pre>";
}

////////////////////////////////
//THESE SHOULD NEVER BE HERE ;-(
////////////////////////////////
function mostraTabela($filtro, $xml, $tipofrm, $numpts, $numpgs, $pgAtual) {
	global $dir;
	$n=0;
	?>

	<div class="row" id="selecaoAtual">
		<?php mostraSelecaoAtual($filtro); ?>
	</div>
	<?php if (!$numpts) { ?>
		<div class="row">
			<div class="col-md-12">
				<div id="semResultados">
					<div class="alert alert-danger">
						Oooops: Não há resultados para a sua busca e/ou filtros aplicados.
					</div>
				</div>
			</div>
		</div>
		<?php return; ?>
	<?php } ?>
	<div class="row">
		<div class="col-md-12">
			<div class="card-deck" data-numpts="<?=$numpts?>">
				<?php foreach ($xml as $x) { ?>
					<?php foreach ($x->markers->pt as $pt) { ?>
						<?php if ($n/2==round($n/2)) { ?>
							<div class="w-100 d-md-block d-lg-none"><!-- wrap every 2 on md--></div>
						<?php } ?>
						<?php if ($n/3==round($n/3)) { ?>
							<div class="w-100 d-none d-lg-block"><!-- wrap every 3 on lg--></div>
						<?php } ?>
						<?php /*if ($n/4==round($n/4)) { ?>
							<div class="w-100 d-none d-xl-block"><!-- wrap every 4 on xl--></div>
						<?php } */ ?>
						<?php
						$n++;
						$imgs = array();
						if (isset($pt['imgs']) && $pt['imgs']) {
							$imgsRaw = explode(',',$pt['imgs']);
							foreach ($imgsRaw as $img) {
								if (file_exists($dir['upload'].$img) && is_array(getimagesize($dir['upload'].$img))) {
									$imgs[] = $img;
								}
							}
						}
						$ellipses = ' <a href="'.str_replace('{id}',$pt['id'],$dir['mostra_'.$tipofrm.'_URL']).'">(...)</a>';
						?>
						<div class="card mb-4">
							<?php if (count($imgs)>0) { ?>
					    	<a href="<?=str_replace('{id}',$pt['id'],$dir['mostra_'.$tipofrm.'_URL'])?>">
									<img class="card-img-top" src="<?=$dir['upload_URL'].$imgs[array_rand($imgs)]?>" alt="Imagem da experiência">
								</a>
							<?php } ?>
					    <div class="card-body">
					      <p class="card-title"><?=$pt['nome']?></p>
								<p class="card-text text-right"><span class="badge badge-light"><?=$pt['cidade'].' / '.estadoParaUF($pt['estado'])?></span></p>
					      <p class="card-text"><?=truncate($pt['descricao'],220,$ellipses)?></p>
					      <p class="card-text"><small class="text-muted">Cadastrado na base em <?=date('d/m/Y',strtotime($pt['criacao']))?></small></p>
					    </div>
							<div class="card-footer text-center">
								<a href="<?=str_replace('{id}',$pt['id'],$dir['mostra_'.$tipofrm.'_URL'])?>" class="btn btn-info">Ver <?=($tipofrm=='experiencia')?"experiência":"detalhes e experiências relacionadas"?></a>
							</div>
					  </div>
						<?php
					}
				}
				?>

			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<?=faz_indice_tabela($numpts, $numpgs, $pgAtual, 'indiceRodape')?>
		</div>
	</div>
	<?php
}

function faz_indice_tabela($numpts, $numpgs, $pgAtual, $id) {
	global $padrao_itens_por_pagina;

	$href = str_replace('&pgLista='.$pgAtual,'',$_SERVER[QUERY_STRING]);
		$numptsAtual = ($pgAtual==$numpgs)
			? $numpts - (($numpgs-1)*$padrao_itens_por_pagina)
			: $padrao_itens_por_pagina;
	$ellipsisI = $ellipsisF = false;
	//$p = ($numpgs<=11) ? '' : ' p-1';
	?>
	<div id="<?=$id?>" class="indiceMostraTabela">
		<?php if ($numpgs>1) { ?>
			<ul class="pagination pagination-sm" style="flex-wrap: wrap;">
				<li class="page-item<?=($pgAtual>1) ? '' : ' disabled'?>"><a class="page-link<?=$p?>" href="?<?=$href?>&pgLista=<?=$pgAtual-1?>"><i class="oi oi-chevron-left"></i></a></li>
				<?php for ($i=1;$i<=$numpgs;$i++) { ?>
					<?php if ($numpgs<=11 || ($i<3 || $i>$numpgs-2 || ($i>$pgAtual-2 && $i<$pgAtual+2) ) ) { ?>
						<li class="page-item<?=($i==$pgAtual) ? ' active' : ''?>"><a class="page-link<?=$p?>" href="?<?=$href?>&pgLista=<?=$i?>"><?=$i?></a></li>
					<?php } else if (($i<$pgAtual && !$ellipsisI) || ($i>$pgAtual && !$ellipsisF)) { ?>
						<li class="page-item disabled"><a class="page-link<?=$p?>" href="#"><i class="oi oi-ellipses"></i></a></li>
						<?php if ($i<$pgAtual) { ?>
							<?php $ellipsisI = true; ?>
						<?php } else { ?>
							<?php $ellipsisF = true; ?>
						<?php } ?>
					<?php } ?>
				<?php } ?>
				<li class="page-item<?=($pgAtual<$numpgs) ? '' : ' disabled'?>"><a class="page-link<?=$p?>" href="?<?=$href?>&pgLista=<?=$pgAtual+1?>"><i class="oi oi-chevron-right"></i></a></li>
			</ul>
		<?php } ?>
	</div>
	<?php
}

function formata_dados_xml_agrorede($xml, $visao) {
	if (!$xml && !is_array($xml)) {
		return null;
	}
	foreach ($xml as $xtmp) {
		/*$numpts+= ($visao=='mapa')
			? $xtmp->num['num']
			: $xtmp->markers->pt['num'];*/
			$numpts+= (isset($xtmp->markers->pt['num']))
				? $xtmp->markers->pt['num']
				: $xtmp->num['num'];
		// E aqui eu recolho as categorias (para checkboxes dinâmicos), caso existam:
		if ($xtmp->config['cats']) {
			$tmp=explode('||',$xtmp->config['cats']);
			foreach ($tmp as $tm) {
				$tmp2=explode('|',$tm);
				if (!isset($ativ)) {
					$ativ = array();
				}
				$ativ[$tmp2[0]]=$tmp2[1];
			}
		}
		$numpgs = $xtmp->num['pgs'];
	}
	$t = new StdClass();
	$t->numpts = $numpts;
	$t->numpgs = $numpgs;
	$t->ativ = isset($ativ) ? $ativ : null;
	$t->xml = $xml;
	return $t;
}

function mostraSelecaoAtual($filtro) {
	?>
	<div class="col-md-12">
		<div class="alert alert-light text-muted border border-primary">
					<div class="row">
						<div class="col-md-12">
							<h5>Seleção atual</h5>
							<?php if((isset($filtro->textoBusca) && $filtro->textoBusca) || $filtro->sels) { ?>
							<ul class="list-inline">
								<?php if (isset($filtro->textoBusca) && $filtro->textoBusca) { ?>
									<li class="list-inline-item">
										<strong>Busca textual</strong>: <small>"<?= $filtro->textoBusca ?>"</small>
									</li>
								<?php } ?>
								<?php if (isset($filtro->sels)) { ?>
									<?php foreach($filtro->sels as $campo=>$valores) { ?>
										<li class="list-inline-item">
											<strong><?=($filtro->mapa[$campo]->titulocurto)?$filtro->mapa[$campo]->titulocurto:$filtro->mapa[$campo]->titulo?></strong>: <small><?= implode(', ', getNomesItensSelecionados($valores, $filtro->mapa[$campo]->tipo_form, $filtro->opcoes[$campo]))?></small>
										</li>
									<?php } ?>
								<?php } ?>
							</ul>
							<?php } else { ?>
							<strong>Nenhum filtro selecionado</strong>
							<?php } ?>
						</div>
					</div>
		</div>
	</div>
	<?php
}

function getNomesItensSelecionados($valores, $tipo, $opcoes) {
	$valoresNome = array();
	switch($tipo) {
		case 'georref':
			foreach ($valores as $v) {
				$geo = organiza_geos($v);
				$valoresNome[] = estadoParaUF($geo->nome);
			}
			break;
		default:
			foreach ($valores as $v) {
				$valoresNome[] = $opcoes[$v];
			}
			break;
	}
	return $valoresNome;
}

function estadoParaUF($estado) {
	$t = array('acre'=>'ac','alagoas'=>'al','amapá'=>'ap','amazonas'=>'am','bahia'=>'ba','ceará'=>'ce','distrito federal'=>'df','espírito santo'=>'es','goiás'=>'go','maranhão'=>'ma','mato grosso'=>'mt','mato grosso do sul'=>'ms','minas gerais'=>'mg','pará'=>'pa','paraíba'=>'pb','paraná'=>'pr','pernambuco'=>'pe','piauí'=>'pi','rio de janeiro'=>'rj','rio grande do norte'=>'rn','rio grande do sul'=>'rs','rondônia'=>'ro','roraima'=>'rr','santa catarina'=>'sc','são paulo'=>'sp','sergipe'=>'se','tocantins'=>'to');
	$ret =  (isset($t[strtolower($estado)]))
		? strtoupper($t[strtolower($estado)])
		: $estado;
	return $ret;
}

function pega_metadados() {
	global $dir,$txt;
	$queriesRaw = explode('&',$_SERVER['QUERY_STRING']);
	$queries = array();
	foreach ($queriesRaw as $qq) {
		$q = explode('=',$qq);
		$queries[$q[0]]=$q[1];
	}
	$titulo = 'Agroecologia em Rede';
	$titulocurto = 'AeR';
	$descricao = 'O Agroecologia em Rede é um sistema de informações da Agroecologia, vinculado aos processos de construção e articulação do movimento agroecológico nos territórios. Nele você pode encontrar, cadastrar e comentar experiências, grupos, coletivos, redes e pessoas relacionados à Agroecologia, tanto em lista como no mapa. Para facilitar as buscas, é possível filtrar por localização, áreas temáticas, identidades, busca livre, entre outros mecanismos... Tudo de forma leve, simples e acessível, tanto para computadores como celulares e tablets com baixo acesso a internet!';
	$script = str_replace($dir['base'],'',$_SERVER['SCRIPT_FILENAME']);
	$url = $dir['base_URL'].$script."?".$_SERVER['QUERY_STRING'];
	$m = new StdClass();
	$m->url = $url;
	$m->description = $descricao;
	$m->title = $titulo;
	$m->image = $dir['base_URL'].'imagens/logoAeR_400.png';
	switch ($script) {
		case 'experiencias.php':
			if (isset($queries['experiencia']) && $queries['experiencia']) {
				$sql = 'select * from frm_exp_base_comum where ex_id="'.$queries['experiencia'].'"';
				$r = faz_query($sql,'','object')[0];
				if ($r->ex_descricao) {
					$m->title = $titulocurto.': Conheça a experiência "'.strtolower(stripslashes($r->ex_descricao)).'"';
					$m->description = stripslashes(strip_tags(truncate($r->ex_resumo,250)));
					$arqs=array();
					if ($r->ex_anexos) {
					  $anexos = explode('|',$r->ex_anexos);
					  if ($anexos) {
					  	foreach ($anexos as $arq) {
					  		if (file_exists($dir['upload'].$arq) && is_array(getimagesize($dir['upload'].$arq))) {
					  			$arqs[]=$arq;
					  		}
					  	}
					  }
						if (count($arqs)) {
							$arq =  $arqs[0];
							$m->image = $dir['upload'].$arq;
						}
					}
				}
			}
			break;
		case 'instituicoes.php':
			if (isset($queries['inst']) && $queries['inst']) {
				$sql = 'select * from instituicao where in_id="'.$queries['inst'].'"';
				$r = faz_query($sql,'','object')[0];
				if ($r->in_nome) {
					$m->title = $titulocurto.': Conheça a instituição ou grupo '.stripslashes(strip_tags($r->in_nome));
				}
			}
			break;
			case 'usuarios.php':
				if (isset($queries['usu']) && $queries['usu']) {
					$sql = 'select * from usuario where us_login="'.$queries['usu'].'"';
					$r = faz_query($sql,'','object')[0];
					if ($r->us_nome) {
						$m->title = $titulocurto.': Conheça '.stripslashes(strip_tags($r->us_nome));
					}
				}
				break;
		case 'usuarios_busca.php':
			break;
		case 'mapeo/index.php':
			switch($queries['tipofrm']) {
				case 'experiencias':
					break;
				case 'instituicoes':
					break;
			}
			break;
	}
	return $m;
}

?>
