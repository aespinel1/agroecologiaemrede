<?php

require_once "config.php";
mb_internal_encoding("UTF-8");
if (isset($_REQUEST['cadastro']) && $_REQUEST['cadastro']) {
	$aviso='cadastro_sucesso';
} else {
	$aviso = '';
}

//Aqui pego as funções de sessão:
require_once $dir['apoio']."sessao.php";
//Aqui pego o detector de browser:
//require_once $dir['apoio']."classe_detecta_navegador.php";
//Aqui eu recupero arquivos de configuração, funções comuns do programa, funções de acentuação, e funções de Formulário (se estou na área restrita):
require_once $dir['apoio']."funcoes_comuns.php";
//Faço a conexão ao banco de dados:
$db = conecta($bd);

// Aqui verifico se estou em área restrita. Em caso positivo, conecto. Mas se a pessoa estava tentando desconectar, já desconecto aqui mesmo:
if (isset($_COOKIE['cookieUsuario'])) {
	if (!isset($s)) {
		$s = new stdClass();
	}
	$s->u = resgata_dados_usuario();
	// Aqui eu evito da pessoa entrar como admin, pois ela não colocou senha! Se quer entrar como admin, deve entrar apenas lá no /admin, e lá coloca senha e login!
	if (!isset($_COOKIE['cookieAdmin']) || !$_COOKIE['cookieAdmin'])
			unset($s->u->us_admin);
	if (isset($_REQUEST['desconectar']) && $_REQUEST['desconectar']) {
		setcookie("cookieUsuario", "", 0,'/');
		setcookie("cookieSession", "", 0,'/');
		unset($s, $_COOKIE['cookieUsuario'], $_COOKIE['cookieSession']);
	}
}

$loggedIn = isset($s) && isset($s->u) && $s->u;
$isAdmin = $loggedIn && isset($s->u->us_admin) && $s->u->us_admin;

/*
$sql = "SELECT ex_id, CONCAT(ex_instituicao_referencia, '|', ex_instituicoes) as inst, ex_areas_tematicas FROM frm_exp_base_comum";
$dados = faz_query($sql,'','object');
$insts=array();
foreach ($dados as $dado) {
	$insttmps = explode('|',$dado->inst);
	$ats = explode('|',$dado->ex_areas_tematicas);
	foreach($insttmps as $insttmp) {
		if (!$insts[$insttmp])
			$insts[$insttmp]=array();
		$insts[$insttmp]+=$ats;
	}
}
foreach ($insts as $i=>$inst) {
	$insts[$i] = implode('|',array_unique($inst));
	$sql = 'UPDATE frm_instituicao SET in_areas_tematicas="'.$insts[$i].'" WHERE in_id="'.$i.'"';
	echo $sql.'<br>';
	faz_query($sql);
}
exit;
*/
//Aqui eu retiro os slashes de todos os POSTs enviados:
if (get_magic_quotes_gpc() && $_POST) $_POST=stripslashes_array($_POST);
if (get_magic_quotes_gpc() && $_GET) $_GET=stripslashes_array($_GET);

//Aqui eu pego a função passada, se houver. Só se estou na área restrita!
$get_f = (isset($_GET['f'])) ? $_GET['f'] : null;
	$f  = (in_array($get_f, $funcoes_offline) || (isset($s) && $s))
		? $get_f
		: '';
if (!in_array($f,$funcoes_offline) && (!$_COOKIE['cookieUsuario'] || $_COOKIE['cookieUsuario']==''))
	header('Location:'.$dir['base_URL']);



// Status do mostra-lista, se é da lixeira ou das não-apagadas
$publicada = (isset($_GET['publicada'])) ? $_GET['publicada'] : 1;

// Caso esteja na área restrita, carrego a classe de formulários e de ajax dropdown, além de carregar informações passadas pelo GET:
if (isset($s->u) && $s->u) {
	if ($s->u->mapeo_lembranca) $tmp=explode('|',$s->u->mapeo_lembranca);
	$ano		= (isset($_REQUEST['ano'])) ? $_REQUEST['ano'] 	: $tmp[0];
	$lingua 	= (isset($_REQUEST['lingua']))	? $_REQUEST['lingua'] : $tmp[1];
	$id_hidden = (isset($_POST['id_hidden'])) ? $_POST['id_hidden'] : null;
	$id = (isset($_GET['id'])) ? $_GET['id'] : $id_hidden;
	if (isset($_REQUEST['ano']) || isset($_REQUEST['lingua'])) {
		$res=faz_query("UPDATE usuario SET mapeo_lembranca='".$ano."|".$_REQUEST['lingua']."' WHERE us_login='".$s->u->us_login."'");
		$s->u->mapeo_lembranca=$ano.'|'.$lingua;
	}
	// Nome da tabela mysql associada ao formulário
	//$tab=$prefrm.$FrmAno.$pg;
// Fim do if está conectado ($s)... Se não está conectado, e acaba de fazer a desconexão, deve vir o aviso:
} elseif (isset($_POST['desconectar']) && $_POST['desconectar']) $aviso='desconectado';

// Aqui faço o processo de detecção de língua, se a pessoa não está conectada:
/*
// Por enquanto, nesta nova versão, deixarei a detecção de língua inativa: ficará apenas em português.
// TODO: Atualizar detecção de idioma para funcionar em celulares e sistemas modernos (html5)
if (!isset($lingua) || !$lingua) {
	if ((isset($_REQUEST['lingua']) && $_REQUEST['lingua']) || (isset($_COOKIE['agrorede_lingua']) && $_COOKIE['agrorede_lingua']))
		$lingua = (isset($_REQUEST['lingua']) && $_REQUEST['lingua'])
			? $_REQUEST['lingua']
			: $_COOKIE['agrorede_lingua'];
	else {
		//Aqui eu detecto a língua pelo BROWSER:
		require_once $dir['apoio'].'classe_detecta_lingua.php';
		$detectalinguas = new LanguageDetect();
		$linguas = $detectalinguas->getLanguagesList($tab_linguas);
		$lingua = (isset($linguas) && $linguas)
			 ? key($linguas)
			 : $padrao_lingua;
	}
}
*/
$lingua = $padrao_lingua;

//Aqui eu construo a lista de países:
$sql="SELECT id FROM ".$bd['refs'].".__paises WHERE continente='ALC'";
$tmp=faz_query($sql,'','rows');
$paises=$tmp[0];

//Aqui envio as informações de cabeçalho:
header('Content-type: text/html; charset='.$charset);

//Aqui carrego as mensagens na língua $lingua:
include $dir['apoio']."textos/".$lingua.'.php';
setcookie('agrorede_lingua',$lingua,time()+60*60*24*365,'/');

//Aqui carrego as mensagens que vêm do mysql (tabela "texto") e enfio na variável $txt:
$sql = "SELECT tex_id, tex_texto, tex_titulo FROM texto";
$tmp = faz_query($sql, '', 'object_assoc','tex_id');
foreach ($tmp as $n=>$t) {
	$tmp[$n]->tex_texto = traduz($t->tex_texto,$lingua);
	$tmp[$n]->tex_titulo = traduz($t->tex_titulo,$lingua);
}
$txt['mysql']=$tmp;

if (isset($aviso) && $aviso) {
	$aviso=$txt[$aviso];
}
if (isset($msg)) {
	$msg=$txt[$msg];
}
// Se a pessoa vai editar / adicionar um formulário ou acabou de fazê-lo, torna-se necessário executar o script de inicialização, validação e, eventualmente (se a validação der certo) armazenamento dos dados adicionados.
if (in_array($f,array('formulario_geral','editar','consultar'))) {
	require_once $dir['apoio']."forms/forms.php";
	require_once $dir['apoio']."forms/form_dtygel.php";
	$ajax_select=true;
}
if (in_array($f,array('formulario_geral','editar')) || isset($_POST['processe'])) {
	// por enquanto vou deixar explicitamente os 3 iguais ao nome do formulário no mysql. Se esta teoria estiver coreta, vou eliminar 2 variáveis e escolher uma, talvez $tab que é a mais simples...
	$tab = $FrmAno = $nomefrm = $_REQUEST['nomefrm'];
	$tmp=faz_query('SELECT nmaxpgs, titulo, titulocurto, tab_base_comum, usuarias_administradoras FROM mapeo_formularios WHERE ano="'.$ano.'" AND nomefrm="'.$nomefrm.'"', '', 'array');
	$tab_base_comum = $tmp['tab_base_comum'][0];
	$pg=$nmaxpgs=$tmp['nmaxpgs'][0];
	$tituloform=traduz($tmp['titulo'][0],$lingua);
	$titulocurtoform=traduz($tmp['titulocurto'][0],$lingua);
	// Aqui eu pego as informações dos admins deste formulário:
	$tmp_admins=explode('|',$tmp['usuarias_administradoras'][0]);
	$sql = 'SELECT us_nome, us_email FROM usuario WHERE ';
	foreach ($tmp_admins as $t)
		$sql .= 'us_login LIKE "'.$t.'" OR ';
	$sql = substr($sql,0,-4);
	$admins=faz_query($sql,'','object');
	// Esta função retorna o valor do $mapa e do $cmd!
	$mapa=gera_mapa_campos($ano,$nomefrm,$tab_base_comum,$cmd);
	if ($_POST['processe']) {
		include $dir['apoio'].'processa_formulario.php';
		if (!$erro)
			$f='lista_formularios';
	}
	if (!$_POST['processe'] || $erro)
		include $dir['apoio']."inicializa_formulario.php";
}


include $dir['cont']."cabecalho.php";

/*
if ($aviso) {
	ob_start ();
	?>
	<script language="JavaScript" type="text/JavaScript">
	<!--
		alert("<?=addslashes($aviso)?>");
	//-->
	</script>
	<?php
	$ret=ob_get_contents ();
	ob_end_clean ();
	echo $ret;
	unset($aviso);
} */
$containerClass = ($f=='consultar')
	? 'container-fluid'
	: 'container';
?>
<!-- Seção principal //-->
<div id="" class="<?=$containerClass?><?=(MAPA)?' h-100':''?>">
	<?php
	// Aqui eu decido o que vai na coluna de conteúdo principal, dependendo das funções e de estar logado ou não:
	if (!isset($s) || !$s) {
		switch ($f) {
			case 'estatisticas':
				include $dir['cont']."estatisticas.php";
			break;
			case 'consultar':
				include $dir['cont']."mapas_bootstrap.php";
			break;
			case 'sobre':
			default:
				include $dir['cont']."pagina_inicial.php";
			break;
		}
	} else {
		switch ($f) {
			case 'estatisticas':
				include $dir['cont']."estatisticas.php";
			break;
			case 'editar':
			case 'formulario_geral':
				include $dir['cont']."edita_formulario.php";
			break;
			case 'apagar':
			case 'recuperar':
			case 'salvar':
			case 'esvaziar_lixeira':
				include $dir['apoio']."processamento.php";
				include $dir['cont']."lista_formularios.php";
			break;
			case 'previa_frm_instituicao':
				include $dir['cont']."previa_frm_instituicao.php";
			break;
			case 'ajuda':
				include $dir['cont']."ajuda.php";
			break;
			case 'lista_formularios':
				include $dir['cont']."lista_formularios.php";
			break;
			case 'consultar':
				include $dir['cont']."mapas_bootstrap.php";
			break;
			default:
				include $dir['cont']."pagina_inicial.php";
			break;
		}
	}
	?>
<?php $db->close(); ?>
</div> <!-- div container -->
</body>
</html>
