DROP TABLE IF EXISTS `lista_conceitos`;
CREATE TABLE `lista_conceitos` (
  `id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO lista_conceitos SET id=1, nome='Ruim';
INSERT INTO lista_conceitos SET id=2, nome='Razo�vel';
INSERT INTO lista_conceitos SET id=3, nome='Bom';
INSERT INTO lista_conceitos SET id=4, nome='�timo';
INSERT INTO lista_conceitos SET id=5, nome='N�o sei avaliar';

-----------------------------------------------------------------------------------------------------

-- Se � classe, n�o coloco o item. Os valores ser�o classe-item, sendo classe de 2 d�gitos e item de 2 d�gitos tb.
DROP TABLE IF EXISTS `lista_CNPS`;
DROP TABLE IF EXISTS `lista_CNPS_classes`;
CREATE TABLE `lista_CNPS` (
  `id` varchar(5) NOT NULL,  
  `nome` varchar(255) NOT NULL,
  `descricao` text not null,
PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO lista_CNPS SET id='01', nome='Produtos e subst�ncias qu�micas e minerais e aqueles de origem animal ou vegetal, predominantemente destinados ao uso industrial', descricao='Contrap�e-se, portanto, aos produtos destinados ao uso final, com exce��o daqueles inclu�dos nos itens 01.40 e 01.45. Em geral, os produtos e subst�ncias deste item dependem ainda de algum tipo de tratamento para seu consumo final, podendo ent�o estar inclu�dos em outros itens previstos nas demais classes.';
INSERT INTO lista_CNPS SET id='01-10', nome='Resinas em geral', descricao='';

-----------------------------------------------------------------------------------------------------

