<?php

$menuConsultas = array();
$i=0;
$menuConsultas[$i] = new StdClass();
$menuConsultas[$i]->url = "mapeo/index.php?f=consultar&tipofrm=experiencia";
$menuConsultas[$i]->title = $txt['menu_consultar_experiencia'];
/*$i++;
$menuConsultas[$i] = new StdClass();
$menuConsultas[$i]->url = "banco_pesquisas.php";
$menuConsultas[$i]->title = $txt['menu_consultar_pesquisa'];*/
$i++;
$menuConsultas[$i] = new StdClass();
$menuConsultas[$i]->url = "usuarios.php";
$menuConsultas[$i]->title = $txt['menu_consultar_pessoa'];
$i++;
$menuConsultas[$i] = new StdClass();
$menuConsultas[$i]->url = "mapeo/index.php?f=consultar&tipofrm=instituicao";
$menuConsultas[$i]->title = $txt['menu_consultar_instituicao'];

$menuCadastros = array();
$i=0;
$menuCadastros[$i] = new StdClass();
$menuCadastros[$i]->url = "mapeo/index.php?f=previa_frm_instituicao";
$menuCadastros[$i]->title = $txt['menu_cadastrar_instituicao'];
$i++;
$menuCadastros[$i] = new StdClass();
$menuCadastros[$i]->url = "mapeo/index.php?f=formulario_geral&nomefrm=frm_exp_geral";
$menuCadastros[$i]->title = $txt['menu_cadastrar_experiencia'];

$menuAlteracoes = array();
$i=0;
$menuAlteracoes[$i] = new StdClass();
$menuAlteracoes[$i]->url = "mapeo/index.php?f=lista_formularios&nomefrm=frm_instituicao";
$menuAlteracoes[$i]->title = $txt['menu_alterar_instituicao'];
$i++;
$menuAlteracoes[$i] = new StdClass();
$menuAlteracoes[$i]->url = "mapeo/index.php?f=lista_formularios&nomefrm=frm_exp_geral";
$menuAlteracoes[$i]->title = $txt['menu_cadastrar_experiencia_geral'];

$menuIdiomas = array();
$i=0;
$menuIdiomas[$i] = new StdClass();
$menuIdiomas[$i]->url = '?lingua=es';
$menuIdiomas[$i]->title = "Castellano";
$i++;
$menuIdiomas[$i] = new StdClass();
$menuIdiomas[$i]->url = '?lingua=pt-br';
$menuIdiomas[$i]->title = "Português";

$menuExperiencias = array();
$i=0;
$menuExperiencias[$i] = new StdClass();
$menuExperiencias[$i]->title = 'Cadastrar';
$menuExperiencias[$i]->url = "mapeo/index.php?f=formulario_geral&nomefrm=frm_exp_geral";
$i++;
$menuExperiencias[$i] = new StdClass();
$menuExperiencias[$i]->title = 'Analisar e alterar';
$menuExperiencias[$i]->url = "mapeo/index.php?f=lista_formularios&nomefrm=frm_exp_geral";
$i++;
$menuExperiencias[$i] = new StdClass();
$menuExperiencias[$i]->title = 'Comentários - análise';
$menuExperiencias[$i]->url = "admin/experiencias_libera_comentario.php";
$i++;
$menuExperiencias[$i] = new StdClass();
$menuExperiencias[$i]->title = 'Comentários - cancelar liberação/rejeição';
$menuExperiencias[$i]->url = "admin/experiencias_cancela_liberacao_comentario.php";

$menuPesquisas = array();
$i=0;
$menuPesquisas[$i] = new StdClass();
$menuPesquisas[$i]->title = 'Cadastrar';
$menuPesquisas[$i]->url = "admin/pesquisas_cadastro.php";
$i++;
$menuPesquisas[$i] = new StdClass();
$menuPesquisas[$i]->title = 'Alterar';
$menuPesquisas[$i]->url = "admin/pesquisas_altera.php";
$i++;
$menuPesquisas[$i] = new StdClass();
$menuPesquisas[$i]->title = 'Analisar';
$menuPesquisas[$i]->url = "admin/pesquisas_libera.php";
$i++;
$menuPesquisas[$i] = new StdClass();
$menuPesquisas[$i]->title = 'Cancelar liberação/rejeição';
$menuPesquisas[$i]->url = "admin/pesquisas_cancela_liberacao.php";
$i++;
$menuPesquisas[$i] = new StdClass();
$menuPesquisas[$i]->title = 'Excluir';
$menuPesquisas[$i]->url = "admin/pesquisas_excluir.php";
$i++;
$menuPesquisas[$i] = new StdClass();
$menuPesquisas[$i]->title = 'Anexos';
$menuPesquisas[$i]->url = "admin/pesquisas_arquivos.php";
$i++;
$menuPesquisas[$i] = new StdClass();
$menuPesquisas[$i]->title = 'Comentários - analisar';
$menuPesquisas[$i]->url = "admin/pesquisa_libera_comentario.php";
$i++;
$menuPesquisas[$i] = new StdClass();
$menuPesquisas[$i]->title = 'Comentários - cancelar liberação/rejeição';
$menuPesquisas[$i]->url = "admin/pesquisa_cancela_liberacao_comentario.php";

$menuPessoas = array();
$i=0;
$menuPessoas[$i] = new StdClass();
$menuPessoas[$i]->title = 'Cadastrar';
$menuPessoas[$i]->url = "admin/usuarios.php";
$i++;
$menuPessoas[$i] = new StdClass();
$menuPessoas[$i]->title = 'Alterar';
$menuPessoas[$i]->url = "admin/usuarios_cadastro.php";
$i++;
$menuPessoas[$i] = new StdClass();
$menuPessoas[$i]->title = 'Liberar';
$menuPessoas[$i]->url = "admin/usuarios_liberacao.php";
$i++;
$menuPessoas[$i] = new StdClass();
$menuPessoas[$i]->title = 'Cancelar liberação/rejeição';
$menuPessoas[$i]->url = "admin/usuarios_liberacao_cancela.php";
$i++;
$menuPessoas[$i] = new StdClass();
$menuPessoas[$i]->title = 'Excluir';
$menuPessoas[$i]->url = "admin/usuarios_exclusao.php";

$menuInstituicoes = array();
$i=0;
$menuInstituicoes[$i] = new StdClass();
$menuInstituicoes[$i]->title = 'Cadastrar';
$menuInstituicoes[$i]->url = "mapeo/index.php?f=formulario_geral&nomefrm=frm_instituicao";
$i++;
$menuInstituicoes[$i] = new StdClass();
$menuInstituicoes[$i]->title = 'Alterar e excluir';
$menuInstituicoes[$i]->url = "mapeo/index.php?f=lista_formularios&nomefrm=frm_instituicao";
$i++;
$menuInstituicoes[$i] = new StdClass();
$menuInstituicoes[$i]->title = 'Liberar';
$menuInstituicoes[$i]->url = "admin/instituicoes_liberacao.php";
$i++;
$menuInstituicoes[$i] = new StdClass();
$menuInstituicoes[$i]->title = 'Cancelar liberação/rejeição';
$menuInstituicoes[$i]->url = "admin/instituicoes_liberacao_cancela.php";

$menuLinks = array();
$i=0;
$menuLinks[$i] = new StdClass();
$menuLinks[$i]->title = 'Gerenciamento';
$menuLinks[$i]->url = "admin/links.php";
$i++;
$menuLinks[$i] = new StdClass();
$menuLinks[$i]->title = 'Categorias';
$menuLinks[$i]->url = "admin/tabela_simples.php?tipoTabela=LK";

$menuTabelas = array();
$i=0;
$menuTabelas[$i] = new StdClass();
$menuTabelas[$i]->title = 'Áreas Temáticas';
$menuTabelas[$i]->url = "admin/tabela_complexa.php?tipo=AT";
$i++;
$menuTabelas[$i] = new StdClass();
$menuTabelas[$i]->title = 'Biomas';
$menuTabelas[$i]->url = "admin/tabela_complexa.php?tipo=HA";
$i++;
$menuTabelas[$i] = new StdClass();
$menuTabelas[$i]->title = 'Idiomas';
$menuTabelas[$i]->url = "admin/tabela_simples.php?tipoTabela=ID";
$i++;
$menuTabelas[$i] = new StdClass();
$menuTabelas[$i]->title = 'Tipos de bibliografias';
$menuTabelas[$i]->url = "admin/tabela_simples.php?tipoTabela=TB";
$i++;
$menuTabelas[$i] = new StdClass();
$menuTabelas[$i]->title = 'Tipos de instituições';
$menuTabelas[$i]->url = "admin/tabela_simples.php?tipoTabela=TI";
$i++;
$menuTabelas[$i] = new StdClass();
$menuTabelas[$i]->title = 'Identidades';
$menuTabelas[$i]->url = "admin/tabela_simples.php?tipoTabela=lista_identidades";
$i++;
$menuTabelas[$i] = new StdClass();
$menuTabelas[$i]->title = 'Níveis educacionais';
$menuTabelas[$i]->url = "admin/tabela_simples.php?tipoTabela=lista_nivel_educacional";
$i++;
$menuTabelas[$i] = new StdClass();
$menuTabelas[$i]->title = 'Regimes de ensino';
$menuTabelas[$i]->url = "admin/tabela_simples.php?tipoTabela=lista_regime_ensino";

$menuTextos = array();
$i=0;
$menuTextos[$i] = new StdClass();
$menuTextos[$i]->title = 'Textos e parâmetros do sistema';
$menuTextos[$i]->url = "admin/textos.php";
$i++;
$menuTextos[$i] = new StdClass();
$menuTextos[$i]->title = 'Formulário de edição e cadastro de experiências';
$menuTextos[$i]->url = "admin/tabela_complexa2.php?tipo=mapeo_mapa_campos&frm=frm_exp_geral";
$i++;
$menuTextos[$i] = new StdClass();
$menuTextos[$i]->title = 'Formulário de edição e cadastro de instituições';
$menuTextos[$i]->url = "admin/tabela_complexa2.php?tipo=mapeo_mapa_campos&frm=frm_instituicao";

$menuRelatorios = array();
$i=0;
$menuRelatorios[$i] = new StdClass();
$menuRelatorios[$i]->title = 'Buscas mais realizadas';
$menuRelatorios[$i]->url = "admin/rel_mais_procurados.php";
$i++;
$menuRelatorios[$i] = new StdClass();
$menuRelatorios[$i]->title = 'Lista de endereços - instituições';
$menuRelatorios[$i]->url = "admin/rel_enderecos_instituicoes.php";
$i++;
$menuRelatorios[$i] = new StdClass();
$menuRelatorios[$i]->title = 'Lista de endereços - pessoas';
$menuRelatorios[$i]->url = "admin/rel_enderecos_pessoas.php";
$i++;
$menuRelatorios[$i] = new StdClass();
$menuRelatorios[$i]->title = 'Cadastro de pesquisas por data';
$menuRelatorios[$i]->url = "admin/rel_pesquisa_data.php";
$i++;
$menuRelatorios[$i] = new StdClass();
$menuRelatorios[$i]->title = 'Cadastro de experiências por data';
$menuRelatorios[$i]->url = "admin/rel_experiencia_data.php";
$i++;
$menuRelatorios[$i] = new StdClass();
$menuRelatorios[$i]->title = 'Cadastro de pessoas por data';
$menuRelatorios[$i]->url = "admin/rel_pessoa_data.php";
$i++;
$menuRelatorios[$i] = new StdClass();
$menuRelatorios[$i]->title = 'Evolução';
$menuRelatorios[$i]->url = "admin/rel_evolucao.php";
$i++;
$menuRelatorios[$i] = new StdClass();
$menuRelatorios[$i]->title = 'Estatísticas de acesso';
$menuRelatorios[$i]->url = $dir['estatisticas_URL'];

// ----------------------------------
// ----------------------------------
// ----------------------------------

$menu = array();

if (!$loggedIn) {
  $i = 0;
  $menu[$i] = new StdClass();
  $menu[$i]->title = $txt['menu_inicio'];
  $menu[$i]->url = "index.php";
  $menu[$i]->active = strpos($_SERVER['PHP_SELF'],"agroecologiaemrede/index.php")>0;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = $txt['menu_consultar'];
  $menu[$i]->url = "";
  $menu[$i]->active = strpos($_SERVER['PHP_SELF'],"mapeo/index.php")>0;
  $menu[$i]->submenus = $menuConsultas;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = $txt['menu_cadastrar'];
  $menu[$i]->url = "login.php";
  $menu[$i]->active = strpos($_SERVER['PHP_SELF'],$menu[$i]->url)>0;
  /*$i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = $txt['lingua'];
  $menu[$i]->url = "";
  $menu[$i]->submenus = $menuIdiomas;*/
  /*$i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = $txt['menu_fale_conosco'];
  $menu[$i]->url = "contato.php";
  $menu[$i]->active = strpos($_SERVER['PHP_SELF'],$menu[$i]->url)>0;*/
} else if ($isAdmin) {
  $i = 0;
  $menu[$i] = new StdClass();
  $menu[$i]->title = "Principal";
  $menu[$i]->url = "admin/main.php";
  $menu[$i]->active = strpos($_SERVER['PHP_SELF'],'admin/'.$menu[$i]->url)>0;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = "Experiências";
  $menu[$i]->url = "";
  $menu[$i]->active = false; // TODO !!
  $menu[$i]->submenus = $menuExperiencias;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = "Pesquisas";
  $menu[$i]->url = "";
  $menu[$i]->active = strpos($_SERVER['PHP_SELF'],'admin/pesquisa')>0;
  $menu[$i]->submenus = $menuPesquisas;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = "Pessoas";
  $menu[$i]->url = "";
  $menu[$i]->active = strpos($_SERVER['PHP_SELF'],'admin/usuarios')>0;
  $menu[$i]->submenus = $menuPessoas;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = "Instituições";
  $menu[$i]->url = "";
  $menu[$i]->active = false; // TODO !!
  $menu[$i]->submenus = $menuInstituicoes;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = "Links";
  $menu[$i]->url = "";
  $menu[$i]->submenus = $menuLinks;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = "Relatórios";
  $menu[$i]->url = "";
  $menu[$i]->active = false; // TODO !!
  $menu[$i]->submenus = $menuRelatorios;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = "Tabelas";
  $menu[$i]->url = "";
  $menu[$i]->active = false; // TODO !!
  $menu[$i]->submenus = $menuTabelas;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->title = "Textos";
  $menu[$i]->url = "";
  $menu[$i]->active = false; // TODO !!
  $menu[$i]->submenus = $menuTextos;
} else {
  $i = 0;
  $menu[$i] = new StdClass();
  $menu[$i]->url = "mapeo/index.php";
  $menu[$i]->active = strpos($_SERVER['PHP_SELF'],$menu[$i]->url)>0;
  $menu[$i]->title = $txt['menu_inicio'];
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->url = "";
  $menu[$i]->active = false; // TODO !!
  $menu[$i]->title = $txt['menu_consultar'];
  $menu[$i]->submenus = $menuConsultas;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->url = "";
  $menu[$i]->active = false; // TODO !!
  $menu[$i]->title = $txt['menu_cadastrar'];
  $menu[$i]->submenus = $menuCadastros;
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->url = "";
  $menu[$i]->active = false; // TODO !!
  $menu[$i]->title = $txt['menu_alterar'];
  $menu[$i]->submenus = $menuAlteracoes;
  /*$i++;
  $menu[$i] = new StdClass();
  $menu[$i]->url = "";
  $menu[$i]->title = $txt['lingua'];
  $menu[$i]->submenus = $menuIdiomas;*/
  $i++;
  $menu[$i] = new StdClass();
  $menu[$i]->url = "documentacao/manual_agroecologia_em_rede_set2009.pdf";
  $menu[$i]->active = false;
  $menu[$i]->title = $txt['menu_ajuda'];
  /*$i++;
  $menu[$i] = new StdClass();
  $menu[$i]->url = "contato.php";
  $menu[$i]->active = strpos($_SERVER['PHP_SELF'],$menu[$i]->url)>0;
  $menu[$i]->title = $txt['menu_fale_conosco'];*/
}

?>
