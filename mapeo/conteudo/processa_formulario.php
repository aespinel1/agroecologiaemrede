<?php

	$frm->StartLayoutCapture();
	
	//Se houve erro de validação, deve ser mostrado o erro
	if($error_message)
	{
 		$active = 0;
 		?>
 		<p class="alerta"><?=$txt['alerta_erro_validacao']?><br>
 		<i><?=$error_message?></i>
 		</p> 		
<?php
	}
?>
		<h1><?=$titulo_form?></h1>
		<div class="conteudo">
		<p><?=$txt['intro_edicao_form']?></p> 
		</div>
		<?php include "tema_formulario_html.php"; ?>
		<?php $frm->AddInputPart('processe'); ?>
<?php
	$frm->EndLayoutCapture();

/*
 * Output the form using the function named Output.
 */
	$frm->DisplayOutput();
?>
