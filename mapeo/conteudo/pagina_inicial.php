<?php
if ($s) {
	$is_admin = (isset($s->u) && isset($s->u->us_admin))
		? $s->u->us_admin
		: 0;
	?>
	<div class="row">
		<div class="col-sm-12 col-md-10 col-lg-8 mx-auto">
			<h1><?=$txt['area_restrita']?> - <?=$s->u->us_nome?></h1>
			<div class="conteudo">
				<?php if ($aviso) {
					?>
					<p class="aviso"><?=$aviso?></p>
					<?php
					unset($aviso);
				}
				?>
				<p><?=$txt['boas_vindas']?></p>
				<h2><?=($is_admin) ? $txt['tit_lista_cadastros_admin'] : $txt['tit_lista_cadastros']?></h2>
				<ul>
				<?php
				$sql = "SELECT nomefrm, titulo, tab_base_comum FROM mapeo_formularios WHERE ano='".$ano."' AND admin<=".intval($is_admin)." ORDER BY ord";
				$frms = faz_query($sql,'','object');
				$i=0;
				foreach ($frms as $frm) {
					$sql = ($frm->tab_base_comum)
						? "SELECT a.* FROM ".$frm->nomefrm." AS a, ".$frm->tab_base_comum." AS b WHERE a.".$campo[$frm->nomefrm]->id."=b.".$campo[$frm->tab_base_comum]->id
						: "SELECT * FROM ".$frm->nomefrm." WHERE 1";
					if (!$is_admin)
						$sql .= ($frm->tab_base_comum)
							? " AND b.".$campo[$frm->tab_base_comum]->digitadora." LIKE '".$s->u->us_login."'"
							: " AND ".$campo[$frm->nomefrm]->digitadora." LIKE '".$s->u->us_login."'";
					$num=faz_query($sql,'','num');
					if ($num) {
						?>
						<li><a href="<?=$dir['base_URL']?>mapeo/?f=lista_formularios&nomefrm=<?=$frm->nomefrm?>"><?=traduz($frm->titulo,$lingua)?></a>: <?=$num?></li>
						<?php
						$i++;
					}
				}
			?>
				</ul>
				<?php
				if (!$i)
					echo '<p class="aviso">'.$txt['sem_questionarios'].'</p>';
				?>
			</div>
		</div>
	</div>
<?php
} else {
?>
	<div class="row">
		<div class="col-sm-12 col-md-10 col-lg-8 mx-auto">
			<h1><?=$txt['inicio_titulo']?></h1>
			<div class="conteudo">
				<?=$txt['inicio_apresentacao']?>
			</div>
			<H1><?=$txt['ajuda_titulo']?></H1>
			<div class="conteudo">
				<?=$txt['ajuda_texto']?>
			</div>
		</div>
	</div>
<?php
}
?>
