<?php
//por daniel tygel (dtygel em fbes pt org pt br) em junho de 2009
foreach ($_REQUEST as $i=>$b)
	if ($b=='-9999')
		unset($_REQUEST[$i]);
$mostratabela = $_REQUEST['mostratabela'];
$pg_lista = ($_REQUEST['pgLista'])
	? $_REQUEST['pgLista']
	: 1;
$tipofrm=($_REQUEST['tipofrm'])
	? $_REQUEST['tipofrm']
	: 'experiencia';
if (!$_REQUEST['limpar'])
	$c = $_REQUEST['c'];
else {
	$c='';
	unset($_REQUEST);
}
$filtro = resgata_filtros($tipofrm);
$urls = resgata_urls(array('agrorede'=>'mapas/gera_xml_agrorede.php'));
?>
<script type="text/javascript" src="<?=$dir['apoio_URL']?>json_min.js"></script>
<center>
<table class="fareja_table" id="tablemap">
<tr>
<td id="tdPainel"><?=mostraPainel()?></td>
<td>
<?php
if ($c) {
	// Caso tenha havido consulta (c=1), já tem que se mostrar resultados!
	if (!$mostratabela)
		$compl = '&conta=1';
	foreach ($urls as $tipo=>$u) {
		if ($xmltmp=pega_xml_arquivo($u->url.$u->dados_get.$compl))
			$xml[]=$xmltmp;
			else unset($ativ[$tipo]);
	}
	// Aqui eu recolho a quantidade de elementos passados pelo arquivo XML:
	if ($xml) {
		foreach ($xml as $xtmp) {
			$numpts+= ($mostratabela)
				? $xtmp->num['num']
				: $xtmp->markers->pt['num'];
			// E aqui eu recolho as categorias (para checkboxes dinâmicos), caso existam:
			if ($xtmp->config['cats']) {
				$tmp=explode('||',$xtmp->config['cats']);
				foreach ($tmp as $tm) {
					$tmp2=explode('|',$tm);
					$ativ[$tmp2[0]]=$tmp2[1];
				}
			}
			$numpgs = $xtmp->num['pgs'];
		}
	}
	if ($numpts) {
		if (!$mostratabela) {
			mostraMapa($numpts,$urls,$ativ);
			?>
			<div id="carregando_mapa"></div>
			<div id="legendaMapa"><?=mostraLegenda($filtro)?></div>
			<?php
		} else {
			mostraTabela($xml);
		} // fim do else do if $mapa
	} else {
		?>
		<h3><?=$txt['alerta_nada_encontrado']?></h3>
		<?php
	}
} else {
	// Aqui é para o caso de não ter havido consulta (c não foi definido):
	?>
	<script language="JavaScript" type="text/javascript">
		<!--
		document.getElementById('filtros').style.visibility='visible';
		-->
	</script>
	<center>
	<div id="aberturaConsultaMapas">
		<h1><?=str_replace('{tipo}',$txt['titulo_filtros_'.$tipofrm],$txt['mysql']['ABERTURA_CONSULTA_MAPAS']->tex_titulo)?></h1>
		<?=$txt['mysql']['ABERTURA_CONSULTA_MAPAS']->tex_texto?>
	</div>
	</center>
	<?php
}

?>
</td></tr>
</table>
<script language="JavaScript" type="text/javascript">
	<!--
		var w=window.size();
		if (typeof map != 'undefined') {
			document.getElementById('map').style.width=w.width-284;
			document.getElementById('map').style.height=w.height-110;
		}
		document.getElementById('tablemap').style.width=w.width-34;
		document.getElementById('tablemap').style.height=w.height-110;
	//-->
</script>
<?php
if (!$c || $mostratabela) {
	foreach ($urls as $g)
			$urltxt.="'".stripslashes($g->url)."', ";
	$urltxt=substr($urltxt,0,-2);
	?>
	<script language="JavaScript" type="text/javascript">
		<!--
		var urls=Array(<?=$urltxt?>);
		//-->
	</script>
	<?php
}

function resgata_urls ($fontes) {
	global $dir,$mostratabela,$tipofrm,$pg_lista;
	$dados_get='?c=1&tipofrm='.$tipofrm;
	if ($mostratabela) {
		$dados_get.='&mostratabela=1';
		if ($pg_lista)
			$dados_get.='&pgLista='.$pg_lista;
	}
	$filtro = resgata_filtros($tipofrm);
	if (isset($filtro->textoBusca) && $filtro->textoBusca)
		$dados_get.='&textoBusca='.urlencode($filtro->textoBusca);
	if (isset($filtro->sels) && $filtro->sels) {
		foreach($filtro->sels as $cmpo=>$valores) {
			if (is_array($valores))
				$valores = implode('|',$valores);
			$dados_get .= '&'.$cmpo.'='.urlencode($valores);
		}
	}
	foreach ($fontes as $tipo=>$ftmp) {
		$fonte_ext = ($fontes_ext[$tipo])
			? '&fonte='.$fontes_ext[$tipo]
			: '';
		$urls[$tipo]->url = $dir['base_URL'].$ftmp;
		$urls[$tipo]->dados_get = $dados_get.$fonte_ext;
	}
	return $urls;
}
function mostraLegenda($filtro) {
	global $txt, $tipofrm, $numpts;
	$frm = new form_dtygel_class;
	$frm->rodaMapaCampos($filtro->mapa,$filtro->cmd,$filtro->sels);
	?>
	<h5><span style="float:left"><b>agroecologiaemrede.org.br</b></span><?=date('d/m/Y')?></h5>
	<h4><?=$txt['titulo_legenda_'.$tipofrm]?><br />
	<span class="peq"><?=str_replace('{pts}','<span id="span_legendaMapaSels">'.$numpts.'</span>',$txt['mapa_itens_encontrados'])?></span></h4>
	<div id="legendaMapaSels">
	<?php
	foreach ($filtro->filtro as $cmpo) {
		$m = $filtro->mapa[$cmpo];
		$cmdo = $filtro->cmd[$m->campo];
		$cmdo_mapa = $filtro->cmd_mapa[$m->campo];
		$opcoes = $filtro->opcoes[$m->campo];
		$sels = $filtro->sels[$m->campo];
		if ($sels) {
			?>
			<p><b><?=$m->titulo?>:</b> 
			<?php
			unset($html);
			if (in_array($m->tipo_form,array('select_hierarquia','georref'))) {
				// Caso dos selects:
				if (is_array($sels)) {
					foreach ($sels as $nivel=>$sel) {
						if ($frm->inputs[$m->campo.'_'.$nivel]['OPTIONS'][$sel]) {
							$html[] = $frm->inputs[$m->campo.'_'.$nivel]['OPTIONS'][$sel];
						} else {
							$html[] = $frm->inputs[$m->campo]['OPTIONS'][$sel];
						}
					}
					echo implode(', ',$html);
				} else
					echo $frm->inputs[$m->campo]['OPTIONS'][$sel];
			} else {
				foreach ($sels as $sel) {
					// Caso dos checkboxes:
					$html[] = $opcoes[$sel];
				}
				echo implode('; ',$html);
			}
			?></p><?php
		}
	}
	if (isset($filtro->textoBusca) && $filtro->textoBusca) {
		?>
		<p><b><?=$txt['textoBusca']?>:</b> "<?=$filtro->textoBusca?>"</p>
		<?php
	}
	?>
	</div>
	<?php
}
// Inseri um lance para poder exibir o painel com controles a partir dos formulários. Não sei se está no melhor lugar, mas enfim...
function mostraPainel() {
	global $c,$tipofrm, $filtro, $txt, $urls, $mostratabela, $geoXml;
	$frm = new form_dtygel_class;
	// Faço as definições iniciais do formulário:
	$frm->NAME="painel_mapas";
	$frm->METHOD="GET";
	$frm->ACTION="index.php";
	$frm->debug="trigger_error"; //Esse é pra eu debugar, enquanto programo
	$frm->ResubmitConfirmMessage=$msg['resubmissao_de_mesmos_dados'];
	$frm->OutputPasswordValues=0; //ver manual sobre isso...
	$frm->OptionsSeparator="<br />\n";
	$frm->ShowAllErrors=0;
	$frm->InvalidCLASS='invalido';
	$frm->encoding='utf-8';
	$frm->ErrorMessagePrefix="-> ";
	$frm->ErrorMessageSuffix="";
	$frm->AddInput(array("TYPE"=>"hidden","NAME"=>"c","VALUE"=>1));
	$frm->AddInput(array("TYPE"=>"hidden","NAME"=>"f","VALUE"=>"consultar"));
	$frm->AddInput(array("TYPE"=>"hidden","ID"=>"tipofrm","NAME"=>"tipofrm","VALUE"=>$tipofrm));
	if ($c) {
		// Por causa dos clusters, é melhor reinicializar o mapa a cada busca. portanto, estou comentando o item abaixo e inserindo o que vem em seguida, de type=submit.
		//$frm->AddInput(array("TYPE"=>"button","ID"=>"buscar","NAME"=>"buscar","VALUE"=>"buscar","ONCLICK"=>"resgataPontos('painelPreResultados',false,true,'legendaMapaSels')","ONMOUSEOVER"=>"entrouMousePainel('')"));
		$frm->AddInput(array("TYPE"=>"submit","ID"=>"buscar","NAME"=>"buscar","VALUE"=>"buscar","ONMOUSEOVER"=>"entrouMousePainel('')"));
		$frm->AddInput(array("TYPE"=>"submit","ID"=>"limpar","NAME"=>"limpar","VALUE"=>$txt['painel_limpar_selecao'],"ONMOUSEOVER"=>"entrouMousePainel('')"));
	} else 
		$frm->AddInput(array("TYPE"=>"submit","ID"=>"buscar","NAME"=>"buscar","VALUE"=>"buscar","ONMOUSEOVER"=>"entrouMousePainel('')"));
	// Item do formulário para definir se o resultado sai em mapa ou em lista:
	$frm->AddInput(array("TYPE"=>"radio","ID"=>"mostratabela","NAME"=>"mostratabela","VALUE"=>1,"CHECKED"=>($mostratabela==1)));
	$frm->AddInput(array("TYPE"=>"radio","ID"=>"mostramapa","NAME"=>"mostratabela","VALUE"=>0, "CHECKED"=>(!$mostratabela)));
	$frm->rodaMapaCampos($filtro->mapa,$filtro->cmd,$filtro->sels);
	$frm->StartLayoutCapture();
	// Construção do título maior, e estrutura de DIVs
	$htmlsel = '<div id="selecionadas">';
	?>
	<div id="filtros">
	<h1><?=$txt['titulo_filtros_'.$tipofrm]?></h1>
	<?php
	$frm->AddInputPart("c");
	$frm->AddInputPart("f");
	$frm->AddInputPart("tipofrm");
	foreach ($filtro->filtro as $cmpo) {
		$m = $filtro->mapa[$cmpo];
		$n = count($filtrosJS);
		$filtrosJS[$n]->idCampo=$m->campo;
		$filtrosJS[$n]->nome = ($m->titulocurto) ? $m->titulocurto : $m->titulo;
		$cmdo = $filtro->cmd[$m->campo];
		$cmdo_mapa = $filtro->cmd_mapa[$m->campo];
		$opcoes = $filtro->opcoes[$m->campo];
		$sels = $filtro->sels[$m->campo];
		
		if ($m->titulocurto)
			$m->titulo=$m->titulocurto;
		if ($sels) {
			$filtrosJS[$n]->sels = array();
			if (in_array($m->tipo_form,array('select_hierarquia','georref'))) {
				// Caso dos selects:
				if (is_array($sels)) {
					foreach ($sels as $nivel=>$sel) {
						$j = count($filtrosJS[$n]->sels);
						if ($frm->inputs[$m->campo.'_'.$nivel]['OPTIONS'][$sel]) {
							$filtrosJS[$n]->sels[$j]->id = $m->campo.'_'.$nivel;
							$filtrosJS[$n]->sels[$j]->nome = $frm->inputs[$m->campo.'_'.$nivel]['OPTIONS'][$sel];
						} else {
							$filtrosJS[$n]->sels[$j]->id = $m->campo;
							$filtrosJS[$n]->sels[$j]->nome = $frm->inputs[$m->campo]['OPTIONS'][$sel];
						}
						$filtrosJS[$n]->sels[$j]->valor = $sel;
						$filtrosJS[$n]->sels[$j]->select = 1;
					}
				} else {
					$j = count($filtrosJS[$n]->sels);
					$filtrosJS[$n]->sels[$j]->id = $m->campo;
					$filtrosJS[$n]->sels[$j]->nome = $frm->inputs[$m->campo]['OPTIONS'][$sel];
					$filtrosJS[$n]->sels[$j]->valor = $sel;
					$filtrosJS[$n]->sels[$j]->select=1;
				}
			} else foreach ($sels as $sel) {
				// Caso dos checkboxes:
				$j = count($filtrosJS[$n]->sels);
				$filtrosJS[$n]->sels[$j]->id = $m->campo.'_'.$sel;
				$filtrosJS[$n]->sels[$j]->nome = $opcoes[$sel];
				$filtrosJS[$n]->sels[$j]->valor = $sel;
			}
		}
		?>
		<?php
		if (in_array($m->tipo_form,array('georref','select_hierarquia'))) { ?>
			<div class="painelCarregando" id="carregando_<?=$m->campo?>"></div>
		<?php } ?>
		<div id="div_<?=$m->campo?>" class="painelOpcoesTitulo" <?php if ($cmdo_mapa->css) { ?> style="<?=$cmdo_mapa->css?>" <?php }?> onMouseOver="entrouMousePainel('<?=$m->campo?>')">
		<p><span style="float:right">-></span><?=$m->titulo?></p>
		<div id="<?=$m->campo?>_opcoesSels" class="painelOpcoesSelecionadas"></div>
		<div id="<?=$m->campo?>_opcoes" class="painelOpcoesPopup" onMouseOver="entrouMousePainel('<?=$m->campo?>')">
		<h2 onclick="saiuMousePainel('<?=$m->campo?>')"><?=$m->titulo?> <a href="javascript:saiuMousePainel('<?=$m->campo?>')">(fechar)</a></h2>
		<div class="painelOpcoes">
		<?php
		switch ($m->tipo_form) {
			case 'checkbox':
			case 'radio':
				if ($opcoes)
					foreach ($opcoes as $id=>$o) {
						$frm->inputs[$m->campo.'_'.$id]['EVENTS']['ONCLICK']=faz_atualizaSelecao($m->campo,$m->campo.'_'.$id, 0, 0, false, $id, $frm->inputs[$m->campo.'_'.$id]['LABEL']);
						$frm->AddInputPart($m->campo.'_'.$id); ?> <?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$id)); 
						?><br><?php
					}
			break;
			case 'select_hierarquia':
				$nmaxsels=$cmdo->niveis;
				// Aqui vai toda a cascata de selects
				for ($i=0;$i<$nmaxsels;$i++) {
					if ($i==$nmaxsels-1) $tmp=$m->campo;
						else $tmp=$m->campo.'_'.$i;
					$frm->inputs[$tmp]['EVENTS']['ONCHANGE'].=faz_atualizaSelecao($m->campo,$tmp,1,0);
					$frm->AddLabelPart(array("FOR"=>$tmp)); ?>: 
					<?php $frm->AddInputPart($tmp);
					?><br><?php
				}
			break;
			case 'georref':
				// Aqui vai toda a cascata de selects
				?>
				<table style="border:none">
				<?php
				for ($i=0;$i<=2;$i++) {
					?><tr><?php
					if ($i==2) $tmp=$m->campo;
						else $tmp=$m->campo.'_'.$i; 
					$frm->inputs[$tmp]['EVENTS']['ONCHANGE'].=faz_atualizaSelecao($m->campo,$tmp,1,0);
					?><td style="border:none;text-align:right"><?php
					$frm->AddLabelPart(array("FOR"=>$tmp)); ?>: </td><td style="border:none"><?php
					$frm->AddInputPart($tmp);
					?></td></tr><?php
				}
				?></table><?php
			break;
			case 'arvore':
				$campo_id=$cmdo->campo_id;
				$campo_nome=$cmdo->campo_nome;
				$campo_mae=$cmdo->campo_mae;
				$arvore = resgata_arvore($cmdo->apoio,$campo_id,$campo_nome,$campo_mae);
				$nivel=0;
				$idmae[0]='';
				foreach ($arvore as $i=>$no) {
					if ($no->nivel>$nivel) {
							$nivel=$no->nivel;
							$idmae[$nivel] = $arvore[$i-1]->$campo_id;
							echo '<div id="div_'.$m->campo.'_'.$idmae[$nivel].'">';
						} elseif ($no->nivel<$nivel) {
							echo str_repeat('</div>',$nivel-$no->nivel);
							$nivel=$no->nivel;
						} ?>
					<?=str_repeat('&nbsp;',$no->nivel*8)?>
					<?php $frm->inputs[$m->campo.'_'.$no->$campo_id]['EVENTS']['ONCLICK']=faz_atualizaSelecao($m->campo,$m->campo.'_'.$no->$campo_id, 0, 0, false, $no->$campo_id, $frm->inputs[$m->campo.'_'.$no->$campo_id]['LABEL']);?>
					<?php $frm->AddInputPart($m->campo.'_'.$no->$campo_id); ?>
					<?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$no->$campo_id)); ?>
					<br />
					<?php
				}
				for ($i=0;$i<$nivel;$i++)
					echo '</div>';
			break;
		}
		?>
		</div></div></div>
		<?php
		$htmlsel .= "</div>";
	}
	$n = count($filtrosJS);
	$filtrosJS[$n]->idCampo = 'textoBusca';
	$filtrosJS[$n]->nome = $txt['textoBusca'];
	unset($tmp);
	$tmp->id='textoBusca';
	$tmp->nome=$filtro->textoBusca;
	$tmp->valor=$filtro->textoBusca;
	$tmp->texto=true;
	$filtrosJS[$n]->sels = array($tmp);
	?>
	<div class="painelOpcoesTitulo" onMouseOver="entrouMousePainel('')">
	<p><?=$txt['textoBusca']?>:</p>
	<p><input id="textoBusca" name="textoBusca" value="<?=$filtro->textoBusca?>" onChange="<?=faz_atualizaSelecao('textoBusca','textoBusca',0,0,1)?>" onKeyPress="return intercepta_enter(event,'textoBusca','<?=faz_atualizaSelecao('','textoBusca',0,0,1,'','',true)?>')"></p>
	</div>
	<div class="painelOpcoesTitulo" style="cursor:auto" onMouseOver="entrouMousePainel('')">
		<p>Formato do resultado</p>
		<div class="painelOpcoesMapaLista">
		<table><tr>
		<td width="20px"><?php$frm->AddInputPart("mostramapa");?></td>
		<td>Mapa</td>
		<td>&nbsp;</td>
		<td td width="20px"><?php$frm->AddInputPart("mostratabela");?></td>
		<td>Lista</td>
		</tr></table>
		</div>
	</div>
	<?php if ($c) { ?>
	<div class="painelOpcoesTitulo" style="cursor:auto" onMouseOver="entrouMousePainel('')">
		<p>Camadas</p>
		<div class="painelOpcoesMapaLista">
		<table>
		<tr>
		<td width="20px"><input type="checkbox" name="territorios_encontro_dialogos" id="territorios_encontro_dialogos" onClick="camadas(this,'<?=$geoXml['territorios_encontro_dialogos']?>',7,1)"></td>
		<td>Territórios</td>
		</tr>
		<tr>
		<td width="20px"><input type="checkbox" name="bioma" id="bioma" onClick="camadas(this,'<?=$geoXml['biomas']?>',0)"<?php/*if ($_REQUEST['bioma']) echo 'checked="checked"'*/?>></td>
		<td><?=$txt['Biomas']?></td>
		</tr>
		<tr>
		<td width="20px"><input type="checkbox" name="BrasilIndigena" onClick="camadas(this,'<?=$geoXml['BrasilIndigena']?>',1,1)"></td>
		<td><?=$txt['TIs']?></td>
		</tr>
		<tr>
		<td width="20px"><input type="checkbox" name="BrasilIndigenaLegenda" onClick="camadas(this,'<?=$geoXml['BrasilIndigenaLegenda']?>',2,1)"></td>
		<td style="white-space:nowrap"><?=$txt['TIs_nomes']?></td>
		</tr>
		<tr>
		<td width="20px"><input type="checkbox" name="UC" onClick="camadas(this,'<?=$geoXml['UC']?>',3,1)"></td>
		<td style="white-space:nowrap"><?=$txt['UCs']?></td>
		</tr>
		</table>
		</div>
	</div>
	<?php } ?>
	<center>
	<div id="painelPreResultados"><?=str_replace('{numpts}','<span id="span_painelPreResultados"></span>',$txt['painel_pre_resultados'])?></div>
	<?php$frm->AddInputPart("buscar");
	if ($c) { ?>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<?php$frm->AddInputPart("limpar");
	}?>
	</center></div><?php
	$frm->EndLayoutCapture();
	$frm->DisplayOutput();
	?>
	<script language="JavaScript" type="text/javascript">
	<!--
		var filtros=eval('('+'<?=json_encode($filtrosJS)?>'+')');
		setTimeout('inicializaFiltros()',2000);
	-->
	</script>
	<?php
}

function mostraMapa($numpts,$urls,$ativ) {
	global $dir, $txt, $padrao_mapa;
	include $dir['mapas']."/phoogle2.php";
	$map = new PhoogleMap();
	$map->cabecalho_sidebar=$txt['cabecalho_sidebar'];
	$map->sidebar='filtros';
	$map->txt['mapa_clique_para_detalhes']=$txt['mapa_clique_para_detalhes'];
	if ($ativ)
		$map->categorias=$ativ;
	$map->autozoom=$padrao_mapa->autozoom;
	$map->apiKey=$padrao_mapa->apiKey;
	$map->numpts=$numpts;
	$map->gera_xml = $urls;
	$map->printGoogleJS();
	$map->controlType = $padrao_mapa->controlType;
	$map->showControl = $padrao_mapa->showControl;
	$map->posicaoControle = $padrao_mapa->posicaoControle;
	$map->addMapType = $padrao_mapa->addMapType;
	$map->showType=$padrao_mapa->showType;
	$map->centerMap( '0','0', "mapa" );
	$map->tipo_mapa = $padrao_mapa->tipo_mapa;
	$map->mapWidth=$padrao_mapa->mapWidth;
	$map->mapHeight=$padrao_mapa->mapHeight;
	$map->showMap();
}

function mostraTabela($xml) {
	global $dir, $padrao_itens_por_pagina, $numpgs, $numpts, $pg_lista, $txt, $tipofrm;
	?>
	<div id='mostraTabela'>
	<h1>Resultados</h1>
	<?=faz_indice_tabela($numpgs,$pg_lista)?>
	<br>
	<?php
	foreach ($xml as $x)
		foreach ($x->markers->pt as $pt) {
			$descricao = '<div style="text-align:right;font-style:italic"><b>'.$txt['painel_localizacao'].':</b> '.$pt['cidade'].' / '.$pt['estado'].' / '.$pt['pais'].'</div><br /><p>'.$pt['descricao'].'</p>';
			$descricao = addslashes(str_replace(array(chr(10),chr(13)),'<br>',str_replace('"',"'",$descricao)));
			$titulo = addslashes($pt['nome']);
			?>
			<p><a class="Preto" href="<?=str_replace('{id}',$pt['id'],$dir['mostra_'.$tipofrm.'_URL'])?>" onmouseover="return overlib('<?=$descricao?>', CAPTION, '<?=$titulo?>', ABOVE, RIGHT, CLOSETEXT, 'fechar', WIDTH, 350, AUTOSTATUSCAP, DELAY, '200');" onmouseout="return nd();"><?=$pt['nome']?></a></p>
			<?php
		}
	?>
	<br>
	<?=faz_indice_tabela($numpgs,$pg_lista)?>
	</div>
	<script language="JavaScript" type="text/javascript">
	<!--
		document.getElementById('filtros').style.visibility='visible';
	-->
	</script>
	<?php
}
function faz_indice_tabela($numpgs, $pg_lista) {
	$href = str_replace('&pgLista='.$pg_lista,'',$_SERVER[QUERY_STRING]);
	?>
	<p class="indiceMostraTabela">
	<?php if ($numpgs>1) { ?>
		<span>
		<?php
		for ($i=1;$i<=$numpgs;$i++)
			if ($i==$pg_lista) { ?>
				[<?=$i?>]&nbsp;
			<?php } else { ?>
				<a class="Preto" href="?<?=$href?>&pgLista=<?=$i?>">[<?=$i?>]</a>&nbsp;
			<?php
			}
	?>
	</span>
	<?php } ?>
	<b>pg. <?=$pg_lista?>/<?=$numpgs?></b></p>
	<?php
}
function faz_atualizaSelecao($idCampo,$id,$select,$href=0,$texto=false,$valor='',$nome='',$soparams=false) {
	$r->id = $id;
	$r->select = $select;
	$r->texto = $texto;
	$r->href = $href;
	if (!$select && !$texto) {
		$r->valor = $valor;
		$r->nome = $nome;
	}
	return ($soparams)
		? str_replace('"',"\'",json_encode($r))
		: "atualizaSelecao('".$idCampo."', '".str_replace('"',"\'",json_encode($r))."');";
}

?>
