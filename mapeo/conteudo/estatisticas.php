<?php


//Se a pessoa não está conectada, pode ser que o ano tenha sido passado pelo GET!
if (!$s) {
	if ($_GET['ano']) {
		$ano=$_GET['ano'];
	} elseif ($_GET['grafico'] && !$ano) {
		$sql="SELECT ano FROM graficos WHERE id=".$_GET['grafico'];
		$gtmp=faz_query($sql,'','array');
		$ano=$grafico['ano'][0];
	}
}

?>
<h1>
<?php
	echo $txt['titulo_estatisticas'];
	if ($ano)
		echo ' - 20'.$ano;
?>
</h1>

<div class="conteudo">
<?php

if (!$_GET['grafico']) {
	if (!$ano) {
	//Caso o ano não esteja definido
	$sql="SELECT ano, count(*) as n FROM graficos GROUP BY ano";
	$graficos=faz_query($sql,'',"array");
	?>
	<a href="index.php"> Página inicial</a></p>
	<h2 style="clear:none">Escolha o ano</h2>
	<ul>
	<?php
	foreach ($graficos['ano'] as $i=>$ano)
		echo '<li><a href="?f=estatisticas&ano='.$ano.'">20'.$ano.'</a> <i>('.$graficos['n'][$i].' gráficos)</i></li>';
	?>
	</ul>
	<?php
	
	} else {
		// Caso o ano esteja definido:
		?>
		<p><a href="?f=estatisticas">Mudar o ano</a> &nbsp;|&nbsp;<a href="index.php"> Página inicial</a></p>
		<?php
		$sql='SELECT * FROM graficos_secoes ORDER BY `ord`';
		$gsecoes=faz_query($sql,'',"array");
		if ($gsecoes) {
			foreach($gsecoes['titulo'] as $i=>$gtitulo) {
				$sql='SELECT id, titulo FROM graficos WHERE ano="'.$ano.'" AND id_secao="'.$gsecoes['id'][$i].'" ORDER BY `ord`';
				$graficos=faz_query($sql,'','array');
				if ($graficos) {
					?>	
					<h2 style='clear:none'><?php echo $gtitulo; ?></h2>
					<ul>
						<?php				
						foreach ($graficos['id'] as $k=>$gid)
							echo '<li><a href="index.php?f=estatisticas&ano='.$ano.'&gsecao='.$gsecoes['id'][$i].'&grafico='.$gid.'">'.$graficos['titulo'][$k].'</a></li>';
						?>
					</ul>
					<?php
				} // fim do if tem $graficos dentro da seção gsecoes
			} // fim do foreach
		} else echo '<p>'.str_replace('{ANO}',$ano,$txt['erro_nao_ha_grafico']).'</p>';
	} //fim do if tem ano definido...	
} else {
	//Aqui é o caso do id do gráfico já estar definido:
	?>
	<p><a href="?f=estatisticas">Mudar o ano</a>
	&nbsp;|&nbsp;
	<a href="index.php?f=estatisticas&ano=<?=$ano?>">Escolher outro gráfico de 20<?=$ano?></a>
	&nbsp;|&nbsp;
	<a href="index.php"> Página inicial</a></p>
	<?php
	$gsecao=$_GET['gsecao'];
	$gid=$_GET['grafico'];
	if (intval($_GET['recorte0'])) $grecorte=intval($_GET['recorte0']);
	
	include ($dir['apoio']."jpgraph/src/jpgraph.php");

	// É o momento de recuperar as informações guardadas na tabela 'graficos':
	$sql='SELECT * FROM graficos WHERE id='.$gid;
	$graficos=faz_query($sql,'','array','',false);

	//Agora construo nomes de variáveis mais confortáveis a partir da consulta mysql deste gráfico:
	$gtit=$graficos['titulo'][0];

	
	// Aqui obtenho outras opções específicas:
	$tmp=explode('|',$graficos['opcoes'][0]);
	foreach ($tmp as $i=>$t) {
		$tmp2=explode(':',$t);
		$opcao[$tmp2[0]]=$tmp2[1];
	}
	// Aqui obtenho o nome do 'outros':
	$outros_nome = ($opcao['outros_nome'])
		? $opcao['outros_nome']
		: 'Outros';

	// Se o tipo de gráfico é "script", tem que inserir o arquivo do campo 'frm' sem preparar nada, pois é um gráfico TOTALMENTE customizado!
	if ($graficos['tipo'][0]=='script') {
		$garq=$dir['apoio']."jpgraph/graficos/".$graficos['frm'][0];
		$camporecorte='{ID}';
		// Caso contrário, devo extrair as informações do gráfico:
	} else {
		$gfrm=$graficos['frm'][0];
		$ano=$graficos['ano'][0];
		$gfrmano=$ano.$gfrm;
		$gpg=$graficos['pg'][0];
		$gtab='form_'.$gfrmano.$gpg;
	
		$camporecorte = ($graficos['camporecorte'][0])
			? $graficos['camporecorte'][0]
			: $gtab.'.'.$campo[$gfrmano]->id;
	
		/* Aqui obtenho o campo X e eventualmente a sua tabela de apoio e as relações entre a tabela fonte e a tabela principal ($gtab). Se tem tabela de apoio, tem que haver 4 informações:
			tabfonte: a tabela fonte que será referenciada
			campo_tabfonte: o campo que dá o valor final, na tabela fonte (de apoio). Ou seja, é o que vai no SELECT.
			campo_tabprincipal: o campo da tabela principal no where pra fechar a relação. Se não for espcificado, é o que está no 'campoy'!! Ou seja, a relação é com o campoy.
			campo_tabfonte: o campo da tabela de apoio (fonte) no where para fechar a relação.
			Ou seja, o select acabará tendo o 'campo_tabfonte' no select, no FROM o 'tabfonte', e no WHERE a igualdade campo_tabprincipal=tabfonte.campo_tabfonte !
		*/
		$tmp=explode('|',$graficos['campox'][0]);
		foreach ($tmp as $t) {
			$tmp2=explode(':',$t);
			if ($tmp2[0]=='mostra_tabfonte') $campox=$tmp2[1];
				elseif ($tmp2[1]) $tabfontex[$tmp2[0]]=$tmp2[1];
					else $campox=$tmp[0];
		}
		if ($tabfontex && !$tabfontex['ref_tabfonte']) $tabfontex['ref_tabfonte']='id';
		// Se tem o comando {ID} dentro da opção ref_tabprincipal ou existe o tabfontex e não está especificada a ref_tabprincipal, eu devo defini-lo como o id da tabela!
		if ($tabfontex['ref_tabprincipal']) 
			$tabfontex['ref_tabprincipal']=str_replace('{ID}',$gtab.'.'.$campo[$gfrmano]->id,$tabfontex['ref_tabprincipal']);
		elseif ($tabfontex && !$tabfontex['ref_tabfonte'])
			$tabfontex['ref_tabprincipal']=$gtab.'.'.$campo[$gfrmano]->id;
		// Se não está definido o campox, mas ao mesmo tempo existe o tabfontex, estou querendo dizer que o campox é 'nome':
		if (!$campox && $tabfontex) $campox='nome';

		// Aqui obtenho os campos y. É um pouco mais complexo, pois pode ou não ter uma tabela de apoio, o que significa que teremos múltiplos campos, definidos pelo campo. Além disso, pode ser de vários campos...:
		$tmp=explode('|',$graficos['campoy'][0]);
		foreach ($tmp as $i=>$t) {
			$tmp2=explode(':',$t);
			
			//Caso tenha opções, faço como no campoX!
			
			if ($tmp2[1]) {
				if ($tmp2[0]=='mostra_tabfonte') $campofontey=$tmp2[1];
					else $tabfontey[$tmp2[0]]=$tmp2[1];
				if ($tabfontey && !$tabfontey['ref_tabfonte'])
					$tabfontey['ref_tabfonte']='id';
				// Se tem o comando {ID} dentro da opção ref_tabprincipal ou existe o tabfontex e não está especificada a ref_tabprincipal, eu devo defini-lo como o id da tabela!
				if ($tabfontey['ref_tabprincipal']) 
					$tabfontey['ref_tabprincipal']=str_replace('{ID}',$gtab.'.'.$campo[$gfrmano]->id,$tabfontey['ref_tabprincipal']);
					elseif ($tabfontey && !$tabfontey['ref_tabfonte'])
						$tabfontey['ref_tabprincipal']=$gtab.'.'.$campo[$gfrmano]->id;
				// Se não está definido o campoy, mas ao mesmo tempo existe o tabfontey, estou querendo dizer que o campofontey é 'nome':
				if (!$campofontey && $tabfontey)
					$campofontey='nome';
					
			} else $campoy[$i]=$tmp2[0];
		} // fim do loop de resgate do campoy
		
		// Se, no campoY, eu defini tabfonteY, significa que o conjunto de valores da tabela de apoio, no campo campofontey, definem as múltiplas consultas: será um gráfico múltiplo (ou seja, se é pizza, são várias pizzas, se é barra, são múltiplas barras ou pilha, se é linha, são várias linhas)

		// Aqui obtenho as legendas:
		$tmp=explode('|',$graficos['legenda'][0]);
		foreach ($tmp as $t)
			$legenda[]=$t;
	
		// Aqui obtenho opções dos eixos:
		$tmp=explode('|',$graficos['eixox'][0]);
		foreach ($tmp as $i=>$t) {
			$tmp2=explode(':',$t);
			$eixox[$tmp2[0]]=$tmp2[1];
		}
		$tmp=explode('|',$graficos['eixoy'][0]);
		foreach ($tmp as $i=>$t) {
			$tmp2=explode(':',$t);
			$eixoy[$tmp2[0]]=$tmp2[1];
		}
	
		// Aqui obtenho as margens (a partir das opções específicas):
		$margens = ($opcao['margens'])
			? $opcao['margens']
			: '50,50,60,40';
		$margem=explode(",",$margens);
		
		$sql="SELECT ";
		$from=' FROM '.$gtab;
		$where=' WHERE '.$graficos['where'][0];
		$garq=$dir['apoio']."jpgraph/graficos/tipo_".$graficos['tipo'][0].'.php';
	} // fim do else 'tipo'=='script'.

	// Aqui eu defino o whererecorte e os subtítulo para o caso de um recorte ter sido escolhido:
	if ($grecorte)
	switch ($grecorte) {
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
			$whererecorte=" AND SUBSTRING(".$camporecorte.",1,1)=".$grecorte;
			$subtitulo=chr(10).'(Região '.$lista_MR[$grecorte].')';
		break;
		default:
			$whererecorte=" AND SUBSTRING(".$camporecorte.",1,2)='".$grecorte."'";
			$subtitulo=chr(10).'('.$lista_feiras[$grecorte]->nome.')';
		break;
	} // fim do switch

	// dtygel: ATENÇÃO! provisoriamente estou desativando o subtítulo a pedidos de Wilson e Jonas!! Depois, tenho que colocar normal de novo!!!
	// $subtitulo.=' - Ano: 20'.$ano;

	// Enfim, posso carregar o tipo específico de gráfico para que seja gerado:
	include ($garq);

	$grafico_arq='apoio/jpgraph/graficos/imagens_geradas/G-'.$gsecao.'-'.$gid;
$grafico_arq .= ($_GET['recorte0'])
		? '_'.$_GET['recorte0'].'.'.$extensao
		: '.'.$extensao;

	$graph->img->SetQuality(100);
	$graph->img->SetImgFormat($tipografico);
	$graph->Stroke($grafico_arq);

	// Já gerei o gráfico, e tenho que inserir o código HTML que mostra a caixa de seleção de recortes, se necessário, e depois o próprio gráfico gerado.
	if (!$opcao['nao_faz_recorte']) {
		//Primeiramente, defino que itens aparecem no select dos recortes, a partir do query gerado o arquivo, um pouco alterado:
		$sql=str_replace($whererecorte,'',$sql);
		$i=strpos($sql,' FROM ');
		$sql=substr($sql,$i,strlen($sql));
		if ($i=strpos($sql,' GROUP BY '))
			$sql=substr($sql,0,$i);
		if ($i=strpos($sql,' ORDER BY '))
			$sql=substr($sql,0,$i);
		$sql='SELECT DISTINCTROW SUBSTRING('.$camporecorte.',1,2) AS aa'.$sql;
		$sql.=' ORDER BY aa';
		$sql=str_replace('{CAMPOY}',$campoy[0],$sql);
		$dados=faz_query($sql,'','array');
		$abrangencia_opcoes=$dados[0];
		foreach($abrangencia_opcoes as $d)
			if (!in_array($d[0],$abrangencia_opcoes))
				$abrangencia_opcoes[]=$d[0];
		$usa_ufs = !(in_array($graficos['tipo'][0],array('barra','linha')) && $campox=='UF');
	
		// Listas de valores-padrão do select do recorte seguem abaixo. Durante a execução do tipo específico de gráfico, foi elaborada a lista de valores que devem aparecer ($abrangencia_opcoes), e portanto esta especificação será levada em conta pra gerar a lista de opções:
		$opcoes_recortes=array();
		foreach ($lista_MR as $tmp1=>$tmp2)
			if ($tmp1>0 && $tmp1<6 && in_array($tmp1,$abrangencia_opcoes))
				$opcoes_recortes+=array($tmp1=>'Região '.$tmp2);
		asort($opcoes_recortes);
		
		if ($usa_ufs) {
			$opcoes_recortes+=array(0=>'-----------');
			$tmp=array();
			foreach ($lista_feiras as $tmp1=>$tmp2)
				if (!in_array($tmp1,array(70,99)) && in_array($tmp1,$abrangencia_opcoes))
					$tmp+=array($tmp1=>$tmp2->nome);
			asort($tmp);
			$opcoes_recortes+=$tmp;
		}

		// Com os dados gerados, posso enfim inserir a caixa de selects dos recortes:	
		include 'estatisticas_recortes.php';
	
	} // Fim do if faz_recortes!
	
	// Agora obtenho o valor total do universo de dados, se não tiver sido obtido já:
	if (!$guniverso) {
		$sql=str_replace(' DISTINCTROW','',$sql);
		if ($i=strpos($sql,' ORDER BY '))
			$sql=substr($sql,0,$i);
		$sql.=$whererecorte;
		$res=mysql_query($sql);
		$guniverso=mysql_numrows($res);
	}
	
	// E inserir o gráfico gerado:
	?>
	<div style='text-align:center;margin-top:20px'><img src='<?php echo $grafico_arq; ?>'>
	<?php if (in_array($graficos['tipo'][0],array('pizza','script'))) {
	?>
	<br>
	Total de formulários para realizar este gráfico: <b><?php echo yLabelformat($guniverso); ?></b>.
	<?php } ?>
	</div>
	

	<?php
} // fim do else 'tipo'=='script'
/*exit;



echo '<table width="500px" align="center" border="1px">'.chr(10);
echo '<tr>'.chr(10).'<td align="center"><b>Categoria</b></td>'.chr(10).'<td align="center"><b>num</b></td>'.chr(10);
echo '</tr>'.chr(10);

while ($q=mysql_fetch_array($res)) {
	echo '<tr>'.chr(10);
	if ($titulo!=$q['A']) {
		$titulo=$q['A'];
		echo '<td colspan=2><h2>'.$q['A'].'</h2></td>'.chr(10);
		echo '</tr>'.chr(10).'<tr>'.chr(10);
	}
	if (!$q['A']) {
    	echo '<td align="right" valign="top"><b>Total geral</b></td>'.chr(10);
	    echo '<td align="center" valign="top"><b>'.$q['subtotal'].'</b><br></td>'.chr(10);
	} elseif ($q['categoria']) {
		$nsub=0;
    	echo '<td width="100%">'.$q['categoria'].'</td>'.chr(10);
	    echo '<td align="center">'.$q['subtotal'].'</td>'.chr(10);
	} else {
		$nsub++;
		switch ($nsub) {
			case 1:
				echo '<td height="60px" align="right" valign="top" bgcolor="#ddaaaa"><h3>subtotal do ano</h3></td>'.chr(10);
				$bgcolor='#ddaaaa';
			break;

			case 2:
				echo '<td height="60px" align="right" valign="top" bgcolor="#ff99aa"><h2>total geral</h2></td>'.chr(10);
				$bgcolor='#ff99aa';
			break;
		}
	    echo '<td align="center" valign="top" bgcolor="'.$bgcolor.'"><b>'.$q['subtotal'].'</b><br></td>'.chr(10);
	}
    echo '</tr>'.chr(10);
}
*/



/*
?>
</table>

<br><div align="center"><a href="#topo">voltar ao topo</a></div><br><hr><br>
*/
?>
</div> <!-- fim de bloco de conteúdo //-->

<?php
