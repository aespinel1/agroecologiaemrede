<?php
//por daniel tygel (dtygel em fbes pt org pt br) em junho de 2009
foreach ($_REQUEST as $i=>$b)
	if ($b=='-9999')
		unset($_REQUEST[$i]);
//define(MAPA, false); // TODO: ENQUANTO NÃO CONSIGO FAZER O MAPA, DEIXO APENAS COMO LISTA!
$pgAtual = (isset($_REQUEST['pgLista']))
	? $_REQUEST['pgLista']
	: 1;
$tipofrm=(isset($_REQUEST['tipofrm']))
	? $_REQUEST['tipofrm']
	: 'experiencia';
if (!isset($_REQUEST['limpar']) || !$_REQUEST['limpar']) {
	$c = isset($_REQUEST['c'])
		? $_REQUEST['c']
		: 1;
} else {
	$c='';
	unset($_REQUEST);
}
$filtro = resgata_filtros($tipofrm);
$urls = resgata_urls(array('agrorede'=>'mapas/gera_xml_agrorede.php'));
$resultados = ($c)
	? carregaResultados()
	: null;
$frm = new form_dtygel_class;
// Faço as definições iniciais do formulário:
$frm->NAME="painel_mapas";
$frm->METHOD="GET";
$frm->ACTION="index.php";
if (MAPA) {
	$frm->CLASS="h-100";
}
$frm->debug="trigger_error"; //Esse é pra eu debugar, enquanto programo
$frm->ResubmitConfirmMessage=$msg['resubmissao_de_mesmos_dados'];
$frm->OutputPasswordValues=0; //ver manual sobre isso...
$frm->OptionsSeparator="<br />\n";
$frm->ShowAllErrors=0;
$frm->InvalidCLASS='invalido';
$frm->encoding='utf-8';
$frm->ErrorMessagePrefix="-> ";
$frm->ErrorMessageSuffix="";
$frm->AddInput(array("TYPE"=>"hidden","NAME"=>"c","VALUE"=>1));
$frm->AddInput(array("TYPE"=>"hidden","NAME"=>"f","VALUE"=>"consultar"));
$frm->AddInput(array("TYPE"=>"hidden","ID"=>"tipofrm","NAME"=>"tipofrm","VALUE"=>$tipofrm));
if ($c) {
	$frm->AddInput(array("TYPE"=>"submit","ID"=>"buscar","NAME"=>"buscar","VALUE"=>"buscar","CLASS"=>"btn btn-warning w-100 mb-2"));
	$frm->AddInput(array("TYPE"=>"submit","ID"=>"limpar","NAME"=>"limpar","VALUE"=>$txt['painel_limpar_selecao'],"CLASS"=>"btn btn-warning w-100 mb-2"));
} else {
	$frm->AddInput(array("TYPE"=>"submit","ID"=>"buscar","NAME"=>"buscar","VALUE"=>"buscar","CLASS"=>"btn btn-warning w-100 mb-2"));
}
$frm->rodaMapaCampos($filtro->mapa,$filtro->cmd,isset($filtro->sels) ? $filtro->sels : null);
$frm->StartLayoutCapture();
if (MAPA) {
	$classPainel = "col-sm-12 col-md-4 col-lg-3 d-none d-md-block";
	$classResultados = "col-sm-12 col-md-8 col-lg-9 d-flex flex-column";
	$classTituloResultados = "col-6 col-sm-6 pt-2";
	$classBotoesResultados = "col-6 col-sm-6 text-right text-nowrap";
	$classBotaoFiltroPainel = "col-md-12 col-lg-12 mb-2";
} else {
	$classPainel = "col-sm-12 mb-4";
	$classResultados = "col-sm-12";
	$classTituloResultados = "col-8 col-sm-6 pt-1";
	$classBotoesResultados = "col-4 col-sm-6 text-right text-nowrap";
	$classBotaoFiltroPainel = "col-12 col-sm-6 col-lg-3 mb-2";
}
?>
<script type="text/javascript" src="<?=$dir['apoio_URL']?>json_min.js"></script>
<div class="row h-100">
	<div id="painel" class="<?=$classPainel?>">
		<div class="px-2 py-2 bg-light rounded">
			<div class="row">
				<div class="col-md-12">
					<?=(MAPA)?'<h5>':'<h3>'?><i class="aericon-<?=$tipofrm=='instituicao' ? 'instituicoes' : 'experiencias'?>"></i> <?=$txt['titulo_filtros_'.$tipofrm]?><?=(MAPA)?'</h5>':'</h3>'?>
				</div>
			</div>
			<div id="filtros" class="row">
				<div class="col-md-12">
					<?php mostraPainel($frm, $classBotaoFiltroPainel); ?>
				</div>
			</div>
		</div>
	</div>
	<div id="resultados" class="<?=$classResultados?>">
		<div class="px-2 py-2 bg-light rounded">
			<div class="row">
				<div class="col-md-12">
					<div class="row">
						<div class="<?=$classTituloResultados?>">
							<h4 class="text-truncate">
								<span class="span_painelPreResultados"><?=$resultados->numpts?></span> resultados
							</h4>
						</div>
						<div class="<?=$classBotoesResultados?>">
							<?php if (MAPA) { ?>
								<a href="#" title="Filtros" data-toggle="modal" data-target="#modalPainel" class="btn btn-primary btn-sm mr-1 mb-1 d-md-none">
									<i class="oi oi-magnifying-glass"></i><span> Filtros <span id="badgeFiltros" class="badge badge-light<?=(count($filtro->sels)>1)?'':' d-none'?>"><?=count($filtro->sels)-1?></span></span>
								</a>
								<a href="?<?=str_replace('&visao=mapa','',$_SERVER['QUERY_STRING']).'&visao=tabela'?>" class="toggleDisplay btn btn-info" title="Ver lista" data-toggle="tooltip">
									<i class="oi oi-list"></i><span class="d-none d-sm-inline">  Ver lista</span>
								</a>
							<?php } else { ?>
								<a href="?<?=str_replace('&visao=tabela','',$_SERVER['QUERY_STRING']).'&visao=mapa'?>" class="toggleDisplay btn btn-info" title="Ver mapa" data-toggle="tooltip">
									<i class="oi oi-map"></i><span class="">  Ver mapa</span>
								</a>
							<?php } ?>
						</div>
					</div>
					<?php if (!MAPA) { ?>
					<div class="row">
						<div class="col-md-12">
							<?php faz_indice_tabela($resultados->numpts, $resultados->numpgs, $pgAtual, 'indiceCabecalho') ?>
						</div>
					</div>
					<?php } ?>
				</div>
			</div>
			<?php if (!MAPA) { /* 1==2 to remove this box to see... */ ?>
			<?php } ?>

			<?php if (!MAPA) { ?>
				<div id="mostraTabela">
					<?php mostraTabela($filtro, $resultados->xml, $tipofrm, $resultados->numpts, $resultados->numpgs, $pgAtual); ?>
				</div>
			<?php } ?>
		</div>
		<?php if (MAPA) { ?>
			<?php mostraMapa($resultados->numpts, $urls, $resultados->ativ); ?>
		<?php } ?>
		<div id="carregando_mapa" class="<?=(MAPA)?'':' d-none'?>"></div>
	</div>
	<div class="modal" id="modalPainel" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Filtros</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
					</div>
				</div>
				<div class="modal-footer">
					<!--<input type="submit" class="btn btn-primary" value="Selecionar">-->
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="modalFiltros" data-idCampo="" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<form id="modal-form" name="modal-form" action="GET">
					<div class="modal-header">
						<h5 class="modal-title"></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
					</div>
					<div class="modal-footer">
						<!--<input type="submit" class="btn btn-primary" value="Selecionar">-->
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php $frm->EndLayoutCapture(); ?>
<?php $frm->DisplayOutput(); ?>
<?php if ($c=='nem pensar') {
	mostraLinhaDoTempo();
} ?>

<script>
	var isMap = <?=(MAPA) ? 'true' : 'false'?>;
	<?php $urlsRaw = array(); ?>
	<?php foreach ($urls as $g) { ?>
		<?php $urlsRaw[]=stripslashes($g->url); ?>
	<?php } ?>
	var urls=<?=json_encode($urlsRaw)?>;
	<?php if (!MAPA) { ?>
		var url = <?=json_encode(reset($urls))?>;
	<?php } ?>
</script>

<?php

function resgata_urls ($fontes) {
	global $dir,$tipofrm,$pgAtual;
	$dados_get='?c=1&tipofrm='.$tipofrm;
	if (!MAPA) {
		$dados_get.='&orderby=dt_criacao&order=DESC&visao=tabela';
		if ($pgAtual) {
			$dados_get.='&pgLista='.$pgAtual;
		}
		if ($tipofrm!='instituicao') {
			$dados_get.='&include_images=1';
		}
	} else {
		$dados_get.='&visao=mapa';
	}
	$filtro = resgata_filtros($tipofrm);
	if (isset($filtro->textoBusca) && $filtro->textoBusca)
		$dados_get.='&textoBusca='.urlencode($filtro->textoBusca);
	if (isset($filtro->sels) && $filtro->sels) {
		foreach($filtro->sels as $cmpo=>$valores) {
			if (is_array($valores))
				$valores = implode('|',$valores);
			$dados_get .= '&'.$cmpo.'='.urlencode($valores);
		}
	}


	//Especial: linha do tempo de inserção de iniciativas no sistema. Ainda estou pensando se seria possível que esta função pudesse ficar especificada no mapeo_mapa_campos no campo "dt_criacao". Seria algo como um filtro:1, mas que não geraria o menu lateral, e o tipo de filtro seria um que pudesse pegar 3 tipos de intervalo: antes de datai, depois de dataf, e entre (between) datai e dataf. Por enquanto vai na marra mesmo:
	$datai = (isset($_REQUEST['datai']))
		? $_REQUEST['datai']
		: '';
	$dataf = (isset($_REQUEST['dataf']))
		? $_REQUEST['dataf']
		: '';
	if ($datai || $dataf) {
		$tipodata = isset($_REQUEST['tipodata'])
			? $_REQUEST['tipodata']
			: '';
		if ($datai)
			$dados_get .= "&datai=$datai";
		if ($dataf)
			$dados_get .= "&dataf=$dataf";
		if ($tipodata)
			$dados_get .= "&tipodata=$tipodata";
	}
	// acabou a forçação de barra especial.

	$urls = array();
	foreach ($fontes as $tipo=>$ftmp) {
		$fonte_ext = (isset($fontes_ext[$tipo]) && $fontes_ext[$tipo])
			? '&fonte='.$fontes_ext[$tipo]
			: '';
		$urls[$tipo] = new stdClass();
		$urls[$tipo]->url = $dir['base_URL'].$ftmp;
		$urls[$tipo]->dados_get = $dados_get.$fonte_ext;
	}

	return $urls;
}
function mostraLegenda($numpts) {
	global $txt, $tipofrm, $filtro;
	$frm = new form_dtygel_class;
	$frm->rodaMapaCampos($filtro->mapa,$filtro->cmd,$filtro->sels);
	?>
	<h5><span style="float:left"><b>agroecologiaemrede.org.br</b></span><?=date('d/m/Y')?></h5>
	<h4><?=$txt['titulo_legenda_'.$tipofrm]?><br />
	<span class="peq"><?=str_replace('{pts}','<span id="span_legendaMapaSels">'.$numpts.'</span>',$txt['mapa_itens_encontrados'])?></span></h4>
	<div id="legendaMapaSels">
	<?php
	foreach ($filtro->filtro as $cmpo) {
		$m = $filtro->mapa[$cmpo];
		$cmdo = $filtro->cmd[$m->campo];
		$cmdo_mapa = $filtro->cmd_mapa[$m->campo];
		$opcoes = $filtro->opcoes[$m->campo];
		$sels = $filtro->sels[$m->campo];
		if ($sels) {
			?>
			<p><b><?=$m->titulo?>:</b>
			<?php
			unset($html);
			if (in_array($m->tipo_form,array('select_hierarquia','georref'))) {
				// Caso dos selects:
				if (is_array($sels)) {
					foreach ($sels as $nivel=>$sel) {
						if ($frm->inputs[$m->campo.'_'.$nivel]['OPTIONS'][$sel]) {
							$html[] = $frm->inputs[$m->campo.'_'.$nivel]['OPTIONS'][$sel];
						} else {
							$html[] = $frm->inputs[$m->campo]['OPTIONS'][$sel];
						}
					}
					echo implode(', ',$html);
				} else
					echo $frm->inputs[$m->campo]['OPTIONS'][$sel];
			} else {
				foreach ($sels as $sel) {
					// Caso dos checkboxes:
					$html[] = $opcoes[$sel];
				}
				echo implode('; ',$html);
			}
			?></p><?php
		}
	}
	if (isset($filtro->textoBusca) && $filtro->textoBusca) {
		?>
		<p><b><?=$txt['textoBusca']?>:</b> "<?=$filtro->textoBusca?>"</p>
		<?php
	}
	?>
	</div>
	<?php
}
// Inseri um lance para poder exibir o painel com controles a partir dos formulários. Não sei se está no melhor lugar, mas enfim...
function mostraPainel(&$frm, $classBotaoFiltroPainel) {
	global $c,$tipofrm, $filtro, $txt, $urls, $geoXml, $msg;

	// Construção do título maior, e estrutura de DIVs
	//$htmlsel = '<div id="selecionadas">';
	$frm->AddInputPart("c");
	$frm->AddInputPart("f");
	$frm->AddInputPart("tipofrm");

	$n = count($filtrosJS);
	if (!isset($filtrosJS[$n])) {
		$filtrosJS[$n] = new StdClass();
	}
	$filtrosJS[$n]->idCampo = 'textoBusca';
	$filtrosJS[$n]->nome = $txt['textoBusca'];
	$tmp = new StdClass();
	$tmp->id='textoBusca';
	$tmp->nome=$filtro->textoBusca;
	$tmp->valor=$filtro->textoBusca;
	$tmp->texto=true;
	$filtrosJS[$n]->sels = array($tmp);
	?>
	<div class="input-group mb-2">
    <div class="input-group-prepend">
      <div class="input-group-text"><i class="oi oi-magnifying-glass"></i></div>
    </div>
    <input id="textoBusca" class="form-control" type="text" name="textoBusca" value="<?=$filtro->textoBusca?>" onChange="<?=faz_atualizaSelecao('textoBusca','textoBusca',0,0,1)?>" onKeyPress="return intercepta_enter(event,'textoBusca','<?=faz_atualizaSelecao('','textoBusca',0,0,1,'','',true)?>')" placeholder="Busca por texto...">
  </div>
	<div class="row">
	<?php
	foreach ($filtro->filtro as $cmpo) {
		$m = $filtro->mapa[$cmpo];
		$n = count($filtrosJS);
		if (!isset($filtrosJS[$n])) {
			$filtrosJS[$n] = new StdClass();
		}
		$filtrosJS[$n]->idCampo=$m->campo;
		$filtrosJS[$n]->nome = ($m->titulocurto) ? $m->titulocurto : $m->titulo;
		$cmdo = $filtro->cmd[$m->campo];
		$cmdo_mapa = $filtro->cmd_mapa[$m->campo];
		$opcoes = $filtro->opcoes[$m->campo];
		$sels = $filtro->sels[$m->campo];

		if ($m->titulocurto) $m->titulo=$m->titulocurto; if ($sels) {
		$filtrosJS[$n]->sels = array(); if
		(in_array($m->tipo_form,array('select_hierarquia','georref'))) {
				// Caso dos selects:
				if (is_array($sels)) {
					foreach ($sels as $nivel=>$sel) {
						$j = count($filtrosJS[$n]->sels);
						if (!isset($filtrosJS[$n]->sels[$j])) {
							$filtrosJS[$n]->sels[$j] = new StdClass();
						}
						if ($frm->inputs[$m->campo.'_'.$nivel]['OPTIONS'][$sel]) {
							$filtrosJS[$n]->sels[$j]->id = $m->campo.'_'.$nivel;
							$filtrosJS[$n]->sels[$j]->nome = $frm->inputs[$m->campo.'_'.$nivel]['OPTIONS'][$sel];
						} else {
							$filtrosJS[$n]->sels[$j]->id = $m->campo;
							$filtrosJS[$n]->sels[$j]->nome = $frm->inputs[$m->campo]['OPTIONS'][$sel];
						}
						$filtrosJS[$n]->sels[$j]->valor = $sel;
						$filtrosJS[$n]->sels[$j]->select = 1;
					}
				} else {
					$j = count($filtrosJS[$n]->sels);
					$filtrosJS[$n]->sels[$j]->id = $m->campo;
					$filtrosJS[$n]->sels[$j]->nome = $frm->inputs[$m->campo]['OPTIONS'][$sel];
					$filtrosJS[$n]->sels[$j]->valor = $sel;
					$filtrosJS[$n]->sels[$j]->select=1;
				}
			} else foreach ($sels as $sel) {
				// Caso dos checkboxes:
				$j = count($filtrosJS[$n]->sels);
				$filtrosJS[$n]->sels[$j] = new StdClass();
				$filtrosJS[$n]->sels[$j]->id = $m->campo.'_'.$sel;
				$filtrosJS[$n]->sels[$j]->nome = $opcoes[$sel];
				$filtrosJS[$n]->sels[$j]->valor = $sel;
			}
		}
		?>
		<div class="<?=$classBotaoFiltroPainel?>">
			<button id="div_<?=$m->campo?>" type="button" class="btn btn-primary w-100" onClick="entrouMousePainel('<?=$m->campo?>')"><span class="float-right"><i class="oi oi-chevron-bottom"></i></span><?=$m->titulo?></button>
			<div id="<?=$m->campo?>_opcoesSels" class="painelOpcoesSelecionadas"></div>
			<div id="<?=$m->campo?>_opcoesTitulo" class="d-none"><?=$m->titulo?></div>
			<div id="<?=$m->campo?>_opcoes" class="painelOpcoesPopup d-none">
				<div class="painelOpcoes">
					<?php mostraOpcao($frm, $m, $cmdo, $opcoes); ?>
				</div>
			</div>
		</div>
		<?php
	}
	?>
	</div>

	<?php if ($c && 1==2) { /* 1==2 é para deixar o mapa sem camadas ainda */ ?>
	<div class="d-none" style="cursor:auto">
		<p>Camadas</p>
		<div class="painelOpcoesMapaLista">
		<table>
		<tr>
		<td width="20px"><input type="checkbox" name="territorios_encontro_dialogos" id="territorios_encontro_dialogos" onClick="camadas(this,'<?=$geoXml['territorios_encontro_dialogos']?>',7,1)"></td>
		<td>Territórios</td>
		</tr>
		<tr>
		<td width="20px"><input type="checkbox" name="bioma" id="bioma" onClick="camadas(this,'<?=$geoXml['biomas']?>',0)"<?php /*if ($_REQUEST['bioma']) echo 'checked="checked"'*/?>></td>
		<td><?=$txt['Biomas']?></td>
		</tr>
		<tr>
		<td width="20px"><input type="checkbox" name="BrasilIndigena" onClick="camadas(this,'<?=$geoXml['BrasilIndigena']?>',1,1)"></td>
		<td><?=$txt['TIs']?></td>
		</tr>
		<tr>
		<td width="20px"><input type="checkbox" name="BrasilIndigenaLegenda" onClick="camadas(this,'<?=$geoXml['BrasilIndigenaLegenda']?>',2,1)"></td>
		<td style="white-space:nowrap"><?=$txt['TIs_nomes']?></td>
		</tr>
		<tr>
		<td width="20px"><input type="checkbox" name="UC" onClick="camadas(this,'<?=$geoXml['UC']?>',3,1)"></td>
		<td style="white-space:nowrap"><?=$txt['UCs']?></td>
		</tr>
		</table>
		</div>
	</div>
	<?php } ?>

	<script>
		if (typeof filtros =='undefined') {
			var filtros=eval('('+'<?=json_encode($filtrosJS)?>'+')');
			$(document).ready(function () {
				inicializaFiltros();
				$(".selectpicker").selectpicker("destroy");
			});
		}
	</script>
	<?php
}
function mostraOpcao(&$frm, $m, $cmdo, $opcoes) {
	switch ($m->tipo_form) {
		case 'checkbox':
		case 'radio':
			if ($opcoes) {
				foreach ($opcoes as $id=>$o) {
					$frm->inputs[$m->campo.'_'.$id]['EVENTS']['ONCLICK']=faz_atualizaSelecao($m->campo,$m->campo.'_'.$id, 0, 0, false, $id, $frm->inputs[$m->campo.'_'.$id]['LABEL']);
					$frm->AddInputPart($m->campo.'_'.$id); ?> <?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$id));
					?><br><?php
				}
			}
			break;
		case 'select_hierarquia':
			$nmaxsels=$cmdo->niveis;
			for ($i=0;$i<$nmaxsels;$i++) {
				if ($i==$nmaxsels-1) $tmp=$m->campo;
					else $tmp=$m->campo.'_'.$i;
				$frm->inputs[$tmp]['EVENTS']['ONCHANGE'].=faz_atualizaSelecao($m->campo,$tmp,1,0);
				$frm->AddLabelPart(array("FOR"=>$tmp)); ?>:
				<?php $frm->AddInputPart($tmp);
				?><br><?php
			}
			break;
		case 'georref':
			// Aqui vai toda a cascata de selects
			?>
			<?php for ($i=0;$i<=2;$i++) { ?>
				<?php $tmp = ($i==2) ? $m->campo : $m->campo.'_'.$i; ?>
				<?php $frm->inputs[$tmp]['EVENTS']['ONCHANGE'].=faz_atualizaSelecao($m->campo,$tmp,1,0); ?>
				<div class="form-group row">
					<label for="ex_cidade_ref_0" class="col-sm-4 col-md-3 col-form-label form-control-label text-truncate">
						<?php $frm->AddLabelPart(array("FOR"=>$tmp)); ?>
					</label>
					<div class="col-sm-8 col-md-9">
						<?php $frm->AddInputPart($tmp); ?>
					</div>
				</div>
			<?php } ?>
		<?php
			break;
		case 'arvore':
			$campo_id=$cmdo->campo_id;
			$campo_nome=$cmdo->campo_nome;
			$campo_mae=$cmdo->campo_mae;
			$arvore = resgata_arvore($cmdo->apoio,$campo_id,$campo_nome,$campo_mae);
			$nivel=0;
			$idmae[0]='';
			foreach ($arvore as $i=>$no) {
				if ($no->nivel>$nivel) {
						$nivel=$no->nivel;
						$idmae[$nivel] = $arvore[$i-1]->$campo_id;
						echo '<div id="div_'.$m->campo.'_'.$idmae[$nivel].'">';
					} elseif ($no->nivel<$nivel) {
						echo str_repeat('</div>',$nivel-$no->nivel);
						$nivel=$no->nivel;
					} ?>
				<?=str_repeat('&nbsp;',$no->nivel*8)?>
				<?php $frm->inputs[$m->campo.'_'.$no->$campo_id]['EVENTS']['ONCLICK']=faz_atualizaSelecao($m->campo,$m->campo.'_'.$no->$campo_id, 0, 0, false, $no->$campo_id, $frm->inputs[$m->campo.'_'.$no->$campo_id]['LABEL']);?>
				<?php $frm->AddInputPart($m->campo.'_'.$no->$campo_id); ?>
				<?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$no->$campo_id)); ?>
				<br />
				<?php
			}
			for ($i=0;$i<$nivel;$i++) {
				echo '</div>';
			}
			break;
	}
}

function mostraMapa($numpts,$urls,$ativ) {
	?>
	<div class="row d-flex flex-fill">
		<div class="col-md-12">
			<div id="map"></div>
			<div id="semResultados" class="d-none">
				<div class="alert alert-danger">
					Oooops: Não há resultados para a sua busca e/ou filtros aplicados.
				</div>
			</div>
			<!--<div id="legendaMapa" class="d-md-none d-sm-block"><?php /*mostraLegenda($numpts);*/ ?></div>-->
		</div>
	</div>
	<script>
		var url = <?=json_encode(reset($urls))?>;
		loadData();
	</script>
	<?php
}

function carregaResultados() {
	global $urls, $dados_get;
	if (MAPA) {
		$compl = '&conta=1';
	}
	foreach ($urls as $tipo=>$u) {
		if ($xmltmp=pega_xml_arquivo($u->url.$u->dados_get.$compl)) {
			$xml[]=$xmltmp;
		} else {
			unset($ativ[$tipo]);
		}
	}
	//pR($u->url.$u->dados_get.$compl);exit;
	// Aqui eu formato os dados passados pelo arquivo XML:
	return formata_dados_xml_agrorede($xml, (MAPA)?'mapa':'tabela');
}

function faz_atualizaSelecao($idCampo,$id,$select,$href=0,$texto=false,$valor='',$nome='',$soparams=false) {
	$r = new StdClass();
	$r->id = $id;
	$r->select = $select;
	$r->texto = $texto;
	$r->href = $href;
	if (!$select && !$texto) {
		$r->valor = $valor;
		$r->nome = $nome;
	}
	return ($soparams)
		? str_replace('"',"\'",json_encode($r))
		: "atualizaSelecao('".$idCampo."', '".str_replace('"',"\'",json_encode($r))."');";
}

function mostraLinhaDoTempo() {
	$sql = "SELECT DISTINCT ROW YEAR(dt_criacao) as data from ORDER BY dt_criacao";
	$anos = faz_query($sql,'','object');
	?>
	<div id="linhadotempo">
	<table
	</div>
<?php
/*
	<tr>
<td>
Linha do tempo: Ano de inclusão das experiências no sistema
</td>
<td>
<table width="100%"><tr>
<td style="text-align:center"><a href="<?=$_SERVER['SCRIPT_NAME'].'?'.$_SERVER['QUERY_STRING'
].'&dataf=2005&tipodata=ano'?>">antes de</a><br><b>2003</b><br><a href="#">em diante</a></td>
<td style="text-align:center"><a href="#">antes de</a><br><b>2004</b><br><a href="#">em diante</a></td>
<td style="text-align:center"><a href="#">antes de</a><br><b>2005</b><br><a href="#">em diante</a></td>
<td style="text-align:center"><a href="#">antes de</a><br><b>2006</b><br><a href="#">em diante</a></td>
<td style="text-align:center"><a href="#">antes de</a><br><b>2007</b><br><a href="#">em diante</a></td>
<td style="text-align:center"><a href="#">antes de</a><br><b>2008</b><br><a href="#">em diante</a></td>
<td style="text-align:center"><a href="#">antes de</a><br><b>2009</b><br><a href="#">em diante</a></td>
<td style="text-align:center"><a href="#">antes de</a><br><b>2010</b><br><a href="#">em diante</a></td>
<td style="text-align:center"><a href="#">antes de</a><br><b>2011</b><br><a href="#">em diante</a></td>
</tr></table>
</td>
</tr>
*/
}

?>
