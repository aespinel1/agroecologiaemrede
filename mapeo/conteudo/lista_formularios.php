<?php

$val_admin=intval($s->u->us_admin);
$tmp_tem_acoes=false;
$sql = "SELECT nomefrm, titulo, tab_base_comum FROM mapeo_formularios WHERE nomefrm='".$_REQUEST['nomefrm']."'";
$tmp = faz_query($sql,'','object');
$frm=$tmp[0];
if (!$publicada) {
	?>
	<script>
		function alerta(id) {
			var resposta = confirm('<?=$txt['confirma_apagar']?> '+id+'??');
			if (resposta)
				window.location = "index.php?f=apagar&id="+id+"&publicada=<?=strval($publicada)?>";
		}
	</script>
	<?php
}

// Loop no conjunto de formulários
//foreach ($frms as $tt=>$frm) {
	$th = $filtrosql = $sql = "";
  $fazlink = $filtroselect = array();
	$t = (isset($tt)) ? $tt+1 : 1;

	// Esta função retorna o valor do $mapa e do $cmd!
	$mapa=gera_mapa_campos($ano,$frm->nomefrm,$frm->tab_base_comum,$cmd);

	// Simplificação da variável $campo[$frm->nomefrm] dentro do loop, pois é muito usada:
	$cmpo = $campo[$frm->nomefrm];
	$mostrar = $cmpo->mostrar;

	if (isset($_GET['ord']))
		$ord=$_GET['ord'];

	// Se a pessoa não é admin, elimino os itens da variável "mostrar" que têm o atributo admin=1
	if (!$s->u->us_admin) {
		$mostrartmp = $mostrar;
		$mostrar=array();
		foreach ($mostrartmp as $k=>$mmc)
			if (!$mmc['admin'])
				$mostrar[]=$mmc;
	}


	// Tabela básica. Se há a base comum, é ela. Senão, é a nomefrm:
	$tab_basica = ($frm->tab_base_comum)
		? $frm->tab_base_comum
		: $frm->nomefrm;

	$h1=traduz($frm->titulo,$lingua);
	if ($publicada) {
		$th1='apagar';
		$acao='editar';
	} else {
		$acao=$th1='recuperar';
	}

	//Recupero o número total de formulários, para o caso de um recorte:
	$sql = ($frm->tab_base_comum)
		? 'SELECT '.$campo[$frm->tab_base_comum]->id.' FROM '.$frm->tab_base_comum.' WHERE publicada=1'
		: 'SELECT '.$cmpo->id.' FROM '.$frm->nomefrm.' WHERE publicada=1';
	$ntotal=faz_query($sql,'','num_rows');

	// Construção do sql que vai obter os campos a serem mostrados na tabela:
	$sql='SELECT ';
	$tabelas=' FROM '.$frm->nomefrm;
	if ($frm->tab_base_comum)
		$tabelas .= ', '.$frm->tab_base_comum;
	$wheres=' WHERE publicada='.intval($publicada).' AND ';
	if ($frm->tab_base_comum)
		$wheres .= $frm->nomefrm.'.'.$cmpo->id.'='.$frm->tab_base_comum.'.'.$campo[$frm->tab_base_comum]->id.' AND ';
	if (!$s->u->us_admin) {
		$wheres .= ($frm->tab_base_comum)
			? $frm->tab_base_comum : $frm->nomefrm;
		$wheres .= '.'.$cmpo->digitadora.' LIKE "'.$s->u->us_login.'" AND ';
	}
	$i=0;
	foreach ($mostrar as $mmc) {
		$mc=$mmc['campo'];
		// Resgate do campo id e os que devem ter combo de filtro no título do formulário na linguagem tX_c0, tX_c1, etc...:
		if ($mc==$cmpo->id)
			$cid=$cid='t'.$t.'_c'.$i;
		if ($mmc['combo']) {
			$filtroselect[$i]='t'.$t.'_c'.$i;
			// Aqui defino o filtro passado pelos títulos com select...
			if (isset($_GET['t'.$t.'_s'.$i]))
				$filtrosql .= ($cmd[$mostrar[$i]['campo']]->apoio)
					? ' AND '.$mostrar[$i]['campo'].'="'.$_GET['t'.$t.'_s'.$i].'"'
					: '';
		}
		// Aqui eu capturo a ordem a ser passada (qual campo será ordenado, e em qual sentido)
		if (isset($mmc['ord_padrao']) && !isset($_GET['ord']))
			$ord = 't'.$t.'_c'.$i;
		// Aqui defino os itens que terão links
		if (isset($mmc['href']))
			$fazlink[$i]=$mmc['href'];
		if ($mc!='pais/estado') {
			$pretab = (in_array($mc, array($cmpo->digitadora,'dt_criacao')))
				? $tab_basica.'.'
				: '';
			if ($mc==$cmpo->id)
				$pretab=$frm->nomefrm.'.';
			if (isset($cmd[$mc]->apoio)) {
				$campo_id = ($cmd[$mc]->campo_id) ? $cmd[$mc]->campo_id : 'id';
				$campo_nome = ($cmd[$mc]->campo_nome) ? $cmd[$mc]->campo_nome : 'nome';
				$tabelatmp = (isset($cmd[$mc]->bd)) ? $bd[$cmd[$mc]->bd].'.'.$cmd[$mc]->apoio : $cmd[$mc]->apoio;
				if (!strpos($tabelas, $tabelatmp)) $tabelas.=', '.$tabelatmp;
				$sql.= $tabelatmp.'.`'.$campo_nome.'` AS t'.$t.'_c'.$i.', ';
				// Se é um campo que terá combos, tenho que resgatar o id do campo, não só o nome...
				if ($mmc['combo'])
					$sql.= $tabelatmp.'.`'.$campo_id.'` AS t'.$t.'_s'.$i.', ';
				// Nos wheres eu vou colecionando os vínculos entre tabelas:
				$wheres.= $pretab.'`'.$mc.'`='.$tabelatmp.'.`'.$campo_id.'` AND ';
				$th[$i] = ($mapa[$mc]->titulocurto) ? $mapa[$mc]->titulocurto : $mapa[$mc]->titulo;
			}
			// Aqui é para poder ver a cidade e o pais/estado:
			elseif ($mc==$cmpo->localizacao) {
				$sql.='"{pais/estado}" AS t'.$t.'_c'.$i.', '.$mc.' AS t'.$t.'_c'.($i+1).', ';
				$loc_estado='t'.$t.'_c'.$i;
				$loc_cidade='t'.$t.'_c'.($i+1);
				$th[$i]=$txt['pais'].', '.$txt['estado'];
				// um extra, pois foram adicionados 2 campos:
				$i++;
				for ($j=count($mostrar);$j>$i;$j--) {
					$mostrar[$j]=$mostrar[$j-1];
				}
				$mostrar[$i]=array('campo'=>'municipio');

				$th[$i]=$txt['municipio'];
			} else {
				$sql .= ($mapa[$mc]->tipo_mysql=='date')
					? 'DATE_FORMAT('.$pretab.'`'.$mc.'`, "%d/%m/%Y") AS t'.$t.'_c'.$i.', '
					: $pretab.'`'.$mc.'` AS t'.$t.'_c'.$i.', ';
				$th[$i] = ($mapa[$mc]->titulocurto) ? $mapa[$mc]->titulocurto : $mapa[$mc]->titulo;
				if ($ord=='t'.$t.'_c'.$i && $mapa[$mc]->tipo_mysql=='date') {
          if (!isset($ordem)) {
            $ordem = array();
          }
					$ordem[$pretab.'`'.$mc.'`'] = isset($ordem[$ord]) ? $ordem[$ord] : null;
					$ord = $pretab.'`'.$mc.'`';
				}
			}
			$i++;
		} // fim do if é o próprio pais/estado...
	} // fim do loop dos campos a serem mostrados

	// Construção do sql:
	$ordsql=' ORDER BY '.$ord;
	if (!isset($_GET['t'.$t.'_ordem']) || !$_GET['t'.$t.'_ordem']) {
    $ordem[$ord]='&t'.$t.'_ordem=DESC';
  } else {
    unset($ordem[$ord]);
  }
	if (!isset($ordem[$ord]) || !$ordem[$ord]) {
    $ordsql.=' DESC';
  }
  if (!isset($filtrosql)) {
    $filtrosql = "";
  }
	$sql = substr($sql,0,strlen($sql)-2) . $tabelas . substr($wheres,0,strlen($wheres)-5) . $filtrosql . $ordsql;
	//pR($sql);exit;
	$dados=faz_query($sql,'','object');
	//foreach ($dados as $q) print_r($q);



?>
<div class="row mb-4">
	<div class="col-md-12">
		<div class="alert alert-success">
			<h1><i class="aericon-<?=str_replace('frm_','',$frm->nomefrm)=='instituicao' ? 'instituicoes' : 'experiencias'?>"></i> <?=$h1?></h1>
		</div>
	</div>
</div>


<?php if ($aviso) { ?>
	<div class="row mb-4">
		<div class="col-md-12">
			<div class="alert alert-success">
				<?=$aviso?>
			</div>
		</div>
	</div>
<?php } ?>

<a class="btn btn-secondary mb-4" href="index.php?f=formulario_geral&nomefrm=<?=$frm->nomefrm?>"><?=$txt['fazer_novo_cadastro']?></a>

<?php if (!count($dados)) { ?>
	<div class="row mb-4">
		<div class="col-md-12">
			<div class="alert alert-info">
				<?=$txt['sem_questionarios']?> <?=($mr) ? printf($txt['sem_questionarios_na_regiao'],$lista_MR[$mr]) : '<br> '.$txt['sem_questionarios2']?> <?=(!isset($publicada) || !$publicada) ? ' na lixeira' : ''?>
			</div>
		</div>
	</div>
<?php } else { ?>
		<?php if (isset($erro) && $erro) { ?>
			<div class="row mb-4">
				<div class="col-md-12">
					<div class="alert alert-danger">
						<?=$erro?>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php if ($s->u->us_admin && $tmp_tem_acoes) { ?>
			<form enctype="multipart/form-data" action="index.php?f=salvar<?php if (!$publicada) echo '&publicada=0'; ?>" method="POST">
		<?php } ?>
		<h5><?=$txt['frm_qtde_formularios']?>: <?=count($dados)?> <?php
			if (isset($mr) && isset($publicada) && $publicada) {
        echo 'na região '.$lista_MR[$mr];
      }
			if (isset($filtrosql)) echo ', de um total de '.$ntotal.' formulários inseridos.';
			?></h5>
			<?php
			$filtrostxt = '&';
			foreach($mostrar as $k=>$mmc)
				if (isset($_GET['t'.$t.'_s'.$k]) && isset($mmc['combo']) && $mmc['combo']) {
					$filtrostxt .= 't'.$t.'_s'.$k.'='.$_GET['t'.$t.'_s'.$k].'&';
					$mostrartmp = $cmd[$mostrar[$k]['campo']];
					$idtmp = (isset($mostrartmp->campo_id) && $mostrartmp->campo_id)
						? $mostrartmp->campo_id
						: 'id';
					$filtrosarraysql[$k] =
						' AND '.$mostrartmp->apoio.'.'.$idtmp.'="'.$_GET['t'.$t.'_s'.$k].'"'.
						' AND '.$tab_basica.'.'.$mostrar[$k]['campo'].
						'='.$mostrartmp->apoio.'.'.$idtmp;
					$filtrosarraytab[$k]=$mostrartmp->apoio;
				}
			$filtrostxt=substr($filtrostxt, 0, strlen($filtrostxt)-1);
			?>

			<div class="row mb-4">
				<div class="col-md-12 table-responsive-md">
			<table class="table table-striped table-light">
				<thead>
					<tr>
						<?php if ($s->u->us_admin && $tmp_tem_acoes) { ?>
							<th><?=$th1?></th>
							<th><?=$txt['frm_acoes']?></th>
						<?php
						} //fim do if ($edicao)
						// Aqui eu insiro os títulos da tabela:
						$i=0;
						if (is_array($th)) {
							foreach ($th as $ii=>$tit) {
							?>
							<th>
								<?php if (!in_array($tit, array($txt['pais'].', '.$txt['estado'], $txt['municipio']))) { ?>
									<a href="index.php?f=lista_formularios&nomefrm=<?=$frm->nomefrm?>&ord=t<?=$t?>_c<?=$i?><?= isset($ordem['t'.$t.'_c'.$i]) ? $ordem['t'.$t.'_c'.$i] : ""?><?=$filtrostxt?>">
									<?=traduz($tit,$lingua)?>
									</a>
								<?php } else {?>
									<?=traduz($tit,$lingua)?>
								<?php }
								// Se o título é pra ter um select embaixo de seleção de filtros, então tenho que construí-lo:
								if (isset($filtroselect[$i])) {
									$mostrartmp = $cmd[$mostrar[$i]['campo']];
									// Tenho que verificar quais os valores da tabela de apoio estão nos formulários mostrados. Construo a variável $listatmp para isso:
									$listatmp = array();
	                $from = $where = "";
									if (isset($mostrartmp->apoio) && $mostrartmp->apoio) {
										$tmp=$mostrartmp->apoio;
										$nometmp = (isset($mostrartmp->campo_nome) && $mostrartmp->campo_nome)
											? $mostrartmp->campo_nome
											: 'nome';
										$idtmp = (isset($mostrartmp->campo_id) && $mostrartmp->campo_id)
											? $mostrartmp->campo_id
											: 'id';
										$where=' WHERE '.$mostrar[$i]['campo'];
									}
									$sql='SELECT DISTINCTROW ';
									$sql.=$tmp.'.'.$idtmp.', '.$tmp.'.'.$nometmp.' FROM '.$frm->nomefrm.', '.$tmp;
									if ($frm->tab_base_comum)
										$sql .= ', '.$frm->tab_base_comum;

									$where.='='.$tmp.'.'.$idtmp;
									$where.=' AND publicada=1';
									if ($frm->tab_base_comum)
										$where .= ' AND '.$frm->nomefrm.'.'.$cmpo->id.'='.$frm->tab_base_comum.'.'.$campo[$frm->tab_base_comum]->id;
									if (!$s->u->us_admin)
										$where .= ' AND '.$cmpo->digitadora.' LIKE "'.$s->u->us_login.'"';
									if (isset($filtrosarraysql))
										foreach($filtrosarraysql as $k=>$ft)
											if ($k<>$i) {
												$where.=$ft;
												if ($filtrosarraytab[$k]) $from.=', '.$filtrosarraytab[$k];
											}
	                if (!isset($from)) {
	                  $from = "";
	                }
									$sql.=$from.$where.' ORDER BY '.$tmp.'.'.$nometmp;
									$tmp=faz_query($sql, '', 'array_assoc', $idtmp);
									foreach($tmp as $k=>$l)
										$listatmp[$k]=$l[$nometmp];

									$ff='t'.$t.'_s'.$i;

									// Apenas dou a opção de combo se houver mais de uma opção da tabela de apoio. Se houver apenas uma, deixa pra lá :)
									if (count($listatmp)>1) {
										$txttmp = '?f=lista_formularios&nomefrm='.$frm->nomefrm;
										foreach($mostrar as $k=>$mmc)
											if (isset($_GET['t'.$t.'_s'.$k]) && $k<>$i && isset($mmc['combo']) && $mmc['combo'])
													$txttmp .= '&t'.$t.'_s'.$k.'='.$_GET['t'.$t.'_s'.$k];
										echo '<select class="selectpicker" data-style="btn-info" name="select_'.$filtroselect[$i].'" onchange="MM_jumpMenu(\'parent\',this,0)">';
										echo '<option value="index.php'.$txttmp.'">---</option>';
										foreach($listatmp as $k=>$l) {
											echo '<option value="index.php'.$txttmp.'&'.$ff.'='.$k.'"';
											if (isset($_GET[$ff]) && $_GET[$ff]==$k) echo ' selected';
											echo '>'.traduz($l,$lingua).'</option>'.chr(10);
										}
										echo '</select>';
									} // fim do if count($listatmp)
								} // fim do if $filtroselect($i)
							$i++;
							?>
							</th>
							<?php } ?>
						<?php } ?>
					</tr>
				</thead>
				<tfoot></tfoot>
			<tbody>
	<?php
	$memoria=array();
	$alternador=FALSE;

	foreach ($dados as $q) {
		$idtmp=$q->$cid;
		//Aqui faço a alteração dos dados de estado/país e cidade, para corresponder aos nomes corretos. Acumulo numa variável as consultas realizadas para não precisar fazer novamente quando a próxima cidade for de um mesmo estado/país:
		if ($loc_cidade) {
			$id_cidade = ($q->$loc_cidade!='0' && $q->$loc_cidade!='')
				? $q->$loc_cidade
				: 'BR';
			//echo $q->$loc_cidade.' - '.$id_cidade.'<br><br>';
			if (in_array($id_cidade,array_keys($memoria))) {
				$q->$loc_cidade=$memoria[$id_cidade]['cidade'];
				$q->$loc_estado=$memoria[$id_cidade]['estado'];
			} elseif (in_array(substr($id_cidade,0,2),$paises)) {
					$pais=substr($id_cidade,0,2);
					$sql = "SELECT a.id, a.nome as cidade, CONCAT('".$pais."',', ',b.nome) as estado FROM _".$pais." as a, _".$pais."_maes as b WHERE a.id_mae=b.id AND a.id='".$id_cidade."'";
					$res=faz_query($sql,'','array_assoc','id');
				if ($res) {
					$q->$loc_cidade=$res[$id_cidade]['cidade'];
					$q->$loc_estado=$res[$id_cidade]['estado'];
					$memoria += $res;
				} else {
					$q->$loc_cidade='--';
					$q->$loc_estado=$id_cidade;
					$memoria[$id_cidade]['cidade']=$q->$loc_cidade;
					$memoria[$id_cidade]['estado']=$q->$loc_estado;
				}
			} else {
				$q->$loc_cidade=$id_cidade;
				$q->$loc_estado='--';
			}
		}


		//Aqui defino o que acontece quando a pessoa clica em apagar: se é definitivo, tem que vir uma confirmação! Um alerta!
		$ahref = ($publicada)
			? 'index.php?f=apagar&id='.$idtmp.'&publicada=1'
			: " javascript:alerta('".$idtmp."');";

		//Aqui eu faço a linha do formulário $q
		if ($alternador) echo '<tr class="sectiontableentry2">'.chr(10);
			else echo '<tr class="sectiontableentry1">'.chr(10);
		$alternador=!$alternador;
		if ($s->u->us_admin && $tmp_tem_acoes) {
			?>
			<td class="<?=$th1?>">
				<input type="checkbox" name="<?=$th1.'_'.$idtmp?>">
			</td>
			<td style="white-space: nowrap;">
				<a style="border:none" href="index.php?f=<?=$acao?>&nomefrm=<?=$frm->nomefrm?>&id=<?=$idtmp?>&publicada=<?=strval($publicada)?>"><img src="imgs/<?=$acao?>.png" border=0></a>
				<a style="border:none" href="<?=$ahref?>"><img src="imgs/apagar.png" border=0></a>
			</td>

		<?php
		} // fim do if ($edicao)

		// Aqui eu insiro os campos do formulário $q:
		$i=0;
		unset($q->cmr);
		foreach ($q as $k=>$c) {
			// Este if abaixo é para que só apareçam as colunas começadas com "c", que são de dados. As que começam com "s" são informação para os selects dos títulos (ou seja, os ids para os casos que têm tabela de apoio)!
			if (isset($k[3]) && $k[3]=='c') {
				$ff = 't'.$t.'_s'.$k[4];
				if (isset($publicada) && $publicada && (isset($fazlink[$k[4]]) && $fazlink[$k[4]])) {
          $linktmp=str_replace('{id}',$q->$cid,$fazlink[$k[4]]);
          if (isset($q->$ff)) {
            $linktmp=str_replace('{s}',$q->$ff,$linktmp);
          }
          if (isset($k)) {
            $linktmp=str_replace('{c}',$k,$linktmp);
          }
					$linktmp=str_replace('{nomefrm}',$frm->nomefrm,$linktmp);
					$pre = '<a title="'.$txt['clique_para_editar'].'" alt="'.$txt['clique_para_editar'].'" href="'.$linktmp.'">';
					//$w=' width="50%"';
					$w='';
					$pos='</a>';
				} else {
          $pre = $pos = $w = "";
        }
				$i++;
				?>
				<td<?=$w?>><?=$pre.traduz($c,$lingua).$pos?></td>
			<?php }
		}
		echo '</tr>'.chr(10);
	} // fim do loop que mostra os formulários

	?>
	</tbody>
	</table>
	</div>
	</div>
	<?php if ($s->u->us_admin && $tmp_tem_acoes) { ?>
		<input class="btn btn-primary" type="submit" name="processar" value="<?=$th1?> selecionados">
		<?php if (!$publicada) { ?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" class="btn btn-danger" name="processar" value="esvaziar a lixeira">
		<?php } ?>
		</form>
	<?php }
	} //fim do if tem linhas para mostrar...
	?><?php
//} // Fim do loop no conjunto de formulários


?>
