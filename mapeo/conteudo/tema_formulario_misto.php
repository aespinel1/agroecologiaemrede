<?php

//Campo de seleção rápida se o $id não está especificado...
if (!$id && in_array('selecao_rapida', $frm->GetInputs())) { ?>
	<h1>Seleção rápida de formulários</h1>
	<div class="conteudo">
	<p class="form">
	<?php if(IsSet($verify["selecao_rapida"])) echo '<span class="verificar">'.$txt['verificar'].'</span>';
	$frm->AddLabelPart(array("FOR"=>"selecao_rapida"));
	echo ': ';
	$frm->AddInputPart("selecao_rapida"); ?>
	</p>
	</div>
<?php }

$primeiro=true;
$primeirobloco=true;
$alterna=false;
$c=0;

$fecha_subbloco='</table>';
$abre_subbloco='<table class="sub_bloco">';
$abre_linha='<tr ';
$fecha_linha='</tr>';
$abre_celula='<td ';
$fecha_linha='</td>';

foreach ($mapa as $m) {
	unset($opsform, $cmdo);
	// Como sempre uso o cmd[$m->campo] vamos economizar um pouco:
	$cmdo=$cmd[$m->campo];
	if (!isset($cmdo->campo_nome) || !$cmdo->campo_nome) $cmdo->campo_nome='nome';
	if (!isset($cmdo->campo_id) || !$cmdo->campo_id) $cmdo->campo_id='id';
	// Aqui eu fecho e abro os divs correspondentes de bloco e sub_bloco!
	if ($m->bloco) {
		$primeiralinha='primeira';
		$alterna=false;
		if (!$primeiro) {
			if ($sub_bloco) echo $fecha_subbloco;
			$classe_linha=' style="clear:both"';
			/*?>
			<div class="botao">
				<?php if ($pg>1) $frm->AddInputPart("voltar"); echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
				<?php $frm->AddInputPart("enviar"); ?>
			</div>	*/ ?>
			</div> <!-- fim de bloco de conteúdo //-->
		<?php } else $primeiro=false; ?>

		<h1><?php echo $m->bloco; ?></h1>
		<div class="conteudo">
		<?php if ($m->sub_bloco) { ?>
			<h2><?php echo $m->sub_bloco; ?></h2>
			<?php echo $abre_subbloco; ?>
			<?php $sub_bloco=true; ?>
		<?php } else $sub_bloco=false;
	} elseif ($m->sub_bloco) {
		$alterna=false;
		if ($sub_bloco) echo $fecha_subbloco;
		?>
		<h2><?php echo $m->sub_bloco; ?></h2>
		<?php echo $abre_subbloco; ?>
		<?php
		$sub_bloco=true;
	}
	if (!$m->bloco) $primeiralinha='';
	if ($m->tipo_form=='hidden' && $dados[$m->campo]) $frm->AddInputPart($m->campo);
	elseif (!in_array($m->tipo_form,array('hidden', 'ignorar'))) {

	echo $abre_linha; ?>class="<?php echo $primeiralinha; ?>linha<?php if ($alterna) echo '_a'; ?>"<?php echo $classe_linha; ?>>
	<?php
	//Agora insiro o(s) input(s) (addpart) deste campo de acordo com o mapa_campos
	switch ($m->tipo_form) {
		case 'checkbox':
		case 'radio':
			// $opcoes é o array que tem as opções possíveis do checkbox ou select. É do tipo valor=>texto, a partir da tabela auxiliar.
			$tabapoiotmp = ($cmd[$m->campo]->bd) ? $bd[$cmd[$m->campo]->bd].'.'.$cmd[$m->campo]->apoio : $cmd[$m->campo]->apoio;
			$opcoes=faz_select($tabapoiotmp,$cmdo->campo_id,'',$cmdo->campo_nome, $cmd[$m->campo]->order, '',false);

			// $max indica o limiar para o campo virar select. Menor que $max é radio ou checkbox
			$max = ($cmdo->nmax_opcoes) ? $cmd[$m->campo]->nmax_opcoes : $padrao_nmax_opcoes;
			//Se há mais opções que o $max, então teremos forçosamente um elemento SELECT, e não CHECKBOX:
			if (!$opcoes) $opcoes=array();
			$num_opcoes=count($opcoes);
			$select=($num_opcoes>$max || !$num_opcoes);

			// Se é select, é apenas um elemento:
			if ($select) {
				// Lado direito:
				$htmldir=$abre_celula.'class="dir" id="dir'.$c.'">';
				$htmldir.=$frm->AddInputPart($m->campo);
				if ($cmd[$m->campo]->outro && $edicao) {
					if ($cmd[$m->campo]->outro_divisoria=='br' || $m->repeticoes) $htmldir.='<br>';
						else $htmldir.='&nbsp;&nbsp;&nbsp;&nbsp;';
					if ($m->repeticoes) for ($i=0;$i<$m->repeticoes;$i++) {
						$htmldir.='Outro '.($i+1).': ';
						$htmldir.=$frm->AddInputPart($m->campo.'_outro'.$i).'<br>';
					} else {
						$htmldir.='Outro: ';
						$htmldir.=$frm->AddInputPart($m->campo.'_outro');
					}
				}
				$htmldir.=$fecha_celula;
				echo $htmldir;exit;

				// Lado esquerdo:
				$htmlesq=$abre_celula.' class="esq" id="esq'. $c.'">';
				$htmlesq.=$frm->AddLabelPart(array("FOR"=>$m->campo));
				if ($cmd[$m->campo]->outro_divisoria=='br' || $m->repeticoes) {
					$htmlesq.='<br>';
					/*$tmp = ($cmd[$m->campo]->size) ? $cmd[$m->campo]->size : $padrao_size_select_multiplo;
					for ($i=1;$i<($m->repeticoes+$tmp+2);$i++) echo '<br>';*/
				}
				$htmlesq.=$fecha_celula;

			// Se é checkbox ou radio, temos que fazer um AddInputPart para cada opção
			} else {
			/* Além disso, se tem opção 'outro' e é radio, tenho que dar a oportunidade do usuário escolher um
				rádio vazio ("outro...") */
			if ($cmd[$m->campo]->outro && $m->tipo_form=='radio' && $edicao) {
				$opcoes=$opcoes+array(-9999=>'Outro...');
				$num_opcoes++;
			}
				$id_valores=array_keys($opcoes);

				//Lado direito:
				$htmldir=$abre_celula.' class="dir" id="dir'.$c.'">';
				$htmldir.='<table class="multiplos">';
				$thmldir.='<tr>';
				// A determinação do número de colunas depende da quantidade de opções. É importante contar com um a mais, onde ficará o "outro..."
				$numcols = ($cmd[$m->campo]->numcols) ? $cmd[$m->campo]->numcols : $padrao_numcols;
				$n=0;
				for ($i=0;$i<$num_opcoes;$i++) {
					if ($n==$numcols) {
						$htmldir.='</tr><tr>';
						$n=0;
					}
					$n++;
					$htmldir.='<td>';
					$htmldir.=$frm->AddInputPart($m->campo.'_'.$id_valores[$i]).' '.$frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$id_valores[$i]));
					$htmldir.='</td>';
				} // fim do loop nas opções
				//Se é pra colocar "outro...", coloquemos!
				if ($cmd[$m->campo]->outro && $edicao) {
					if ($n==$numcols) {
						$n=1;
						$htmldir.='</tr><tr>';
					}
					if ($m->repeticoes) {
						for ($i=0;$i<$m->repeticoes;$i++) {
							if ($n==$numcols) {
								$n=1;
								$htmldir.='</tr><tr>';
							}
							$n++;
							$htmldir.='<td>';
							$htmldir.='Outro '.($i+1).':  '.$frm->AddInputPart($m->campo.'_outro'.$i);
							$htmldir.='</td>';
						} // fim do loop nas repetições do "outro".
					} else {
						$n++;
						$htmldir.='<td>';
						$htmldir.=$frm->AddLabelPart(array("FOR"=>$m->campo.'_outro')).' '.$frm->AddInputPart($m->campo.'_outro');
						$htmldir.='</td>';
					} // fim do if $m->repeticoes
				} // fim do if cmd['outro
				//Aqui tenho que acrescentar <td>s vazios pra fechar a tabela:
				while ($n<$numcols) {
					$n++;
					$htmldir.='<td>&nbsp;</td>';
				}
				$htmldir.='</tr>';
				$htmldir.='</table>';
				$htmldir.=$fecha_celula;

				//Lado esquerdo:
				$htmlesq=$abre_celula.' class="esq" id="esq'.$c.'">';
				$htmlesq.=$m->titulo;
				$htmlesq.=$fecha_celula;
			} // fim do else (ou seja, fim da produção do checkbox ou select)

		break;

		case 'select_hierarquia':
			$nmaxsels=$cmd[$m->campo]->niveis;
			// Aqui vai toda a cascata de selects

			//Lado direito
			$htmldir=$abre_celula.'class="dir" id="dir'.$c.'">';
			$htmldir.='<div id="carregando">Carregando a lista de valores...</div>';
			for ($i=0;$i<$nmaxsels;$i++) {
				if ($i==$nmaxsels-1) $tmp=$m->campo;
					else $tmp=$m->campo.'_'.$i;
				if ($i % 2==$i/2) $htmldir.='<br>';
					else $htmldir.='&nbsp;';
				$htmldir.=$frm->AddLabelPart(array("FOR"=>$tmp)).': '.$frm->AddInputPart($tmp);
			}
			$htmldir.=$fecha_celula;

			//Lado esquerdo:
			$htmlesq=$abre_celula.'class="esq" id="esq'.$c.'">'.$m->titulo.$fecha_celula;
		break;

		case 'text':
		case 'textarea':
		case 'data':
			//Lado direito:
			$htmldir=$abre_celula.'class="dir" id="dir'.$c.'">';
			$htmldir.=$frm->AddInputPart($m->campo);
			$htmldir.=$fecha_celula;

			//Lado esquerdo:
			$htmlesq=$abre_celula.'class="esq" id="esq'.$c.'">';
			$htmlesq.=$frm->AddLabelPart(array("FOR"=>$m->campo));
			$htmlesq.=$fecha_celula;
		break;
	} // FIM DO SWITCH
	if ($usa_nifty)
		echo $htmldir.$htmlesq;
		else echo $htmlesq.$htmldir;

	echo $fecha_linha;

	$alterna=!$alterna; // Para alternar o div dos inputs
	$c++;
	} // Fim do else do if (HIDDEN)!
} // Fim do loop pelos campos todos da tabela $tab
if ($sub_bloco) echo $fecha_subbloco; ?>
<div class="botao">
	<?php if ($pg>1) $frm->AddInputPart("voltar"); echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
	<?php $frm->AddInputPart("enviar"); ?>
</div>
</div> <!-- fim do bloco de conteúdo //-->
<?php
