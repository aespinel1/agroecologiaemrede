<?php

//Campo de seleção rápida se o $id não está especificado (vou deixar apenas para o admin)...
if (!$id && in_array('selecao_rapida', $frm->GetInputs()) && $s->u->us_admin) { ?>
	<div class="row">
		<div class="col-md-12">
      <span class="anchor" id="selecao_rapida"></span>
      <div class="card border-primary mb-4">
        <div class="card-header">
          <h3 class="mb-0"><?=$txt['titulo_selecao_rapida']?></h3>
        </div>
        <div class="card-body">
					<div class="form-group row">
						<?php
						if(IsSet($verify["selecao_rapida"])) {
							echo '<span class="verificar">'.$txt['verificar'].'</span>';
						}
						$frm->AddLabelPart(array("FOR"=>"selecao_rapida", "CLASS"=>"col-sm-12 col-md-3 col-form-label form-control-label"));
						echo ': ';
						?>
						<div class="col-sm-12 col-md-9">
							<?=$frm->AddInputPart("selecao_rapida")?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }

//Campo de seleção de vínculo se o $id não está especificado...
if (!$id && in_array('selecao_vinculo', $frm->GetInputs())) { ?>
	<div class="row">
		<div class="col-md-12">
      <span class="anchor" id="selecao_rapida"></span>
      <div class="card border-primary mb-4">
        <div class="card-header">
          <h3 class="mb-0"><?=$vinculotitulo?></h3>
        </div>
        <div class="card-body">
					<div class="form-group row">
						<?php
						if(IsSet($verify["selecao_rapida"])) {
							echo '<span class="verificar">'.$txt['verificar'].'</span>';
						}
						$frm->AddLabelPart(array("FOR"=>"selecao_vinculo", "CLASS"=>"col-sm-12 col-md-3 col-form-label form-control-label"));
						echo ': ';
						?>
						<div class="col-sm-12 col-md-9">
							<?=$frm->AddInputPart("selecao_vinculo")?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php }

$primeiro=true;
$primeirobloco=true;
$alterna=false;
$c=0;
foreach ($mapa as $m) {
//if 	((!$cmd[$m->campo]->admin || $cmd[$m->campo]->mostra) || ($cmd[$m->campo]->admin && $s->u->us_admin)) {
	unset($opsform, $cmdo);
	// Como sempre uso o cmd[$m->campo] vamos economizar um pouco:
	$cmdo=$cmd[$m->campo];
	if (!$cmdo) {
		$cmdo = new StdClass();
	}
	// Aqui, se a pessoa não é admin e é um campo admin que não deve ser mostrado, o tipo de form é hidden:
	if 	(
	(!$cmdo->mostra) && (
	($cmdo->admin && !$s->u->us_admin) ||
	($cmdo->naoadmin && $s->u->us_admin)
	) )
		$m->tipo_form='hidden';

	if (!isset($cmdo->campo_nome) || !$cmdo->campo_nome) $cmdo->campo_nome='nome';
	if (!isset($cmdo->campo_id) || !$cmdo->campo_id) $cmdo->campo_id='id';
	// Aqui eu fecho e abro os divs correspondentes de bloco e sub_bloco!
	if ($m->bloco) {
		$primeiralinha='primeira';
		$alterna=false;
		if ($m->sub_bloco) {
			$div_h1='';
			$div_h2=' id="bloco_'.$m->campo.'"';
		} else {
			$div_h1=' id="bloco_'.$m->campo.'"';
		}
		if (!$primeiro) {
			?>
		</div>
		</div> <!-- fim de card-body -->
		</div> <!-- fim de card -->
			</div> <!-- fim col-md-12 //-->
			</div> <!-- fim de row //-->
		<?php } else {
			$primeiro=false;
		} ?>
		<div<?=$div_h1?> class="row">
		<div class="col-md-12">
			<span class="anchor" id="selecao_rapida"></span>
			<div class="card border-primary mb-4">
				<div class="card-header">
					<h3 class="mb-0"><?=$m->bloco?></h3>
				</div>
				<div class="card-body">
		<?php if ($m->sub_bloco) { ?>
			<div class="sub_bloco" <?=$div_h2?>>
			<?php $sub_bloco=true; ?>
		<?php } else {
			$sub_bloco=false;
			?>
			<div <?=$div_h2?>>
			<?php
		}
	} elseif ($m->sub_bloco) {
		$alterna=false;
		$div_h2=' id="sub_bloco_'.$m->campo.'"';
		echo '</div>';
		?>
		<div <?=$div_h2?>>
		<?php
		$sub_bloco=true;
	}
	if (!$m->bloco) {
		$primeiralinha='';
	}
	if ($m->tipo_form=='hidden' && ($dados[$m->campo] || $cmdo->padrao)) {
		$frm->AddInputPart($m->campo);
	} elseif (!in_array($m->tipo_form,array('hidden', 'ignorar')) && !$cmdo->ignorar) {
		if ($sub_bloco) {
			$classeRow = "col-sm-12 col-md-9 offset-md-3 card";
		} else {
			echo ($primeiralinha || $sub_bloco) ? '' : '<hr>';
			$classeRow = "form-group row mb-2";
		}
	?>

	<!-- div que envelopa a linha do campo -->
	<div class="<?=$classeRow?>">

	<?php
	//Agora insiro o(s) input(s) (addpart) deste campo de acordo com o mapa_campos
	switch ($m->tipo_form) {
		case 'checkbox':
		case 'radio':
			// $opcoes é o array que tem as opções possíveis do checkbox ou select. É do tipo valor=>texto, a partir da tabela auxiliar.
			$tabapoiotmp = ($cmd[$m->campo]->bd) ? $bd[$cmd[$m->campo]->bd].'.'.$cmd[$m->campo]->apoio : $cmd[$m->campo]->apoio;
			$opcoes=faz_select($tabapoiotmp,$cmdo->campo_id,'',$cmdo->campo_nome, $cmd[$m->campo]->order, '',false);

			// $max indica o limiar para o campo virar select. Menor que $max é radio ou checkbox
			$max = ($cmdo->nmax_opcoes) ? $cmd[$m->campo]->nmax_opcoes : $padrao_nmax_opcoes;
			//Se há mais opções que o $max, então teremos forçosamente um elemento SELECT, e não CHECKBOX:
			if (!$opcoes) $opcoes=array();
			$num_opcoes=count($opcoes);
			$select=($num_opcoes>$max || !$num_opcoes);

			// Se é select, é apenas um elemento:
			if ($select) {
				?>
				<?php $frm->AddLabelPart(array("FOR"=>$m->campo, "CLASS"=>"col-sm-12 col-md-3 col-form-label form-control-label"));
				/*if ($cmd[$m->campo]->outro_divisoria=='br' || $m->repeticoes) {
					echo '<br>';
					//$tmp = ($cmd[$m->campo]->size) ? $cmd[$m->campo]->size : $padrao_size_select_multiplo;
					for ($i=1;$i<($m->repeticoes+$tmp+2);$i++) echo '<br>';
				}*/ ?>
				<div class="col-sm-12 col-md-9">
					<?php $frm->AddInputPart($m->campo); ?>
					<?php if ($cmd[$m->campo]->outro && $edicao) {
						/*if ($cmd[$m->campo]->outro_divisoria=='br' || $m->repeticoes) {
							echo '<br>';
						} else {
							echo '&nbsp;&nbsp;&nbsp;&nbsp;';
						}*/
						if ($m->repeticoes) { ?>
							<?php for ($i=0;$i<$m->repeticoes;$i++) { ?>
								<div class="form-group row">
									<div class="col-sm-3 col-md-3 col-lg-2 col-form-label form-control-label">
										<?=$txt['frm_outro'].' '.($i+1).': '?>
									</div>
									<div class="col-sm-9 col-md-9 col-lg-10">
										<?=$frm->AddInputPart($m->campo.'_outro'.$i)?>
									</div>
								</div>
							<?php } ?>
					<?php } else { ?>
						<div class="form-group row">
							<div class="col-sm-3 col-md-3 col-lg-2 col-form-label form-control-label">
								<?=$txt['frm_outro'].': '?>
							</div>
							<div class="col-sm-9 col-md-9 col-lg-10">
								<?=$frm->AddInputPart($m->campo.'_outro')?>
							</div>
						</div>
					<?php } ?>
				<?php } ?>
			</div>
			<?php
			// Se é checkbox ou radio, temos que fazer um AddInputPart para cada opção
			} else {
			/* Além disso, se tem opção 'outro' e é radio, tenho que dar a oportunidade do usuário escolher um
				rádio vazio ("outro...") */
			if ($cmd[$m->campo]->outro && $m->tipo_form=='radio' && $edicao) {
				$opcoes=$opcoes+array(-9999=>$txt['frm_outro'].'...');
				$num_opcoes++;
			}
				$id_valores=array_keys($opcoes);

				?>
				<div class="col-sm-12 col-md-3 col-form-label form-control-label">
					<?=$m->titulo?>
				</div>

				<div class="col-sm-12 col-md-9">
				<table class="multiplos">
					<tr>
					<?php
					// A determinação do número de colunas depende da quantidade de opções. É importante contar com um a mais, onde ficará o "outro..."
					$numcols = ($cmd[$m->campo]->numcols) ? $cmd[$m->campo]->numcols : $padrao_numcols;
					$n=0;
					for ($i=0;$i<$num_opcoes;$i++) {
						if ($n==$numcols) {
							?>
							</tr>
							<tr>
							<?php
							$n=0;
						}
						$n++;
						?>
						<td>
							<?php $frm->AddInputPart($m->campo.'_'.$id_valores[$i]); ?> <?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$id_valores[$i])); ?>
						</td>
					<?php
					} // fim do loop nas opções
					//Se é pra colocar "outro...", coloquemos!
					if ($cmd[$m->campo]->outro && $edicao) {
						if ($n==$numcols) {
							$n=1;
							?>
							</tr>
							<tr>
							<?php
						}
						if ($m->repeticoes) {
							for ($i=0;$i<$m->repeticoes;$i++) {
								if ($n==$numcols) {
									$n=1;
									?></tr><tr><?php
								}
								$n++;
								?>
								<td>
								<?php echo $txt['frm_outro'].' '.($i+1).': '; ?> <?php $frm->AddInputPart($m->campo.'_outro'.$i); ?>
								</td>
								<?php
							} // fim do loop nas repetições do "outro".
						} else {
							$n++;
							?>
							<td>
								<?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_outro')); ?> <?php $frm->AddInputPart($m->campo.'_outro'); ?>
							</td>
							<?php
						} // fim do if $m->repeticoes
					} // fim do if cmd['outro']
					//Aqui tenho que acrescentar <td>s vazios pra fechar a tabela:
					while ($n<$numcols) {
						$n++;
						?>
						<td>&nbsp;</td>
						<?php
					}
					?>
				</tr>
				</table>
				<?php if ($m->ajuda) { ?>
					<small class="form-text text-muted"><?=$m->ajuda?></small>
				<?php } ?>
			</div>

				<?php
			} // fim do else (ou seja, fim da produção do checkbox ou select)

		break;
		case 'select_hierarquia':
			$nmaxsels=$cmd[$m->campo]->niveis;
			// Aqui vai toda a cascata de selects
			?>
			<div class="col-sm-12 col-md-3 col-form-label form-control-label">
				<?=$m->titulo?>
				<?php if ($m->ajuda) { ?>
					<small class="form-text text-muted"><?=$m->ajuda?></small>
				<?php } ?>
			</div>

			<div class="col-sm-12 col-md-9">
			<div id="carregando_<?=$m->campo?>" class="carregando"><?=$txt['carregando_lista_valores']?></div>
			<?php
			for ($i=0;$i<$nmaxsels;$i++) {
				if ($i==$nmaxsels-1) $tmp=$m->campo;
					else $tmp=$m->campo.'_'.$i;
				echo ( ($i % 2==$i/2) || $m->repeticoes) ? '' : '&nbsp;';
				if ($m->repeticoes && $i>0) {
					$divespeciali = '<div style="margin:10px 0 0 0;padding:0;clear:both">';
					$divespecialf = '</div>';
				} else unset($divespeciali,$divespecialf); ?>
				<?=$divespeciali?>
				<?php $frm->AddLabelPart(array("FOR"=>$tmp)); ?>:
				<?=$divespecialf?>
				<?php $frm->AddInputPart($tmp);
			}
			?>
		</div>
			<?php
		break;
		case 'georref':
			// Aqui vai toda a cascata de selects
			?>
			<div class="col-sm-12 col-md-3 col-form-label form-control-label">
				<?=$m->titulo?>
				<?php if ($m->ajuda) { ?>
					<small class="form-text text-muted"><?=$m->ajuda?></small>
				<?php } ?>
			</div>

			<div class="col-sm-12 col-md-9">
			<div id="carregando_<?=$m->campo?>" class="carregando"></div>
			<?php
			for ($i=0;$i<=2;$i++) {
				if ($i==2) $tmp=$m->campo;
					else $tmp=$m->campo.'_'.$i;
				echo ( ($i % 2==$i/2) || $m->repeticoes) ? '' : '&nbsp;';
				if ($m->repeticoes && $i>0) {
					$divespeciali = '<div style="margin:10px 0 0 0;padding:0;clear:both">';
					$divespecialf = '</div>';
				} else {
					unset($divespeciali,$divespecialf);
				} ?>
				<div class="form-group row">
					<?php $frm->AddLabelPart(array("FOR"=>$tmp, "CLASS"=>"col-sm-4 col-md-3 col-form-label form-control-label text-truncate")); ?>
					<div class="col-sm-8 col-md-9">
						<?php $frm->AddInputPart($tmp);?>
					</div>
				</div>
			<?php } ?>
		</div>
			<?php
		break;
		case 'file':
			?>
			<div class="col-sm-12 col-md-3 col-form-label form-control-label">
			<?=$m->titulo?>
				<?php if ($m->ajuda) { ?>
					<small class="form-text text-muted"><?=$m->ajuda?></small>
				<?php } ?>
			</div>
			<div class="col-sm-12 col-md-9">
			<?php
			$txturl = ($cmdo->link)
				? ''
				: $dir['upload_URL'];

			for ($i=0;$i<($m->repeticoes+1);$i++) {
			//pR($frm->inputs);exit;
				if ($frm->inputs['hidden_'.$m->campo.'_'.$i]) {
					?>
					<?php $frm->AddInputPart('hidden_'.$m->campo.'_'.$i); ?>
					<a id="a_<?=$m->campo.'_'.$i?>" target="_blank" href="<?=$txturl.$frm->inputs[$m->campo.'_'.$i]['VALUE']?>"><?=$frm->inputs['hidden_'.$m->campo.'_'.$i]['VALUE']?></a>
					<?php $frm->AddInputPart($m->campo.'_'.$i); ?> <?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$i)); ?>
					<br /><br />
					<?php
				} else {
					$frm->AddInputPart($m->campo.'_'.$i); ?><br /><br />
				<?php }
			} ?>
		</div>
			<?php
		break;
		case 'text':
		case 'textarea':
		case 'data':
			?>
			<?php $frm->AddLabelPart(array("FOR"=>$m->campo, "CLASS"=>"col-sm-12 col-md-3 col-form-label form-control-label")); ?>
			<div class="col-sm-12 col-md-9">
			<?php if ($m->tipo_form=='file' && $frm->inputs[$m->campo]['VALUE']) {
				?>
				<a target="_blank" href="<?=$dir['upload_URL'].$frm->inputs[$m->campo]['VALUE']?>"><?=$frm->inputs[$m->campo]['VALUE']?></a><br /><br />
			<?php } ?>
			<?php $frm->AddInputPart($m->campo); ?>
			</div>

			<?php
		break;
		case 'arvore':
			$campo_id=$cmdo->campo_id;
			$campo_nome=$cmdo->campo_nome;
			$campo_mae=$cmdo->campo_mae;
			$arvore = resgata_arvore($cmdo->apoio,$campo_id,$campo_nome,$campo_mae);
			$nivel=0;
			$idmae[0]='';
			?>
			<div class="col-sm-12 col-md-3 col-form-label form-control-label">
				<?=$m->titulo?>
				<?php if ($m->ajuda) { ?>
					<small class="form-text text-muted"><?=$m->ajuda?></small>
				<?php } ?>
			</div>
			<div class="col-sm-12 col-md-9">
				<ul id="tree_<?=$m->campo?>">
					<?php foreach ($arvore as $i=>$no) { ?>
						<?php
						if ($no->nivel>$nivel) {
							$nivel=$no->nivel;
							$idmae[$nivel] = $arvore[$i-1]->$campo_id;
							echo '<div id="div_'.$m->campo.'_'.$idmae[$nivel].'">';
						} elseif ($no->nivel<$nivel) {
							echo str_repeat('</div>',$nivel-$no->nivel);
							$nivel=$no->nivel;
						} ?>
						<li class="tree-item list-unstyled">
							<?=str_repeat('<span class="mr-4 border-left border-info"></span>',$nivel)?>
							<?php $frm->AddInputPart($m->campo.'_'.$no->$campo_id); ?>
							<?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$no->$campo_id)); ?>
						</li>
					<?php } ?>
					<?php for ($i=0;$i<$nivel;$i++) { ?>
						</div>
					<?php } ?>
				</ul>
			</div>
		<?php
		break;
	} // FIM DO SWITCH
	?>
</div>
	<?php
	$alterna=!$alterna; // Para alternar o div dos inputs
	$c++;
	} // Fim do else do if (HIDDEN)!
//} // Fim do if que filtra os campos de admin para só o admin ver!
} // Fim do loop pelos campos todos da tabela $tab
?>
</div>
</div> <!-- fim do bloco de conteúdo //-->
</div> <!-- fim de bloco em torno do H1 //-->
<div class="col-md-12 text-center">
	<?php if ($pg>1) $frm->AddInputPart("voltar"); echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
	<?php $frm->AddInputPart("enviar"); ?>
</div>

<?php
