<?php
//	$tituloform.=' / PARTE '.$pg.'<br>';
	$tituloform = ($id)
		? $tituloform.' <span class="badge badge-light text-wrap">'.$txt['edicao_formulario'].': <i>'.$dados[$campo[$FrmAno]->nome].'</i> (id='.$id.')</span>'
		: $tituloform.' <span class="badge badge-light">'.$txt['novo_formulario'].'</span>';

	$frm->StartLayoutCapture();

	//Se houve erro de validação, deve ser mostrado o erro
	if($error_message)
	{
 		$active = 0;
 		?>
 		<p class="alerta"><?=$txt['alerta_erro_validacao']?><br>
 		<i><?=$error_message?></i>
 		</p>
<?php
	}
?>
		<div class="row">
	    <div class="col-md-12">
				<div class="alert alert-success">
					<span class="anchor" id="selecao_rapida"></span>
					<h1><i class="aericon-<?=str_replace('frm_','',$nomefrm)=='instituicao' ? 'instituicoes' : 'experiencias'?>"></i> <?=$tituloform?></h1>
				</div>
	    </div>
			<?php if (isset($txt['intro_edicao_form']) && $txt['intro_edicao_form']) { ?>
				<div class="col-md-12">
					<div class="alert alert-info">
						<?=$txt['intro_edicao_form']?>
					</div>
		    </div>
			<?php } ?>
		<?php if ($aviso) {
			?>
			<div class="col-md-12">
				<div class="alert alert-danger">
					<?=$aviso?>
				</div>
			</div>
			<?php
			unset($aviso);
		}
		?>
		</div>
		<?php include "tema_formulario_bootstrap.php"; ?>
		<?php $frm->AddInputPart('processe'); ?>
		<?php $frm->AddInputPart('nomefrm'); ?>
		<?php $frm->AddInputPart('pg'); ?>
		<?php if ($id) $frm->AddInputPart('id_hidden'); ?>
		<?php if ($novofrm || !$id) $frm->AddInputPart('novofrm'); ?>

<?php
	$frm->EndLayoutCapture();

/*
 * Output the form using the function named Output.
 */
	$frm->DisplayOutput();
?>
