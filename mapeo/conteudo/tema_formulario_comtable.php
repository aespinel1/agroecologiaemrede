<?php

//Campo de seleção rápida se o $id não está especificado (vou deixar apenas para o admin)...
if (!$id && in_array('selecao_rapida', $frm->GetInputs()) && $s->u->us_admin) { ?>
	<h1><?=$txt['titulo_selecao_rapida']?></h1>
	<div class="conteudo">
	<p class="form">
	<?php if(IsSet($verify["selecao_rapida"])) echo '<span class="verificar">'.$txt['verificar'].'</span>';
	$frm->AddLabelPart(array("FOR"=>"selecao_rapida"));
	echo ': ';
	$frm->AddInputPart("selecao_rapida"); ?>
	</p>
	</div>
<?php }

//Campo de seleção de vínculo se o $id não está especificado...
if (!$id && in_array('selecao_vinculo', $frm->GetInputs())) { ?>
	<h1><?php echo $vinculotitulo; ?></h1>
	<div class="conteudo">
	<p class="form">
	<?php if(IsSet($verify["selecao_vinculo"])) echo '<span class="verificar">'.$txt['verificar'].'</span>';
	$frm->AddLabelPart(array("FOR"=>"selecao_vinculo"));
	echo ': ';
	$frm->AddInputPart("selecao_vinculo"); ?>
	</p>
	</div>
<?php }

$primeiro=true;
$primeirobloco=true;
$alterna=false;
$c=0;
foreach ($mapa as $m) {
//if 	((!$cmd[$m->campo]->admin || $cmd[$m->campo]->mostra) || ($cmd[$m->campo]->admin && $s->u->us_admin)) {
	unset($opsform, $cmdo);
	// Como sempre uso o cmd[$m->campo] vamos economizar um pouco:
	$cmdo=$cmd[$m->campo];
	if (!$cmdo) {
		$cmdo = new StdClass();
	}
	// Aqui, se a pessoa não é admin e é um campo admin que não deve ser mostrado, o tipo de form é hidden:
	if 	(
	(!$cmdo->mostra) && (
	($cmdo->admin && !$s->u->us_admin) ||
	($cmdo->naoadmin && $s->u->us_admin)
	) )
		$m->tipo_form='hidden';

	if (!isset($cmdo->campo_nome) || !$cmdo->campo_nome) $cmdo->campo_nome='nome';
	if (!isset($cmdo->campo_id) || !$cmdo->campo_id) $cmdo->campo_id='id';
	// Aqui eu fecho e abro os divs correspondentes de bloco e sub_bloco!
	if ($m->bloco) {
		$primeiralinha='primeira';
		$alterna=false;
		if ($m->sub_bloco) {
			$div_h1='';
			$div_h2=' id="bloco_'.$m->campo.'"';
		} else
			$div_h1=' id="bloco_'.$m->campo.'"';
		if (!$primeiro) {
			?>
			</table>
			</div> <!-- fim de bloco de conteúdo //-->
			</div> <!-- fim de bloco em torno do H1 //-->
		<?php } else $primeiro=false; ?>
		<div<?=$div_h1?>>
		<h1><?php echo $m->bloco; ?></h1>
		<div class="conteudo">
		<?php if ($m->sub_bloco) { ?>
			<h2><?php echo $m->sub_bloco; ?></h2>
			<table class="sub_bloco"<?=$div_h2?>>
			<?php $sub_bloco=true; ?>
		<?php } else {
			$sub_bloco=false;
			?>
			<table class="sub_bloco"<?=$div_h2?>>
			<?php
		}
	} elseif ($m->sub_bloco) {
		$alterna=false;
		$div_h2=' id="sub_bloco_'.$m->campo.'"';
		echo '</table>';
		?>
		<h2><?php echo $m->sub_bloco; ?></h2>
		<table class="sub_bloco"<?=$div_h2?>>
		<?php
		$sub_bloco=true;
	}
	if (!$m->bloco) $primeiralinha='';
	if ($m->tipo_form=='hidden' && ($dados[$m->campo] || $cmdo->padrao))
		$frm->AddInputPart($m->campo);
	elseif (!in_array($m->tipo_form,array('hidden', 'ignorar')) && !$cmdo->ignorar) {
	?>
	<tr class="<?php echo $primeiralinha; ?>linha<?php if ($alterna) echo '_a'; ?>"<?php echo $classe_linha; ?>>
	<?php
	//Agora insiro o(s) input(s) (addpart) deste campo de acordo com o mapa_campos
	switch ($m->tipo_form) {
		case 'checkbox':
		case 'radio':
			// $opcoes é o array que tem as opções possíveis do checkbox ou select. É do tipo valor=>texto, a partir da tabela auxiliar.
			$tabapoiotmp = ($cmd[$m->campo]->bd) ? $bd[$cmd[$m->campo]->bd].'.'.$cmd[$m->campo]->apoio : $cmd[$m->campo]->apoio;
			$opcoes=faz_select($tabapoiotmp,$cmdo->campo_id,'',$cmdo->campo_nome, $cmd[$m->campo]->order, '',false);

			// $max indica o limiar para o campo virar select. Menor que $max é radio ou checkbox
			$max = ($cmdo->nmax_opcoes) ? $cmd[$m->campo]->nmax_opcoes : $padrao_nmax_opcoes;
			//Se há mais opções que o $max, então teremos forçosamente um elemento SELECT, e não CHECKBOX:
			if (!$opcoes) $opcoes=array();
			$num_opcoes=count($opcoes);
			$select=($num_opcoes>$max || !$num_opcoes);

			// Se é select, é apenas um elemento:
			if ($select) {
				?>
				<td class="esq">
				<?php $frm->AddLabelPart(array("FOR"=>$m->campo));
				if ($cmd[$m->campo]->outro_divisoria=='br' || $m->repeticoes) {
					echo '<br>';
					/*$tmp = ($cmd[$m->campo]->size) ? $cmd[$m->campo]->size : $padrao_size_select_multiplo;
					for ($i=1;$i<($m->repeticoes+$tmp+2);$i++) echo '<br>';*/
				} ?>
				</td>

				<td class="dir">
				<?php $frm->AddInputPart($m->campo); ?>
				<?php if ($cmd[$m->campo]->outro && $edicao) {
					if ($cmd[$m->campo]->outro_divisoria=='br' || $m->repeticoes) echo '<br>';
						else echo '&nbsp;&nbsp;&nbsp;&nbsp;';
					if ($m->repeticoes) for ($i=0;$i<$m->repeticoes;$i++) {
						echo $txt['frm_outro'].' '.($i+1).': ';
						$frm->AddInputPart($m->campo.'_outro'.$i);
						echo '<br>';
					} else {
						echo $txt['frm_outro'].': ';
						$frm->AddInputPart($m->campo.'_outro');
					}
				} ?>
				</td>
			<?php
			// Se é checkbox ou radio, temos que fazer um AddInputPart para cada opção
			} else {
			/* Além disso, se tem opção 'outro' e é radio, tenho que dar a oportunidade do usuário escolher um
				rádio vazio ("outro...") */
			if ($cmd[$m->campo]->outro && $m->tipo_form=='radio' && $edicao) {
				$opcoes=$opcoes+array(-9999=>$txt['frm_outro'].'...');
				$num_opcoes++;
			}
				$id_valores=array_keys($opcoes);

				?>
				<td class="esq">
					<?=$m->titulo?>
					<?php
					if ($m->ajuda)
						echo $frm->mostra_hint($txt['menu_ajuda'].': '.$m->titulo, $m->ajuda, strtolower($txt['menu_ajuda']));
					?>
				</td>

				<td class="dir">
				<table class="multiplos">
					<tr>
					<?php
					// A determinação do número de colunas depende da quantidade de opções. É importante contar com um a mais, onde ficará o "outro..."
					$numcols = ($cmd[$m->campo]->numcols) ? $cmd[$m->campo]->numcols : $padrao_numcols;
					$n=0;
					for ($i=0;$i<$num_opcoes;$i++) {
						if ($n==$numcols) {
							?>
							</tr>
							<tr>
							<?php
							$n=0;
						}
						$n++;
						?>
						<td>
							<?php $frm->AddInputPart($m->campo.'_'.$id_valores[$i]); ?> <?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$id_valores[$i])); ?>
						</td>
					<?php
					} // fim do loop nas opções
					//Se é pra colocar "outro...", coloquemos!
					if ($cmd[$m->campo]->outro && $edicao) {
						if ($n==$numcols) {
							$n=1;
							?>
							</tr>
							<tr>
							<?php
						}
						if ($m->repeticoes) {
							for ($i=0;$i<$m->repeticoes;$i++) {
								if ($n==$numcols) {
									$n=1;
									?></tr><tr><?php
								}
								$n++;
								?>
								<td>
								<?php echo $txt['frm_outro'].' '.($i+1).': '; ?> <?php $frm->AddInputPart($m->campo.'_outro'.$i); ?>
								</td>
								<?php
							} // fim do loop nas repetições do "outro".
						} else {
							$n++;
							?>
							<td>
								<?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_outro')); ?> <?php $frm->AddInputPart($m->campo.'_outro'); ?>
							</td>
							<?php
						} // fim do if $m->repeticoes
					} // fim do if cmd['outro']
					//Aqui tenho que acrescentar <td>s vazios pra fechar a tabela:
					while ($n<$numcols) {
						$n++;
						?>
						<td>&nbsp;</td>
						<?php
					}
					?>
				</tr>
				</table>
				</td>

				<?php
			} // fim do else (ou seja, fim da produção do checkbox ou select)

		break;
		case 'select_hierarquia':
			$nmaxsels=$cmd[$m->campo]->niveis;
			// Aqui vai toda a cascata de selects
			?>
			<td class="esq">
				<?=$m->titulo?>
				<?php
				if ($m->ajuda)
					echo $frm->mostra_hint($txt['menu_ajuda'].': '.$m->titulo, $m->ajuda, strtolower($txt['menu_ajuda']));
				?>
			</td>

			<td class="dir">
			<div id="carregando_<?=$m->campo?>" class="carregando"><?=$txt['carregando_lista_valores']?></div>
			<?php
			for ($i=0;$i<$nmaxsels;$i++) {
				if ($i==$nmaxsels-1) $tmp=$m->campo;
					else $tmp=$m->campo.'_'.$i;
				echo ( ($i % 2==$i/2) || $m->repeticoes) ? '' : '&nbsp;';
				if ($m->repeticoes && $i>0) {
					$divespeciali = '<div style="margin:10px 0 0 0;padding:0;clear:both">';
					$divespecialf = '</div>';
				} else unset($divespeciali,$divespecialf); ?>
				<?=$divespeciali?>
				<?php $frm->AddLabelPart(array("FOR"=>$tmp)); ?>:
				<?=$divespecialf?>
				<?php $frm->AddInputPart($tmp);
			}
			?>
			</td>
			<?php
		break;
		case 'georref':
			// Aqui vai toda a cascata de selects
			?>
			<td class="esq">
				<?=$m->titulo?>
				<?php
				if ($m->ajuda)
					echo $frm->mostra_hint($txt['menu_ajuda'].': '.$m->titulo, $m->ajuda, strtolower($txt['menu_ajuda']));
				?>
			</td>

			<td class="dir">
			<div id="carregando_<?=$m->campo?>" class="carregando"></div>
			<?php
			for ($i=0;$i<=2;$i++) {
				if ($i==2) $tmp=$m->campo;
					else $tmp=$m->campo.'_'.$i;
				echo ( ($i % 2==$i/2) || $m->repeticoes) ? '' : '&nbsp;';
				if ($m->repeticoes && $i>0) {
					$divespeciali = '<div style="margin:10px 0 0 0;padding:0;clear:both">';
					$divespecialf = '</div>';
				} else unset($divespeciali,$divespecialf); ?>
				<?=$divespeciali?>
				<?php $frm->AddLabelPart(array("FOR"=>$tmp)); ?>:
				<?=$divespecialf?>
				<?php $frm->AddInputPart($tmp);
			}
			?>
			</td>
			<?php
		break;
		case 'file':
			?>
			<td class="esq">
			<?=$m->titulo?>
				<?php
				if ($m->ajuda)
					echo $frm->mostra_hint($txt['menu_ajuda'].': '.$m->titulo, $m->ajuda, strtolower($txt['menu_ajuda']));
				?>
			</td>
			<td class="dir">
			<?php
			$txturl = ($cmdo->link)
				? ''
				: $dir['upload_URL'];

			for ($i=0;$i<($m->repeticoes+1);$i++) {
			//print_r($frm->inputs);exit;
				if ($frm->inputs['hidden_'.$m->campo.'_'.$i]) {
					?>
					<?php $frm->AddInputPart('hidden_'.$m->campo.'_'.$i); ?>
					<a id="a_<?=$m->campo.'_'.$i?>" target="_blank" href="<?=$txturl.$frm->inputs[$m->campo.'_'.$i]['VALUE']?>"><?=$frm->inputs['hidden_'.$m->campo.'_'.$i]['VALUE']?></a>
					<?php $frm->AddInputPart($m->campo.'_'.$i); ?> <?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$i)); ?>
					<br /><br />
					<?php
				} else {
					$frm->AddInputPart($m->campo.'_'.$i); ?><br /><br />
				<?php }
			} ?>
			</td>
			<?php
		break;
		case 'text':
		case 'textarea':
		case 'data':
			?>
			<td class="esq">
			<?php $frm->AddLabelPart(array("FOR"=>$m->campo)); ?>
			</td>
			<td class="dir">
			<?php if ($m->tipo_form=='file' && $frm->inputs[$m->campo]['VALUE']) {
				?>
				<a target="_blank" href="<?=$dir['upload_URL'].$frm->inputs[$m->campo]['VALUE']?>"><?=$frm->inputs[$m->campo]['VALUE']?></a><br /><br />
			<?php } ?>
			<?php $frm->AddInputPart($m->campo); ?>
			</td>

			<?php
		break;
		case 'arvore':
			$campo_id=$cmdo->campo_id;
			$campo_nome=$cmdo->campo_nome;
			$campo_mae=$cmdo->campo_mae;
			$arvore = resgata_arvore($cmdo->apoio,$campo_id,$campo_nome,$campo_mae);
			$nivel=0;
			$idmae[0]='';
			?>
			<td class="esq">
				<?=$m->titulo?>
				<?php
				if ($m->ajuda)
					echo $frm->mostra_hint($txt['menu_ajuda'].': '.$m->titulo, $m->ajuda, strtolower($txt['menu_ajuda']));
				?>
			</td>
			<td class="dir">
			<?php
			foreach ($arvore as $i=>$no) {
				if ($no->nivel>$nivel) {
						$nivel=$no->nivel;
						$idmae[$nivel] = $arvore[$i-1]->$campo_id;
						echo '<div id="div_'.$m->campo.'_'.$idmae[$nivel].'">';
					} elseif ($no->nivel<$nivel) {
						echo str_repeat('</div>',$nivel-$no->nivel);
						$nivel=$no->nivel;
					} ?>
				<?=str_repeat('&nbsp;',$no->nivel*8)?>
				<?php $frm->AddInputPart($m->campo.'_'.$no->$campo_id); ?>
				<?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$no->$campo_id)); ?>
				<br />
				<?php
			}
			for ($i=0;$i<$nivel;$i++)
				echo '</div>';
			?> </td> <?php
		break;
	} // FIM DO SWITCH
	?>
	</tr>
	<?php
	$alterna=!$alterna; // Para alternar o div dos inputs
	$c++;
	} // Fim do else do if (HIDDEN)!
//} // Fim do if que filtra os campos de admin para só o admin ver!
} // Fim do loop pelos campos todos da tabela $tab
echo '</table>'; ?>
</div> <!-- fim do bloco de conteúdo //-->
</div> <!-- fim de bloco em torno do H1 //-->
<div class="botao">
	<?php if ($pg>1) $frm->AddInputPart("voltar"); echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
	<?php $frm->AddInputPart("enviar"); ?>
</div><br><br>
<?php
