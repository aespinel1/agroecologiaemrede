<?php
$memoria=array();
$sql = "SELECT in_id, in_nome, in_email, in_url, ti_descricao, ei_endereco, ei_cidade, ei_cep, ei_telefone FROM frm_instituicao as a, tipo_instituicoes as b WHERE a.in_tipo=b.ti_id AND publicada=1 ORDER BY in_nome";
$dados=faz_query($sql,'','object');
foreach ($dados as $i=>$dado) {
	if (in_array($dado->ei_cidade,array_keys($memoria))) {
		$dado->ei_cidade=$memoria[$dado->ei_cidade]['cidade'];
		$dado->ei_estado=$memoria[$dado->ei_cidade]['estado'];
	} elseif (in_array(substr($dado->ei_cidade,0,2),$paises)) {
			$pais=substr($dado->ei_cidade,0,2);
			$sql = "SELECT a.id, a.nome as cidade, CONCAT('".$pais."',', ',b.nome) as estado FROM _".$pais." as a, _".$pais."_maes as b WHERE a.id_mae=b.id AND a.id='".$dado->ei_cidade."'";
			$res=faz_query($sql,'','array_assoc','id');
		if ($res) {
			$dado->ei_cidade=$res[$dado->ei_cidade]['cidade'];
			$dado->ei_estado=$res[$dado->ei_cidade]['estado'];
			$memoria += $res;
		} else {
			$dado->ei_cidade='--';
			$dado->ei_estado=$dado->ei_cidade;
			$memoria[$dado->ei_cidade]['cidade']=$dado->ei_cidade;
			$memoria[$dado->ei_cidade]['estado']=$dado->ei_pais;
		}
	} else {
		$dado->ei_estado='--';
	}
	$dados[$i]=$dado;
}
?>
<h1><?=$txt['menu_cadastrar_instituicao']?></h1>
<div class="card border-primary">
	<div class="card-header">
		<h3><?=$txt['titulo_lista_instituicoes']?></h3>
	</div>
	<div class="card-body">
		<div class="alert alert-info"><?=$txt['ajuda_previa_frm_instituicao']?></div>
		<p class="mb-4 text-center">
			<a class="btn btn-primary btn-lg text-wrap w-100" href="index.php?f=formulario_geral&nomefrm=frm_instituicao"><?=$txt['fazer_novo_cadastro_instituicao']?></a>
		</p>
		<div class="input-group mb-2">
	    <div class="input-group-prepend">
	      <div class="input-group-text"><i class="oi oi-magnifying-glass"></i></div>
	    </div>
			<input id="previa_frm_instituicao_search" type="text" class="form-control input-lg" placeholder="buscar..." />
		</div>

		<div class="list-group pre-scrollable">
			<?php foreach ($dados as $i=>$dado) {	?>
				<a class="list-group-item list-group-item-action" href="<?=$dir['base_URL']?>instituicoes.php?inst=<?=$dado->in_id?>">
					<?=$dado->in_nome?> <span class="badge badge-warning"><?=$dado->ti_descricao?></span>
				</a>
			<?php } ?>
		</div>
	</div>
</div>
