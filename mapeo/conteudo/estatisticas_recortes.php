<?php


?>

<script src="request.js"></script>
<script>
function handleOnChange(dd1,id_dependente,select_dependente)
{
  var idx = dd1.selectedIndex;
  var val = dd1[idx].value;
  var p=document.getElementById(id_dependente).style;
  var par = document.forms["estatisticas"];
  var parelmts = par.elements;
  var prezsel = parelmts[select_dependente];
  var escolha = val.toLowerCase();
  if (escolha != "")
  {
    p.visibility='visible';
 	Http.get({
		url: "./apoio/stats/" + escolha,		
		callback: fillPrez,
		cache: Http.Cache.Get
	}, [prezsel]);
  }
  else p.visibility='hidden';
}

function fillPrez(xmlreply, prezelmt)
{
  if (xmlreply.status == Http.Status.OK)
  {
    var prezresponse = xmlreply.responseText;
    var prezar = prezresponse.split("|");
    prezelmt.length = 1;
    prezelmt.length = prezar.length;
    for (o=1; o < prezar.length; o++)
    {
      prezardois = prezar[o].split(",");
      if (prezardois[1][0]=="*") {
      	prezelmt[o].selected=true;
      	prezardois[1]=prezardois[1].replace("*","");
      }
      prezelmt[o].value = prezardois[0];
      prezelmt[o].text = prezardois[1];
    }
  }
  else
  {
    alert("A chamada AJAX deu errado!");
  }
}
</script>



<form>
<div class="linha">
<div class="dir">
		<form action="index.php" method="GET">
			<select name="recorte0" onchange="javascript:this.form.submit();">
				<option value="nenhum">Nenhum recorte</option>				
				<?php 
				foreach ($opcoes_recortes as $k=>$opcao) {
					echo '<option value="'.$k.'"';
					if ($grecorte && $grecorte==$k) echo ' SELECTED="SELECTED"';
					echo '>'.$opcao.'</option>'.chr(10);
				}
				/*<option value="regiao"<?php if ($_GET['recorte0']=='regiao') echo 'SELECTED="SELECTED"'; ?>>Por região</option>
				foreach ($lista_MR as $k=>$mr) {
					if (in_array($k,$gregioes) {
						echo '<option value="'.$k.'"';
						if ($grecorte==$k) echo ' SELECTED="SELECTED"';
						echo '>Região '.$mr.'</option>'.chr(10);
					}
				}
				
				mysql_select_db($bd['ref']);
				echo faz_select('estados', 'id_UF', $grecorte, 'Estado', '', 'id_UF<>0 AND id_UF<>99 AND id_UF<>70');
				mysql_select_db($bd['pafes']);*/
				?>
			</select>
			</p>
		<?php /*
			<div id='recorte1' style="visibility:hidden">
				<p class="descricao_titulo">
				Cidade: <select name="recorte1" class="inputbox">
				<option value="">&nbsp;</option>					
				</select>
			</div>
		*/ ?>
		<input type='hidden' name='f' value='estatisticas'>
		<input type='hidden' name='gsecao' value='<?php echo $gsecao; ?>'>
		<input type='hidden' name='grafico' value='<?php echo $gid; ?>'>
		</form>
</div>
<div class="esq">Recorte territorial:</div>
</div>
<?php
