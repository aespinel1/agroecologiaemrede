<?php
echo 'oi';exit;
//Campo de seleção rápida se o $id não está especificado...
if (!$id && in_array('selecao_rapida', $frm->GetInputs())) { ?>
	<h1><?=$txt['titulo_selecao_rapida']?></h1>
	<div class="conteudo">
	<p class="form">
	<?php if(IsSet($verify["selecao_rapida"])) echo '<span class="verificar">'.$txt['verificar'].'</span>';
	$frm->AddLabelPart(array("FOR"=>"selecao_rapida"));
	echo ': ';
	$frm->AddInputPart("selecao_rapida"); ?>
	</p>
	</div>
<?php }

//Campo de seleção de vínculo se o $id não está especificado...
if (!$id && in_array('selecao_vinculo', $frm->GetInputs())) { ?>
	<h1><?php echo $vinculotitulo; ?></h1>
	<div class="conteudo">
	<p class="form">
	<?php if(IsSet($verify["selecao_vinculo"])) echo '<span class="verificar">'.$txt['verificar'].'</span>';
	$frm->AddLabelPart(array("FOR"=>"selecao_vinculo"));
	echo ': ';
	$frm->AddInputPart("selecao_vinculo"); ?>
	</p>
	</div>
<?php }

$primeiro=true;
$primeirobloco=true;
$alterna=false;
$c=0;
foreach ($mapa as $m) {
	unset($opsform, $cmdo);
	// Como sempre uso o cmd[$m->campo] vamos economizar um pouco:
	$cmdo=$cmd[$m->campo];
	if (!isset($cmdo->campo_nome) || !$cmdo->campo_nome) $cmdo->campo_nome='nome';
	if (!isset($cmdo->campo_id) || !$cmdo->campo_id) $cmdo->campo_id='id';
	// Aqui eu fecho e abro os divs correspondentes de bloco e sub_bloco!
	if ($m->bloco) {
		$primeiralinha='primeira';
		$alterna=false;
		if ($m->sub_bloco) {
			$div_h1='';
			$div_h2=' id="'.$m->campo.'"';
		} else
			$div_h1=' id="'.$m->campo.'"';
		if (!$primeiro) {
			if ($sub_bloco) echo '</div>';
			$classe_linha=' style="clear:both"';
			/*?>
			<div class="botao">
				<?php
					if ($pg>1) $frm->AddInputPart("voltar"); echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
					$frm->AddInputPart("enviar");
				?>
			</div>
			*/ ?>
			</div> <!-- fim de bloco de conteúdo //-->
			</div> <!-- fim de bloco em torno do H1 //-->
		<?php } else $primeiro=false; ?>
		<div<?=$div_h1?>>
		<h1><?=$m->bloco?></h1>
		<div class="conteudo">
		<?php if ($m->sub_bloco) { ?>
			<h2><?=$m->sub_bloco?></h2>
			<div class="sub_bloco"<?=$div_h2?>>
			<?php $sub_bloco=true; ?>
		<?php } else $sub_bloco=false;
	} elseif ($m->sub_bloco) {
		$alterna=false;
		$div_h2=' id="'.$m->campo.'"';
		if ($sub_bloco) echo '</div>';
		?>
		<h2><?=$m->sub_bloco?></h2>
		<div class="sub_bloco"<?=$div_h2?>>
		<?php
		$sub_bloco=true;
	}
	if (!$m->bloco) $primeiralinha='';
	if ($m->tipo_form=='hidden' && $m->campo) $frm->AddInputPart($m->campo);
	elseif (!in_array($m->tipo_form,array('hidden', 'ignorar'))) {
	?>
	<div class="<?php echo $primeiralinha; ?>linha<?php if ($alterna) echo '_a'; ?>"<?php echo $classe_linha; ?>>
	<?php
	//Agora insiro o(s) input(s) (addpart) deste campo de acordo com o mapa_campos
	switch ($m->tipo_form) {
		case 'checkbox':
		case 'radio':
			// $opcoes é o array que tem as opções possíveis do checkbox ou select. É do tipo valor=>texto, a partir da tabela auxiliar.
			$tabapoiotmp = ($cmdo->bd) ? $bd[$cmdo->bd].'.'.$cmdo->apoio : $cmdo->apoio;
			$opcoes=faz_select($tabapoiotmp,$cmdo->campo_id,'',$cmdo->campo_nome, $cmdo->order, '',false);

			// $max indica o limiar para o campo virar select. Menor que $max é radio ou checkbox
			$max = ($cmdo->nmax_opcoes) ? $cmdo->nmax_opcoes : $padrao_nmax_opcoes;
			//Se há mais opções que o $max, então teremos forçosamente um elemento SELECT, e não CHECKBOX:
			if (!$opcoes) $opcoes=array();
			$num_opcoes=count($opcoes);
			$select=($num_opcoes>$max || !$num_opcoes);

			// Se é select, é apenas um elemento:
			if ($select) {
				?>
				<div class="dir" id="dir<?php echo $c; ?>">
				<?php $frm->AddInputPart($m->campo); ?>
				<?php if ($cmdo->outro && $edicao) {
					if ($cmdo->outro_divisoria=='br' || $m->repeticoes) echo '<br>';
						else echo '&nbsp;&nbsp;&nbsp;&nbsp;';
					if ($m->repeticoes) for ($i=0;$i<$m->repeticoes;$i++) {
						echo 'Outro '.($i+1).': ';
						$frm->AddInputPart($m->campo.'_outro'.$i);
						echo '<br>';
					} else {
						echo 'Outro: ';
						$frm->AddInputPart($m->campo.'_outro');
					}
				} ?>
				</div>
				<div class="esq" id="esq<?php echo $c; ?>">
				<?php $frm->AddLabelPart(array("FOR"=>$m->campo));
				if ($cmdo->outro_divisoria=='br' || $m->repeticoes) {
					echo '<br>';
					$tmp = ($cmdo->size) ? $cmdo->size : $padrao_size_select_multiplo;
					for ($i=1;$i<($m->repeticoes+$tmp+2);$i++) echo '<br>';
				} ?>
				</div>
				<?php
			// Se é checkbox ou radio, temos que fazer um AddInputPart para cada opção
			} else {
			/* Além disso, se tem opção 'outro' e é radio, tenho que dar a oportunidade do usuário escolher um
				rádio vazio ("outro...") */
			if ($cmdo->outro && $m->tipo_form=='radio' && $edicao) {
				$opcoes=$opcoes+array(-9999=>'Outro...');
				$num_opcoes++;
			}
				$id_valores=array_keys($opcoes);

				?>
				<div class="dir" id="dir<?php echo $c; ?>">
				<table class="multiplos">
				<tr>
				<?php
				// A determinação do número de colunas depende da quantidade de opções. É importante contar com um a mais, onde ficará o "outro..."
				$numcols = ($cmdo->numcols) ? $cmdo->numcols : $padrao_numcols;
				$n=0;
				for ($i=0;$i<$num_opcoes;$i++) {
					if ($n==$numcols) {
						?>
						</tr>
						<tr>
						<?php
						$n=0;
					}
					$n++;
					?>
					<td>
						<?php $frm->AddInputPart($m->campo.'_'.$id_valores[$i]); ?> <?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_'.$id_valores[$i])); ?>
					</td>
				<?php
				} // fim do loop nas opções
				//Se é pra colocar "outro...", coloquemos!
				if ($cmdo->outro && $edicao) {
					if ($n==$numcols) {
						$n=1;
						?>
						</tr>
						<tr>
						<?php
					}
					if ($m->repeticoes) {
						for ($i=0;$i<$m->repeticoes;$i++) {
							if ($n==$numcols) {
								$n=1;
								?></tr><tr><?php
							}
							$n++;
							?>
							<td>
							<?php echo 'Outro '.($i+1).': '; ?> <?php $frm->AddInputPart($m->campo.'_outro'.$i); ?>
							</td>
							<?php
						} // fim do loop nas repetições do "outro".
					} else {
						$n++;
						?>
						<td>
							<?php $frm->AddLabelPart(array("FOR"=>$m->campo.'_outro')); ?> <?php $frm->AddInputPart($m->campo.'_outro'); ?>
						</td>
						<?php
					} // fim do if $m->repeticoes
				} // fim do if cmd['outro
				//Aqui tenho que acrescentar <td>s vazios pra fechar a tabela:
				while ($n<$numcols) {
					$n++;
					?>
					<td>&nbsp;</td>
					<?php
				}
				?>
				</tr>
				</table>
				</div>
				<div class="esq" id="esq<?php echo $c; ?>">
					<?php echo $m->titulo; ?>
				</div>
				<?php
			} // fim do else (ou seja, fim da produção do checkbox ou select)

		break;
		case 'select_hierarquia':
			$nmaxsels=$cmdo->niveis;
			// Aqui vai toda a cascata de selects
			?>
			<div class="dir" id="dir<?php echo $c; ?>">
			<div id="carregando">Carregando a lista de valores...</div>
			<?php
			for ($i=0;$i<$nmaxsels;$i++) {
				if ($i==$nmaxsels-1) $tmp=$m->campo;
					else $tmp=$m->campo.'_'.$i;
				echo ( ($i % 2==$i/2) || $m->repeticoes) ? '<br />' : '&nbsp;';
				$frm->AddLabelPart(array("FOR"=>$tmp)); ?>: <?php $frm->AddInputPart($tmp);
			}
			?>
			</div>
			<div class="esq" id="esq<?php echo $c; ?>"><?php echo $m->titulo; ?></div>
			<?php
		break;
		case 'text':
		case 'textarea':
		case 'data':
		case 'file':
			?>
			<div class="dir" id="dir<?php echo $c; ?>">
			<?php if ($m->tipo_form=='file' && $frm->inputs[$m->campo]['VALUE']) {
				echo '<a href="'.$dir['upload'].$frm->inputs[$m->campo]['VALUE'].'">'.$frm->inputs[$m->campo]['VALUE'].'</a><br /><br />';
			} ?>
			<?php $frm->AddInputPart($m->campo); ?>
			</div>
			<div class="esq" id="esq<?php echo $c; ?>">
			<?php $frm->AddLabelPart(array("FOR"=>$m->campo)); ?>
			</div>
			<?php
		break;
	} // FIM DO SWITCH
	?>
	</div>
	<?php
	$alterna=!$alterna; // Para alternar o div dos inputs
	$c++;
	} // Fim do else do if (HIDDEN)!
} // Fim do loop pelos campos todos da tabela $tab
if ($sub_bloco) echo '</div>'; ?>
</div> <!-- fim do bloco de conteúdo //-->
</div> <!-- fim de bloco em torno do H1 //-->
<div class="botao">
	<?php if ($pg>1) $frm->AddInputPart("voltar"); echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' ?>
	<?php $frm->AddInputPart("enviar"); ?>
</div>
<?php
