﻿<?php
if (isset($mapa)) {
	$c=0;
	foreach ($mapa as $m) {
		if (in_array($m->tipo_form, array('select_hierarquia','georref'))) $ajax_select=true;
		$c++;
	}
}
if (!defined('MAPA')) {
	define(MAPA, ((isset($_REQUEST['f']) && $_REQUEST['f']=='consultar') && (isset($_REQUEST['visao']) && $_REQUEST['visao']=='mapa')));
}
?>
<!doctype html>
<html<?=(MAPA)?' class="mapa"':''?>>
<head>
	<title><?php echo $txt['titulo_geral']; ?></title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<?php if (isset($ajax_select)) { ?><script type="text/javascript" src="<?php echo $dir['apoio_URL']; ?>ajax_dd/ajax.js"></script><?php } ?>
<?php include($dir['base']."menu_geral.php"); ?>
