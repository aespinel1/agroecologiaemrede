<?php

	//$d = scandir($dir['graficos'],1);
	
	$dh = opendir($dir['graficos']);
	while (false !== ($arqtmp = readdir($dh)))
	    $d[] = $arqtmp;
	/*trecho para limpar o diretorio de graficos. Uso soh de vez em quando: elimino assim os graficos que nao existem mais...
	foreach ($d as $img) {
		if ($img[0]<>'.') {
			$tmp=explode('-',$img);
			$tmp=explode('.',$tmp[2]);
			$tmp=explode('_',$tmp[0]);
			$sql='SELECT titulo, ano FROM graficos WHERE id='.$tmp[0];
			$res=mysql_query($sql);
			if (!mysql_num_rows($res))
				if (unlink($dir['graficos'].$img))
					echo $img.' apagado com sucesso.<br>';
		}
	}*/
	
	shuffle($d);
	//Isto eh para eliminar os recortes, de modo a escolher apenas graficos sem recorte:
	$img_aleatoria='_';
	while (strpos($img_aleatoria,'_')!==false || $img_aleatoria[1]=='.') {
		$img_aleatoria=$d[array_rand($d)];
	}
	$tmp=explode('.',$img_aleatoria);
	$tmp=explode('-',$tmp[0]);
	$agsecao='&gsecao='.$tmp[1];
	$tmp=explode('_',$tmp[2]);
	$agid=$tmp[0];
	$agrecorte = ($tmp[1] && $tmp[1]!='nenhum')
		? '&recorte0='.$tmp[1]
		: '';
	$sql="SELECT titulo, ano FROM graficos WHERE id=".$agid;
	$grafico=faz_query($sql,'','array');
	$agano=$grafico['ano'][0];
	$agtitulo=$grafico['titulo'][0];
	
?>
	<h1>Gráficos</h1>
	<p>
	<a style="text-decoration:none" href="?f=estatisticas&ano=<?=$agano.$agsecao?>&grafico=<?=$agid.$agrecorte?>">
		<img src="<?=$dir['graficos'].$img_aleatoria?>" alt="<?=$agtitulo.' (20'.$agano.')'?> - clique para ampliar" title="<?=$agtitulo.' (20'.$agano.')'?> - clique para ampliar">		
	</a>
	</p>
	<p class="legenda_img_stat">
	<a style="text-decoration:none" href="?f=estatisticas&ano=<?=$agano.$agsecao?>&grafico=<?=$agid.$agrecorte?>">
	<?=$agtitulo.' (20'.$agano.')'?><br>(clique para ampliar)
	</a>
	</p>
	<br>
	<p class="menu_stat"><a href='?f=estatisticas'>ver todos os gráficos...</a></p>
	<br>
<?php
?>
