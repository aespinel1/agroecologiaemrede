<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<!doctype html public "-//W3C//DTD HTML 4.0 Transitional//PT-BR">
<!DOCTYPE html>
<html>
<head>
<title>Banco de dados do FBES</title>
<link rel="stylesheet" href="css/styles_screen.css" type ="text/css" media="screen">
<link rel="stylesheet" href="css/styles_print.css" type ="text/css" media="print">
<meta name="Generator" content="DaDaBIK 3.1 beta  - http://www.dadabik.org/">

<script language="JavaScript">
<!--
function fill_cap(city_field){

	temp = 'document.contacts_form.'+city_field+'.value';

	city = eval(temp);
	cap=open("fill_cap.php?city="+escape(city),"schermo","toolbar=no,directories=no,menubar=no,width=170,height=250,resizable=yes");
}
//-->
</script>

<script language="Javascript1.2">
<!-- // load htmlarea
_editor_url = "include/htmlarea/";                     // URL to htmlarea files
var win_ie_ver = parseFloat(navigator.appVersion.split("MSIE")[1]);
if (navigator.userAgent.indexOf('Mac')        >= 0) { win_ie_ver = 0; }
if (navigator.userAgent.indexOf('Linux')      >= 0) { win_ie_ver = 0; }
if (navigator.userAgent.indexOf('Windows CE') >= 0) { win_ie_ver = 0; }
if (navigator.userAgent.indexOf('Opera')      >= 0) { win_ie_ver = 0; }
if (win_ie_ver >= 5.5) {
  document.write('<scr' + 'ipt src="' +_editor_url+ 'editor.js"');
  document.write(' language="Javascript1.2"></scr' + 'ipt>');
}
else {
	document.write('<scr'+'ipt>function editor_generate() { return false; }</scr'+'ipt>');
}
// -->
</script>

</head>

<body>
<h1>Banco de dados do FBES</h1>
<table class="main_table" cellpadding="10">
<tr>
<td valign="top">
<h1>Cadastro de produtos e serviços</h1><a class="onlyscreen" href="?table_name=lista_produtos_e_servicos&function=show_insert_form">Inserir um novo registro</a> | <a class="onlyscreen" href="?table_name=lista_produtos_e_servicos&function=search">Mostrar a tabela</a> | <a class="onlyscreen" href="?table_name=lista_produtos_e_servicos&function=show_search_form">Pesquisar</a> | <a href="index.php">Voltar ao menu inicial</a> | <a href="javascript:window.close()">Fechar esta janela</a><br><hr><br><h3>Inserir um novo registro</h3><p>Campos obrigatórios est�o em vermelho.</p><span style='color:#f00'>Você escolheu a categoria do ramo de atividade, agora deve definir a subcategoria e o ramo espec�fico...</span><br><br><form name="contacts_form" method="post" action="index.php?table_name=lista_produtos_e_servicos&page=0&function=insert" enctype="multipart/form-data"><table><tr><td align="right" valign="top"><table><tr><td class="td_label_form"><font class="required_field_labels">Nome do produto ou serviço </font></td></tr></table></td><td><table border="0"><tr><td class="td_input_form"><input type="text" name="NomeProdutoServico" maxlength="100" value=""></td><td class="td_hint_form"></td></tr></table></td></tr><tr><td align='right' valign='top'><table><tr><td class="td_label_form"><font class='required_field_labels'>Categoria</font></td></tr></table></td><td><script language="JavaScript">
<!--
function submitForm() {
document.contacts_form.submit();
}
//-->

</script>
<table border='0'><tr><td class='td_input_form'><select style='font-size:7pt' name='cat' onChange="submitForm()">
<option value=''>N�o sei</option>
<option value='75'>ADMINISTRA��O P�BLICA, DEFESA E SEGURIDADE SOCIAL</option>
<option value='01'>AGRICULTURA, PECU�RIA E SERVI�OS RELACIONADOS</option>
<option value='55'>ALOJAMENTO E ALIMENTA��O</option>
<option value='71'>ALUGUEL DE VE�CULOS, M�QUINAS E EQUIPAMENTOS SEM CONDUTORES OU OPERADORES E DE OBJETOS PESSOAIS E DOM�STICOS</option>
<option value='63'>ATIVIDADES ANEXAS E AUXILIARES DO TRANSPORTE E AG�NCIAS DE VIAGEM</option>
<option value='91'>ATIVIDADES ASSOCIATIVAS</option>
<option value='67'>ATIVIDADES AUXILIARES DA INTERMEDIA��O FINANCEIRA, SEGUROS E PREVID�NCIA COMPLEMENTAR</option>

<option value='72'>ATIVIDADES DE INFORM�TICA E SERVI�OS RELACIONADOS</option>
<option value='70'>ATIVIDADES IMOBILI�RIAS</option>
<option value='92'>ATIVIDADES RECREATIVAS, CULTURAIS E DESPORTIVAS</option>
<option value='41'>CAPTA��O, TRATAMENTO E DISTRIBUI��O DE �GUA</option>
<option value='50'>COM�RCIO E REPARA��O DE VE�CULOS AUTOMOTORES E MOTOCICLETAS</option>
<option value='51'>COM�RCIO POR ATACADO E REPRESENTANTES COMERCIAIS E AGENTES DO COM�RCIO</option>
<option value='52'>COM�RCIO VAREJISTA E REPARA��O DE OBJETOS PESSOAIS E DOM�STICOS</option>
<option value='18'>CONFEC��O DE ARTIGOS DO VESTU�RIO E ACESS�RIOS</option>
<option value='45'>CONSTRU��O</option>

<option value='64'>CORREIO E TELECOMUNICA��ES</option>
<option value='22'>EDI��O, IMPRESS�O E REPRODU��O DE GRAVA��ES</option>
<option value='80'>EDUCA��O</option>
<option value='40'>ELETRICIDADE, G�S E �GUA QUENTE</option>
<option value='10'>EXTRA��O DE CARV�O MINERAL</option>
<option value='13'>EXTRA��O DE MINERAIS MET�LICOS</option>
<option value='14'>EXTRA��O DE MINERAIS N�O-MET�LICOS</option>
<option value='11'>EXTRA��O DE PETR�LEO E SERVI�OS RELACIONADOS</option>
<option value='25'>FABRICA��O DE ARTIGOS DE BORRACHA E PL�STICO</option>

<option value='21'>FABRICA��O DE CELULOSE, PAPEL E PRODUTOS DE PAPEL</option>
<option value='23'>FABRICA��O DE COQUE, REFINO DE PETR�LEO, ELABORA��O DE COMBUST�VEIS NUCLEARES E PRODU��O DE �LCOOL</option>
<option value='33'>FABRICA��O DE EQUIPAMENTOS DE INSTRUMENTA��O M�DICO-HOPITALARES, INSTRUMENTOS DE PRECIS�O E �PTICOS, EQUIPAMENTOS PARA AUTOMA��O INSDUSTRIAL, CRON�METROS E REL�GIOS</option>
<option value='32'>FABRICA��O DE MATERIAL ELETR�NICO E DE APARELHOS E EQUIPAMENTOS DE COMUNICA��ES</option>
<option value='29'>FABRICA��O DE M�QUINAS E EQUIPAMENTOS</option>
<option value='30'>FABRICA��O DE M�QUINAS PARA ESCRIT�RIO E EQUIPAMENTOS DE INFORM�TICA</option>
<option value='31'>FABRICA��O DE M�QUINAS, APARELHOS E MATERIAIS EL�TRICOS</option>
<option value='36'>FABRICA��O DE M�VEIS E IND�STRIAS DIVERSAS</option>
<option value='35'>FABRICA��O DE OUTROS EQUIPAMENTOS DE TRANSPORTE</option>

<option value='15'>FABRICA��O DE PRODUTOS ALIMENT�CIOS E BEBIDAS</option>
<option value='20'>FABRICA��O DE PRODUTOS DE MADEIRA</option>
<option value='28'>FABRICA��O DE PRODUTOS DE METAL - EXCETO M�QUINAS E EQUIPAMENTOS</option>
<option value='26'>FABRICA��O DE PRODUTOS DE MINERAIS N�O-MET�LICOS</option>
<option value='16'>FABRICA��O DE PRODUTOS DO FUMO</option>
<option value='24'>FABRICA��O DE PRODUTOS QU�MICOS</option>
<option value='17'>FABRICA��O DE PRODUTOS T�XTEIS</option>
<option value='34'>FABRICA��O E MONTAGEM DE VE�CULOS AUTOMOTORES, REBOQUES E CARROCERIAS</option>
<option value='65'>INTERMEDIA��O FINANCEIRA</option>

<option value='90'>LIMPEZA URBANA E ESGOTO E ATIVIDADES RELACIONADAS</option>
<option value='27'>METALURGIA B�SICA</option>
<option value='99'>ORGANISMOS INTERNACIONAIS E OUTRAS INSTITUI��ES EXTRATERRITORIAIS</option>
<option value='05'>PESCA, AQUICULTURA E SERVI�OS RELACIONADOS</option>
<option value='73'>PESQUISA E DESENVOLVIMENTO</option>
<option value='19'>PREPARA��O DE COUROS E FABRICA��O DE ARTEFATOS DE COURO, ARTIGOS DE VIAGEM E CAL�ADOS</option>
<option value='37'>RECICLAGEM</option>
<option value='85'>SA�DE E SERVI�OS SOCIAIS</option>
<option value='66'>SEGUROS E PREVID�NCIA COMPLEMENTAR</option>

<option value='95'>SERVI�OS DOM�STICOS</option>
<option value='93'>SERVI�OS PESSOAIS</option>
<option value='74' selected>SERVI�OS PRESTADOS PRINCIPALMENTE AS EMPRESAS</option>
<option value='02'>SILVICULTURA, EXPLORA��O FLORESTAL E SERVI�OS RELACIONADOS</option>
<option value='62'>TRANSPORTE A�REO</option>
<option value='61'>TRANSPORTE AQUAVI�RIO</option>
<option value='60'>TRANSPORTE TERRESTRE</option>
</select></td></tr></table></td></tr>
<tr><td align='right' valign='top'><table><tr><td class="td_label_form"><font class='required_field_labels'>Sub-categoria</font></td></tr></table></td><td><table border='0'><tr><td class='td_input_form'><select name=subcat onChange="redirect(this.options.selectedIndex)">

<option value='9999'>mudar a categoria...</option>
<option value='7416-0'>Atividades de assessoria em gest�o empresarial</option>
<option value='7412-8'>Atividades de contabilidade e auditoria</option>
<option value='7492-6'>Atividades de envasamento e empacotamento, por conta de terceiros</option>
<option value='7470-5'>Atividades de imuniza��o, higieniza��o e de limpeza em pr�dios e em domic�lios</option>
<option value='7460-8'>Atividades de investiga��o, vigil�ncia e seguran�a</option>
<option value='7491-8'>Atividades fotogr�ficas</option>
<option value='7411-0'>Atividades jur�dicas</option>
<option value='7430-6'>Ensaios de materiais e de produtos</option>

<option value='7414-4'>Gest�o de participa��es societ�rias (holdings)</option>
<option value='7499-3'>Outras atividades de serviços prestados principalmente �s empresas n�o especificadas anteriormente</option>
<option value='7413-6'>Pesquisas de mercado e de opini�o p�blica</option>
<option value='7440-3'>Publicidade</option>
<option value='7415-2'>Sedes de empresas e unidades administrativas locais</option>
<option value='7450-0'>Sele��o, agenciamento e loca��o de m�o-de-obra</option>
<option value='7420-9'>Serviços de arquitetura e engenharia e de assessoramento t�cnico especializado</option>
</select></td>
</tr></table></td></tr>
<tr><td align='right' valign='top'><table><tr><td class="td_label_form"><font class='required_field_labels'>Ramo de Atividade</font></td></tr></table></td><td><table border='0'><tr><td class='td_input_form'><select name='lista_ramos_de_atividade_codCNAE5'>

<option value=''>escolha a subcategoria...</option>
</select></td></tr></table></td></tr>
<script>
<!--
/*Double Combo Script Credit - By JavaScript Kit (www.javascriptkit.com)
Over 200+ free JavaScripts here!
*/var groups=document.contacts_form.subcat.options.length
var group=new Array(groups)
for (i=0; i<groups; i++) group[i]=new Array()
group[0][0]=new Option("escolha a subcategoria...","")

group[1][0]=new Option("Assessoria �s atividades agr�colas e pecu�rias","7416-0/01")
group[1][1]=new Option("Atividades de assessoria em gest�o empresarial","7416-0/02")

group[2][0]=new Option("Atividades de auditoria cont�bil","7412-8/02")
group[2][1]=new Option("Atividades de contabilidade","7412-8/01")

group[3][0]=new Option("Atividades de envasamento e empacotamento, por conta de terceiros","7492-6/00")

group[4][0]=new Option("Atividades de imuniza��o e controle de pragas urbanas","7470-5/02")
group[4][1]=new Option("Atividades de limpeza em im�veis","7470-5/01")

group[5][0]=new Option("Atividades de investiga��o particular","7460-8/01")
group[5][1]=new Option("Atividades de vigil�ncia e seguran�a privada","7460-8/02")
group[5][2]=new Option("Serviços de adestramento de c�es de guarda","7460-8/03")
group[5][3]=new Option("Serviços de transporte de valores","7460-8/04")

group[6][0]=new Option("Est�dios fotogr�ficos","7491-8/01")
group[6][1]=new Option("Filmagem de festas e eventos.","7491-8/05")
group[6][2]=new Option("Laboratórios fotogr�ficos","7491-8/03")
group[6][3]=new Option("Serviços de fotografias a�reas, submarinas e similares","7491-8/04")
group[6][4]=new Option("Serviços de microfilmagem.","7491-8/06")

group[7][0]=new Option("Agente de propriedade industrial","7411-0/04")
group[7][1]=new Option("Atividades auxiliares da justi�a","7411-0/03")
group[7][2]=new Option("Atividades cartoriais","7411-0/02")
group[7][3]=new Option("Serviços advocatícios","7411-0/01")

group[8][0]=new Option("Ensaios de materiais e de produtos","7430-6/00")

group[9][0]=new Option("Gest�o de participa��es societ�rias (holdings)","7414-4/00")

group[10][0]=new Option("Atividade de intermedia��o e agenciamento de serviços e neg�cios em geral, sem especializa��o definida.","7499-3/12")
group[10][1]=new Option("Casas de festas e eventos","7499-3/13")
group[10][2]=new Option("Emiss�o de vales alimenta��o, transporte e similares","7499-3/11")
group[10][3]=new Option("Escafandria e Mergulho","7499-3/09")
group[10][4]=new Option("Fotoc�pias, digitaliza��o e serviços correlatos.","7499-3/02")
group[10][5]=new Option("Outros serviços prestados principalmente �s empresas","7499-3/99")
group[10][6]=new Option("Serviço de medi��o de consumo de energia el�trica, g�s e �gua.","7499-3/10")
group[10][7]=new Option("Serviços administrativos para terceiros","7499-3/05")
group[10][8]=new Option("Serviços de cobran�a e de informações cadastrais","7499-3/08")
group[10][9]=new Option("Serviços de contatos telef�nicos","7499-3/03")
group[10][10]=new Option("Serviços de decora��o de interiores","7499-3/06")
group[10][11]=new Option("Serviços de leiloeiros","7499-3/04")
group[10][12]=new Option("Serviços de organiza��o de festas e eventos - exceto culturais e desportivos","7499-3/07")
group[10][13]=new Option("Serviços de tradu��o, interpreta��o e similares","7499-3/01")

group[11][0]=new Option("Pesquisas de mercado e de opini�o p�blica","7413-6/00")

group[12][0]=new Option("Agenciamento e loca��o de espa�os publicit�rios","7440-3/02")
group[12][1]=new Option("Ag�ncias de publicidade e propaganda","7440-3/01")
group[12][2]=new Option("Outros serviços de publicidade","7440-3/99")


group[14][0]=new Option("Loca��o de m�o-de-obra","7450-0/02")
group[14][1]=new Option("Sele��o e agenciamento de m�o-de-obra","7450-0/01")

group[15][0]=new Option("Atividades de prospec��o geológica","7420-9/04")
group[15][1]=new Option("Outros serviços t�cnicos especializados","7420-9/99")
group[15][2]=new Option("Serviços de desenho t�cnico especializado","7420-9/05")
group[15][3]=new Option("Serviços t�cnicos de arquitetura","7420-9/01")
group[15][4]=new Option("Serviços t�cnicos de cartografia, topografia e geod�sia","7420-9/03")
group[15][5]=new Option("Serviços t�cnicos de engenharia","7420-9/02")

var temp=document.contacts_form.lista_ramos_de_atividade_codCNAE5
function redirect(x){
for (m=temp.options.length-1;m>0;m--)
temp.options[m]=null
for (i=0;i<group[x].length;i++){
temp.options[i]=new Option(group[x][i].text,group[x][i].value)
}
temp.options[0].selected=true
}
//-->
</script>
<tr><td class="tr_button_form" colspan="4"><input type="submit" class="button_form" value="Inserir!"></td></tr></table></form>
<hr class="onlyscreen">
<table width="100%" class="onlyscreen">
<tr>
	<td align="right" nowrap><a class="home" href="index.php">In�cio</a>
</td>
</tr>
</table>
<div align="left"><font size="1">Powered by: <a href="http://www.dadabik.org/">DaDaBIK</a></font></div>


</td>
</tr>
</table>

</body>
</html>
