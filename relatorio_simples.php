<?php include("conexao.inc.php"); ?>
<!DOCTYPE html>
<html>
<head>
<title>Relat&oacute;rio</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="760" border="0" cellpadding="3">
  <tr> 
    <td width="380" colspan=2><img src="imagens/agroecologia_em_rede.gif" width="149" height="34"></td>
  </tr>
  <tr> 
    <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
  </tr>
  <tr> 
    <td colspan="2">
<?php
$RELATORIO = "<!DOCTYPE html>
<html>
				<body leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">
				<table width=\"760\" border=\"0\" cellpadding=\"3\">	
				  <tr> 
				    <td height=\"1\" colspan=\"2\" bgcolor=\"#000000\"> <div align=\"center\"></div></td>
				  </tr>
				  <tr> 
			    <td colspan=\"2\">";


/******************** �REAS GEOGR�FICAS ********************/
if ($tipo == "AG1") {

	$busca = strtoupper($busca);

	$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Áreas Geográficas</strong> do <strong>SisGEA</strong> com o argumento <b>\"$busca\"</b> e instituição <b>\"$inst\"</b>:<br><br>";
	echo "<p class=\"textoPreto10px\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b> e instituição <b>\"$inst\"</b> e instituição <b>\"$inst\"</b>:<br><br>";

	function untree($parent, $level) {
		$parentsql = $db->query("SELECT * FROM area_geografica WHERE ag_pertence=$parent ORDER BY ag_descricao");
	    if (!$parentsql->num_rows) {
		    return;
	    }
	    else {
	        while($branch = $parentsql->fetch_object()) {
				$echo_this = "";
                for ($x=1; $x<=$level; $x++) {
	                $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                }
	            $echo_this.="<a href=\"areas_geograficas.php?areaDesc=$branch->ag_descricao&area=$branch->ag_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$branch->ag_descricao</a><br>";
	            $RELATORIO .= $echo_this;
				echo $echo_this;
	            $rename_level = $level;
    	        untree($branch->ag_id, ++$rename_level);
            }
        }
    }
	$sql = "SELECT * FROM area_geografica WHERE UCASE(ag_descricao) LIKE '$busca%' ORDER BY ag_descricao";
	$compsql = $db->query($sql);
	$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b>:<br><br>";
	echo "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b>:<br><br>";
	$i = 1;
	while ($ag = $compsql->fetch_object()) {
		$RELATORIO .= "<br>$i. <a href=\"areas_geograficas.php?areaDesc=$ag->ag_descricao&area=$ag->ag_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>$ag->ag_descricao</strong></a><br>";
		echo "<br>$i. <a href=\"areas_geograficas.php?areaDesc=$ag->ag_descricao&area=$ag->ag_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>$ag->ag_descricao</strong></a><br>";
	    untree($ag->ag_id, 1);
		$i++;
	}
	$RELATORIO .= "</p>";
	echo "</p>";
}
else if ($tipo == "AG2") {
	$sql = "SELECT * FROM area_geografica,rel_geo_experiencia,experiencia WHERE ag_id='$area' AND ag_id=rge_id_geo ".
		   "AND ex_id=rge_id_experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>�rea Geogr�fica</strong> <b>\"$areaDesc\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>�rea Geogr�fica</strong> <b>\"$areaDesc\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\" href=\"experiencias.php?experiencia=$row->ex_id\">$row->ex_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>N�o h� experiências nesta �rea!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências nesta �rea!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
/******************** FIM �REAS GEOGR�FICAS ********************/

/******************** �REAS TEM�TICAS ********************/
else if ($tipo == "AT1") {

	$busca = strtoupper($busca);
	
	if (isset($inst) && strlen($inst) > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Áreas Temáticas</strong> com o argumento <b>\"$busca\"</b> e instituição <strong>\"$inst\"</strong>:<br><br>";
		echo "<p class=\"textoPreto10px\"><strong>Áreas Temáticas</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
	}
	else {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Áreas Temáticas</strong> com o argumento <b>\"$busca\"</b> e instituição <strong>\"$inst\"</strong>:<br><br>";
		echo "<p class=\"textoPreto10px\"><strong>Áreas Temáticas</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
	}
	
	function untree($parent, $level) {
		$parentsql = $db->query("SELECT * FROM area_tematica WHERE at_pertence=$parent ORDER BY at_descricao");
	    if (!$parentsql->num_rows) {
		    return;
	    }
	    else {
	        while($branch = $parentsql->fetch_object()) {
	            $echo_this = "";
                for ($x=1; $x<=$level; $x++) {
	                $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                }
	            $echo_this.="<a href=\"areas_tematicas.php?areaDesc=$branch->at_descricao&area=$branch->at_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$branch->at_descricao</a><br>";
	            $RELATORIO .= $echo_this;
				echo $echo_this;
	            $rename_level = $level;
    	        untree($branch->at_id, ++$rename_level);
            }
        }
    }
	$sql = "SELECT * FROM area_tematica";
	if (isset($inst) && strlen($inst) > 0) {
		$sql .= ",rel_area_inst,frm_instituicao WHERE rai_id_inst=in_id AND rai_id_area=at_id AND UCASE(in_nome) LIKE '$inst%' AND UCASE(at_descricao) LIKE '$busca%' ORDER BY at_descricao";
	}
	else {
		$sql .= " WHERE UCASE(at_descricao) LIKE '$busca%' ORDER BY at_descricao";
	}
	$compsql = $db->query($sql);
	if (isset($inst) && strlen($inst) > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b> e instituição <b>\"$inst\"</b>:<br><br>";
		echo "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b> e instituição <b>\"$inst\"</b>:<br><br>";
	}
	else {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b>:<br><br>";
		echo "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b>:<br><br>";
	}
	$i = 1;
	while ($at = $compsql->fetch_object()) {
		$RELATORIO .= "<br>$i. <a href=\"areas_tematicas.php?areaDesc=$at->at_descricao&area=$at->at_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>$at->at_descricao</strong></a><br>";
		echo "<br>$i. <a href=\"areas_tematicas.php?areaDesc=$at->at_descricao&area=$at->at_id\" class=\"preto\"><strong>$at->at_descricao</strong></a><br>";
	    untree($at->at_id, 1);
		$i++;
	}
	$RELATORIO .= "</p>";
	echo "</p>";
} 
else if ($tipo == "AT2") {
	$sql = "SELECT * FROM area_tematica,rel_area_experiencia,experiencia WHERE at_id=$area AND at_id=rae_id_area ".
		   "AND ex_id=rae_id_experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>�rea Tem�tica</strong> <b>\"$areaDesc\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>�rea Tem�tica</strong> <b>\"$areaDesc\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->ex_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>N�o h� experiências nesta �rea!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências nesta �rea!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
	$RELATORIO .= "<br><br>";
	echo "<br><br>";
	
	$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">Foram encontrados as seguintes <strong>tecnologias</strong> para a �rea <b>\"$areaDesc\"</b>:<br><br>";
	echo "<p class=\"textoPreto10px\">Foram encontrados as seguintes <strong>tecnologias</strong> para a �rea <b>\"$areaDesc\"</b>:<br><br>";
	function untreeTec($parent, $level) {
		$parentsql = $db->query("SELECT * FROM tecnologia WHERE tc_pertence=$parent ORDER BY tc_descricao");
	    if (!$parentsql->num_rows) {
		    return;
	    }
	    else {
	        while($branch = $parentsql->fetch_object()) {
	            $echo_this = "";
                for ($x=1; $x<=$level; $x++) {
	                $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                }
	            $echo_this.="<a href=\"tecnologias.php?tecDesc=$branch->tc_descricao&tec=$branch->tc_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$branch->tc_descricao</a><br>";
	            $RELATORIO .= $echo_this;
				echo $echo_this;
	            $rename_level = $level;
    	        untreeTec($branch->tc_id, ++$rename_level);
            }
        }
    }
	$compsql = $db->query("SELECT * FROM area_tematica,rel_area_tec,tecnologia WHERE at_id=$area AND at_id=rat_id_at AND tc_id=rat_id_tc");
	$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">Foram encontrados as seguintes tecnologias para a �rea tem�tica <b>\"$areaDesc\"</b>:<br><br>";
	echo "<p class=\"textoPreto10px\">Foram encontrados as seguintes tecnologias para a �rea tem�tica <b>\"$areaDesc\"</b>:<br><br>";
	$i = 1;
	while ($tc = $compsql->fetch_object()) {
		$RELATORIO .= "<br>$i. <a href=\"tecnologias.php?tecDesc=$tc->tc_descricao&area=$tc->tc_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>$tc->tc_descricao</strong></a><br>";
		echo "<br>$i. <a href=\"tecnologias.php?tecDesc=$tc->tc_descricao&area=$tc->tc_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>$tc->tc_descricao</strong></a><br>";
	    untreeTec($tc->tc_id, 1);
		$i++;
	}
	$RELATORIO .= "</p>";
	echo "</p>";
}
/******************** FIM �REAS TEM�TICAS ********************/

/******************** ENTIDADES BIOL�GICAS ********************/
else if ($tipo == "EB1") {
	$busca = strtoupper($busca);
	$sql = "SELECT * FROM entidade_biologica WHERE UCASE(eb_descricao) LIKE '$busca%' ORDER BY eb_descricao";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Animais e Plantas</strong> do <strong>SisGEA</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\"><strong>Animais e Plantas</strong> do <strong>SisGEA</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"entidades_biologicas.php?bioDesc=$row->eb_descricao&bio=$row->eb_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->eb_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"entidades_biologicas.php?bioDesc=$row->eb_descricao&bio=$row->eb_id\" class=\"preto\">$row->eb_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Sua busca n�o retornou resultados!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
else if ($tipo == "EB2") {
	$sql = "SELECT * FROM entidade_biologica,rel_entidade_experiencia,experiencia WHERE eb_id=$bio AND eb_id=ree_id_entidade ".
		   "AND ex_id=ree_id_experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>Animal ou Planta</strong> <b>\"$bioDesc\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>Animal ou Planta</strong> <b>\"$bioDesc\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->ex_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>N�o h� experiências para estas Animais e Plantas!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências para estas Animais e Plantas!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
/******************** FIM ENTIDADES BIOL�GICAS ********************/

/******************** LOCALIDADES********************/
else if ($tipo == "LC1") {
	$busca = strtoupper($busca);
	$sql = "SELECT * FROM localidade WHERE UCASE(lc_descricao) LIKE '$busca%' ORDER BY lc_descricao";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Localidades</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\"><strong>Localidades</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"localidades.php?lcDesc=$row->lc_descricao&local=$row->lc_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->lc_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"localidades.php?lcDesc=$row->lc_descricao&local=$row->lc_id\" class=\"preto\">$row->lc_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Sua busca n�o retornou resultados!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
else if ($tipo == "LC2") {
	$sql = "SELECT * FROM localidade,rel_local_experiencia,experiencia WHERE lc_id=$local AND lc_id=rle_id_local ".
		   "AND ex_id=rle_id_experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>Localidade</strong> <b>\"$lcDesc\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>Localidade</strong> <b>\"$lcDesc\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->ex_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>N�o h� experiências nesta localidade!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências nesta localidade!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
/******************** FIM LOCALIDADES ********************/

/******************** VEGETA��O ********************/
else if ($tipo == "VG1") {
	$busca = strtoupper($busca);
	$sql = "SELECT * FROM vegetacao WHERE UCASE(vg_descricao) LIKE '$busca%' ORDER BY vg_descricao";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Tipos de Vegetação</strong>  com o argumento <b>\"$busca\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\"><strong>Tipos de Vegetação</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"vegetacao.php?vgDesc=$row->vg_descricao&vegetacao=$row->vg_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->vg_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"vegetacao.php?vgDesc=$row->vg_descricao&vegetacao=$row->vg_id\" class=\"preto\">$row->vg_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Sua busca n�o retornou resultados!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
else if ($tipo == "LC2") {
	$sql = "SELECT * FROM vegetacao,rel_veg_experiencia,experiencia WHERE vg_id=$vegetacao AND vg_id=rve_id_veg ".
		   "AND ex_id=rve_id_experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para o <strong>Tipo de Vegetação</strong> <b>\"$vgDesc\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para o <strong>Tipo de Vegetação</strong> <b>\"$vgDesc\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->ex_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>N�o h� experiências neste tipo de vegeta��o!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências neste tipo de vegeta��o!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
/******************** FIM LOCALIDADES ********************/

/******************** ZONAS CLIM�TICAS ********************/
else if ($tipo == "ZC1") {
	$busca = strtoupper($busca);
	$sql = "SELECT * FROM zona_climatica WHERE UCASE(zc_descricao) LIKE '$busca%' ORDER BY zc_descricao";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Zonas Climáticas</strong>  com o argumento <b>\"$busca\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\"><strong>Zonas Climáticas</strong>  com o argumento <b>\"$busca\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"vegetacao.php?zcDesc=$row->zc_descricao&zona=$row->zc_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->zc_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"vegetacao.php?zcDesc=$row->zc_descricao&zona=$row->zc_id\" class=\"preto\">$row->zc_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Sua busca n�o retornou resultados!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
else if ($tipo == "ZC2") {
	$sql = "SELECT * FROM zona_climatica,rel_zona_experiencia,experiencia WHERE zc_id=$zona AND zc_id=rze_id_zona ".
		   "AND ex_id=rze_id_experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para o <strong>Tipo de Vegetação</strong> <b>\"$vgDesc\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>Zona Clim�tica</strong> <b>\"$zcDesc\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->ex_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>N�o h� experiências nesta zona clim�tica!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências nesta zona clim�tica!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
/******************** FIM ZONAS ********************/

/******************** INSTITUI��ES ********************/
else if ($tipo == "IN1") {
	$busca = strtoupper($busca);
	if (isset($localizacao) && $localizacao == "1") {
		if (strlen(trim($tipoInst)) == 0) {
			$sql = "SELECT distinct(in_id),in_nome,ti_id,ti_descricao FROM frm_instituicao,_BR as c,tipo_instituicoes,tipo_enderecos ".
				   "WHERE in_tipo=ti_id AND te_id=ei_id_endereco AND ei_cidade=c.id ".
				   "AND c.nome like '$cidade%' AND ei_estado LIKE '$estado%' AND ei_pais LIKE '$pais%' ".
				   "AND ei_cep LIKE '$cep%' AND UCASE(in_nome) LIKE '$busca%' ORDER BY ti_descricao,in_nome";
		}
		else {
			$sql = "SELECT distinct(in_id),in_nome,ti_id,ti_descricao FROM frm_instituicao,_BR as c,tipo_instituicoes,tipo_enderecos ".
				   "WHERE in_tipo=ti_id AND te_id=ei_id_endereco AND ei_cidade=c.id ".
				   "AND c.nome like '$cidade%' AND ei_estado LIKE '$estado%' AND ei_pais LIKE '$pais%' ".
				   "AND ei_cep LIKE '$cep%' AND UCASE(in_nome) LIKE '$busca%' AND in_tipo=$tipoInst ORDER BY ti_descricao,in_nome";
		}
	}
	else {
		if (strlen(trim($tipoInst)) == 0) {
			$sql = "SELECT * FROM frm_instituicao,tipo_instituicoes WHERE in_tipo=ti_id AND UCASE(in_nome) LIKE '$busca%' ORDER BY ti_descricao,in_nome";
		}
		else {
			$sql = "SELECT * FROM frm_instituicao,tipo_instituicoes WHERE in_tipo=ti_id AND UCASE(in_nome) LIKE '$busca%' AND in_tipo=$tipoInst ORDER BY ti_descricao,in_nome";
		}
	}
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Institui��es</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\"><strong>Institui��es</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
		$i = 1;
		$flagTipo = "";
		while ($row = $query->fetch_object()) {
			if ($flagTipo != $row->ti_id) {
				$i = 1;
				$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"instituicoes.php?busca=&tipoDesc=$row->ti_descricao&tipo=$row->ti_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>traduz($row->ti_descricao)</strong></a><br><br>";
				echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"instituicoes.php?busca=&tipoDesc=$row->ti_descricao&tipo=$row->ti_id\" class=\"preto\"><strong>traduz($row->ti_descricao)</strong></a><br><br>";
			}
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"instituicoes.php?instDesc=$row->in_nome&inst=$row->in_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->in_nome</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"instituicoes.php?instDesc=$row->in_nome&inst=$row->in_id\" class=\"preto\">$row->in_nome</a><br><br>";
			$i++;
			$flagTipo = $row->ti_id;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Sua busca n�o retornou resultados!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
else if ($tipo == "IN2") {		  		   
    $sql = "SELECT * FROM frm_instituicao,rel_inst_experiencia,experiencia WHERE in_id=$inst AND in_id=rie_id_inst ".
		   "AND ex_id=rie_id_experiencia AND ex_liberacao='S'  order by ex_chamada";
		   
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>Instituição</strong> <b>\"$instDesc\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\">As seguintes <strong>experiências</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>Instituição</strong> <b>\"$instDesc\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->ex_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>N�o h� experiências para esta instituição!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências para esta instituição!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
		
}
/******************** FIM INSTITUI��ES ********************/

/******************** TECNOLOGIAS ********************/

else if ($tipo == "TC1") {

	$busca = strtoupper($busca);
	
	$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>Tecnologias</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
	echo "<p class=\"textoPreto10px\"><strong>Tecnologias</strong> com o argumento <b>\"$busca\"</b>:<br><br>";
	function untreeTec($parent, $level) {
		$parentsql = $db->query("SELECT * FROM tecnologia WHERE tc_pertence=$parent ORDER BY tc_descricao");
	    if (!$parentsql->num_rows) {
		    return;
	    }
	    else {
	        while($branch = $parentsql->fetch_object()) {
	            $echo_this = "";
                for ($x=1; $x<=$level; $x++) {
	                $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                }
	            $echo_this.="<a href=\"tecnologias.php?tecDesc=$branch->tc_descricao&tec=$branch->tc_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$branch->tc_descricao</a><br>";
	            $RELATORIO .= $echo_this;
				echo $echo_this;
	            $rename_level = $level;
    	        untreeTec($branch->tc_id, ++$rename_level);
            }
        }
    }
	$compsql = $db->query("SELECT * FROM tecnologia WHERE UCASE(tc_descricao) LIKE '$busca%' ORDER BY tc_descricao");
	$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">Foram encontrados as seguintes tecnologias para o argumento <b>\"$busca\"</b>:<br><br>";
	echo "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">Foram encontrados as seguintes tecnologias para o argumento <b>\"$busca\"</b>:<br><br>";
	$i = 1;
	while ($tc = $compsql->fetch_object()) {
		$RELATORIO .= "<br>$i. <a href=\"tecnologias.php?tecDesc=$tc->tc_descricao&area=$tc->tc_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>$tc->tc_descricao</strong></a><br>";
		echo "<br>$i. <a href=\"tecnologias.php?tecDesc=$tc->tc_descricao&area=$tc->tc_id\" class=\"preto\"><strong>$tc->tc_descricao</strong></a><br>";
	    untreeTec($tc->tc_id, 1);
		$i++;
	}
	$RELATORIO .= "</p>";
	echo "</p>";
}
else if ($tipo == "TC2") {
	$sql = "SELECT * FROM area_tematica,rel_area_tec,tecnologia WHERE tc_id=$tec AND at_id=rat_id_at ".
		   "AND tc_id=rat_id_tc";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		$RELATORIO .= "<p style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">As seguintes <strong>Áreas Temáticas</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>Tecnologia</strong> <b>\"$tecDesc\"</b>:<br><br>";
		echo "<p class=\"textoPreto10px\">As seguintes <strong>Áreas Temáticas</strong> est�o cadastradas no <strong>Agroecologia em Rede</strong> para a <strong>Tecnologia</strong> <b>\"$tecDesc\"</b>:<br><br>";
		$i = 1;
		while ($row = $query->fetch_object()) {
			$RELATORIO .= "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"areas_tematicas.php?areaDesc=$row->at_descricao&area=$row->at_id\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\">$row->at_descricao</a><br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"areas_tematicas.php?areaDesc=$row->at_descricao&area=$row->at_id\" class=\"preto\">$row->at_descricao</a><br><br>";
			$i++;
		}
		$RELATORIO .= "</p>";
		echo "</p>";			
	} 
	else {
		$RELATORIO .= "<p align=\"center\" style=\"font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color: #000000\"><strong>N�o h� �reas tem�ticas relacionadas com esta tecnologia!</strong></p>";
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� �reas tem�ticas relacionadas com esta tecnologia!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados
}
/******************** FIM TECNOLOGIAS ********************/

$RELATORIO .= "	</td>
			  </tr>
			</table>
			</body>
			</html>";
			
if ($acao == "IM") {
?>
<script language="JavaScript">
window.print();
alert('O relatório está sendo impresso. Você será redirecionado dentro de 10 segundos.');
var delay = 10; // Delay in seconds
var targetURL = "index.php"; // URl to load
setTimeout('self.location.replace(targetURL)', (delay * 1000));
</script>
<?php
	return;
}
else {
	$headers = "From: Agroecologia em rede <victor@aspta.org.br>\n";
	$headers .= "X-Sender: <sisgea@cnip.org.br>\n";
	$headers .= "X-Mailer: PHP\n";
	$headers .= "X-Priority: 0\n";
	$headers .= "Return-Path: <victor@aspta.org.br>\n";
	$headers .= "Content-Transfer-Encoding: 8bit\n";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-Type: text/html; charset=iso-8859-1" ;

	$mail = mail($email, "RELAT�RIO AGROECOLOGIA EM REDE",$RELATORIO,$headers);
	if ($mail == FALSE) {
?>
		<script language="JavaScript">
			alert('Houve um problema no envio do e-mail para o cliente!');
		</script>
<?php

	} // fim do if que verifica se o e-mail foi enviado
	
} // Fim do if que verifica se o relatório deve ser impresso ou enviado por e-mail
?>
	</td>
  </tr>
</table>
</body>
</html>
