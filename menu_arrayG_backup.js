// Milonic DHTML Menu version 3.3
// Please note that major changes to this file have been made and is not compatible with earlier versions..
//
// You no longer need to number your menus as in previous versions. 
// The new menu structure allows you to name the menu instead. This means that you to remove menus and not break the system.
// The structure should also be much easier to modify, add & remove menus and menu items.
// 
// If you are having difficulty with the menu please read the FAQ at http://www.milonic.co.uk/menu/faq.php before contacting us.
//
// Please note that the above text CAN be erased if you wish.


// Milonic DHTML Menu version 3.2.0
// The following line is critical for menu operation, and must appear only once.
menunum=0;menus=new Array();_d=document;function addmenu(){menunum++;menus[menunum]=menu;}function dumpmenus(){mt="<script language=javascript>";for(a=1;a<menus.length;a++){mt+=" menu"+a+"=menus["+a+"];"}mt+="<\/script>";_d.write(mt)}
//Please leave the above line intact. The above also needs to be enabled.



////////////////////////////////////
// Editable properties START here777777 //
////////////////////////////////////

timegap=500					// The time delay for menus to remain visible
followspeed=5				// Follow Scrolling speed
followrate=20				// Follow Scrolling Rate
suboffset_top=9;			// Sub menu offset Top position 
suboffset_left=20;			// Sub menu offset Left position

effect = "fade(duration=0.2);Shadow(color='#ffffcc', Direction=135, Strength=10)" // Special effect string for IE5.5 or above please visit http://www.milonic.co.uk/menu/filters_sample.php for more filters

style1=[					// style1 is an array of properties. You can have as many property arrays as you need. This means that menus can have their own style.
"000000",					// Mouse Off Font Color
"F1B616",					// Mouse Off Background Color
"006633",					// Mouse On Font Color
"ffffcc",					// Mouse On Background Color
"000000",					// Menu Border Color
9,						    // Font Size
"normal",					// Font Style 
"bold",					    // Font Weight
"Verdana,Tahoma,Helvetica",	// Font Name
4,						    // Menu Item Padding
,							// Sub Menu Image (Leave this blank if not needed)
2,						    // 3D Border & Separator bar
"000000",					// 3D High Color "3666ff"
"000000",					// 3D Low Color
,							// Referer Item Font Color (leave this blank to disable)
,							// Referer Item Background Color (leave this blank to disable)
,							// Top Bar image (Leave this blank to disable)
,							// Menu Header Font Color (Leave blank if headers are not needed)
,							// Menu Header Background Color (Leave blank if headers are not needed)
]


addmenu(menu=[				// This is the array that contains your menu properties and details
"menu_principal",       		// Menu Name - This is needed in order for the menu to be called
52,					// Menu Top - The Top position of the menu in pixels
-1,					// Menu Left - The Left position of the menu in pixels
110,			    // Menu Width - Menus width in pixels
0,					// Menu Border Width 
"left",		        	    // Screen Position - here you can use "center;left;right;middle;top;bottom" or a combination of "center:middle"
style1,						// Properties Array - this is set higher up, as above
1,							// Always Visible - allows the menu item to be visible at all time
"center",					// Alignment - sets the menu elements text alignment, values valid here are: left, right or center
,							// Filter - Text variable for setting transitional effects on menu activation - see above for more info
,							// Follow Scrolling - Tells the menu item to follow the user down the screen (visible at all times)
1, 							// Horizontal Menu - Tells the menu to become horizontal instead of top to bottom style
,							// Keep Alive - Keeps the menu visible until the user moves over another menu or clicks elsewhere on the page
"right",					// Position of TOP sub image left:center:right
"form",						// Type of menu use "form" or blank
,							// Right To Left - Used in Hebrew for example.
,							// Open the Menus OnClick - leave blank for OnMouseover
,							// ID of the div you want to hide on MouseOver (useful for hiding form elements)
,							// Reserved for future use
,							// Reserved for future use
,							// Reserved for future use
,"Banco Experiências","banco_experiencias.php target=_self",,,1		        // "Description Text", "URL", "Alternate URL", "Status", "Separator Bar"
,"Banco Pesquisas","banco_pesquisas.php target=_self",,"",1
,"Contatos","show-menu=menu_contatos",,"",1
,"Cadastros","show-menu=menu_cadastros",,"",1
//,"Mapa iniciativas","mapeo/index.php target=_self",,"",1
,"Fale Conosco","contato.php target=_self",,"",1
,"Links","links.php target=_self",,"",1
,"Sobre","index.php?f=inicio target=_self",,"",1
])

addmenu(menu=["menu_contatos",
,,170,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Pessoas", "usuarios.php target=_self",,,0
," Instituições", "instituicoes.php target=_self",,,0
])
addmenu(menu=["menu_cadastros",
,,170,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Pessoas", "usuarios_inclusao.php target=_self",,,0
," Instituições", "login.php target=_self",,,0
," Experiências sistematizadas", "login.php target=_self",,,0
," Pesquisas", "login.php target=_self",,,0
," Experiências identificadas", "mapeo/index.php target=_self",,,0
," Pendências", "login.php target=_self",,,0
])



//////////////////////////////////
// Editable properties END here  //
//////////////////////////////////
dumpmenus() // This must be the last line in this file
