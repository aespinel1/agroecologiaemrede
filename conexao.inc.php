<?php
require_once 'mapeo/config.php';

require_once $dir['apoio'].'funcoes_comuns.php';
//$maquinalocal=1;
//require_once $dir['base'].'DB.php';

$db = new mysqli($bd['servidor'],$bd['usuaria'],$bd['senha'],$bd['agrorede']);
if ($db->connect_errno) {
    printf("Connect failed: %s\n", $db->connect_error);
    exit();
}
$db->set_charset('utf8');
$db->query('SET sql_mode=(SELECT REPLACE(@@sql_mode, "ONLY_FULL_GROUP_BY", ""))');
// Aqui faço o processo de detecção de língua, se a pessoa não está conectada:
/*
// Por enquanto, nesta nova versão, deixarei a detecção de língua inativa: ficará apenas em português.
// TODO: Atualizar detecção de idioma para funcionar em celulares e sistemas modernos (html5)
if (!isset($lingua)) {
	if ((isset($_REQUEST['lingua']) && $_REQUEST['lingua']) || (isset($_COOKIE['agrorede_lingua']) && $_COOKIE['agrorede_lingua'])) {
		$lingua = (isset($_REQUEST['lingua']) && $_REQUEST['lingua'])
			? $_REQUEST['lingua']
			: $_COOKIE['agrorede_lingua'];
	} else {
		//Aqui eu detecto a língua pelo BROWSER:
		require_once $dir['apoio'].'classe_detecta_lingua.php';
		$detectalinguas = new LanguageDetect();
		$linguas = $detectalinguas->getLanguagesList($tab_linguas);
		$lingua = (isset($linguas) && $linguas)
			 ? key($linguas)
			 : $padrao_lingua;
	}
}
*/
$lingua = $padrao_lingua;

//Aqui carrego as mensagens na língua $lingua:
include $dir['apoio']."textos/".$lingua.'.php';
foreach ($txt as $i=>$t)
	$txt[$i]=$t;
//Aqui carrego as mensagens que vêm do mysql (tabela "texto") e enfio na variável $txt:
$sql = "SELECT tex_id, tex_texto, tex_titulo FROM texto";
$query = $db->query($sql);
$tmp = array();
while ($r = $query->fetch_object())
	$tmp[$r->tex_id]=$r;
foreach ($tmp as $n=>$t) {
	$tmp[$n]->tex_texto = traduz($t->tex_texto,$lingua);
	$tmp[$n]->tex_titulo = traduz($t->tex_titulo,$lingua);
	unset($tmp[$n]->tex_id);
}
$txt['mysql']=$tmp;
if (!session_id())
	session_start();
$_SESSION['agrorede_lingua']=$lingua;

$paises=array();
$sql="SELECT id FROM ".$bd['refs'].".__paises WHERE continente='ALC'";
$query = $db->query($sql);
while ($rowParam = $query->fetch_object())
	$paises[]=$rowParam->id;
$where_select_paises = 'id IN (';
foreach ($paises as $pa)
	$where_select_paises.='"'.$pa.'", ';
$where_select_paises = substr($where_select_paises,0,-2).')';

foreach ($_REQUEST as $k=>$v) {
  ${$k} = $v;
}

?>
