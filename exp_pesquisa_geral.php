<?php
include("conexao.inc.php");

if (strlen($cookieUsuarioPub) != 0) {

	$sql = "SELECT us_nome FROM usuario WHERE us_login='$cookieUsuarioPub' AND us_session='$cookieSessionPub'";
	$query = $db->query($sql);
	if (!$query) {
    	die($db->error);
	}
	$row = $query->fetch_object();
	$nome = trim($row->us_nome);
}

$sql = "SELECT count(ex_id) AS count FROM experiencia";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}
$row = $query->fetch_object();
$qtExperiencias = $row->count;

$sql = "SELECT count(us_login) AS count FROM usuario";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}
$row = $query->fetch_object();
$qtUsuarios = $row->count;

$sql = "SELECT count(in_id) AS count FROM frm_instituicao";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}
$row = $query->fetch_object();
$qtInstituicoes = $row->count;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57">&nbsp; </td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top">
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr> 
          <td colspan="2"><p><strong>BANCO DE EXPERI&Ecirc;NCIAS 
              EM AGROECOLOGIA</strong></p></td>
        </tr>
        <tr> 
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr> 
          <td width="290"> <form name="form1" method="post" action="exp_pesquisa_geral.php">
              <table width="290" border="0" cellspacing="0" cellpadding="3">
                <tr> 
                  <td width="217"> <input name="busca" type="text" id="busca" size="40"></td>
                  <td width="61"> <input name="Submit" type="submit" class="botaoLogin" value="Buscar" class="btn btn-success"></td>
                </tr>
                <tr> 
                  <td> <input type="checkbox" name="checkbox" value="checkbox">
                    Vasculhar base</td>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </form></td>
          <td width="254" valign="top"><img src="imagens/seta02.gif" width="12" height="13"> 
            <a href="experiencias_sobre.php" class="Preto">O que &eacute; Experi&ecirc;ncia 
            em Agroecologia?</a><br> <img src="imagens/seta02.gif" width="12" height="13"> 
            <a href="experiencias_estatisticas.php" class="Preto">O que voc&ecirc; 
            encontra aqui?</a></td>
        </tr>
        <tr> 
          <td colspan="2"><p><strong>&Aacute;REAS TEM&Aacute;TICAS</strong></p></td>
        </tr>
        <tr> 
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr> 
          <td colspan="2"><table width="555" border="0" cellpadding="10" cellspacing="2">
              <tr bgcolor="#ACCBD0"> 
                <td width="125" onmouseover="mOvr(this,'#86AEB0');" onmouseout="mOut(this,'#ACCBD0');" onclick="gotourl('contato_tecnicos.php');"> 
                  <div align="center"><strong>Sistemas de Cria&ccedil;&atilde;o 
                    Animal</strong></div></td>
                <td width="247" onmouseover="mOvr(this,'#86AEB0');" onmouseout="mOut(this,'#ACCBD0');" onclick="gotourl('contato_tecnicos.php');"> 
                  <div align="center"><strong>Sistemas Agroflorestais Agroextrativismo</strong></div></td>
                <td width="149" onmouseover="mOvr(this,'#86AEB0');" onmouseout="mOut(this,'#ACCBD0');" onclick="gotourl('contato_tecnicos.php');"> 
                  <div align="center"><strong>Manejo de Recursos H&iacute;dricos</strong></div></td>
              </tr>
              <tr bgcolor="#ACCBD0"> 
                <td onmouseover="mOvr(this,'#86AEB0');" onmouseout="mOut(this,'#ACCBD0');" onclick="gotourl('contato_tecnicos.php');"> 
                  <div align="center"><strong>Sementes</strong></div></td>
                <td onmouseover="mOvr(this,'#86AEB0');" onmouseout="mOut(this,'#ACCBD0');" onclick="gotourl('contato_tecnicos.php');"> 
                  <div align="center"><strong>Alimenta&ccedil;&atilde;o, Sa&uacute;de 
                    e Plantas Medicinais</strong></div></td>
                <td onmouseover="mOvr(this,'#86AEB0');" onmouseout="mOut(this,'#ACCBD0');" onclick="gotourl('contato_tecnicos.php');"> 
                  <div align="center"><strong>Desenvolvimento Rural</strong></div></td>
              </tr>
              <tr bgcolor="#ACCBD0"> 
                <td onmouseover="mOvr(this,'#86AEB0');" onmouseout="mOut(this,'#ACCBD0');" onclick="gotourl('contato_tecnicos.php');"> 
                  <div align="center"><strong>Sistemas de Produ&ccedil;&atilde;o 
                    Agr&iacute;cola</strong></div></td>
                <td onmouseover="mOvr(this,'#86AEB0');" onmouseout="mOut(this,'#ACCBD0');" onclick="gotourl('contato_tecnicos.php');"> 
                  <div align="center"><strong>Armazenamento, Transforma&ccedil;&atilde;o, 
                    Comercializa&ccedil;&atilde;o, Certifica&ccedil;&atilde;o</strong></div></td>
                <td onmouseover="mOvr(this,'#86AEB0');" onmouseout="mOut(this,'#ACCBD0');" onclick="gotourl('contato_tecnicos.php');"> 
                  <div align="center"><strong>Agricultura Urbana</strong></div></td>
              </tr>
            </table></td>
        </tr>
        <tr> 
          <td colspan="2"><p><strong>OUTRAS OP&Ccedil;&Otilde;ES 
              DE BUSCA</strong></p></td>
        </tr>
        <tr> 
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr> 
          <td colspan="2"><table width="555" border="0" cellpadding="10" cellspacing="2">
              <tr bgcolor="#B9D69E"> 
                <td width="209" onclick="gotourl('areas_geograficas.php');" onmouseover="mOvr(this,'#89BA5C');" onmouseout="mOut(this,'#B9D69E');"> 
                  <div align="center"><strong>&Aacute;reas 
                    Geogr&aacute;ficas</strong></div></td>
                <td width="90" onclick="gotourl('contato_tecnicos.php');" onmouseover="mOvr(this,'#89BA5C');" onmouseout="mOut(this,'#B9D69E');"> 
                  <div align="center"><strong>Biomas</strong></div></td>
                <td width="188" onclick="gotourl('exp_pesquisa_avancada.php');" onmouseover="mOvr(this,'#89BA5C');" onmouseout="mOut(this,'#B9D69E');"> 
                  <div align="center"><strong>Busca 
                    Avan&ccedil;ada</strong></div></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
