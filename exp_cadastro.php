<?php
include("conexao.inc.php");

$sql = "SELECT * FROM usuario WHERE us_login='$cookieUsuarioPub' AND us_session='$cookieSessionPub'";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='$cookieSessionPub' AND fu_login='$cookieUsuarioPub' AND (fu_id='040000' OR fu_id='040100')";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaFormExp() {
	desc = formNE.nome.value;
	if (desc.length == 0) {
		alert('Digite um t�tulo v�lido para a experiência!');
		formNE.nome.focus();
		return;
	}
	formNE.submit();
}

function criticaFormAtualiza() {
	nome = formAtualiza.nome.value;
	if (nome.length == 0) {
		alert('Digite um nome v�lido para a experiência!');
		formAtuliza.nome.focus();
		return;
	}
	
	formAtualiza.submit();
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
</script>
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57">&nbsp; </td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Cadastro 
              de experi&ecirc;ncia</strong>&nbsp;&nbsp;
			  <?php if (strlen($cookieUsuarioPub) != 0) { ?>
			  [ <a href="xt_logout.php" class="Preto">LOGOUT</a> ]
			  <?php } ?>
			  </p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr> 
          <td colspan="2"><p><strong>Op��es</strong></p></td>
        </tr>
        <tr> 
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr> 
          <td width="283" valign="top"><img src="imagens/seta02.gif" width="12" height="13"> 
            <a href="exp_cadastro.php" class="Preto">Cadastro de Experi&ecirc;ncia</a> 
          </td>
          <td width="283" valign="top"><img src="imagens/seta02.gif" width="12" height="13"> 
            <a href="experiencias_arquivos.php" class="Preto">Gravar arquivos</a><br> 
            <br>
            <img src="imagens/seta02.gif" width="12" height="13"> <a href="experiencias_imagens.php" class="Preto">Gravar 
            imagens</a></td>
        </tr>
      </table>
      <br>
<?php
if ($numPerm > 0) {

	if (isset($novaExperiencia)) {
		echo "<p align=\"center\" class=\"textoPreto10px\">Experiência <b>$expDescricao</b> cadastrada! Esta experiência foi submetida para o administrador do sistema para uma avalia��o contextual";
	}
?>
	<form name="formNE" method="post" action="xt_exp_insere_experiencia.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td colspan="4"> <p> <strong>Cadastrar nova 
                experiência</strong></p></td>
          </tr>
          <tr> 
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td width="159"> <div align="right"><strong>T&iacute;tulo</strong>:</div></td>
            <td colspan="3"><input name="nome" type="text" id="nome" size="69" maxlength="255"></td>
          </tr>
          <tr> 
            <td> <div align="right"><strong>Tipo</strong>:</div></td>
            <td colspan="3"><select name="tipoExp" id="tipoExp">
                <?php
			$sql = "SELECT * FROM tipo_experiencias ORDER BY tx_descricao";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->tx_id; ?>"><?php echo trim($row->tx_descricao); ?></option>
                <?php
			} // la�o do loop
?>
              </select></td>
          </tr>
          <tr> 
            <td> <div align="right"><strong>Status</strong>:</div></td>
            <td width="162"> <select name="status" id="status">
                <?php
			$sql = "SELECT * FROM status_experiencia";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->se_id; ?>"><?php echo trim($row->se_status); ?></option>
                <?php
			} // la�o do loop
?>
              </select></td>
            <td width="106"> <div align="right"><strong>Acomp. 
                T&eacute;cnico</strong>:</div></td>
            <td width="95"> <select name="acompanha">
                <option value="1">Sim</option>
                <option value="0">N&atilde;o</option>
              </select></td>
          </tr>
          <tr> 
            <td valign="top"> <div align="right"><strong>Autor(es)</strong>:</div></td>
            <td colspan="3"> <select name="autor[]" size="6" multiple id="autor[]">
                <?php
		$sql = "SELECT * FROM usuario WHERE us_autor='1' ORDER BY us_login";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select> <select name="instAutor[]" size="6" multiple id="instAutor[]">
                <?php
		$sql = "SELECT in_id,in_nome FROM frm_instituicao ORDER BY in_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select></td>
          </tr>
          <tr> 
            <td valign="top"> <div align="right"><strong>Relator(es)</strong>:</div></td>
            <td colspan="3"><select name="relator[]" size="6" multiple id="relator">
                <?php
		$sql = "SELECT * FROM usuario WHERE us_relator='1' ORDER BY us_login";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select> <select name="instRelator[]" size="6" multiple id="instRelator[]">
                <?php
		$sql = "SELECT in_id,in_nome FROM frm_instituicao ORDER BY in_nome";
		$query = $db->query($sql);
		while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select></td>
          </tr>
          <tr> 
            <td valign="top"> <div align="right"><strong>Palavras-chave</strong>:<br>
                <br>
                Digite no campo ao lado palavras-chaves relacionadas com esta 
                experi&ecirc;ncia. Isto ir&aacute; otimizar as buscas no futuro. 
                Utilize ponto-e-v&iacute;rgula (;)para separar as palavras. </div></td>
            <td colspan="3"><textarea name="chaves" cols="67" rows="10" id="chaves"></textarea></td>
          </tr>
          <tr> 
            <td valign="top"> <div align="right"><strong>Resumo</strong>:<br>
                <br>
                Tags HTML s&atilde;o aceitos. &lt;i&gt;&lt;/i&gt; para it&aacute;lico, 
                &lt;b&gt;&lt;/b&gt; para negrito. As quebras de linha s&atilde;o 
                automaticamente convertidas em quebras na impress&atilde;o do 
                relat&oacute;rio. </div></td>
            <td colspan="3"><textarea name="resumo" cols="67" rows="10" id="resumo"></textarea></td>
          </tr>
          <tr> 
            <td valign="top"><div align="right"><strong>&Aacute;reas 
                Geogr&aacute;ficas</strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla 
                CTRL do seu teclado.</div></td>
            <td colspan="3" valign="top"> <select name="geo[]" size="6" multiple>
                <?php
				//$sql = "SELECT * FROM area_geografica ORDER BY ag_descricao";
				$sql = "SELECT a.ag_id,a.ag_descricao,b.ag_descricao as bdesc FROM area_geografica a LEFT JOIN area_geografica b ON a.ag_pertence=b.ag_id ORDER BY a.ag_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->ag_descricao);
					$desc2 = trim($row->bdesc);
					if (strlen(trim($desc2)) > 0) {
						$temp = "($desc2)";
					}
					echo "<option value=\"$row->ag_id\">$desc $temp</option>";
					$temp = "";
				}
?>
              </select> </td>
          </tr>
          <tr> 
            <td valign="top"><div align="right"><strong>&Aacute;reas 
                Tem&aacute;ticas</strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla 
                CTRL do seu teclado.</div></td>
            <td colspan="3" valign="top"> <select name="tema[]" size="6" multiple>
                <?php
				//$sql = "SELECT * FROM area_tematica ORDER BY at_descricao";
				$sql = "SELECT a.at_id,a.at_descricao,b.at_descricao as bdesc FROM area_tematica a LEFT JOIN area_tematica b ON a.at_pertence=b.at_id ORDER BY a.at_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->at_descricao);
					$desc2 = trim($row->bdesc);
					if (strlen(trim($desc2)) > 0) {
						$temp = "($desc2)";
					}
					echo "<option value=\"$row->at_id\">$desc $temp</option>";
					$temp = "";
				}
?>
              </select> </td>
          </tr>
          <tr> 
            <td valign="top"><div align="right"><strong>Tipos 
                de Vegeta&ccedil;&atilde;o</strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla 
                CTRL do seu teclado.</div></td>
            <td colspan="3" valign="top"> <select name="vege[]" size="6" multiple>
                <?php
				$sql = "SELECT * FROM vegetacao ORDER BY vg_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->vg_descricao);
					echo "<option value=\"$row->vg_id\">$desc</option>";
					$temp = "";
				}
?>
              </select> </td>
          </tr>
          <tr> 
            <td valign="top"><div align="right"><strong>Localidades</strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla 
                CTRL do seu teclado.</div></td>
            <td colspan="3" valign="top"> <select name="local[]" size="6" multiple>
                <?php
				$sql = "SELECT * FROM localidade ORDER BY lc_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->lc_descricao);
					echo "<option value=\"$row->lc_id\">$desc</option>";
					$temp = "";
				}
?>
              </select> </td>
          </tr>
          <tr> 
            <td valign="top"><div align="right"><strong>Zonas 
                Clim&aacute;ticas </strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla 
                CTRL do seu teclado.</div></td>
            <td colspan="3" valign="top"> <select name="zona[]" size="6" multiple>
                <?php
				$sql = "SELECT * FROM zona_climatica ORDER BY zc_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->zc_descricao);
					echo "<option value=\"$row->zc_id\">$desc</option>";
					$temp = "";
				}
?>
              </select> </td>
          </tr>
          <tr> 
            <td valign="top"><div align="right"><strong>Entidades 
                Biol&oacute;gicas</strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla 
                CTRL do seu teclado.</div></td>
            <td colspan="3" valign="top"> <select name="bio[]" size="6" multiple>
                <?php
				$sql = "SELECT distinct(eb_id),ebn_nome,ebf_familia,ebg_genero,ebe_especie FROM entidade_biologica ".
					   "LEFT JOIN eb_nome_vulgar ON eb_id=ebn_id_entidade ".
					   "LEFT JOIN eb_familia ON eb_familia=ebf_id ".
					   "LEFT JOIN eb_genero ON eb_genero=ebg_id ".
					   "LEFT JOIN eb_especie ON  eb_especie=ebe_id ".
					   "GROUP BY eb_id ".
					   "ORDER BY ebf_familia,ebg_genero,ebe_especie,ebn_nome";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$nomeCientifico = trim(trim($row->ebf_familia) . " " . trim($row->ebg_genero) . " " . trim($row->ebe_especie));
					$nomeVulgar = trim($row->ebn_nome);
					if (strlen($nomeCientifico) > 0) {
						echo "<option value=\"$row->eb_id\">$nomeCientifico</option>";
					}
					else if (strlen($nomeVulgar) > 0) {
						echo "<option value=\"$row->eb_id\">$nomeVulgar</option>";
					}
					else {
						echo "<option value=\"$row->eb_id\">$row->eb_id</option>";
					}
				}
?>
              </select> </td>
          </tr>
          <tr> 
            <td valign="top"><div align="right"><strong>Ref. 
                Bibliogr&aacute;ficas</strong>:<br>
                <br>
                Selecione todos os que forem necess&aacute;rios, segurando a tecla 
                CTRL do seu teclado.</div></td>
            <td colspan="3" valign="top"> <select name="bib[]" size="6" multiple>
                <?php
				$sql = "SELECT * FROM bibliografia ORDER BY bb_titulo_todo";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->bb_titulo_todo);
					echo "<option value=\"$row->bb_id\">$desc</option>";
				}
?>
              </select> </td>
          </tr>
          <tr> 
            <td colspan="4"><div align="center"> 
                <input type="button" class="botaoLogin" value="   INSERIR   " onClick="criticaFormExp();">
              </div></td>
          </tr>
          <tr bgcolor="#000000"> 
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
	  </form>
	  
	  <form name="formAD" method="post" action="exp_cadastro.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
          <td height="24" colspan="4"> 
            <p> <strong>Alterar Dados - Experi&ecirc;ncia 
                - usu&aacute;rio: <?php echo $cookieUsuarioPub; ?> </strong></p></td>
          </tr>
          <tr> 
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td colspan="4">
			 Experi&ecirc;ncia: 
              <select name="experiencia">
<?php
				$sql = "SELECT * FROM experiencia WHERE ex_usuario='$cookieUsuarioPub' ORDER BY ex_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->ex_descricao);
					$varSelected = "";
					if (isset($experiencia) && $experiencia == $row->ex_id) {
						$varSelected = "selected";
					}
					echo "<option value=\"$row->ex_id\" $varSelected>$desc</option>";
				}
?>
              </select> &nbsp;&nbsp;&nbsp; <input type="submit" class="botaoLogin" value="   BUSCAR   "> 
            </td>
          </tr>
          <tr> 
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		</table>
	  </form>
<?php
				if (isset($experiencia)) {
				
					$sql = "SELECT * FROM experiencia WHERE ex_id=$experiencia";
					$query = $db->query($sql);
					$rowExp = $query->fetch_object();
?>
	<form name="formAtualiza" method="post" action="xt_exp_atualiza_experiencia.php">
	<input type="hidden" name="id" value="<?php echo $rowExp->ex_id; ?>">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr> 
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr> 
            <td width="196"> <div align="right"><strong>T&iacute;tulo</strong>:</div></td>
            <td colspan="3"> <input name="nome" type="text" size="69" maxlength="255" value="<?php echo trim($rowExp->ex_descricao); ?>"></td>
          </tr>
          <tr> 
            <td> <div align="right"><strong>Tipo</strong>:</div></td>
            <td colspan="3"> <select name="tipo" id="select">
                <?php
					$sql = "SELECT * FROM tipo_experiencias ORDER BY tx_descricao";
					$query = $db->query($sql);
					while ($row = $query->fetch_object()) {
						$varSelected = "";
						if ($rowExp->ex_tipo == $row->tx_id) {
							$varSelected = "selected";
						}
?>
                <option <?php echo $varSelected; ?> value="<?php echo $row->tx_id; ?>"><?php echo trim($row->tx_descricao); ?></option>
                <?php
					} // la�o do loop
?>
              </select></td>
          </tr>
          <tr> 
            <td> <div align="right"><strong>Status</strong>:</div></td>
            <td width="131"> <select name="status">
                <?php
				$sql = "SELECT * FROM status_experiencia";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
						$varSelected = "";
						if ($rowExp->ex_status == $row->se_id) {
							$varSelected = "selected";
						}
?>
                <option <?php echo $varSelected; ?> value="<?php echo $row->se_id; ?>"><?php echo trim($row->se_status); ?></option>
                <?php
				} // la�o do loop
?>
              </select></td>
            <td width="104"> <div align="right"><strong>Acomp. 
                T&eacute;cnico</strong>:</div></td>
            <td width="91"> 
              <?php
					switch ($rowExp->ex_acompanhamento) {
						case "1":
							$desc = "Sim";
							break;
						case "0":
							$desc = "N�o";
							break;
					}
?>
              <select name="acompanha">
                <option value="<?php echo $rowExp->ex_acompanhamento; ?>"><?php echo $desc; ?></option>
                <option value="">--</option>
                <option value="1">Sim</option>
                <option value="0">N&atilde;o</option>
              </select></td>
          </tr>
          <tr> 
            <td valign="top"> <div align="right"><strong>Palavras-chave</strong>:<br>
                <br>
                Digite no campo ao lado palavras-chaves relacionadas com esta 
                experi&ecirc;ncia. Isto ir&aacute; otimizar as buscas no futuro. 
                Utilize ponto-e-v&iacute;rgula (;) para separar as palavras. </div></td>
            <td colspan="3"> <textarea name="chaves" cols="67" rows="10" id="chaves">
<?php
					$sql = "SELECT * FROM experiencia_chave,palavra_chave WHERE ec_id_experiencia=$experiencia ".
					       "AND ec_id_palavra=pc_id ORDER BY ec_id_palavra";
					$query2 = $db->query($sql);
					while ($row2 = $query2->fetch_object()) {
						echo trim($row2->pc_palavra) . "; ";
					}
?>
			</textarea> </td>
          </tr>
          <tr> 
            <td valign="top"> <div align="right"><strong>Resumo</strong>:<br>
                <br>
                Tags HTML s&atilde;o aceitos. &lt;i&gt;&lt;/i&gt; para it&aacute;lico, 
                &lt;b&gt;&lt;/b&gt; para negrito. As quebras de linha s&atilde;o 
                automaticamente convertidas em quebras na impress&atilde;o do 
                relat&oacute;rio. </div></td>
            <td colspan="3"> <textarea name="resumo" cols="67" rows="10" id="textarea"><?php echo trim($rowExp->ex_resumo); ?></textarea></td>
          </tr>
          <tr> 
            <td colspan="4" valign="top"> 
              <div align="center">[ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=AG&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">&Aacute;reas 
                Geogr&aacute;ficas</a> ] &nbsp;&nbsp;[ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=AT&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">&Aacute;reas 
                Tem&aacute;ticas</a> ]&nbsp;&nbsp; [ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=HA&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Habitats</a> 
                ]&nbsp;&nbsp;&nbsp;[ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_autores.php?id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Autores</a> 
                ]&nbsp;&nbsp;&nbsp;[ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relatores.php?id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Relatores</a> 
                ]<br>
                <br>
                [ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=LC&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Localidades</a> 
                ] &nbsp;&nbsp;[ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=ZC&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Zonas 
                Clim&aacute;ticas </a> ] &nbsp;&nbsp;[ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=VG&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Tipos 
                de Vegeta&ccedil;&atilde;o</a> ] <br>
                <br>
                [ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=EB&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Entidades 
                Biol&oacute;gicas </a> ]&nbsp;&nbsp; [ <a href="#" class="Preto" onClick="MM_openBrWindow('exp_popup_relacionamentos.php?tipo=RB&id=<?php echo $rowExp->ex_id; ?>&nome=<?php echo trim($rowExp->ex_descricao); ?>','expPopup','scrollbars=yes,width=700,height=300')">Refer&ecirc;ncias 
                Bibliogr&aacute;ficas </a> ]</div></td>
          </tr>
          <tr> 
            <td height="1" colspan="4" bgcolor="#000000"> 
              <div align="center"></div></td>
          </tr>
          <tr> 
            <td colspan="4"><div align="center"> 
                <input Type="button" class="botaoLogin" onClick="criticaFormAtualiza();" value="   ATUALIZAR   ">
              </div></td>
          </tr>
          <tr> 
            <td height="1" colspan="4" bgcolor="#000000"> 
              <div align="center"></div></td>
          </tr>
          <?php
				} // fim do if que verifica se foi feita a busca
?>
        </table>
	  </form>

<?php
}
else {
?>
	<p align="center"><b>Você precisa estar autenticado para cadastrar uma nova experiência!</b></p>
<?php
	include("autentica.inc.php");
}
?>
	  </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
