<?php
include("conexao.inc.php");

$prefix = (isset($_REQUEST['ag']))
	? 'ag'
	: 'at';

$cookieName = 'cookieBusca'.strtoupper($prefix);

$dados = implode(';',$_REQUEST[$prefix]);

if (strlen(trim($dados)) > 0) {
	if (!isset($_COOKIE[$cookieName]) || strlen(trim($_COOKIE[$cookieName])) == 0) {
		setcookie($cookieName,$dados);
	} else {
		setcookie($cookieName,$_COOKIE[$cookieName].";".$dados);
	}
}

?>

<script language="JavaScript">
	window.location.href='<?php echo $url; ?>?initFiltro=';
	self.close();
</script>
