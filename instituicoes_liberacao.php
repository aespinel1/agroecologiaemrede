<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$varDesc = "Libera��o de Institui��es";

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='".$_COOKIE['cookieSession']."' AND fu_login='".$_COOKIE['cookieUsuario']."' AND (fu_id='020000' OR fu_id='020500') AND us_admin=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaFormInst() {
	desc = formNE.nome.value;
	mail = formNE.email.value;
	ag = formNE.agID.value;
	at = formNE.atID.value;

	if (desc.length == 0) {
		alert('Digite um nome válido!');
		formNE.nome.focus();
		return;
	}

	if (ag.length == 0) {
		alert('Selecione pelo menos uma �rea Geogr�fica do seu interesse!');
		formNE.areasGeograficas.focus();
		return;
	}

	if (at.length == 0) {
		alert('Selecione pelo menos uma �rea Tem�tica do seu interesse!');
		formNE.areasTematicas.focus();
		return;
	}

	formNE.submit();
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function insereEndereco() {
	formEnd.action = 'xt_inst_insere_endereco.php';
	formEnd.submit();
}

function insereTelefone() {
	formTel.action = 'xt_inst_insere_telefone.php';
	formTel.submit();
}

function insereArea() {
	formTema.action = 'xt_inst_insere_area.php';
	formTema.submit();
}

function insereGeo() {
	formGeo.action = 'xt_inst_insere_geo.php';
	formGeo.submit();
}

function excluiEndereco(desc,id) {
	inst = formEnd.inst.value;
	if(confirm('Você tem certeza que deseja excluir o endereço de ' + desc + ' para esta instituição?')) {
		window.location.href='xt_instituicao_exclui_endereco.php?inst='+inst+'&id='+id;
	}
}

function excluiTelefone(desc,id) {
	inst = formEnd.inst.value;
	if(confirm('Você tem certeza que deseja excluir o telefone de ' + desc + ' para esta instituição?')) {
		window.location.href='xt_instituicao_exclui_telefone.php?inst='+inst+'&id='+id;
	}
}

function excluiArea(desc,id) {
	inst = formTema.inst.value;
	if(confirm('Você tem certeza que deseja excluir a �rea tem�tica ' + desc + ' para esta instituição?')) {
		window.location.href='xt_inst_exclui_area.php?inst='+inst+'&tema='+id;
	}
}

function excluiGeo(desc,id) {
	inst = formGeo.inst.value;
	if(confirm('Você tem certeza que deseja excluir a �rea geogr�fica ' + desc + ' para esta instituição?')) {
		window.location.href='xt_inst_exclui_geo.php?inst='+inst+'&geo='+id;
	}
}

function submitFormLI(acao) {
	if (acao == 'libera') {
		formLI.acao.value = 'libera';
	}
	formLI.submit();
}
</script>

<?php include("menu_admin.php"); ?>


<table width="589" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="579" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_instituicoes.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Instituição Libera��o</strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>

	  <?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {

 $sql = "SELECT * FROM frm_instituicao WHERE in_liberacao='N' ";
 $query = $db->query($sql);
?>
	  <form name="formLI" method="post" action="xt_inst_libera_instituicao.php">
	  <input type="hidden" name="acao" value="">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td height="24" colspan="5"> <p> <strong>Liberar
                Institui��es</strong>
                <?php if (isset($instLibera)) { ?>
                - Libera��o realizada com sucesso!
                <?php } ?>
              </p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000" colspan="5"> <div align="center"></div></td>
          </tr>
          <tr bgcolor="#CCCCCC">
            <td><div align="center"></div></td>
            <td><b>Nome</b></td>
            <td><b>E-mail</b></td>
            <td><b>Website</b></td>
            <td><b>Data Entrada</b></td>
          </tr>
          <?php
					$i = 0;
					while ($row = $query->fetch_object()) {
?>
          <tr>
            <td> <div align="center">
                <input type="checkbox" name="inst<?php echo $i; ?>" value="<?php echo $row->in_id; ?>">
              </div></td>
            <td><b><?php echo $row->in_nome; ?></b></td>
            <td><b><?php echo $row->in_email; ?></b></td>
            <td><b><?php echo $row->in_url; ?></b></td>
            <td><b><?php echo $row->in_data_proc; ?></b></td>
          </tr>
          <?php
						$i++;
					}
?>
          <input type="hidden" name="qtItens" value="<?php echo $i; ?>">
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="5"> <div align="center">
                <input name="Button" Type="button" class="botaoLogin" value="   Liberar   " onClick="submitFormLI('libera');">
              </div></td>
          </tr>
        </table>
	  </form>
<?php
			} // fim do if que verifica se o usuário tem acesso a estas informações

?>

  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
