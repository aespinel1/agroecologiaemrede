<?php
if (strlen($cookieUsuarioPub) == 0) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");


switch ($tipo) {
	case 'AG':
		$varTabelaRel = "rel_geo_experiencia";
		$varCampoRel = "rge_id_geo";
		$varIDRel = "rge_id_experiencia";
		break;
	case 'AT':
		$varTabelaRel = "rel_area_experiencia";
		$varCampoRel = "rae_id_area";
		$varIDRel = "rae_id_experiencia";
		break;
	case 'IN':
		$varTabelaRel = "rel_inst_experiencia";
		$varCampoRel = "rie_id_inst";
		$varIDRel = "rie_id_experiencia";
		break;
	case 'HA':
		$varTabelaRel = "rel_habitat_experiencia";
		$varCampoRel = "rhe_id_habitat";
		$varIDRel = "rhe_id_experiencia";
		break;
	case 'EX':
		$varTabelaRel = "rel_usuario_experiencia";
		$varCampoRel = "rue_id_usuario";
		$varIDRel = "rue_id_experiencia";
		break;
	case 'EB':
		$varTabelaRel = "rel_entidade_experiencia";
		$varCampoRel = "ree_id_entidade";
		$varIDRel = "ree_id_experiencia";
		break;
	case 'RB':
		$varTabelaRel = "rel_bib_experiencia";
		$varCampoRel = "rbe_id_bibliografia";
		$varIDRel = "rbe_id_experiencia";
		break;
	case 'LC':
		$varTabelaRel = "rel_local_experiencia";
		$varCampoRel = "rle_id_local";
		$varIDRel = "rle_id_experiencia";
		break;
	case 'VG':
		$varTabelaRel = "rel_veg_experiencia";
		$varCampoRel = "rve_id_veg";
		$varIDRel = "rve_id_experiencia";
		break;
	case 'ZC':
		$varTabelaRel = "rel_zona_experiencia";
		$varCampoRel = "rze_id_zona";
		$varIDRel = "rze_id_experiencia";
		break;
} // fim do switch

if ($tipo == "EX") {
	$sql = "DELETE FROM $varTabelaRel WHERE $varCampoRel='$idRel' AND $varIDRel=$idExp";
}
else {
	$sql = "DELETE FROM $varTabelaRel WHERE $varCampoRel=$idRel AND $varIDRel=$idExp";
}
$query = $db->query($sql);
if (!$query) {
	die($db->error);
}

$db->close();
?>
<script language="JavaScript">
	window.location.href='exp_popup_relacionamentos.php?tipo=<?php echo $tipo; ?>&id=<?php echo $idExp; ?>&nome=<?php echo trim($nome); ?>&relacionamentoExcluido=1';
</script>