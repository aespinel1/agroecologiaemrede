<?php
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function enviaEmail(url) {
	var email = prompt("Digite um endereço de e-mail:","");
	if (email != null && email != "") {
		url = url + '&email=' + email;
		window.location.href=url;
	}
}
</script>
<?php include("menu_geral.php"); ?>
<br>
<br>
<table width="760" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr align="center">
    <td width="173" valign="top" align="center">
	  <?php if (isset($busca)) { ?>
	      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="relatorio_simples.php?tipo=AT1&acao=IM&busca=<?= isset($busca) ? $busca : ""; ?>&inst=<?php echo $inst; ?>"><img src="imagens/ico_imprimir.gif" width="59" height="12" border="0"></a>
		     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onClick="enviaEmail('relatorio_simples.php?tipo=AT1&acao=EM&busca=<?= isset($busca) ? $busca : ""; ?>&inst=<?php echo $inst; ?>');"><img src="imagens/ico_envia_email.gif" width="99" height="13" border="0"></a></p>
	  <?php } else if (isset($area)) { ?>
	      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="relatorio_simples.php?tipo=AT2&acao=IM&areaDesc=<?php echo $areaDesc; ?>&area=<?php echo $area; ?>&inst=<?php echo $inst; ?>"><img src="imagens/ico_imprimir.gif" width="59" height="12" border="0"></a>
        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onClick="enviaEmail('relatorio_simples.php?tipo=AT2&acao=EM&areaDesc=<?php echo $areaDesc; ?>&area=<?php echo $area; ?>&inst=<?php echo $inst; ?>');"><img src="imagens/ico_envia_email.gif" width="99" height="13" border="0"></a></p>
	   <?php } ?>
    </td>

<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">

    <td width="567" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_simples.gif" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>&Aacute;reas
              Tem&aacute;ticas</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
	   <form name="form1" method="post" action="areas_tematicas.php">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td colspan="2"><p><strong>Busca simples</strong>
                - digite o nome da &aacute;rea no campo abaixo</p></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="88"> <div align="right">&Aacute;rea
                Tem&aacute;tica: </div></td>
            <td width="456"><input name="busca" type="text" id="busca3" size="80" maxlength="255" value="<?= isset($busca) ? $busca : ""; ?>"></td>
          </tr>
          <tr>
            <td><div align="right">Institui&ccedil;&atilde;o:
              </div></td>
            <td><input name="inst" type="text" id="inst2" size="80" maxlength="255" value="<?php echo $inst; ?>"></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><input name="Submit" type="submit" class="botaoLogin" value="Buscar" class="btn btn-success"></td>
          </tr>
        </table>
      </form>
<?php
if (isset($busca)) {

	$busca = strtoupper($busca);
	$inst = strtoupper($inst);

	function untree($parent, $level) {
		$parentsql = $db->query("SELECT * FROM area_tematica WHERE at_pertence=$parent ORDER BY at_descricao");
	    if (!$parentsql->num_rows) {
		    return;
	    }
	    else {
	        while($branch = $parentsql->fetch_object()) {
	            $echo_this = "";
                for ($x=1; $x<=$level; $x++) {
	                $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                }
	            $echo_this.="<a href=\"areas_tematicas.php?areaDesc=$branch->at_descricao&area=$branch->at_id\" class=\"preto\">$branch->at_descricao</a><br>";
	            echo $echo_this;
	            $rename_level = $level;
    	        untree($branch->at_id, ++$rename_level);
            }
        }
    }
	$sql = "SELECT * FROM area_tematica";
	if (isset($inst) && strlen($inst) > 0) {
		$sql .= ",rel_area_inst,frm_instituicao WHERE rai_id_inst=in_id AND rai_id_area=at_id AND UCASE(in_nome) LIKE '$inst%' AND UCASE(at_descricao) LIKE '$busca%' AND at_pertence=0 ORDER BY at_descricao";
	}
	else {
		$sql .= " WHERE UCASE(at_descricao) LIKE '$busca%' AND at_pertence=0 ORDER BY at_descricao";
	}
	$compsql = $db->query($sql);
	if (isset($inst) && strlen($inst) > 0) {
		echo "<p class=\"textoPreto10px\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b> e instituição <b>\"$inst\"</b>:<br><br>";
	}
	else {
		echo "<p class=\"textoPreto10px\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b>:<br><br>";
	}
	$i = 1;
	while ($at = $compsql->fetch_object()) {
		echo "<br>$i. <a href=\"areas_tematicas.php?areaDesc=$at->at_descricao&area=$at->at_id\" class=\"preto\"><strong>$at->at_descricao</strong></a><br>";
	    untree($at->at_id, 1);
		$i++;
	}
	echo "</p>";

} // fim do if que verifica se uma busca foi feita

if (isset($area)) {

	$sql = "SELECT * FROM area_tematica,rel_area_experiencia,experiencia WHERE at_id=$area AND at_id=rae_id_area ".
		   "AND ex_id=rae_id_experiencia AND ex_liberacao='S'";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {

		echo "<p class=\"textoPreto10px\">Foram encontrados as seguintes <strong>experiências</strong> para a �rea <b>\"$areaDesc\"</b>:<br><br>";

		$i = 1;
		while ($row = $query->fetch_object()) {

			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";

			$i++;

		}

		echo "</p>";

	}
	else {

		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências nesta �rea!</strong></p>";

	} // fim do if que verifica se a busca retornou os resultados



	function untreeTec($parent, $level) {
		$parentsql = $db->query("SELECT * FROM tecnologia WHERE tc_pertence=$parent ORDER BY tc_descricao");
	    if (!$parentsql->num_rows) {
		    return;
	    }
	    else {
	        while($branch = $parentsql->fetch_object()) {
	            $echo_this = "";
                for ($x=1; $x<=$level; $x++) {
	                $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                }
	            $echo_this.="<a href=\"tecnologias.php?tecDesc=$branch->tc_descricao&tec=$branch->tc_id\" class=\"preto\">$branch->tc_descricao</a><br>";
	            echo $echo_this;
	            $rename_level = $level;
    	        untreeTec($branch->tc_id, ++$rename_level);
            }
        }
    }
	$compsql = $db->query("SELECT * FROM area_tematica,rel_area_tec,tecnologia WHERE at_id=$area AND at_id=rat_id_at AND tc_id=rat_id_tc");
	echo "<p class=\"textoPreto10px\">Foram encontrados as seguintes tecnologias para a �rea tem�tica <b>\"$areaDesc\"</b>:<br><br>";
	$i = 1;
	while ($tc = $compsql->fetch_object()) {
		echo "<br>$i. <a href=\"tecnologias.php?tecDesc=$tc->tc_descricao&area=$tc->tc_id\" class=\"preto\"><strong>$tc->tc_descricao</strong></a><br>";
	    untreeTec($tc->tc_id, 1);
		$i++;
	}
	echo "</p>";

} // fim do if que verifica se o usuário clicou em alguma �rea tem�tica
?>

	  </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
