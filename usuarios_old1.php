<?php
include("conexao.inc.php");

if (!isset($initFiltro)) {
	setcookie("cookieBuscaAT");
	setcookie("cookieBuscaAG");
	setcookie("cookieBuscaBM");
}

function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) {
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}


function mOvr(src,clrOver) {
  if (!src.contains(event.fromElement)) {
      src.style.cursor = 'hand';
      src.bgColor = clrOver;
  }
}
function mOut(src,clrIn) {
  if (!src.contains(event.toElement)) {
     src.style.cursor = 'default';
     src.bgColor = clrIn;
  }
}
function gotourl(url) {
  window.location.href=url;
}
</script>


<?php include("menu_geral.php"); ?>
<br>
<br>
<table width="589" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="579" valign="top">
		 <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><img src="imagens/ico_usuarios.gif" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Pessoas</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
	   <form name="form1" method="post" action="usuarios.php#result">
	  <?php if (isset($initFiltro)) { ?>
	  	<input type="hidden" name="initFiltro" value="">
	  <?php } ?>
        <table width="567" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td><p><strong>Busca</strong> - digite o nome
                de uma pessoa no campo abaixo.</p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td>
			<input name="busca" type="text" id="busca" size="70" maxlength="255" value="<?= isset($busca) ? $busca : ""; ?>">
            <?php if (!isset($initFiltro)) { ?>
				<input type="submit" value="Buscar" class="botaoLogin">
			<?php } ?>
			</td>
          </tr>
<?php
if (isset($initFiltro)) {
?>
        <tr>
          <td>
		  	<table width="555" border="0" cellspacing="1" cellpadding="3" align="center">
		  	  <tr>
			  	<td colspan="2">
					<p><b>Formulação de Busca</b></p>
				</td>
			  </tr>
			  <tr>
			  	<td colspan="2" height="1" bgcolor="#000000"><div></div></td>
			  </tr>
			  <tr>
                <td width="50%" class="textoPreto" valign="top" bgcolor="#ACCBD0">
<?php
	echo "<b>Áreas Temáticas</b><br><br>";
	if (isset($_COOKIE['cookieBuscaAT']) && strlen(trim($_COOKIE['cookieBuscaAT'])) > 0) {
		$arrayAT = explode(";",$_COOKIE["cookieBuscaAT"]);
		$arrayAT = my_array_unique($arrayAT);
		for ($x = 0; $x < count($arrayAT); $x++) {
			$temp = $arrayAT[$x];
			$sql = "SELECT * FROM area_tematica WHERE at_id=$temp";
			$query = $db->query($sql);
			$row = $query->fetch_object();
		echo "&nbsp;&nbsp; - $row->at_descricao<br>";
		}
	}
	else {
		echo "&nbsp;&nbsp; - Todos<br>";
	}
?>
				</td>
                <td width="50%" class="textoPreto" valign="top" bgcolor="#B9D69E">
<?php
	echo "<b>Áreas Geográficas</b><br><br>";
	if (isset($_COOKIE['cookieBuscaAG']) && strlen(trim($_COOKIE['cookieBuscaAG'])) > 0) {
		$arrayAG = explode(";",$_COOKIE['cookieBuscaAG']);
		$arrayAG = my_array_unique($arrayAG);
		for ($x = 0; $x < count($arrayAG); $x++) {
			$temp = $arrayAG[$x];
			$sql = "SELECT * FROM area_geografica WHERE ag_id='$temp'";
			$query = $db->query($sql);
			$row = $query->fetch_object();
		echo "&nbsp;&nbsp; - $row->ag_descricao<br>";
		}
	}
	else {
		echo "&nbsp;&nbsp; - Todos<br>";
	}
?>
				</td>
              </tr>
            </table>
			<br>
			<center><input name="Submit" type="button" class="botaoLogin" value="     Buscar     " onClick="javascript:form1.submit();"></center>
		  </td>
        </tr>
<?php
} // fim do if que verifica os filtros
?>
<tr>
	<td colspan="2">
<?php
if (isset($busca)) {
	$busca = strtoupper($busca);


	if (isset($_COOKIE['cookieBuscaAT']) && strlen(trim($_COOKIE['cookieBuscaAT'])) > 0) {

		function untree($parent, $level, &$filhos) {
			$parentsql = $db->query("SELECT * FROM area_tematica WHERE at_pertence=$parent ORDER BY at_descricao");
		    if (!$parentsql->num_rows) {
			    return;
		    }
    		else {
		        while($branch = $parentsql->fetch_object()) {
					$filhos .= "$branch->at_id;";
    		        $rename_level = $level;
	    	        untree($branch->at_id, ++$rename_level, $filhos);
       		    }
	        }
			return $filhos;
	    }

		$j = 0;
		$z = 0;

		$arrayTema = array();
		$arrayAT = explode(";",$_COOKIE["cookieBuscaAT"]);
		$arrayAT = my_array_unique($arrayAT);
		$filhosAT = "";
		for ($i = 0; $i < count($arrayAT); $i++){
			$temaID = $arrayAT[$i];
			$sql = "SELECT * FROM rel_area_usuario WHERE rau_id_area=$temaID";
			$query = $db->query($sql);
			$num = $query->num_rows;

			$son = "";
			$filhosAT = untree($temaID,1,$son);

			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayTema[$z][$j] = $row->rau_id_usuario;
					$j++;
				}
				if (strlen(trim($filhosAT)) > 0) {
					$arrayATF = explode(";",$filhosAT);
					$arrayATF = my_array_unique($arrayATF);
					for ($c = 0; $c < count($arrayATF) - 1; $c++){
						$temaID = $arrayATF[$c];
						$sql = "SELECT * FROM rel_area_usuario WHERE rau_id_area=$temaID";
						$query = $db->query($sql);
						$num = $query->num_rows;
						if ($num > 0) {
							while($row = $query->fetch_object()) {
								$arrayTema[$z][$j] = $row->rau_id_usuario;
								$j++;
							}
						}
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else if ($num == 0 && strlen(trim($filhosAT)) > 0) {
				$arrayATF = explode(";",$filhosAT);
				$arrayATF = my_array_unique($arrayATF);
				for ($c = 0; $c < count($arrayATF) - 1; $c++){
					$temaID = $arrayATF[$c];
					$sql = "SELECT * FROM rel_area_usuario WHERE rau_id_area=$temaID";
					$query = $db->query($sql);
					$num = $query->num_rows;
					if ($num > 0) {
						while($row = $query->fetch_object()) {
							$arrayTema[$z][$j] = $row->rau_id_usuario;
							$j++;
						}
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else {
				$arrayTema[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

		} // la�o do for

		if ($z > 0) { // os selects montaram pelo menos 1 array?
			if ($z == 1) {
				$intersecaoArraysAT = $arrayTema[0];
			}
			if ($z > 1) {
				$intersecaoArraysAT = array_intersect($arrayTema[0],$arrayTema[1]);
				if ($z > 2) {
					for ($x = 2; $x < $z; $x++) {
						$intersecaoArraysAT = array_intersect($intersecaoArraysAT,$arrayTema[$x]);
					} // la�o do for
				}
			}
			$intersecaoArraysAT = array_unique($intersecaoArraysAT);
			sort($intersecaoArraysAT,SORT_STRING);
		} // fim do if que verifica se a sele��o retornou mais de 0 arrays
	} // fim do if que pergunta se tem filtro de AT

	/******************************** AREA GEOGRFICA ****************************************/

	if (isset($_COOKIE['cookieBuscaAG']) && strlen(trim($_COOKIE['cookieBuscaAG'])) > 0) {

		function untree2($parent,$level,&$filhos) {
			$parentsql = $db->query("SELECT * FROM area_geografica WHERE ag_pertence=$parent ORDER BY ag_descricao");
		    if (!$parentsql->num_rows) {
			    return;
		    }
    		else {
		        while($branch = $parentsql->fetch_object()) {
					$filhos .= "$branch->ag_id;";
    		        $rename_level = $level;
	    	        untree2($branch->ag_id, ++$rename_level, $filhos);
       		    }
	        }

			return $filhos;
	    }


		$j = 0;
		$z = 0;
		$arrayGeo = array();
		$arrayAG = explode(";",$_COOKIE['cookieBuscaAG']);
		$arrayAG = my_array_unique($arrayAG);
		$filhosAG = "";
		for ($i = 0; $i < count($arrayAG); $i++){
			$geoID = $arrayAG[$i];
			$sql = "SELECT * FROM rel_geo_usuario WHERE rgu_id_geo=$geoID";
			$query = $db->query($sql);
			$num = $query->num_rows;

			$son = "";
			$filhosAG = untree2($geoID,1,$son);

			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayGeo[$z][$j] = $row->rgu_id_usuario;
					$j++;
				}
				if (strlen(trim($filhosAG)) > 0) {
					$arrayAGF = explode(";",$filhosAG);
					$arrayAGF = my_array_unique($arrayAGF);
					for ($c = 0; $c < count($arrayAGF) - 1; $c++){
						$geoID = $arrayAgF[$c];
						$sql = "SELECT * FROM rel_geo_usuario WHERE rgu_id_geo=$geoID";
						$query = $db->query($sql);
						$num = $query->num_rows;
						if ($num > 0) {
							while($row = $query->fetch_object()) {
								$arrayGeo[$z][$j] = $row->rgu_id_usuario;
								$j++;
							}
						}
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else if ($num == 0 && strlen(trim($filhosAG)) > 0) {
				$arrayAGF = explode(";",$filhosAG);
				$arrayAGF = my_array_unique($arrayAGF);
				for ($c = 0; $c < count($arrayAGF) - 1; $c++){
					$geoID = $arrayAGF[$c];
					$sql = "SELECT * FROM rel_geo_usuario WHERE rgu_id_geo=$geoID";
					$query = $db->query($sql);
					$num = $query->num_rows;
					if ($num > 0) {
						while($row = $query->fetch_object()) {
							$arrayGeo[$z][$j] = $row->rgu_id_usuario;
							$j++;
						}
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else {
				$arrayGeo[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

		} // la�o do for

		if ($z > 0) { // os selects montaram pelo menos 1 array?
			if ($z == 1) {
				$intersecaoArraysAG = $arrayGeo[0];
			}
			if ($z > 1) {
				$intersecaoArraysAG = array_merge($arrayGeo[0],$arrayGeo[1]);
				if ($z > 2) {
					for ($x = 2; $x < $z; $x++) {
						$intersecaoArraysAG = array_merge($intersecaoArraysAG,$arrayGeo[$x]);
					} // la�o do for
				}
			}
			$intersecaoArraysAG = array_unique($intersecaoArraysAG);
			sort($intersecaoArraysAG,SORT_STRING);
		} // fim do if que verifica se a sele��o retornou mais de 0 arrays
	} // fim do if que pergunta se tem filtro de AG

	$arrays = 0;
	if (isset($intersecaoArraysAT) && count($intersecaoArraysAT) >= 0) {
		$arraysCompletos[$arrays] = $intersecaoArraysAT;
		$arrays++;
		$arrayUnico = $intersecaoArraysAT;
	}
	if (isset($intersecaoArraysAG) && count($intersecaoArraysAG) >= 0) {
		$arraysCompletos[$arrays] = $intersecaoArraysAG;
		$arrays++;
		$arrayUnico = $intersecaoArraysAG;
	}

	$sql = "SELECT * FROM usuario WHERE UCASE(us_nome) LIKE '%$busca%' AND us_liberacao='S' ".
	       "AND ";

	if (isset($arraysCompletos) && count($arraysCompletos) == 1) {
		$arrayMontado = "";
		$arrayFinal = $arraysCompletos[0];
		while (list ($key, $val) = each ($arraysCompletos[0])) {
    		$arrayMontado .= "'$val',";
		}
		$arrayMontado = substr($arrayMontado,0,-1);
	}
	else if (count($arraysCompletos) > 1) {
		$arrayFinal = array_intersect($arraysCompletos[0],$arraysCompletos[1]);
		if (count($arraysCompletos) > 2) {
			for ($x = 2; $x < count($arraysCompletos); $x++) {
				$arrayFinal = array_intersect($arrayFinal,$arraysCompletos[$x]);
			}
		}
		$arrayMontado = "";
		while (list ($key, $val) = each ($arrayFinal)) {
    		$arrayMontado .= "'$val',";
		}
		$arrayMontado = substr($arrayMontado,0,-1);
	}

	$temp = substr(trim($sql),-3);
	if ($temp == "AND") {
		$sql = substr(trim($sql),0,-3);
	}
	if (isset($arrayMontado) && strlen(trim($arrayMontado)) > 0) {
		$sql .= " AND us_login in ($arrayMontado)";
	}
	else if (isset($arrayMontado) && strlen(trim($arrayMontado)) == 0) {
		$sql .= " AND us_login in ('0')";
	}

	$sql .= " ORDER BY us_nome";

	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {

		echo "<a name=\"result\">";
		echo "<p class=\"textoPreto10px\">";

		$i = 1;
		$varAutor = "";
		$varRelator = "";
		$perm = 0;
		while ($row = $query->fetch_object()) {
			if ($row->us_autor == '1') {
				$varAutor = 'A';
			}
			if ($row->us_relator == '1') {
				$varRelator = 'R';
			}
			if (strpos($row->us_permissoes,"T") !== false || strpos($row->us_permissoes,"E") !== false || strpos($row->us_permissoes,"A") !== false) {
				$perm++;
			}
			//echo "$i. <a href=\"usuarios.php?usuNome=$row->us_nome&usu=$row->us_login\" class=\"preto\">$row->us_nome</a> [ $varAutor $varRelator ] ";
			echo "$i. <a href=\"usuarios.php?usuNome=$row->us_nome&usu=$row->us_login\" class=\"preto\">$row->us_nome</a> ";
			/*
			if ($perm > 0) {
				echo "[ <a href=\"#\" class=\"Preto\" onClick=\"MM_openBrWindow('usuarios_popup_dados.php?tipo=usu&id=$row->us_login&nome=$row->us_nome&permissoes=$row->us_permissoes','usuPopup','scrollbars=yes,width=500,height=300')\">Endereços e Telefones</a> ]";
			}
			*/
			echo "<br><br>";
			$perm = 0;
			$i++;
		}
		echo "</p>";
	}
	else {
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";
	} // fim do if que verifica se a busca retornou os resultados

} // fim do if que verifica se uma busca foi feita

if (isset($usu)) {
?>
      <table width="567" border="0" cellspacing="0" cellpadding="3" align="center">
        <tr>
          <td  valign="top">
		  	<table width="100%" border="0" cellspacing="0" cellpadding="3" align="center">
              <tr>
                <td class="textoPreto"><strong>Dados Cadastrais</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
				<p>
<?php
	$sql = "SELECT * FROM usuario WHERE us_login='$usu'";
	$query = $db->query($sql);
	$rowUSR = $query->fetch_object();
	echo "Nome: <b>$rowUSR->us_nome</b>";
	if (strlen(trim($rowUSR->us_email)) > 0) {
		echo "<br>";
		echo "E-mail: <a href=\"mailto:$rowUSR->us_email\" class=\"Preto\">$rowUSR->us_email</a>";
		echo "<br><br>";
	}
	if (strlen(trim($rowUSR->us_email)) > 0) {
		echo "<br>";
		echo "Telefone: $rowUSR->us_telefone";
		echo "<br><br>";
	}

    $sql = "SELECT * FROM experiencia_autor WHERE  exa_id_usuario = '$rowUSR->us_login' ";
    $query2 = $db->query($sql);
    $numRec = $query2->num_rows;

    if ($numRec != 0) {
       echo "<br>";
       echo "- Autor de experiências";
	}

    $sql = "SELECT * FROM experiencia_relator  WHERE  exr_id_usuario = '$rowUSR->us_login' ";
    $query2 = $db->query($sql);
    $numRec = $query2->num_rows;
    if ($numRec != 0) {
        echo "<br>";
     	echo "- Relator de experiências";
    }
?>
				</p>
				</td>
              </tr>

			  <?php if ($rowUSR->txt_telefone != "") {?>
              <tr>
                <td class="textoPreto"><strong>Telefones</strong></td>
              </tr>
			  <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
			  <tr>
			  <td class="textoPreto"><?php echo $rowUSR->txt_telefone; ?></td>
			  </tr>
			  <?php }  ?>
              <?php if ($rowUSR->txt_endereco != "") {?>
              <tr>
                <td class="textoPreto"><strong>Endere&ccedil;os</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
			  <tr><td>
			  <?php
				echo "<table>";
				echo "</tr>\n";
				echo "<td bgcolor=\"#D8D8D8\" class=\"textoPreto10px\">\n";
				echo "<strong>Endereço</strong>:<br>$rowUSR->txt_endereco<br>\n";
				if ($rowUSR->txt_cep != "") {
				echo "<strong>Cidade</strong>:<br>$rowUSR->txt_cidade<br>\n";
				}
				if ($rowUSR->txt_cep != "") {
   			       echo "<strong>Estado</strong>:<br>$rowUSR->txt_estado<br>\n";
			    }

				if ($rowUSR->txt_cep != "") {
				   echo "<strong>CEP</strong>:<br>$rowUSR->txt_cep<br>\n";
				}

				if ($rowUSR->txt_pais != "") {
				   echo "<strong>País</strong>:<br>$rowUSR->txt_pais\n";
				}
				echo "</td>\n";
				echo "</tr>\n";
				echo "</table>";

			 }  ?>
          </td></tr>
          <td width="328" valign="top">
			<table width="274" border="0" cellspacing="0" cellpadding="3" align="center">
<?php
	$sql = "SELECT * FROM experiencia_autor,experiencia WHERE exa_id_experiencia=ex_id AND exa_id_usuario='$usu'";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {
	?>
              <tr>
                <td width="268"  class="textoPreto"><strong>Experi&ecirc;ncias
                  Relacionadas</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td class="textoPreto">
<?php

		$i = 1;
		while ($row = $query->fetch_object()) {

			echo "$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";

			$i++;

		}

		echo "</p>";

	}

	} // fim do if que verifica se a busca retornou os resultados
	$sql = "SELECT * FROM experiencia_relator,experiencia WHERE exr_id_experiencia=ex_id AND exr_id_usuario='$usu'";
	$query = $db->query($sql);
	$num = $query->num_rows;


	if ($num > 0) {

		$i = 1;
		while ($row = $query->fetch_object()) {

			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";

			$i++;

		}

		echo "</p>";

	}
	else {

		//echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Este usuário n�o � relator de nenhuma experiência!</strong></p>";

	} // fim do if que verifica se a busca retornou os resultados
?>
				</td>
              </tr>
<?php
	$sql = "SELECT * FROM rel_inst_usuario,frm_instituicao WHERE riu_id_inst=in_id AND riu_id_usuario='$usu'";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
	?>
              <tr>
                <td class="textoPreto"><strong>Institui&ccedil;&otilde;es</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
<?php
		while ($arrayAreas = $query->fetch_object()) {
			echo "<img src=\"imagens/seta02.gif\"> <a href=\"instituicoes.php?instDesc=$arrayAreas->in_nome&inst=$arrayAreas->in_id\" class=\"preto\">$arrayAreas->in_nome</a><br>";
		}
	} // fim do if que verifica se a busca retornou os resultados
?>
				</td>
              </tr>
	<?php
	$sql = "SELECT * FROM rel_area_usuario,area_tematica WHERE rau_id_area=at_id AND rau_id_usuario='$usu'";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) { ?>
              <tr>
                <td class="textoPreto"><strong>&Aacute;reas Tem&aacute;ticas</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
<?php
		while ($arrayAreas = $query->fetch_object()) {
			echo "<img src=\"imagens/seta02.gif\">  <a href=\"xt_adiciona_agat.php?url=banco_experiencias&tipo=at&id=$arrayAreas->at_id\" class=\"preto\">$arrayAreas->at_descricao</a><br>";
		}
	} // fim do if que verifica se a busca retornou os resultados
?>
				</td>
              </tr>
	<?php
	$sql = "SELECT * FROM rel_geo_usuario,area_geografica WHERE rgu_id_geo=ag_id AND rgu_id_usuario='$usu'";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
?>

              <tr>
                <td class="textoPreto"><strong>&Aacute;reas Geogr&aacute;ficas</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
<?php
		while ($arrayAreas = $query->fetch_object()) {
			echo "<img src=\"imagens/seta02.gif\">  <a href=\"xt_adiciona_agat.php?url=banco_experiencias&tipo=ag&id=$arrayAreas->ag_id\" class=\"preto\">$arrayAreas->ag_descricao</a><br>";
		}
	} // fim do if que verifica se a busca retornou os resultados
?>

				</td>
              </tr>
            </table>
          </td>
        </tr>
      </table>

	</td>
</tr>
          <tr >
            <td colspan="2" ><p align="center"><strong>Áreas Temáticas
                </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[
                <a href="usuarios.php" class="Preto"><strong>Nova Busca</strong></a>
                ]<strong> </strong></p>
			</td>
         </tr>
        <tr>
            <td height="1" colspan="2" bgcolor="#000000">
              <div align="center"></div></td>
        </tr>
        <tr>
          <td colspan="2">
		  	<table width="555" border="0" cellpadding="10" cellspacing="2" align="center">
<?php
$sql = "SELECT * FROM area_tematica WHERE at_pertence=0 ORDER BY at_descricao";
$query = $db->query($sql);
$i = 0;
while ($row = $query->fetch_object()) {
	if ($i == 0) {
    	echo "<tr bgcolor=\"#ACCBD0\" class=\"textoPreto10px\">";
	}
    echo "<td onmouseover=\"mOvr(this,'#86AEB0');\" onmouseout=\"mOut(this,'#ACCBD0');\" onClick=\"abrePopup('exp_popup_busca_temas.php?id=$row->at_id&url=usuarios.php','tema','scrollbars=yes,width=517,height=300')\">
          <div align=\"center\"><strong>$row->at_descricao</strong></div>
		  </td>";
	if ($i == 3) {
		echo "</tr>";
	}

	$i++;

	if ($i == 3) {
		$i = 0;
	}

}
?>
            </table>
		  </td>
        </tr>
        <tr>
          <td colspan="2">
		  	<table width="555" border="0" cellpadding="10" cellspacing="2" align="center">
              <tr bgcolor="#B9D69E">
                <td bgcolor="#B9D69E" onClick="abrePopup('exp_popup_busca_ag.php?url=usuarios.php','ag','scrollbars=yes,width=517,height=300')" onmouseover="mOvr(this,'#89BA5C');" onmouseout="mOut(this,'#B9D69E');">
                  <div align="center"><strong>&Aacute;reas Geogr&aacute;ficas</strong></div></td>
              </tr>
            </table>
		  </td>
        </tr>
        </table>
      </form>
      <p>&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
