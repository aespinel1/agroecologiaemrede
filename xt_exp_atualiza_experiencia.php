<?php

$atID = $_POST["atID"]; 
$agID = $_POST["agID"];
$biomas = $_POST["biomas"]; 

if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");



function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) { 
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}

$sql = "UPDATE experiencia SET ex_descricao='$nome',ex_resumo='$resumo',ex_chamada='$chamada', txt_comentario='$comentario', ano_publicacao='$ano_publicacao', lat=str_replace(',','.',$latitude), lng=str_replace(',','.',$longitude) WHERE ex_id=$experiencia";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "DELETE FROM experiencia_chave WHERE ec_id_experiencia=$experiencia";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}
if (strlen($chaves) > 0) {
	$array = explode(";",$chaves);
	for ($i = 0; $i < count($array); $i++) {
		$palavraChave = trim($array[$i]);
		$tam = strlen($palavraChave);
		if ($tam > 0) {
			$sql = "SELECT pc_id FROM palavra_chave WHERE pc_palavra='$palavraChave'";
			$query = $db->query($sql);
			$row = $query->fetch_object();
			$num = $query->num_rows;
			if ($num > 0) {
				$palavraChaveID = $row->pc_id;
			}
			else {
				$sql = "INSERT INTO palavra_chave (pc_palavra) VALUES ('$palavraChave')";
				$query = $db->query($sql);
			
				$sql = "SELECT pc_id FROM palavra_chave WHERE pc_palavra='$palavraChave'";
				$query = $db->query($sql);
				$row = $query->fetch_object();
				$palavraChaveID = $row->pc_id;
			}
			
			$sql = "INSERT INTO experiencia_chave VALUES ($experiencia,$palavraChaveID)";
			$query = $db->query($sql);
			if (!$query) {
		   		die($db->error);
			}
		}
	}
}

$sql = "DELETE FROM experiencia_autor WHERE exa_id_experiencia=$experiencia";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}


if (isset($autor)) { 
	$i=0; 
	while($i<count($autor)){ 
		$autorID = $autor[$i];
		$sql = "INSERT INTO experiencia_autor VALUES ($experiencia,'$autorID',0)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

if (isset($instAutor)) { 
	$i=0; 
	while($i<count($instAutor)){ 
		$instID = $instAutor[$i];
		$sql = "INSERT INTO experiencia_autor VALUES ($experiencia,'NULL',$instID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}


$sql = "DELETE FROM experiencia_relator WHERE exr_id_experiencia=$experiencia";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

if (isset($relator)) { 
	$i=0; 
	while($i<count($relator)){ 
		$relatorID = $relator[$i];
		$sql = "INSERT INTO experiencia_relator VALUES ($experiencia,'$relatorID',0)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

if (isset($instRelator)) { 
	$i=0; 
	while($i<count($instRelator)){ 
		$instID = $instRelator[$i];
		$sql = "INSERT INTO experiencia_relator VALUES ($experiencia,'NULL',$instID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

$sql = "DELETE FROM rel_geo_experiencia WHERE rge_id_experiencia=$experiencia";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}

$agID = $_POST["agID"];
$atID = $_POST["atID"];


if (strlen(trim($agID)) > 0) {
	$arrayAG = explode(";",$agID);
	$arrayAG = my_array_unique($arrayAG);
	for ($x = 0; $x < count($arrayAG); $x++) {
		$temp = $arrayAG[$x];
		if (strlen(trim($temp)) > 0) {
			$sql = "INSERT INTO rel_geo_experiencia VALUES ($temp,$experiencia)";	
			$query = $db->query($sql);
		}
	}
}


$sql = "DELETE FROM rel_area_experiencia WHERE rae_id_experiencia=$experiencia";
$query = $db->query($sql);
if (!$query) {
   	die($db->error);
}


if (strlen(trim($atID)) > 0) {
	$arrayAT = explode(";",$atID);
	$arrayAT = my_array_unique($arrayAT);
	for ($x = 0; $x < count($arrayAT); $x++) {
		$temp = $arrayAT[$x];
		if (strlen(trim($temp)) > 0) {
			$sql = "INSERT INTO rel_area_experiencia VALUES ($temp,$experiencia)";	
			$query = $db->query($sql);
		}
	}
}

$sql = "DELETE FROM rel_habitat_experiencia WHERE rhe_id_experiencia=$experiencia";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

if (strlen(trim($biomas)) > 0) {
	$sql = "INSERT INTO rel_habitat_experiencia VALUES ($biomas,$experiencia)";	
	$query = $db->query($sql);
}


$db->close();
?>
<script language="JavaScript">
	window.location.href='experiencias_altera.php?expAtualizada=1&experiencia=<?php echo $experiencia; ?>';
</script>
