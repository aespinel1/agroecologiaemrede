<?php
include("conexao.inc.php");

if (!isset($initFiltro)) {
	setcookie("cookieBuscaAT");
	setcookie("cookieBuscaAG");
	setcookie("cookieBuscaBM");
}

function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) {
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}

$ATs = array();
$sql = "SELECT * FROM area_tematica WHERE at_pertence=0 ORDER BY at_descricao";
$query = $db->query($sql);
while ($row = $query->fetch_object()) {
	$ATs[] = $row;
}

$filtroATs = array();
$filtroATsRaw = array();
if (isset($_COOKIE['cookieBuscaAT']) && $_COOKIE['cookieBuscaAT']) {
	$arrayAT = explode(";",$_COOKIE["cookieBuscaAT"]);
	$arrayAT = my_array_unique($arrayAT);
	$sql = "SELECT * FROM area_tematica WHERE at_id IN (".implode(',',$arrayAT).") ORDER BY at_descricao";
	$query = $db->query($sql);
	while ($r = $query->fetch_object()) {
		$filtroATs[] = traduz($r->at_descricao,$lingua);
		$filtroATsRaw[] = $r;
	}
}

$filtroAGs = array();
$filtroAGsRaw = array();
if (isset($_COOKIE['cookieBuscaAG']) && $_COOKIE['cookieBuscaAG']) {
	$arrayAG = explode(";",$_COOKIE["cookieBuscaAG"]);
	$arrayAG = my_array_unique($arrayAG);
	$sql = "SELECT * FROM area_geografica WHERE ag_id IN ('".implode("','",$arrayAG)."') ORDER BY ag_descricao";
	$query = $db->query($sql);
	while ($r = $query->fetch_object()) {
		$filtroAGs[] = $r->ag_descricao;
		$filtroAGsRaw[] = $r;
	}
}

?>

<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="JavaScript">
	function abrePopup(theURL,winName,features) { //v2.0
	  window.open(theURL,winName,features);
	}
</script>

<?php include("menu_geral.php");?>

<div class="container">
	<div class="modal" id="modalFiltros" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
				<form id="modal-form" name="modal-form" action="">
		      <div class="modal-header">
		        <h5 class="modal-title"></h5>
						<input class="btn btn-sm btn-primary" value="Selecionar" type="submit">
		        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		      </div>
		      <div class="modal-footer">
		        <input type="submit" class="btn btn-primary" value="Selecionar">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
		      </div>
				</form>
	    </div>
	  </div>
	</div>
	<div class="row">
    <div class="col-md-12">
			<div class="alert alert-success">
      	<h1><i class="aericon-pessoas"></i> Pesquisa de pessoas</h1>
			</div>
    </div>
		<?php if (isset($busca)) { ?>
			<?php if (!$busca && count($filtroATs)==0 && count($filtroAGs)==0) { ?>
			<div class="col-md-12">
				<div class="alert alert-danger">
				Erro. Você não digitou um nome nem escolheu uma ou mais áreas temáticas ou geográficas. Tente novamente.
				</div>
			</div>
			<?php } else { ?>
				<?php $query = faz_busca(); ?>
				<div class="col-md-12">
					<div class="alert alert-light text-muted">
						<div class="row">
							<div class="col-md-12">
								<h5>Seleção atual</h5>
							</div>
							<ul class="list-inline col-md-12">
								<li class="list-inline-item">
									<strong>Busca textual:</strong> <small><?=($busca) ? '"'.$busca.'"' : 'nenhuma'?></small>
								</li>
								<li class="list-inline-item">
									<strong>Áreas Temáticas:</strong> <small><?=($filtroATs) ? implode(', ',$filtroATs) : 'Todas'?></small>
								</li>
								<li class="list-inline-item">
									<strong>Áreas Geográficas:</strong> <small><?=($filtroAGs) ? implode(', ',$filtroAGs) : 'Todas'?></small>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<p><span class=""><?= $query->num_rows ?> pessoas encontradas</span> <a class="btn btn-sm btn-primary" href="<?=$dir['base_URL']?>usuarios_busca.php"><i class="oi oi-magnifying-glass"></i> Fazer nova busca</a></p>
					<ul class="list-group">
						<?php while ($r = $query->fetch_object()) { ?>
							<a class="list-group-item list-group-item-active" href="<?=$dir['base_URL']?>usuarios.php?usu=<?=$r->us_login?>"><?=$r->us_nome?></a>
						<?php } ?>
					</ul>
					<p class="mt-4"><a class="btn btn-sm btn-primary" href="<?=$dir['base_URL']?>usuarios_busca.php"><i class="oi oi-magnifying-glass"></i> Fazer nova busca</a></p>
				</div>
			<?php } ?>
		<?php } else { ?>
			<div class="col-md-12">
				<div class="alert alert-light text-muted">
					<div class="row">
					<?php if (count($filtroATs)>0 || count($filtroAGs)>0) { ?>
						<div class="col-md-12">
							<h5>Seleção atual</h5>
						</div>
						<div class="col-md-6">
							<strong>Áreas Temáticas:</strong>
							<ul class="list-group list-group-flush">
								<?php if (count($filtroATs)>0) { ?>
									<?php foreach ($filtroATs as $f) { ?>
										<li class="list-group-item bg-transparent"><?=$f?></li>
									<?php } ?>
								<?php } else { ?>
										<li class="list-group-item bg-transparent">
											Todas
										</li>
								<?php } ?>
							</ul>
						</div>
						<div class="col-md-6">
							<strong>Áreas Geográficas:</strong>
							<ul class="list-group list-group-flush">
								<?php if (count($filtroAGs)>0) { ?>
									<?php foreach ($filtroAGs as $f) { ?>
										<li class="list-group-item bg-transparent"><?=$f?></li>
									<?php } ?>
								<?php } else { ?>
										<li class="list-group-item bg-transparent">
											Todas
										</li>
								<?php } ?>
							</ul>
						</div>
					<?php } else { ?>
						<div class="col-md-12">
							<h5>Seleção atual</h5>
							<small>Nenhuma área temática ou geográfica selecionada</small>
						</div>
					<?php } ?>
					</div>
				</div>
			</div>
			<form class="col-md-12" method="POST" action="<?=$dir['base_URL']?>usuarios_busca.php">
				<div class="row">
					<div class="form-group col-md-12">
						<div class="input-group inline">
							<div class="input-group-prepend">
			          <div class="input-group-text"><i class="oi oi-magnifying-glass"></i></div>
			        </div>
			    		<input type="text" name="busca" class="form-control" placeholder="Busca por nome...">
						</div>
						<input type="hidden" name="initFiltro" value="">
			  	</div>
					<div class="form-group col-md-12">
						<input type="submit" class="btn btn-primary" value="Buscar">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h5>Escolha uma ou mais áreas temáticas:</h5>
					</div>
					<?php foreach ($ATs as $r) { ?>
						<div class="col-sm-6 col-md-4 col-lg-3">
							<a href="#" onClick="abreDialog('ajax_seleciona_tema.php?id=<?=$r->at_id?>&url=usuarios_busca.php&tipo=AT', 'modalFiltros', 'Selecione uma ou mais áreas temáticas', 'xt_exp_adiciona_filtros_pesq.php?tipo=AT')" class="btn btn-secondary text-center w-100 py-4 mb-2 text-wrap">
								<?=traduz($r->at_descricao,$lingua)?>
							</a>
						</div>
					<?php } ?>
				</div>
				<div class="row">
					<div class="col-md-12 mt-4">
						<h5>Escolha uma ou mais áreas geográficas:</h5>
					</div>
					<div class="col-md-12">
						<a href="#" onClick="abreDialog('ajax_seleciona_tema.php?url=usuarios_busca.php&tipo=AG&maxLevel=2', 'modalFiltros', 'Selecione uma ou mais áreas geográficas', 'xt_exp_adiciona_filtros_pesq.php?tipo=AG')" class="btn btn-info text-white text-center w-100 py-4 text-wrap">
								Áreas Geográficas
						</a>
					</div>
					</div>
				</div>
			</form>
		<?php } ?>
  </div>
</div>

</body>
</html>

<?php

function faz_busca() {
	global $db, $busca;

	$busca = strtoupper($busca);
	if (isset($_COOKIE['cookieBuscaAT']) && strlen(trim($_COOKIE['cookieBuscaAT'])) > 0) {
		$j = 0;
		$z = 0;
		$arrayTema = array();
		$arrayAT = explode(";",$_COOKIE["cookieBuscaAT"]);
		$arrayAT = my_array_unique($arrayAT);
		$filhosAT = "";
		for ($i = 0; $i < count($arrayAT); $i++){
			$temaID = $arrayAT[$i];
			$sql = "SELECT * FROM rel_area_usuario WHERE rau_id_area=$temaID";
			$query = $db->query($sql);
			$num = $query->num_rows;
			$son = "";
			$filhosAT = untree($temaID,1,$son);
			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayTema[$z][$j] = $row->rau_id_usuario;
					$j++;
				}
				if (strlen(trim($filhosAT)) > 0) {
					$arrayATF = explode(";",$filhosAT);
					$arrayATF = my_array_unique($arrayATF);
					for ($c = 0; $c < count($arrayATF) - 1; $c++){
						$temaID = $arrayATF[$c];
						$sql = "SELECT * FROM rel_area_usuario WHERE rau_id_area=$temaID";
						$query = $db->query($sql);
						$num = $query->num_rows;
						if ($num > 0) {
							while($row = $query->fetch_object()) {
								$arrayTema[$z][$j] = $row->rau_id_usuario;
								$j++;
							}
						}
					}
				}
				$z++;
				// fim do if que verifica se há registros
			} else if (strlen(trim($filhosAT)) > 0) {
				$arrayATF = explode(";",$filhosAT);
				$arrayATF = my_array_unique($arrayATF);
				for ($c = 0; $c < count($arrayATF) - 1; $c++){
					$temaID = $arrayATF[$c];
					$sql = "SELECT * FROM rel_area_usuario WHERE rau_id_area=$temaID";
					$query = $db->query($sql);
					$num = $query->num_rows;
					if ($num > 0) {
						while($row = $query->fetch_object()) {
							$arrayTema[$z][$j] = $row->rau_id_usuario;
							$j++;
						}
					}
				}
				$z++;
				// fim do if que verifica se há registros
			} else {
				$arrayTema[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}
		} // fim do loop em arrayAT

		if ($z > 0) { // os selects montaram pelo menos 1 array?
			if ($z == 1) {
				$intersecaoArraysAT = $arrayTema[0];
			} else {
				$intersecaoArraysAT = array_intersect($arrayTema[0],$arrayTema[1]);
				if ($z > 2) {
					for ($x=2; $x<$z; $x++) {
						$intersecaoArraysAT = array_intersect($intersecaoArraysAT,$arrayTema[$x]);
					}
				}
			}
			$intersecaoArraysAT = array_unique($intersecaoArraysAT);
			sort($intersecaoArraysAT,SORT_STRING);
		}
	} // fim do if que pergunta se tem filtro de AT

	if (isset($_COOKIE['cookieBuscaAG']) && strlen(trim($_COOKIE['cookieBuscaAG'])) > 0) {
		$j = 0;
		$z = 0;
		$arrayGeo = array();
		$arrayAG = explode(";",$_COOKIE['cookieBuscaAG']);
		$arrayAG = my_array_unique($arrayAG);
		$filhosAG = "";
		for ($i = 0; $i < count($arrayAG); $i++){
			$geoID = $arrayAG[$i];
			$sql = "SELECT * FROM rel_geo_usuario WHERE rgu_id_geo=$geoID";
			$query = $db->query($sql);
			$num = $query->num_rows;
			$son = "";
			$filhosAG = untree2($geoID,1,$son);
			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayGeo[$z][$j] = $row->rgu_id_usuario;
					$j++;
				}
				if (strlen(trim($filhosAG)) > 0) {
					$arrayAGF = explode(";",$filhosAG);
					$arrayAGF = my_array_unique($arrayAGF);
					for ($c = 0; $c < count($arrayAGF) - 1; $c++){
						$geoID = $arrayAGF[$c];
						$sql = "SELECT * FROM rel_geo_usuario WHERE rgu_id_geo=$geoID";
						$query = $db->query($sql);
						$num = $query->num_rows;
						if ($num > 0) {
							while($row = $query->fetch_object()) {
								$arrayGeo[$z][$j] = $row->rgu_id_usuario;
								$j++;
							}
						}
					}
				}
				$z++;
				// fim do if que verifica se há registros
			} else if ($num == 0 && strlen(trim($filhosAG)) > 0) {
				$arrayAGF = explode(";",$filhosAG);
				$arrayAGF = my_array_unique($arrayAGF);
				for ($c = 0; $c < count($arrayAGF) - 1; $c++){
					$geoID = $arrayAGF[$c];
					$sql = "SELECT * FROM rel_geo_usuario WHERE rgu_id_geo=$geoID";
					$query = $db->query($sql);
					$num = $query->num_rows;
					if ($num > 0) {
						while($row = $query->fetch_object()) {
							$arrayGeo[$z][$j] = $row->rgu_id_usuario;
							$j++;
						}
					}
				}
				$z++;
				// fim do if que verifica se há registros
			} else {
				$arrayGeo[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}
		} // Fim do loop em arrayAG

		if ($z > 0) { // os selects montaram pelo menos 1 array?
			if ($z == 1) {
				$intersecaoArraysAG = $arrayGeo[0];
			} else {
				$intersecaoArraysAG = array_merge($arrayGeo[0],$arrayGeo[1]);
				if ($z > 2) {
					for ($x = 2; $x < $z; $x++) {
						$intersecaoArraysAG = array_merge($intersecaoArraysAG,$arrayGeo[$x]);
					}
				}
			}
			$intersecaoArraysAG = array_unique($intersecaoArraysAG);
			sort($intersecaoArraysAG,SORT_STRING);
		}
	} // fim do if que pergunta se tem filtro de AG


	$arrays = 0;
	if (isset($intersecaoArraysAT) && count($intersecaoArraysAT) >= 0) {
		$arraysCompletos[$arrays] = $intersecaoArraysAT;
		$arrays++;
		$arrayUnico = $intersecaoArraysAT;
	}
	if (isset($intersecaoArraysAG) && count($intersecaoArraysAG) >= 0) {
		$arraysCompletos[$arrays] = $intersecaoArraysAG;
		$arrays++;
		$arrayUnico = $intersecaoArraysAG;
	}

	$sql = "
		SELECT *
		FROM usuario
		WHERE UCASE(us_nome) LIKE '%$busca%'
		AND us_liberacao='S'
		AND
	";

	if (isset($arraysCompletos) && count($arraysCompletos) == 1) {
		$arrayMontado = "";
		$arrayFinal = $arraysCompletos[0];
		while (list ($key, $val) = each ($arraysCompletos[0])) {
    		$arrayMontado .= "'$val',";
		}
		$arrayMontado = substr($arrayMontado,0,-1);
	} else if (isset($arraysCompletos) && count($arraysCompletos) > 1) {
		$arrayFinal = array_intersect($arraysCompletos[0],$arraysCompletos[1]);
		if (count($arraysCompletos) > 2) {
			for ($x = 2; $x < count($arraysCompletos); $x++) {
				$arrayFinal = array_intersect($arrayFinal,$arraysCompletos[$x]);
			}
		}
		$arrayMontado = "";
		while (list ($key, $val) = each ($arrayFinal)) {
    		$arrayMontado .= "'$val',";
		}
		$arrayMontado = substr($arrayMontado,0,-1);
	}


	if (substr(trim($sql),-3) == "AND") {
		$sql = substr(trim($sql),0,-3);
	}

	if (isset($arrayMontado) && strlen(trim($arrayMontado)) > 0) {
		$sql .= " AND us_login in ($arrayMontado)";
	} else if (isset($arrayMontado) && strlen(trim($arrayMontado)) == 0) {
		$sql .= " AND us_login in ('0')";
	}

	$sql .= " ORDER BY us_nome";

	$query = $db->query($sql);
	return $query;
}


function untree($parent, $level, &$filhos) {
	global $db;
	$parentsql = $db->query("SELECT * FROM area_tematica WHERE at_pertence=$parent ORDER BY at_descricao");
	if (!$parentsql->num_rows) {
		return;
	} else {
		while($branch = $parentsql->fetch_object()) {
			$filhos .= "$branch->at_id;";
			$rename_level = $level;
			untree($branch->at_id, ++$rename_level, $filhos);
		}
	}
	return $filhos;
}

function untree2($parent,$level,&$filhos) {
	global $db;
	$parentsql = $db->query("SELECT * FROM area_geografica WHERE ag_pertence=$parent ORDER BY ag_descricao");
	if (!$parentsql->num_rows) {
		return;
	} else {
		while($branch = $parentsql->fetch_object()) {
			$filhos .= "$branch->ag_id;";
			$rename_level = $level;
			untree2($branch->ag_id, ++$rename_level, $filhos);
		}
	}
	return $filhos;
}




$db->close();

?>
