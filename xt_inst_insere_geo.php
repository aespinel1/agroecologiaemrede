<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}

include("conexao.inc.php");


$sql = "SELECT * FROM rel_geo_inst WHERE rgi_id_inst=$inst AND rgi_id_geo=$geo";
$query = $db->query($sql);
$num = $query->num_rows;

if ($num == 0) {
	$sql = "INSERT INTO rel_geo_inst VALUES ($inst,$geo)";
	$query = $db->query($sql);
	if (!$query) {
 	   die($db->error);
	}
?>
	<script language="JavaScript">
		window.location.href='instituicoes_altera.php?inst=<?php echo $inst; ?>';
	</script>
<?php
}
else {
?>
	<script language="JavaScript">
		alert('Já existe um registro desta �rea geogr�fica para esta instituição!');
		window.location.href='instituicoes_altera.php?inst=<?php echo $inst; ?>&novaGeo=1';
	</script>
<?php
}
$db->close();
?>
