<?php

$atID = $_POST["atID"];
$agID = $_POST["agID"];
$biomas = $_POST["biomas"];

include("conexao.inc.php");


function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) { 
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}

$data = date("Y-m-d");
$hora = date("H:i:s");
$sql = "INSERT INTO pesquisa (pe_descricao,pe_resumo,pe_liberacao,pe_usuario,pe_data_proc,pe_hora_proc,pe_chamada,dt_criacao,txt_comentario, cod_usuario,ano_publicacao) ".
	   "VALUES ('$nome','$resumo','N','$cookieUsuario','$data','$hora','$nome','$current_date','$comentario','$cookieUsuario','$ano_publicacao')";
$query = $db->query($sql);
if (!$query) {
    die($db->error);
}

$sql = "SELECT pe_id FROM pesquisa WHERE pe_data_proc='$data' AND pe_hora_proc='$hora' AND pe_descricao='$nome'";
$query = $db->query($sql);
$row = $query->fetch_object();
$pesquisaID = $row->pe_id;


if (isset($autor)) { 
	$i=0; 
	while($i<count($autor)){ 
		$autorID = $autor[$i];
		$sql = "INSERT INTO pesquisa_autor VALUES ($pesquisaID,'$autorID',0)";
		$query = $db->query($sql);
    	$i++; 
	} 
}

if (isset($instAutor)) { 
	$i=0; 
	while($i<count($instAutor)){ 
		$instID = $instAutor[$i];
		$sql = "INSERT INTO pesquisa_autor VALUES ($pesquisaID,'NULL',$instID)";
		$query = $db->query($sql);
    	$i++; 
	} 
}




if (strlen(trim($agID)) > 0) {
	$arrayAG = explode(";",$agID);
	$arrayAG = my_array_unique($arrayAG);
	for ($x = 0; $x < count($arrayAG); $x++) {
		$temp = $arrayAG[$x];
		if (strlen(trim($temp)) > 0) {
			$sql = "INSERT INTO rel_geo_pesquisa VALUES ($temp,$pesquisaID)";	
			$query = $db->query($sql);
		}
	}
}


if (strlen(trim($atID)) > 0) {
	$arrayAT = explode(";",$atID);
	$arrayAT = my_array_unique($arrayAT);
	for ($x = 0; $x < count($arrayAT); $x++) {
		$temp = $arrayAT[$x];
		if (strlen(trim($temp)) > 0) {
			$sql = "INSERT INTO rel_area_pesquisa VALUES ($temp,$pesquisaID)";	
			$query = $db->query($sql);
		}
	}
}


if (strlen(trim($biomas)) > 0) {
	$sql = "INSERT INTO rel_habitat_pesquisa VALUES ($biomas,$pesquisaID)";	
	$query = $db->query($sql);
}



$db->close();
?>
<script language="JavaScript">
	window.location.href='pesquisas_arquivos.php?tipo=NE&pesquisa=<?php echo $pesquisaID; ?>&pesDescricao=<?php echo trim($nome); ?>';
</script>