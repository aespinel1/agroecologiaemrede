<?php
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function enviaEmail(url) {
	var email = prompt("Digite um endereço de e-mail:","");
	var nome = prompt("Digite o nome do destinatário:","");
	var remet = prompt("Digite o seu nome:","");
	if (email != null && email != "") {
		url = url + '&email=' + email + '&nome=' + nome + '&remet=' + remet;
		window.location.href=url;
	}
}
</script>
<?php include("menu_geral.php"); ?>
<br>
<br>

<table width="577" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="567" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td  bgcolor="#80AAD2"><p><img src="imagens/ico_bibliografia.gif" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Detalhes
              da Pesquisa </strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
		<tr>
		<td colspan=4 align="center" valign="middle"><br><br><a href="relatorio_pesquisa.php?tipo=EX1&acao=IM&pesquisa=<?php echo $pesquisa; ?>"><img src="imagens/ico_imprimir.gif" width="59" height="12" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="enviaEmail('relatorio_pesquisa.php?tipo=EX1&acao=EM&pesquisa=<?php echo $pesquisa; ?>');"><img src="imagens/ico_envia_email.gif" width="99" height="13" border="0"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="#" onClick="javascript: document.execCommand('SaveAs','1','');"><p class="btn btn-secondary"><span class="oi oi-data-transfer-download"></span> Salvar</p><img style="display:none" src="imagens/ico_salvar_arquivo.gif" width="125" height="15" border="0"></a><br></td>
		</tr>
      </table>
	  <br>
<?php
$sql = "SELECT * FROM pesquisa WHERE pe_id=$pesquisa";
$query = $db->query($sql);
$row = $query->fetch_object();

	$sql = "SELECT * FROM pesquisa WHERE pe_id=$pesquisa";
	$query = $db->query($sql);
	$row = $query->fetch_object();

	$sql = "SELECT * FROM pesquisa_autor,usuario WHERE pea_id_usuario=us_login AND pea_id_pesquisa=$pesquisa";
	$queryAutor = $db->query($sql);
	$numAutor = $queryAutor->num_rows;


	$sql = "SELECT * FROM pesquisa_relator,usuario WHERE per_id_usuario=us_login AND per_id_pesquisa=$pesquisa";
	$queryRelator = $db->query($sql);
	$numRelator = $queryRelator->num_rows;

	$sql = "SELECT * FROM pesquisa_autor,frm_instituicao WHERE pea_id_inst=in_id AND pea_id_pesquisa=$pesquisa";
	$queryInstAutor = $db->query($sql);
	$numInstAutor = $queryInstAutor->num_rows;


	$sql = "SELECT * FROM pesquisa_relator,frm_instituicao WHERE per_id_inst=in_id AND per_id_pesquisa=$pesquisa";
	$queryInstRelator = $db->query($sql);
	$numInstRelator = $queryInstRelator->num_rows;
?>
      <table width="567" border="0">
        <tr>
          <td colspan="2">
		  <table width="561" border="0">
              <tr>
                <td colspan="2">
<?php
		echo "Pesquisa: <strong>$row->pe_descricao</strong><br>";

?>
				<br>
				<?php if (!isset($emailEnviado)) { ?>
					[ <a href="#" class="Preto" onClick="javascript:history.back(1);">Voltar aos resultados de busca</a> ]
				<?php } else { ?>
					[ <a href="#" class="Preto" onClick="javascript:history.go(-3);">Voltar aos resultados de busca</a> ]
				<?php } ?>
			  </td>
              </tr>
            </table>
		  </td>
        </tr>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td width="313">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td valign="top"> <?php echo nl2br($row->pe_resumo); ?>
          </td>
          <td width="244" valign="top">
		      <?php if ($numAutor > 0 or $numInstAutor > 0) { ?>
		    <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="21"><img src="imagens/ico_usuarios_transp_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Autor(es):</strong></td>
              </tr>
              <tr>
                <td height="1" colspan="2" bgcolor="#000000">
                  <div align="center"></div></td>
              </tr>
              <tr bgcolor="#B3CBCC">
                <td colspan="2">
              <?php
			if ($numAutor > 0) {
			   echo "<br>";
			   while ($rowAutor = $queryAutor->fetch_object()) {
				  echo "<img src=\"imagens/seta02.gif\"><a href=\"usuarios.php?usuNome=$rowAutor->us_nome&usu=$rowAutor->us_login\" class=\"Preto\">$rowAutor->us_nome</a><br>";
			   }
			}

		   if ($numInstAutor > 0) {
			  echo "<br>";
			  while ($rowInstAutor = $queryInstAutor->fetch_object()) {
				   echo "<img src=\"imagens/seta02.gif\"><a href=\"instituicoes.php?instDesc=$rowInstAutor->in_nome&inst=$rowInstAutor->in_id\" class=\"Preto\">$rowInstAutor->in_nome</a><br>";
			  }
		   }
?>
                </td>
              </tr>
            </table>
			   <br>
			<?php }
         	if ($numRelator > 0 or $numInstRelator > 0) { ?>

            <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="21"><img src="imagens/ico_bibliografia_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Relator(es):</strong></td>
              </tr>
              <tr>
                <td height="1" colspan="2" bgcolor="#000000">
                  <div align="center"></div></td>
              </tr>
              <tr bgcolor="#B3CBCC">
                <td colspan="2">
              <?php

		      if ($numRelator > 0) {
			     echo "<br>";
			     while ($rowRelator = $queryRelator->fetch_object()) {
				      echo "<img src=\"imagens/seta02.gif\"><a href=\"usuarios.php?usuNome=$rowRelator->us_nome&usu=$rowRelator->us_login\" class=\"Preto\">$rowRelator->us_nome</a><br>";
			     }
		      }


		    if ($numInstRelator > 0) {
			   echo "<br>";
			   while ($rowInstRelator = $queryInstRelator->fetch_object()) {
				  echo "<img src=\"imagens/seta02.gif\"><a href=\"instituicoes.php?instDesc=$rowInstRelator->in_nome&inst=$rowInstRelator->in_id\" class=\"Preto\">$rowInstRelator->in_nome</a><br>";
			   }
		   }
?>
                </td>
              </tr>
            </table>
            <br>
			<?php }
			$sql = "SELECT * FROM pesquisa_arquivo WHERE pa_id_pesquisa=$pesquisa";
            $query = $db->query($sql);
            $num = $query->num_rows;

            if ($num > 0) {
			?>
            <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="17"><span class="oi oi-document lead text-secondary"></span><img src="imagens/ico_ficha.gif" style="display:none"  width="14" height="13"></td>
                <td width="176"><strong>Anexos</strong></td>
              </tr>
              <tr>
                <td height="1" colspan="2" bgcolor="#000000">
                  <div align="center"></div></td>
              </tr>
              <tr>
                <td colspan="2" bgcolor="#DADADA">
<?php
}

	while ($arrayArquivos = $query->fetch_object()) {
		$nomeDoc = substr(trim($arrayArquivos->pa_arquivo),0,-20);
		$fileSizeDoc = my_filesize("/upload/arquivos/".$arrayArquivos->ea_arquivo);
		echo "&nbsp;<img src=\"imagens/seta02.gif\"> <a href=\"/upload/arquivos/$arrayArquivos->pa_arquivo\" class=\"preto\">$nomeDoc ($fileSizeDoc)</a><br>";
	}


?>
                </td>
              </tr>
            </table>
            <br>
			<?php } ?>
			                  <?php
	$sql = "SELECT * FROM rel_area_pesquisa,area_tematica WHERE rap_id_area=at_id AND rap_id_pesquisa=$pesquisa";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) { ?>
	 <table width="240" border="0" cellspacing="0">
              <tr bgcolor="#333333">
                <td height="1" colspan="2"> <div align="center"></div></td>
              </tr>
              <tr>
			    <td width="21"><img src="imagens/ico_bibliografia_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Áreas Temáticas</strong></td>
              </tr>
              <tr>
                <td valign="top"><img src=imagens/seta02.gif width="12" height="13"></td>
                <td>
<?php
		while ($arrayAreas = $query->fetch_object()) {
			echo "&nbsp;$arrayAreas->at_descricao";
		}

?>
                </td>
              </tr>
            </table>
<?php }

	$sql = "SELECT * FROM rel_geo_pesquisa,area_geografica WHERE rgp_id_geo=ag_id AND rgp_id_pesquisa=$pesquisa";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {
?>
            <br> <table width="239" border="0" cellspacing="0">
              <tr bgcolor="#999999">
                <td height="1" colspan="2"> <div align="center"></div></td>
              </tr>
              <tr>
			    <td width="21"><img src="imagens/ico_bibliografia_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Áreas Geográficas</strong></td>
              </tr>
              <tr>
                <td valign="top"><img src=imagens/seta02.gif width="12" height="13"></td>
                <td>
                  <?php

		while ($arrayGeo = $query->fetch_object()) {
			echo "&nbsp;$arrayGeo->ag_descricao";
		}


?>
                </td>
              </tr>
            </table>
<?php }
	$sql = "SELECT * FROM rel_habitat_pesquisa,habitat WHERE rhp_id_habitat=ha_id AND rhe_id_pesquisa=$pesquisa";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) { ?>
	        <br>
			<table width="239" border="0" cellspacing="0">
              <tr bgcolor="#B4B4B4">
                <td height="1" colspan="2"> <div align="center"></div></td>
              </tr>
              <tr>
			    <td width="21"><img src="imagens/ico_bibliografia_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Biomas</strong></td>
              </tr>
              <tr>
                <td valign="top"><img src=imagens/seta02.gif width="12" height="13"></td>
                <td>
                  <?php

		while ($arrayHab = $query->fetch_object()) {
			echo "&nbsp;$arrayHab->ha_descricao";
		}



?>
                </td>
              </tr>
            </table>
<?php } ?>		<br>

            <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td><strong>Pessoas e Institui&ccedil;&otilde;es-
                  </strong><font size="1">clique nos &iacute;cones para conhecer
                  pessoas ou institui&ccedil;&otilde;es nestes temas<br>
                  <img src="imagens/ico_usuarios_transp_pequeno.gif" width="17" height="16">
                  = Pessoas<br>
                  <img src="imagens/ico_tabela_simples_transp_p.gif" width="17" height="16">
                  = Institui&ccedil;&otilde;es</font></td>
              </tr>
              <tr bgcolor="#000000">
                <td height="1"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
				<table width="238" border="0" cellspacing="0" cellpadding="3">
<?php
$sql = "SELECT * FROM rel_area_pesquisa,area_tematica WHERE rap_id_area=at_id AND rap_id_pesquisa=$pesquisa";
$query = $db->query($sql);
$num = $query->num_rows;

if ($num > 0) {
	while ($arrayAreas = $query->fetch_object()) {
		//<td width=\"186\" valign=\"top\"><a href=\"areas_tematicas.php?areaDesc=$arrayAreas->at_descricao&area=$arrayAreas->at_id\" class=\"preto\">$arrayAreas->at_descricao</a></td>
		echo "<tr>
                <td width=\"40\" valign=\"top\" align=\"center\">
				<a href=\"xt_adiciona_agat.php?url=usuarios&tipo=at&id=$arrayAreas->at_id\"><img border=\"0\" src=\"imagens/ico_usuarios_transp_pequeno.gif\" width=\"17\" height=\"16\"></a>&nbsp;<a href=\"xt_adiciona_agat.php?url=instituicoes&tipo=at&id=$arrayAreas->at_id\"><img border=\"0\" src=\"imagens/ico_tabela_simples_transp_p.gif\" width=\"17\" height=\"16\"></a>
				</td>
                <td width=\"186\" valign=\"top\" class=\"textoPreto\">$arrayAreas->at_descricao</td>
               </tr>\n";

	}
}

?>
                  </table></td>
              </tr>
            </table>
            <br>
<?php
/*
			$sql = "SELECT * FROM rel_area_pesquisa,area_tematica WHERE rap_id_area=at_id AND rap_id_pesquisa=$pesquisa";
            $query = $db->query($sql);
            $num = $query->num_rows;
			if ($num > 0) {
?>
            <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td class="textoPreto" colspan="2"><span><strong>Conhe&ccedil;a</strong></span>
                  documentos nestas &aacute;reas tem&aacute;ticas! Basta clicar
                  nas imagens abaixo.</td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000" colspan="2"> <div align="center"></div></td>
              </tr>
                  <?php

if ($num > 0) {
	while ($arrayAreas = $query->fetch_object()) {
		echo "<tr>
                <td width=\"40\" valign=\"top\" align=\"center\">
				<a href=\"bib_pesquisa.php?tituloTodo=&at=$arrayAreas->at_id#result\"><img border=\"0\" src=\"imagens/ico_bibliografia_pequeno.gif\" width=\"17\" height=\"16\"></a>
				</td>
                <td width=\"186\" valign=\"top\" class=\"textoPreto\">$arrayAreas->at_descricao</td>
               </tr>\n";

	}
}

?>
            </table>
<?php }

*/ ?>
          </td>
        </tr>
      </table> </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
