<?php
if (!isset($_REQUEST['usu'])) {
	header('Location: usuarios_busca.php');
	exit;
}

include("conexao.inc.php");

$arqs = array();
$imgs = array();

$sql = "SELECT * FROM usuario WHERE us_login='$usu'";
$query = $db->query($sql);
$rowUSR = $query->fetch_object();

// Dados geográficos:
if (substr($rowUSR->txt_cidade,6,1) != '.') {
	// invalid city!!
	$geo = null;
	$cidadeUF = $rowUSR->txt_cidade." - ".$rowUSR->txt_estado;
	$pais = $rowUSR->txt_pais;
} else {
	$pais = substr($rowUSR->txt_cidade,0,2);
	$sql = "SELECT a.nome as cidade, b.nome as UF, c.nome as pais FROM _".$pais." as a, _".$pais."_maes as b, __paises as c WHERE a.id_mae=b.id AND c.id='".$pais."' AND a.id='".$rowUSR->txt_cidade."'";
	$query = $db->query($sql);
	$geo = $query->fetch_object();
	$cidadeUF = $geo->cidade."/".$geo->UF;
	$pais = $geo->pais;
}

// Experiências das quais a pessoa é autora
$sql = "SELECT * FROM experiencia_autor, experiencia WHERE  exa_id_usuario = '$rowUSR->us_login' AND exa_id_experiencia=ex_id ";
$queryAutora = $db->query($sql);
$numAutora = $queryAutora->num_rows;

// Lista de experiências e de imagens
$experienciasComoAutora = array();
if ($numAutora>0) {
	while ($row = $queryAutora->fetch_object()) {
		$experienciasComoAutora[$row->ex_id] = $row;
	}
	// Lista de experiências e de imagens
	$sql = "SELECT * FROM frm_exp_base_comum WHERE ex_id IN (".implode(array_keys($experienciasComoAutora),',').")";
	$queryAnexos = $db->query($sql);
	$numAnexos = $queryAnexos->num_rows;
	while($rowAnexos = $queryAnexos->fetch_object()) {
		if ($rowAnexos->ex_anexos) {
		  $anexos = explode('|',$rowAnexos->ex_anexos);
		  if ($anexos) {
		  	foreach ($anexos as $arq) {
		  		if (file_exists($dir['upload'].$arq) && is_array(getimagesize($dir['upload'].$arq))) {
						$newImage = array(
							'arquivo'=>$arq,
							'ex_id'=>$rowAnexos->ex_id,
							'ex_descricao'=>$experienciasComoAutora[$rowAnexos->ex_id]->ex_descricao,
							'relacao'=>'autor/a'
						);
		  			$imgs[] = $newImage;
						$experienciasComoAutora[$rowAnexos->ex_id]->imgs[] = $newImage;
						$arqs[]=$arq;
		  		}
		  	}
		  }
		}
	}
}

// Experiências nas quais a pessoa foi relatora
$sql = "SELECT * FROM experiencia_relator, experiencia  WHERE  exr_id_usuario = '$rowUSR->us_login' AND exr_id_experiencia = ex_id";
$queryRelatora = $db->query($sql);
$numRelatora = $queryRelatora->num_rows;

// Lista de experiências e de imagens
$experienciasComoRelatora = array();
if ($numRelatora>0) {
	while ($row = $queryRelatora->fetch_object()) {
		$experienciasComoRelatora[$row->ex_id] = $row;
	}
	// Imagens das experiências relacionadas a esta instituição
	$sql = "SELECT * FROM frm_exp_base_comum WHERE ex_id IN (".implode(array_keys($experienciasComoRelatora),',').")";
	$queryAnexos = $db->query($sql);
	$numAnexos = $queryAnexos->num_rows;
	while($rowAnexos = $queryAnexos->fetch_object()) {
		if ($rowAnexos->ex_anexos) {
		  $anexos = explode('|',$rowAnexos->ex_anexos);
		  if ($anexos) {
		  	foreach ($anexos as $arq) {
		  		if (file_exists($dir['upload'].$arq) && is_array(getimagesize($dir['upload'].$arq)) && !in_array($arq, $arqs)) {
						$newImage = array(
							'arquivo'=>$arq,
							'ex_id'=>$rowAnexos->ex_id,
							'ex_descricao'=>$experienciasComoRelatora[$rowAnexos->ex_id]->ex_descricao,
							'relacao'=>'relator/a'
						);
		  			$imgs[] = $newImage;
						$experienciasComoRelatora[$rowAnexos->ex_id]->imgs[] = $newImage;
						$arqs[]=$arq;
		  		}
		  	}
		  }
		}
	}
}

// Experiências relacionadas à pessoa enquanto autora
$sql = "SELECT * FROM experiencia_autor,experiencia WHERE exa_id_experiencia=ex_id AND exa_id_usuario='$usu'";
$queryExperienciasRelacionadas = $db->query($sql);
$numExperienciasRelacionadas = $queryExperienciasRelacionadas->num_rows;

// Experiências relacionadas à pessoa enquanto relatora
$sql = "SELECT * FROM experiencia_relator,experiencia WHERE exr_id_experiencia=ex_id AND exr_id_usuario='$usu'";
$queryExperienciasRelacionadasR = $db->query($sql);
$numExperienciasRelacionadasR = $queryExperienciasRelacionadasR->num_rows;

// Instituições das quais a pessoa faz parte
$sql = "SELECT * FROM rel_inst_usuario,frm_instituicao WHERE riu_id_inst=in_id AND riu_id_usuario='$usu'";
$queryInst = $db->query($sql);
$numInst = $queryInst->num_rows;

// Áreas temáticas
$sql = "SELECT * FROM rel_area_usuario,area_tematica WHERE rau_id_area=at_id AND rau_id_usuario='$usu'";
$queryATs = $db->query($sql);
$numATs = $queryATs->num_rows;

// Áreas geográficas
$sql = "SELECT * FROM rel_geo_usuario,area_geografica WHERE rgu_id_geo=ag_id AND rgu_id_usuario='$usu'";
$queryAGs = $db->query($sql);
$numAGs = $queryAGs->num_rows;

shuffle($imgs);

$meta = pega_metadados();
?>

<!DOCTYPE html>
<html>
<head>
<title><?=$meta->title?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script language="JavaScript">
	function abrePopup(theURL,winName,features) { //v2.0
  	window.open(theURL,winName,features);
}
</script>

<?php include("menu_geral.php");?>

<div class="container">
  <div class="row">
    <div class="col-md-12">
			<div class="alert alert-success">
      	<h1><i class="aericon-pessoas"></i> <?=$rowUSR->us_nome?></h1>
			</div>
    </div>
  </div>
	<?php if($isAdmin) { ?>
		<div class="row mb-4">
			<div class="col-md-12">
				<a title="Editar" class="btn btn-warning mr-2" href="<?=$dir['base_URL']?>admin/usuarios_cadastro.php?usuario=<?=$usu?>"><i class="oi oi-pencil"></i></a>
			</div>
		</div>
	<?php } ?>
  <div class="row">
		<?php if (count($imgs)>0) { ?>
    	<div class="col-sm-12 col-md-6 col-lg-8">
				<div class="alert alert-light">
	        <div class="row mb-3">
	          <div id="carousel" class="carousel slide col-md-12" data-ride="false">
	            <div class="carousel-inner">
	              <?php foreach ($imgs as $i=>$img) { ?>
	                <div class="carousel-item<?=($i==0) ? ' active' : ''?>">
	                  <a class="popImage" id="image<?=$i?>">
	                  	<img class="d-block w-100" src="<?=getResizedImgURL($img['arquivo'],1024,800)?>" alt="<?=$img['ex_descricao']?>">
	                  </a>
										<div class="carousel-caption d-none d-md-block">
									    <!--<h5><a href="<?=$dir['base_URL']?>experiencias.php?experiencia=<?=$img['ex_id']?>"><?=$img['ex_descricao']?></a></h5>-->
											<h5><?=$img['ex_descricao']?></h5>
									    <p>Relação com a experiência: <?=$img['relacao']?></p>
									  </div>
	                </div>
	              <?php } ?>
	            </div>
	            <?php if (count($imgs)>1) { ?>
	              <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
	                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	                <span class="sr-only">Anterior</span>
	              </a>
	              <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
	                <span class="carousel-control-next-icon" aria-hidden="true"></span>
	                <span class="sr-only">Próxima</span>
	              </a>
	            <?php } ?>
	          </div>
	        </div>
				</div>
    	</div>
		<?php } ?>

		<!-- Dados cadastrais básicos: -->
		<div class="col-sm-12 col-md-6 <?=(count($imgs)>0) ? 'col-lg-4' : 'col-lg-6'?>">
			<div class="card">
				<div class="card-header">
					<strong>Dados básicos</strong>
				</div>
				<div class="card-body">
					<dl>
						<?php if($rowUSR->us_url) { ?>
							<dt>Site</dt>
							<dd><a href="<?=$rowUSR->us_url?>"><?=$rowUSR->us_url?></a></dd>
						<?php } ?>
						<?php if($rowUSR->txt_telefone) { ?>
							<dt>Telefone(s)</dt>
							<dd><?=$rowUSR->txt_telefone?></dd>
						<?php } ?>
						<?php if($rowUSR->us_email) { ?>
							<dt>E-mail</dt>
							<dd><a href="mailto:<?=$rowUSR->us_email?>"><?=$rowUSR->us_email?></a></dd>
						<?php } ?>

						<dt>Endereço</dt>
						<dd>
							<?=($rowUSR->txt_endereco)?$rowUSR->txt_endereco.'<br>':''?>
							<?=($cidadeUF)?$cidadeUF.'<br>':''?>
							<?=($rowUSR->txt_cep)?'CEP: '.$rowUSR->txt_cep.'<br>':''?>
							<?=($pais)?$pais:''?>
						</dd>
					</dl>
				</div>
			</div>
		</div>

		<!-- Experiências enquanto autora: -->
		<?php if(count($experienciasComoAutora)>0) { ?>
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="card">
					<div class="card-header">
						<strong><?=(count($experienciasComoAutora)==1) ? "Experiência da qual é autor/a" : count($experienciasComoAutora)." experiências das quais é autor/a"?></strong>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<?php foreach ($experienciasComoAutora as $e) { ?>
								<a class="list-group-item list-group-item-active" href="<?= $dir[base_URL] ?>experiencias.php?experiencia=<?= $e->ex_id ?>">
									<?php if (isset($e->imgs)) { ?>
										<div class="row">
								      <div class="col-md-6 col-lg-3 d-sm-none d-md-inline px-1">
								        <img class="w-100" src="<?=getResizedImgURL($e->imgs[0]['arquivo'],120,120)?>">
								      </div>
								      <div class="col-md-6 col-lg-9">
								  			<?= $e->ex_descricao ?>
								      </div>
										</div>
									<?php } else { ?>
										<?= $e->ex_descricao ?>
									<?php } ?>
								</a>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		<?php } ?>

		<!-- Experiências enquanto relatora: -->
		<?php if(count($experienciasComoRelatora)>0) { ?>
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="card">
					<div class="card-header">
						<strong><?=(count($experienciasComoRelatora)==1) ? "Experiência em que é relator/a" : count($experienciasComoRelatora)." experiências em que é relator/a"?></strong>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<?php foreach ($experienciasComoRelatora as $e) { ?>
								<a class="list-group-item list-group-item-active" href="<?= $dir[base_URL] ?>experiencias.php?experiencia=<?= $e->ex_id ?>">
									<?php if (isset($e->imgs)) { ?>
										<div class="row">
											<div class="col-sm-6 col-md-6 col-lg-3 d-none d-sm-block px-1">
								        <img class="w-100" src="<?=getResizedImgURL($e->imgs[0]['arquivo'],120,120)?>">
								      </div>
								      <div class="col-sm-6 col-md-6 col-lg-9">
								  			<?= $e->ex_descricao ?>
								      </div>
										</div>
									<?php } else { ?>
										<?= $e->ex_descricao ?>
									<?php } ?>
								</a>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		<?php } ?>

		<!-- Instituições -->
		<?php if($numInst>0) { ?>
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="card">
					<div class="card-header">
						<strong><?=(count($numInst)>1) ? "Instituições das quais faz parte" : "Instituição da qual faz parte"?></strong>
					</div>
					<div class="card-body">
						<ul class="list-group list-group-flush">
							<?php while($e = $queryInst->fetch_object()) { ?>
								<a class="list-group-item list-group-item-active" href="<?= $dir[base_URL] ?>instituicoes.php?inst=<?= $e->in_id ?>"><?= $e->in_nome ?></a>
							<?php } ?>
						</ul>
					</div>
				</div>
			</div>
		<?php } ?>

		<!-- Áreas de atuação -->
		<?php if($isAdmin || $numATs>0 || $numAGs>0) { ?>
			<div class="col-sm-12 col-md-6 col-lg-6">
				<div class="card">
					<div class="card-header">
						<strong>Áreas de atuação</strong>
					</div>
					<div class="card-body">
						<?php if ($numATs > 0) { ?>
							<h5 class="card-title"><i class="oi oi-tags"></i> <?=($numATs==1) ? "Área Temática" : "Áreas Temáticas"?></h5>
							<ul class="list-group list-group-flush mb-4">
								<?php while ($rowAT = $queryATs->fetch_object()) { ?>
									<a class="list-group-item list-group-item-active" href="<?=$dir['base_URL']?>mapeo/?f=consultar&c=1&tipofrm=experiencia&ex_areas_tematicas[]=<?=$rowAT->at_id?>"><?= traduz($rowAT->at_descricao) ?></a>
								<?php } ?>
							</ul>
						<?php } ?>
						<?php if ($numAGs > 0) { ?>
							<h5 class="card-title"><i class="oi oi-globe"></i> <?=($numAGs==1) ? "Área Geográfica" : "Áreas Geográficas"?></h5>
							<ul class="list-group list-group-flush">
								<?php while ($rowAG = $queryAGs->fetch_object()) { ?>
									<li class="list-group-item"><?=$rowAG->ag_descricao?></li>
								<?php } ?>
							</ul>
						<?php } ?>
					</div>
				</div>
			</div>
		<?php } ?>
  </div>
</div>

<!-- Full Image Modal -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
      <div class="mr-5 mt-3" style="position:absolute;top:0;right:0;z-index:10">
        <button type="button" class="close bg-light" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span>Fechar</span></button>
      </div>
      <div class="modal-body mx-auto mt-0 mb-0 pt-0 pb-0">
        <img src="" id="imagepreview" style="max-width:100%;max-height:100vh" >
      </div>
    </div>
  </div>
</div>


</body>

</html>

<?php $db->close(); ?>
