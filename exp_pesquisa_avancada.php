<?php
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function replaceString(originalString,searchText,replaceText) { 

	var strLength = originalString.length; 
	var txtLength = searchText.length; 

	if ((strLength == 0) || (txtLength == 0)) {
		 return originalString; 
	} 
	var i = originalString.indexOf(searchText); 
	
	if ((!i) && (searchText != originalString.substring(0,txtLength))) {
		 return originalString; 
	} 
	if (i == -1) {
		 return originalString; 
	} 
	var newstr = originalString.substring(0,i) + replaceText; 
	if (i+txtLength < strLength){
		 newstr += replaceString(originalString.substring(i+txtLength,strLength),searchText,replaceText); 
	} 
	
	return newstr;
} 

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_showHideLayers() { //v6.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) if ((obj=MM_findObj(args[i]))!=null) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}

function liberaAreaTematica() {

	if(formAvanc.ck_area_tematica.checked == false) {
		//formAvanc.rd_at[0].disabled=true;
		//formAvanc.rd_at[1].disabled=true;
		MM_showHideLayers('layerTema','','hide');

		formAvanc.ordem.value = replaceString(formAvanc.ordem.value,'rel_area_experiencia;','');
		
	}
	else {
	
		//formAvanc.rd_at[0].disabled=false;
		//formAvanc.rd_at[1].disabled=false;
		MM_showHideLayers('layerTema','','show');
		
		formAvanc.ordem.value+='rel_area_experiencia';
		
	}
}

function liberaBibliografia() {

	if(formAvanc.ck_bibliografia.checked == false) {

		MM_showHideLayers('layerBib','','hide');

		formAvanc.ordem.value = replaceString(formAvanc.ordem.value,'rel_bib_experiencia;','');
		
	}
	else {
	
		MM_showHideLayers('layerBib','','show');
		
		formAvanc.ordem.value+='rel_bib_experiencia';
		
	}
}

function liberaAreasGeograficas() {

	if(formAvanc.ck_areas_geograficas.checked == false) {
		MM_showHideLayers('layerGeo','','hide');

		formAvanc.ordem.value = replaceString(formAvanc.ordem.value,'rel_geo_experiencia;','');
		
	}
	else {
	
		MM_showHideLayers('layerGeo','','show');
		
		formAvanc.ordem.value+='rel_geo_experiencia';
		
	}
}

function liberaChaves() {

	if(formAvanc.ck_chaves.checked == false) {
		MM_showHideLayers('layerChaves','','hide');

		formAvanc.ordem.value = replaceString(formAvanc.ordem.value,'experiencia_chave;','');
		
	}
	else {
	
		MM_showHideLayers('layerChaves','','show');
		
		formAvanc.ordem.value+='experiencia_chave';
		
	}
}

function liberaAcompanhamento() {

	if(formAvanc.ck_acompanhamento.checked == false) {
		MM_showHideLayers('layerAcompanha','','hide');
	}
	else {
		MM_showHideLayers('layerAcompanha','','show');
	}
}

function liberaStatus() {

	if(formAvanc.ck_status.checked == false) {
		MM_showHideLayers('layerStatus','','hide');
	}
	else {
		MM_showHideLayers('layerStatus','','show');
	}
}

function liberaTipo() {

	if(formAvanc.ck_tipo.checked == false) {
		MM_showHideLayers('layerTipo','','hide');
	}
	else {
		MM_showHideLayers('layerTipo','','show');
	}
}
</script>
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo" onLoad="liberaAreaTematica();liberaBibliografia();liberaAreasGeograficas();liberaChaves();liberaAcompanhamento();liberaStatus();liberaTipo();">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57">&nbsp; </td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Pesquisa 
              avan&ccedil;ada de experi&ecirc;ncias</strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
	  <form name="formAvanc" method="post" action="xt_exp_pesquisa_avancada.php">
	  <input type="hidden" name="ordem">
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr> 
          <td><p><strong>Busca avan&ccedil;ada</strong> 
                - esta parte do sistema poder&aacute; pesquisar as experi&ecirc;ncias 
                que batem com crit&eacute;rios mais espec&iacute;ficos do SisGEA. 
                Quando voc&ecirc; quiser que um dos crit&eacute;rios esteja inclu&iacute;do 
                na busca, selecione-o e digite os par&acirc;metros necess&aacute;rios. 
                Nas listas, para selecionar mais de um par&acirc;metro, utilize 
                a tecla CTRL do seu teclado.</p>
              <p><strong>Abrang&ecirc;ncia</strong> - a 
                busca ir&aacute; retornar a maior quantidade de resultados poss&iacute;veis, 
                batendo &eacute; claro, com os par&acirc;metros escolhidos abaixo.<strong></strong></p></td>
        </tr>
        <tr> 
          <td height="1" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr> 
          <td><table width="555" border="0">
                <tr> 
                  <td width="272" valign="top"> <table width="272" border="0">
                      <tr> 
                        <td width="26"><div align="center"> 
                            <input name="ck_area_tematica" type="checkbox" value="1" onClick="liberaAreaTematica();">
                          </div></td>
                        <td width="236"><strong>&Aacute;rea 
                          Tem&aacute;tica</strong></td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td> <div id="layerTema"> <span> 
                            <input name="rd_at" type="radio" value="1" checked>
                            e 
                            <input type="radio" name="rd_at" value="2">
                            ou</span><br>
                            <select name="tema[]" size="6" multiple>
                              <?php
				//$sql = "SELECT * FROM area_tematica ORDER BY at_descricao";
				$sql = "SELECT a.at_id,a.at_descricao,b.at_descricao as bdesc FROM area_tematica a LEFT JOIN area_tematica b ON a.at_pertence=b.at_id ORDER BY a.at_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->at_descricao);
					$desc2 = trim($row->bdesc);
					if (strlen(trim($desc2)) > 0) {
						$temp = "($desc2)";
					}
					echo "<option value=\"$row->at_id\">$desc $temp</option>";
					$temp = "";
				}
?>
                            </select>
                          </div></td>
                      </tr>
                    </table></td>
                  <td width="273" valign="top"> <table width="272" border="0">
                      <tr> 
                        <td width="26"><div align="center"> 
                            <input name="ck_bibliografia" type="checkbox" value="1" onClick="liberaBibliografia();">
                          </div></td>
                        <td width="236"><strong>Refer&ecirc;ncias 
                          Bibliogr&aacute;ficas </strong></td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td> <div id="layerBib"> <span> 
                            <input name="rd_bb" type="radio" value="1" checked>
                            e 
                            <input type="radio" name="rd_bb" value="2">
                            ou</span><br>
                            <select name="bib[]" size="6" multiple>
                              <?php
				$sql = "SELECT * FROM bibliografia ORDER BY bb_titulo_todo";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->bb_titulo_todo);
					echo "<option value=\"$row->bb_id\">$desc</option>";
				}
?>
                            </select>
                          </div></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td valign="top"><table width="272" border="0">
                      <tr> 
                        <td width="26"><div align="center"> 
                            <input name="ck_areas_geograficas" type="checkbox" value="1" onClick="liberaAreasGeograficas();">
                          </div></td>
                        <td width="236"><strong>&Aacute;reas 
                          Geogr&aacute;ficas</strong></td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td> <div id="layerGeo"> <span> 
                            <input name="rd_ag" type="radio" value="1" checked>
                            e 
                            <input type="radio" name="rd_ag" value="2">
                            ou</span><br>
                            <select name="geo[]" size="6" multiple>
                              <?php
				//$sql = "SELECT a.ag_id,a.ag_descricao,b.ag_descricao as bdesc FROM area_geografica a,area_geografica b WHERE a.ag_pertence=b.ag_id ORDER BY a.ag_descricao";
				$sql = "SELECT a.ag_id,a.ag_descricao,b.ag_descricao as bdesc FROM area_geografica a LEFT JOIN area_geografica b ON a.ag_pertence=b.ag_id ORDER BY a.ag_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->ag_descricao);
					$desc2 = trim($row->bdesc);
					if (strlen(trim($desc2)) > 0) {
						$temp = "($desc2)";
					}
					echo "<option value=\"$row->ag_id\">$desc $temp</option>";
					$temp = "";
				}
?>
                            </select>
                          </div></td>
                      </tr>
                    </table></td>
                  <td valign="top"><table width="272" border="0">
                      <tr> 
                        <td width="26"><div align="center"> 
                            <input name="ck_chaves" type="checkbox" id="ck_chaves2" value="1" onClick="liberaChaves();">
                          </div></td>
                        <td width="236"><strong>Palavras 
                          Chave </strong></td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td> <div id="layerChaves"> <span> 
                            <input name="rd_pc" type="radio" value="1" checked>
                            e 
                            <input type="radio" name="rd_pc" value="2">
                            ou</span><br>
                            <select name="chaves[]" size="6" multiple id="select3">
                              <?php
				$sql = "SELECT * FROM palavra_chave ORDER BY pc_palavra";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->pc_palavra);
					echo "<option value=\"$row->pc_id\">$desc</option>";
				}
?>
                            </select>
                          </div></td>
                      </tr>
                    </table></td>
                </tr>
                <tr> 
                  <td valign="top"><table width="272" border="0">
                      <tr> 
                        <td width="26"><div align="center"> 
                            <input name="ck_acompanhamento" type="checkbox" value="1" onClick="liberaAcompanhamento();">
                          </div></td>
                        <td width="236"><strong>Acompanhamento 
                          T&eacute;cnico</strong></td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td> <div id="layerAcompanha"> 
                            <select name="acompanha">
                              <option value="1">Sim</option>
                              <option value="0">N&atilde;o</option>
                            </select>
                          </div></td>
                      </tr>
                    </table></td>
                  <td valign="top"><table width="272" border="0">
                      <tr> 
                        <td width="26"><div align="center"> 
                            <input name="ck_status" type="checkbox" value="1" onClick="liberaStatus();">
                          </div></td>
                        <td width="236"><strong>Status</strong></td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td> <div id="layerStatus"> 
                            <select name="status" id="select4">
                              <?php
			$sql = "SELECT * FROM status_experiencia";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                              <option value="<?php echo $row->se_id; ?>"><?php echo trim($row->se_status); ?></option>
                              <?php
			} // la�o do loop
?>
                            </select>
                          </div></td>
                      </tr>
                    </table></td>
                </tr>
                <tr valign="top"> 
                  <td><table width="272" border="0">
                      <tr> 
                        <td width="26"><div align="center"> 
                            <input name="ck_tipo" type="checkbox" value="1" onClick="liberaTipo();">
                          </div></td>
                        <td width="236"><strong>Tipo de 
                          Experi&ecirc;ncia </strong></td>
                      </tr>
                      <tr> 
                        <td>&nbsp;</td>
                        <td> <div id="layerTipo"> 
                            <select name="tipo" id="tipo">
                              <?php
			$sql = "SELECT * FROM tipo_experiencias ORDER BY tx_descricao";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                              <option value="<?php echo $row->tx_id; ?>"><?php echo trim($row->tx_descricao); ?></option>
                              <?php
			} // la�o do loop
?>
                            </select>
                          </div></td>
                      </tr>
                    </table></td>
                  <td>&nbsp;</td>
                </tr>
              </table>
              <p align="center"> 
                <input name="Submit" type="submit" class="botaoLogin" value="     Buscar     ">
              </p></td>
        </tr>
      </table>
	  </form>
	  </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>