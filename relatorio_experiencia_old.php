<?php include("conexao.inc.php"); ?>
<!DOCTYPE html>
<html>
<head>
<title>Relat&oacute;rio</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="676" border="0" cellpadding="3">
  <tr>
    <td width="350" ><img src="imagens/agroecologia_em_rede.gif" width="149" height="34"></td>
    <td width="439">
<div align="right"></div></td>
  </tr>
  <tr>
    <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
  </tr>
  <tr>
    <td colspan="2">

<?php
$sql = "SELECT * FROM experiencia WHERE ex_id=$experiencia";
$query = $db->query($sql);
$row = $query->fetch_object();

	$sql = "SELECT * FROM experiencia WHERE ex_id=$experiencia";
	$query = $db->query($sql);
	$row = $query->fetch_object();

	$tituloExperiencia = trim($row->ex_descricao);

	$sql = "SELECT * FROM experiencia_autor,usuario WHERE exa_id_usuario=us_login AND exa_id_experiencia=$experiencia";
	$queryAutor = $db->query($sql);
	$numAutor = $queryAutor->num_rows;


	$sql = "SELECT * FROM experiencia_relator,usuario WHERE exr_id_usuario=us_login AND exr_id_experiencia=$experiencia";
	$queryRelator = $db->query($sql);
	$numRelator = $queryRelator->num_rows;

	$sql = "SELECT * FROM experiencia_autor,frm_instituicao WHERE exa_id_inst=in_id AND exa_id_experiencia=$experiencia";
	$queryInstAutor = $db->query($sql);
	$numInstAutor = $queryInstAutor->num_rows;


	$sql = "SELECT * FROM experiencia_relator,frm_instituicao WHERE exr_id_inst=in_id AND exr_id_experiencia=$experiencia";
	$queryInstRelator = $db->query($sql);
	$numInstRelator = $queryInstRelator->num_rows;
?>
      <table width="567" border="0" align="center">
        <tr>
          <td colspan="2">
		  <table width="561" border="0">
              <tr>
                <td colspan="2">

<?php
		echo "Experiência: <strong>$row->ex_descricao</strong><br>";
		echo "Chamada    : <strong>$row->ex_chamada</strong><br>";
?>
			  </td>
              </tr>
            </table>
			</td>
        </tr>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td width="313">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td valign="top"> <?php echo nl2br($row->ex_resumo); ?>
          </td>
          <td width="244" valign="top"><table width="244" border="0" cellspacing="0" cellpadding="3">
		  	      <?php if ($numAutor > 0 or $numInstAutor > 0) { ?>

              <tr>
                <td width="21"><img src="imagens/ico_usuarios_transp_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Autor(es):</strong></td>
              </tr>
              <tr>
                <td height="1" colspan="2" bgcolor="#000000">
                  <div align="center"></div></td>
              </tr>
              <tr bgcolor="#B3CBCC">
                <td colspan="2">
<?php
		if ($numAutor > 0) {
			echo "<br>";
			while ($rowAutor = $queryAutor->fetch_object()) {
				echo "<img src=\"imagens/seta02.gif\"><a href=\"usuarios.php?usuNome=$rowAutor->us_nome&usu=$rowAutor->us_login\" class=\"Preto\">$rowAutor->us_nome</a><br>";
			}
		}

		if ($numInstAutor > 0) {
			echo "<br>";
			while ($rowInstAutor = $queryInstAutor->fetch_object()) {
				echo "<img src=\"imagens/seta02.gif\"><a href=\"instituicoes.php?instDesc=$rowInstAutor->in_nome&inst=$rowInstAutor->in_id\" class=\"Preto\">$rowInstAutor->in_nome</a><br>";
			}
		}


?>
                </td>
              </tr>
            </table>
			 <br>
			<?php  }

			if ($numRelator > 0 or $numInstRelator > 0) { ?>
            <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="21"><img src="imagens/ico_bibliografia_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Relator(es):</strong></td>
              </tr>
              <tr>
                <td height="1" colspan="2" bgcolor="#000000">
                  <div align="center"></div></td>
              </tr>
              <tr bgcolor="#B3CBCC">
                <td colspan="2">
                  <?php

		if ($numRelator > 0) {
			echo "<br>";
			while ($rowRelator = $queryRelator->fetch_object()) {
				echo "<img src=\"imagens/seta02.gif\"><a href=\"usuarios.php?usuNome=$rowRelator->us_nome&usu=$rowRelator->us_login\" class=\"Preto\">$rowRelator->us_nome</a><br>";
			}
		}

		if ($numInstRelator > 0) {
			echo "<br>";
			while ($rowInstRelator = $queryInstRelator->fetch_object()) {
				echo "<img src=\"imagens/seta02.gif\"><a href=\"instituicoes.php?instDesc=$rowInstRelator->in_nome&inst=$rowInstRelator->in_id\" class=\"Preto\">$rowInstRelator->in_nome</a><br>";
			}
		}

?>
                </td>
              </tr>
            </table>
            <br>
			<?php }

}

$sql = "SELECT * FROM experiencia_arquivo WHERE ea_id_experiencia=$experiencia";
$query = $db->query($sql);
$num = $query->num_rows;

if ($num > 0) {

?>
            <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="17"><span class="oi oi-document lead text-secondary"></span><img src="imagens/ico_ficha.gif" style="display:none"  width="14" height="13"></td>
                <td width="176"><strong>Anexos</strong></td>
              </tr>
              <tr>
                <td height="1" colspan="2" bgcolor="#000000">
                  <div align="center"></div></td>
              </tr>
              <tr>
                <td colspan="2" bgcolor="#DADADA">

<?php
	while ($arrayArquivos = $query->fetch_object()) {
		$nomeDoc = substr(trim($arrayArquivos->ea_arquivo),0,-20);
		$fileSizeDoc = my_filesize("/upload/arquivos/".$arrayArquivos->ea_arquivo);
		echo "&nbsp;<img src=\"imagens/seta02.gif\"> <a href=\"/upload/arquivos/$arrayArquivos->ea_arquivo\" class=\"preto\">$nomeDoc ($fileSizeDoc)</a><br>";
	}

?>
                </td>
              </tr>
            </table>
            <br>
 <?php }

	$sql = "SELECT * FROM rel_area_experiencia,area_tematica WHERE rae_id_area=at_id AND rae_id_experiencia=$experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) { ?>
	 <table width="240" border="0" cellspacing="0">
              <tr bgcolor="#333333">
                <td height="1" colspan="2"> <div align="center"></div></td>
              </tr>
              <tr>
			    <td width="21"><img src="imagens/ico_bibliografia_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Áreas Temáticas</strong></td>
              </tr>
              <tr>
                <td><img src=imagens/seta02.gif width="12" height="13"></td>
                <td>
<?php
		while ($arrayAreas = $query->fetch_object()) {
			echo "&nbsp;$arrayAreas->at_descricao";
		}

?>
                </td>
              </tr>
            </table>
<?php }

	$sql = "SELECT * FROM rel_geo_experiencia,area_geografica WHERE rge_id_geo=ag_id AND rge_id_experiencia=$experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {
?>
            <br> <table width="239" border="0" cellspacing="0">
              <tr bgcolor="#999999">
                <td height="1" colspan="2"> <div align="center"></div></td>
              </tr>
              <tr>
			    <td width="21"><img src="imagens/ico_bibliografia_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Áreas Geográficas</strong></td>
              </tr>
              <tr>
                <td><img src=imagens/seta02.gif width="12" height="13"></td>
                <td>
                  <?php

		while ($arrayGeo = $query->fetch_object()) {
			echo "&nbsp;$arrayGeo->ag_descricao";
		}


?>
                </td>
              </tr>
            </table>
<?php }
	$sql = "SELECT * FROM rel_habitat_experiencia,habitat WHERE rhe_id_habitat=ha_id AND rhe_id_experiencia=$experiencia";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) { ?>
	        <br>
			<table width="239" border="0" cellspacing="0">
              <tr bgcolor="#B4B4B4">
                <td height="1" colspan="2"> <div align="center"></div></td>
              </tr>
              <tr>
			    <td width="21"><img src="imagens/ico_bibliografia_pequeno.gif" width="17" height="16"></td>
                <td width="211"><strong>Biomas</strong></td>
              </tr>
              <tr>
                <td><img src=imagens/seta02.gif width="12" height="13"></td>
                <td>
                  <?php

		while ($arrayHab = $query->fetch_object()) {
			echo "&nbsp;$arrayHab->ha_descricao";
		}



?>
                </td>
              </tr>
            </table>
<?php } ?>		<br>
			<table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td><strong>Pessoas e Institui&ccedil;&otilde;es-
                  </strong><font size="1">clique nos &iacute;cones para conhecer
                  pessoas ou institui&ccedil;&otilde;es nestes temas</font></td>
              </tr>
              <tr bgcolor="#000000">
                <td height="1"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
<?php
$sql = "SELECT * FROM rel_area_experiencia,area_tematica WHERE rae_id_area=at_id AND rae_id_experiencia=$experiencia";
$query = $db->query($sql);
$num = $query->num_rows;

if ($num > 0) { ?>
				<table width="238" border="0" cellspacing="0" cellpadding="3">

<?php
	while ($arrayAreas = $query->fetch_object()) {
		//<td width=\"186\" valign=\"top\"><a href=\"areas_tematicas.php?areaDesc=$arrayAreas->at_descricao&area=$arrayAreas->at_id\" class=\"preto\">$arrayAreas->at_descricao</a></td>
		echo "<tr>
                <td width=\"40\" valign=\"top\" align=\"center\">
				<a href=\"usuarios.php?busca=&at=$arrayAreas->at_id\"><img border=\"0\" src=\"imagens/ico_usuarios_transp_pequeno.gif\" width=\"17\" height=\"16\"></a>&nbsp;<a href=\"instituicoes.php?busca=&at=$arrayAreas->at_id\"><img border=\"0\" src=\"imagens/ico_tabela_simples_transp_p.gif\" width=\"17\" height=\"16\"></a>
				</td>
                <td width=\"186\" valign=\"top\" class=\"textoPreto\">$arrayAreas->at_descricao</td>
               </tr>\n";

	}
	?> </table> <?php
}
else { ?>
&nbsp; <?php } ?>
                  </td>
              </tr>
            </table>
            <br>
			<?php /*
            <table width="244" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td class="textoPreto" colspan="2"><span><strong>Conhe&ccedil;a</strong></span>
                  documentos nestas &aacute;reas tem&aacute;ticas! Basta clicar
                  nas imagens abaixo.</td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000" colspan="2"> <div align="center"></div></td>
              </tr>
                  <?php
$sql = "SELECT * FROM rel_area_experiencia,area_tematica WHERE rae_id_area=at_id AND rae_id_experiencia=$experiencia";
$query = $db->query($sql);
$num = $query->num_rows;

if ($num > 0) {
	while ($arrayAreas = $query->fetch_object()) {
		echo "<tr>
                <td width=\"40\" valign=\"top\" align=\"center\">
				<a href=\"bib_pesquisa.php?tituloTodo=&at=$arrayAreas->at_id\"><img border=\"0\" src=\"imagens/ico_bibliografia_pequeno.gif\" width=\"17\" height=\"16\"></a>
				</td>
                <td width=\"186\" valign=\"top\" class=\"textoPreto\">$arrayAreas->at_descricao</td>
               </tr>\n";

	}
}
else {
	echo "<p class=\"textoPreto10px\">&nbsp;N�o h� registros!</p>";
} // fim do if que verifica se a busca retornou os resultados
?>
            </table>

          </td>
        </tr>
      </table>
	  */ ?>
<?php
if ($acao == "IM") {
?>
<script language="JavaScript">
window.print();
alert('O relatório está sendo impresso. Você será redirecionado dentro de 10 segundos.');
var delay = 10; // Delay in seconds
var targetURL = "experiencias.php?experiencia=<?php echo $experiencia; ?>"; // URl to load
setTimeout('self.location.replace(targetURL)', (delay * 1000));
</script>
<?php
	return;
}
else {
	$headers = "From: Agroecologia em Rede <sisgea@cnip.org.br>\n";
	$headers .= "X-Sender: <@cnip.org.br>\n";
	$headers .= "X-Mailer: PHP\n";
	$headers .= "X-Priority: 0\n";
	$headers .= "Return-Path: <cnip@cnip.org.br>\n";
	$headers .= "Content-Transfer-Encoding: 8bit\n";
	$headers .= "MIME-Version: 1.0\n";
	$headers .= "Content-Type: text/html; charset=iso-8859-1" ;

	$RELATORIO = "<font face=\"verdana,arial\" size=\"2\">\n";
	if (strlen(trim($nome)) > 0) {
		$RELATORIO .= "<b>$nome</b>,<br><br>";
	}
	$RELATORIO .= "Clique <a href=\"http://www.cnip.org.br/agrorede/experiencias.php?experiencia=$experiencia\" target=\"_blank\">aqui</a> ";
	$RELATORIO .= "para acessar a ficha da experiência <strong>$tituloExperiencia</strong> no <strong>Agroecologia em Rede</strong>.";
	$RELATORIO .= "<br><br>";
	$RELATORIO .= "Esta mensagem foi enviada por <strong>$remet</strong> ";
	$RELATORIO .= "atrav�s do sistema <a href=\"http://www.cnip.org.br/agrorede\">Agroecologia em Rede</a>";

	/* $mail = mail($email, "RELAT�RIO Agroecologia em Rede",$RELATORIO,$headers); */
	$mail = false;
	if ($mail == FALSE) {
?>
		<script language="JavaScript">
			alert('Houve um problema no envio do e-mail para o cliente!');
			window.location.href='experiencias.php?experiencia=<?php echo $experiencia; ?>&emailEnviado=1';
		</script>
<?php

	} // fim do if que verifica se o e-mail foi enviado
	else {
?>
		<script language="JavaScript">
			alert('Sua mensagem foi enviada com sucesso!');
			window.location.href='experiencias.php?experiencia=<?php echo $experiencia; ?>&emailEnviado=1';
		</script>
<?php
	}

} // Fim do if que verifica se o relatório deve ser impresso ou enviado por e-mail
?>
	</td>
  </tr>
</table>
</body>
</html>
