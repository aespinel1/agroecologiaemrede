<?php
if (!isset($_COOKIE['cookieUsuario']) || empty($_COOKIE['cookieSession'])) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");


$sql = "SELECT * FROM texto WHERE tex_id='EXPLICACAO_ALTERACAO'";
$query = $db->query($sql);
$rowRes = $query->fetch_object();

$sql = "SELECT * FROM texto WHERE tex_id='MSG_ALTERACAO_PESSOA'";
$query = $db->query($sql);
$rowMsg = $query->fetch_object();

?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function criticaFormUsuario() {

	nome = formCU.nome.value;
	email = formCU.email.value;

	if (nome.length == 0) {
		alert('Preencha um nome v�lido para o usuário!');
		formCU.nome.focus();
		return;
	}


	if (email.length == 0) {
		alert('Preencha um e-mail v�lido para o usuário!');
		formCU.email.focus();
		return;
	}

	formCU.submit();

}



function insereInstituicao() {
	formInst.action = 'xt_usuario_insere_instituicao.php';
	formInst.submit();
}

function insereArea() {
	formTema.action = 'xt_usuario_insere_area.php';
	formTema.submit();
}

function insereGeo() {
	formGeo.action = 'xt_usuario_insere_geo.php';
	formGeo.submit();
}

function excluiEndereco(desc,id) {
	usr = formEnd.usuario.value;
	if(confirm('Você tem certeza que deseja excluir o endereço de ' + desc + ' para este usuário?')) {
		window.location.href='xt_usuario_exclui_endereco.php?usuario='+usr+'&id='+id;
	}
}

function excluiTelefone(desc,id) {
	usr = formEnd.usuario.value;
	if(confirm('Você tem certeza que deseja excluir o telefone de ' + desc + ' para este usuário?')) {
		window.location.href='xt_usuario_exclui_telefone.php?usuario='+usr+'&id='+id;
	}
}

function excluiInstituicao(desc,id) {
	usr = formInst.usuario.value;
	if(confirm('Você tem certeza que deseja excluir a instituição ' + desc + ' para este usuário?')) {
		window.location.href='xt_usuario_exclui_instituicao.php?usuario='+usr+'&inst='+id;
	}
}

function excluiArea(desc,id) {
	usr = formTema.usuario.value;
	if(confirm('Você tem certeza que deseja excluir a �rea tem�tica ' + desc + ' para este usuário?')) {
		window.location.href='xt_usuario_exclui_area.php?usuario='+usr+'&tema='+id;
	}
}

function excluiGeo(desc,id) {
	usr = formGeo.usuario.value;
	if(confirm('Você tem certeza que deseja excluir a �rea geogr�fica ' + desc + ' para este usuário?')) {
		window.location.href='xt_usuario_exclui_geo.php?usuario='+usr+'&geo='+id;
	}
}

function excluiUsuario(usr) {
	if(confirm('Você tem certeza que deseja excluir o usuário ' + usr + '?')) {
		window.location.href='xt_usuario_exclui_usuario.php?usuario='+usr;
	}
}
</script>

<?php include("menu_geral.php"); ?>
<table width="570" height="1800" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>

    <td width="570" valign="top"> <table width="558" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><img src="imagens/ico_usuarios.gif" width="34" height="32"></p></td>
          <td width="477" bgcolor="#80AAD2"> <p><strong>Pessoas - Altera&ccedil;&atilde;o</strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
</table>

<?php


		$sql = "SELECT * FROM usuario where (cod_usuario = '$cookieUsuario' or us_login =  '$cookieUsuario')  and (us_liberacao  = 'P' or us_liberacao = 'N' or us_liberacao  = 'E')  ORDER BY us_nome";
		$query = $db->query($sql);
		$QtdRec = $query->num_rows;
?>
 </p>
<table width="550" align="center">
<tr><td><div align="justify" class="style6"><?php echo $rowRes->tex_texto; ?></div></td>
</tr>
</table>
<br>
<?php if ($QtdRec > 0) {  ?>
<form name="form1" method="post" action="usuarios_cadastro.php">
<table width="570" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td colspan="2"> <p> <strong>Selecione a pessoa </strong>
                </p></td>
          </tr>
          <tr>
            <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td width="0"> <div align="center"></div></td>
            <td width="547"> <select name="usuario">
<?php

		while ($row = $query->fetch_object()) {
			$varSelected = "";
			if ($usuario == trim($row->us_login)) {
				$varSelected = "selected";
			}
?>
                <option <?php echo $varSelected; ?> value="<?php echo trim($row->us_login); ?>"><?php echo trim($row->us_nome); ?></option>
                <?php
		} // la�o do loop
?>
              </select>
              <input name="Submit" type="submit" class="botaoLogin" value="   Selecionar   ">
            </td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="2"> <div align="center"></div></td>
          </tr>
        </table>
	  </form>
<?php } else { ?>
<p align="center" class="style7" ><?php echo $rowMsg->tex_texto; ?></p>
<?php }
		if (isset($usuario)) {
			$sql = "SELECT a.*, date_format(dt_criacao,'%d/%m/%Y') as data_inclusao , date_format(dt_atualizacao,'%d/%m/%Y') as data_alteracao FROM usuario a WHERE us_login='$usuario'";
			$query = $db->query($sql);
			$rowUSR = $query->fetch_object();
?>
      <form name="formCU" method="post" action="xt_usuario_atualiza.php">
	  <input type="hidden" name="login" value="<?php echo trim($rowUSR->us_login); ?>">
        <table width="570" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td colspan="4"> <p>                <?php if (isset($atualiza)) { ?>
                <span class="style1"> Dados da pessoa <?php echo $usuario; ?> atualizados
                com sucesso! </span>
                <?php } ?>
              </p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		   <tr bgcolor="#FFFFCC">
            <td colspan="4" align="center">Data inclusão: <?php echo $rowUSR->data_inclusao; ?> &nbsp;&nbsp;
			                Data da última alteração :  <?php echo $rowUSR->data_alteracao; ?></td>
          </tr>
          <tr>
            <td><div align="right">Nome de usuário (mínimo de 4 caracteres):</div></td>
            <td colspan="4"><input name="nome" type="text" id="login" size="89" maxlength="255" value="<?php echo $rowUSR->us_nome; ?>"></td>
          </tr>
		  <tr>
            <td><div align="right">Nome completo:</div></td>
            <td colspan="4"><input name="nome" type="text" id="nome" size="89" maxlength="255" value="<?php echo $rowUSR->us_nome; ?>"></td>
          </tr>
          <tr>
            <td ><div align="right">E-mail:</div></td>
            <td colspan="4"><input name="email" type="text" id="email3" size="89" maxlength="255" value="<?php echo $rowUSR->us_email; ?>"></td>
          </tr>
          <tr>
            <td> <div align="right">Website:</div></td>
            <td colspan="4"><input name="website" type="text" id="website" size="89" maxlength="255" value="<?php echo $rowUSR->us_website; ?>"></td>
          </tr>
          <tr>
            <td> <div align="right">Telefones:</div></td>
            <td colspan="4">              <span>
              <input name="fone" type="text" id="fone3" size="89" maxlength="255" value="<?php echo $rowUSR->txt_telefone; ?>">
            </span></td>
          </tr>
          <tr>
            <td> <div align="right">Endereço:</div></td>
            <td colspan="4"><input name="endereco" type="text" size="89" maxlength="255" value="<?php echo $rowUSR->txt_endereco; ?>"></td>
          </tr>
          <tr>
            <td> <div align="right">Cidade:</div></td>
            <td width="158"> <input name="cidade" type="text" maxlength="255" value="<?php echo $rowUSR->txt_cidade; ?>">
            </td>
            <td > <div align="right">Estado:</div></td>
            <td width="208" ><input name="estado" type="text" maxlength="255" value="<?php echo $rowUSR->txt_estado; ?>"></td>
          </tr>
          <tr>
            <td> <div align="right">CEP/Zip:</div></td>
            <td><input name="cep" type="text" maxlength="10" value="<?php echo $rowUSR->txt_cep; ?>"></td>
            <td> <div align="right">Pa&iacute;s:</div></td>
            <td ><input name="pais" type="text" maxlength="80" value="<?php echo $rowUSR->txt_pais; ?>"></td>
          </tr>

          <tr bgcolor="#000000">
            <td height="1" colspan="5" valign="top"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4"><div align="center">
                <input type="button" class="botaoLogin" value="   Atualizar    " onClick="criticaFormUsuario();">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

              </div></td>
          </tr>
          <tr bgcolor="#000000">
            <td height="1" colspan="4"> <div align="center"></div></td>
          </tr>
        </table>
      </form>
	    <br>
	<form name="formTema" method="post">
		<input type="hidden" name="usuario" value="<?php echo $usuario; ?>">
        <table width="570" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td> <p> <strong>Áreas Temáticas</strong>&nbsp;&nbsp;
                [ <a href="usuarios_cadastro.php?usuario=<?php echo $usuario; ?>&novaArea=1" class="Preto">Nova Área Temática</a>
                ]</p>
			</td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
<?php
		if (isset($novaArea)) {
?>
          <tr>
            <td >
		 <select name="tema" id="tema">
<?php
			function untree($parent, $level) {
				$parentsql = $db->query("SELECT * FROM area_tematica WHERE at_pertence=$parent ORDER BY at_descricao");
			    if (!$parentsql->num_rows) {
				    return;
			    }
	    		else {
			        while($branch = $parentsql->fetch_object()) {
	    		        $echo_this = "<option value=\"$branch->at_id\">";
		                for ($x=1; $x<=$level; $x++) {
	    		            $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;";
		                }
						$echo_this .= "- $branch->at_descricao</option>";
			            echo $echo_this;
	    		        $rename_level = $level;
		    	        untree($branch->at_id, ++$rename_level);
        		    }
		        }
		    }

			$sql = "SELECT * FROM area_tematica WHERE at_pertence = 0 ORDER BY at_descricao";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->at_id; ?>"><?php echo trim($row->at_descricao); ?></option>
<?php
			    untree($row->at_id, 1);
			} // la�o do loop
?>
              </select>
			</td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td ><div align="center">
                <input type="button" class="botaoLogin" onClick="insereArea();" value="   Inserir �rea Tem�tica   ">
              </div></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000"> <div align="center"></div></td>
          </tr>

<?php
		} // fim do que verifica se foi clicado para um novo endereço

		$sql = "SELECT * FROM rel_area_usuario,usuario,area_tematica WHERE rau_id_usuario=us_login AND ".
			   "rau_id_area=at_id AND rau_id_usuario='$usuario' ORDER BY at_descricao";
		$query = $db->query($sql);
		$i = 0;
		while ($row = $query->fetch_object()) {
?>
		<input type="hidden" name="areaID<?php echo $i; ?>" value="<?php echo trim($row->at_id); ?>">
          <tr>
            <td><strong><?php echo trim($row->at_descricao); ?></strong> [ <a href="#" onClick="excluiArea('<?php echo trim($row->at_descricao); ?>','<?php echo trim($row->at_id); ?>');" class="Preto">Excluir</a> ] </td>
          </tr>
<?php
			$i++;
		} // la�o do loop
?>
          <tr bgcolor="#000000">
            <td height="1"> <div align="center"></div></td>
          </tr>
        </table>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
	  </form>
		<br>
	<form name="formGeo" method="post">
		<input type="hidden" name="usuario" value="<?php echo $usuario; ?>">
        <table width="570" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td > <p> <strong>Áreas Geográficas</strong>&nbsp;&nbsp;
                [ <a href="usuarios_cadastro.php?usuario=<?php echo $usuario; ?>&novaGeo=1" class="Preto">Nova �rea Geogr�fica</a>
                ]</p></td>
          </tr>
          <tr>
            <td height="1"  bgcolor="#000000"> <div align="center"></div></td>
          </tr>
<?php
		if (isset($novaGeo)) {
?>
          <tr>
            <td>
		 <select name="geo" id="tema">
<?php
			function untree($parent, $level) {
				$parentsql = $db->query("SELECT * FROM area_geografica WHERE ag_pertence=$parent ORDER BY ag_descricao");
			    if (!$parentsql->num_rows) {
				    return;
			    }
	    		else {
			        while($branch = $parentsql->fetch_object()) {
	    		        $echo_this = "<option value=\"$branch->ag_id\">";
		                for ($x=1; $x<=$level; $x++) {
	    		            $echo_this .=  "&nbsp;&nbsp;&nbsp;&nbsp;";
		                }
						$echo_this .= "- $branch->ag_descricao</option>";
			            echo $echo_this;
	    		        $rename_level = $level;
		    	        untree($branch->ag_id, ++$rename_level);
        		    }
		        }
		    }

			$sql = "SELECT * FROM area_geografica WHERE ag_pertence = 0 ORDER BY ag_descricao";
			echo $sql;
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->ag_id; ?>"><?php echo trim($row->ag_descricao); ?></option>
<?php
			    untree($row->ag_id, 1);
			} // la�o do loop
?>
              </select>
			</td>
          </tr>
          <tr>
            <td height="1"  bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td><div align="center">
                <input type="button" class="botaoLogin" onClick="insereGeo();" value="   Inserir �rea Geogr�fica   ">
              </div></td>
          </tr>
          <tr>
            <td height="1"  bgcolor="#000000"> <div align="center"></div></td>
          </tr>

<?php
		} // fim do que verifica se foi clicado para uma nova AG

		$sql = "SELECT * FROM rel_geo_usuario,usuario,area_geografica WHERE rgu_id_usuario=us_login AND ".
			   "rgu_id_geo=ag_id AND rgu_id_usuario='$usuario' ORDER BY ag_descricao";
		$query = $db->query($sql);
		$i = 0;
		while ($row = $query->fetch_object()) {
?>
		<input type="hidden" name="geoID<?php echo $i; ?>" value="<?php echo trim($row->ag_id); ?>">
          <tr>
            <td><strong><?php echo trim($row->ag_descricao); ?></strong> [ <a href="#" onClick="excluiGeo('<?php echo trim($row->ag_descricao); ?>','<?php echo trim($row->ag_id); ?>');" class="Preto">Excluir</a> ] </td>
          </tr>
<?php
			$i++;
		} // la�o do loop
?>
          <tr bgcolor="#000000">
            <td height="1"> <div align="center"></div></td>
          </tr>
        </table>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
	  </form>
		<br>
	<form name="formInst" method="post">
		<input type="hidden" name="usuario" value="<?php echo $usuario; ?>">
        <table width="570" border="0" cellpadding="5" cellspacing="1" align="center">
          <tr>
            <td > <p> <strong>Institui��es</strong>&nbsp;&nbsp;
                [ <a href="usuarios_cadastro.php?usuario=<?php echo $usuario; ?>&novaInstituicao=1" class="Preto">Nova instituição</a>
                ]</p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
<?php
		if (isset($novaInstituicao)) {
?>
          <tr width="560">
            <td >
		 <select name="inst" >
<?php
			$sql = "SELECT * FROM frm_instituicao where in_nome is not null and in_nome <> '  ' ORDER BY in_nome";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
?>
                <option value="<?php echo $row->in_id; ?>"><?php echo trim($row->in_nome); ?></option>
                <?php
			} // la�o do loop
?>
              </select>
			</td>
          </tr>
          <tr>
            <td height="1"  bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td ><div align="center">
                <input type="button" class="botaoLogin" onClick="insereInstituicao();" value="   Inserir Instituição   ">
              </div></td>
          </tr>
          <tr>
            <td height="1"  bgcolor="#000000"> <div align="center"></div></td>
          </tr>

<?php
		} // fim do que verifica se foi clicado para um novo endereço

		$sql = "SELECT * FROM rel_inst_usuario,usuario,frm_instituicao WHERE riu_id_usuario=us_login AND ".
			   "riu_id_inst=in_id AND riu_id_usuario='$usuario' ORDER BY in_nome";
		$query = $db->query($sql);
		$i = 0;
		while ($row = $query->fetch_object()) {
?>
		<input type="hidden" name="instID<?php echo $i; ?>" value="<?php echo trim($row->in_id); ?>">
          <tr>
            <td ><strong><?php echo trim($row->in_nome); ?></strong> [ <a href="#" onClick="excluiInstituicao('<?php echo trim($row->in_nome); ?>','<?php echo trim($row->in_id); ?>');" class="Preto">Excluir</a> ] </td>
          </tr>
<?php
			$i++;
		} // la�o do loop
?>
          <tr bgcolor="#000000">
            <td height="1"> <div align="center"></div></td>
          </tr>
        </table>
		<input type="hidden" name="qtItens" value="<?php echo $i; ?>">
	  </form>
		<br>
<?php } ?>
</table>

</body>
</html>
<?php
$db->close();
?>
