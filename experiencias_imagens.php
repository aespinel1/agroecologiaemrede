<?php
if (strlen($cookieUsuarioPub) == 0) {
	echo "Você não tem permissão para acessar esta página";
	return;
}
include("conexao.inc.php");

$sql = "SELECT * FROM funcao_usuario,usuario WHERE us_login=fu_login AND us_session='$cookieSessionPub' AND fu_login='$cookieUsuarioPub' AND (fu_id='040000' OR fu_id='040400') AND us_grupo=1";
$query = $db->query($sql);
$numPerm = $query->num_rows;
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function excluiRelacionamento(arquivo) {
	if(confirm('Você tem certeza que deseja excluir a imagem ' + arquivo + '?')) {
		window.location.href='xt_exp_exclui_imagem.php?idExp=<?php echo $experiencia; ?>&imagem='+arquivo;
	}
}

function criticaFormAltera(action) {
	form1.action = action;
	form1.submit();
}
//-->
</script>
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr>
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57">
      <div align="center"><a class="Branco" href="xt_logout.php"><img src="imagens/ico_logoff.gif" border="0"><br><br>
        <strong>LOGOUT</strong></a></div>
	</td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr>
    <td width="173" valign="top">
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Experi&ecirc;ncias</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr>
          <td colspan="2"><p><strong>Op��es</strong></p></td>
        </tr>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td width="283"><img src="imagens/seta02.gif" width="12" height="13">
            <a href="cadastro_experiencias.php" class="Preto">Cadastro de Experi&ecirc;ncia</a><br>
            <br> <img src="imagens/seta02.gif" width="12" height="13"> <a href="cadastro_experiencias_alterar.php" class="Preto">Alterar
            dados</a></td>
          <td width="283" valign="top"><img src="imagens/seta02.gif" width="12" height="13">
            <a href="experiencias_arquivos.php" class="Preto">Gravar arquivos</a><br>
            <br> <img src="imagens/seta02.gif" width="12" height="13"> <a href="experiencias_imagens.php" class="Preto">Gravar
            imagens</a></td>
        </tr>
      </table>
      <?php
if ($numPerm == 0) {
	echo "<p align=\"center\" class=\"textoPreto10px\">Você não tem acesso a estas informações!</p>";
}
else {
?>
	  <form name="form1" method="post">
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
          <td height="24" colspan="4">
            <p> <strong>Gravar Imagem</strong></p></td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td colspan="4">
			 Experi&ecirc;ncia:
              <select name="experiencia">
<?php
				$sql = "SELECT * FROM experiencia ORDER BY ex_descricao";
				$query = $db->query($sql);
				while ($row = $query->fetch_object()) {
					$desc = trim($row->ex_descricao);
					$varSelected = "";
					if (isset($experiencia) && $experiencia == $row->ex_id) {
						$varSelected = "selected";
					}
					echo "<option value=\"$row->ex_id\" $varSelected>$desc</option>";
				}
?>
              </select> &nbsp;&nbsp;&nbsp; <input type="button" class="botaoLogin" onClick="criticaFormAltera('experiencias_imagens.php');" value="   BUSCAR   ">
            </td>
          </tr>
          <tr>
            <td height="1" colspan="4" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
		</table>
	  </form>
<?php
	if (isset($experiencia)) {

		$sql = "SELECT * FROM experiencia_imagem WHERE ei_id_experiencia=$experiencia";
		$query = $db->query($sql);
?>
    <form name="form2" enctype="multipart/form-data" method="post" action="xt_exp_grava_imagem.php">
	<input type="hidden" name="id" value="<?php echo $experiencia; ?>">
      <table width="567" border="0" align="center" cellpadding="5" cellspacing="1">
        <tr>
          <td colspan="2"> <p>Imagens cadastradas para
              esta Experiência</td>
        </tr>
        <tr>
          <td height="1" bgcolor="#000000" colspan="2"> <div align="center"></div></td>
        </tr>
          <tr>
            <td colspan="2">Gravar nova imagem:
              <input name="arquivo" type="file" size="50">
              <input name="Submit" type="submit" value="Gravar"> </td>
        </tr>
        <tr>
          <td height="1" bgcolor="#000000" colspan="2"><div align="center"></div></td>
        </tr>
<?php


$nomeImg = "";
while ($row = $query->fetch_object()) {
	$nomeImg = substr(trim($row->ei_imagem),0,-20);
?>
        <tr>
          <td width="20"> <div align="center"><a href="#" onClick="excluiRelacionamento('<?php echo $row->ei_imagem; ?>');"><img src="imagens/trash.gif" border="0"></a></div></td>
          <td width="547"> <?php echo $nomeImg . " - " . my_filesize("admin/IMG/".$row->ei_imagem); ?>
          </td>
        </tr>
        <?php
}
?>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000">
            <div align="center"></div></td>
        </tr>
      </table>
     </form>
<?php
	} // fim do if que verifica se uma experiência foi buscada

} // fim do if que verifica se o usuário tem acesso a estas informações
?>


	  </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
