<?php
include("conexao.inc.php");

function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) {
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function enviaEmail(url) {
	var email = prompt("Digite um endereço de e-mail:","");
	if (email != null && email != "") {
		url = url + '&email=' + email;
		window.location.href=url;
	}
}

function mOvr(src,clrOver) {
  if (!src.contains(event.fromElement)) {
      src.style.cursor = 'hand';
      src.bgColor = clrOver;
  }
}
function mOut(src,clrIn) {
  if (!src.contains(event.toElement)) {
     src.style.cursor = 'default';
     src.bgColor = clrIn;
  }
}
function gotourl(url) {
  window.location.href=url;
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
</script>


<?php
include("menu_geral.php");

	$sql = "SELECT * FROM frm_instituicao WHERE in_id=$inst";
	//$sql = "SELECT a.*, b.nome as cidade, b.UF FROM frm_instituicao as a, _BR as b WHERE a.ei_cidade=b.id AND in_id=$inst";
	$query = $db->query($sql);
	$rowUSR = $query->fetch_object();

	if (substr($rowUSR->ei_cidade,6,1) != '.') {
		// invalid city!!
		$geo = null;
		$cidadeUF = $rowUSR->ei_cidade." - ".$rowUSR->ei_estado;
		$pais = $rowUSR->ei_pais;
	} else {
		$pais = substr($rowUSR->ei_cidade,0,2);
		$sql = "SELECT a.nome as cidade, b.nome as UF, c.nome as pais FROM _".$pais." as a, _".$pais."_maes as b, __paises as c WHERE a.id_mae=b.id AND c.id='".$pais."' AND a.id='".$rowUSR->ei_cidade."'";
		$query = $db->query($sql);
		$geo = $query->fetch_object();
		$cidadeUF = $geo->cidade."/".$geo->UF;
		$pais = $geo->pais;
	}


?>
<table width="589" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="579" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_simples.gif" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong><?=$rowUSR->in_nome?></strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>

      <table width="567" border="0" cellspacing="0" cellpadding="3">
        <tr>
          <td width="311" valign="top">
		  	<table width="100%" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td class="tabela_bordas_bottom"><strong>Dados Cadastrais</strong></td>
              </tr>
              <tr>
                <td>
				<p>
<?php
	if (strlen(trim($rowUSR->in_email)) > 0) {
		echo "E-mail: <a href=\"mailto:$rowUSR->in_email\" class=\"Preto\">$rowUSR->in_email</a><br><br>\n";
	}
	if (strlen(trim($rowUSR->in_url)) > 0) {
		echo "Website: <a href=\"http://$rowUSR->in_url\" class=\"Preto\">$rowUSR->in_url</a><br><br>\n";
	}

?>



				</p>
				</td>
              </tr>
              <tr>
                <td class="tabela_bordas"><strong>Endere&ccedil;o</strong></td>
              </tr>
              <tr>
                <td>
				<p>
<?php
	/*$sql = "SELECT a.*, b.nome as cidade, b.UF FROM frm_instituicao as a, _BR as b WHERE a.ei_cidade=b.id AND in_id=$inst";
	$query = $db->query($sql);
	$numEnd = $query->num_rows;*/
		echo "<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"5\" cellspacing=\"1\">";
			echo "<tr bgcolor=\"#D8D8D8\"> \n";
		    echo "<td align=\"center\" valign=\"top\" bgcolor=\"#D8D8D8\" class=\"textoPreto10px\"><strong>&nbsp;</strong></td>\n";
			echo "<td bgcolor=\"#D8D8D8\" class=\"textoPreto10px\">\n";
			echo "<strong>Endereço</strong>:<br>$rowUSR->ei_endereco<br>\n";
			echo "<strong>Cidade / Estado</strong>:<br>$cidadeUF<br>\n";
			echo "<strong>CEP</strong>:<br>$rowUSR->ei_cep<br>\n";
			echo "<strong>País</strong>:<br>$pais\n";
            echo "<br><strong>Telefone</strong>:<br>$rowUSR->ei_telefone\n";
			echo "</td>\n";
			echo "</tr>\n";
		echo "</table>";

?>
					</p>
				</td>
              </tr>
            </table>
		  </td>
          <td width="256" valign="top">
			<table width="250" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="250" class="tabela_bordas_bottom"><strong>Experi&ecirc;ncias
                  Relacionadas</strong></td>

              </tr>
              <tr>
                <td> <?php

	$sql = "SELECT * FROM frm_instituicao,rel_inst_experiencia,experiencia WHERE in_id=$inst AND in_id=rie_id_inst ".
		   "AND ex_id=rie_id_experiencia AND ex_liberacao='S'  order by ex_chamada";

	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {
		/* echo "<p class=\"textoPreto10px\">Foram encontrados as seguintes experiências para a instituição <b>\"$instDesc\"</b>: */ echo "<br><br>"; /*";   */
		$i = 1;
		while ($row = $query->fetch_object()) {
			echo "$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_chamada</a><br><br>";
			$i++;
		}
		echo "</p>";
	}


	$sql = "SELECT * FROM experiencia_autor,experiencia WHERE exa_id_experiencia=ex_id AND exa_id_inst=$inst order by ex_chamada";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {

		echo "<p class=\"textoPreto10px\"><b>Autor(a):</b><br><br>";

		$i = 1;
		while ($row = $query->fetch_object()) {

			echo "$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_chamada</a><br><br>";

			$i++;

		}

		echo "</p>";

	}


	$sql = "SELECT * FROM experiencia_relator,experiencia WHERE exr_id_experiencia=ex_id AND exr_id_inst=$inst order by ex_chamada";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {

		echo "<p class=\"textoPreto10px\"><b>Relator(a):</b><br><br>";

		$i = 1;
		while ($row = $query->fetch_object()) {

			echo "$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_chamada</a><br><br>";

			$i++;

		}

		echo "</p>";

	}

?> </td>
              </tr>
              <tr>
                <td class="tabela_bordas_bottom"><strong>Experimentadores e T�cnicos</strong></td>
              </tr>
              <tr>
                <td>
                  <?php
	$sql = "SELECT * FROM rel_inst_usuario,usuario WHERE riu_id_usuario=us_login AND riu_id_inst=$inst";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		while ($arrayAreas = $query->fetch_object()) {
			echo "<img src=\"imagens/seta02.gif\"> <a href=\"usuarios.php?usuNome=$arrayAreas->us_nome&usu=$arrayAreas->us_login\" class=\"preto\">$arrayAreas->us_nome</a><br>";
		}
	}
?>
                </td>
              </tr>

              <tr>
                <td class="tabela_bordas_bottom"><strong>&Aacute;reas Tem&aacute;ticas</strong></td>
              </tr>
              <tr>
                <td>
<?php
	$sql = "SELECT at_id, at_descricao FROM frm_instituicao as a, area_tematica as b WHERE LOCATE(at_id,a.in_areas_tematicas)>0 AND in_id=".$inst;
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0)
		while ($arrayAreas = $query->fetch_object()) {
			//echo "<img src=\"imagens/seta02.gif\"> <a href=\"xt_adiciona_agat.php?url=banco_experiencias&tipo=at&id=$arrayAreas->at_id\" class=\"preto\">$arrayAreas->at_descricao</a><br>";
			?>
			<img src="imagens/seta02.gif">
			<a href="mapeo/?f=consultar&c=1&tipofrm=instituicao&in_areas_tematicas=<?=$arrayAreas->at_id?>" class="preto"><?=$arrayAreas->at_descricao?></a><br />
			<?php
		}
?>
                </td>
              </tr>
<!--
              <tr>
                <td class="tabela_bordas_bottom"><strong>Abrang�ncia</strong></td>
              </tr>
              <tr>
                <td>
                  <?php
	$sql = "SELECT * FROM rel_geo_inst,area_geografica WHERE rgi_id_geo=ag_id AND rgi_id_inst=$inst";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		while ($arrayAreas = $query->fetch_object()) {
			echo "<img src=\"imagens/seta02.gif\"> <a href=\"xt_adiciona_agat.php?url=banco_experiencias&tipo=ag&id=$arrayAreas->ag_id\" class=\"preto\">$arrayAreas->ag_descricao</a><br>";
		}
	}
?>
                </td>
              </tr>
//-->
            </table>
          </td>
        </tr>
      </table>
<?php
// } // fim do if que verifica se foi clicado em algum usuário
?>
</td>
</tr>

       <tr>
          <td colspan="2"><p align="center" class="tabela_bordas"><strong>[ <a href="mapeo/?f=consultar&tipofrm=instituicao" class="Preto">Nova Busca</a> ]</strong></p></td>
        </tr>
            </table>
</body>
</html>
<?php
$db->close();
?>
