<?php

include("conexao.inc.php");

$sql = "SELECT * FROM texto WHERE tex_id='EXPLICACAO_INSTITUICAO_CTRL'";
$query = $db->query($sql);
$rowExpCTRL = $query->fetch_object();
$varDesc = "Cadastro";

$nivel=0;
$idmae = array(0=>'');
$campo = 'ex_areas_tematicas';
$arvore = resgata_arvore('area_tematica','at_id','at_descricao','at_pertence');

$arvoreJS = array();
$arvoreJS[0] = new StdClass();
$arvoreJS[0]->campo = $campo;
$arvoreJS[0]->dados = $arvore;

?>
<!DOCTYPE html>
<html>
<head>
<title><?=$txt['agrorede']?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="<?php echo $dir['apoio_URL']; ?>ajax_dd/ajax_down.js"></script>

<?php include("menu_geral.php") ?>

<script>
	$(function () {
		$("#formNE").submit(function(e) {
			var error = criticaFormUsuario();
			if (error==false) {
				return true;
			} else {
				alert(error);
				$('html, body').animate({scrollTop: $('#resto_da_tabela').offset().top-140})
				e.preventDefault();
				return false;
			}
		});
	});
	function trim(str) {
		return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}

	function criticaFormUsuario() {
		<?php
		$sql = "SELECT us_login, us_email FROM usuario";
		$query = $db->query($sql);
		while ($q = $query->fetch_object()) {
			$qs[]=trim($q->us_login);
			$qe[]=trim($q->us_email);
		}
		$logins = json_encode($qs);
		$emails = json_encode($qe);
		?>
		var logins=<?=$logins?>;
		var emails=<?=$emails?>;
		var existe=false;
		for (var i=0;i<emails.length;i++) {
			if (emails[i]==trim(email.value.replace(' ',''))) {
				existe=true;
				break;
			}
		}
		if (email.value.length == 0 || existe==true) {
			return '<?=$txt["erro_email_invalido"]?>';
		} else {
			for (var i=0;i<logins.length;i++)
			if (logins[i]==trim(login.value.replace(' ',''))) {
				existe=true;
				break;
			}
			if (login.value.length == 0 || existe==true) {
				return '<?=$txt["erro_login_invalido"]?>';
			}
		}

		return false;
	}

	var arvore = <?=json_encode($arvoreJS)?>;

</script>


<div class="container">
  <div class="row">
    <div class="col-md-12">
			<div class="alert alert-success">
      	<h1><i class="aericon-pessoas"></i> <?=$txt["frm_pessoa"]?> <span class="badge badge-light badge-sm"><?=$varDesc?></span></h1>
			</div>
    </div>
  </div>
  <div class="row">
		<div class="col-md-12">
			<div class="alert alert-info">
				<?=$txt['mysql']['MSG_ABERTURA_CADASTRO_PESSOA']->tex_texto?>
			</div>
		</div>
	</div>
	<?php if (isset($loginNovo)) { ?>
		<div class="row">
			<div class="col-md-12 alert alert-success">
				Sua identificação foi criada com sucesso, anote-a por favor: <b><?=$loginNovo?></b>. Em breve você receberá uma confirmação do seu cadastro, e receberá instruções sobre o uso do sistema.
			</div>
		</div>
	<?php } ?>
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
			<div class="card card border-primary">
				<div class="card-header">
					<h3><?=$txt['termos_de_uso']?></h3>
				</div>
				<div class="card-body pre-scrollable">
					<?=$txt['mysql']['CARTA_POLITICA']->tex_texto?>
				</div>
				<div class="card-footer">
					<div class="form-check">
				    <input type="checkbox" class="form-check-input" name="carta_politica" id="carta_politica" onClick="$('#resto_da_tabela').toggleClass('d-block','d-none');$('html, body').animate({scrollTop: $('#resto_da_tabela').offset().top-140})">
				    <label class="form-check-label" for="carta_politica">
							<?=$txt['li_e_concordei']?>
						</label>
				  </div>
				</div>
			</div>
		</div>
	</div>

	<div id="resto_da_tabela" class="d-none row mt-4">
		<div class="col-sm-12 col-md-12 col-lg-8 offset-lg-2">
			<div class="card border-primary">
				<div class="card-header">
					<h3>Identificação</h3>
				</div>
				<div class="card-body">
					<form id="formNE" name="formNE" method="post" action="xt_usuario_insere_usuario.php" class="needs-validation">
						<div class="alert alert-info">
							<strong>Atenção</strong>: O formulário abaixo é apenas para você se cadastrar INDIVIDUALMENTE, ou seja, não se trata de uma inscrição institucional. Depois de ter se cadastrado como pessoa física, você poderá cadastrar novas instituições que queira inserir no sistema. Em outras palavras: aqui é apenas o caminho para você acessar individualmente o sistema, e não para cadastrar novas instituições!
						</div>

						<div class="form-group row border-bottom pb-2">
							<label for="nome" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
								<?=$txt['frm_nome']?>
							</label>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<input type="text" class="form-control" name="nome" id="nome"  value="<?=$nome?>" required>
							</div>
						</div>

						<div class="form-group row border-bottom pb-2">
							<label for="email" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
								<?=$txt['frm_email']?>
							</label>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<input class="form-control" name="email" type="email" id="email" required>
							</div>
						</div>

						<div class="form-group row border-bottom pb-2">
							<label for="website" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
								<?=$txt['frm_www']?>
							</label>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<input class="form-control" name="website" type="text" id="website" >
							</div>
						</div>

						<div class="form-group row border-bottom pb-2">
							<label for="fone3" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
								<?=$txt['frm_tel']?>
							</label>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<input class="form-control" name="fone" type="text" id="fone3">
							</div>
						</div>

						<div class="form-group row border-bottom pb-2">
							<label for="endereco" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
								<?=$txt['frm_endereco']?>
							</label>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<input class="form-control" name="endereco" type="text" id="endereco">
							</div>
						</div>

						<div class="form-group row border-bottom pb-2">
							<label for="cep" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
								<?=$txt['frm_CEP']?>
							</label>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<input class="form-control" name="cep" type="text" maxlength="10" id="cep">
							</div>
						</div>

						<div class="form-group row border-bottom pb-2">
							<label for="ei_cidade_0" class="col-sm-12 col-md-4 col-lg-3 col-form-label form-control-label text-truncate">
								País
							</label>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<select data-style="btn-light text-dark" name="pais" onchange="javascript:Ajax.Request('method=getGeorref&amp;nivel=0&amp;primeiro=--Selecione--&amp;valor='+this.value+'', Ajax.Response, 'ei_cidade_1', ['ei_cidade'],'carregando_ei_cidade');" id="ei_cidade_0" class="form-control selectpicker">
									<option value="">---</option>
									<?=faz_select($bd['refs'].'.__paises', 'id', '', 'nome', 'nome', $where_select_paises, true)?>
								</select>
							</div>
						</div>
						<div class="form-group row border-bottom pb-2">
							<label for="ei_cidade_1" class="col-sm-12 col-md-4 col-lg-3 col-form-label form-control-label text-truncate">
								Estado
							</label>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<select data-style="btn-light text-dark" name="estado" onchange="javascript:Ajax.Request('method=getGeorref&amp;nivel=1&amp;valor='+this.value+'&amp;primeiro=--Selecione--', Ajax.Response, 'ei_cidade',false,'carregando_ei_cidade');" id="ei_cidade_1" class="form-control selectpicker">
									<option value="" selected="selected">---</option>
								</select>
							</div>
						</div>
						<div class="form-group row border-bottom pb-2">
							<label for="ei_cidade" class="col-sm-12 col-md-4 col-lg-3 col-form-label form-control-label text-truncate">
								Município
							</label>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<select data-style="btn-light text-dark" name="cidade" id="ei_cidade" class="form-control selectpicker">
									<option value="" selected="selected">---</option>
								</select>
							</div>
						</div>

						<div class="form-group row border-bottom pb-2">
							<label for="login" class="col-sm-12 col-md-4 col-lg-3 col-form-label">
								<?=$txt['frm_login']?>
							</label>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<input class="form-control" name="login" type="text" id="login"  value="<?=$login?>" required>
								<small id="loginHelp" class="form-text text-muted"><?=$txt['ajuda_identificacao']?></small>
							</div>
						</div>

						<div class="form-group row border-bottom pb-2">
							<div class="col-sm-12 col-md-4 col-lg-3 col-form-label form-control-label">
								Área(s) Temática(s)
							</div>
							<div class="col-sm-12 col-md-8 col-lg-9">
								<ul id="tree_<?=$campo?>">
									<?php foreach ($arvore as $i=>$no) { ?>
										<?php
										$filhos=false;
										foreach ($arvore as $arv) {
											if (in_array($no->at_id,explode('|',$arv->parentes))) {
												$filhos=true;
												break;
											}
										}
										if ($no->nivel>$nivel) {
											$nivel=$no->nivel;
											$idmae[$nivel] = $arvore[$i-1]->at_id;
											$hidden = ($no->nivel>0)
												? ' style="display:none"'
												: '';
											echo '<div id="div_'.$campo.'_'.$idmae[$nivel].'"'.$hidden.'>';
										} elseif ($no->nivel<$nivel) {
											echo str_repeat('</div>',$nivel-$no->nivel);
											$nivel=$no->nivel;
										} ?>
										<li class="tree-item list-unstyled">
											<?=str_repeat('<span class="mr-4 border-left border-info"></span>',$nivel)?>
											<input name="<?=$campo?>[]" value="<?=$no->at_id?>" id="<?=$campo.'_'.$no->at_id?>" type="checkbox">
											<?php if($filhos) { ?>
												<span id="s<?=$campo.'_'.$no->at_id?>" onclick="alterna_visao_arvore('<?=$campo.'_'.$no->at_id?>')" class="btn btn-info btn-xs"><i class="oi oi-plus"></i></span>
											<?php } ?>
											<label for="<?=$campo.'_'.$no->at_id?>"><?=$no->at_descricao?></label>
										</li>
									<?php } ?>
									<?php for ($i=0;$i<$nivel;$i++) { ?>
										</div>
									<?php } ?>
								</ul>
							</div>
						</div>

						<div class="form-group row border-bottom pb-2">
							<div class="col-md-12 text-center">
								<input type="submit" class="btn btn-success btn-lg" value="Cadastrar">
							</div>
						</div>

					</form>
				</div>
			</div>
		</div>
	</div>

</body>
</html>
<?php
$db->close();
?>
