<?php
include "mapeo/config.php";
header("Content-type: text/javascript; charset: UTF-8");
$lingua = (isset($_GET['lingua']))
	? $_GET['lingua']
	: 'pt-br';
$u = isset($_GET['U']) ? $_GET['U'] : '';
$a = isset($_GET['A']) ? $_GET['A'] : '';
$referer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
$largura = ($a) ? 125 : 140;
require_once "mapeo/apoio/textos/".$lingua.".php";

$txtREFERER = ($pospto = strpos($referer,'?'))
	? str_replace($dir['base_URL'],'',substr($referer,0,$pospto))
	: $referer;
$txtREFERER = str_replace($dir['base_URL'],'',$txtREFERER);
$txtAlvo = $txtREFERER . '?' . $_SERVER['QUERY_STRING'];
?>
// alert('<?=$txtAlvo?>');
// alert('<?=json_encode($_SERVER)?>');
// Milonic DHTML Menu version 3.3
// Please note that major changes to this file have been made and is not compatible with earlier versions..
//
// You no longer need to number your menus as in previous versions.
// The new menu structure allows you to name the menu instead. This means that you to remove menus and not break the system.
// The structure should also be much easier to modify, add & remove menus and menu items.
//
// If you are having difficulty with the menu please read the FAQ at http://www.milonic.co.uk/menu/faq.php before contacting us.
//
// Please note that the above text CAN be erased if you wish.


// Milonic DHTML Menu version 3.2.0
// The following line is critical for menu operation, and must appear only once.
menunum=0;menus=new Array();_d=document;function addmenu(){menunum++;menus[menunum]=menu;}function dumpmenus(){mt="<script language=javascript>";for(a=1;a<menus.length;a++){mt+=" menu"+a+"=menus["+a+"];"}mt+="<\/script>";_d.write(mt)}
//Please leave the above line intact. The above also needs to be enabled.

////////////////////////////////////
// Editable properties START here777777 //
////////////////////////////////////

timegap=500					// The time delay for menus to remain visible
followspeed=5				// Follow Scrolling speed
followrate=20				// Follow Scrolling Rate
suboffset_top=9;			// Sub menu offset Top position
suboffset_left=20;			// Sub menu offset Left position

effect = "fade(duration=0.2);Shadow(color='#ffffcc', Direction=135, Strength=10)" // Special effect string for IE5.5 or above please visit http://www.milonic.co.uk/menu/filters_sample.php for more filters

style1=[					// style1 is an array of properties. You can have as many property arrays as you need. This means that menus can have their own style.
"000000",					// Mouse Off Font Color
"bbddbb",					// Mouse Off Background Color
"006633",					// Mouse On Font Color
"eeffcc",					// Mouse On Background Color
"000000",					// Menu Border Color
11,						    // Font Size
"normal",					// Font Style
"bold",					    // Font Weight
"Verdana,Tahoma,Helvetica",	// Font Name
4,						    // Menu Item Padding
"imagens/seta02.gif",		// Sub Menu Image (Leave this blank if not needed)
2,						    // 3D Border & Separator bar
"000000",					// 3D High Color "3666ff"
"000000",					// 3D Low Color
,							// Referer Item Font Color (leave this blank to disable)
,							// Referer Item Background Color (leave this blank to disable)
,							// Top Bar image (Leave this blank to disable)
,							// Menu Header Font Color (Leave blank if headers are not needed)
,							// Menu Header Background Color (Leave blank if headers are not needed)
]

style2=[					// style1 is an array of properties. You can have as many property arrays as you need. This means that menus can have their own style.
"000000",					// Mouse Off Font Color
"bbddbb",					// Mouse Off Background Color
"006633",					// Mouse On Font Color
"eeffcc",					// Mouse On Background Color
"000000",					// Menu Border Color
9,						    // Font Size
"normal",					// Font Style
"bold",					    // Font Weight
"Verdana,Tahoma,Helvetica",	// Font Name
4,						    // Menu Item Padding
"clientsc.gif",							// Sub Menu Image (Leave this blank if not needed)
2,						    // 3D Border & Separator bar
"000000",					// 3D High Color "3666ff"
"000000",					// 3D Low Color
,							// Referer Item Font Color (leave this blank to disable)
,							// Referer Item Background Color (leave this blank to disable)
,							// Top Bar image (Leave this blank to disable)
,							// Menu Header Font Color (Leave blank if headers are not needed)
,							// Menu Header Background Color (Leave blank if headers are not needed)

]

addmenu(menu=[				// This is the array that contains your menu properties and details
"menu_principal",      		// Menu Name - This is needed in order for the menu to be called
50,							// Menu Top - The Top position of the menu in pixels
-1,							// Menu Left - The Left position of the menu in pixels
<?=$largura?>,			    // Menu Width - Menus width in pixels
0,							// Menu Border Width
"left",		        	    // Screen Position - here you can use "center;left;right;middle;top;bottom" or a combination of "center:middle"
style1,						// Properties Array - this is set higher up, as above
1,							// Always Visible - allows the menu item to be visible at all time
"center",					// Alignment - sets the menu elements text alignment, values valid here are: left, right or center
,							// Filter - Text variable for setting transitional effects on menu activation - see above for more info
,							// Follow Scrolling - Tells the menu item to follow the user down the screen (visible at all times)
1, 							// Horizontal Menu - Tells the menu to become horizontal instead of top to bottom style
,							// Keep Alive - Keeps the menu visible until the user moves over another menu or clicks elsewhere on the page
"right",					// Position of TOP sub image left:center:right
"form",						// Type of menu use "form" or blank
,							// Right To Left - Used in Hebrew for example.
,							// Open the Menus OnClick - leave blank for OnMouseover
,							// ID of the div you want to hide on MouseOver (useful for hiding form elements)
,							// Reserved for future use
,							// Reserved for future use
,							// Reserved for future use
<?php if ($u) {
	?>
	,"<?=htmlentities($txt['menu_inicio'])?>","mapeo/index.php target=_self",,"",1
	,"<?=htmlentities($txt['menu_consultar'])?>...","show-menu=menu_consultas",,,1
	,"<?=htmlentities($txt['menu_cadastrar'])?>...","show-menu=menu_cadastros",,,1
	,"<?=htmlentities($txt['menu_alterar'])?>...","show-menu=menu_alteracoes",,,1
	,"<?=htmlentities($txt['lingua'])?>...","show-menu=menu_idiomas",,"",1
//	,"<?=htmlentities($txt['menu_ajuda'])?>","mapeo/index.php?f=ajuda",,"",1
	,"<?=htmlentities($txt['menu_ajuda'])?>","documentacao/manual_agroecologia_em_rede_set2009.pdf",,"",1
	,"<?=htmlentities($txt['menu_fale_conosco'])?>","contato.php target=_self",,"",1
	<?php
} elseif ($a) {
	?>
	,"Principal","admin/main.php target=_self",,"",1
	,"Experi&ecirc;ncias","show-menu=menu_experiencias",,,1
	,"Pesquisas","show-menu=menu_pesquisas",,"",1
	,"Pessoas","show-menu=menu_pessoa",,"",1
	,"Institui&ccedil;&otilde;es","show-menu=menu_instituicao",,"",1
	,"Links","show-menu=menu_links",,"",1
	,"Tabelas","show-menu=menu_tabelas_auxiliares",,"",1
	,"Consultas","show-menu=menu_relatorios",,"",1
	<?php
} else {
	?>
	,"<?=htmlentities($txt['menu_inicio'])?>","index.php target=_self",,"",1
	,"<?=htmlentities($txt['menu_consultar'])?>...","show-menu=menu_consultas",,,1
	,"<?=htmlentities($txt['menu_cadastrar'])?>","login.php target=_self",,"",1
	,"<?=htmlentities($txt['lingua'])?>...","show-menu=menu_idiomas",,"",1
//	,"<?=htmlentities($txt['menu_sobre'])?>","index.php?f=inicio target=_self",,"",1
	,"<?=htmlentities($txt['menu_fale_conosco'])?>","contato.php target=_self",,"",1
	<?php
} ?>
//,"Links","links.php target=_self",,"",1
])

addmenu(menu=["menu_idiomas",
,,200,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
,"Castellano","<?=str_replace('lingua='.$lingua,'lingua=es',$txtAlvo)?> target=_self",,,0
,"Portugu&ecirc;s","<?=str_replace('lingua='.$lingua,'lingua=pt-br',$txtAlvo)?> target=_self",,,0
])

addmenu(menu=["menu_consultas",
,,240,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
//,"<?=htmlentities($txt['menu_consultar_experiencia'])?>","banco_experiencias.php target=_self",,,0
,"<?=htmlentities($txt['menu_consultar_experiencia'])?>","mapeo/index.php?f=consultar&tipofrm=experiencia target=_self",,,0
,"<?=htmlentities($txt['menu_consultar_pesquisa'])?>","banco_pesquisas.php target=_self",,"",1
,"<?=htmlentities($txt['menu_consultar_pessoa'])?>", "usuarios.php target=_self",,,0
//,"<?=htmlentities($txt['menu_consultar_instituicao'])?>", "instituicoes.php target=_self",,,0
,"<?=htmlentities($txt['menu_consultar_instituicao'])?>", "mapeo/index.php?f=consultar&tipofrm=instituicao target=_self",,,0
])

addmenu(menu=["menu_cadastros",
,,240,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
,"<?=htmlentities($txt['menu_cadastrar_instituicao'])?>","mapeo/index.php?f=previa_frm_instituicao",,"",0
,"<?=htmlentities($txt['menu_cadastrar_experiencia'])?>","show-menu=menu_cadastro_experiencias",,"",0
//,"<?=htmlentities($txt['menu_cadastrar_pesquisa'])?>", "pesquisas_cadastro.php target=_self",,,0
])

addmenu(menu=["menu_alteracoes",
,,240,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
,"<?=htmlentities($txt['menu_alterar_instituicao'])?>","mapeo/index.php?f=lista_formularios&nomefrm=frm_instituicao",,"",0
,"<?=htmlentities($txt['menu_alterar_experiencia'])?>","show-menu=menu_alteracao_experiencias",,"",0
])

addmenu(menu=["menu_cadastro_experiencias",
,,240,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
,"<?=htmlentities($txt['menu_cadastrar_experiencia_cca'])?>","mapeo/index.php?f=formulario_geral&nomefrm=frm_exp_cca",,"",0
,"<?=htmlentities($txt['menu_cadastrar_experiencia_geral'])?>","mapeo/index.php?f=formulario_geral&nomefrm=frm_exp_geral",,"",0
])

addmenu(menu=["menu_alteracao_experiencias",
,,240,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
,"<?=htmlentities($txt['menu_cadastrar_experiencia_cca'])?>","mapeo/index.php?f=lista_formularios&nomefrm=frm_exp_cca",,"",0
,"<?=htmlentities($txt['menu_cadastrar_experiencia_geral'])?>","mapeo/index.php?f=lista_formularios&nomefrm=frm_exp_geral",,"",0
])
mapeo/index.php?f=formulario_geral&nomefrm=frm_exp_geral
mapeo/index.php?f=lista_formularios&nomefrm=frm_exp_geral

addmenu(menu=["menu_experiencias",
,,170,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Cadastrar...", "show-menu=menu_cadastro_experiencias",,,0
," Analisar e alterar", "show-menu=menu_alteracao_experiencias",,,0
//," An&aacute;lise", "admin/experiencias_libera.php target=_self",,,0
//," Cancelar Libera&ccedil;&atilde;o/Rejei&ccedil;&atilde;o", "admin/experiencias_cancela_liberacao.php target=_self",,,0
//," Excluir", "admin/experiencias_excluir.php target=_self",,,0
//," Anexos", "experiencias_arquivos.php target=_self",,,0
," Coment&aacute;rios", "show-menu=menu_experiencias_comentario",,,0

])

addmenu(menu=["menu_experiencias_comentario",
,,170,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," An&aacute;lise", "admin/experiencias_libera_comentario.php target=_self",,,0
," Cancelar Libera&ccedil;&atilde;o/Rejei&ccedil;&atilde;o", "admin/experiencias_cancela_liberacao_comentario.php target=_self",,,0
])

addmenu(menu=["menu_pesquisas",
,,170,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Cadastrar", "admin/pesquisas_cadastro.php target=_self",,,0
," Alterar", "admin/pesquisas_altera.php target=_self",,,0
," An&aacute;lise", "admin/pesquisas_libera.php target=_self",,,0
," Cancelar Libera&ccedil;&atilde;o/Rejei&ccedil;&atilde;o", "admin/pesquisas_cancela_liberacao.php target=_self",,,0
," Excluir", "admin/pesquisas_excluir.php target=_self",,,0
," Anexos", "admin/pesquisas_arquivos.php target=_self",,,0
," Coment&aacute;rios", "show-menu=menu_pesquisas_comentario",,,0
])


addmenu(menu=["menu_pesquisas_comentario",
,,170,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," An&aacute;lise", "admin/pesquisa_libera_comentario.php target=_self",,,0
," Cancelar Libera&ccedil;&atilde;o/Rejei&ccedil;&atilde;o", "admin/pesquisa_cancela_liberacao_comentario.php target=_self",,,0
])

addmenu(menu=["menu_cd",
,,150,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Cadastrar", "admin/bibliografia.php?tipo=NR target=_self",,,0
," Alterar", "admin/bibliografia.php?tipo=AR  target=_self",,,0
," Liberar", "admin/bibliografia.php?tipo=LR target=_self",,,0
," Cancelar Libera&ccedil;&atilde;o", "admin/bibliografia.php?tipo=CL target=_self",,,0
," Excluir", "admin/bibliografia.php?tipo=ER target=_self",,,0

])



addmenu(menu=["menu_pessoa",

,,200,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Cadastrar", "admin/usuarios.php target=_self",,,0
," Alterar", "admin/usuarios_cadastro.php target=_self",,,0
," Liberar", "admin/usuarios_liberacao.php target=_self",,,0
," Cancelar Libera&ccedil;&atilde;o", "admin/usuarios_liberacao_cancela.php target=_self",,,0
," Excluir", "admin/usuarios_exclusao.php?tipo=EX target=_self",,,0

])



addmenu(menu=["menu_instituicao",
,,150,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Cadastrar", "mapeo/index.php?f=formulario_geral&nomefrm=frm_instituicao target=_self",,,0
," Alterar e excluir", "mapeo/index.php?f=lista_formularios&nomefrm=frm_instituicao  target=_self",,,0
," Liberar", "admin/instituicoes_liberacao.php target=_self",,,0
," Cancela Libera&ccedil;&atilde;o", "admin/instituicoes_liberacao_cancela.php target=_self",,,0
//," Excluir", "admin/instituicoes_exclusao.php target=_self",,,0

])

addmenu(menu=["menu_tabelas_auxiliares",
,,250,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
//," &aacute;reas geogr&aacute;ficas", "tabela_complexa.php?tipo=AG target=_self",,,0
," &Aacute;reas Tem&aacute;ticas", "admin/tabela_complexa.php?tipo=AT  target=_self",,,0
," Biomas", "admin/tabela_complexa.php?tipo=HA target=_self",,,0
," Idiomas", "admin/tabela_simples.php?tipoTabela=ID target=_self",,,0
," Tipos de bibliografias", "admin/tabela_simples.php?tipoTabela=TB target=_self",,,0
," Tipos de institui&ccedil;&atilde;o", "admin/tabela_simples.php?tipoTabela=TI target=_self",,,0
," Identidades", "admin/tabela_simples.php?tipoTabela=lista_identidades target=_self",,,0
," N&iacute;veis educacionais", "admin/tabela_simples.php?tipoTabela=lista_nivel_educacional target=_self",,,0
," Regime de ensino", "admin/tabela_simples.php?tipoTabela=lista_regime_ensino target=_self",,,0
," Textos/Par&acirc;metros do sistema", "admin/textos.php target=_self",,,0
," Textos dos formul&aacute;rios", "show-menu=menu_textos_formularios",,,0
])

addmenu(menu=["menu_textos_formularios",
,,250,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Experi&ecirc;ncias CCA", "admin/tabela_complexa2.php?tipo=mapeo_mapa_campos&frm=frm_exp_cca target=_self",,,0
," Outras experi&ecirc;ncias", "admin/tabela_complexa2.php?tipo=mapeo_mapa_campos&frm=frm_exp_geral target=_self",,,0
," <?=htmlentities($txt['menu_consultar_instituicao'])?>", "admin/tabela_complexa2.php?tipo=mapeo_mapa_campos&frm=frm_instituicao target=_self",,,0
])



addmenu(menu=["menu_relatorios",
,,230,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Buscas mais Realizadas", "admin/rel_mais_procurados.php target=_self",,,0
," Lista de Endere&ccedil;os", "show-menu=menu_lista_enderecos",,,0
," Lista de Cadastros", "show-menu=menu_lista_cadastros",,,0
," Evolu&ccedil;&atilde;o", "admin/rel_evolucao.php target=_self",,,0
,"Estat&iacute;sticas de Acesso (Rits)","http://www.agroecologiaemrede.org.br/awstats target=_self",,,0

])

addmenu(menu=["menu_lista_enderecos",
,,150,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Institui&ccedil;&otilde;es", "admin/rel_enderecos_instituicoes.php target=_self",,,0
," Pessoas", "admin/rel_enderecos_pessoas.php target=_self",,,0
])

addmenu(menu=["menu_lista_cadastros",
,,150,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Pesquisas x Data", "admin/rel_pesquisa_data.php target=_self",,,0
," Experi&ecirc;ncias x Data", "admin/rel_experiencia_data.php target=_self",,,0
," Pessoas x Data", "admin/rel_pessoa_data.php target=_self",,,0
])

addmenu(menu=["menu_links",
,,150,1,"",style1,,,"RandomDissolve(duration=0.2)",,,,,,,,,,,,
," Gerenciamento", "admin/links.php target=_self",,,0
," Categorias", "admin/tabela_simples.php?tipoTabela=LK  target=_self",,,0

])

//////////////////////////////////
// Editable properties END here  //
//////////////////////////////////
dumpmenus() // This must be the last line in this file
