<?php


if (!isset($initFiltro)) {
	setcookie("cookieBuscaAT");
	setcookie("cookieBuscaAG");
	setcookie("cookieBuscaBM");
}

include("conexao.inc.php");

function my_array_unique($somearray) {
	$tmparr = array_unique($somearray);
	$i=0;
	foreach ($tmparr as $v) {
		$newarr[$i] = $v;
		$i++;
	}
	return $newarr;
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function enviaEmail(url) {
	var email = prompt("Digite um endereço de e-mail:","");
	if (email != null && email != "") {
		url = url + '&email=' + email;
		window.location.href=url;
	}
}

function mOvr(src,clrOver) {
  if (!src.contains(event.fromElement)) {
      src.style.cursor = 'hand';
      src.bgColor = clrOver;
  }
}
function mOut(src,clrIn) {
  if (!src.contains(event.toElement)) {
     src.style.cursor = 'default';
     src.bgColor = clrIn;
  }
}
function gotourl(url) {
  window.location.href=url;
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

function abrePopup(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
</script>


<?php include("menu_geral.php"); ?>
<br>
<br>
<table width="589" border="0" cellpadding="5" cellspacing="0" align="center">
  <tr>
    <td width="579" valign="top"> <table width="555" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td width="20" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="42" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_simples.gif" width="34" height="32"></p></td>
          <td width="474" bgcolor="#80AAD2"> <p><strong>Institui&ccedil;&otilde;es
              Cadastradas </strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
	  <table>
	  <tr align="center"><td width="568" align="center">
	  	  <?php if (isset($busca)) { ?>
	      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="relatorio_simples.php?tipo=IN1&acao=IM&busca=<?= isset($busca) ? $busca : ""; ?>&localizacao=<?php echo $localizacao; ?>&tipoInst=<?php echo $tipo; ?>&cidade=<?php echo $cidade; ?>&estado=<?php echo $estado; ?>&pais=<?php echo $pais; ?>&cep=<?php echo $cep; ?>"><img src="imagens/ico_imprimir.gif" width="59" height="12" border="0"></a>
        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onClick="enviaEmail('relatorio_simples.php?tipo=IN1&acao=EM&busca=<?= isset($busca) ? $busca : ""; ?>&localizacao=<?php echo $localizacao; ?>&tipoInst=<?php echo $tipo; ?>&cidade=<?php echo $cidade; ?>&estado=<?php echo $estado; ?>&pais=<?php echo $pais; ?>&cep=<?php echo $cep; ?>');"><img src="imagens/ico_envia_email.gif" width="99" height="13" border="0"></a></p>
	  <?php } else if (isset($inst)) { ?>
	      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="relatorio_simples.php?tipo=IN2&acao=IM&instDesc=<?php echo $instDesc; ?>&inst=<?php echo $inst; ?>"><img src="imagens/ico_imprimir.gif" width="59" height="12" border="0"></a>
        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onClick="enviaEmail('relatorio_simples.php?tipo=IN2&acao=EM&instDesc=<?php echo $instDesc; ?>&inst=<?php echo $inst; ?>');"><img src="imagens/ico_envia_email.gif" width="99" height="13" border="0"></a></p>
	   <?php } ?>
</td></tr>
	  </table>
      <br>
      <br>
	   <form name="form1" method="post" action="instituicoes.php#result">
	  <?php if (isset($initFiltro)) { ?>
	  	<input type="hidden" name="initFiltro" value="">
	  <?php } ?>
        <table width="567" border="0" cellpadding="5" cellspacing="1">
          <tr>
            <td><p><strong>Busca</strong> - digite o nome
                da <strong>INSTITUI&Ccedil;&Atilde;O</strong> no campo abaixo.
                Pode-se tamb&eacute;m fazer uma combina&ccedil;&atilde;o entre
                nome e tipo, ou buscar todas as institui&ccedil;&otilde;es de
                um tipo apenas - para isto, basta deixar o campo 'nome' em branco,
                selecionar o tipo e fazer a busca.</p></td>
          </tr>
          <tr>
            <td height="1" bgcolor="#000000"> <div align="center"></div></td>
          </tr>
          <tr>
            <td> <input name="busca" type="text" id="busca" size="40" maxlength="255" value="<?= isset($busca) ? $busca : ""; ?>">
              &nbsp;&nbsp; <select name="tipo" id="tipo">
                <option value="" selected>-- Tipos de Instituição (opcional) --</option>
                <?php
			$sql = "SELECT * FROM tipo_instituicoes ORDER BY ti_descricao";
			$query = $db->query($sql);
			while ($row = $query->fetch_object()) {
				$varSelected = "";
				if (isset($tipo) && $tipo == $row->ti_id) {
					$varSelected = "selected";
				}
?>
                <option <?php echo $varSelected; ?> value="<?php echo $row->ti_id; ?>"><?php echo trim(traduz($row->ti_descricao)); ?></option>
                <?php
			} // la�o do loop
?>
              </select>
	       <?php if (!isset($initFiltro)) { ?>
				<input type="submit" value="Buscar" class="botaoLogin">
			<?php } ?>
		    </td>
          </tr>
<?php

if (isset($initFiltro)) {

?>
        <tr>
          <td>
		  	<table width="555" border="0" cellspacing="1" cellpadding="3">
              <tr>
                <td width="50%" class="textoPreto" valign="top" bgcolor="#ACCBD0">
<?php
	echo "<b>Áreas Temáticas</b><br><br>";
	if (isset($_COOKIE['cookieBuscaAT']) && strlen(trim($_COOKIE['cookieBuscaAT'])) > 0) {
		$arrayAT = explode(";",$_COOKIE["cookieBuscaAT"]);
		$arrayAT = my_array_unique($arrayAT);
		for ($x = 0; $x < count($arrayAT); $x++) {
			$temp = $arrayAT[$x];
			$sql = "SELECT * FROM area_tematica WHERE at_id=$temp";
			$query = $db->query($sql);
			$row = $query->fetch_object();
		echo "&nbsp;&nbsp; - ".traduz($row->at_descricao,$lingua)."<br>";
		}
	}
	else {
		echo "&nbsp;&nbsp; - Todos<br>";
	}
?>
				</td>
                <td width="50%" class="textoPreto" valign="top" bgcolor="#B9D69E">
<?php
	echo "<b>Áreas Geográficas</b><br><br>";
	if (isset($_COOKIE['cookieBuscaAG']) && strlen(trim($_COOKIE['cookieBuscaAG'])) > 0) {
		$arrayAG = explode(";",$_COOKIE['cookieBuscaAG']);
		$arrayAG = my_array_unique($arrayAG);
		for ($x = 0; $x < count($arrayAG); $x++) {
			$temp = $arrayAG[$x];
			$sql = "SELECT * FROM area_geografica WHERE ag_id='$temp'";
			$query = $db->query($sql);
			$row = $query->fetch_object();
		echo "&nbsp;&nbsp; - $row->ag_descricao<br>";
		}
	}
	else {
		echo "&nbsp;&nbsp; - Todos<br>";
	}
?>
				</td>
              </tr>
            </table>
			<br>
			<center><input name="Submit" type="button" class="botaoLogin" value="     Buscar     " onClick="javascript:form1.submit();"></center>
		  </td>
        </tr>
<?php
} // fim do if que verifica os filtros
?>
<tr>
<td colspan="2">
<?php
if (isset($busca)) {

	$busca = strtoupper($busca);

	if (isset($_COOKIE['cookieBuscaAT']) && strlen(trim($_COOKIE['cookieBuscaAT'])) > 0) {

		function untree($parent,$level,&$filhos) {
			$parentsql = $db->query("SELECT * FROM area_tematica WHERE at_pertence=$parent ORDER BY at_descricao");
		    if (!$parentsql->num_rows) {
			    return;
		    }
    		else {
		        while($branch = $parentsql->fetch_object()) {
					$filhos .= "$branch->at_id;";
    		        $rename_level = $level;
	    	        untree($branch->at_id,++$rename_level,$filhos);
       		    }
	        }

			return $filhos;
	    }

		$j = 0;
		$z = 0;

		$arrayTema = array();
		$arrayAT = explode(";",$_COOKIE["cookieBuscaAT"]);
		$arrayAT = my_array_unique($arrayAT);
		$filhosAT = "";
		for ($i = 0; $i < count($arrayAT); $i++){
			$temaID = $arrayAT[$i];
			$sql = "SELECT * FROM rel_area_inst WHERE rai_id_area=$temaID";
			$query = $db->query($sql);
			$num = $query->num_rows;

			$son = "";
			$filhosAT = untree($temaID,1,$son);

			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayTema[$z][$j] = $row->rai_id_inst;
					$j++;
				}
				if (strlen(trim($filhosAT)) > 0) {
					$arrayATF = explode(";",$filhosAT);
					$arrayATF = my_array_unique($arrayATF);
					for ($c = 0; $c < count($arrayATF) - 1; $c++){
						$temaID = $arrayATF[$c];
						$sql = "SELECT * FROM rel_area_inst WHERE rai_id_area=$temaID";
						$query = $db->query($sql);
						$num = $query->num_rows;
						if ($num > 0) {
							while($row = $query->fetch_object()) {
								$arrayTema[$z][$j] = $row->rai_id_inst;
								$j++;
							}
						}
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else if ($num == 0 && strlen(trim($filhosAT)) > 0) {
				$arrayATF = explode(";",$filhosAT);
				$arrayATF = my_array_unique($arrayATF);
				for ($c = 0; $c < count($arrayATF) - 1; $c++){
					$temaID = $arrayATF[$c];
					$sql = "SELECT * FROM rel_area_inst WHERE rai_id_area=$temaID";
					$query = $db->query($sql);
					$num = $query->num_rows;
					if ($num > 0) {
						while($row = $query->fetch_object()) {
							$arrayTema[$z][$j] = $row->rai_id_inst;
							$j++;
						}
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else {
				$arrayTema[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

		} // la�o do for

		if ($z > 0) { // os selects montaram pelo menos 1 array?
			if ($z == 1) {
				$intersecaoArraysAT = $arrayTema[0];
			}
			if ($z > 1) {
				$intersecaoArraysAT = array_intersect($arrayTema[0],$arrayTema[1]);
				if ($z > 2) {
					for ($x = 2; $x < $z; $x++) {
						$intersecaoArraysAT = array_intersect($intersecaoArraysAT,$arrayTema[$x]);
					} // la�o do for
				}
			}
			$intersecaoArraysAT = array_unique($intersecaoArraysAT);
			sort($intersecaoArraysAT,SORT_NUMERIC);
		} // fim do if que verifica se a sele��o retornou mais de 0 arrays

	} // fim do if que pergunta se tem filtro de AT



	/******************************** AREA GEOGRFICA ****************************************/



	if (isset($_COOKIE['cookieBuscaAG']) && strlen(trim($_COOKIE['cookieBuscaAG'])) > 0) {

		function untree2($parent, $level, &$filhos) {
			$parentsql = $db->query("SELECT * FROM area_geografica WHERE ag_pertence=$parent ORDER BY ag_descricao");
		    if (!$parentsql->num_rows) {
			    return;
		    }
    		else {
		        while($branch = $parentsql->fetch_object()) {
					$filhos .= "$branch->ag_id;";
    		        $rename_level = $level;
	    	        untree2($branch->ag_id,++$rename_level,$filhos);
       		    }
	        }

			return $filhos;
	    }


		$j = 0;
		$z = 0;
		$arrayGeo = array();
		$arrayAG = explode(";",$_COOKIE['cookieBuscaAG']);
		$arrayAG = my_array_unique($arrayAG);
		$filhosAG = "";
		for ($i = 0; $i < count($arrayAG); $i++){
			$geoID = $arrayAG[$i];
			$sql = "SELECT * FROM rel_geo_inst WHERE rgi_id_geo=$geoID";
			$query = $db->query($sql);
			$num = $query->num_rows;

			$son = "";
			$filhosAG = untree2($geoID,1,$son);

			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayGeo[$z][$j] = $row->rgi_id_inst;
					$j++;
				}
				if (strlen(trim($filhosAG)) > 0) {
					$arrayAGF = explode(";",$filhosAG);
					$arrayAGF = my_array_unique($arrayAGF);
					for ($c = 0; $c < count($arrayAGF) - 1; $c++){
						$geoID = $arrayAGF[$c];
						$sql = "SELECT * FROM rel_geo_inst WHERE rgi_id_geo=$geoID";
						$query = $db->query($sql);
						$num = $query->num_rows;
						if ($num > 0) {
							while($row = $query->fetch_object()) {
								$arrayGeo[$z][$j] = $row->rgi_id_inst;
								$j++;
							}
						}
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else if ($num == 0 && strlen(trim($filhosAG)) > 0) {
				$arrayAGF = explode(";",$filhosAG);
				$arrayAGF = my_array_unique($arrayAGF);
				for ($c = 0; $c < count($arrayAGF) - 1; $c++){
					$geoID = $arrayAGF[$c];
					$sql = "SELECT * FROM rel_geo_inst WHERE rgi_id_geo=$geoID";
					$query = $db->query($sql);
					$num = $query->num_rows;
					if ($num > 0) {
						while($row = $query->fetch_object()) {
							$arrayGeo[$z][$j] = $row->rgi_id_inst;
							$j++;
						}
					}
				}
				$z++;
			} // fim do if que verifica se h� registros
			else {
				$arrayGeo[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

		} // la�o do for

		if ($z > 0) { // os selects montaram pelo menos 1 array?
			if ($z == 1) {
				$intersecaoArraysAG = $arrayGeo[0];
			}
			if ($z > 1) {
				$intersecaoArraysAG = array_merge($arrayGeo[0],$arrayGeo[1]);
				if ($z > 2) {
					for ($x = 2; $x < $z; $x++) {
						$intersecaoArraysAG = array_merge($intersecaoArraysAG,$arrayGeo[$x]);
					} // la�o do for
				}
			}
			$intersecaoArraysAG = array_unique($intersecaoArraysAG);
			sort($intersecaoArraysAG,SORT_NUMERIC);
		} // fim do if que verifica se a sele��o retornou mais de 0 arrays

	} // fim do if que pergunta se tem filtro de AG

	$arrays = 0;
	if (isset($intersecaoArraysAT) && count($intersecaoArraysAT) >= 0) {
		$arraysCompletos[$arrays] = $intersecaoArraysAT;
		$arrays++;
		$arrayUnico = $intersecaoArraysAT;
	}
	if (isset($intersecaoArraysAG) && count($intersecaoArraysAG) >= 0) {
		$arraysCompletos[$arrays] = $intersecaoArraysAG;
		$arrays++;
		$arrayUnico = $intersecaoArraysAG;
	}


	if (strlen(trim($tipo)) == 0) {
		$sql = "SELECT a.*, b.*, c.nome as cidade, c.UF FROM frm_instituicao a,tipo_instituicoes b, _BR c
				WHERE in_liberacao='S' AND in_tipo=ti_id AND a.ei_cidade=c.id AND UCASE(in_nome) LIKE '$busca%' AND ";
	}
	else {
		$sql = "SELECT a.*, b.*, c.nome as cidade, c.UF FROM frm_instituicao a,tipo_instituicoes b, _BR c
				WHERE in_liberacao='S' AND in_tipo=ti_id AND a.ei_cidade=c.id AND UCASE(in_nome) LIKE '$busca%' AND in_tipo=$tipo AND ";
	}

	if (isset($arraysCompletos) && count($arraysCompletos) == 1) {
		$arrayMontado = "";
		$arrayFinal = $arraysCompletos[0];
		while (list ($key, $val) = each ($arraysCompletos[0])) {
    		$arrayMontado .= "$val,";
		}
		$arrayMontado = substr($arrayMontado,0,-1);
	}
	else if (count($arraysCompletos) > 1) {
		$arrayFinal = array_intersect($arraysCompletos[0],$arraysCompletos[1]);
		if (count($arraysCompletos) > 2) {
			for ($x = 2; $x < count($arraysCompletos); $x++) {
				$arrayFinal = array_intersect($arrayFinal,$arraysCompletos[$x]);
			}
		}
		$arrayMontado = "";
		while (list ($key, $val) = each ($arrayFinal)) {
    		$arrayMontado .= "'$val',";
		}
		$arrayMontado = substr($arrayMontado,0,-1);
	}

	$temp = substr(trim($sql),-3);
	if ($temp == "AND") {
		$sql = substr(trim($sql),0,-3);
	}
	if (isset($arrayMontado) && strlen(trim($arrayMontado)) > 0) {
		$sql .= " AND in_id in ($arrayMontado)";
	}
	else if (isset($arrayMontado) && strlen(trim($arrayMontado)) == 0) {
		$sql .= " AND in_id in (0)";
	}

	$sql .= " ORDER BY ti_descricao,in_nome";

	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {

		echo "<a name=\"result\">";
		echo "<p class=\"textoPreto10px\">";

		$i = 1;
		$flagTipo = "";
		while ($row = $query->fetch_object()) {

			if ($flagTipo != $row->ti_id) {
				$i = 1;
				echo "&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"instituicoes.php?busca=&tipoDesc=$row->ti_descricao&tipo=$row->ti_id\" class=\"preto\"><strong>".traduz($row->ti_descricao)."</strong></a><br><br>";
			}
			//echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"instituicoes.php?instDesc=$row->in_nome&inst=$row->in_id\" class=\"preto\">$row->in_nome</a> [ <a href=\"#\" class=\"Preto\" onClick=\"MM_openBrWindow('usuarios_popup_dados.php?tipo=inst&id=$row->in_id&nome=$row->in_nome','usuPopup','scrollbars=yes,width=500,height=300')\">Endereços e Telefones</a> ]<br><br>";
			echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"instituicoes.php?instDesc=$row->in_nome&inst=$row->in_id\" class=\"preto\">$row->in_nome</a><br><br>";
			$i++;
			$flagTipo = $row->ti_id;

		}

		echo "</p>";

	}
	else {

		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";

	} // fim do if que verifica se a busca retornou os resultados

} // fim do if que verifica se uma busca foi feita

if (isset($inst)) {
?>
      <table width="567" border="0" cellspacing="0" cellpadding="3">
        <tr>
          <td width="311" valign="top">
		  	<table width="100%" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td class="textoPreto"><strong>Dados Cadastrais</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
				<p>
<?php
	$sql = "SELECT a.*, b.nome as cidade, b.UF FROM frm_instituicao as a, _BR as b WHERE a.ei_cidade=b.id AND in_id=$inst";
	$query = $db->query($sql);
	$rowUSR = $query->fetch_object();
	echo "Nome: <b>$rowUSR->in_nome</b>\n";
	if (strlen(trim($rowUSR->in_email)) > 0) {
		echo "<br>\n";
		echo "E-mail: <a href=\"mailto:$rowUSR->in_email\" class=\"Preto\">$rowUSR->in_email</a>\n";
	}
	if (strlen(trim($rowUSR->in_url)) > 0) {
		echo "<br>\n";
		echo "Website: <a href=\"http://$rowUSR->in_url\" class=\"Preto\">$rowUSR->in_url</a>\n";
	}

?>
				</p>
				</td>
              </tr>
              <tr>
                <td>
				<p></p>

				</td>
              </tr>
              <tr>
                <td class="textoPreto"><strong>Endere&ccedil;os</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
				<p>
<?php
	$sql = "SELECT a.*, b.nome as cidade, b.UF FROM frm_instituicao as a, _BR as b WHERE a.ei_cidade=b.id AND in_id=$inst";
	$query = $db->query($sql);
	$numEnd = $query->num_rows;
	if ($numEnd > 0) {
		echo "<table width=\"100%\" border=\"0\" align=\"center\" cellpadding=\"5\" cellspacing=\"1\">";
		while ($row = $query->fetch_object()) {

			echo "<tr bgcolor=\"#D8D8D8\"> \n";
		    echo "<td align=\"center\" valign=\"top\" bgcolor=\"#D8D8D8\" class=\"textoPreto10px\"><strong>&nbsp;</strong></td>\n";
			echo "<td bgcolor=\"#D8D8D8\" class=\"textoPreto10px\">\n";
			echo "<strong>Endereço</strong>:<br>$row->ei_endereco<br>\n";
			echo "<strong>Cidade / Estado</strong>:<br>$row->cidade / $row->UF<br>\n";
			echo "<strong>CEP</strong>:<br>$row->ei_cep<br>\n";
			echo "<strong>País</strong>:<br>$row->ei_pais\n";
            echo "<br><strong>Telefone</strong>:<br>$row->ei_telefone\n";
			echo "</td>\n";
			echo "</tr>\n";

		}
		echo "</table>";
	}

?>
					</p>
				</td>
              </tr>
            </table>
		  </td>
          <td width="256" valign="top">
			<table width="250" border="0" cellspacing="0" cellpadding="3">
              <tr>
                <td width="250" class="textoPreto"><strong>Experi&ecirc;ncias
                  Relacionadas</strong></td>

              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td> <?php


	$sql = "SELECT * FROM frm_instituicao,rel_inst_experiencia,experiencia WHERE in_id=$inst AND in_id=rie_id_inst ".
		   "AND ex_id=rie_id_experiencia AND ex_liberacao='S'  order by ex_chamada";

	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {
		/* echo "<p class=\"textoPreto10px\">Foram encontrados as seguintes experiências para a instituição <b>\"$instDesc\"</b>: */ echo "<br><br>"; /*";   */
		$i = 1;
		while ($row = $query->fetch_object()) {
			echo "$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_chamada</a><br><br>";
			$i++;
		}
		echo "</p>";
	}


	$sql = "SELECT * FROM experiencia_autor,experiencia WHERE exa_id_experiencia=ex_id AND exa_id_inst=$inst order by ex_chamada";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {

		echo "<p class=\"textoPreto10px\"><b>Autor(a):</b><br><br>";

		$i = 1;
		while ($row = $query->fetch_object()) {

			echo "$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_chamada</a><br><br>";

			$i++;

		}

		echo "</p>";

	}


	$sql = "SELECT * FROM experiencia_relator,experiencia WHERE exr_id_experiencia=ex_id AND exr_id_inst=$inst order by ex_chamada";
	$query = $db->query($sql);
	$num = $query->num_rows;

	if ($num > 0) {

		echo "<p class=\"textoPreto10px\"><b>Relator(a):</b><br><br>";

		$i = 1;
		while ($row = $query->fetch_object()) {

			echo "$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_chamada</a><br><br>";

			$i++;

		}

		echo "</p>";

	}

?> </td>
              </tr>
              <tr>
                <td class="textoPreto"><strong>Experimentadores e T�cnicos</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
                  <?php
	$sql = "SELECT * FROM rel_inst_usuario,usuario WHERE riu_id_usuario=us_login AND riu_id_inst=$inst";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		while ($arrayAreas = $query->fetch_object()) {
			echo "<img src=\"imagens/seta02.gif\"> <a href=\"usuarios.php?usuNome=$arrayAreas->us_nome&usu=$arrayAreas->us_login\" class=\"preto\">$arrayAreas->us_nome</a><br>";
		}
	}
?>
                </td>
              </tr>

              <tr>
                <td class="textoPreto"><strong>&Aacute;reas Tem&aacute;ticas</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
<?php
	$sql = "SELECT * FROM rel_area_inst,area_tematica WHERE rai_id_area=at_id AND rai_id_inst=$inst";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		while ($arrayAreas = $query->fetch_object()) {
			echo "<img src=\"imagens/seta02.gif\"> <a href=\"xt_adiciona_agat.php?url=banco_experiencias&tipo=at&id=$arrayAreas->at_id\" class=\"preto\">$arrayAreas->at_descricao</a><br>";
		}
	}

?>
                </td>
              </tr>
              <tr>
                <td class="textoPreto"><strong>&Aacute;reas Geogr&aacute;ficas</strong></td>
              </tr>
              <tr>
                <td height="1" bgcolor="#000000"> <div align="center"></div></td>
              </tr>
              <tr>
                <td>
                  <?php
	$sql = "SELECT * FROM rel_geo_inst,area_geografica WHERE rgi_id_geo=ag_id AND rgi_id_inst=$inst";
	$query = $db->query($sql);
	$num = $query->num_rows;
	if ($num > 0) {
		while ($arrayAreas = $query->fetch_object()) {
			echo "<img src=\"imagens/seta02.gif\"> <a href=\"xt_adiciona_agat.php?url=banco_experiencias&tipo=ag&id=$arrayAreas->ag_id\" class=\"preto\">$arrayAreas->ag_descricao</a><br>";
		}
	}
?>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
<?php
// } // fim do if que verifica se foi clicado em algum usuário
?>
</td>
</tr>

<?php

if (isset($inst)) { ?>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>

       <tr>
          <td colspan="2"><p align="center">[ <a href="instituicoes.php" class="Preto">Nova Busca</a>
                ]<strong> </strong></p></td>
        </tr>

        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>

<?php } else { ?>


          <tr>
          <td colspan="2"><p><strong>&Aacute;REAS TEM&Aacute;TICAS
                </strong>[ <a href="instituicoes.php" class="Preto">Nova Busca</a>
                ]<strong> </strong></p></td>
        </tr>
        <tr>
          <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr>
          <td colspan="2">
		  	<table width="555" border="0" cellpadding="10" cellspacing="2">
<?php
$sql = "SELECT * FROM area_tematica WHERE at_pertence=0 ORDER BY at_descricao";
$query = $db->query($sql);
$i = 0;
while ($row = $query->fetch_object()) {
	if ($i == 0) {
    	echo "<tr bgcolor=\"#ACCBD0\" class=\"textoPreto10px\">";
	}
    echo "<td onmouseover=\"mOvr(this,'#86AEB0');\" onmouseout=\"mOut(this,'#ACCBD0');\" onClick=\"abrePopup('exp_popup_busca_temas.php?id=$row->at_id&url=instituicoes.php','tema','scrollbars=yes,width=517,height=300')\">
          <div align=\"center\"><strong>".traduz($row->at_descricao,$lingua)."</strong></div>
		  </td>";
	if ($i == 3) {
		echo "</tr>";
	}

	$i++;

	if ($i == 3) {
		$i = 0;
	}

}
?>
            </table>
		  </td>
        </tr>
        <tr>
          <td colspan="2">
		  	<table width="555" border="0" cellpadding="10" cellspacing="2">
              <tr bgcolor="#B9D69E">
                <td bgcolor="#B9D69E" onClick="abrePopup('exp_popup_busca_ag.php?url=instituicoes.php','ag','scrollbars=yes,width=517,height=300')" onmouseover="mOvr(this,'#89BA5C');" onmouseout="mOut(this,'#B9D69E');">
                  <div align="center"><strong>&Aacute;reas Geogr&aacute;ficas</strong></div></td>
              </tr>
            </table>
		  </td>
        </tr>
        </table>

		<?php
		}

} // fim do if que verifica se foi clicado em algum usuário
?>

    </td>
  </tr>

            <tr >
            <td colspan="2" ><p align="center"><strong>Áreas Temáticas
                </strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[
                <a href="instituicoes.php" class="Preto"><strong>Nova Busca</strong></a>
                ]<strong> </strong></p>
			</td>
         </tr>
        <tr>
            <td height="1" colspan="2" bgcolor="#000000">
              <div align="center"></div></td>
        </tr>
        <tr>
          <td colspan="2">
		  	<table width="555" border="0" cellpadding="10" cellspacing="2" align="center">
<?php
$sql = "SELECT * FROM area_tematica WHERE at_pertence=0 ORDER BY at_descricao";
$query = $db->query($sql);
$i = 0;
while ($row = $query->fetch_object()) {
	if ($i == 0) {
    	echo "<tr bgcolor=\"#ACCBD0\" class=\"textoPreto10px\">";
	}
    echo "<td onmouseover=\"mOvr(this,'#86AEB0');\" onmouseout=\"mOut(this,'#ACCBD0');\" onClick=\"abrePopup('exp_popup_busca_temas_pesq.php?id=$row->at_id&url=instituicoes.php','tema','scrollbars=yes,width=517,height=300')\">
          <div align=\"center\"><strong>".traduz($row->at_descricao,$lingua)."</strong></div>
		  </td>";
	if ($i == 3) {
		echo "</tr>";
	}

	$i++;

	if ($i == 3) {
		$i = 0;
	}

}
?>
            </table>
		  </td>
        </tr>
        <tr>
          <td colspan="2">
		  	<table width="555" border="0" cellpadding="10" cellspacing="2" align="center">
              <tr bgcolor="#B9D69E">
                <td bgcolor="#B9D69E" onClick="abrePopup('exp_popup_busca_ag.php?url=instituicoes.php','ag','scrollbars=yes,width=517,height=300')" onmouseover="mOvr(this,'#89BA5C');" onmouseout="mOut(this,'#B9D69E');">
                  <div align="center"><strong>&Aacute;reas Geogr&aacute;ficas</strong></div></td>
              </tr>
            </table>
		  </td>
        </tr>

</table>
</form>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>
