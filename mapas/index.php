<?php
//por daniel tygel (dtygel em fbes pt org pt br) em 2008
require_once "config.php";
require_once "lang/pt-br.php";
?><link rel="stylesheet" href="<?=$_SESSION['dtdir']['base_url']?>/estilo_fareja.css" type="text/css"><?php

//Aqui eu recupero arquivos de configuração, funções comuns do programa, funções de acentuação, e funções de Formulário (se estou na área restrita):
require_once "funcoes_comuns.php";
//Faço a conexão ao banco de dados:

$id_mr=$_REQUEST['MR'];
$id_uf=$_REQUEST['UF'];
$busca=$_REQUEST['busca'];
$mapa=1;
	if($id_microrreg) {
		$sql='select nome as cidade, lat as latitude, lng as longitude
			FROM '.$bd['ref'].'.br_nivel2
			WHERE id='.$q3.' LIMIT 1';
		$database->setQuery( $sql );
		$dados1 = $database->loadObjectList();
		$cidade_ref=$dados1[0]->cidade;
		$id_cidade_ref=$q3;
		$id_uf=substr($q3,0,2);
		$tmplat="(".$dados1[0]->latitude.")";
		$tmplong="(".$dados1[0]->longitude.")";
		$dados_get='?distmax='.$distmax.'&Q3='.$q3;
		$tit=str_replace(array('{1}','{2}'),array($distmax,$cidade_ref),$tra['subtit_com_distmax']);
		//$tit=" até ".$distmax."Km de ".$cidade_ref;
		$ord=$tra['e'].' '.strtolower($rta['distancia']);
	} elseif ($id_uf) {
		$sql='select lat as latitude, lng as longitude
			FROM '.$bd['ref'].'.br_nivel2
			WHERE SUBSTRING(id,1,2)="'.$id_uf.'" AND capital=1';
		$database->setQuery( $sql );
		$dados1 = $database->loadObjectList();
		$tmplat="(".$dados1[0]->latitude.")";
		$tmplong="(".$dados1[0]->longitude.")";
		$dados_get='?Q3='.$id_uf;
		$tit="<br />Estado: ".$lista_UFs[$id_uf];
		$ord=$tra['e'].' '.strtolower($rta['nome']);
	} elseif($id_mr) {
		$sql='select lat as latitude, lng as longitude
			FROM '.$bd['ref'].'.br_nivel2
			WHERE SUBSTRING(id,1,1)="'.$id_mr.'" AND capital=1 LIMIT 1';
		$database->setQuery( $sql );
		$dados1 = $database->loadObjectList();
		$tmplat="(".$dados1[0]->latitude.")";
		$tmplong="(".$dados1[0]->longitude.")";
		$dados_get='?Q3='.$id_mr;
		$tit="<br />Região ".$lista_MRs[$id_mr];
		$ord=$tra['e'].' '.strtolower($rta['nome']);
	} else {
		$dados_get='?Q3=0';
		$tit=" no Brasil";
	}
	$cabecalho_sidebar=str_replace('{ord}',$ord,$cabecalho_sidebar);
	if ($mapa)
		$dados_get.='|mapa=1';
	if ($busca)
		$dados_get.='|busca='.$busca;

	$fontes=array(
		'foruns'=>'gera_xml_foruns.php'
	);
	foreach ($fontes as $tipo=>$ftmp) {
		$fonte_ext = ($fontes_ext[$tipo])
			? '&fonte='.$fontes_ext[$tipo]
			: '';
		$urls[$tipo]=$dir['base_url'].'/'.$ftmp.$dados_get.$fonte_ext;
		if ($xmltmp=pega_xml_arquivo($urls[$tipo].'&conta=1'))
			$xml[]=$xmltmp;
			else unset($ativ[$tipo],$fontes[$tipo],$urls[$tipo]);
	}
	// Aqui eu recolho a quantidade de elementos passados pelo arquivo XML:
	foreach ($xml as $xtmp) {
		$numpts+=$xtmp->markers->pt['num'];
		// E aqui eu recolho as categorias (para checkboxes dinâmicos), caso existam:
		if ($xtmp->config['cats']) {
			$tmp=explode('||',$xtmp->config['cats']);
			foreach ($tmp as $tm) {
				$tmp2=explode('|',$tm);
				$ativ[$tmp2[0]]=$tmp2[1];
			}
		}
	}
	?>
	<h3><b>Fóruns encontrados:</b> <?=$numpts?></h3>
	<?php if ($busca) echo '<h4>'.strtolower($tra['busca']).': "<i>'.$busca.'</i>"</h4>';?>
	<form action="index.php" method="GET">
	<?php
	foreach ($_REQUEST as $tmp1=>$tmp2) {
		if (in_array($tmp1,array('option','task','id','Itemid')))
			echo '<input type="hidden" name="'.$tmp1.'" value="'.$tmp2.'">';
	}
	if ($numpts) {
		if ($mapa) {
			include $dir['phoogle']."/phoogle2.php";
			$map = new PhoogleMap();
			$map->cabecalho_sidebar=$tra['cabecalho_sidebar'];
			if ($ativ)
				$map->categorias=$ativ;
			$map->autozoom=true;
			$map->apiKey=$key;
			$map->numpts=$numpts;
			$map->gera_xml = $urls;
			$map->printGoogleJS();
			$map->controlType ='large';
			$map->showControl=true;
			$map->addMapType[]='G_PHYSICAL_MAP';
			$map->showType=true;
			$map->centerMap( '0','0', "mapa" );
			$map->tipo_mapa="G_PHYSICAL_MAP";
			$map->mapWidth=670;
			$map->mapHeight=500;
			$repetido=Array();
			?>
			<center>
			<table align="center" class="fareja_table" width="670px">
			<tr>
			<td>
			<center>
			<b>Filtros:</b><br /><br />
			<?php
			foreach ($ativ as $k=>$a)
				echo '<input type="checkbox" id="'.$k.'" name="'.$k.'" checked="checked" onclick="boxclick(this,\''.$k.'\')">&nbsp;'.$a.'&nbsp;';
			?>
			&nbsp;|&nbsp;&nbsp;
			Estado:
			<select name='UF' onchange="javascript:this.form.submit();">
			<option value="">---</option>
			<?php
			foreach ($lista_UFs as $iduf=>$estado) {
				if (!$id_mr || substr($iduf,0,1)==$id_mr) {
					echo '<option value="'.$iduf.'"';
					if ($id_uf==$iduf)
						echo ' SELECTED="SELECTED"';
					echo '>'.$estado.'</option>';
				}
			}
			?>
			</select>
			&nbsp;&nbsp;
			Macrorregião:
			<select name='MR' onchange="javascript:this.form.UF.value='';this.form.submit();">
			<option value="">---</option>
			<?php
			foreach ($lista_MRs as $idmr=>$macrorreg) {
				if (!$id_uf || $idmr==substr($id_uf,0,1)) {
					echo '<option value="'.$idmr.'"';
					if (in_array($idmr,array($id_mr,substr($id_uf,0,1))))
						echo ' SELECTED="SELECTED"';
					echo '>'.$macrorreg.'</option>';
				}
			}
			?>
			</select>
			</center>
			</td>
			</tr>
	<tr>
			<td><?=$map->showMap();?></td>
			<!--<td width="290" valign="top"> <div id="sidebar" style="overflow: auto; height: 500px;"><h3>Lista de Fóruns</h3><hr></div></td>!-->
			</tr>
			</table>
			</center>
			</form>
			<div id="carregando_mapa"><?=$tra['carregando_mapa']?></div>
			<?php
		} else {
			$tmp=explode('||',$xml->config['colunas']);
			foreach ($tmp as $tm) {
				$tmp2=explode('==',$tm);
				$tmp3=explode('|',$tmp2[1]);
				$col[$tmp2[0]]=$tmp3;
			}
			?>
			<table>
			<tr>
			<?php
				foreach ($col as $nome=>$campos)
				echo '<th>'.$nome.'</th>'.chr(10);
			?>
			</tr>
			<?php
			foreach ($xml->markers->pt as $pt) {
			?>
				<tr>
				<?=faz_msg($pt['msg'],$col,$mapa)?>
				</tr>
				<?php
			} // fim do loop nos dados $pt
			?>
			</table>
			<?php
		} // fim do else do if $mapa
	} else {
		echo '<h3>'.$tra['alerta_nada_encontrado'].'</h3>';
	}

?>
<p>Ajude a completar este mapa enviando informações, a partir do Fórum Estadual, sobre o Fórum Local que participas. Divulgando onde estamos organizados, empreendimentos e organizações de apoio que ainda não participam do Fórum terão informações sobre como entrar em contato e se integrar.</p>
<br><br><br>
<div id="fareja_rodape">
<p>Criação, desenvolvimento e implementação: <a href="mailto:dtygel@fbes.org.br">daniel tygel</a> (novembro de 2008; última atualização: novembro de 2008).</p>
<hr>
<p>Uma iniciativa do <a href="http://www.fbes.org.br" target="_blank">Fórum Brasileiro de Economia Solidária (www.fbes.org.br)</a>!</p>
<hr>
<p>A fonte dos dados é o levantamento realizado em novembro pela Secretaria Executiva do FBES</p>
</div>
<script language="JavaScript" type="text/javascript">
	<!--
		setTimeout( "fitMap( map, points );", 4500 );
	//-->
