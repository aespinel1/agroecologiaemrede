var map,
    CATS = [
      {'catName': 'ESCOLA', 'id': 0, 'text': 'Escola', 'icon': 'ESCOLA_cor_48.png', 'iconSize': [24,24], 'color': '#ff4b00', 'colorClass': 'yellow', 'visible':true, 'radius':160},
      {'catName': 'SEMENTES', 'id': 1, 'text': 'Sementes', 'icon': 'SEMENTES_cor_48.png', 'iconSize': [24,24], 'color': '#4238FF', 'colorClass': 'orange', 'visible':true, 'radius':160},
      {'catName': 'P1MC', 'id': 2, 'text': 'P1MC', 'icon': 'P1MC_cor_48.png', 'iconSize': [24,24], 'color': '#55BCBE', 'colorClass': 'pink', 'visible':true, 'radius':80},
      {'catName': 'P1+2', 'id': 3, 'text': 'P1+2', 'icon': 'P1+2_cor_48.png', 'iconSize': [24,24], 'color': '#D2204C', 'colorClass': 'red', 'visible':true, 'radius':80},
    ],
    BASE_LAYERS = [],
    BASE_MAPS = {},
    CATS_MARKERS,
    OVERLAY_LAYERS = [],
    QTTY = 0,
		msgs = [],
		markers;
    //ISMOBILE = ($.browser.mobile || ipad);

function loadData(target) {
	$('#carregando_mapa').removeClass('d-none');
	$.get(url.url+url.dados_get+'&outputFormat=json', function(data) {
    var firstLoad = (typeof CATS_MARKERS == 'undefined');
		CATS_MARKERS = data.markers;
    if (firstLoad) {
      initMap();
    }
		loadMarkers(target);
    if (firstLoad) {
      initMapLayerControl();
    }
	});
}

function initMap() {

  BASE_LAYERS.push(L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    maxNativeZoom: 17
  }));

  BASE_LAYERS.push(L.tileLayer('https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png', {
  	maxZoom: 17,
  	attribution: 'Map data: &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, <a href="http://viewfinderpanoramas.org">SRTM</a> | Map style: &copy; <a href="https://opentopomap.org">OpenTopoMap</a> (<a href="https://creativecommons.org/licenses/by-sa/3.0/">CC-BY-SA</a>)'
  }));

  BASE_LAYERS.push(L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
  	attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
  }));

  BASE_MAPS = {
    "Mapa": BASE_LAYERS[0],
    "Topografia": BASE_LAYERS[1],
    "Satélite": BASE_LAYERS[2],
  };

  map = L.map("map", {
    attributionControl: false,
    zoomControl: true,
    attributon: "Agroecologia em Rede",
    preferCanvas: true,
    scrollWheelZoom: true,
    layers: BASE_LAYERS[0]
  }).setView(new L.LatLng(-11.148, -38.328), 5.5);

  var credits = L.control.attribution().addTo(map);
  credits.addAttribution('<a href="http://agroecologiaemrede.org.br" target="_blank">Agroecologia em Rede</a>');
  //L.control.scale().addTo(map);
}

function loadMarkers(target) {
  if (CATS_MARKERS.length==0) {
    $('#map').addClass('d-none');
    $('#semResultados').removeClass('d-none');
    if (target) {
      $('.'+target).text(0);
      $('#carregando_mapa').addClass('d-none');
    }
    return;
  } else if ($('#map').hasClass('d-none')) {
    $('#semResultados').addClass('d-none');
    $('#map').removeClass('d-none');
  }
  var icon = new L.icon({
    iconUrl: '../mapas/imgs/defaultPin.png',
    iconSize: [21, 32],
    iconAnchor: [32, 32],
    popupAnchor: [-22, -34]
  });
  if (markers) {
    markers.remove();
  }
	markers = new L.markerClusterGroup();
  var drawn = [];
  for (i=0;i<CATS_MARKERS.length;i++) {
		if (typeof CATS_MARKERS[i].id != 'undefined') {
			var marker = new L.marker(new L.LatLng(CATS_MARKERS[i].lat,CATS_MARKERS[i].lng), {icon:icon, i:i});
			markers.addLayer(marker);
      drawn.push(i);
		}
  }
	markers.addTo(map);
	map.fitBounds(markers.getBounds());
	markers.on('click', showMarker);
  if (target) {
    $('.'+target).text(drawn.length);
    $('#carregando_mapa').addClass('d-none');
  }
}

function initMapLayerControl() {
  /*L.Control.SEARCHBAR = L.Control.extend({
    onAdd: function(map) {
      var div = L.DomUtil.create('div', 'input-group');
      var prepend = L.DomUtil.create('div', 'input-group-prepend', div);
      var inputText = L.DomUtil.create('div', 'input-group-text', prepend);
      var i = L.DomUtil.create('i', 'fa fa-search', inputText);
      var input = L.DomUtil.create('input', 'form-control', div);
      input.placeholder = 'buscar município ou estado';
      input.id = 'search';
      return div;
    },
    onRemove: function(map) {
        // Nothing to do here
    }
  });
  L.control.searchbar = function(opts) {
    return new L.Control.SEARCHBAR(opts);
  }
  L.control.searchbar({ position: 'topright' }).addTo(map);
	*/

  // Select of layers:
  var overlays = {};
  L.control.layers(BASE_MAPS, overlays, {collapsed: true}).addTo(map);
  map.on("load", mapInitialized());
}

function mapInitialized() {
	$('#carregando_mapa').addClass('d-none');
}

function showMarker(marker) {
	var i = marker.layer.options.i;
	if (typeof msgs[i] == 'undefined') {
		var target = url.url + '?c=1&f=infos&nomeFrm='+CATS_MARKERS[i].nomefrm+'&idPt='+CATS_MARKERS[i].id+'&outputFormat=json';
		$.get(target, function (data) {
			marker.layer.bindPopup(data.info);
			marker.layer.openPopup();
		});
	}
}
