<?php
require_once "../mapeo/config.php";
$nomebase = 'Brasil_Indigena_legenda';
$nomearquivo = "gera_$nomebase";
$tab = "camadas_$nomebase";
include $dir['apoio'].'funcoes_comuns.php';
include $dir['apoio'].'zip.lib.php';

$db = conecta($bd);


$minlat = $_GET['minlat'];
$minlng = $_GET['minlng'];
$maxlat = $_GET['maxlat'];
$maxlng = $_GET['maxlng'];
$verifica = $_GET['verifica'];
$tamanho_maximo = 1500000;

$mbr = "GeomFromText('LineString($minlng $minlat,$maxlng $maxlat)')";

$sql = "SELECT {campos} FROM $tab WHERE MBRIntersects(coords, $mbr)=1";

if ($verifica) {
	$sql = str_replace('{campos}',"SUM(tam) as 'tam'", $sql);
	$dados = faz_query($sql,'','object');
	if ($dados)
		$tam = $dados[0]->tam;
	if (!$dados)
		$msg='Não há Territórios indígenas da Amazônia nesta área';
	elseif ($tam<$tamanho_maximo)
		$msg='ok';
	else $msg="Nesta área há uma quantidade muito grande de Territórios Indígenas (".number_format(($tam/(1024*1024)),2,',','.')."MB), o que ultrapassa nossa capacidade de processamento. Favor aproximar mais o zoom e tentar novamente";
	echo json_encode($msg);
	
} else {
	$sql = str_replace('{campos}',"id,coords_kml,estilo", $sql);
	$dados = faz_query($sql,'','object');

	if ($dados)
		foreach ($dados as $d)
			$corpo.=gera_kml_placemark($d->nome,$d->coords_kml,$d->estilo);

	$zip = new zipfile("$nomearquivo.kmz");
	$tam = $zip->addFile(gera_kml_geral($corpo), "$nomearquivo.kml");
	if ($dados && $tam<$tamanho_maximo) {
		ob_start();
			echo $zip->file();
		ob_end_flush();
	}
}

function gera_kml_geral($corpo) {
	$str='<?phpxml version="1.0" encoding="UTF-8"?><kml xmlns="http://earth.google.com/kml/2.0">
<Document>
  <name>Brasil_indigena_legenda</name>
  <Style id="Style2">
    <PolyStyle>
      <color>ff222222</color>
      <outline>0</outline>
      <fill>1</fill>
    </PolyStyle>
  </Style>{corpo}
</Document>
</kml>';
	return str_replace('{corpo}',$corpo,$str);
}

function gera_kml_placemark($nome,$coords,$estilo) {
	$str="<Placemark>
	<styleUrl>#$estilo</styleUrl>
	<Polygon>
	  <outerBoundaryIs>
	    <LinearRing>
	      <coordinates>$coords
          </coordinates>
        </LinearRing>
      </outerBoundaryIs>
    </Polygon>
  </Placemark>";
	return $str;
}

?>

