<?php
require_once "../mapeo/config.php";
$nomebase='UC';
$nomearquivo = "gera_$nomebase";
$tab = "camadas_$nomebase";
include $dir['apoio'].'funcoes_comuns.php';
include $dir['apoio'].'zip.lib.php';

$db = conecta($bd);


$minlat = $_GET['minlat'];
$minlng = $_GET['minlng'];
$maxlat = $_GET['maxlat'];
$maxlng = $_GET['maxlng'];
$verifica = $_GET['verifica'];
$tamanho_maximo = 1500000;

$sql = "SELECT {campos} FROM $tab WHERE minlat<$maxlat && minlng<$maxlng && maxlat>$minlat && maxlng>$minlng";

if ($verifica) {
	$sql = str_replace('{campos}',"SUM(tam) as 'tam'", $sql);
	$dados = faz_query($sql,'','object');
	if ($dados)
		$tam = $dados[0]->tam;
	if (!$dados)
		$msg='Não há Unidades de Conservação nesta área';
	elseif ($tam<$tamanho_maximo)
		$msg='ok';
	else $msg='Nesta área há uma quantidade muito grande de Unidades de Conservação, o que ultrapassa nossa capacidade de processamento. Favor aproximar mais o zoom e tentar novamente';
	echo json_encode($msg);
	
} else {
	$sql = str_replace('{campos}',"id,nome,coords, IF(tipo='RESEX','RESEX','UC') as estilo", $sql);
	$dados = faz_query($sql,'','object');

	if ($dados)
		foreach ($dados as $d)
			$corpo.=gera_kml_placemark($d->nome,$d->coords,$d->estilo);

	/*header('Content-type: text/xml; charset=utf-8');
	echo gera_kml_geral($corpo);*/
	$zip = new zipfile("$nomearquivo.kmz");
	$tam = $zip->addFile(gera_kml_geral($corpo), "$nomearquivo.kml");
	if ($dados && $tam<$tamanho_maximo) {
		ob_start();
			echo $zip->file();
		ob_end_flush();
	}
}

function gera_kml_geral($corpo) {
	$str='<?phpxml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
<Document>
  <name>Unidades de Conservação</name>
  <Style id="UC">
		<LineStyle>
			<color>ff006600</color>
			<width>2</width>
		</LineStyle>
		<PolyStyle>
			<fill>1</fill>
			<color>88229922</color>
		</PolyStyle>
	</Style>
	<Style id="RESEX">
		<LineStyle>
			<color>ff006600</color>
			<width>2</width>
		</LineStyle>
		<PolyStyle>
			<fill>1</fill>
			<color>88bb5522</color>
		</PolyStyle>
	</Style>{corpo}
</Document>
</kml>';
	return str_replace('{corpo}',$corpo,$str);
}

function gera_kml_placemark($nome,$coords,$estilo) {
	$str="
  <Placemark>
    <name>$nome</name>
	<styleUrl>#$estilo</styleUrl>
	<Polygon>
	  <outerBoundaryIs>
	    <LinearRing>
	      <coordinates>$coords
          </coordinates>
        </LinearRing>
      </outerBoundaryIs>
    </Polygon>
  </Placemark>";
	return $str;
}

?>

