<?php
// por daniel tygel (dtygel@fbes.org.br) em 2009
// last update: 2018

require_once "../mapeo/config.php";
mb_internal_encoding("UTF-8");
require_once $dir['apoio']."funcoes_comuns.php";
//Faço a conexão ao banco de dados:
$db = conecta($bd);

if ($nomefrm = $_REQUEST['nomeFrm']) {
	$sql = "SELECT * FROM mapeo_formularios WHERE nomefrm = '".$nomefrm."'";
	$res = faz_query($sql,'','object');
	$tipofrm = $res[0]->tipo;
} else {
	$tipofrm = isset($_REQUEST['tipofrm'])
		? $_REQUEST['tipofrm']
		: '';
}
$outputFormat = (isset($_REQUEST['outputFormat']) && in_array($_REQUEST['outputFormat'], array('xml','json','html')))
	? $_REQUEST['outputFormat']
	: 'xml';
$idpt = isset($_REQUEST['idPt'])
	? $_REQUEST['idPt']
	: '';
$visao=isset($_REQUEST['visao'])
	? $_REQUEST['visao']
	: 'tabela';
$mostratabela = ($visao=='tabela');
$conta=isset($_REQUEST['conta'])
	? $_REQUEST['conta']
	: '';
$lingua = ($_REQUEST['lingua'])
	? $_REQUEST['lingua']
	: $_COOKIE['agrorede_lingua'];
$datai = isset($_REQUEST['datai'])
	? $_REQUEST['datai']
	: '';
$dataf = isset($_REQUEST['dataf'])
	? $_REQUEST['dataf']
	: '';
$tipodata = isset($_REQUEST['tipodata'])
	? $_REQUEST['tipodata']
	: '';
$pgAtual = isset($_REQUEST['pgLista'])
	? $_REQUEST['pgLista']
	: 1;
$orderby = isset($_REQUEST['orderby'])
	? $_REQUEST['orderby']
	: 'nome';
$order = isset($_REQUEST['order'])
	? ' '.$_REQUEST['order']
	: '';

$filtro = resgata_filtros($tipofrm);
$geos = explode('|',$_REQUEST[$campo[$filtro->tab]->localizacao]);
//if ($geos = $filtro->sels[$campo[$filtro->tab]->localizacao]) {
if ($geos) {
	rsort($geos);
	$geo = organiza_geos($geos[0],$_REQUEST['distmax'],$lingua);
}
$f=isset($_REQUEST['f'])
	? $_REQUEST['f']
	: '';
$include_images = isset($_REQUEST['include_images'])
	? $_REQUEST['include_images']
	: false;

$sql = "SELECT * FROM mapeo_formularios WHERE tipo='".$tipofrm."'";
if ($filtro->sels['nomefrm']) {
	$sql .= " AND nomefrm IN (";
	foreach ($filtro->sels['nomefrm'] as $o)
		$sql .= "'".$o."',";
	$sql = substr($sql,0,-1).")";
}
$frms = faz_query($sql,'','object');
unset($filtro->sels['nomefrm'],$filtro->mapa['nomefrm']);

// Aqui eu começo a preparar o resultado (a raiz é o FAREJA):
switch ($outputFormat) {
	case 'xml':
	case 'html':
		$docxml = new DOMDocument("1.0");
		$docxml->formatOutput=true;
		$node = $docxml->CreateElement("fareja");
		$raiz = $docxml->AppendChild($node);
		break;
	case 'json':
		$json = new StdClass();
		break;
}

//Aqui monto o resultado conforme o pedido realizado
switch ($f) {
	case 'infos':
		if ($idpt && $nomefrm) {
			$from = " FROM ".$nomefrm." as a";
			$where = " WHERE a.".$campo[$nomefrm]->id."='".$idpt."'";
			if ($frms[0]->tab_base_comum) {
				$from2 = ", ".$frms[0]->tab_base_comum." as b";
				$where .= " AND a.".$campo[$nomefrm]->id."=b.".$campo[$frms[0]->tab_base_comum]->id;
			}
			$sql = "SELECT ";
			foreach ($filtro->info as $c=>$tit) {
				$cmdo = $filtro->cmd[$c];
				$m = $filtro->mapa[$c];
				if (isset($cmdo->apoio) && $cmdo->apoio) {
					$tmp = " LEFT JOIN ".$cmdo->apoio." ON ".$cmdo->apoio.".".$cmdo->campo_id."=".$c;
					if ($m->frm == $nomefrm) {
						$from .= $tmp;
						$sql_compl = "a.";
					} else {
						$from2 .= $tmp;
						$sql_compl = "b.";
					}
					$sql_compl .= $c." AS 'id_".$c."', ";
					$sql .= $cmdo->apoio.".".$cmdo->campo_nome." AS '".$c."', ".$sql_compl;
				} else {
					$tmp = ($m->frm == $nomefrm)
						? "a.".$c
						: "b.".$c;
					$sql .= ($m->tipo_mysql=='date')
						? "UNIX_TIMESTAMP(".$tmp.") AS '".$c."', "
						: $tmp.", ";
				}
			}
			$sql = substr($sql,0,-2);
			$sql.=$from.$from2.$where;

			$dados = faz_query($sql,'','object');
			$dado=$dados[0];
			$res=array();
			$tab=array();
			$hi = '<a href="?c=1&f=consultar&tipofrm='.$tipofrm;
			$hf = '</a>';
			foreach ($filtro->info as $c=>$tit) {
				$m = $filtro->mapa[$c];
				$cmdo_mapa = $filtro->cmd_mapa[$c];
				$campo_id='id_'.$c;
				$titulo_campo = $tit;
				if ($dado->$c) {
					$n=count($res);
					if (!isset($res[$n])) {
						$res[$n] = new StdClass();
					}
					$res[$n]->campo=$c;
					switch ($m->tipo_form) {
						case 'georref':
							$geotmp = organiza_geos($dado->$c);
							$dado->$campo_id = $dado->$c;
							$c0='&'.$c.'_0='.$geotmp->pais->id;
							$c1='&'.$c.'_1='.$geotmp->estado->id;
							$c2='&'.$c.'='.$geotmp->cidade->id;
							$dado->$c =
								$hi.$c0.$c1.$c2.'">'.$geotmp->cidade->nome.$hf
								. ' / '
								. $hi.$c0.$c1.'">'.estadoParaUF($geotmp->estado->nome).$hf
								. ', '
								. $hi.$c0.'">'.$geotmp->pais->nome.$hf;
						break;
						case 'file':
							$arqsRaw = explode('|',$dado->$c);
							$arqs = array();
							$arq = '';
							foreach ($arqsRaw as $arqTmp) {
								if (file_exists($dir['upload'].$arqTmp) && is_array(getimagesize($dir['upload'].$arqTmp))) {
									$arqs[]=$arqTmp;
								}
							}
							if (count($arqs)) {
								$arq = $arqs[array_rand($arqs)];
								$imagem = '<a href="'.str_replace('{id}',$idpt,$dir['mostra_'.$tipofrm.'_URL']).'"><img class="w-100 rounded" src="'.getResizedImgURL($arq, 400, 400).'"></a>';
							}
						break;
						default:
							if ($m->tipo_mysql=='date')
								$dado->$c = date('d/m/Y',$dado->$c);
							if ($campo[$filtro->tab]->nome==$c) {
								$titulo_campo='';
								$res[$n]->css='_titulo';
								$dado->$c =
									'<a href="'.str_replace('{id}',$idpt,$dir['mostra_'.$tipofrm.'_URL']).'">'
									. $dado->$c . '</a>';
							}
						break;
					}
					if ($m->tipo_form!='file') {
						if ($titulo_campo) {
							$res[$n]->tit=$titulo_campo;
						}
						$res[$n]->txt=traduz($dado->$c,$lingua);
						if ($dado->$campo_id) {
							$res[$n]->idref=$dado->$campo_id;
						}
					}
				}
			}
			$html = '<p class="popup-title">'.$res[0]->txt.'</p>';
			if (isset($imagem)) {
				$html .= '<div class="row"><div class="md-12">'.$imagem.'</div></div>';
			}
			$html .= '<div class="row">';
			$html.='<dl>';
			for ($i=1;$i<count($res);$i++) {
				$r = $res[$i];
				if ($r->tit) {
					$html .= '<dt>'.$r->tit.'</dt>';
				}
				$html .= '<dd>'.$r->txt.'</dd>';
			}
			$html .= '</dl>';
			$html .= '</div></div>';
			switch ($outputFormat) {
				case 'xml':
				case 'html':
					$node = $docxml->CreateElement("markers");
					$nivel1 = $raiz->AppendChild($node);
					$node = $docxml->createElement("info");
					$newnode = $nivel1->appendChild($node);
					//$newnode->setAttribute("msg", json_encode($res));
					$newnode->setAttribute("msg", $html);
					break;
				case 'json':
					$json->markers = array();
					$json->info = $html;
					break;
			}
		}
		break;
	case 'pts':
	default:
		// Dados iniciais de configuração
		switch ($outputFormat) {
			case 'xml':
			case 'html':
				$node = $docxml->CreateElement("config");
				$nivel1 = $raiz->AppendChild($node);
				$nivel1->SetAttribute("tipo", $icone_tipo_padrao);
				$nivel1->SetAttribute("cor", $icone_cor_padrao);
				// Agora os pontos:
				$node = $docxml->CreateElement("markers");
				$nivel1 = $raiz->AppendChild($node);
				break;
			case 'json':
				$t = new StdClass();
				$t->tipo = $icone_tipo_padrao;
				$t->cor = $icone_cor_padrao;
				$json->config = $t;
				$json->markers = array();
				break;
		}

		// Loop nos formulários de tipo $tipofrm, que por enquanto podem ser experiências ou instituições:
		$dados = ($conta)
			? 0
			: array();
		foreach ($frms as $frm) {
			unset($leftjoin);
			$cmpo = $campo[$frm->nomefrm];
			$sql = "SELECT '".$frm->nomefrm."' as 'nomefrm'";
			if ($cmpo->descricao && $mostratabela && !$conta) {
				$sql .= ", ".$cmpo->descricao." AS 'descricao'";
				$sql .= ", ".$cmpo->criacao." AS 'criacao'";
			}
			foreach ($filtro->geraXML as $gcampo=>$gnome)
				if ($gnome == 'abrangencia_latlng')
					$sql .= ", REPLACE(a.".$gcampo.",'0,00/0,00;0,00/0,00','') as 'abrangencia_latlng'";
					else $sql .= ($gnome)
						? ", a.".$gcampo." AS '".$gnome."'"
						: ", a.".$gcampo;

			if ($geo->distmax)
				$sql.= ", ".sql_distancia_geo($geo->cidade,$cmpo->localizacao_lat,$cmpo->localizacao_lng)." as distancia";

			// FROM:
			$from = ' FROM ';
			$from .= ($frm->tab_base_comum)
				? $frm->nomefrm.' as b, '.$frm->tab_base_comum.' as a'
				: $frm->nomefrm.' as a';

			// WHERE:
			$where = " WHERE 1";
			if ($frm->tab_base_comum)
				$where .= " AND b.".$cmpo->id."=a.".$campo[$frm->tab_base_comum]->id;
			if ($geo->distmax)
				$where.=" AND ".sql_distancia_geo($geo->cidade,$cmpo->localizacao_lat,$cmpo->localizacao_lng)." < ".$geo->distmax;
			if ($datai || $dataf) {
				if ($tipodata=="ano")
					$where .= " AND YEAR(".$cmpo->localizacao.")";
					elseif ($tipodata="mes")
						$where .= " AND MONTH(".$cmpo->localizacao.")";
							else $where .= " AND ".$cmpo->localizacao;
				if ($datai && $dataf) {
					$datatxt = " BETWEEN $datai AND $dataf";
				} elseif ($datai) {
					$datatxt = ">=$datai";
				} else {
					$datatxt = "<=$dataf";
				}
				$where.=$datatxt;
			}
			// Este é para prevenir do campo de localização não seguir a regra de ter 11 dígitos.
			if ($gcampo == $cmpo->localizacao)
				$where.=" AND LENGTH(".$cmpo->localizacao.")=11 AND LOCATE(' ',".$cmpo->localizacao.")=0";
			// WHEREs dos filtros (se houver!):
			if (isset($filtro->sels) && $filtro->sels) {
				foreach ($filtro->sels as $sel_campo=>$sel_valor) {
					$cmdo = $filtro->cmd[$sel_campo];
					// Antes de mais nada eu percorro a árvore se o campo é de árvore, para que a escolha do usuário implique também na escolha dos filhos do item escolhido!
					$sub_sel_valor = array();
					if ($filtro->mapa[$sel_campo]->tipo_form == 'arvore') {
						foreach ($sel_valor as $v) {
							$campo_id = $cmdo->campo_id;
							$sub_sel_valor[$v][] = $v;
							if ($arvore = resgata_arvore($cmdo->apoio,$campo_id,$cmdo->campo_nome,$cmdo->campo_mae,$v))
								foreach ($arvore as $a) {
									//if (!in_array($a->$campo_id,$sel_valor))
										//$sel_valor[] = $a->$campo_id;
									$sub_sel_valor[$v][] = $a->$campo_id;
								}
						}
					}
					// Pronto, agora eu gero o $where, dependendo se é o campo de localização ou se é dos demais. Não gero $where se o distmax estava definido e é de localização, pois já foi feito o where lá em cima.
					if ($sel_campo == $cmpo->localizacao && !$geo->distmax) {
						if (is_array($sel_valor)) {
							rsort($sel_valor);
							$sel_valor=$sel_valor[0];
						}
						$where .= " AND SUBSTRING(".$cmpo->localizacao.",1,".strlen($sel_valor).")='".$sel_valor."'";
					} elseif (!$geo->distmax) {
						$where .= " AND (";
						$tipo_juncao = " AND ";
						$tipo_juncao_tam = strlen($tipo_juncao);
						foreach ($sel_valor as $v) {
							if ($sub_sel_valor[$v]) {
								$where .= "(";
								foreach ($sub_sel_valor[$v] as $s)
									$where .= "LOCATE('".$s."', ".$sel_campo.")>0 OR ";
								$where = substr($where,0,-4).") ".$tipo_juncao;
							} else
								$where .= "LOCATE('".$v."', ".$sel_campo.")>0".$tipo_juncao;

						}
						$where = substr($where,0,-$tipo_juncao_tam)." )";
					}
				}
			}


			//Ainda preciso fazer o foreach de um campo de buscas dentro da variável $filtro!
			if ($filtro->textoBusca && is_array($cmpo->textoBusca)) {
				$where .= " AND (";
				foreach ($cmpo->textoBusca as $c)
					$where .= $c." LIKE '%".$filtro->textoBusca."%' OR ";
				$where = substr($where,0,-4).")";
			}

			// ORDER BY:
			$orderbySql = " ORDER BY ";
			if ($geo->distmax) {
				$orderbySql.="distancia, ";
			}
			$orderbySql .= $orderby.$order;


			// Aqui nós adicionamos as definições de ícones de tabelas de apoio em que o $cmdo_mapa->icone está definido. Isso é algo para lembrar: sempre que eu colocar $cmdo_mapa->icone ou cor = 1, devem estar definidas as colunas icone_cor e icone_tipo na tabela de apoio!! Se estas 2 colunas não existirem, dá erro!
			foreach ($filtro->mapa as $m) {
				//if ($m>campo != 'ex_areas_tematicas') {
				$cmdo_mapa = $filtro->cmd_mapa[$m->campo];
				$cmdo = $filtro->cmd[$m->campo];
				// Um eventual where do $cmdo_mapa tem primazia sobre o where do $cmdo:
				if ($cmdo->apoio && $filtro->opcoes[$m->campo]) {
					if ($cmdo_mapa->where)
						$cmdo->where = $cmdo_mapa->where;
					// WHEREs que vêm no cmdo ou cmdo_mapa para restringir valores das tabelas de apoio:
					if ($cmdo->where) {
						$cmdo->where = str_replace('{campo_id}',$m->campo,$cmdo->where);
						$cmdo->where = str_replace('{campo_nome}',$cmdo->campo_nome,$cmdo->where);
						$where .= " AND ".$cmdo->where;
					}
					// Definições de cor e tipo de ícone vindos das tabelas de apoio:
					if ($cmdo_mapa->icone) {
						$sql .= ($include_images)
							? ", ANY_VALUE(".$cmdo->apoio.".icone_cor) as ".$cmdo->apoio."_icone_cor, GROUP_CONCAT(".$cmdo->apoio.".icone_tipo) as ".$cmdo->apoio."_icone_tipo"
							: ", ".$cmdo->apoio.".icone_cor as ".$cmdo->apoio."_icone_cor, ".$cmdo->apoio.".icone_tipo as ".$cmdo->apoio."_icone_tipo";
						$leftjoin .= " LEFT JOIN ".$cmdo->apoio." ON ".$cmdo->apoio.".".$cmdo->campo_id."=".$m->campo;
						$where .= " AND (".$cmdo->apoio.".icone_cor<>'-1' AND ".$cmdo->apoio.".icone_tipo<>'-1')";
						$tabs_definem_icones[]=$cmdo->apoio;
					}
				}
			}

			if ($include_images) {
				$leftjoin .= " LEFT JOIN experiencia_arquivo d ON a.ex_id=d.ea_id_experiencia";
				$sql .= ", GROUP_CONCAT(d.ea_arquivo SEPARATOR ',') AS imgs";
				$groupby = " GROUP BY a.ex_id";
			} else {
				$groupby = "";
			}

			$sql.=$from.$leftjoin.$where.$groupby.$orderbySql;
			//pR($sql);
				if ($conta) {
					$dados += faz_query($sql,'','num');
				} else {
					$res = faz_query($sql,'','object');
					if ($res)
						$dados = array_merge($dados,$res);
				}
		} // fim do loop nos formulários do tipo $tipofrm
		//exit;
		//print_r($dados);exit;
		if ($conta) {
			// Se era pra contar, o resultado tem apenas o total de pontos:
			switch ($outputFormat) {
				case 'xml':
				case 'html':
					$node = $docxml->createElement("pt");
					$newnode = $nivel1->appendChild($node);
					$newnode->setAttribute("num", $dados);
					break;
				case 'json':
					$json = new StdClass();
					$json->num = $dados;
					break;
			}

		} else {
			// Foi feito o select que tirou os pontos. Entretanto, é preciso agora acertar mais detalhes, depedendo se estou gerando dados para mapa ou para tabela:
			$paises = $cidades = $abrangencias = $latlngs = array();
			$n = count($dados);
			// Os resultados devem ser limitados de acordo com o parâmetro do usuário, se for lista. Se for mapa, aparece tudo!
			if ($mostratabela) {
				$pgs = ceil($n/$padrao_itens_por_pagina);
				switch ($outputFormat) {
					case 'xml':
					case 'html':
						$numnode = $docxml->createElement("num");
						$novonode = $raiz->AppendChild($numnode);
						$novonode->setAttribute("num", $n);
						$novonode->setAttribute("pgs", $pgs);
						break;
					case 'json':
						$t = new StdClass();
						$t->num = $n;
						$t->pgs = $pgs;
						$json->num = $t;
						break;
				}
				if (!isset($_REQUEST['mostraTodos'])) {
					if ($pgAtual>1) {
						$min = ($pgAtual-1)*$padrao_itens_por_pagina;
						$dados = array_slice($dados,$min);
					}
					$dados = array_slice($dados,0,$padrao_itens_por_pagina);
					$n = count($dados);
				}
			}
			foreach ($dados as $i=>$d) {
				if ($t = $d->localizacao)
					if ($mostratabela) {
						// Se é o resultado para tabela, precisarei dos nomes das cidades e estados:
						if (!in_array(substr($t,0,2),$paises))
							$paises[]=substr($t,0,2);
						if (!in_array($t,$cidades))
							$cidades[]=$t;
					} else {
						// Se é para mapa, só pego as cidades e estados para o caso desta tabela não ter já alimentado automaticamente o localizacao_lat!
						if (!$campo[$filtro->tab]->localizacao_lat) {
							if (!in_array(substr($t,0,2),$paises))
								$paises[]=substr($t,0,2);
							if (!in_array($t,$cidades))
								$cidades[]=$t;
						}

					// e aqui é também só para mapa: incluir os pontos de abrangência e também definir a cor e tipo do ícone dos dados alimentados pelo mysql:
					if ($d->abrangencia) {
						$tmp = explode('|',$d->abrangencia);
						foreach ($tmp as $t) {
							if ($t) {
								if (!in_array(substr($t,0,2),$paises))
									$paises[]=substr($t,0,2);
								if (!in_array($t,$cidades))
									$cidades[]=$t;
								$abrangencias[$t][]=$d->id;
							}
						}
					}
					if ($d->abrangencia_latlng) {
						$tmp1 = explode(';',$d->abrangencia_latlng);
						foreach ($tmp1 as $t) {
							$tmp2 = explode('/',str_replace(',','.',$t));
							if ((floatval($tmp2[0]) && floatval($tmp2[1]))) {
								$tmpid=$d->id;
								$t = new StdClass();
								$t->id_mae=$tmpid;
								$t->localizacao_lat=$tmp2[0];
								$t->localizacao_lng=$tmp2[1];
								$t->icone_tipo = 'Pontogde';
								$t->icone_cor = 'Verde';
								$dados[$n] = $t;
								// para ficar hachurado, veremos!
								if ($mostra_hachurado) {
									$dados[$i]->localizacao_lat .= ','.$dados[$n]->localizacao_lat;
									$dados[$i]->localizacao_lng .= ','.$dados[$n]->localizacao_lng;
								}
								$n++;
							}
						}
					}
					if ($tabs_definem_icones) {
						$cores = $tipos = array();
						foreach ($tabs_definem_icones as $apoio) {
							$txt_cor = $apoio.'_icone_cor';
							$txt_tipo = $apoio.'_icone_tipo';
							if ($d->$txt_cor)
								$cores[]=$d->$txt_cor;
							if ($d->$txt_tipo)
								$tipos[]=$d->$txt_tipo;
							unset($dados[$i]->$txt_cor,$dados[$i]->$txt_tipo);
						}
						if (count($cores))
							$dados[$i]->icone_cor = $cores[0];
						if (count($tipos))
							$dados[$i]->icone_tipo = $tipos[0];
					}
				} // fim do else do if $mostratabela
			} // fim do loop nos dados
			// Agora resgato os dados de latitude e longitude das cidades que são abrangência e/ou das cidades de tabelas que não tenham lat/lng definidas e apenas cidade:
			if (count($paises)) {
				foreach ($paises as $pais) {
					$sql = "
						SELECT a.id, a.nome as cidade, b.nome as estado, c.nome as pais, a.lat, a.lng
						FROM _".$pais." as a
							LEFT JOIN _".$pais."_maes as b ON a.id_mae=b.id
							LEFT JOIN __paises as c ON c.id='".$pais."'
						WHERE a.id IN (";
					foreach ($cidades as $c) {
						if (substr($c,0,2)==$pais)
							$sql.="'".$c."', ";
					}
					$sql = substr($sql,0,-2).")";
					$res = faz_query($sql,'','object');
					if ($res)
						$latlngs = array_merge($latlngs,$res);
				}
				// Definição das lat/lng se não estão na tabela:
				if (!$campo[$filtro->tab]->localizacao_lat || $mostratabela)
					foreach ($dados as $n=>$dado)
						foreach ($latlngs as $latlng)
							if ($latlng->id == $dado->localizacao) {
								if (!$mostratabela) {
									$dados[$n]->localizacao_lat = $latlng->lat;
									$dados[$n]->localizacao_lng = $latlng->lng;
								} else {
									// Se vai mostrar a tabela, devo colocar a cidade/estado/pais:
									$dados[$n]->cidadeNome = $latlng->cidade;
									$dados[$n]->estadoNome = $latlng->estado;
									$dados[$n]->paisNome = traduz($latlng->pais,$lingua);
								}
							}
				// Definição das abrangências:
				foreach ($latlngs as $latlng)
					if ($abrangencias[$latlng->id])
						foreach ($abrangencias[$latlng->id] as $a) {
							$n++;
							$t = new StdClass();
							$t->id_mae=$a;
							//$t->cidade=$latlng->id;
							$t->localizacao_lat=$latlng->lat;
							$t->localizacao_lng=$latlng->lng;
							$t->icone_tipo = 'Pontogde';
							$t->icone_cor = 'Verde';
							$dados[$n] = $t;

							// para ficar hachurado, veremos!
							if ($mostra_hachurado)
								foreach ($dados as $di=>$da)
									if ($da->id == $a) {
										$dados[$di]->localizacao_lat .= ','.$latlng->lat;
										$dados[$di]->localizacao_lng .= ','.$latlng->lng;
									}
						}
			}
			// Pronto, criei a tabela de dados final. O lance agora é tranformá-la no XML para envio (já fazendo os desvios aleatórios das latitudes e longitudes que batem no mesmo ponto (monitorados por $lats e $lngs):
			$lats = $lngs = array();
			foreach ($dados as $d) {
				if (in_array($d->localizacao_lng,$lngs) || in_array($d->localizacao_lat,$lats)) {
					$d->localizacao_lng+=rand(-500,0)*(0.03/500);
					$d->localizacao_lat+=rand(-500,0)*(0.03/500);
				} else {
					$lngs[]=$d->localizacao_lng;
					$lats[]=$d->localizacao_lat;
				}

				$o = new StdClass();
				if ($d->id) {
					$o->id = $d->id;
					$o->nomefrm = $d->nomefrm;
					$o->nome = $d->nome;
					// Se é lista, mostra-se, além do nome, também a cidade, estado, país e o resumo pelo overlib
					if ($mostratabela) {
						$o->url = str_replace('{id}',$d->id,$dir['mostra_'.$tipofrm.'_URL']);
						$o->cidade = $d->cidadeNome;
						$o->estado = $d->estadoNome;
						$o->pais = $d->paisNome;
						$o->descricao = $d->descricao;
						if (isset($d->criacao)) {
							$o->criacao = $d->criacao;
						} else if (isset($d->dt_criacao)) {
							$o->criacao = $d->dt_criacao;
						}
						if ($include_images) {
							$o->imgs = $d->imgs;
						}
					}
				} elseif ($d->id_mae) {
					$o->id_mae = $d->id_mae;
				}
				$o->lat = $d->localizacao_lat;
				$o->lng = $d->localizacao_lng;
				if ($d->distancia)
					$o->distancia = $d->distancia;
				if ($d->icone_cor)
					$o->cor = $d->icone_cor;
				if ($d->icone_tipo)
					$o->tipo = $d->icone_tipo;
				if ($include_images && $d->imgs) {
					$o->imgs = $d->imgs;
				}

				switch($outputFormat) {
					case 'xml':
					case 'html':
						$node = $docxml->createElement("pt");
						$newnode = $nivel1->appendChild($node);
						foreach(get_object_vars($o) as $key=>$value) {
							$newnode->setAttribute($key, $value);
						}
						break;
					case 'json':
						$json->markers[] = $o;
						break;
				}
			}
		} // fim do if $conta
	break;
}

header('Content-Type: text/'.$outputFormat.'; charset=utf-8');

switch ($outputFormat) {
	case 'xml':
		echo $docxml->saveXML();
		break;
	case 'json':
		echo json_encode($json);
		break;
	case 'html':
		$filtro = resgata_filtros($tipofrm);
		$resultados = formata_dados_xml_agrorede(array(simplexml_load_string($docxml->saveXML())), $visao);
		mostraTabela($filtro, $resultados->xml, $tipofrm, $resultados->numpts, $resultados->numpgs, $pgAtual);
		break;
}

function mensagem_html($dados) {
global $cidade_ref, $id_cidade_ref, $filtro;
	$dados->ex_descricao=wordwrap($dados->ex_descricao,30,"<br />");
	$dados->ex_chamada=wordwrap($dados->ex_chamada,30,"<br />");
	$dados->ex_resumo=wordwrap($dados->ex_resumo,30,"<br />");
	$dados->txt_comentario=wordwrap($dados->txt_comentario,30,"<br />");
	$dados->tema=wordwrap(str_replace('|',', ',$dados->tema),30,"<br />");
	$dados->local=wordwrap(str_replace('|',', ',$dados->local),30,"<br />");
	if ($filtro->busca) {
		$dados->ex_descricao=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_descricao);
		$dados->ex_chamada=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_chamada);
		$dados->ex_resumo=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_resumo);
		$dados->ex_tema=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_tema);
	}
	$texto.=$dados->ex_descricao.'|'
		. $dados->ex_chamada.'|'
//		. $dados->ex_resumo.'|'
//		. $dados->txt_comentario.'|'
		. urlencode($dados->ea_arquivo).'|'
		. $dados->tema.'|'
		. $dados->local.'|'
		. $dados->ex_id;
	return $texto;
}
?>
