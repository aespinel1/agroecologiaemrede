<?php
/**
* Phoogle Maps 2.0 | Uses Google Maps API to create customizable maps
* that can be embedded on your website
*    Copyright (C) 2005  Justin Johnson
*    Extensively modified by:
*    Daniel Tygel - september 2008
*
*    This program is free software; you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation; either version 2 of the License, or
*    (at your option) any later version.
*
*
* Phoogle Maps 2.0
* Uses Google Maps Mapping API to create customizable
* Google Maps that can be embedded on your website
*
* @class        Phoogle Maps 2.0
* @author       Justin Johnson <justinjohnson@system7designs.com>
* @copyright    2005 system7designs
*/
//Nossa, aqui eu mexi muito muito muito.....
//Enfim: alterações por daniel tygel <dtygel em fbes pt org pt br> em 2006, 2007 e 2008.

class PhoogleMap{
/**
* mapWidth
* width of the Google Map, in pixels
*/
    var $mapWidth = 300;
/**
* mapHeight
* height of the Google Map, in pixels
*/
    var $mapHeight = 300;
/**
* tipo_mapa
* mapa padrao do GoogleMaps
*/
	var $tipo_mapa = "G_NORMAL_MAP";

	var $geoXml=0;

/**
* apiKey
* Google API Key
*/
    var $apiKey = "";

/**
* showControl
* True/False whether to show map controls or not
*/
    var $showControl = true;

/**
* showType
* True/False whether to show map type controls or not
*/
    var $showType = true;
/**
* controlType
* string: can be 'small' or 'large'
* display's either the large or the small controls on the map, small by default
*/
    var $controlType = 'small';

/**
* zoomLevel
* int: 0-17
* set's the initial zoom level of the map
*/
    var $zoomLevel = 10;

/**
* dir_phoogle
* string
* sets the directory where we can find the mapa.js file
*/
    var $dir_phoogle='';

/**
* gera_xml
* array
* é a url do script que gera o XML com os pontos a serem plotados
*/
    var $gera_xml = array('gera_xml_agrorede.php');

/**
* meteJS
* string
* custom javascript
*/
    var $meteJS = '';

    var $txt='';

    var $posicaoControle='';

/*** cluster
* boolean
* se é true, eu uso a classe markerclusterer
*/
    var $cluster = true;


function phoogleMap () {
	global $dir;
	$this->dir_phoogle = $dir['base_URL'].'mapas';
}

/**
* @function     centerMap
* @description  center's Google Map on a specific point
*               (thus eliminating the need for two different show methods from version 1.0)
*/

function centerMap($lat,$long,$tipo=""){
	switch ($tipo) {
		case "mapa":
			$tipo='G_NORMAL_MAP';
		break;
		case "satelite":
			$tipo='G_SATELLITE_MAP';
		break;
		case "hibrido":
			$tipo='G_HYBRID_MAP';
		break;
		case "terreno":
			$tipo='G_PHYSICAL_MAP';
		break;
		default:
			$tipo=$this->tipo_mapa;
		break;
	}
    $this->centerMap = "map.setCenter(new GLatLng(".$lat.",".$long."), ".$this->zoomLevel.", ".$tipo.");".chr(10);
}
/**
* @function     meteJS
* @returns      nothing
* @description  Adds new javascript customized
*
*/
	function meteJS($txt){
		$this->meteJS.=$txt;
	}
/**
* @function     printGoogleJS
* @returns      nothing
* @description  Adds the necessary Javascript for the Google Map to function
*               should be called in between the html <head></head> tags
*/
	function printGoogleJS(){?>
		<script src="http://maps.google.com/maps?file=api&v=2&hl=pt-br&key=<?php echo $this->apiKey; ?>" type="text/javascript"></script>
		<script src="<?=$this->dir_phoogle?>/icones.js.php" type="text/javascript"></script>
		<script src="<?=$this->dir_phoogle?>/mapa.js" type="text/javascript"></script>
		<?php if ($this->cluster) {?>
		<!--<script src="http://gmaps-utility-library.googlecode.com/svn/trunk/markerclusterer/1.0/src/markerclusterer_packed.js" type="text/javascript"></script>-->
    <script src="https://cdn.rawgit.com/googlemaps/js-marker-clusterer/gh-pages/src/markerclusterer.js" type="text/javascript"></script>
		<?php}?>
	<?php}
/**
* @function     showMap
* @description  Displays the Google Map on the page
*/
	function showMap(){
		// Preparo os textos que devem aparecer:
		// Definição da array de categorias (se houver)
		if ($this->categorias) {
			foreach ($this->categorias as $cat=>$catnome)
				$cattxt.='new Array("'.$cat.'","'.$catnome.'"), ';
			$cattxt = 'var categorias=new Array('.substr($cattxt,0,strlen($tmpcattxt)-2).');';
		}
		// Adição de tipos de mapa (se houver)
		if ($this->addMapType)
   			foreach ($this->addMapType as $add)
				$addmaptypetxt.="map.addMapType(".$add.");".chr(10);
		// Criação do array de urls de dados com o dados_get:
		if (!is_array($this->gera_xml)) {
			$urltxt="'".stripslashes($this->gera_xml->url)."'";
			$dadosgettxt="'".stripslashes($this->gera_xml->dados_get)."'";
		} else {
			foreach ($this->gera_xml as $g) {
				$urltxt.="'".stripslashes($g->url)."', ";
				$dadosgettxt.="'".stripslashes($g->dados_get)."', ";
			}
			$urltxt=substr($urltxt,0,-2);
			$dadosgettxt=substr($dadosgettxt,0,-2);
		}
		if ($this->txt)
			$linguatxt="var txt=eval('('+'".json_encode($this->txt)."'+')');";
		if ($this->cluster)
			$clustertxt= 'markerCluster=new MarkerClusterer(map, [], {gridSize: 35, maxZoom: 8});';

	?>
		<div id="map" style="width:<?=$this->mapWidth?>px; height:<?=$this->mapHeight?>px">
		</div>
		<script type="text/javascript">
		//<![CDATA[
		var cabecalho_sidebar="<?=$this->cabecalho_sidebar?>";
		var sidebarId = '<?=$this->sidebar?>';
		var urls = Array(<?=$urltxt?>);
		<?=$cattxt?>
		<?=$linguatxt?>

		function carrega() {
	   		if (GBrowserIsCompatible()) {
   				map = new GMap2(document.getElementById("map"));
   				<?=$clustertxt?>
   				<?php if ($this->geoXml) {?>
   				geoXml = new GGeoXml('<?=$this->geoXml?>');
   				<?php}?>

				map.enableContinuousZoom();
     			<?php
				if ($this->showControl){
					$txtmp = ($this->controlType == 'small')
						? "Small"
						: "Large";
					echo ($this->posicaoControle)
						? "map.addControl(new G".$txtmp."MapControl(), new GControlPosition(".$this->posicaoControle."));".chr(10)
						: "map.addControl(new G".$txtmp."MapControl());".chr(10);
				} ?>
	   	  		<?=$this->centerMap?>
	   	 		<?=$addmaptypetxt?>
				<?=$showtypetxt?>
				map.addControl(new GScaleControl());
				<?php if ($this->showType) { ?>
					// === Set up the map types exactly like Google Maps ===
					var hierarchy = new GHierarchicalMapTypeControl();
					hierarchy.addRelationship(G_SATELLITE_MAP, G_HYBRID_MAP, null, true);
					map.addControl(hierarchy);
				<?php }
				   if ($this->geoXml) { ?>
				     map.addOverlay(geoXml);
				<?php }
				?>
				pega_dados(Array(<?=$dadosgettxt?>));
				<?=$this->meteJS?>
			} // fim do if browser compatible';
		} // fim da funcao carrega();
		window.onload=carrega;
		window.onunload = function(){ GUnload() };
		//]]>
		</script>
		<?php
	} // fim da função ShowMap();
}//End Of Class
    ?>
