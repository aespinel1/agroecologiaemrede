<?php
include("conexao.inc.php");
$hasNewComment = false;
if ($comment) {
  $sql = "INSERT INTO comentarios
         (txt_nome, id_objeto,    ind_tipo,    e_mail,       txt_comentario, dt_comentario, ind_situacao, dt_situacao )
         VALUES ('$commentName',  '$experiencia', 'E', '$commentEmail', '$comment',  '$current_date',  'N', '$current_date')";
   $query = $db->query($sql);
   if (!$query) {
       die($db->error);
   }
   $hasNewComment = true;
}
$meta = pega_metadados();
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$meta->title?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script>
  function enviaEmail(url) {
  	var email = prompt("Digite um endereço de e-mail:","");
  	var nome = prompt("Digite o nome do destinatário:","");
  	var remet = prompt("Digite o seu nome:","");
  	if (email != null && email != "") {
  		url = url + '&email=' + email + '&nome=' + nome + '&remet=' + remet;
  		window.location.href=url;
  	}
  }
  <?php if ($hasNewComment) { ?>
    window.onload = function(){
      $("#commentsChat").get(0).scrollIntoView({ behavior: 'smooth' });
    };
  <?php } ?>
</script>
<?php
include("menu_geral.php");

// Aqui eu pego as imagens da experiência
$sql = "SELECT * FROM frm_exp_base_comum WHERE ex_id=$experiencia";
$query = $db->query($sql);
$row = $query->fetch_object();
$arqs=array();
if ($row->ex_anexos) {
  $anexos = explode('|',$row->ex_anexos);
  if ($anexos) {
  	foreach ($anexos as $arq) {
  		if (file_exists($dir['upload'].$arq) && is_array(getimagesize($dir['upload'].$arq))) {
  			$arqs[]=$arq;
  		}
  	}
  }
}
$sql = "SELECT e.*, f.dt_atualizacao as frm_dt_atualizacao, f.ex_cidade_ref FROM experiencia e, frm_exp_base_comum f WHERE e.ex_id=$experiencia AND f.ex_id=$experiencia";
$query = $db->query($sql);
$row = $query->fetch_object();

$AG = organiza_geos($row->ex_cidade_ref);

	$sql = "SELECT * FROM experiencia_autor,usuario WHERE exa_id_usuario=us_login AND exa_id_experiencia=$experiencia";
	$queryAutor = $db->query($sql);
	$numAutor = $queryAutor->num_rows;


	$sql = "SELECT * FROM experiencia_relator,usuario WHERE exr_id_usuario=us_login AND exr_id_experiencia=$experiencia";
	$queryRelator = $db->query($sql);
	$numRelator = $queryRelator->num_rows;

	$sql = "SELECT * FROM experiencia_autor,frm_instituicao WHERE exa_id_inst=in_id AND exa_id_experiencia=$experiencia";
	$queryInstAutor = $db->query($sql);
	$numInstAutor = $queryInstAutor->num_rows;


	$sql = "SELECT * FROM experiencia_relator,frm_instituicao WHERE exr_id_inst=in_id AND exr_id_experiencia=$experiencia";
	$queryInstRelator = $db->query($sql);
	$numInstRelator = $queryInstRelator->num_rows;

  $sql = "SELECT * FROM experiencia_arquivo WHERE ea_id_experiencia=$experiencia";
  $queryAnexos = $db->query($sql);
  $numAnexos = $queryAnexos->num_rows;
  $anexos = array();
  while ($rowAnexo = $queryAnexos->fetch_object()) {
    if (file_exists($dir['upload'].$rowAnexo->ea_arquivo)) {
      $anexos[] = $rowAnexo;
    }
  }

  $sql = "SELECT * FROM rel_area_experiencia,area_tematica WHERE rae_id_area=at_id AND rae_id_experiencia=$experiencia";
	$queryATs = $db->query($sql);
	$numATs = $queryATs->num_rows;

  /*$sql = "SELECT * FROM rel_geo_experiencia,area_geografica WHERE rge_id_geo=ag_id AND rge_id_experiencia=$experiencia";
	$queryAGs = $db->query($sql);
	$numAGs = $queryAGs->num_rows;*/

  $sql = "select a.*, dt_comentario from comentarios a where ind_tipo='E' AND ID_OBJETO = '$experiencia'  order by dt_comentario ASC";
  $queryComentarios = $db->query($sql);
  $numComentarios = $queryComentarios->num_rows;

  $ano_publicacao = ($row->ano_publicacao && $row->ano_publicacao!='0000')
   ? $row->ano_publicacao
   : substr($row->dt_criacao,0,4);
?>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="alert alert-success">
        <h1><i class="aericon-experiencias"></i> <?=$row->ex_descricao?></h1>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 col-md-6 col-lg-8">
      <div class="alert alert-light">
        <?php if (count($arqs)>0) { ?>
          <div class="row mb-3">
            <div id="carousel" class="carousel slide col-md-12" data-ride="false">
              <div class="carousel-inner">
                <?php foreach ($arqs as $i=>$img) { ?>
                  <div class="carousel-item<?=($i==0) ? ' active' : ''?>">
                    <a class="popImage" id="image<?=$i?>">
                    <img class="d-block w-100" src="<?=getResizedImgURL($img,1024,800)?>" alt="foto <?=($i+1)?>">
                    </a>
                  </div>
                <?php } ?>
              </a>
              </div>
              <?php if (count($arqs)>1) { ?>
                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Anterior</span>
                </a>
                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Próxima</span>
                </a>
              <?php } ?>
            </div>
          </div>
        <?php } ?>
        <div class="row">
          <p class="col-md-12"><?=$row->ex_resumo?></p>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-md-6 col-lg-4">
      <div class="row mb-4">
        <div class="col-md-12">
          <?php if ($isAdmin) { ?>
            <a title="Editar" class="btn btn-warning mr-2" href="<?=$dir['base_URL']?>mapeo/index.php?f=formulario_geral&nomefrm=<?=$row->tipo?>&id=<?=$experiencia?>"><i class="oi oi-pencil"></i></a>
          <?php } ?>
          <a title="Deixe seu comentário!" class="btn btn-primary mr-2" href="#" onClick="$('#commentsChat').get(0).scrollIntoView({ behavior: 'smooth' });"><i class="oi oi-chat"></i></a>
          <a title="Imprimir" class="btn btn-primary mr-2" href="relatorio_experiencia.php?tipo=EX1&acao=IM&experiencia=<?php echo $experiencia; ?>"><i class="oi oi-print"></i></a>
          <a title="Enviar por e-mail" class="btn btn-primary mr-2" href="#" onClick="enviaEmail('relatorio_experiencia.php?tipo=EX1&acao=EM&experiencia=<?php echo $experiencia; ?>');"><i class="oi oi-envelope-closed"></i></a>
          <!--<a title="Salvar" class="btn btn-primary" href="#" onClick="javascript: document.execCommand('SaveAs','1','');"><i class="oi oi-data-transfer-download"></i></a>-->
        </div>
      </div>
      <div class="row">
        <dl class="col-md-12">
          <dt>Experiência</dt>
          <dd><?=$row->ex_descricao?></dd>
          <?php if (isset($row->ex_chamada) && $row->ex_chamada) { ?>
            <dt>Chamada</dt>
            <dd><?=$row->ex_chamada?></dd>
          <?php } ?>
          <dt>Ano de publicação</dt>
          <dd><?=$ano_publicacao?></dd>
          <dt>Última atualização</dt>
          <dd><?=date("d/m/Y", strtotime($row->frm_dt_atualizacao))?></dd>
          <dt><i class="oi oi-wrench"></i> <?=($numAutor+$numInstAutor==1) ? "Autor/a" : "Autoras/es"?></dt>
          <dd>
            <ul class="list-group">
              <?php if ($numAutor>0) { ?>
                  <?php while ($rowAutor = $queryAutor->fetch_object()) { ?>
                   <a class="list-group-item list-group-item-active" href="usuarios.php?usu=<?= $rowAutor->us_login ?>"><?= $rowAutor->us_nome ?></a>
                  <?php } ?>
              <?php } ?>
              <?php if ($numInstAutor>0) { ?>
                <?php while ($rowInstAutor = $queryInstAutor->fetch_object()) { ?>
                 <a class="list-group-item list-group-item-active" href="instituicoes.php?instDesc=<?= $rowInstAutor->in_nome ?>&inst=<?= $rowInstAutor->in_id ?>"><?= $rowInstAutor->in_nome ?></a>
                <?php } ?>
              <?php } ?>
            </ul>
          </dd>
          <?php if ($numRelator > 0 or $numInstRelator > 0) { ?>
            <dt><i class="oi oi-pencil"></i> <?=($numRelator+$numInstRelator==1) ? "Relator/a" : "Relatoras/es"?></dt>
            <dd>
              <ul class="list-group">
                <?php if ($numRelator>0) { ?>
                    <?php while ($rowRelator = $queryRelator->fetch_object()) { ?>
                     <a class="list-group-item list-group-item-active" href="usuarios.php?usu=<?= $rowRelator->us_login ?>"><?= $rowRelator->us_nome ?></a>
                    <?php } ?>
                <?php } ?>
                <?php if ($numInstRelator>0) { ?>
                  <?php while ($rowInstRelator = $queryInstRelator->fetch_object()) { ?>
                   <a class="list-group-item list-group-item-active" href="instituicoes.php?instDesc=<?= $rowInstRelator->in_nome ?>&inst=<?= $rowInstRelator->in_id ?>"><?= $rowInstRelator->in_nome ?></a>
                  <?php } ?>
                <?php } ?>
              </ul>
            </dd>
          <?php } ?>
          <?php if (count($anexos) > 0) { ?>
            <dt><i class="oi oi-paperclip"></i> <?=(count($anexos)==1) ? "Anexo" : "Anexos"?></dt>
            <dd>
              <ul class="list-group">
                <?php foreach($anexos as $rowAnexo) { ?>
                  <?php $size = " (".my_filesize($dir['upload'].$rowAnexo->ea_arquivo).")"; ?>
                  <a class="list-group-item list-group-item-active text-truncate" href="<?=$dir['upload_URL'].$rowAnexo->ea_arquivo?>" title="<?= $rowAnexo->ea_arquivo.$size ?>" data-toggle="tooltip"><?= $rowAnexo->ea_arquivo ?> <small><?=$size?></small></a>
                <?php } ?>
              </ul>
            </dd>
          <?php } ?>
          <?php if ($numATs > 0) { ?>
            <dt><i class="oi oi-tags"></i> <?=($numATs==1) ? "Área Temática" : "Áreas Temáticas"?></dt>
            <dd>
              <ul class="list-group">
                <?php while ($rowAT = $queryATs->fetch_object()) { ?>
                  <a class="list-group-item list-group-item-active" href="<?=$dir['base_URL']?>mapeo/?f=consultar&c=1&tipofrm=experiencia&ex_areas_tematicas[]=<?=$rowAT->at_id?>"><?= traduz($rowAT->at_descricao) ?></a>
                <?php } ?>
              </ul>
            </dd>
          <?php } ?>
          <?php if ($AG->cidade) { ?>
            <dt><i class="oi oi-globe"></i> <?=($numAGs==1) ? "Área Geográfica" : "Áreas Geográficas"?></dt>
            <dd>
              <ul class="list-group">
                  <li class="list-group-item"><a href="<?=$dir['base_URL']?>mapeo?c=1&f=consultar&tipofrm=experiencia&ex_cidade_ref=<?=$AG->pais->id?>|<?=$AG->estado->id?>|<?=$AG->cidade->id?>"><?=$AG->cidade->nome?></a>, <a href="<?=$dir['base_URL']?>mapeo?c=1&f=consultar&tipofrm=experiencia&ex_cidade_ref=<?=$AG->pais->id?>|<?=$AG->estado->id?>"><?=$AG->estado->nome?></a></li>
              </ul>
            </dd>
          <?php } ?>
        </dl>
      </div>

      <div class="card">
        <div class="card-header">
          <i class="oi oi-chat"></i> Comentários
        </div>
        <div class="card-body">
          <?php if(!$numComentarios) { ?>
            Ainda não há comentários sobre esta experiência.
          <?php } else { ?>
            <ul class="chat">
              <?php $i = 0; ?>
              <?php while ($comentario = $queryComentarios->fetch_object()) { ?>
                <?php $i++; ?>
                <?php /*$float = ($i/2==round(1/2)) ? "right" : "left";*/ ?>
                <li class="<?=/*$float*/''?> clearfix<?= ($i==$numComentarios && $hasNewComment) ? ' alert alert-success' : '' ?>"><!--<span class="chat-img float-<?=$float?>">
                  <img src="http://placehold.it/50/55C1E7/fff&amp;text=U" alt="User Avatar" class="rounded-circle">
                </span>-->
                  <div class="chat-body">
                    <div class="header">
                      <strong class="primary-font"><?=$comentario->txt_nome?></strong> <small class="float-right text-muted">
                        <span class="oi oi-clock"></span> <?=date("d/m/Y à\s H\hi", strtotime($comentario->dt_comentario))?></small>
                    </div>
                    <p><?=$comentario->txt_comentario?></p>
                  </div>
                </li>
              <?php } ?>
            </ul>
          <?php } ?>
        </div>
        <div id="commentsChat" class="card-footer">
	<fieldset disabled>
          <form class="needs-validation" action="experiencias.php?experiencia=<?=$experiencia?>" method="POST">
            <div class="mb-1">
              <input name="commentName" class="form-control input-sm" placeholder="Seu nome" type="text" required>
            </div>
            <div class="form-group mb-1">
              <input name="commentMail" class="form-control input-sm" placeholder="Seu email" type="email" required>
            </div>
            <div class="form-group mb-1">
              <textarea name="comment" class="form-control input-sm" placeholder="Faça seu comentário..." type="text" required></textarea>
            </div>
            <div>
            <button type="submit" class="btn btn-primary">Comentar</button>
	</fieldset>
          </form>
        </div>
      </div>

    </div>
  </div>
</div>

<!-- Full Image Modal -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-full">
    <div class="modal-content">
      <div class="mr-5 mt-3" style="position:absolute;top:0;right:0;z-index:10">
        <button type="button" class="close bg-light" data-dismiss="modal"><span aria-hidden="true">&times;</span> <span>Fechar</span></button>
      </div>
      <div class="modal-body mx-auto mt-0 mb-0 pt-0 pb-0">
        <img src="" id="imagepreview" style="max-width:100%;max-height:100vh" >
      </div>
    </div>
  </div>
</div>

</body>
</html>
<?php
$db->close();
?>
