<?php
//coding:utf-8
//por daniel tygel (dtygel em fbes pt org pt br) em 2009

require_once "mapeo/config.php";
$icone_cor_padrao='Verde';
$icone_tipo_padrao='Agrorede';
mb_internal_encoding("UTF-8");
require_once $dir['apoio']."funcoes_comuns.php";
//Faço a conexão ao banco de dados:
$db = conecta($bd);


$tipofrm='experiencia';
/*if ($nomefrm = $_REQUEST['nomeFrm']) {
	$sql = "SELECT * FROM mapeo_formularios WHERE nomefrm = '".$nomefrm."'";
	$res = faz_query($sql,'','object');
	$tipofrm = $res[0]->tipo;
} else
	$tipofrm = $_REQUEST['tipofrm'];
*/
$idpt = $_REQUEST['idPt'];
$mostratabela=$_REQUEST['mostratabela'];
$conta=$_REQUEST['conta'];
$lingua = ($_REQUEST['lingua'])
	? $_REQUEST['lingua']
	: $_COOKIE['agrorede_lingua'];
$filtro = resgata_filtros($tipofrm);
$geos = explode('|',$_REQUEST[$campo[$filtro->tab]->localizacao]);
if ($geos = $filtro->sels[$campo[$filtro->tab]->localizacao]) {
	rsort($geos);
	$geo = organiza_geos($geos[0],$_REQUEST['distmax'],$lingua);
}
$f=$_REQUEST['f'];

$sql = "SELECT * FROM mapeo_formularios WHERE tipo='".$tipofrm."'";
if ($filtro->sels['nomefrm']) {
	$sql .= " AND nomefrm IN (";
	foreach ($filtro->sels['nomefrm'] as $o)
		$sql .= "'".$o."',";
	$sql = substr($sql,0,-1).")";
}
$frms = faz_query($sql,'','object');
unset($filtro->sels['nomefrm'],$filtro->mapa['nomefrm']);

// Aqui eu começo a preparar o XML (a raiz é o FAREJA):
$docxml = new DOMDocument("1.0");
$docxml->formatOutput=true;
$node = $docxml->CreateElement("fareja");
$raiz = $docxml->AppendChild($node);
//Aqui monto o resultado conforme o pedido realizado
switch ($f) {
	case 'infos':
		if ($idpt && $nomefrm) {
			$from = " FROM ".$nomefrm." as a";
			$where = " WHERE a.".$campo[$nomefrm]->id."='".$idpt."'";
			if ($frms[0]->tab_base_comum) {
				$from2 = ", ".$frms[0]->tab_base_comum." as b";
				$where .= " AND a.".$campo[$nomefrm]->id."=b.".$campo[$frms[0]->tab_base_comum]->id;
			}
			$sql = "SELECT ";
			foreach ($filtro->info as $c=>$tit) {
				$cmdo = $filtro->cmd[$c];
				$m = $filtro->mapa[$c];
				if (isset($cmdo->apoio) && $cmdo->apoio) {
					$tmp = " LEFT JOIN ".$cmdo->apoio." ON ".$cmdo->apoio.".".$cmdo->campo_id."=".$c;
					if ($m->frm == $nomefrm) {
						$from .= $tmp;
						$sql_compl = "a.";
					} else {
						$from2 .= $tmp;
						$sql_compl = "b.";
					}
					$sql_compl .= $c." AS 'id_".$c."', ";
					$sql .= $cmdo->apoio.".".$cmdo->campo_nome." AS '".$c."', ".$sql_compl;
				} else {
					$tmp = ($m->frm == $nomefrm)
						? "a.".$c
						: "b.".$c;
					$sql .= ($m->tipo_mysql=='date')
						? "UNIX_TIMESTAMP(".$tmp.") AS '".$c."', "
						: $tmp.", ";
				}
			}
			$sql = substr($sql,0,-2);
			$sql.=$from.$from2.$where;

			$dados = faz_query($sql,'','object');
			$dado=$dados[0];
			$res=array();
			$tab=array();
			$hi = '<a href="?c=1&f=consultar&tipofrm='.$tipofrm;
			$hf = '</a>';
			foreach ($filtro->info as $c=>$tit) {
				$m = $filtro->mapa[$c];
				$cmdo_mapa = $filtro->cmd_mapa[$c];
				$campo_id='id_'.$c;
				$titulo_campo = $tit;
				if ($dado->$c) {
					$n=count($res);
					$res[$n]->campo=$c;
					switch ($m->tipo_form) {
						case 'georref':
							$geotmp = organiza_geos($dado->$c);
							$dado->$campo_id = $dado->$c;
							$c0='&'.$c.'_0='.$geotmp->pais->id;
							$c1='&'.$c.'_1='.$geotmp->estado->id;
							$c2='&'.$c.'='.$geotmp->cidade->id;
							$dado->$c =
								$hi.$c0.$c1.$c2.'">'.$geotmp->cidade->nome.$hf
								. ' / '
								. $hi.$c0.$c1.'">'.$geotmp->estado->nome.$hf
								. ' / '
								. $hi.$c0.'">'.$geotmp->pais->nome.$hf;
						break;
						case 'file':
							$tmp = explode('|',$dado->$c);
							$dado->$c = '<ul>';
							foreach ($tmp as $f) {
								$ftxt = (strlen($f)>30)
									? substr($f,0,30).'...'
									: $f;
								$dado->$c .= '<li><a target="_BLANK" href="'.$dir['upload_URL'].$f.'" title="'.$f.'">'.$ftxt.'</a></li>';
							}
							$dado->$c .= '</ul>';
						break;
						default:
							if ($m->tipo_mysql=='date')
								$dado->$c = date('d/m/Y',$dado->$c);
							if ($campo[$filtro->tab]->nome==$c) {
								$titulo_campo='';
								$res[$n]->css='_titulo';
								$dado->$c =
									'<a href="'.str_replace('{id}',$idpt,$dir['mostra_'.$tipofrm.'_URL']).'">'
									. $dado->$c . '</a>';
							}
						break;
					}
					if ($titulo_campo)
						$res[$n]->tit=$titulo_campo;
					$res[$n]->txt=traduz($dado->$c,$lingua);
					if ($dado->$campo_id)
						$res[$n]->idref=$dado->$campo_id;
				}
			}
			$html='';
			$pre='<p class="mensagem_balao';
			$pos='</p>'.chr(10);
			foreach ($res as $r) {
				$html .= $pre;
				$html .= ($r->css) ? $r->css.'">' : '_corpo">';
				if ($r->tit)
					$html .= '<b>'.$r->tit.'</b>: <br />';
				$html .= $r->txt.$pos;
			}
			$node = $docxml->CreateElement("markers");
			$nivel1 = $raiz->AppendChild($node);
			$node = $docxml->createElement("info");
			$newnode = $nivel1->appendChild($node);
			//$newnode->setAttribute("msg", json_encode($res));
			$newnode->setAttribute("msg", $html);
		}
	break;
	case 'pts':
	default:
		// Dados iniciais de configuração
		$node = $docxml->CreateElement("config");
		$nivel1 = $raiz->AppendChild($node);
		$nivel1->SetAttribute("tipo", $icone_tipo_padrao);
		$nivel1->SetAttribute("cor", $icone_cor_padrao);
		$nivel1->SetAttribute("cat", 'agrorede');
		// Agora os pontos:
		$node = $docxml->CreateElement("markers");
		$nivel1 = $raiz->AppendChild($node);

		// Loop nos formulários de tipo $tipofrm, que por enquanto podem ser experiências ou instituições:
		$dados = ($conta)
			? 0
			: array();
		foreach ($frms as $frm) {
			unset($leftjoin);
			$cmpo = $campo[$frm->nomefrm];
			$sql = "SELECT '".$frm->nomefrm."' as 'nomefrm'";
			if ($cmpo->descricao && $mostratabela && !$conta)
				$sql .= ", ".$cmpo->descricao." AS 'descricao'";
			foreach ($filtro->geraXML as $gcampo=>$gnome)
				if ($gnome == 'abrangencia_latlng')
					$sql .= ", REPLACE(a.".$gcampo.",'0,00/0,00;0,00/0,00','') as 'abrangencia_latlng'";
					else $sql .= ($gnome)
						? ", a.".$gcampo." AS '".$gnome."'"
						: ", a.".$gcampo;

			if ($geo->distmax)
				$sql.= ", ".sql_distancia_geo($geo->cidade,$cmpo->localizacao_lat,$cmpo->localizacao_lng)." as distancia";

			// FROM:
			$from = ' FROM ';
			$from .= ($frm->tab_base_comum)
				? $frm->nomefrm.' as b, '.$frm->tab_base_comum.' as a'
				: $frm->nomefrm.' as a';

			// WHERE:
			$where = " WHERE 1";
			if ($frm->tab_base_comum)
				$where .= " AND b.".$cmpo->id."=a.".$campo[$frm->tab_base_comum]->id;
			if ($geo->distmax)
				$where.=" AND ".sql_distancia_geo($geo->cidade,$cmpo->localizacao_lat,$cmpo->localizacao_lng)." < ".$geo->distmax;
			// Este é para prevenir do campo de localização não seguir a regra de ter 11 dígitos.
			if ($gcampo == $cmpo->localizacao)
				$where.=" AND LENGTH(".$cmpo->localizacao.")=11 AND LOCATE(' ',".$cmpo->localizacao.")=0";
			// WHEREs dos filtros (se houver!):
			if (isset($filtro->sels) && $filtro->sels) {
				foreach ($filtro->sels as $sel_campo=>$sel_valor) {
					$cmdo = $filtro->cmd[$sel_campo];
					// Antes de mais nada eu percorro a árvore se o campo é de árvore, para que a escolha do usuário implique também na escolha dos filhos do item escolhido!
					$sub_sel_valor = array();
					if ($filtro->mapa[$sel_campo]->tipo_form == 'arvore') {
						foreach ($sel_valor as $v) {
							$campo_id = $cmdo->campo_id;
							$sub_sel_valor[$v][] = $v;
							if ($arvore = resgata_arvore($cmdo->apoio,$campo_id,$cmdo->campo_nome,$cmdo->campo_mae,$v))
								foreach ($arvore as $a) {
									//if (!in_array($a->$campo_id,$sel_valor))
										//$sel_valor[] = $a->$campo_id;
									$sub_sel_valor[$v][] = $a->$campo_id;
								}
						}
					}
					// Pronto, agora eu gero o $where, dependendo se é o campo de localização ou se é dos demais. Não gero $where se o distmax estava definido e é de localização, pois já foi feito o where lá em cima.
					if ($sel_campo == $cmpo->localizacao && !$geo->distmax) {
						if (is_array($sel_valor)) {
							rsort($sel_valor);
							$sel_valor=$sel_valor[0];
						}
						$where .= " AND SUBSTRING(".$cmpo->localizacao.",1,".strlen($sel_valor).")='".$sel_valor."'";
					} elseif (!$geo->distmax) {
						$where .= " AND (";
						$tipo_juncao = " AND ";
						$tipo_juncao_tam = strlen($tipo_juncao);
						foreach ($sel_valor as $v) {
							if ($sub_sel_valor[$v]) {
								$where .= "(";
								foreach ($sub_sel_valor[$v] as $s)
									$where .= "LOCATE('".$s."', ".$sel_campo.")>0 OR ";
								$where = substr($where,0,-4).") ".$tipo_juncao;
							} else
								$where .= "LOCATE('".$v."', ".$sel_campo.")>0".$tipo_juncao;

						}
						$where = substr($where,0,-$tipo_juncao_tam)." )";
					}
				}
			}


			//Ainda preciso fazer o foreach de um campo de buscas dentro da variável $filtro!
			if ($filtro->textoBusca && is_array($cmpo->textoBusca)) {
				$where .= " AND (";
				foreach ($cmpo->textoBusca as $c)
					$where .= $c." LIKE '%".$filtro->textoBusca."%' OR ";
				$where = substr($where,0,-4).")";
			}

			// ORDER BY:
			$orderby = " ORDER BY ";
			if ($geo->distmax)
				$orderby.="distancia, ";
			$orderby.="nome";

			// Aqui nós adicionamos as definições de ícones de tabelas de apoio em que o $cmdo_mapa->icone está definido. Isso é algo para lembrar: sempre que eu colocar $cmdo_mapa->icone ou cor = 1, devem estar definidas as colunas icone_cor e icone_tipo na tabela de apoio!! Se estas 2 colunas não existirem, dá erro!
			foreach ($filtro->mapa as $m) {
				//if ($m>campo != 'ex_areas_tematicas') {
				$cmdo_mapa = $filtro->cmd_mapa[$m->campo];
				$cmdo = $filtro->cmd[$m->campo];
				// Um eventual where do $cmdo_mapa tem primazia sobre o where do $cmdo:
				if ($cmdo->apoio && $filtro->opcoes[$m->campo]) {
					if ($cmdo_mapa->where)
						$cmdo->where = $cmdo_mapa->where;
					// WHEREs que vêm no cmdo ou cmdo_mapa para restringir valores das tabelas de apoio:
					if ($cmdo->where) {
						$cmdo->where = str_replace('{campo_id}',$m->campo,$cmdo->where);
						$cmdo->where = str_replace('{campo_nome}',$cmdo->campo_nome,$cmdo->where);
						$where .= " AND ".$cmdo->where;
					}
					// Definições de cor e tipo de ícone vindos das tabelas de apoio:
					if ($cmdo_mapa->icone) {
						$sql .= ", ".$cmdo->apoio.".icone_cor as ".$cmdo->apoio."_icone_cor, ".$cmdo->apoio.".icone_tipo as ".$cmdo->apoio."_icone_tipo";
						$leftjoin .= " LEFT JOIN ".$cmdo->apoio." ON ".$cmdo->apoio.".".$cmdo->campo_id."=".$m->campo;
						$where .= " AND (".$cmdo->apoio.".icone_cor<>'-1' AND ".$cmdo->apoio.".icone_tipo<>'-1')";
						$tabs_definem_icones[]=$cmdo->apoio;
					}
				}
			}
			$sql.=$from.$leftjoin.$where.$orderby;
			//echo $sql.'<br><br><br>';
				if ($conta) {
					$dados += faz_query($sql,'','num');
				} else {
					$res = faz_query($sql,'','object');
					if ($res)
						$dados = array_merge($dados,$res);
				}
		} // fim do loop nos formulários do tipo $tipofrm
		//exit;

		if ($conta) {
			// Se era pra contar, basta gerar um XML pequenino com o total de pontos que aparecerão no mapa:
			$node = $docxml->createElement("pt");
			$newnode = $nivel1->appendChild($node);
			$newnode->setAttribute("num", $dados);
		} else {
			// Foi feito o select que tirou os pontos. Entretanto, é preciso agora acertar mais detalhes, depedendo se estou gerando dados para mapa ou para tabela:
			$paises = $cidades = $abrangencias = $latlngs = array();
			$n = count($dados);
			// Os resultados devem ser limitados de acordo com o parâmetro do usuário, se for lista. Se for mapa, aparece tudo!
			if ($mostratabela) {
				$pgs = ceil($n/$padrao_itens_por_pagina);
				$numnode = $docxml->createElement("num");
				$novonode = $raiz->AppendChild($numnode);
				$novonode->setAttribute("num", $n);
				$novonode->setAttribute("pgs", $pgs);
				if ($_REQUEST['pgLista']>1) {
					$min = ($_REQUEST['pgLista']-1)*$padrao_itens_por_pagina;
					$dados = array_slice($dados,$min);
				}
				$dados = array_slice($dados,0,$padrao_itens_por_pagina);
				$n = count($dados);
			}
			foreach ($dados as $i=>$d) {
				if ($t = $d->localizacao)
					if ($mostratabela) {
						// Se é o resultado para tabela, precisarei dos nomes das cidades e estados:
						if (!in_array(substr($t,0,2),$paises))
							$paises[]=substr($t,0,2);
						if (!in_array($t,$cidades))
							$cidades[]=$t;
					} else {
						// Se é para mapa, só pego as cidades e estados para o caso desta tabela não ter já alimentado automaticamente o localizacao_lat!
						if (!$campo[$filtro->tab]->localizacao_lat) {
							if (!in_array(substr($t,0,2),$paises))
								$paises[]=substr($t,0,2);
							if (!in_array($t,$cidades))
								$cidades[]=$t;
						}

					// e aqui é também só para mapa: incluir os pontos de abrangência e também definir a cor e tipo do ícone dos dados alimentados pelo mysql:
					if ($d->abrangencia) {
						$tmp = explode('|',$d->abrangencia);
						foreach ($tmp as $t) {
							if ($t) {
								if (!in_array(substr($t,0,2),$paises))
									$paises[]=substr($t,0,2);
								if (!in_array($t,$cidades))
									$cidades[]=$t;
								$abrangencias[$t][]=$d->id;
							}
						}
					}
					if ($d->abrangencia_latlng) {
						$tmp1 = explode(';',$d->abrangencia_latlng);
						foreach ($tmp1 as $t) {
							$tmp2 = explode('/',str_replace(',','.',$t));
							if ((floatval($tmp2[0]) && floatval($tmp2[1]))) {
								$tmpid=$d->id;
								$dados[$n]->id_mae=$tmpid;
								$dados[$n]->localizacao_lat=$tmp2[0];
								$dados[$n]->localizacao_lng=$tmp2[1];
								$dados[$n]->icone_tipo = 'Pontogde';
								$dados[$n]->icone_cor = 'Verde';
								// para ficar hachurado, veremos!
								if ($mostra_hachurado) {
									$dados[$i]->localizacao_lat .= ','.$dados[$n]->localizacao_lat;
									$dados[$i]->localizacao_lng .= ','.$dados[$n]->localizacao_lng;
								}
								$n++;
							}
						}
					}
					if ($tabs_definem_icones) {
						$cores = $tipos = array();
						foreach ($tabs_definem_icones as $apoio) {
							$txt_cor = $apoio.'_icone_cor';
							$txt_tipo = $apoio.'_icone_tipo';
							if ($d->$txt_cor)
								$cores[]=$d->$txt_cor;
							if ($d->$txt_tipo)
								$tipos[]=$d->$txt_tipo;
							unset($dados[$i]->$txt_cor,$dados[$i]->$txt_tipo);
						}
						if (count($cores))
							$dados[$i]->icone_cor = $cores[0];
						if (count($tipos))
							$dados[$i]->icone_tipo = $tipos[0];
					}
				} // fim do else do if $mostratabela
			} // fim do loop nos dados
			// Agora resgato os dados de latitude e longitude das cidades que são abrangência e/ou das cidades de tabelas que não tenham lat/lng definidas e apenas cidade:
			if (count($paises)) {
				foreach ($paises as $pais) {
					$sql = "
						SELECT a.id, a.nome as cidade, b.nome as estado, c.nome as pais, a.lat, a.lng
						FROM _".$pais." as a
							LEFT JOIN _".$pais."_maes as b ON a.id_mae=b.id
							LEFT JOIN __paises as c ON c.id='".$pais."'
						WHERE a.id IN (";
					foreach ($cidades as $c) {
						if (substr($c,0,2)==$pais)
							$sql.="'".$c."', ";
					}
					$sql = substr($sql,0,-2).")";
					$res = faz_query($sql,'','object');
					if ($res)
						$latlngs = array_merge($latlngs,$res);
				}
				// Definição das lat/lng se não estão na tabela:
				if (!$campo[$filtro->tab]->localizacao_lat || $mostratabela)
					foreach ($dados as $n=>$dado)
						foreach ($latlngs as $latlng)
							if ($latlng->id == $dado->localizacao) {
								if (!$mostratabela) {
									$dados[$n]->localizacao_lat = $latlng->lat;
									$dados[$n]->localizacao_lng = $latlng->lng;
								} else {
									// Se vai mostrar a tabela, devo colocar a cidade/estado/pais:
									$dados[$n]->cidadeNome = $latlng->cidade;
									$dados[$n]->estadoNome = $latlng->estado;
									$dados[$n]->paisNome = traduz($latlng->pais,$lingua);
								}
							}
				// Definição das abrangências:
				foreach ($latlngs as $latlng)
					if ($abrangencias[$latlng->id])
						foreach ($abrangencias[$latlng->id] as $a) {
							$n++;
							$dados[$n]->id_mae=$a;
							//$dados[$n]->cidade=$latlng->id;
							$dados[$n]->localizacao_lat=$latlng->lat;
							$dados[$n]->localizacao_lng=$latlng->lng;
							$dados[$n]->icone_tipo = 'Pontogde';
							$dados[$n]->icone_cor = 'Verde';

							// para ficar hachurado, veremos!
							if ($mostra_hachurado)
								foreach ($dados as $di=>$da)
									if ($da->id == $a) {
										$dados[$di]->localizacao_lat .= ','.$latlng->lat;
										$dados[$di]->localizacao_lng .= ','.$latlng->lng;
									}
						}
			}
			// Pronto, criei a tabela de dados final. O lance agora é tranformá-la no XML para envio (já fazendo os desvios aleatórios das latitudes e longitudes que batem no mesmo ponto (monitorados por $lats e $lngs):
			$lats = $lngs = array();
			foreach ($dados as $d) {
				if (in_array($d->localizacao_lng,$lngs) || in_array($d->localizacao_lat,$lats)) {
					$d->localizacao_lng+=rand(-500,0)*(0.03/500);
					$d->localizacao_lat+=rand(-500,0)*(0.03/500);
				} else {
					$lngs[]=$d->localizacao_lng;
					$lats[]=$d->localizacao_lat;
				}
				$node = $docxml->createElement("pt");
				$newnode = $nivel1->appendChild($node);
				if ($d->id) {
					$newnode->setAttribute("id", $d->id);
					//$newnode->setAttribute("nomefrm", $d->nomefrm);
					$newnode->setAttribute("nome", $d->nome);
					// Se é lista, mostra-se, além do nome, também a cidade, estado, país e o resumo pelo overlib
					if ($mostratabela) {
						$newnode->setAttribute("cidade", $d->cidadeNome);
						$newnode->setAttribute("estado", $d->estadoNome);
						$newnode->setAttribute("pais", $d->paisNome);
						$newnode->setAttribute("descricao", $d->descricao);
					}
				} elseif ($d->id_mae)
					$newnode->setAttribute("id_mae", $d->id_mae);
				$newnode->setAttribute("lat", $d->localizacao_lat);
				$newnode->setAttribute("lng", $d->localizacao_lng);
				if ($d->distancia)
					$newnode->setAttribute("distancia", $d->distancia);
				if ($d->icone_cor)
					$newnode->setAttribute("cor", $d->icone_cor);
				if ($d->icone_tipo)
					$newnode->setAttribute("tipo", $d->icone_tipo);
			}
		} // fim do if $conta
	break;
}

header('Content-Type: text/xml; charset=utf-8');
echo $docxml->saveXML();

function mensagem_html($dados) {
global $cidade_ref, $id_cidade_ref, $filtro;
	$dados->ex_descricao=wordwrap($dados->ex_descricao,30,"<br />");
	$dados->ex_chamada=wordwrap($dados->ex_chamada,30,"<br />");
	$dados->ex_resumo=wordwrap($dados->ex_resumo,30,"<br />");
	$dados->txt_comentario=wordwrap($dados->txt_comentario,30,"<br />");
	$dados->tema=wordwrap(str_replace('|',', ',$dados->tema),30,"<br />");
	$dados->local=wordwrap(str_replace('|',', ',$dados->local),30,"<br />");
	if ($filtro->busca) {
		$dados->ex_descricao=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_descricao);
		$dados->ex_chamada=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_chamada);
		$dados->ex_resumo=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_resumo);
		$dados->ex_tema=str_ireplace($filtro->busca,'<span class="busca">'.$filtro->busca.'</span>',$dados->ex_tema);
	}
	$texto.=$dados->ex_descricao.'|'
		. $dados->ex_chamada.'|'
//		. $dados->ex_resumo.'|'
//		. $dados->txt_comentario.'|'
		. urlencode($dados->ea_arquivo).'|'
		. $dados->tema.'|'
		. $dados->local.'|'
		. $dados->ex_id;
	return $texto;
}
?>
