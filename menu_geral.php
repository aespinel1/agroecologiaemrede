<?php
if (!defined('MAPA')) {
	define(MAPA, ((isset($_REQUEST['f']) && $_REQUEST['f']=='consultar') && (isset($_REQUEST['visao']) && $_REQUEST['visao']=='mapa')));
}
// If this menu is called from scripts outside mapeo directory:
if (!isset($loggedIn)) {
	if (isset($_COOKIE['cookieUsuario']) && $_COOKIE['cookieUsuario']) {
		if (!isset($s) || !$s) {
			$s = new stdClass();
			$s->u = resgata_dados_usuario();
		}
		if ((!isset($_COOKIE['cookieAdmin']) || !$_COOKIE['cookieAdmin']) && (isset($s) && isset($s->u) && isset($s->u->us_admin))) {
			unset($s->u->us_admin);
		}
	}

	$loggedIn = isset($s) && isset($s->u) && $s->u;
	$isAdmin = $loggedIn && isset($s->u->us_admin) && $s->u->us_admin;
}
if (!isset($lingua) || !$lingua) {
	$lingua = 'pt-br';
}
if (!isset($meta)) {
	$meta = pega_metadados();
}
?>

<link rel="apple-touch-icon" sizes="57x57" href="<?=$dir['base_URL']?>imagens/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?=$dir['base_URL']?>imagens/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?=$dir['base_URL']?>imagens/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?=$dir['base_URL']?>imagens/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?=$dir['base_URL']?>imagens/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?=$dir['base_URL']?>imagens/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?=$dir['base_URL']?>imagens/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?=$dir['base_URL']?>imagens/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?=$dir['base_URL']?>imagens/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?=$dir['base_URL']?>imagens/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?=$dir['base_URL']?>imagens/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?=$dir['base_URL']?>imagens/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?=$dir['base_URL']?>imagens/favicon/favicon-16x16.png">
<link rel="manifest" href="<?=$dir['base_URL']?>imagens/favicon/manifest.json">

<!-- METADATA -->
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?=$dir['base_URL']?>imagens/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta property="og:url" content='<?=$meta->url?>'>
<meta property="og:type" content='article'>
<meta property="og:title" content='<?=$meta->title?>'>
<meta property="og:description" content='<?=$meta->description?>'>
<meta property="og:image" content='<?=$meta->image?>'>
<meta property="article:author" content="https://www.facebook.com/articulacaonacionaldeagroecologia/">
<meta property="og:site_name" content="Agroecologia em Rede">
<!--<meta property="article:published_time" content="2014-08-12T00:01:56+00:00">-->
<meta name="twitter:card" content='<?=$meta->description?>'>
<!--<meta name="twitter:site" content='<?=$meta->url?>'>-->
<!--<meta name="twitter:creator" content="[@yourtwitter]">-->
<meta itemscope itemtype="http://schema.org/Article">
<meta itemprop="headline" content='<?=$meta->title?>'>
<meta itemprop="description" content='<?=$meta->description?>'>
<meta itemprop="image" content='<?=$meta->image?>'>

<!-- Tema Bootstrap: MINTY: -->
<link rel="stylesheet" href="<?=$dir['apoio_URL']?>bootstrap-minty/bootstrap.min.css">
<link rel="stylesheet" href="<?=$dir['apoio_URL']?>bootstrap-select/1.13.0-beta/css/bootstrap-select.min.css">
<?php if ($isAdmin) { ?>
	<link rel="stylesheet" type="text/css" href="<?=$dir['apoio_URL']?>DataTables/datatables.min.css"/>
<?php } ?>
<link rel="stylesheet" href="<?=$dir['apoio_URL']?>open-iconic/font/css/open-iconic-bootstrap.css" type="text/css">
<?php if (MAPA) { ?>
	<link rel="stylesheet" href="<?=$dir['apoio_URL']?>leaflet/leaflet.css">
	<link rel="stylesheet" href="<?=$dir['apoio_URL']?>Leaflet.markercluster-1.3.0/dist/MarkerCluster.css">
	<link rel="stylesheet" href="<?=$dir['apoio_URL']?>Leaflet.markercluster-1.3.0/dist/MarkerCluster.AeR.css">
<?php } ?>
<link rel="stylesheet" href="<?=$dir['apoio_URL']?>aer_icons/style.css" type="text/css">
<link rel="stylesheet" href="<?=$dir['base_URL']?>estilo.css" type="text/css">
<link rel="stylesheet" href="<?=$dir['apoio_URL']?>estilo.css" type="text/css">
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="<?=$dir['apoio_URL']?>bootstrap/4.1.0-dist/js/bootstrap.min.js"></script>
<script src="<?=$dir['apoio_URL']?>bootstrap-select/1.13.0-beta/js/bootstrap-select.js"></script>
<script src="<?=$dir['apoio_URL']?>bootstrap-select/1.13.0-beta/js/i18n/defaults-pt_BR.js"></script>
<?php if ($isAdmin) { ?>
	<script type="text/javascript" src="<?=$dir['apoio_URL']?>DataTables/datatables.min.js"></script>
<?php } ?>
<?php if (MAPA) { ?>
	<script type="text/javascript" src="<?=$dir['apoio_URL']?>leaflet/leaflet.js"></script>
	<script type="text/javascript" src="<?=$dir['apoio_URL']?>Leaflet.markercluster-1.3.0/dist/leaflet.markercluster.js"></script>
	<script type="text/javascript" src="<?=$dir['base_URL']?>mapas/mapa.js"></script>
<?php } ?>
<script src="<?=$dir['apoio_URL']?>funcoes_comuns.js"></script>
<!-- Estatísticas de acesso -->
<script>
  var _paq = _paq || [];
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//piwik.eita.org.br/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '4']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
</head>
<body <?=(MAPA)?'class="mapa" style="padding-top:80px;"':'style="padding-top:80px;padding-bottom:20px;"'?>>
<?php
	if ($foradoar && !$_COOKIE['cookieInterno'] && $_GET['int']!='AGROREDEsisgea') {
		include($dir['base'].'foradoar.php');
	}

	include $dir['base']."mapeo/config_menus.php";

	if ($loggedIn) {
		$imgico ='ico_logoff.gif';
		$txtmp = $txt['login_titulo_desconectar'];
  		if (!$isAdmin) {
			$opcao = "<a class='Branco' href='".$dir['base_URL']."mapeo/index.php?desconectar=1'>";
		} else {
			$opcao = "<a class='Branco' href='".$dir['base_URL']."admin/xt_logout.php'>";
		}
	} else {
      $opcao = "<a class='Branco' href='".$dir['base_URL']."login.php'>";
      $imgico='login_icon.gif';
	  $txtmp = $txt['login_titulo_conectar'];
	}
?>
<script>
	var dirBase="<?=$dir['base_URL']?>";
</script>
<nav id="menuGeralAeR" class="navbar navbar-expand-lg fixed-top navbar-light bg-light">
	<div class="container-fluid">
	  <a class="navbar-brand text-primary d-none d-md-block" href="<?=$dir['base_URL']?>"><strong><?= ($isAdmin) ? "AeR" : "Agroecologia em Rede"?></strong></a>
		<a class="navbar-brand text-primary d-block d-md-none" href="<?=$dir['base_URL']?>"><strong>AeR</strong></a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>

		<!--<form class="form-inline">
	    <div class="input-group">
	      <div class="input-group-prepend">
	        <span class="input-group-text oi oi-magnifying-glass" id="basic-addon1"></span>
	      </div>
	      <input type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
	    </div>
	  </form>-->
	  <div class="collapse navbar-collapse" id="navbarSupportedContent">
	    <ul class="navbar-nav mr-auto">
				<?php foreach ($menu as $m) { ?>
					<?php $active = ($m->active) ? ' active' : ''?>
					<?php if (isset($m->submenus)) { ?>
						<li class="nav-item dropdown">
			        <a class="nav-link text-dark<?=$active?> dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			          <?=$m->title?>
			        </a>
			        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
								<?php foreach ($m->submenus as $sub) { ?>
			          	<a class="dropdown-item" href="<?=(substr($sub->url,0,4)=='http')?$sub->url:$dir['base_URL'].$sub->url ?>"><?= $sub->title ?></a>
								<?php } ?>
			        </div>
			      </li>
					<?php } else { ?>
						<li class="nav-item<?=$active?>">
			        <a class="nav-link text-dark" href="<?=$dir['base_URL'].$m->url?>"><?=$m->title?></a>
			      </li>
					<?php } ?>
				<?php } ?>
	    </ul>
			<!--<a href="<?= $dir['base_URL'] ?><?= $loggedIn ? 'xt_logout' : 'login' ?>.php" class="my-2 my-lg-0 text-dark" title="<?= $loggedIn ? 'Sair' : 'Fazer login' ?>">
				<span class="oi <?= $loggedIn ? 'oi-account-logout' : 'oi-account-login' ?>" aria-hidden="true"></span> <?= $loggedIn ? 'sair' : 'entrar' ?>
			</a>-->
	  </div>
	</div>
</nav>
<!-- top-link-block -->
<div id="top-link-block" class="d-none">
  <a href="#top" class="btn btn-dark btn-sm"  onclick="$('html,body').animate({scrollTop:0},'slow');return false;">
    <i class="oi oi-chevron-top"></i>
  </a>
</div>
<!-- /top-link-block -->
