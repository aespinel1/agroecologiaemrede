<?php
session_start();
if (!isset($_SESSION['agrorede_lingua'])) {
	$_SESSION['agrorede_lingua']='pt-br';
}
setcookie('agrorede_lingua',$_SESSION['agrorede_lingua'],time()+60*60*24*365,'/');
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title><?=$txt['agrorede']?></title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="admin/estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
	function criticaForm() {
		login = formLogin.login.value;
		if (login.length == 0) {
			alert('<?=$txt["erro_identificacao"]?>');
			formLogin.login.focus();
			return;
		}
		formLogin.submit();
	}
	function FaleConosco() {
		window.location.href = 'contato.php';
	}
</script>
<?php

include("menu_geral.php");

$sql = "SELECT * FROM texto WHERE tex_id='RESUMO_ABERTURA_IDENTIFICACAO'";
$query = $db->query($sql);
$row = $query->fetch_object();
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-success">
      	<h1><i class="oi oi-account-login"></i> Entrar</h1>
			</div>
    </div>
	</div>
	<?php if (isset($authFailed))	 { ?>
		<div class="row">
			<div class="col-sm-12 col-md-8 col-lg-6 mx-auto">
				<div class="alert alert-danger">
					<?=$txt['erro_login_inexistente']?>
				</div>
			</div>
		</div>
	<?php } ?>
	<div class="row mb-4">
		<div class="col-md-12">
			<div class="alert alert-info">
				<?=traduz($row->tex_texto,$lingua)?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12 col-md-8 col-lg-6 mx-auto">
			<form id="formLogin" method="post" action="xt_loga.php" class="needs-validation">
				<div class="form-row mb-2">
				 	<div class="col-md-3">
						<label for="frm_login"><?=$txt['frm_login']?></label>
					</div>
					<div class="col-md-9">
						<input id="frm_login" type="text" class="form-control" name="login" required>
					</div>
				</div>
				<div class="form-row">
        	<input name="botao" type="submit" class="btn btn-primary mr-2 mb-2" value="<?=$txt['login_titulo_conectar']?>">
					<a class="btn btn-primary mr-2 mb-2" href="<?=$dir['base_URL']?>usuarios_inclusao.php"><?=$txt['login_criar_nova_conta']?></a>
					<a class="btn btn-primary mb-2" href="<?=$dir['base_URL']?>admin">Entrar como admin</a>
				</div>
      </form>
		</div>
	</div>
</div>
</body>
</html>
