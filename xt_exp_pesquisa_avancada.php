<?php
include("conexao.inc.php");

if ($ck_area_tematica == "1") {
	if (isset($tema)) { 
		$i = 0;
		$j = 0;
		$z = 0;
		$arrayTema = array();
		while($i < count($tema)){ 
			$temaID = $tema[$i];
			$sql = "SELECT * FROM rel_area_experiencia WHERE rae_id_area=$temaID";
			$query = $db->query($sql);
			$num = $query->num_rows;
			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayTema[$z][$j] = $row->rae_id_experiencia;
					$j++;
				}
				$z++;
	
			} // fim do if que verifica se h� registros
			else {
				$arrayTema[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

   			$i++;
	
		} // la�o do while
				
		if ($z > 0) { // os selects montaram pelo menos 1 array?
		
			if ($rd_at == "1") { // verifica se � E ou OU
			
				if ($z == 1) {
					$intersecaoArraysAT = $arrayTema[0];
				}
				
				if ($z > 1) {
		
					$intersecaoArraysAT = array_intersect($arrayTema[0],$arrayTema[1]);
				
					if ($z > 2) {
				
						for ($x = 2; $x < $z; $x++) {
			
							$intersecaoArraysAT = array_intersect($intersecaoArraysAT,$arrayTema[$x]);
					
						} // la�o do for
				
					}
			
				}
	
			} // n�o � E, logo � OU
			else {
				
				if ($z == 1) {
					$intersecaoArraysAT = $arrayTema[0];
				}
		
				if ($z > 1) {
		
					$intersecaoArraysAT = array_merge($arrayTema[0],$arrayTema[1]);
				
					if ($z > 2) {
				
						for ($x = 2; $x < $z; $x++) {
			
							$intersecaoArraysAT = array_merge($intersecaoArraysAT,$arrayTema[$x]);
					
						} // la�o do for
			
					} // fim do if que verifica se i � maior que 2
			
				} // fim do if que verifica se i � maior que 1
		
			$intersecaoArraysAT = array_unique($intersecaoArraysAT);
			
			} // fim do if que verifica se � E ou OU
		
			sort($intersecaoArraysAT,SORT_NUMERIC);
		
		} // fim do if que verifica se a sele��o retornou mais de 0 arrays
		
	} // fim do if que verifica se a vari�vel foi setada
	
}

if ($ck_bibliografia == "1") {
	if (isset($bib)) { 
		$i = 0;
		$j = 0;
		$z = 0;
		$arrayBib = array();
		while($i < count($bib)){ 
			$bibID = $bib[$i];
			$sql = "SELECT * FROM rel_bib_experiencia WHERE rbe_id_bibliografia=$bibD";
			$query = $db->query($sql);
			$num = $query->num_rows;
			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayBib[$z][$j] = $row->rbe_id_experiencia;
					$j++;
				}
				$z++;
	
			} // fim do if que verifica se h� registros
			else {
				$arrayBib[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

   			$i++;
	
		} // la�o do while
				
		if ($z > 0) { // os selects montaram pelo menos 1 array?
		
			if ($rd_bb == "1") { // verifica se � E ou OU
			
				if ($z == 1) {
					$intersecaoArraysBB = $arrayBib[0];
				}
				
				if ($z > 1) {
		
					$intersecaoArraysBB = array_intersect($arrayBib[0],$arrayBib[1]);
				
					if ($z > 2) {
				
						for ($x = 2; $x < $z; $x++) {
			
							$intersecaoArraysBB = array_intersect($intersecaoArraysBB,$arrayBib[$x]);
					
						} // la�o do for
				
					}
			
				}
	
			} // n�o � E, logo � OU
			else {
				
				if ($z == 1) {
					$intersecaoArraysBB = $arrayBib[0];
				}
		
				if ($z > 1) {
		
					$intersecaoArraysBB = array_merge($arrayBib[0],$arrayBib[1]);
				
					if ($z > 2) {
				
						for ($x = 2; $x < $z; $x++) {
			
							$intersecaoArraysBB = array_merge($intersecaoArraysBB,$arrayBib[$x]);
					
						} // la�o do for
			
					} // fim do if que verifica se i � maior que 2
			
				} // fim do if que verifica se i � maior que 1
		
			$intersecaoArraysBB = array_unique($intersecaoArraysBB);
			
			} // fim do if que verifica se � E ou OU
		
			sort($intersecaoArraysBB,SORT_NUMERIC);
		
		} // fim do if que verifica se a sele��o retornou mais de 0 arrays
		
	} // fim do if que verifica se a vari�vel foi setada
	
}

if ($ck_areas_geograficas == "1") {
	if (isset($geo)) { 
		$i = 0;
		$j = 0;
		$z = 0;
		$arrayGeo = array();
		while($i < count($geo)){ 
			$geoID = $geo[$i];
			$sql = "SELECT * FROM rel_geo_experiencia WHERE rge_id_geo=$geoID";
			$query = $db->query($sql);
			$num = $query->num_rows;
			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayGeo[$z][$j] = $row->rge_id_experiencia;
					$j++;
				}
				$z++;
	
			} // fim do if que verifica se h� registros
			else {
				$arrayGeo[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

   			$i++;
	
		} // la�o do while
				
		if ($z > 0) { // os selects montaram pelo menos 1 array?
		
			if ($rd_ag == "1") { // verifica se � E ou OU
			
				if ($z == 1) {
					$intersecaoArraysAG = $arrayGeo[0];
				}
				
				if ($z > 1) {
		
					$intersecaoArraysAG = array_intersect($arrayGeo[0],$arrayGeo[1]);
				
					if ($z > 2) {
				
						for ($x = 2; $x < $z; $x++) {
			
							$intersecaoArraysAG = array_intersect($intersecaoArraysAG,$arrayGeo[$x]);
					
						} // la�o do for
				
					}
			
				}
	
			} // n�o � E, logo � OU
			else {
				
				if ($z == 1) {
					$intersecaoArraysAG = $arrayGeo[0];
				}
		
				if ($z > 1) {
		
					$intersecaoArraysAG = array_merge($arrayGeo[0],$arrayGeo[1]);
				
					if ($z > 2) {
				
						for ($x = 2; $x < $z; $x++) {
			
							$intersecaoArraysAG = array_merge($intersecaoArraysAG,$arrayGeo[$x]);
					
						} // la�o do for
			
					} // fim do if que verifica se i � maior que 2
			
				} // fim do if que verifica se i � maior que 1
		
			$intersecaoArraysAG = array_unique($intersecaoArraysAG);
			
			} // fim do if que verifica se � E ou OU
		
			sort($intersecaoArraysAG,SORT_NUMERIC);
		
		} // fim do if que verifica se a sele��o retornou mais de 0 arrays
		
	} // fim do if que verifica se a vari�vel foi setada
	
}

if ($ck_chaves == "1") {
	if (isset($chaves)) { 
		$i = 0;
		$j = 0;
		$z = 0;
		$arrayChaves = array();
		while($i < count($chaves)){ 
			$chaveID = $chaves[$i];
			$sql = "SELECT * FROM experiencia_chave,palavra_chava WHERE pc_id=ec_id_palavrada AND pc_id=$chaveID";
			$query = $db->query($sql);
			$num = $query->num_rows;
			if ($num > 0) {
				while($row = $query->fetch_object()) {
					$arrayChaves[$z][$j] = $row->ec_id_experiencia;
					$j++;
				}
				$z++;
	
			} // fim do if que verifica se h� registros
			else {
				$arrayChaves[$z][$j] = 0; // se o relacionamento n�o possui experiências, zera
				$j++;
				$z++;
			}

   			$i++;
	
		} // la�o do while
				
		if ($z > 0) { // os selects montaram pelo menos 1 array?
		
			if ($rd_pc == "1") { // verifica se � E ou OU
			
				if ($z == 1) {
					$intersecaoArraysPC = $arrayChaves[0];
				}
				
				if ($z > 1) {
		
					$intersecaoArraysPC = array_intersect($arrayChaves[0],$arrayChaves[1]);
				
					if ($z > 2) {
				
						for ($x = 2; $x < $z; $x++) {
			
							$intersecaoArraysPC = array_intersect($intersecaoArraysPC,$arrayChaves[$x]);
					
						} // la�o do for
				
					}
			
				}
	
			} // n�o � E, logo � OU
			else {
				
				if ($z == 1) {
					$intersecaoArraysPC = $arrayChaves[0];
				}
		
				if ($z > 1) {
		
					$intersecaoArraysPC = array_merge($arrayChaves[0],$arrayChaves[1]);
				
					if ($z > 2) {
				
						for ($x = 2; $x < $z; $x++) {
			
							$intersecaoArraysPC = array_merge($intersecaoArraysPC,$arrayChaves[$x]);
					
						} // la�o do for
			
					} // fim do if que verifica se i � maior que 2
			
				} // fim do if que verifica se i � maior que 1
		
			$intersecaoArraysPC = array_unique($intersecaoArraysPC);
			
			} // fim do if que verifica se � E ou OU
		
			sort($intersecaoArraysPC,SORT_NUMERIC);
		
		} // fim do if que verifica se a sele��o retornou mais de 0 arrays
		
	} // fim do if que verifica se a vari�vel foi setada
}

if ($ck_acompanhamento == "1") {
	$sqlAcompanha = "ex_acompanhamento='$acompanha'";
}

if ($ck_status == "1") {
	$sqlStatus = "ex_status=$status";
}

if ($ck_tipo == "1") {
	$sqlTipo = "ex_tipo=$tipo";
}

$sqlMaster = "SELECT ex_id,ex_descricao FROM experiencia WHERE ex_liberacao='S' AND ";

if(strlen(trim($sqlAcompanha)) > 0) {
	$sqlMaster .= "$sqlAcompanha AND ";
}
if(strlen(trim($sqlStatus)) > 0) {
	$sqlMaster .= "$sqlStatus AND ";
}
if(strlen(trim($sqlTipo)) > 0) {
	$sqlMaster .= "$sqlTipo AND ";
}

/********* vou verificar se os arrays de intersect vieram com dados *************/

$arrays = 0;
if (isset($intersecaoArraysAT) && count($intersecaoArraysAT) > 0) {
	$arraysCompletos[$arrays] = $intersecaoArraysAT;
	$arrays++;
	$arrayUnico = $intersecaoArraysAT;
}
if (isset($intersecaoArraysBB) && count($intersecaoArraysBB) > 0) {
	$arraysCompletos[$arrays] = $intersecaoArraysBB;
	$arrays++;
	$arrayUnico = $intersecaoArraysBB;
}
if (isset($intersecaoArraysAG) && count($intersecaoArraysAG) > 0) {
	$arraysCompletos[$arrays] = $intersecaoArraysAG;
	$arrays++;
	$arrayUnico = $intersecaoArraysAG;
}
if (isset($intersecaoArraysPC) && count($intersecaoArraysPC) > 0) {
	$arraysCompletos[$arrays] = $intersecaoArraysPC;
	$arrays++;
	$arrayUnico = $intersecaoArraysPC;
}

if (isset($arraysCompletos) && count($arraysCompletos) == 1) {
	$arrayMontado = "";
	$arrayFinal = $arraysCompletos[0];
	while (list ($key, $val) = each ($arraysCompletos[0])) {
    	$arrayMontado .= "$val,";
	}
	$arrayMontado = substr($arrayMontado,0,-1);
}
else if (count($arraysCompletos) > 1) {
	$arrayFinal = array_intersect($arraysCompletos[0],$arraysCompletos[1]);
	if (count($arraysCompletos) > 2) {
		for ($x = 2; $x < count($arraysCompletos); $x++) {
			$arrayFinal = array_intersect($arrayFinal,$arraysCompletos[$x]);
		}
	}
	$arrayMontado = "";
	while (list ($key, $val) = each ($arrayFinal)) {
    	$arrayMontado .= "$val,";
	}
	$arrayMontado = substr($arrayMontado,0,-1);
}

//$sqlMaster .= "ex_id IN ('$arrayMontado')";

$temp = substr(trim($sqlMaster),-3);
if ($temp == "AND") {
	$sqlMaster = substr(trim($sqlMaster),0,-3);
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57">&nbsp; </td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><span class="oi oi-book text-white lead"></span><img src="imagens/ico_experiencias.gif" style="display:none" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Pesquisa 
              avan&ccedil;ada de experi&ecirc;ncias</strong></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr> 
          <td><p><strong>Resultados</strong><strong></strong></p>
              </td>
        </tr>
        <tr> 
          <td height="1" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
          <tr> 
            <td>
<?php

if (count($arrayFinal) > 0) {

	echo "<p>Foram encontrados as seguintes experiências com os argumentos providos:
	<br><br>";

	$i = 1;
	for ($x = 0; $x < count($arrayFinal); $x ++) {
		$tempID = $arrayFinal[$x];
		$sql = $sqlMaster . " AND ex_id=$tempID";
		$query = $db->query($sql);
		$num = $query->num_rows;
		if ($num > 0) {
			$row = $query->fetch_object();
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
			$i++;
		}
	}
		
	echo "</p>";			

} 
else {
		
	echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";

} // fim do if que verifica se a busca retornou os resultados
?>
			</td>
        </tr>
      </table>

	  </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>