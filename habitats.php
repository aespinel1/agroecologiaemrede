<?php
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Agroecologia em rede</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript">
function enviaEmail(url) {
	var email = prompt("Digite um endereço de e-mail:","");
	if (email != null && email != "") {
		url = url + '&email=' + email;
		window.location.href=url;
	}
}
</script>
</head>

<body bgcolor="#EBEBEB" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" class="fundoFixo">
<table width="760" height="100" border="0" cellpadding="0" cellspacing="0" background="imagens/fundo_barra.jpg">
  <tr> 
    <td width="190"><img src="imagens/logo_sisgea.jpg" width="190" height="100"></td>
    <td width="513">&nbsp;</td>
    <td width="57">&nbsp; </td>
  </tr>
</table>
<table width="760" border="0" cellpadding="5" cellspacing="0">
  <tr> 
    <td width="173" valign="top"> 
      <?php include("menu.inc.php"); ?>
	  <?php if (isset($busca)) { ?>
	      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="relatorio_simples.php?tipo=HA1&acao=IM&busca=<?= isset($busca) ? $busca : ""; ?>"><img src="imagens/ico_imprimir.gif" width="59" height="12" border="0"></a><br>
    	    <br>
        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onClick="enviaEmail('relatorio_simples.php?tipo=HA1&acao=EM&busca=<?= isset($busca) ? $busca : ""; ?>');"><img src="imagens/ico_envia_email.gif" width="99" height="13" border="0"></a></p>
	  <?php } else if (isset($habitat)) { ?>
	      <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="relatorio_simples.php?tipo=HA2&acao=IM&haDesc=<?php echo $haDesc; ?>&habitat=<?php echo $habitat; ?>"><img src="imagens/ico_imprimir.gif" width="59" height="12" border="0"></a><br>
    	    <br>
        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a href="#" onClick="enviaEmail('relatorio_simples.php?tipo=HA2&acao=EM&haDesc=<?php echo $haDesc; ?>&habitat=<?php echo $habitat; ?>');"><img src="imagens/ico_envia_email.gif" width="99" height="13" border="0"></a></p>
	   <?php } ?>
    </td>
    <td width="567" valign="top"> <table width="567" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="21" bgcolor="#80AAD2"> <div align="left" style="display:none"><img src="imagens/barra_azul_esquerda.gif" width="18" height="32"></div></td>
          <td width="43" bgcolor="#80AAD2"><p><img src="imagens/ico_tabela_simples.gif" width="34" height="32"></p></td>
          <td width="484" bgcolor="#80AAD2"> <p><strong>Habitats</strong><b></b></p></td>
          <td width="19" bgcolor="#80AAD2"> <div align="right" style="display:none"><img src="imagens/barra_azul_direita.gif" width="18" height="32"></div></td>
        </tr>
      </table>
      <br>
      <br>
	   <form name="form1" method="post" action="habitats.php">
      <table width="567" border="0" cellpadding="5" cellspacing="1">
        <tr> 
          <td><p><strong>Busca simples</strong> - digite 
              o nome da &aacute;rea no campo abaixo</p></td>
        </tr>
        <tr> 
          <td height="1" bgcolor="#000000"> <div align="center"></div></td>
        </tr>
        <tr> 
          <td>
              <input name="busca" type="text" id="busca" size="80" maxlength="255" value="<?= isset($busca) ? $busca : ""; ?>">
              <input name="Submit" type="submit" class="botaoLogin" value="Buscar" class="btn btn-success"> </td>
        </tr>
      </table>
      </form>
<?php
if (isset($busca)) {
	$busca = strtoupper($busca);
	$sql = "SELECT * FROM habitat WHERE UCASE(ha_descricao) LIKE '$busca%' ORDER BY ha_descricao";
	$query = $db->query($sql);
	$num = $query->num_rows;
	
	if ($num > 0) {
	
		echo "<p class=\"textoPreto10px\">Foram encontrados os seguintes resultados com o argumento <b>\"$busca\"</b>:<br><br>";

		$i = 1;
		while ($row = $query->fetch_object()) {
			
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"habitats.php?haDesc=$row->ha_descricao&habitat=$row->ha_id\" class=\"preto\">$row->ha_descricao</a><br><br>";
	
			$i++;

		}
		
		echo "</p>";			

	} 
	else {
		
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";

	} // fim do if que verifica se a busca retornou os resultados
	
} // fim do if que verifica se uma busca foi feita

if (isset($habitat)) {
	
	$sql = "SELECT * FROM habitat,rel_habitat_experiencia,experiencia WHERE ha_id=$habitat AND ha_id=rhe_id_habitat ".
		   "AND ex_id=rhe_id_experiencia AND ex_liberacao='S'";
	$query = $db->query($sql);
	$num = $query->num_rows;
	
	if ($num > 0) {
	
		echo "<p class=\"textoPreto10px\">Foram encontrados as seguintes experiências para o habitat <b>\"$haDesc\"</b>:<br><br>";

		$i = 1;
		while ($row = $query->fetch_object()) {
			
			echo "&nbsp;&nbsp;&nbsp;&nbsp;$i. <a href=\"experiencias.php?experiencia=$row->ex_id\" class=\"preto\">$row->ex_descricao</a><br><br>";
			
			$i++;

		}
		
		echo "</p>";			

	} 
	else {
		
		echo "<p align=\"center\" class=\"textoPreto10px\"><strong>N�o h� experiências neste habitat!</strong></p>";

	} // fim do if que verifica se a busca retornou os resultados
	
} // fim do if que verifica se o usuário clicou em algum habitat
?>

	  </td>
  </tr>
</table>
<p>&nbsp;</p>
</body>
</html>
<?php
$db->close();
?>