<?php include("conexao.inc.php"); ?>
<!DOCTYPE html>
<html>
<head>
<title>Relat&oacute;rio</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table width="676" border="0" cellpadding="3">
  <tr> 
    <td width="350" ><img src="imagens/agroecologia_em_rede.gif" width="149" height="34"></td>
    <td width="439">
<div align="right"></div></td>
  </tr>
  <tr> 
    <td height="1" colspan="2" bgcolor="#000000"> <div align="center"></div></td>
  </tr>
  <tr> 
    <td colspan="2">
	
<?php
$busca = strtoupper($argumento);

if ($tipo == "1") {
	$sql = "SELECT * FROM bibliografia WHERE bb_liberacao='1' AND (UCASE(bb_titulo) LIKE '%$busca%' OR 
			UCASE(bb_detalhes) LIKE '%$busca%' OR UCASE(bb_resumo) LIKE '%$busca%' OR UCASE(bb_titulo_todo) LIKE '%$busca%' OR 
			UCASE(bb_comp_todo) LIKE '%$busca%' OR UCASE(bb_titulo) LIKE '%$busca%') ORDER BY bb_titulo_todo";
}
else if ($tipo == "2") {
	$sql = str_replace("\\'","'",$consulta);
	//$sql = $consulta;
}
//echo $sql;
//return;
$query = $db->query($sql);
$num = $query->num_rows;

if ($num > 0) {
	echo "<table border=\"0\" cellpadding=\"3\" cellspacing=\"1\">\n";
	echo "<tr bgcolor=\"#B9D69E\">\n";
	echo "<td class=\"textoPreto\"><strong>T�tulo</strong></td>\n";
	echo "<td class=\"textoPreto\" align=\"center\"><strong>Autor</strong></td>\n";
	echo "<td class=\"textoPreto\" align=\"center\"><strong>Ano</strong></td>\n";
	echo "</tr>\n";
	
	while ($row = $query->fetch_object()) {
	
		$sql = "SELECT * FROM bib_autor,bib_autor_todo WHERE bia_id=bat_id_autor AND bat_id_bib=$row->bb_id";
		$queryAutor = $db->query($sql);
		$autor = "";
		while ($rowAutor = $queryAutor->fetch_object()) {
			$autor .= " $rowAutor->bia_nome;";
		}
		
		$autor = substr($autor,0,-1);
		
		$varDesc = str_replace($row->bb_titulo_todo,"\"","");
		echo "<tr bgcolor=\"#cccccc\">
				<td class=\"textoPreto10px\">
					$row->bb_titulo_todo
				</td>
				<td class=\"textoPreto10px\">$autor</td>
				<td class=\"textoPreto10px\">$row->bb_ano</td>
				</tr>";
	}
	echo "</table>";
	
}
else {
	echo "<p align=\"center\" class=\"textoPreto10px\"><strong>Sua busca n�o retornou resultados!</strong></p>";
} // fim do if que verifica se a busca retornou os resultados
?>
          </td>
        </tr>
      </table> 
<script language="JavaScript">
window.print();
alert('O relatório está sendo impresso. Você será redirecionado dentro de 10 segundos.');
var delay = 10; // Delay in seconds
var targetURL = "bib_pesquisa.php?busca=<?= isset($busca) ? $busca : ""; ?>&init=0&pagina=1"; // URl to load
//setTimeout('self.location.replace(targetURL)', (delay * 1000));
setTimeout('history.back(1)', (delay * 1000));
</script>
	</td>
  </tr>
</table>
</body>
</html>
