<?php
include("conexao.inc.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>Relacionamentos</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="estilo.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#EBEBEB">
  
<table width="460" border="0" align="center" cellpadding="5" cellspacing="1">
  <tr> 
      <td colspan="2"> <p>
	  <strong>Endere&ccedil;o(s) - <?php echo $nome; ?></strong>
	  </p></td>
    </tr>
    <tr> 
      <td height="1" bgcolor="#000000" colspan="2"> <div align="center"></div></td>
    </tr>
<?php
if ($tipo == 'usu') {
	$sql = "SELECT * FROM endereco_usuario,tipo_enderecos WHERE eu_id_endereco=te_id AND eu_id_usuario='$id'";
	$query = $db->query($sql);
	while ($row = $query->fetch_object()) {
?>
    <tr> 
      <td align="right" valign="top"><strong><?php echo $row->te_descricao; ?></strong></td>
	  <td>
	  <strong>Endereço</strong>:<br><?php echo $row->eu_endereco; ?><br>
  	  <strong>Cidade / Estado</strong>:<br><?php echo $row->eu_cidade; ?> / <?php echo $row->eu_estado; ?><br>
	  <strong>CEP</strong>:<br><?php echo $row->eu_cep; ?><br>
	  <strong>País</strong>:<br><?php echo $row->eu_pais; ?>
	  </td>
    </tr>
<?php
	}
}
else {
	$sql = "SELECT a.*, b.*, c.nome as cidade, c.UF FROM frm_instituicao as a,tipo_enderecos as b, _BR as c WHERE ei_id_endereco=te_id AND ei_cidade=c.id AND ei_id_in=$id";
	$query = $db->query($sql);
	while ($row = $query->fetch_object()) {
?>
    <tr> 
      <td align="right" valign="top"><strong><?php echo $row->te_descricao; ?></strong></td>
	  <td>
	  <strong>Endereço</strong>:<br><?php echo $row->ei_endereco; ?><br>
  	  <strong>Cidade / Estado</strong>:<br><?php echo $row->cidade; ?> / <?php echo $row->UF; ?><br>
	  <strong>CEP</strong>:<br><?php echo $row->ei_cep; ?><br>
	  <strong>País</strong>:<br><?php echo $row->ei_pais; ?>
	  </td>
    </tr>
<?php
	}
}
?>
    <tr bgcolor="#000000"> 
      <td height="1" colspan="2"> <div align="center"></div></td>
    </tr>
  </table>
  <br><br>
<table width="460" border="0" align="center" cellpadding="5" cellspacing="1">
  <tr> 
      <td colspan="2"> <p>
	  <strong>Telefone(s) - <?php echo $nome; ?></strong>
	  </p></td>
    </tr>
    <tr> 
      <td height="1" bgcolor="#000000" colspan="2"> <div align="center"></div></td>
    </tr>
<?php
if ($tipo == 'usu') {
	$sql = "SELECT * FROM telefone_usuario,tipo_telefones WHERE tu_id_telefone=tt_id AND tu_id_usuario='$id'";
	$query = $db->query($sql);
	while ($row = $query->fetch_object()) {
?>
    <tr> 
      <td align="right" valign="top" width="150"><strong><?php echo $row->tt_descricao; ?></strong></td>
	  <td width="310">
		<?php echo $row->tu_telefone; ?>
	  </td>
    </tr>
<?php
	}
}
else {
	$sql = "SELECT * FROM telefone_instituicao,tipo_telefones WHERE ti_id_instituicao=tt_id AND ti_id_instituicao=$id";
	$query = $db->query($sql);
	while ($row = $query->fetch_object()) {
?>
    <tr> 
      <td align="right" valign="top" width="150"><strong><?php echo $row->tt_descricao; ?></strong></td>
	  <td width="310">
		<?php echo $row->ti_telefone; ?>
	  </td>
    </tr>
<?php
	}
}
?>
    <tr bgcolor="#000000"> 
      <td height="1" colspan="2"> <div align="center"></div></td>
    </tr>
  </table>
</body>
</html>
<?php
$db->disconnect;
?>
